/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;

$(document).ready(function() {
    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateUsedContentsList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateUsedContentsList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateUsedContentsList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateUsedContentsList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateUsedContentsList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateUsedContentsList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateUsedContentsList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateUsedContentsList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#btnRegistered").click(function(){
        pageMove("/statistics/contents/registered");
    });

    $("#sectionList").change(function() {
        updateSelectBoxContainer();
    });

    $("#mainList").change(function() {
        var value = $("#sectionList option:selected").val();
        if (value == "store") {
            changeServiceList();
        } else {
            updateUsedContentsList();
        }
    });

    $("#storeList").change(function() {
        changeStoreList();
    });

    $(".btnSearch").click(function() {
        updateUsedContentsList();
    });

    $("#keyword").keydown(function(event) {
        if (event.keyCode == 13) {
            updateUsedContentsList();
        }
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDatePickerOnSelectCallback("updateUsedContentsList");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;

        var strHash = document.location.hash.replace("#", "");
        var section = findGetParameter(strHash, "section");
        $("#sectionList").val(section);

        updateSelectBoxContainer();
    });

    if (document.location.hash) {
        var strHash = document.location.hash.replace("#", "");
        var section = findGetParameter(strHash, "section");
        $("#sectionList").val(section);
        hasChangedHash = true;
    }

    updateSelectBoxContainer();
});


getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#mainList").html("");
        $("#storeList").html("");
        return;
    }

    var serviceListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#mainList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#mainList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#mainList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
        $("#mainList").attr("disabled", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var svcSeq = findGetParameter(strHash, "svcSeq");
            $("#mainList").val(svcSeq);
        } else {
            $("#mainList option:eq(0)").prop("selected", true);
        }
    }

    var value = $("#sectionList option:selected").val();
    if (value == "service") {
        if (!didFirstLayout) {
            initUsedContentsList();
            didFirstLayout = true;
            return;
        }
        updateUsedContentsList();
        return;
    }

    getStoreList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#mainList").html("");
    $("#storeList").html("");
}

getCpList = function() {
    callByGet("/api/codeInfo?comnCdCtg=CP", "didReceiveCpList", '', "didNotReceiveCpList");
}

didReceiveCpList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.nocp.msg");
        }
        popAlertLayer(msg);
        $("#mainList").html("");
        return;
    }

    var serviceListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#mainList").html(serviceListHtml);

    if (hasChangedHash) {
        var strHash = document.location.hash.replace("#", "");
        var cpSeq = findGetParameter(strHash, "cpSeq");
        $("#mainList").val(cpSeq);
    } else {
        $("#mainList option:eq(0)").prop("selected", true);
    }

    if (!didFirstLayout) {
        initUsedContentsList();
        didFirstLayout = true;
        return;
    }

    updateUsedContentsList();
}

didNotReceiveCpList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#mainList").html("");
}

getStoreList = function() {
    var svcSeq = ($("#mainList option:selected").val() == 0) ? "" : $("#mainList option:selected").val();
    formData("NoneForm" , "comnCdValue", svcSeq);
    callByGet("/api/codeInfo?comnCdCtg=STORE", "didReceiveStoreList", "NoneForm", "didNotReceiveStoreList");
    formDataDeleteAll("NoneForm");
}

didReceiveStoreList = function(data) {
    var storeListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";

    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.nostore.msg");
        }
        popAlertLayer(msg);
        $("#storeList").html(storeListHtml);

        if (!didFirstLayout) {
            initUsedContentsList();
            didFirstLayout = true;
            return;
        }

        updateUsedContentsList();
        return;
    }

    $(data.result.codeList).each(function(i, item) {
        storeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#storeList").html(storeListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#storSeq").val() != 0) {
        $("#storeList option").not("[value='"+ $("#storSeq").val() + "']").remove();
        $("#storeList option[value='"+ $("#storSeq").val() + "']").prop("selected", true);
        $("#storeList").attr("disabled", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var storSeq = findGetParameter(strHash, "storSeq");
            $("#storeList").val(storSeq);
        } else {
            $("#storeList option:eq(0)").prop("selected", true);
        }
    }

    if (!didFirstLayout) {
        initUsedContentsList();
        didFirstLayout = true;
        return;
    }

    updateUsedContentsList();
}

didNotReceiveStoreList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#storeList").html("<option value='0'>"+getMessage("select.all")+"</option>");
}

changeServiceList = function() {
    getStoreList();
}

changeStoreList = function() {
    updateUsedContentsList();
}

initUsedContentsList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("table.category"),
        getMessage("contents.cpNm"),
        getMessage("contents.table.name"),
        getMessage("contents.table.playCount"),
        getMessage("contents.table.playTime"),
        getMessage("contents.table.avgPlayTime"),
        "seq"
    ];
    var colModel = [
        {name:"num", index: "num", align:"center", width:40, hidden:true},
        {name:"ctgNm", index:"ctgNm", align:"center"},
        {name:"cpNm", index:"cpNm", align:"center"},
        {name:"contsTitle", index: "contsTitle", align:"center",formatter:pointercursor},
        {name:"contsTotalPlayCount", index:"contsTotalPlayCount", align:"center"},
        {name:"contsTotalPlayTime", index:"contsTotalPlayTime", align:"center"},
        {name:"contsAvgPlayTime", index:"contsAvgPlayTime", align:"center"},
        {name:"contsSeq", index:"contsSeq", hidden:true}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.usedContentsList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "ctgNm",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "usedContentsList",
            section : $("#sectionList option:selected").val(),
            startDate : $("#startDate").val(),
            endDate : $("#endDate").val()
        },

        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");

            updateParamBeforeSending();

            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");
                var target = findGetParameter(strHash, "target");
                var keyword = findGetParameter(strHash, "keyword");

                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);

                $("#target").val(target);
                $("#keyword").val(keyword);

                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
                if (keyword != null) {
                    myPostData.target = target;
                    myPostData.keyword = keyword;
                } else {
                    delete myPostData.target;
                    delete myPostData.keyword;
                }
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
            $(this).find(".pointer").parent("td").addClass("pointer");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        onCellSelect: function(rowId, columnId, cellValue, event){
            // 콘텐츠 ID 선택시
            if(columnId == 3){
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");

                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                var hashlocation = "";

                hashlocation += "dateType=" + dateType;
                var startDate = $("#startDate").val();
                if (startDate) {
                    hashlocation += "&startDate=" + startDate;
                }
                var endDate = $("#endDate").val();
                if (endDate) {
                    hashlocation += "&endDate=" + endDate;
                }

                hashlocation += "&section=" + $("#sectionList option:selected").val();
                var value = $("#sectionList option:selected").val();
                if (value == "service") {
                    hashlocation += "&svcSeq=" + $("#mainList option:selected").val();
                } else if (value == "cp") {
                    hashlocation += "&cpSeq=" + $("#mainList option:selected").val();
                } else if (value == "store") {
                    hashlocation += "&svcSeq=" + $("#mainList option:selected").val();
                    hashlocation += "&storSeq=" + $("#storeList option:selected").val();
                }

                pageMove("/statistics/contents/used/"+list.contsSeq+"#"+hashlocation);

            }
         }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateUsedContentsList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            search : true,
            postData: {
                type : "usedContentsList",
                section : $("#sectionList option:selected").val(),
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val(),
                target: $("#target").val(),
                keyword: $("#keyword").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");

    var value = $("#sectionList option:selected").val();

    var svcSeq = 0;
    var storSeq = 0;
    var cpSeq = 0;
    if (value == "service") {
        svcSeq = $("#mainList option:selected").val();
        storSeq = 0;
        cpSeq = 0;
    } else if (value == "cp") {
        svcSeq = 0;
        storSeq = 0;
        cpSeq = $("#mainList option:selected").val();

        var memberSec = $("#memberSec").val();
        if (memberSec == "04") {
            svcSeq = $("#svcSeq").val();
        } else if (memberSec == "05") {
            svcSeq = $("#svcSeq").val();
            storSeq = $("#storSeq").val();
        }
    } else if (value == "store") {
        svcSeq = $("#mainList option:selected").val();
        storSeq = $("#storeList option:selected").val();
        cpSeq = 0;
    }

    var params = {
            "type" : "usedContentsList",
            "section" : $("#sectionList option:selected").val(),
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : svcSeq,
            "storSeq" : storSeq,
            "cpSeq" : cpSeq,
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
            "target" : $("#target").val(),
            "keyword" : $("#keyword").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

setSearchField = function() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        var searchHtml = "";

        for (var i = 0; i < colModel.length; i++) {
            var name = colModel[i].name;
            if (name == "contsTitle") {
                searchHtml += "<option value=\"" + name + "\">" + colNames[i] + "</option>";
            }
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
}

updateSelectBoxContainer = function() {
    var value = $("#sectionList option:selected").val();
    if (value == "service") {
        $("#mainListTitle").text(getMessage("product.comn.seviceNm"));
        toggleStoreListVisibility(false);
        getServiceList();
    } else if (value == "cp") {
        $("#mainListTitle").text(getMessage("cp.table.name"));
        $("#mainList").attr("disabled", false);
        toggleStoreListVisibility(false);
        getCpList();
    } else if (value == "store") {
        $("#mainListTitle").text(getMessage("product.comn.seviceNm"));
        toggleStoreListVisibility(true);
        getServiceList();
    }
}

updateParamBeforeSending = function() {
    var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
    var value = $("#sectionList option:selected").val();
    if (value == "service") {
        myPostData.svcSeq = $("#mainList option:selected").val();
        myPostData.storSeq = 0;
        myPostData.cpSeq = 0;
    } else if (value == "cp") {
        myPostData.svcSeq = 0;
        myPostData.storSeq = 0;
        myPostData.cpSeq = $("#mainList option:selected").val();

        var memberSec = $("#memberSec").val();
        if (memberSec == "04") {
            myPostData.svcSeq = $("#svcSeq").val();
        } else if (memberSec == "05") {
            myPostData.svcSeq = $("#svcSeq").val();
            myPostData.storSeq = $("#storSeq").val();
        }
    } else if (value == "store") {
        myPostData.svcSeq = $("#mainList option:selected").val();
        myPostData.storSeq = $("#storeList option:selected").val();
        myPostData.cpSeq = 0;
    }

    $('#jqgridData').jqGrid("setGridParam", { postData: myPostData });
}

toggleStoreListVisibility = function(visible) {
    if (visible) {
        $("#storeListTitle").show();
        $("#storeList").show();
    } else {
        $("#storeListTitle").hide();
        $("#storeList").hide();
    }
}

didCompleteLoad = function(obj) {
    setSearchField();

    //session
    if (sessionStorage.getItem("last-url") != null) {
        sessionStorage.removeItem('state');
        sessionStorage.removeItem('last-url');
    }

    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        hashlocation += "&section=" + $("#sectionList option:selected").val();
        var value = $("#sectionList option:selected").val();
        if (value == "service") {
            hashlocation += "&svcSeq=" + $("#mainList option:selected").val();
        } else if (value == "cp") {
            hashlocation += "&cpSeq=" + $("#mainList option:selected").val();
        } else if (value == "store") {
            hashlocation += "&svcSeq=" + $("#mainList option:selected").val();
            hashlocation += "&storSeq=" + $("#storeList option:selected").val();
        }

        var searchTarget = $("#target").val();
        var searchKeyword = $("#keyword").val();
        if ($(obj).context.p.search && searchKeyword) {
            hashlocation += "&target=" + searchTarget + "&keyword=" + searchKeyword;
        }
        document.location.hash = hashlocation;
    }
}