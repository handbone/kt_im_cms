/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;

$(document).ready(function() {
    $("#btnUsed").click(function() {
        pageMove("/statistics/contents/used");
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#sectionList").change(function() {
        updateRegisteredContentsList();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    initRegisteredContentsList();
});

initRegisteredContentsList = function() {
    var columnNames = [
        getMessage("common.service"),
        getMessage("select.all"),
        getMessage("contents.table.state.regist"),
        getMessage("contents.table.state.verify.Waiting"),
        getMessage("contents.table.state.verify.ing"),
        getMessage("contents.table.state.verify.Companion"),
        getMessage("contents.table.state.verify.success")
    ];

    var colModel = [
        {name:"name", index:"name", align:"center"},
        {name:"totalCnt", index:"totalCnt", align:"center"},
        {name:"saveCnt", index:"saveCnt", align:"center"},
        {name:"requestCnt", index:"requestCnt", align:"center"},
        {name:"verifyCnt", index:"verifyCnt", align:"center"},
        {name:"rejectCnt", index:"rejectCnt", align:"center"},
        {name:"completeCnt", index:"completeCnt", align:"center"}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.registeredContentsList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "name",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "registeredContentsList",
            section : $("#sectionList option:selected").val()
        },
        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));

                var section = findGetParameter(strHash, "section");
                if (section != undefined) {
                    $("#sectionList").val(section);
                    myPostData.section = $("#sectionList option:selected").val();
                    updateSectionColumnName();
                }

                myPostData.page = page;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateRegisteredContentsList = function() {
    updateSectionColumnName();

    $("#jqgridData").jqGrid("setGridParam",
        {
            postData: {
                type : "registeredContentsList",
                section : $("#sectionList option:selected").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

updateSectionColumnName = function() {
    var newColumnName = ($("#sectionList option:selected").val() == "service") ? getMessage("common.service") : getMessage("common.cp");
    $("#jqgridData").jqGrid('setLabel', "name", newColumnName);
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "registeredContentsList",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "section" : $("#sectionList option:selected").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;

        var section = $("#sectionList option:selected").val();
        if (section) {
            hashlocation += "&section=" + section;
        }

        document.location.hash = hashlocation;
    }
}