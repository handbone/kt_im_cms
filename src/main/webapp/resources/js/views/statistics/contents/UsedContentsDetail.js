/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;
var chgcolumnNames;

$(document).ready(function() {
    contentInfo();

    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateUsedContentsList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateUsedContentsList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateUsedContentsList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateUsedContentsList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateUsedContentsList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateUsedContentsList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateUsedContentsList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateUsedContentsList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#btnRegistered").click(function(){
        pageMove("/statistics/contents/registered");
    });

    $("#sectionList").change(function() {
        updateSelectBoxContainer();
    });

    $("#mainList").change(function() {
        var value = $("#sectionList option:selected").val();
        if (value == "store") {
            changeServiceList();
        } else {
            updateUsedContentsList();
        }
    });

    $("#storeList").change(function() {
        changeStoreList();
    });

    $(".btnSearch").click(function() {
        updateUsedContentsList();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDatePickerOnSelectCallback("updateUsedContentsList");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;

        var strHash = document.location.hash.replace("#", "");
        var section = findGetParameter(strHash, "section");
        $("#sectionList").val(section);

        updateSelectBoxContainer();
    });

    if (document.location.hash) {
        var strHash = document.location.hash.replace("#", "");
        var section = findGetParameter(strHash, "section");
        $("#sectionList").val(section);
        hasChangedHash = true;
    }

    updateSelectBoxContainer();
});


getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#mainList").html("");
        $("#storeList").html("");
        return;
    }

    var serviceListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#mainList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#mainList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#mainList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var svcSeq = findGetParameter(strHash, "svcSeq");
            $("#mainList").val(svcSeq);
        } else {
            $("#mainList option:eq(0)").prop("selected", true);
        }
    }

    var value = $("#sectionList option:selected").val();
    if (value == "service") {
        if (!didFirstLayout) {
            initUsedContentsList();
            didFirstLayout = true;
            return;
        }
        updateUsedContentsList();
        return;
    }

    getStoreList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#mainList").html("");
    $("#storeList").html("");
}

getCpList = function() {
    callByGet("/api/codeInfo?comnCdCtg=CP", "didReceiveCpList", '', "didNotReceiveCpList");
}

didReceiveCpList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.nocp.msg");
        }
        popAlertLayer(msg);
        $("#mainList").html("");
        return;
    }

    var serviceListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#mainList").html(serviceListHtml);

    if (hasChangedHash) {
        var strHash = document.location.hash.replace("#", "");
        var cpSeq = findGetParameter(strHash, "cpSeq");
        $("#mainList").val(cpSeq);
    } else {
        $("#mainList option:eq(0)").prop("selected", true);
    }

    if (!didFirstLayout) {
        initUsedContentsList();
        didFirstLayout = true;
        return;
    }

    updateUsedContentsList();
}

didNotReceiveCpList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#mainList").html("");
}

getStoreList = function() {
    var svcSeq = ($("#mainList option:selected").val() == 0) ? "" : $("#mainList option:selected").val();
    formData("NoneForm" , "comnCdValue", svcSeq);
    callByGet("/api/codeInfo?comnCdCtg=STORE", "didReceiveStoreList", "NoneForm", "didNotReceiveStoreList");
    formDataDeleteAll("NoneForm");
}

didReceiveStoreList = function(data) {
    var storeListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";

    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.nostore.msg");
        }
        popAlertLayer(msg);
        $("#storeList").html(storeListHtml);

        if (!didFirstLayout) {
            initUsedContentsList();
            didFirstLayout = true;
            return;
        }

        updateUsedContentsList();
        return;
    }

    $(data.result.codeList).each(function(i, item) {
        storeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#storeList").html(storeListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#storSeq").val() != 0) {
        $("#storeList option").not("[value='"+ $("#storSeq").val() + "']").remove();
        $("#storeList option[value='"+ $("#storSeq").val() + "']").prop("selected", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var storSeq = findGetParameter(strHash, "storSeq");
            $("#storeList").val(storSeq);
        } else {
            $("#storeList option:eq(0)").prop("selected", true);
        }
    }

    if (!didFirstLayout) {
        initUsedContentsList();
        didFirstLayout = true;
        return;
    }

    updateUsedContentsList();
}

didNotReceiveStoreList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#storeList").html("<option value='0'>"+getMessage("select.all")+"</option>");
}

changeServiceList = function() {
    getStoreList();
}

changeStoreList = function() {
    updateUsedContentsList();
}

initUsedContentsList = function() {
    var columnNames;
    var beforeDateType = dateType;
    if (dateType == "day") {
        columnNames = [
            getMessage("table.num"),
            getMessage("common.time"),
            getMessage("contents.table.playCount"),
            getMessage("contents.table.playTime"),
            getMessage("contents.table.avgPlayTime"),
            "seq"
        ]
    } else {
        columnNames = [
            getMessage("table.num"),
            getMessage("table.date"),
            getMessage("contents.table.playCount"),
            getMessage("contents.table.playTime"),
            getMessage("contents.table.avgPlayTime"),
            "seq"
        ]
    }

    var colModel = [
        {name:"num", index: "num", align:"center", width:40, hidden:true},
        {name:"useDt", index:"useDt", align:"center"},
        {name:"contsTotalPlayCount", index:"contsTotalPlayCount", align:"center"},
        {name:"contsTotalPlayTime", index:"contsTotalPlayTime", align:"center"},
        {name:"contsAvgPlayTime", index:"contsAvgPlayTime", align:"center"},
        {name:"contsSeq", index:"contsSeq", hidden:true}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.usedContentsDetailList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "useDt",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "usedContentsDetail",
            dateType : dateType,
            section : $("#sectionList option:selected").val(),
            startDate : $("#startDate").val(),
            contsSeq : $("#contsSeq").val(),
            endDate : $("#endDate").val()
        },

        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");

            updateParamBeforeSending();

            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");
                var contsSeq = $("#contsSeq").val();


                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);


                myPostData.contsSeq = contsSeq;
                myPostData.dateType = dateType;
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;

                if(beforeDateType != dateType){
                    if (dateType == "day") {
                        $("#jqgridData").jqGrid('setLabel', "useDt",getMessage("common.time"));
                    } else {
                        $("#jqgridData").jqGrid('setLabel', "useDt",getMessage("table.date"));
                    }
                }
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateUsedContentsList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            search : true,
            postData: {
                type : "usedContentsDetail",
                dateType : dateType,
                section : $("#sectionList option:selected").val(),
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val(),
                contsSeq : $("#contsSeq").val(),
            },
            page: 1
        }
    );

    if (dateType == "day") {
        $("#jqgridData").jqGrid('setLabel', "useDt", getMessage("common.time"));
    } else {
        $("#jqgridData").jqGrid('setLabel', "useDt", getMessage("table.date"));
    }

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");

    var value = $("#sectionList option:selected").val();

    var svcSeq = 0;
    var storSeq = 0;
    var cpSeq = 0;
    if (value == "service") {
        svcSeq = $("#mainList option:selected").val();
        storSeq = 0;
        cpSeq = 0;
    } else if (value == "cp") {
        svcSeq = 0;
        storSeq = 0;
        cpSeq = $("#mainList option:selected").val();

        var memberSec = $("#memberSec").val();
        if (memberSec == "04") {
            svcSeq = $("#svcSeq").val();
        } else if (memberSec == "05") {
            svcSeq = $("#svcSeq").val();
            storSeq = $("#storSeq").val();
        }
    } else if (value == "store") {
        svcSeq = $("#mainList option:selected").val();
        storSeq = $("#storeList option:selected").val();
        cpSeq = 0;
    }

    var titleHead = "";
    for (var i=0; i< $(".rsvSelect select").length; i++) {
        if ($(".rsvSelect select").eq(i).css("display") != "none" && $(".rsvSelect select").eq(i).attr("id") != "sectionList") {
            if (i != 0 && titleHead != "") {
                titleHead += ",";
            }
            titleHead += $(".rsvSelect select").eq(i).parent("span").prev().text()+":"+$(".rsvSelect select").eq(i).find("option:selected").text();
        }
    }

    var date = "";

    var params = {
            "type" : "usedContentsDetail",
            "section" : $("#sectionList option:selected").val(),
            "contsSeq" : $("#contsSeq").val(),
            titleHead : titleHead,
            contsTitle : $("#contsTitle").text(),
            "dateType" : dateType,
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : svcSeq,
            "storSeq" : storSeq,
            "cpSeq" : cpSeq,
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
    };

    downloadExcelFile("/api/statistics/download", params);
}


updateSelectBoxContainer = function() {
    var value = $("#sectionList option:selected").val();
    if (value == "service") {
        $("#mainListTitle").text(getMessage("product.comn.seviceNm"));
        toggleStoreListVisibility(false);
        getServiceList();
    } else if (value == "cp") {
        $("#mainListTitle").text(getMessage("cp.table.name"));
        toggleStoreListVisibility(false);
        getCpList();
    } else if (value == "store") {
        $("#mainListTitle").text(getMessage("product.comn.seviceNm"));
        toggleStoreListVisibility(true);
        getServiceList();
    }
}

updateParamBeforeSending = function() {
    var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
    var value = $("#sectionList option:selected").val();
    myPostData.contsSeq = $("#contsSeq").val();
    if (value == "service") {
        myPostData.svcSeq = $("#mainList option:selected").val();
        myPostData.storSeq = 0;
        myPostData.cpSeq = 0;
    } else if (value == "cp") {
        myPostData.svcSeq = 0;
        myPostData.storSeq = 0;
        myPostData.cpSeq = $("#mainList option:selected").val();

        var memberSec = $("#memberSec").val();
        if (memberSec == "04") {
            myPostData.svcSeq = $("#svcSeq").val();
        } else if (memberSec == "05") {
            myPostData.svcSeq = $("#svcSeq").val();
            myPostData.storSeq = $("#storSeq").val();
        }
    } else if (value == "store") {
        myPostData.svcSeq = $("#mainList option:selected").val();
        myPostData.storSeq = $("#storeList option:selected").val();
        myPostData.cpSeq = 0;
    }
    myPostData.dateType = dateType;

    $('#jqgridData').jqGrid("setGridParam", { postData: myPostData });
}

toggleStoreListVisibility = function(visible) {
    if (visible) {
        $("#storeListTitle").show();
        $("#storeList").show();
    } else {
        $("#storeListTitle").hide();
        $("#storeList").hide();
    }
}

didCompleteLoad = function(obj) {
    //session
    if (sessionStorage.getItem("last-url") != null) {
        sessionStorage.removeItem('state');
        sessionStorage.removeItem('last-url');
    }

    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        hashlocation += "&section=" + $("#sectionList option:selected").val();
        var value = $("#sectionList option:selected").val();
        if (value == "service") {
            hashlocation += "&svcSeq=" + $("#mainList option:selected").val();
        } else if (value == "cp") {
            hashlocation += "&cpSeq=" + $("#mainList option:selected").val();
        } else if (value == "store") {
            hashlocation += "&svcSeq=" + $("#mainList option:selected").val();
            hashlocation += "&storSeq=" + $("#storeList option:selected").val();
        }

        document.location.hash = hashlocation;
    }
}

contentInfo = function(){
    callByGet("/api/contents?contsSeq="+$("#contsSeq").val(), "contentsInfoSuccess", "NoneForm", "contentsInfoFail");
}

contentsInfoSuccess = function(data) {
    if(data.resultCode == "1000") {
        var item = data.result.contentsInfo;
        $("#contsTitle").html(item.contsTitle);

        listBack($(".btnBack"));
        formDataDeleteAll("NoneForm");
    } else {
        popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
    }
}

contentsInfoFail = function(data) {
    popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
}