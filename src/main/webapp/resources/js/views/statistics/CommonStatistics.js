/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var dateType = "week"; // 디폴트 값 변경 "custom" ==> "week"
var datePickerOnSelectCallback;

onFocusWeeklyHighlight = function() {
    $(".ui-datepicker-calendar tr td[data-handler='selectDay']").mouseover(function() {
        var dataYear = $(this).attr('data-year');
        var dataMonth = $(this).attr('data-month');
        var dataDate = $(this).text();
        var targetDate = new Date(dataYear, dataMonth, dataDate);

        var startDate = getPeriod(targetDate).startDate;
        var baseDate = parseInt(startDate.getDate())
        var lastDate = (baseDate + 7);
        for (; baseDate < lastDate; baseDate++) {
            var selectedDate = new Date(startDate.getFullYear(), startDate.getMonth(), baseDate);
            $(".ui-datepicker-calendar tr td[data-handler='selectDay']").each(function() {
                var dataYear = $(this).attr('data-year');
                var dataMonth = $(this).attr('data-month');
                var dataDate = $(this).text();
                var targetDate = new Date(dataYear, dataMonth, dataDate);

                if (targetDate.getFullYear() == selectedDate.getFullYear() && targetDate.getMonth() == selectedDate.getMonth()
                        && targetDate.getDate() == selectedDate.getDate()) {
                    $(this).find("a").addClass("ui-state-week-hover");
                }
            });
        }
    });

    $(".ui-datepicker-calendar tr td[data-handler='selectDay']").mouseout(function() {
        $(".ui-datepicker-calendar tr td[data-handler='selectDay'] .ui-state-week-hover").each(function() {
            $(this).removeClass("ui-state-week-hover");
        });
    });
}

onSelectWeeklyHighlight = function(startDate) {
    $(".ui-datepicker-calendar tr td[data-handler='selectDay']").each(function() {
        $(this).find("a").removeClass("ui-state-active");
        $(this).find("a").removeClass("ui-state-highlight");
        var dataYear = $(this).attr('data-year');
        var dataMonth = $(this).attr('data-month');
        var dataDate = $(this).text();

        var baseDate = parseInt(startDate.getDate());
        var lastDate = (baseDate + 7);
        for (; baseDate < lastDate; baseDate++) {
            var targetDate = new Date(dataYear, dataMonth, dataDate);
            var selectedDate = new Date(startDate.getFullYear(), startDate.getMonth(), baseDate);
            if (targetDate.getFullYear() == selectedDate.getFullYear() && targetDate.getMonth() == selectedDate.getMonth()
                    && targetDate.getDate() == selectedDate.getDate()) {
                $(this).find("a").addClass("ui-state-active");
                $(this).find("a").removeClass("ui-state-highlight");
            }
        }
    });
}

getPeriod = function(date) {
    var result = {
        startDate : "",
        endDate : ""
    };

    var todayDate = new Date();
    var selectedDate =  date || todayDate;

    /* mon : 0, tue : 1, wed : 2, thu : 3, fri : 4, sat : 5, sun : 6 */
    var adjustDay = (selectedDate.getDay() == 0) ? 6 : selectedDate.getDay() - 1;

    var firstDay = selectedDate.getDate() - adjustDay;
    var startDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), firstDay);

    var today = todayDate.getDate();
    var lastDay = selectedDate.getDate() - adjustDay + 6;

    var endDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), lastDay);

    if (dateString(endDate) > dateString(todayDate)) {
        endDate = todayDate;
    }

    result.startDate = startDate;
    result.endDate = endDate;

    return result;
}

setYears = function(id, firstOffset, lastOffset) {
    var yearHtml = "";

    var date = new Date();
    var currentYear = date.getFullYear();
    var startYear = firstOffset ? (currentYear - firstOffset) : "2017";
    var endYear = lastOffset ? (currentYear + lastOffset) : currentYear;
    for (startYear; startYear <= endYear; startYear++) {
        yearHtml += "<option value=\"" + startYear + "\">" + startYear + "</option>";
    }

    $("#" + id).html(yearHtml);
}

setMonths = function(id) {
    var monthsHtml = "";

    for (var i = 1; i <= 12; i++) {
        var month = i;
        if (i < 10) {
            month = "0" + month;
        }
        monthsHtml += "<option value=\"" + i + "\">" + month + "</option>";
    }

    $("#" + id).html(monthsHtml);
}

didChangeDateSelector = function() {
    var startDate = new Date($("#yearSelector").val(), ($("#monthSelector").val() - 1), 1);
    var endDate = new Date($("#yearSelector").val(), $("#monthSelector").val(), 0);
    $("#startDate").val(dateString(startDate));
    $("#endDate").val(dateString(endDate));
}

clickPrevDateButton = function() {
    if (dateType == "month") {
        return;
    }

    var curDate = dateString(new Date());
    var date = $("#inputDatePicker1").datepicker('getDate');
    var result = date;
    if (dateType == "day") {
        result = getPrevDay(date);
        result = dateString(result);
        $("#inputDatePicker1").datepicker().datepicker('setDate', result);
        $("#startDate").val(result);
        $("#endDate").val(result);
    }

    if (dateType == "week") {
        result = getPrevWeek(date);
        var startDate = dateString(result.startDate);
        var endDate = dateString(result.endDate);
        result = startDate + "~" + endDate;
        $("#inputDatePicker1").val(result);
        $("#startDate").val(startDate);
        $("#endDate").val(endDate);
    }
}

clickNextDateButton = function() {
    if (dateType == "month") {
        return;
    }

    var curDate = dateString(new Date());
    var shouldHiddenNextButton = false;
    var date = $("#inputDatePicker1").datepicker('getDate');
    var result = date;
    if (dateType == "day") {
        if (dateString(date) == curDate) {
            popAlertLayer(getMessage("fail.common.next.date.msg"));
            return;
        }
        result = getNextDay(date);
        result = dateString(result);

        $("#inputDatePicker1").datepicker().datepicker('setDate', result);
        $("#startDate").val(result);
        $("#endDate").val(result);
        return;
    }

    if (dateType == "week") {
        if (dateString(date) == curDate) {
            popAlertLayer(getMessage("fail.common.next.date.msg"));
            return;
        }

        result = getNextWeek(date);

        var startDate = dateString(result.startDate);
        var endDate = dateString(result.endDate);
        result = startDate + "~" + endDate;

        $("#inputDatePicker1").val(result);
        $("#startDate").val(startDate);
        $("#endDate").val(endDate);
    }

    if (shouldHiddenNextButton) {
        $("#btnNextDate").hide();
    }
}

getPrevDay = function(date) {
    var date = new Date(date);
    date.setDate(date.getDate() - 1);
    return date;
}

getNextDay = function(date) {
    var date = new Date(date);
    date.setDate(date.getDate() + 1);
    return date;
}

getPrevWeek = function(date) {
    var date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 7);
    var result = getPeriod(date);
    return result;
}

getNextWeek = function(date) {
    var date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 7);
    var result = getPeriod(date);
    return result;
}

setDateType = function(type) {
    if (!type || type == undefined || (type != "custom" && type != "day" && type != "week" && type != "month")) {
        type = "week"; // 디폴트 값 변경 "custom" ==> "week"
    }

    dateType = type;
    setFocusTab(type);
    if (type == "month") {
        initDateSelector();
    } else {
        initDatePicker(type);
    }

    updateDateLayout(type);
}

setFocusTab = function(type) {
    $(".tabPeriod > div").removeClass("tabBtnOn");
    var name = type.substring(0, 1).toUpperCase(0) + type.substring(1);
    $(".tabPeriod > div[id='btn" + name + "']").addClass("tabBtnOn");
}

initDatePicker = function(type) {
    $("#periodTxt").hide();
    $("#inputDatePicker2").hide();

    var datePickerHtml = "<input id='inputDatePicker1' type='text' class='inputBox2 lengthM txtCenter datepicker' readonly>";
    $("#inputDatePicker1").replaceWith(datePickerHtml);

    var monthNames = [
        getMessage("datePicker.mon.jan"),
        getMessage("datePicker.mon.feb"),
        getMessage("datePicker.mon.mar"),
        getMessage("datePicker.mon.apr"),
        getMessage("datePicker.mon.may"),
        getMessage("datePicker.mon.jun"),
        getMessage("datePicker.mon.jul"),
        getMessage("datePicker.mon.aug"),
        getMessage("datePicker.mon.sep"),
        getMessage("datePicker.mon.oct"),
        getMessage("datePicker.mon.nov"),
        getMessage("datePicker.mon.dec")
    ];

    var dayNames = [
        getMessage("datePicker.day.sun"),
        getMessage("datePciker.day.mon"),
        getMessage("datePicker.day.tue"),
        getMessage("datePicker.day.wed"),
        getMessage("datePicker.day.thu"),
        getMessage("datePicker.day.fri"),
        getMessage("datePicker.day.sat"),
    ];

    var options = {
            dateFormat:'yy-mm-dd',
            monthNamesShort : monthNames,
            dayNamesMin : dayNames,
            changeMonth:true,
            changeYear:true,
            showMonthAfterYear:true,
            controlType:'select',
            oneLine:true,
            minDate : new Date(2017, 0, 1),
            maxDate : 0
    };

    if (type == "day") {
        var date = dateString(new Date());
        $("#inputDatePicker1").val(date);
        options.onSelect = function(dateText, inst) {
            var date = $("#inputDatePicker1").datepicker('getDate');
            $("#startDate").val(dateString(date));
            $("#endDate").val(dateString(date));
            if (isFunction(datePickerOnSelectCallback)) {
                window[datePickerOnSelectCallback]();
            }
        };

        $('#inputDatePicker1').datepicker(options);
        $('#inputDatePicker1').removeClass("lengthM");
        $('#inputDatePicker1').addClass("lengthS");
        $("#startDate").val(date);
        $("#endDate").val(date);
    } else if (type == "week") {
        var result = getPeriod(new Date());
        $("#inputDatePicker1").val(dateString(result.startDate) + "~" + dateString(result.endDate));
        $("#startDate").val(dateString(result.startDate));
        $("#endDate").val(dateString(result.endDate));

        options.dateFormat = 'yy-mm-dd~yy-mm-dd';
        options.showOtherMonths = true;
        options.selectOtherMonths = true;
        options.selectWeek = true;
        options.onSelect = function(dateText, inst) {
            var date = $("#inputDatePicker1").datepicker('getDate');
            var result = getPeriod(date);
            $("#inputDatePicker1").val(dateString(result.startDate) + "~" + dateString(result.endDate));
            $("#startDate").val(dateString(result.startDate));
            $("#endDate").val(dateString(result.endDate));
            if (isFunction(datePickerOnSelectCallback)) {
                window[datePickerOnSelectCallback]();
            }
        };
        options.beforeShow = function(input, inst) {
            var value = $(this).val();
            if (!value) {
                return;
            }
            var values = value.split("~");
            var startDate = new Date(values[0]);
            setTimeout(function() { onSelectWeeklyHighlight(startDate) }, 0);
            setTimeout("onFocusWeeklyHighlight()", 0);
        };

        options.onChangeMonthYear = function(year, month, inst) {
            var value = $(this).val();
            if (!value) {
                return;
            }
            var values = value.split("~");
            var startDate = new Date(values[0]);
            setTimeout(function() { onSelectWeeklyHighlight(startDate) }, 0);
            setTimeout("onFocusWeeklyHighlight()", 0);
        };
        $('#inputDatePicker1').datepicker(options);
        $('#inputDatePicker1').removeClass("lengthS");
        $('#inputDatePicker1').addClass("lengthM");
    } else if (type == "custom") {
        var date = dateString(new Date());
        $("#inputDatePicker1").val(date);
        $("#inputDatePicker2").val(date);

        $('#inputDatePicker1').datepicker(options);
        $('#inputDatePicker2').datepicker(options);
        $("#inputDatePicker1").datepicker("option", "maxDate", date);
        $("#inputDatePicker2").datepicker("option", "minDate", date);

        $('#inputDatePicker1').datepicker("option", "onSelect", function(dateText, inst) {
            var startDt = $("#inputDatePicker1").datepicker('getDate');
            var endDt = $("#inputDatePicker2").datepicker('getDate');

            $("#inputDatePicker2").datepicker("option", "minDate", dateText);
            $("#startDate").val(dateString(startDt));
            $("#endDate").val(dateString(endDt));
            if (isFunction(datePickerOnSelectCallback)) {
                window[datePickerOnSelectCallback]();
            }
        });

        $('#inputDatePicker2').datepicker("option", "onSelect", function(dateText, inst) {
            var startDt = $("#inputDatePicker1").datepicker('getDate');
            var endDt = $("#inputDatePicker2").datepicker('getDate');

            $("#inputDatePicker1").datepicker("option", "maxDate", dateText);
            $("#startDate").val(dateString(startDt));
            $("#endDate").val(dateString(endDt));
            if (isFunction(datePickerOnSelectCallback)) {
                window[datePickerOnSelectCallback]();
            }
        });
        $("#startDate").val(date);
        $("#endDate").val(date);

        $('#inputDatePicker1').removeClass("lengthM");
        $('#inputDatePicker1').addClass("lengthS");
        $("#periodTxt").show();
        $("#inputDatePicker2").show();
    }
}

initDateSelector = function() {
    if (!$("#yearSelector").val()) {
        setYears("yearSelector");
    }

    if (!$("#monthSelector").val()) {
        setMonths("monthSelector");
    }

    var date = new Date();
    $("#yearSelector").val(date.getFullYear());
    $("#monthSelector").val(date.getMonth() + 1);

    updateDateSelectorLayout();
    didChangeDateSelector();
}

updateDateSelectorLayout = function() {
    var date = new Date();
    var selectedYear = $("#yearSelector").val();
    var currentlyYear = date.getFullYear();
    var lastMonth = date.getMonth() + 2;
    for (lastMonth; lastMonth <= 12; lastMonth++) {
        var label = (lastMonth < 10) ? "0" + lastMonth : lastMonth;
        if (selectedYear == currentlyYear) {
            $("#monthSelector option[value='" + lastMonth + "']").remove();
        } else {
            var optionHtml = "<option value='" + lastMonth + "'>" + label + "</option>";
            $("#monthSelector").append(optionHtml);

            $("#monthSelector option").each(function(i){
                if($("#monthSelector option[value="+$(this).val()+"]").length >= 2){
                    $("#monthSelector").children("option[value="+$(this).val()+"]").not(':first').remove();
                }
            });
        }
    }
}

restoreDateType = function(type, startDate, endDate) {
    if (!type || type == undefined || (type != "custom" && type != "day" && type != "week" && type != "month")) {
        type = "week"; // 디폴트 값 변경 "custom" ==> "week"
    }

    dateType = type;
    setFocusTab(type);
    if (type == "month") {
        resotreDateSelector(startDate);
    } else {
        restoreDatePicker(type, startDate, endDate);
    }

    updateDateLayout(type);
}

restoreDatePicker = function(type, startDate, endDate) {
    $("#periodTxt").hide();
    $("#inputDatePicker2").hide();

    var datePickerHtml = "<input id='inputDatePicker1' type='text' class='inputBox2 lengthM txtCenter datepicker' readonly>";
    $("#inputDatePicker1").replaceWith(datePickerHtml);

    var monthNames = [
        getMessage("datePicker.mon.jan"),
        getMessage("datePicker.mon.feb"),
        getMessage("datePicker.mon.mar"),
        getMessage("datePicker.mon.apr"),
        getMessage("datePicker.mon.may"),
        getMessage("datePicker.mon.jun"),
        getMessage("datePicker.mon.jul"),
        getMessage("datePicker.mon.aug"),
        getMessage("datePicker.mon.sep"),
        getMessage("datePicker.mon.oct"),
        getMessage("datePicker.mon.nov"),
        getMessage("datePicker.mon.dec")
    ];

    var dayNames = [
        getMessage("datePicker.day.sun"),
        getMessage("datePciker.day.mon"),
        getMessage("datePicker.day.tue"),
        getMessage("datePicker.day.wed"),
        getMessage("datePicker.day.thu"),
        getMessage("datePicker.day.fri"),
        getMessage("datePicker.day.sat"),
    ];

    var options = {
            dateFormat:'yy-mm-dd',
            monthNamesShort : monthNames,
            dayNamesMin : dayNames,
            changeMonth:true,
            changeYear:true,
            showMonthAfterYear:true,
            controlType:'select',
            oneLine:true,
            minDate : new Date(2017, 0, 1),
            maxDate : 0
    };

    if (type == "day") {
        $("#inputDatePicker1").val(startDate);
        options.onSelect = function(dateText, inst) {
            var date = $("#inputDatePicker1").datepicker('getDate');
            $("#startDate").val(dateString(date));
            $("#endDate").val(dateString(date));
            if (isFunction(datePickerOnSelectCallback)) {
                window[datePickerOnSelectCallback]();
            }
        };

        $('#inputDatePicker1').datepicker(options);
        $('#inputDatePicker1').removeClass("lengthM");
        $('#inputDatePicker1').addClass("lengthS");
        $("#startDate").val(startDate);
        $("#endDate").val(startDate);
    } else if (type == "week") {
        $("#inputDatePicker1").val(startDate + "~" + endDate);
        $("#startDate").val(startDate);
        $("#endDate").val(endDate);

        options.dateFormat = 'yy-mm-dd~yy-mm-dd';
        options.showOtherMonths = true;
        options.selectOtherMonths = true;
        options.selectWeek = true;
        options.onSelect = function(dateText, inst) {
            var date = $("#inputDatePicker1").datepicker('getDate');
            var result = getPeriod(date);
            $("#inputDatePicker1").val(dateString(result.startDate) + "~" + dateString(result.endDate));
            $("#startDate").val(dateString(result.startDate));
            $("#endDate").val(dateString(result.endDate));
            if (isFunction(datePickerOnSelectCallback)) {
                window[datePickerOnSelectCallback]();
            }
        };
        options.beforeShow = function(input, inst) {
            var value = $(this).val();
            if (!value) {
                return;
            }
            var values = value.split("~");
            var startDate = new Date(values[0]);
            setTimeout(function() { onSelectWeeklyHighlight(startDate) }, 0);
            setTimeout("onFocusWeeklyHighlight()", 0);
        };

        options.onChangeMonthYear = function(year, month, inst) {
            var value = $(this).val();
            if (!value) {
                return;
            }
            var values = value.split("~");
            var startDate = new Date(values[0]);
            setTimeout(function() { onSelectWeeklyHighlight(startDate) }, 0);
            setTimeout("onFocusWeeklyHighlight()", 0);
        };
        $('#inputDatePicker1').datepicker(options);
        $('#inputDatePicker1').removeClass("lengthS");
        $('#inputDatePicker1').addClass("lengthM");
    } else if (type == "custom") {
        $("#inputDatePicker1").val(startDate);
        $("#inputDatePicker2").val(endDate);

        $('#inputDatePicker1').datepicker(options);
        $('#inputDatePicker2').datepicker(options);
        $("#inputDatePicker1").datepicker("option", "maxDate", endDate);
        $("#inputDatePicker2").datepicker("option", "minDate", startDate);

        $('#inputDatePicker1').datepicker("option", "onSelect", function(dateText, inst) {
            var startDt = $("#inputDatePicker1").datepicker('getDate');
            var endDt = $("#inputDatePicker2").datepicker('getDate');

            $("#inputDatePicker2").datepicker("option", "minDate", dateText);
            $("#startDate").val(dateString(startDt));
            $("#endDate").val(dateString(endDt));
            if (isFunction(datePickerOnSelectCallback)) {
                window[datePickerOnSelectCallback]();
            }
        });

        $('#inputDatePicker2').datepicker("option", "onSelect", function(dateText, inst) {
            var startDt = $("#inputDatePicker1").datepicker('getDate');
            var endDt = $("#inputDatePicker2").datepicker('getDate');

            $("#inputDatePicker1").datepicker("option", "maxDate", dateText);
            $("#startDate").val(dateString(startDt));
            $("#endDate").val(dateString(endDt));
            if (isFunction(datePickerOnSelectCallback)) {
                window[datePickerOnSelectCallback]();
            }
        });
        $("#startDate").val(startDate);
        $("#endDate").val(endDate);

        $('#inputDatePicker1').removeClass("lengthM");
        $('#inputDatePicker1').addClass("lengthS");
        $("#periodTxt").show();
        $("#inputDatePicker2").show();
    }
}

resotreDateSelector = function(startDate) {
    if (!$("#yearSelector").val()) {
        setYears("yearSelector");
    }

    if (!$("#monthSelector").val()) {
        setMonths("monthSelector");
    }

    var dateArr = startDate.split("-");
    $("#yearSelector").val(dateArr[0]);
    $("#monthSelector").val(parseInt(dateArr[1]));

    updateDateSelectorLayout();
    didChangeDateSelector();
}

updateDateLayout = function(type) {
    var isShow = (type == "day" || type == "week") ? true : false;
    if (type == "month") {
        $("#datePicker").hide();
        $("#dateSelector").show();
    } else {
        $("#datePicker").show();
        $("#dateSelector").hide();
    }
    showDateChangeButton(isShow);
}

showDateChangeButton = function(isShow) {
    if (isShow) {
        $(".btnPeriod").show();
    } else {
        $(".btnPeriod").hide();
    }
}

setDatePickerOnSelectCallback = function(funcStr) {
    datePickerOnSelectCallback = funcStr;
}