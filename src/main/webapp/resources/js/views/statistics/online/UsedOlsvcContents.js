/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;

$(document).ready(function() {
    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateUsedOlsvcContentsList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateUsedOlsvcContentsList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateUsedOlsvcContentsList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateUsedOlsvcContentsList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateUsedOlsvcContentsList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateUsedOlsvcContentsList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateUsedOlsvcContentsList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateUsedOlsvcContentsList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#serviceList").change(function() {
        changeServiceList();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDatePickerOnSelectCallback("updateUsedOlsvcContentsList");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    getServiceList();
});

getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        if (!didFirstLayout) {
            initUsedOlsvcContentsList();
            didFirstLayout = true;
            return;
        }

        updateUsedOlsvcContentsList();
        return;
    }

    var serviceListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var svcSeq = findGetParameter(strHash, "svcSeq");
            $("#serviceList").val(svcSeq);
        } else {
            $("#serviceList option:eq(0)").prop("selected", true);
        }
    }

    if (!didFirstLayout) {
        initUsedOlsvcContentsList();
        didFirstLayout = true;
        return;
    }

    updateUsedOlsvcContentsList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
}

changeServiceList = function() {
    updateUsedOlsvcContentsList();
}

initUsedOlsvcContentsList = function() {
    var columnNames = [
        getMessage("common.contents.cp.nm"),
        getMessage("common.contents.nm"),
        getMessage("common.contents.sub.title"),
        getMessage("common.contents.total.click.count"),
        getMessage("common.contents.total.streaming.count"),
        getMessage("common.contents.total.streaming.time"),
        getMessage("common.contents.total.download.count")
    ];

    var colModel = [
        {name:"cpNm", index:"cpNm", align:"center"},
        {name:"contsTitle", index:"contsTitle", align:"center"},
        {name:"contsSubTitle", index:"contsSubTitle", align:"center"},
        {name:"totClickCnt", index:"totClickCnt", align:"center"},
        {name:"totStmCnt", index:"totStmCnt", align:"center"},
        {name:"totStmTime", index:"totStmTime", align:"center"},
        {name:"totDlCnt", index:"totDlCnt", align:"center"}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.usedOlsvcContentsList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "cpNm",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "usedOlsvcContentsList",
            svcSeq : $("#serviceList option:selected").val(),
            startDate : $("#startDate").val(),
            endDate : $("#endDate").val()
        },
        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var svcSeq = findGetParameter(strHash, "svcSeq");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");

                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);

                $("#serviceList").val(svcSeq);

                myPostData.page = page;
                myPostData.svcSeq = $("#serviceList option:selected").val();
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateUsedOlsvcContentsList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            postData: {
                type : "usedOlsvcContentsList",
                svcSeq : $("#serviceList option:selected").val(),
                startDate : $("#startDate").val(),
                endDate : $("#endDate").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "usedOlsvcContentsList",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : $("#serviceList option:selected").val(),
            "svcNm" : $("#serviceList option:selected").text(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        hashlocation += "&svcSeq=" + $("#serviceList option:selected").val();
        hashlocation += "&storSeq=" + $("#storeList option:selected").val();

        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        document.location.hash = hashlocation;
    }
}