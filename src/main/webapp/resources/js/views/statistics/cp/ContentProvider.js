/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;

$(document).ready(function() {
    $(".btnSearchWhite").click(function() {
        updateContentsByCpList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    setDateType("custom");

    initContentsByCpList();
});

initContentsByCpList = function() {
    var columnNames = [
        getMessage("common.cp.office"),
        getMessage("statistics.table.total.cnt"),
        getMessage("statistics.table.verify.cnt"),
        getMessage("statistics.table.complete.cnt"),
        getMessage("statistics.table.reject.cnt"),
        getMessage("statistics.table.display.cnt")
    ];

    var colModel = [
        {name:"cpNm", index:"cpNm", align:"center"},
        {name:"totalCnt", index:"totalCnt", align:"center"},
        {name:"verifyCnt", index:"verifyCnt", align:"center"},
        {name:"completeCnt", index:"completeCnt", align:"center"},
        {name:"rejectCnt", index:"rejectCnt", align:"center"},
        {name:"displayCnt", index:"displayCnt", align:"center"}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentsByCpList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "cpNm",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "contentsByCpList",
            startDate: $("#startDate").val(),
            endDate: $("#endDate").val()
        },

        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));;
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");

                $("#startDate").val(startDate);
                $("#endDate").val(endDate);

                restoreDateType("custom", startDate, endDate);

                myPostData.page = page;
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateContentsByCpList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            postData: {
                type : "contentsByCpList",
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var strHash = document.location.hash.replace("#", "");
    var startDate = findGetParameter(strHash, "startDate");
    var endDate = findGetParameter(strHash, "endDate");

    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "contentsByCpList",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "startDate" : startDate,
            "endDate" : endDate
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        document.location.hash = hashlocation;
    }
}