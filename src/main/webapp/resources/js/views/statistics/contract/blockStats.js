/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var readyPage = false;
var apiUrl = makeAPIUrl("/api/cpContStat");
var lastBind = false;
var gridState;
var tempState;
var hashCp;
var hashStore;
var hashSvc;
var hashTab;
var hashType;
var hashYear;
var hashMonth;
var selTab =0;
var totalPage = 0;
var isStatusDetailMode = false;
var shouldPreventReloadDetailMode = false;
var noData = false;
var stateVal;
var formatter_y = new Intl.DateTimeFormat("en", {year: "numeric" });
var formatter = new Intl.DateTimeFormat("en", {month: "numeric" });
var NowYear = formatter_y.format(new Date());
var NowMonth = formatter.format(new Date());
$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
})
$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (noData) {
            return;
        }


        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");

            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashTab = checkUndefined(findGetParameter(str_hash,"pageType"));
            hashCp = checkUndefined(findGetParameter(str_hash,"cpSeq"));
            hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashType = checkUndefined(findGetParameter(str_hash,"type"));
            hashYear = checkUndefined(findGetParameter(str_hash,"year"));
            hashMonth = checkUndefined(findGetParameter(str_hash,"month"));

            if (typeof hashType == "undefined" || hashType == null){
                hashType = "C";
            }

            if (typeof hashYear == "undefined" || hashYear == null){
                hashYear = NowYear;
                hashMonth = NowMonth;
            } else if (typeof hashMonth == "undefined" || hashMonth == null) {
                hashMonth = NowMonth;
            }

            if (hashYear == NowYear) {
                for(var i = parseInt(NowMonth)+1; i<=12; i++){
                    $("#month option[value="+i+"]").remove();
                }
            } else {

                for(var i = $("#month option").length+1; i<=12; i++){
                    $("#month").append("<option value="+i+">"+i+"</option>");
                }
            }


            $("#pageType").val(hashTab);
            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#cpList").val(hashCp);
            $("#storList").val(hashStore);
            $("#svcList").val(hashSvc);
            $("#year").val(hashYear);
            $("#month").val(hashMonth);
            $("input[name=type][value="+hashType+"]").attr("checked",true);
            jQuery("#jqgridData").trigger("reloadGrid");

        } else {
            gridState = "READY";
        }
    });
})
$(document).ready(function(){
    $("input[name=type]").bind("change",function(i){
        hashType = $(this).val();
        tabSet(true);
    });

    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashTab = checkUndefined(findGetParameter(str_hash,"pageType"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        hashCp = checkUndefined(findGetParameter(str_hash,"cpSeq"));
        hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
        hashType = checkUndefined(findGetParameter(str_hash,"type"));
        hashYear = checkUndefined(findGetParameter(str_hash,"year"));
        hashMonth = checkUndefined(findGetParameter(str_hash,"month"));

        if (typeof hashTab == "undefined"){
            hashTab = "contract";
        }

        if (hashYear == NowYear) {
            for(var i = parseInt(NowMonth)+1; i<=12; i++){
                $("#month option[value="+i+"]").remove();
            }
        }

        $("#pageType").val(hashTab);
        $("#target").val(searchField);
        $("#keyword").val(searchString);
        $("#year").val(hashYear);
        $("#month").val(hashMonth);
        $("input[name=type][value="+hashType+"]").attr("checked",true);

        if (hashTab == "" || hashTab == "contract" || hashTab == null) {
            hashTab = "contract";
            apiUrl = makeAPIUrl("/api/cpContStat");
            selTab = 0;
        } else {
            hashTab = "billing";
            apiUrl = makeAPIUrl("/api/svcContStat");
            selTab = 1;
        }

        gridState = "NONE";
    } else {
        $("#year").val(NowYear);
        $("#month").val(NowMonth);

        for(var i = parseInt(NowMonth)+1; i<=12; i++){
            $("#month option[value="+i+"]").remove();
        }
    }
    cpList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

cpList = function(){
    callByGet("/api/cp?rows=10000","cpListSuccess","NoneForm");
}

cpListSuccess = function(data){
    if (data.resultCode == "1000") {
        var cpListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.cpList).each(function(i, item) {
            cpListHtml += "<option value=\""+item.cpSeq+"\">"+item.cpNm+"</option>";
        });
        $("#cpList").html(cpListHtml);
        $("#cpList").val(hashCp);
        if ($("#cpSeq").val() != 0) {
            $("#cpList").val($("#cpSeq").val());
        }


    }
    if (!readyPage) {
        tabSet(false);
        svcList();
    }

    if ($("#mbrSe").val() != "01" && $("#mbrSe").val() != "02" ) {
        $("#cpList option").not("[value="+$("#cpSeq").val()+"]").remove();
        $("#cpList").attr('disabled', 'true');
    }
}


svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
                svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }

        if (hashSvc != "" && hashSvc != null) {
            $("#svcList > option[value=" + hashSvc + "]").attr("selected","true");
            hashSvc = "";
        }

        storList();
    } else {
        $("#svcList").html("<option value=''>"+getMessage("select.all")+"</option>");
        $("#storList").html("<option value=''>"+getMessage("select.all")+"</option>");
        contractList();
    }
}


storList = function(){
    callByGet("/api/store?searchType=list&useYn=ALL&svcSeq="+$("#svcList").val(),"storListSuccess","NoneForm");
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        checkedName = "";
        var memberStoreListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.storeNmList).each(function(i, item) {
            memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(memberStoreListHtml);
        $("#storList").attr("disabled", false);
        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 서비스 변경 불가
            $("#storList").attr('disabled', true);
        } else {
            $("#storList").attr('disabled', false);
        }



        if (hashStore != "" && hashStore != null) {
            $("#storList > option[value=" + hashStore + "]").attr("selected","true");
            hashStore = "";
        }


        if (!readyPage) {
            readyPage = true;
            tabSet(false);
        }

        contractList();
    } else {
        $("#storList").attr('disabled', true);
        if ($("#svcList").val() == "") {
            $("#storList").html("<option value=''>"+getMessage("select.all")+"</option>");

            if (!readyPage) {
                readyPage = true;
                tabSet(false);
            }

            contractList();
        } else {
            $("#storList").html("");
            contractList();
            popAlertLayer(getMessage("info.store.nodata.msg"));
        }


    }
}


function tabVal(seq,e){
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
    $(e).parent("div").removeClass("tabOff").addClass("tabOn").css({ 'pointer-events': 'none' });

    selTab = seq;

    $("#target").val("");
    $("#selectBox").val("");
    $("#cpList").val("");
    $("#svcList").val("");
    $("#storList").val("");
    $("#keyword").val("");

    totalPage = 0;
    if ($("#storList option").length == 0 && selTab == 1) {
        $("#storList").html("<option value=''>"+getMessage("select.all")+"</option>");
        $("#storList").css("disabled",true);
    } else {
        $("#storList").css("disabled",false);
    }

    $('#jqgridData').jqGrid('setGridParam', {jsonReader :""});
    jQuery("#jqgridData").jqGrid('GridUnload');
    tabSet(false);
    $("#target").html("");
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
}

function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {
        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "cpNm":break;
            case "contsTitle":break;
            case "svcNm":break;
            default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $(".target_bak").remove();
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

modelSet = function(){
    var ccm = $("#jqgridData").getGridParam();

    tabSet(false);

    $("#jqgridData").setGridWidth($("#contents").width());
    $("#jqgridData").jqGrid('setGridParam',ccm);

    if (selTab == 0) {
        $("#jqgridData").jqGrid("showCol", ["cb"]);
    } else if (selTab == 2) {
        $("#jqgridData").jqGrid("hideCol", ["cb"]);
    }

    $("#jqgridData").setGridWidth($("#contents").width());
    $("#jqgridData").jqGrid('setGridParam',ccm);
}

var option_common = {
    url:apiUrl,
    datatype: "json", // 데이터 타입 지정
    jsonReader : {
        page: "result.currentPage",
        total: "result.totalPage",
        root: "result.cpContStat",
        records: "result.totalCount",
        repeatitems: false
    },
    colNames:[getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.cpNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("contract.play.acumlate.count"),getMessage("contract.rmndCnt"),getMessage("column.title.pricePerSec"),getMessage("column.title.contsSeq"),getMessage("column.title.cpSeq")],
    colModel: [
        {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
        {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
        {name:"cpNm", index:"CP_NM", align:"center",sortable:false},
        {name:"contract", index:"CONTRACT", align:"center",sortable:false},
        {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
        {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
        {name:"totalPlayCnts", index:"TOT_PLAY_CNTS", align:"center",sortable:false},
        {name:"remainPlayCnts", index:"REMAMIN_PLAY_CNTS", align:"center",sortable:false},
        {name:"pricePerSec", index:"PRICE_PER_SEC", hidden:true,sortable:false},
        {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
        {name:"cpSeq", index:"CP_SEQ", hidden:true,sortable:false}
   ],
    sortorder: "desc",
    height: "auto",
    autowidth: true, // width 자동 지정 여부
    rowNum: 10, // 페이지에 출력될 칼럼 개수
    pager: "#pageDiv", // 페이징 할 부분 지정
    viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
    sortname: "VRF_RQT_DT",
    multiselect: false,
    beforeRequest:function(){
        tabSet(false);
        tempState = gridState;
        var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

        if (gridState == "TABVAL") {
            searchField = "";
            searchString = "";
        } else if (gridState != "SEARCH") {
            $("#target").val(searchField);
            $("#keyword").val(searchString);
        }

        if (isNaN(page)) {
           page =1;
        }

        searchField = checkUndefined($("#target").val());
        searchString = checkUndefined($("#keyword").val());
        hashCp = checkUndefined($("#cpList").val());
        hashStore = checkUndefined($("#storList").val());
        hashSvc = checkUndefined($("#svcList").val());
        hashType = checkUndefined($("input[name=type]:checked").val());
        hashYear = checkUndefined($("#year").val());
        hashMonth = checkUndefined($("#month").val());
/*
        if (hashVerify == "" || hashVerify == "verify" || hashVerify == null) {
            hashVerify = "verify";
            apiUrl = makeAPIUrl("/api/contents");
            selTab = 0;
        } else if (hashVerify == "stat") {
            apiUrl = makeAPIUrl("/api/verifyRecord");
            hashSvc = "";
            selTab = 2;
        } else {
            apiUrl = makeAPIUrl("/api/contents");
            hashSvc = "";
            selTab = 1;
        }
*/
        modelSet();

        $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
        $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

        if (gridState == "HASH"){
            myPostData.page = page;
            tempState = "READY";
        } else {
            if(tempState == "SEARCH"){
               myPostData.page = 1;
            } else {
                tempState = "";
            }
        }

        if(gridState == "NONE"){
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            myPostData.page = page;
            if (searchField != null) {
                tempState = "SEARCH";
            } else {
                tempState = "";
            }
        }

        if(searchField != null && searchField != "" && searchString != ""){
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
        }else{
            delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
        }

        gridState = "GRIDBEGIN";
        if (hashCp != null && hashCp != "") {
            myPostData.cpSeq = hashCp;
        } else {
            myPostData.cpSeq = "";
        }
        if (hashSvc != null && hashSvc != "") {
            myPostData.svcSeq = hashSvc;
        } else {
            myPostData.svcSeq = "";
        }

        if (hashStore != null && hashStore != "") {
            myPostData.storSeq = hashStore;
        } else {
            myPostData.storSeq = "";
        }
        if (hashMonth  != null && hashMonth  != "") {
            myPostData.month = hashMonth ;
        } else {
            myPostData.month = NowMonth;
        }

        if (hashYear != null && hashYear != "") {
            myPostData.year = hashYear;
        } else {
            myPostData.year = NowYear;
        }


        if (hashType != null && hashType != "") {
            myPostData.type = hashType;
        } else {
            myPostData.type = "";
        }

        switch(selTab) {
        case 0:
            if ($("#cpList option").length == 0){
                noData = true;
            } else {
                noData = false;
            }
            break;
        case 1:
            if ($("#storList option").length == 0){
                noData = true;
            } else {
                noData = false;
            }
            break;
        }

        if (noData) {
            myPostData.page = 0;
            hashCp = "";
            switch(selTab) {
            case 0:
                popAlertLayer(getMessage("info.cp.nodata.msg"));
                break;
            case 1:

                break;
            }
        }

        $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page });
    },
    loadComplete: function (data) {
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));
        var pageMove = false;

        //session
        if(sessionStorage.getItem("last-url") != null){
            sessionStorage.removeItem('state');
            sessionStorage.removeItem('last-url');
        }

        //Search 필드 등록
        searchFieldSet();

        if (data.resultCode == 1000) {
            totalPage = data.result.totalPage;
            if (totalPage < page) {
                page = 1;
                pageMove = true;
            }

            if (page != data.result.currentPage) {
                tempState = "";
            }
        } else {
            tempState = "";
            if (page != 1) {
                page = 1;
                pageMove = true;
            }
        }

        if ((isNaN(page) || noData) && tempState != "") {
            page =1;
            pageMove = true;
        }


        /*뒤로가기*/
        var hashlocation;

        if (!pageMove) {
            page = $(this).context.p.page;
        }

        hashlocation = "page="+page+"&pageType="+hashTab;

        if (hashTab == "billing") {
            hashlocation += "&svcSeq="+hashSvc + "&storSeq="+hashStore;
        } else {
            hashlocation += "&cpSeq="+hashCp;
        }
        hashlocation += "&type="+hashType;
        hashlocation += "&year="+hashYear;
        hashlocation += "&month="+hashMonth;


        if($(this).context.p.postData._search && $("#target").val() != null){
            hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
        }

        gridState = "GRIDCOMPLETE";

        document.location.hash = hashlocation;


        if ( (tempState != ""  && tempState != "SEARCH") || str_hash == hashlocation) {
            gridState = "READY";
        }

        if (pageMove) {
            gridState = "HASH";
        }

        $(this).find("td").each(function(){
            var rowId = $(this).parents("tr").attr("id");
            var list = $("#jqgridData").jqGrid('getRowData', rowId);
            var colModelNm = jQuery("#jqgridData").jqGrid ('getGridParam', 'colModel')[$(this).index()].name;
            if (typeof rowId == "undefined") {
                return;
            }
            var typeVal = $("input[name=type]:checked").val();

            switch (colModelNm){
            case "contract" :
                switch(typeVal) {
                case "N":
                    break;
                case "L":
                    var str = $(this).text();
                    str = str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
                    if (str != "") {
                        str += " " + getMessage("common.price.unit");
                    }
                    $(this).text(str);
                    break;
                case "T":
                    break;
                case "C":
                    break;
                }
                break;
            case "price" :
                var str = $(this).text();
                str = str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
                if (str != "") {
                    str += " " + getMessage("common.price.unit");
                }
                $(this).text(str);
                break;
            case "playTime" :
                var str ="";
                var second = parseInt($(this).text());
                var hour = Math.floor(second/3600);
                var minute = Math.floor((second%3600)/60);
                var sec = Math.floor(second%60);
                if (hour != 0) {
                    str += hour + "h";
                }

                if (minute != 0) {
                    str += minute + "m";
                }

                if (sec != 0) {
                    str += sec + "s";
                }


                $(this).text(str);
                break;
            break;

            default:
                break;
            }

//            $(this).index()   $(this).text()  $(this).text(str);
        })

      //pageMove Max
        $('.ui-pg-input').on('keyup', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
        });

        $(this).find(".pointer").parent("td").addClass("pointer");
    },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
    onCellSelect: function(rowId, columnId, cellValue, event){

    }
}

tabSet = function(reload){
    switch(selTab){
    case 0:
        option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.cpContStat",
            records: "result.totalCount",
            repeatitems: false
        };
        switch(hashType) {
        case 'C' :
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.cpNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("contract.play.acumlate.count"),getMessage("contract.rmndCnt"),getMessage("column.title.pricePerSec"),getMessage("column.title.contsSeq"),getMessage("column.title.cpSeq")];
            option_common['colModel'] = [
                {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
                {name:"cpNm", index:"CP_NM", align:"center",sortable:false},
                {name:"contract", index:"CONTRACT", align:"center",sortable:false},
                {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
                {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
                {name:"totalPlayCnts", index:"TOT_PLAY_CNTS", align:"center",sortable:false},
                {name:"remainPlayCnts", index:"REMAMIN_PLAY_CNTS", align:"center",sortable:false},
                {name:"pricePerSec", index:"PRICE_PER_SEC", hidden:true,sortable:false},
                {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
                {name:"cpSeq", index:"CP_SEQ", hidden:true,sortable:false}
            ];
            break;
        case 'T' :
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.cpNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("product.table.use.price"),getMessage("contract.play.acumlate.countMonth"),getMessage("column.title.contsSeq"),getMessage("column.title.cpSeq")];
            option_common['colModel'] = [
                {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
                {name:"cpNm", index:"CP_NM", align:"center",sortable:false},
                {name:"contract", index:"CONTRACT", align:"center",sortable:false},
                {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
                {name:"price", index:"PRICE", align:"center",sortable:false},
                {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
                {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
                {name:"cpSeq", index:"CP_SEQ", hidden:true,sortable:false}
            ];
            break;
        case 'L' :
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.cpNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("column.title.contsSeq"),getMessage("column.title.cpSeq")];
            option_common['colModel'] = [
                {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
                {name:"cpNm", index:"CP_NM", align:"center",sortable:false},
                {name:"contract", index:"CONTRACT", align:"center",sortable:false},
                {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
                {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
                {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
                {name:"cpSeq", index:"CP_SEQ", hidden:true,sortable:false}
            ];
            break;
        case 'N' :
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.cpNm"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("column.title.contsSeq"),getMessage("column.title.cpSeq")];
            option_common['colModel'] = [
                {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
                {name:"cpNm", index:"CP_NM", align:"center",sortable:false},
                {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
                {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
                {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
                {name:"cpSeq", index:"CP_SEQ", hidden:true,sortable:false}
            ];
            break;
        default:
            option_common['colModel'] = [{name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},{name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},{name:"cpNm", index:"CP_NM", align:"center",sortable:false},{name:"contract", index:"CONTRACT", align:"center",sortable:false},{name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},{name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},{name:"totalPlayCnts", index:"TOT_PLAY_CNTS", align:"center",sortable:false},{name:"remainPlayCnts", index:"REMAMIN_PLAY_CNTS", align:"center",sortable:false},{name:"pricePerSec", index:"PRICE_PER_SEC", hidden:true,sortable:false},{name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},{name:"cpSeq", index:"CP_SEQ", hidden:true,sortable:false}];
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.cpNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("contract.play.acumlate.count"),getMessage("contract.rmndCnt"),getMessage("column.title.pricePerSec"),getMessage("column.title.contsSeq"),getMessage("column.title.cpSeq")];
        }


        $(".tab").eq(0).show();
        $(".tab").eq(1).hide();
        $(".rsvSelect#cp").show();
        $(".rsvSelect:not(#cp)").hide();
        hashTab = "contract";
        apiUrl = makeAPIUrl("/api/cpContStat");
        option_common['url'] = makeAPIUrl("/api/cpContStat");
        break;
    case 1:
        $(".rsvSelect#cp").hide();
        $(".rsvSelect:not(#cp)").show();
        option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.svcContStat",
            records: "result.totalCount",
            repeatitems: false
        };

        switch(hashType) {
        case 'C' :
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("contract.play.acumlate.count"),getMessage("contract.rmndCnt"),getMessage("column.title.contsSeq"),getMessage("column.title.svcSeq"),getMessage("column.title.svcNm"),getMessage("column.title.storSeq"),getMessage("column.title.storNm")];
            option_common['colModel'] = [
                {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
                {name:"contract", index:"CONTRACT", align:"center",sortable:false},
                {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
                {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
                {name:"totalPlayCnts", index:"TOT_PLAY_CNTS", align:"center",sortable:false},
                {name:"remainPlayCnts", index:"REMAMIN_PLAY_CNTS", align:"center",sortable:false},
                {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
                {name:"svcSeq", index:"SVC_SEQ", hidden:true,sortable:false},
                {name:"svcNm", index:"SVC_NM", hidden:true,sortable:false},
                {name:"storSeq", index:"STOR_SEQ", hidden:true,sortable:false},
                {name:"storNm", index:"STOR_NM", hidden:true,sortable:false}
            ];
            break;
        case 'T' :
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("product.table.use.price"),getMessage("contract.play.acumlate.countMonth"),getMessage("column.title.pricePerSec"),getMessage("column.title.contsSeq"),getMessage("column.title.svcSeq"),getMessage("column.title.svcNm"),getMessage("column.title.storSeq"),getMessage("column.title.storNm")];
            option_common['colModel'] = [
                {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
                {name:"contract", index:"CONTRACT", align:"center",sortable:false},
                {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
                {name:"price", index:"PRICE", align:"center",sortable:false},
                {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
                {name:"pricePerSec", index:"PRICE_PER_SEC", hidden:true,sortable:false},
                {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
                {name:"svcSeq", index:"SVC_SEQ", hidden:true,sortable:false},
                {name:"svcNm", index:"SVC_NM", hidden:true,sortable:false},
                {name:"storSeq", index:"STOR_SEQ", hidden:true,sortable:false},
                {name:"storNm", index:"STOR_NM", hidden:true,sortable:false}
            ];
            break;
        case 'L' :
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("column.title.contsSeq"),getMessage("column.title.svcSeq"),getMessage("column.title.svcNm"),getMessage("column.title.storSeq"),getMessage("column.title.storNm")];
            option_common['colModel'] = [
                {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
                {name:"contract", index:"CONTRACT", align:"center",sortable:false},
                {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
                {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
                {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
                {name:"svcSeq", index:"SVC_SEQ", hidden:true,sortable:false},
                {name:"svcNm", index:"SVC_NM", hidden:true,sortable:false},
                {name:"storSeq", index:"STOR_SEQ", hidden:true,sortable:false},
                {name:"storNm", index:"STOR_NM", hidden:true,sortable:false}
            ];
            break;
        case 'N' :
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("column.title.contsSeq"),getMessage("column.title.svcSeq"),getMessage("column.title.svcNm"),getMessage("column.title.storSeq"),getMessage("column.title.storNm")];
            option_common['colModel'] = [
                {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},
                {name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},
                {name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},
                {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},
                {name:"svcSeq", index:"SVC_SEQ", hidden:true,sortable:false},
                {name:"svcNm", index:"SVC_NM", hidden:true,sortable:false},
                {name:"storSeq", index:"STOR_SEQ", hidden:true,sortable:false},
                {name:"storNm", index:"STOR_NM", hidden:true,sortable:false}
            ];
            break;
        default:
            option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("contents.contract.context"),getMessage("contract.play.acumlate.time"),getMessage("contract.play.acumlate.countMonth"),getMessage("contract.play.acumlate.count"),getMessage("contract.rmndCnt"),getMessage("column.title.contsSeq"),getMessage("column.title.svcSeq"),getMessage("column.title.svcNm"),getMessage("column.title.storSeq"),getMessage("column.title.storNm")];
            option_common['colModel'] = [{name:"num", index: "num", align:"center", width:25,hidden:true,sortable:false},{name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:false},{name:"contract", index:"CONTRACT", align:"center",sortable:false},{name:"playTime", index:"PLAY_TIME", align:"center",sortable:false},{name:"playCnts", index:"PLAY_CNTS", align:"center",sortable:false},{name:"totalPlayCnts", index:"TOT_PLAY_CNTS", align:"center",sortable:false},{name:"remainPlayCnts", index:"REMAMIN_PLAY_CNTS", align:"center",sortable:false},{name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:false},{name:"svcSeq", index:"SVC_SEQ", hidden:true,sortable:false},{name:"svcNm", index:"SVC_NM", hidden:true,sortable:false},{name:"storSeq", index:"STOR_SEQ", hidden:true,sortable:false},{name:"storNm", index:"STOR_NM", hidden:true,sortable:false}];
            break;
        }

        $(".tab").eq(0).hide();
        $(".tab").eq(1).show();
        hashTab = "billing";
        apiUrl = makeAPIUrl("/api/svcContStat");
        option_common['url'] = makeAPIUrl("/api/svcContStat");
        break;
    }

    option_common['rowNum'] = $("#limit").val();

    if (reload) {
        jQuery("#jqgridData").jqGrid('GridUnload');
        $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    }

}

contractList = function(){
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}


deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

changedYear = function(e) {
    if ($(e).val() == NowYear) {
        for(var i = parseInt(NowMonth)+1; i<=12; i++){
            $("#month option[value="+i+"]").remove();
        }
    } else {
        for(var i = $("#month option").length+1; i<=12; i++){
            $("#month").append("<option value="+i+">"+i+"</option>");
        }
    }

    contractList();
}