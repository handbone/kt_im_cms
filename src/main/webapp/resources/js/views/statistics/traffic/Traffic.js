/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;

$(document).ready(function() {
    $(".btnSearchWhite").click(function() {
        updateDisplayWebTrafficList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    setDateType("custom");

    initDisplayWebTrafficList();
});

initDisplayWebTrafficList = function() {
    var columnNames = [
        getMessage("table.date"),
        getMessage("common.web"),
        getMessage("common.mobile"),
        getMessage("common.web"),
        getMessage("common.mobile"),
    ];

    var colModel = [
        {name:"connectDt", index:"connectDt", align:"center"},
        {name:"webPvCnt", index:"webPvCnt", align:"center"},
        {name:"mobPvCnt", index:"mobPvCnt", align:"center"},
        {name:"webUvCnt", index:"webUvCnt", align:"center"},
        {name:"mobUvCnt", index:"mobUvCnt", align:"center"}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.displayWebTrafficOverviewList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "connectDt",
        sortorder: "desc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "displayWebTrafficOverview",
            startDate: $("#startDate").val(),
            endDate: $("#endDate").val()
        },
        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));;
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");

                $("#startDate").val(startDate);
                $("#endDate").val(endDate);

                restoreDateType("custom", startDate, endDate);

                myPostData.page = page;
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            updateHeaderTitle(data);
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            updateHeaderTitle(null);
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
    $("#jqgridData").jqGrid('setGroupHeaders', {
        useColSpanStyle: true, 
        groupHeaders:[
          {startColumnName: 'webPvCnt', numberOfColumns: 2, titleText: '<em>' + getMessage("common.pv") + '</em>'},
          {startColumnName: 'webUvCnt', numberOfColumns: 2, titleText: '<em>' + getMessage("common.uv") + '</em>'},
        ]
    });

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateDisplayWebTrafficList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            postData: {
                type : "displayWebTrafficOverview",
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

updateHeaderTitle = function(data) {
    var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
    var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

    var webPvTotalCnt = (data == undefined || data.result == undefined || data.result.webPvTotalCnt == undefined) ? 0 : data.result.webPvTotalCnt;
    var mobPvTotalCnt = (data == undefined || data.result == undefined || data.result.mobPvTotalCnt == undefined) ? 0 : data.result.mobPvTotalCnt;
    var webUvTotalCnt = (data == undefined || data.result == undefined || data.result.webUvTotalCnt == undefined) ? 0 : data.result.webUvTotalCnt;
    var mobUvTotalCnt = (data == undefined || data.result == undefined || data.result.mobUvTotalCnt == undefined) ? 0 : data.result.mobUvTotalCnt;
    for (var i = 1; i < (colModel.length); i++) {
        var name = colModel[i].name;
        var titleId = "#jqgh_jqgridData_" + name;
        var title = colNames[i];
        if (name == "webPvCnt") {
            title += "(" + webPvTotalCnt + ")";
        } else if (name == "mobPvCnt") {
            title += "(" + mobPvTotalCnt + ")";
        } else if (name == "webUvCnt") {
            title += "(" + webUvTotalCnt + ")";
        } else if (name == "mobUvCnt") {
            title += "(" + mobUvTotalCnt + ")";
        }
        $(titleId).text(title);
    }

    $(".ui-th-column-header").each(function(i, item) {
        if ($(item).text().indexOf("PV") != -1) {
            $(item).text(getMessage("common.pv") + "(" + (webPvTotalCnt + mobPvTotalCnt) + ")");
        } else if ($(item).text().indexOf("UV") != -1) {
            $(item).text(getMessage("common.uv") + "(" + (webUvTotalCnt + mobUvTotalCnt) + ")");
        }
    });
}

downloadExcel = function() {
    var strHash = document.location.hash.replace("#", "");
    var startDate = findGetParameter(strHash, "startDate");
    var endDate = findGetParameter(strHash, "endDate");

    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "displayWebTrafficOverview",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "startDate" : startDate,
            "endDate" : endDate
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        document.location.hash = hashlocation;
    }
}