/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;
var colsTotalWidth = 0;
var funcall = false;
$(window).resize(function(){
    var colsTotalWidth2 = 0;
    var columns = $('#jqgridData').jqGrid('getGridParam', 'colModel');
    if (typeof columns != "undefined") {
        for (var i = 0; columns[i]; i++) {
            colsTotalWidth2 += columns[i].width;
        }

        if (colsTotalWidth2 >= $("#gridArea").width()) {
            colsTotalWidth2 = $("#gridArea").width();
        }

        $('#jqgridData').setGridWidth(colsTotalWidth2,false);
    }
})

$(document).ready(function() {
    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateTagStoreList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateTagStoreList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateTagStoreList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateTagStoreList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateTagStoreList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateTagStoreList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateTagStoreList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateTagStoreList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#btnTag").click(function(){
        pageMove("/statistics/store/tag");
    });

    $("#btnContentsByTag").click(function(){
        pageMove("/statistics/store/contentsByTag");
    });

    $("#serviceList").change(function() {
        changeServiceList();
    });

    $("#storeList").change(function() {
        changeStoreList();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDatePickerOnSelectCallback("updateTagStoreList");

    setDateType();

    $(window).on('hashchange', function () {
        if(funcall) {
            funcall = false;
        } else {
            shouldPreventHashChangeEvent = false;
        }
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;

        unReloadGridInit();
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }
    storInfo();

    listBack($(".btnBack"));
});

storInfo = function(storSeq){
    callByGet("/api/store?storSeq="+$("#storSeq").val(),"storInfoSuccess","NoneForm");
}

storInfoSuccess = function(data){
    if (data.resultCode == "1000") {
        var item = data.result.storeInfo;

        $("#svcSeq").val(item.svcSeq);

        getServiceList();
    }
}


getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        $("#storeList").html("");
        return;
    }

    var serviceListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);
    $("#serviceList").val($("#svcSeq").val());
    $("#serviceList").attr("disabled", true);



    getStoreList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
    $("#storeList").html("");
}

getStoreList = function() {
    var svcSeq = ($("#serviceList option:selected").val() == 0) ? "" : $("#serviceList option:selected").val();
    formData("NoneForm" , "comnCdValue", svcSeq);
    callByGet("/api/codeInfo?comnCdCtg=STORE", "didReceiveStoreList", "NoneForm", "didNotReceiveStoreList");
    formDataDeleteAll("NoneForm");
}

didReceiveStoreList = function(data) {
    var storeListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $("#storeList").attr("disabled", true);
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.nostore.msg");
        }
        popAlertLayer(msg);
        $("#storeList").html(storeListHtml);
        $("#storeList").val($("#storSeq").val());


        if (!didFirstLayout) {
            initTagStoreList();
            didFirstLayout = true;
            return;
        }

        updateTagStoreList();
        return;
    }

    $(data.result.codeList).each(function(i, item) {
        storeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#storeList").html(storeListHtml);

    $("#storeList").val($("#storSeq").val());

    if (!didFirstLayout) {
        initTagStoreList();
        didFirstLayout = true;
        return;
    }

    updateTagStoreList();
}

didNotReceiveStoreList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#storeList").html("<option value='0'>"+getMessage("select.all")+"</option>");
}

changeServiceList = function() {
    getStoreList();
}

changeStoreList = function() {
    updateTagStoreList();
}
var setCol = false;
var temp;
initTagStoreList = function() {
    var columnNames = [""];
    var beforeDateType = dateType;

    var colModel = [
        {name:"useDt", index:"useDt", align:"center",frozen:true, width:150, resizable:false,fixed: true},
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.visitorDetailList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        shrinkToFit:true,
        autowidth:true,
        scrollrows : true,
        forceFit:true,
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "useDt",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "tagProdList",
            dateType : dateType,
            svcSeq : $("#serviceList option:selected").val(),
            storSeq : $("#storeList option:selected").val(),
            startDate : $("#startDate").val(),
            endDate : $("#endDate").val()
        },
        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");

                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);


                myPostData.page = page;
                myPostData.dateType = dateType;
                myPostData.svcSeq = $("#serviceList option:selected").val();
                myPostData.storSeq = $("#storeList option:selected").val();
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;

                if(beforeDateType != dateType){
                    if (dateType == "day") {
                        $("#jqgridData").jqGrid('setLabel', "useDt","");
                    } else {
                        $("#jqgridData").jqGrid('setLabel', "useDt","");
                    }
                }
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        beforeProcessing : function(data) {
        },
        loadComplete: function(data) {
            /* 정렬 하는 컬럼 Header 만  마우스 커서 pointer S*/
            var grids = $("#jqgridData");
            var cm = grids[0].p.colModel;
            $.each(grids[0].grid.headers, function(index, value) {
                var cmi = cm[index], colName = cmi.name;
                if(!cmi.sortable) {
                    $('div.ui-jqgrid-sortable',value.el).css({cursor:"default"});
                }
            });
            /* 정렬 하는 컬럼 Header 만  마우스 커서 pointer E*/
            var deteCol = getMessage("table.date");
            if (data.resultCode == "1010") {
                popAlertLayer(getMessage("common.nodata.msg"));
                setCol = false;
                temp = null;

                var columns = $('#jqgridData').jqGrid('getGridParam', 'colModel');


                if (dateType == "day") {
                    deteCol = getMessage("common.time");
                }

                $(".frozen-bdiv").css({height:"auto",background: "white"});
                if ($(".tableLineRightTop").length == 0) {
                    $("#jqgh_jqgridData_useDt").addClass("tb_diag");
                    $("#jqgh_jqgridData_useDt").append("<p class='tableLineRightTop'>"+getMessage("statistics.table.prod.section")+"</p><p class='tableLineLftBtm'>"+deteCol+"</p>");
                    $(".frozen-div #jqgridData_useDt").append("<p class='tableLineRightTop'>"+getMessage("statistics.table.prod.section")+"</p><p class='tableLineLftBtm'>"+deteCol+"</p>");
                    $(".frozen-div.ui-state-default.ui-jqgrid-hdiv").css({height:"56px","overflow-y": "hidden"});
                    $(".frozen-div.ui-state-default.ui-jqgrid-hdiv #jqgridData_useDt").css({height:"56px"});

                }
                didCompleteLoad(this);
                return;
            } else {
                var beforeDateType = dateType;
                // Model 셋팅
                var rowdata = {product_1 : "11"};

                didCompleteLoad(this);

                colsTotalWidth = 0;
                var columns = $('#jqgridData').jqGrid('getGridParam', 'colModel');

                $(".frozen-bdiv").css({height:"auto",background: "white"});
                if ($(".tableLineRightTop").length == 0) {
                    $("#jqgh_jqgridData_useDt").addClass("tb_diag");
                    $("#jqgh_jqgridData_useDt").append("<p class='tableLineRightTop'>"+getMessage("statistics.table.prod.section")+"</p><p class='tableLineLftBtm'>"+deteCol+"</p>");
                    $(".frozen-div #jqgridData_useDt").append("<p class='tableLineRightTop'>"+getMessage("statistics.table.prod.section")+"</p><p class='tableLineLftBtm'>"+deteCol+"</p>");
                    $(".frozen-div.ui-state-default.ui-jqgrid-hdiv").css({height:"56px","overflow-y": "hidden"});
                    $(".frozen-div.ui-state-default.ui-jqgrid-hdiv #jqgridData_useDt").css({height:"56px"});

                }


                var frezn = true;
                var rfrezn = false;
                if (typeof columns != "undefined") {
                    for (var i = 0; columns[i]; i++) {
                        colsTotalWidth += columns[i].width;
                    }

                    if (colsTotalWidth >= $("#gridArea").width()) {
                        colsTotalWidth = $("#gridArea").width()-17;
                        frezn = false;
                        rfrezn = true;
                    } else {
                        $(window).trigger('resize');
                    }

                }
                $('#jqgridData').setGridWidth(colsTotalWidth,false);
                fixPositionsOfFrozenDivs.call(this);


                if (!setCol) {
                    setCol = true;

                    var colModels = [
                        {name:"useDt", index:"useDt", align:"center",frozen:rfrezn, width:150, resizable:false,fixed: true},
                    ];
                    var columnNames = [""];


                    // 상품정보 셋팅 [컬럼]
                    $(data.result.prodList).each(function(i,item){
                        columnNames.push(item.prodNm);
                        colModels.push({name:"product_"+i, index:"useDt", align:"center", width:150, resizable:true,sortable:false,fixed: true});
                    });

                    var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                    myPostData.type = "tagStroeList";
                    $(this).jqGrid("setGridParam",{colModel : colModels, colNames : columnNames,postData : myPostData,shrinkToFit:false,autowidth:false});

                    temp = $("#jqgridData").jqGrid('getGridParam');
                    unReloadGrid();
                } else if ($("#jqgridData").jqGrid('getGridParam').postData.type == "tagStroeList"){
                    $(data.result.dateList).each(function(i,item){
                        var rowdata = {};
                        rowdata['useDt'] = item.date;

                        $(data.result.prodList).each(function(j,jtem){
                            $(data.result.tagList).each(function(z,ztem) {
                                if (jtem.prodNm == ztem.prodNm && parseInt(jtem.prodSeq) == ztem.prodSeq && ztem.useDt == item.date) {
                                    rowdata['product_'+j] = ztem.tagCnt;
                                    return true;
                                }
                            });
                        });


                        $("#jqgridData").jqGrid("addRowData", data.result.totalCount - i, rowdata, 'last');

                    });
                    var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                    if (myPostData.page <= 1) {
                        var rowdata = {};
                        rowdata['useDt'] = getMessage("common.totCnt");


                        $(data.result.prodList).each(function(j,jtem){
                            rowdata['product_'+j] = jtem.totCnt;
                        });
                        $("#jqgridData").jqGrid("addRowData", data.result.totalCount+1, rowdata, 'first');
                    }


                }
            }
            $("#jqgridData").jqGrid("setFrozenColumns");
            $(".frozen-div.ui-state-default.ui-jqgrid-hdiv").css({height:"56px","overflow-y": "hidden"});
            $(".frozen-div.ui-state-default.ui-jqgrid-hdiv #jqgridData_useDt").css({height:"56px"});
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateTagStoreList = function() {
    hasChangedHash = false;
    unReloadGridInit();
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "tagStroeList",
            "dateType" : dateType,
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : $("#serviceList option:selected").val(),
            "storSeq" : $("#storeList option:selected").val(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    funcall = true;
    if (hasChangedHash) {
        hasChangedHash = false;
        funcall = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        hashlocation += "&svcSeq=" + $("#serviceList option:selected").val();
        hashlocation += "&storSeq=" + $("#storeList option:selected").val();

        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        if (document.location.hash == "#" + hashlocation) {
            funcall = false;
        }

        document.location.hash = hashlocation;
    }
}

fixPositionsOfFrozenDivs = function () {
    if (typeof this.grid.fbDiv !== "undefined") {
        $(this.grid.fbDiv).css($(this.grid.bDiv).position());
    }
    if (typeof this.grid.fhDiv !== "undefined") {
        $(this.grid.fhDiv).css($(this.grid.hDiv).position());
    }
};

unReloadGrid = function(){
    $("#jqgridData").jqGrid('GridUnload');
    $("#jqgridData").jqGrid(temp);
}

unReloadGridInit = function(){
    setCol = false;
    temp = null;

    $("#jqgridData").jqGrid('GridUnload');
    initTagStoreList();
}