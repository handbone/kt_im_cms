/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;

$(document).ready(function() {
    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateVisitorList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateVisitorList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateVisitorList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateVisitorList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateVisitorList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateVisitorList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateVisitorList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateVisitorList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#btnTag").click(function(){
        pageMove("/statistics/store/tag");
    });

    $("#btnContentsByTag").click(function(){
        pageMove("/statistics/store/contentsByTag");
    });

    $("#serviceList").change(function() {
        changeServiceList();
    });

    $("#storeList").change(function() {
        changeStoreList();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDatePickerOnSelectCallback("updateVisitorList");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    getServiceList();
});


getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        $("#storeList").html("");
        return;
    }

    var serviceListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var svcSeq = findGetParameter(strHash, "svcSeq");
            $("#serviceList").val(svcSeq);
        } else {
            $("#serviceList option:eq(0)").prop("selected", true);
        }
    }

    getStoreList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
    $("#storeList").html("");
}

getStoreList = function() {
    var svcSeq = ($("#serviceList option:selected").val() == 0) ? "" : $("#serviceList option:selected").val();
    formData("NoneForm" , "comnCdValue", svcSeq);
    callByGet("/api/codeInfo?comnCdCtg=STORE", "didReceiveStoreList", "NoneForm", "didNotReceiveStoreList");
    formDataDeleteAll("NoneForm");
}

didReceiveStoreList = function(data) {
    var storeListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";

    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.nostore.msg");
        }
        popAlertLayer(msg);
        $("#storeList").html(storeListHtml);

        if (!didFirstLayout) {
            initVisitorList();
            didFirstLayout = true;
            return;
        }

        updateVisitorList();
        return;
    }

    $(data.result.codeList).each(function(i, item) {
        storeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#storeList").html(storeListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#storSeq").val() != 0) {
        $("#storeList option").not("[value='"+ $("#storSeq").val() + "']").remove();
        $("#storeList option[value='"+ $("#storSeq").val() + "']").prop("selected", true);
        $("#storeList").attr("disabled", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var storSeq = findGetParameter(strHash, "storSeq");
            $("#storeList").val(storSeq);
        } else {
            $("#storeList option:eq(0)").prop("selected", true);
        }
    }

    if (!didFirstLayout) {
        initVisitorList();
        didFirstLayout = true;
        return;
    }

    updateVisitorList();
}

didNotReceiveStoreList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#storeList").html("<option value='0'>"+getMessage("select.all")+"</option>");
}

changeServiceList = function() {
    getStoreList();
}

changeStoreList = function() {
    updateVisitorList();
}

initVisitorList = function() {
    var columnNames = [
        getMessage("common.service"),
        getMessage("common.store"),
        getMessage("dashboard.store.totalVisit"),
        getMessage("dashboard.store.leave"),
        getMessage("dashboard.store.entering"),
        getMessage("column.title.storSeq")
    ];

    var colModel = [
        {name:"svcNm", index:"svcNm", align:"center"},
        {name:"storNm", index:"storNm", align:"center",classes:"pointer",formatter:pointercursor},
        {name:"visitedTotalCnt", index:"visitedTotalCnt", align:"center"},
        {name:"leftCnt", index:"leftCnt", align:"center"},
        {name:"enteringCnt", index:"enteringCnt", align:"center"},
        {name:"storSeq", index: "storSeq", align:"center",hidden:true,sortable:false}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.visitorList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "svcNm",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "visitorList",
            svcSeq : $("#serviceList option:selected").val(),
            storSeq : $("#storeList option:selected").val(),
            startDate : $("#startDate").val(),
            endDate : $("#endDate").val()
        },
        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var svcSeq = findGetParameter(strHash, "svcSeq");
                var storSeq = findGetParameter(strHash, "storSeq");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");

                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);

                $("#serviceList").val(svcSeq);
                $("#storeList").val(storSeq);

                myPostData.page = page;
                myPostData.svcSeq = $("#serviceList option:selected").val();
                myPostData.storSeq = $("#storeList option:selected").val();
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        onCellSelect: function(rowId, columnId, cellValue, event){
            // 콘텐츠 ID 선택시
            if(columnId == 1){
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");

                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                var hashlocation = "";

                hashlocation += "dateType=" + dateType;
                var startDate = $("#startDate").val();
                if (startDate) {
                    hashlocation += "&startDate=" + startDate;
                }
                var endDate = $("#endDate").val();
                if (endDate) {
                    hashlocation += "&endDate=" + endDate;
                }


                pageMove("/statistics/store/visitor/"+list.storSeq+"#"+hashlocation);

            }
         }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateVisitorList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            postData: {
                type : "visitorList",
                svcSeq : $("#serviceList option:selected").val(),
                storSeq : $("#storeList option:selected").val(),
                startDate : $("#startDate").val(),
                endDate : $("#endDate").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "visitorList",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : $("#serviceList option:selected").val(),
            "storSeq" : $("#storeList option:selected").val(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        hashlocation += "&svcSeq=" + $("#serviceList option:selected").val();
        hashlocation += "&storSeq=" + $("#storeList option:selected").val();

        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        document.location.hash = hashlocation;
    }
}