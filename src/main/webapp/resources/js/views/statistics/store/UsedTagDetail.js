/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;
var didFirstLayoutPopup = false;

$(document).ready(function() {
    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateTagDetailList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateTagDetailList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateTagDetailList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateTagDetailList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateTagDetailList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateTagDetailList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateTagDetailList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateTagDetailList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $(".btnClose").click(function() {
        closeUsedHistoryForTagListPopup();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDatePickerOnSelectCallback("updateTagDetailList");

    if ($("#dateType").val() && $("#startDate").val() && $("#endDate").val()) {
        restoreDateType($("#dateType").val(), $("#startDate").val(), $("#endDate").val())
    } else {
        setDateType();
    }

    $(window).on('hashchange', function () {
        closeUsedHistoryForTagListPopup();

        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    var options = {
        "obj" : $(".btnReset"),
        "defaultUri" : "/statistics/store/tag"
    }
    setListButton(options);

    initTagDetailList();

    // 팝업 위로 이용권 종류 타이틀이 보이는 문제 수정
    $(".ticketTitle").css("z-index", 0);

    $("#popupWinHis").css("width", 800);
});

initTagDetailList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("reservate.tagId"),
        getMessage("column.title.useYn"),
        getMessage("reservate.table.regDate"),
        "tagSeq"
    ];

    var colModel = [
        {name:"num", index:"num", align:"center", width:"10%"},
        {name:"tagId", index:"tagId", align:"center", width:"50%", classes:"pointer", formatter:pointercursor},
        {name:"useSttus", index:"useSttus", align:"center", width:"20%"},
        {name:"cretDt", index:"cretDt", align:"center", width:"20%"},
        {name:"tagSeq", index:"tagSeq", align:"center", hidden:true}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.tagDetailList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "num",
        sortorder: "desc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "tagDetailList",
            prodSeq : $("#prodSeq").val(),
            startDate : $("#startDate").val(),
            endDate : $("#endDate").val()
        },
        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");

                $("#dateType").val(dateType);
                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);

                myPostData.page = page;
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            setTitle(data);
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            setTitle(null);
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event) {
            // 아이디, 이름 셀 클릭시
            if (columnId == 1) {
                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                showUsedHistoryForTagListPopup(list.tagSeq, $(list.tagId).text(), list.cretDt);
            }
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateTagDetailList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            postData: {
                type : "tagDetailList",
                prodSeq : $("#prodSeq").val(),
                startDate : $("#startDate").val(),
                endDate : $("#endDate").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "tagDetailList",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "prodSeq" : $("#prodSeq").val(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
            "prodNm" : $("#prodNmTitle").text()
    };

    downloadExcelFile("/api/statistics/download", params);
}

initUsedHistoryForTagList = function(tagSeq) {
    var columnNames = [
        getMessage("reservate.table.useContent"),
        getMessage("reservate.table.device"),
        getMessage("reservate.table.stTime"),
        getMessage("reservate.table.fnsTime")
    ];

    var colModel = [
        {name:"contsTitle", index:"contsTitle", align:"center", width:"50%"},
        {name:"devId", index:"devId", align:"center", width:"30%"},
        {name:"stTime", index:"stTime", align:"center", width:"10%"},
        {name:"fnsTime", index:"fnsTime", align:"center", width:"10%"}
    ];

    $("#jqgridPopupData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.tagHistoryList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 5, // 페이지에 출력될 칼럼 개수
        pager: "#popupPageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "contsTitle",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "tagHistoryList",
            tagSeq : tagSeq
        },

        beforeRequest:function() {

        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {

        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridPopupData").clearGridData();
            $("#sp_1_popupPageDiv")
        }
    });
    $("#jqgridPopupData").jqGrid('navGrid','#popupPageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridPopupData").getGridParam("lastpage")) {
            this.value = $("#jqgridPopupData").getGridParam("lastpage");
        }
    });

    $(window).bind("resize", function() { // 그리드의 width 초기화 
        var resizeWidth = $("#popupGridArea").width();
        $("#jqgridPopupData").setGridWidth(resizeWidth, true);
    }).trigger("resize");
}

updateUsedHistoryForTagList = function(tagSeq) {
    $("#jqgridPopupData").jqGrid("setGridParam",
        {
            postData: {
                type : "tagHistoryList",
                tagSeq : tagSeq
            },
            page: 1
        }
    );

    $("#jqgridPopupData").trigger("reloadGrid");
}

showUsedHistoryForTagListPopup = function(tagSeq, tagId, cretDt) {
    $("#tagId").text("(" + tagId + ")");
    $(".infoDate").text(getMessage("reservate.table.regDate") + " : " + cretDt);
    openPopupLayer("popupWinHis");

    if (!didFirstLayoutPopup) {
        initUsedHistoryForTagList(tagSeq);
        didFirstLayoutPopup = true;
        return;
    }
    $("#jqgridPopupData").clearGridData();
    updateUsedHistoryForTagList(tagSeq);
}

closeUsedHistoryForTagListPopup = function() {
    closePopupLayer();
}

openPopupLayer = function(name) {
    popLayerDiv(name, 500, 330, true);
}

closePopupLayer = function() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}

didCompleteLoad = function(obj) {
    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;

        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        document.location.hash = hashlocation;
    }
}

setTitle= function(data) {
    var svcNm = "";
    if (data != undefined && data.result != undefined && data.result.svcNm != undefined) {
        svcNm = data.result.svcNm;
    }
    $("#svcNmTitle").text(svcNm);
    var storNm = "";
    if (data != undefined &&data.result != undefined && data.result.storNm != undefined) {
        storNm = data.result.storNm;
    }
    $("#storNmTitle").text(storNm);
    var prodNm = "";
    if (data != undefined && data.result != undefined && data.result.prodNm != undefined) {
        prodNm = data.result.prodNm;
    }
    $("#prodNmTitle").text(prodNm);
    $(".infoType").text(getMessage("title.kind") + " : " + prodNm);
}