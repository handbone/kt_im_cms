/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;
var svcList = "";
var appType = "";

$(document).ready(function() {
    contentInfo();

    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateUsedContsDetailList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateUsedContsDetailList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateUsedContsDetailList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateUsedContsDetailList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateUsedContsDetailList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateUsedContsDetailList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#serviceList").change(function() {
        changeServiceList();
    });

    setDatePickerOnSelectCallback("updateUsedContsList");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        var strHash = document.location.hash.replace("#", "");
        var dateType = findGetParameter(strHash, "dateType");
        $("#dateType").val(dateType);

        hasChangedHash = true;
    }

    getServiceList();
});

contentInfo = function(){
    callByGet("/api/contents?contsSeq="+$("#contsSeq").val(), "contentsInfoSuccess", "NoneForm", "contentsInfoFail");
}

contentsInfoSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if(data.resultCode == "1000") {
        var item = data.result.contentsInfo;
        $("#contsTitle").html(item.contsTitle);

        listBack($(".btnBack"));
    } else {
        popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
    }
}

contentsInfoFail = function(data) {
    popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
}

getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ONALL", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        if (!didFirstLayout) {
            initUsedOlsvcContsDetailList();
            didFirstLayout = true;
            return;
        }

        updateUsedOlsvcContsDetailList();
        return;
    }

    svcList = data.result.codeList;
    var serviceListHtml = "";
    $(svcList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#memberSvcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#memberSvcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#memberSvcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var svcSeq = findGetParameter(strHash, "svcSeq");
            $("#serviceList").val(svcSeq);
        } else {
            $("#serviceList option:eq(0)").prop("selected", true);
        }
    }

    if (!didFirstLayout) {
        initUsedContsDetailList();
        didFirstLayout = true;
        return;
    }

    updateUsedContsDetailList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
}

changeServiceList = function() {
    updateUsedContsDetailList();
}

initUsedContsDetailList = function() {
    $(svcList).each(function(i, item) {
        if (item.comnCdValue == $("#serviceList").val()) {
            appType = item.appType;
            return false;
        }
    });

    var columnNames;
    if ($("#dateType").val() == "day") {
        columnNames = [
            getMessage("common.time"),
            getMessage("statistics.column.plcyCnt"),
            "contsSeq"
        ]
    } else {
        columnNames = [
            getMessage("table.date"),
            getMessage("statistics.column.plcyCnt"),
            "contsSeq"
        ]
    }

    var colModel = [
        {name:"useDt", index:"useDt", align:"center"},
        {name:"playCnt", index:"playCnt", align:"center"},
        {name:"contsSeq", index:"contsSeq", hidden:true}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.usedContsDetailList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "useDt",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "launcherUsedContsDetail",
            svcSeq : $("#serviceList option:selected").val(),
            appType : appType,
            dateType : $("#dateType").val(),
            contsSeq : $("#contsSeq").val(),
            startDate : $("#startDate").val(),
            endDate : $("#endDate").val()
        },
        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var svcSeq = findGetParameter(strHash, "svcSeq");
                var contsSeq = $("#contsSeq").val();
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");

                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);

                $("#serviceList").val(svcSeq);

                myPostData.page = page;
                myPostData.svcSeq = $("#serviceList option:selected").val();
                myPostData.dateType = dateType;
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
            resizeJqGridWidth("jqgridData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateUsedContsDetailList = function() {
    $(svcList).each(function(i, item) {
        if (item.comnCdValue == $("#serviceList").val()) {
            appType = item.appType;
            return false;
        }
    });

    $("#jqgridData").jqGrid("setGridParam",
        {
            postData: {
                type : "launcherUsedContsDetail",
                svcSeq : $("#serviceList option:selected").val(),
                appType : appType,
                dateType : dateType,
                startDate : $("#startDate").val(),
                endDate : $("#endDate").val()
            },
            page: 1
        }
    );

    if (dateType == "day") {
        $("#jqgridData").jqGrid('setLabel', "useDt", getMessage("common.time"));
    } else {
        $("#jqgridData").jqGrid('setLabel', "useDt", getMessage("table.date"));
    }

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "launcherUsedContsDetail",
            "titleHead" : getMessage("service.common.service.name") + ":" + $("#serviceList option:selected").text(),
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : $("#serviceList option:selected").val(),
            "svcNm" : $("#serviceList option:selected").text(),
            "contsSeq" : $("#contsSeq").val(),
            "contsTitle" : $("#contsTitle").text(),
            "dateType" : dateType,
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        hashlocation += "&svcSeq=" + $("#serviceList option:selected").val();

        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        document.location.hash = hashlocation;
    }
}