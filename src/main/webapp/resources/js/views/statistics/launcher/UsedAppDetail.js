/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var chgcolumnNames;

$(document).ready(function() {
    appInfo();

    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateUsedAppList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateUsedAppList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateUsedAppList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateUsedAppList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateUsedAppList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateUsedAppList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#btnRegistered").click(function(){
        pageMove("/statistics/contents/registered");
    });

    $("#mainList").change(function() {
        updateUsedAppList();
    });

    setDatePickerOnSelectCallback("updateUsedAppList");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;

        var strHash = document.location.hash.replace("#", "");

        getServiceList();
    });

    if (document.location.hash) {
        var strHash = document.location.hash.replace("#", "");
        hasChangedHash = true;
    }

    getServiceList();

});


getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ONALL", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#mainList").html("");
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#mainList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#mainList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#mainList").attr('disabled', 'true');
        $("#mainList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var svcSeq = findGetParameter(strHash, "svcSeq");
            $("#mainList").val(svcSeq);
        } else {
            $("#mainList option:eq(0)").prop("selected", true);
        }
    }

    updateUsedAppList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#mainList").html("");
}


initUsedAppList = function() {
    var columnNames;
    var beforeDateType = dateType;
    if (dateType == "day") {
        columnNames = [
            getMessage("table.num"),
            getMessage("common.time"),
            getMessage("statistics.column.exeCnt")
        ]
    } else {
        columnNames = [
            getMessage("table.num"),
            getMessage("table.date"),
            getMessage("statistics.column.exeCnt")
        ]
    }

    var colModel = [
        {name:"num", index: "num", align:"center", width:40, hidden:true},
        {name:"useDt", index:"useDt", align:"center"},
        {name:"totCount", index:"totCount", align:"center"}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.usedAppDetailList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "useDt",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "launcherUsedAppDetail",
            dateType : dateType,
            startDate : $("#startDate").val(),
            appSeq : $("#appSeq").val(),
            endDate : $("#endDate").val()
        },

        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");

            updateParamBeforeSending();

            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");
                var svcSeq = findGetParameter(strHash, "svcSeq");
                var appSeq = $("#appSeq").val();


                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);


                myPostData.appSeq = appSeq;
                myPostData.dateType = dateType;
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
                myPostData.page = page;
                myPostData.svcSeq = svcSeq;

                if(beforeDateType != dateType){
                    if (dateType == "day") {
                        $("#jqgridData").jqGrid('setLabel', "useDt",getMessage("common.time"));
                    } else {
                        $("#jqgridData").jqGrid('setLabel', "useDt",getMessage("table.date"));
                    }
                }
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
            resizeJqGridWidth("jqgridData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateUsedAppList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            search : true,
            postData: {
                type : "launcherUsedAppDetail",
                dateType : dateType,
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val(),
                appSeq : $("#appSeq").val(),
            },
            page: 1
        }
    );

    if (dateType == "day") {
        $("#jqgridData").jqGrid('setLabel', "useDt", getMessage("common.time"));
    } else {
        $("#jqgridData").jqGrid('setLabel', "useDt", getMessage("table.date"));
    }

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");

    svcSeq = $("#mainList option:selected").val();

    var titleHead = "";
    titleHead += $(".rsvSelect select").eq(0).parent("span").prev().text()+":"+$(".rsvSelect select").eq(0).find("option:selected").text();

    var date = "";

    var params = {
            "type" : "launcherUsedAppDetail",
            "appSeq" : $("#appSeq").val(),
            titleHead : titleHead,
            "pkgNm" : $("#pkgNm").text(),
            "dateType" : dateType,
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : svcSeq,
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
    };

    downloadExcelFile("/api/statistics/download", params);
}


updateParamBeforeSending = function() {
    var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
    myPostData.appSeq = $("#appSeq").val();
    myPostData.svcSeq = $("#mainList option:selected").val();
    myPostData.dateType = dateType;

    $('#jqgridData').jqGrid("setGridParam", { postData: myPostData });
}

didCompleteLoad = function(obj) {
    //session
    if (sessionStorage.getItem("last-url") != null) {
        sessionStorage.removeItem('state');
        sessionStorage.removeItem('last-url');
    }

    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        hashlocation += "&svcSeq=" + $("#mainList option:selected").val();

        document.location.hash = hashlocation;
    }
}

appInfo = function(){
    callByGet("/api/svcApp?appSeq="+$("#appSeq").val(), "appInfoSuccess", "NoneForm", "appInfoFail");
}

appInfoSuccess = function(data) {
    if(data.resultCode == "1000") {
        var item = data.result.appInfo;
        $("#pkgNm").html(item.pkgNm);
        $("#mainList").val(item.svcSeq);

        initUsedAppList();


        listBack($(".btnBack"));
        formDataDeleteAll("NoneForm");
    } else {
        popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
    }
}

appInfoFail = function(data) {
    popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
}