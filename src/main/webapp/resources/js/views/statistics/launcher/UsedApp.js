/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;
var chgcolumnNames;

$(document).ready(function() {
    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateTrafficList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateTrafficList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateTrafficList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateTrafficList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateTrafficList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateTrafficList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $(".btnSearch").click(function() {
        updateTrafficList();
    });

    setDatePickerOnSelectCallback("updateTrafficList");

    setDateType("day");

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;

        var strHash = document.location.hash.replace("#", "");

        getServiceList()
    });

    if (document.location.hash) {
        var strHash = document.location.hash.replace("#", "");
        hasChangedHash = true;
    }

    getServiceList();
});


getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ONALL", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#mainList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#mainList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#mainList").attr('disabled', 'true');
        $("#mainList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var svcSeq = findGetParameter(strHash, "svcSeq");
            $("#mainList").val(svcSeq);
        } else {
            $("#mainList option:eq(0)").prop("selected", true);
        }
    }

    if (!didFirstLayout) {
        initUsedAppList();
        didFirstLayout = true;
        return;
    }
    updateTrafficList();

}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#mainList").html("");
}


initUsedAppList = function() {
    var columnNames;
    var beforeDateType = dateType;
    columnNames = [
        getMessage("table.num"),
        getMessage("statistics.column.AppNm"),
        getMessage("statistics.column.exeCnt"),
        getMessage("table.num")
    ];

    var colModel = [
        {name:"num", index: "num", align:"center", width:40, hidden:true},
        {name:"pkgNm", index:"pkgNm", align:"center",formatter:pointercursor},
        {name:"totCount", index:"totCount", align:"center"},
        {name:"appSeq", index:"appSeq", align:"center",hidden:true}
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.usedAppList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "useDt",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "launcherUsedApp",
            dateType : dateType,
            startDate : $("#startDate").val(),
            endDate : $("#endDate").val()
        },

        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");

            updateParamBeforeSending();

            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");
                myPostData.page = page;


                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);


                myPostData.dateType = dateType;
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
                myPostData.page = page;

                if(beforeDateType != dateType){
                    if (dateType == "day") {
                        $("#jqgridData").jqGrid('setLabel', "useDt",getMessage("common.time"));
                    } else {
                        $("#jqgridData").jqGrid('setLabel', "useDt",getMessage("table.date"));
                    }
                }
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            didCompleteLoad(this);
            $(this).find(".pointer").parent("td").addClass("pointer");
            resizeJqGridWidth("jqgridData", "gridArea");
        },
        onCellSelect: function(rowId, columnId, cellValue, event){
            // 콘텐츠 ID 선택시
            if(columnId == 1){
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");

                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                var hashlocation = "";

                hashlocation += "dateType=" + dateType;
                var startDate = $("#startDate").val();
                if (startDate) {
                    hashlocation += "&startDate=" + startDate;
                }
                var endDate = $("#endDate").val();
                if (endDate) {
                    hashlocation += "&endDate=" + endDate;
                };

                hashlocation += "&svcSeq=" + $("#mainList").val();


                pageMove("/statistics/olSvcLauncher/usedAppDetail/"+list.appSeq+"#"+hashlocation);

            }
         },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateTrafficList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            search : true,
            postData: {
                type : "launcherUsedApp",
                dateType : dateType,
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val(),
                contsSeq : $("#contsSeq").val(),
            },
            page: 1
        }
    );

    if (dateType == "day") {
        $("#jqgridData").jqGrid('setLabel', "useDt", getMessage("common.time"));
    } else {
        $("#jqgridData").jqGrid('setLabel', "useDt", getMessage("table.date"));
    }

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");


    var svcSeq = 0;
    svcSeq = $("#mainList option:selected").val();


    var date = "";

    var titleHead = "";
    titleHead += $(".rsvSelect select").eq(0).parent("span").prev().text()+":"+$(".rsvSelect select").eq(0).find("option:selected").text();

    var params = {
            "type" : "launcherUsedApp",
            titleHead : titleHead,
            "svcNm": $("#mainList option:selected").text(),
            "dateType" : dateType,
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : svcSeq,
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
    };

    downloadExcelFile("/api/statistics/download", params);
}



updateParamBeforeSending = function() {
    var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
    myPostData.contsSeq = $("#contsSeq").val();
    myPostData.svcSeq = $("#mainList option:selected").val();
    myPostData.dateType = dateType;

    $('#jqgridData').jqGrid("setGridParam", { postData: myPostData });
}


didCompleteLoad = function(obj) {
    //session
    if (sessionStorage.getItem("last-url") != null) {
        sessionStorage.removeItem('state');
        sessionStorage.removeItem('last-url');
    }

    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        hashlocation += "&svcSeq=" + $("#mainList option:selected").val();

        document.location.hash = hashlocation;
    }
}

