/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var dayOfWeek = new Array(getMessage("datePicker.day.sun"), getMessage("datePciker.day.mon")
                   , getMessage("datePicker.day.tue"), getMessage("datePicker.day.wed")
                   , getMessage("datePicker.day.thu"), getMessage("datePicker.day.fri")
                   , getMessage("datePicker.day.sat"));

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;
var firstRowData;
var lastRequestXHR;
$(document).ready(function() {
    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateSettlementList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateSettlementList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateSettlementList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateSettlementList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        if (lastRequestXHR != null) {
            lastRequestXHR.abort();
        }
        updateSettlementList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        if (lastRequestXHR != null) {
            lastRequestXHR.abort();
        }
        updateSettlementList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        if (lastRequestXHR != null) {
            lastRequestXHR.abort();
        }
        updateSettlementList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        if (lastRequestXHR != null) {
            lastRequestXHR.abort();
        }
        updateSettlementList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#serviceList").change(function() {
        changeServiceList();
    });

    $("#storeList").change(function() {
        changeStoreList();
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDatePickerOnSelectCallback("updateSettlementList");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    getServiceList();
});


getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        $("#storeList").html("");
        return;
    }

    var serviceListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var svcSeq = findGetParameter(strHash, "svcSeq");
            $("#serviceList").val(svcSeq);
        } else {
            $("#serviceList option:eq(0)").prop("selected", true);
        }
    }

    getStoreList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
    $("#storeList").html("");
}

getStoreList = function() {
    var svcSeq = ($("#serviceList option:selected").val() == 0) ? "" : $("#serviceList option:selected").val();
    formData("NoneForm" , "comnCdValue", svcSeq);
    callByGet("/api/codeInfo?comnCdCtg=STORE", "didReceiveStoreList", "NoneForm", "didNotReceiveStoreList");
    formDataDeleteAll("NoneForm");
}

didReceiveStoreList = function(data) {
    var storeListHtml = "<option value='0'>"+getMessage("select.all")+"</option>";

    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.nostore.msg");
        }
        popAlertLayer(msg);
        $("#storeList").html(storeListHtml);

        if (!didFirstLayout) {
            initSettlementList();
            didFirstLayout = true;
            return;
        }

        updateSettlementList();
        return;
    }

    $(data.result.codeList).each(function(i, item) {
        storeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#storeList").html(storeListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#storSeq").val() != 0) {
        $("#storeList option").not("[value='"+ $("#storSeq").val() + "']").remove();
        $("#storeList option[value='"+ $("#storSeq").val() + "']").prop("selected", true);
        $("#storeList").attr("disabled", true);
    } else {
        if (hasChangedHash) {
            var strHash = document.location.hash.replace("#", "");
            var storSeq = findGetParameter(strHash, "storSeq");
            $("#storeList").val(storSeq);
        } else {
            $("#storeList option:eq(0)").prop("selected", true);
        }
    }

    if (!didFirstLayout) {
        initSettlementList();
        didFirstLayout = true;
        return;
    }

    updateSettlementList();
}

didNotReceiveStoreList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#storeList").html("<option value='0'>"+getMessage("select.all")+"</option>");
}

changeServiceList = function() {
    getStoreList();
}

changeStoreList = function() {
    updateSettlementList();
}

initSettlementList = function() {
    var columnNames = [
        getMessage("common.period"),
        getMessage("statistics.payInfo.table.netAmount"),
        getMessage("statistics.payInfo.table.card"),
        getMessage("statistics.payInfo.table.money"),
        getMessage("statistics.payInfo.table.etc"),
        getMessage("statistics.payInfo.table.discount")
    ];

    var colModel = [
        {name:"settlDt", index:"settlDt", align:"center", formatter:periodFormatter},
        {name:"totalAmount", index:"totalAmount", align:"center", formatter:amountFormatter},
        {name:"cardAmount", index:"cardAmount", align:"center", formatter:amountFormatter},
        {name:"cashAmount", index:"cashAmount", align:"center", formatter:amountFormatter},
        {name:"etcAmount", index:"etcAmount", align:"center", formatter:amountFormatter},
        {name:"dscntAmount", index:"dscntAmount", align:"center", formatter:amountFormatter},
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.settlementManageList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "settlDt",
        sortorder: "desc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "settlementManageList",
            svcSeq : $("#serviceList option:selected").val(),
            storSeq : $("#storeList option:selected").val(),
            startDate : $("#startDate").val(),
            endDate : $("#endDate").val(),
            target : dateType
        },
        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var svcSeq = findGetParameter(strHash, "svcSeq");
                var storSeq = findGetParameter(strHash, "storSeq");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");

                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);

                $("#serviceList").val(svcSeq);
                $("#storeList").val(storSeq);

                myPostData.page = page;
                myPostData.svcSeq = $("#serviceList option:selected").val();
                myPostData.storSeq = $("#storeList option:selected").val();
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
                myPostData.target = dateType;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadBeforeSend: function (xhr) {
            lastRequestXHR = xhr;
        },
        loadComplete: function(data) {
            lastRequestXHR = null;
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        }
    });

    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

updateSettlementList = function() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            postData: {
                type : "settlementManageList",
                svcSeq : $("#serviceList option:selected").val(),
                storSeq : $("#storeList option:selected").val(),
                startDate : $("#startDate").val(),
                endDate : $("#endDate").val(),
                target : dateType
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

downloadExcel = function() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "settlementManageList",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "svcSeq" : $("#serviceList option:selected").val(),
            "storSeq" : $("#storeList option:selected").val(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
            "target" : dateType
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        hashlocation += "&svcSeq=" + $("#serviceList option:selected").val();
        hashlocation += "&storSeq=" + $("#storeList option:selected").val();

        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        document.location.hash = hashlocation;
    }

    // 하단 페이지 인덱스 표시 여부, 정렬 기능 사용여부, 컬럼명 변경
    var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
    if (dateType == "custom") {
        $("#pageDiv").show();
        setSortable(true);
    } else {
        $("#pageDiv").hide();
        setSortable(false);
    }

    if (dateType == "day") {
        $("#jqgridData").jqGrid("setLabel", colModel[0]['name'], getMessage("statistics.payInfo.table.top"));
    } else {
        $("#jqgridData").jqGrid("setLabel", colModel[0]['name'], getMessage("common.period"));
    }
}

periodFormatter = function(cellvalue, option, rowData, action) {
    if (dateType != "day") {
        return cellvalue;
    }

    var description = getMessage("statistics.payInfo.table.today");
    if (option.rowId == 2) {
        description = getMessage("statistics.payInfo.table.yesterday");
    } else if (option.rowId == 3) {
        description = getMessage("statistics.payInfo.table.prevWeek");
    }
    var html = "<div class='txtLeft'>" + description + "<br>" + cellvalue + " " + getDayLabel(cellvalue) + "</div>";
    return html;
}

amountFormatter = function(cellvalue, option, rowData, action) {
    var money = toKRW(cellvalue);
    if (dateType != "day") {
        return money;
    }

    var html = "";
    if (option.rowId == 1) {
        firstRowData = rowData;
        html = "<div class='txtRight h5'>" + money;
        html += "<br><span class='txtBlueGreen h6 txtNormal'>&nbsp;</span></div>";
    } else {
        var firstData = 0;
        var secondData = 0;
        if (option.colModel.name == "totalAmount") {
            firstData = firstRowData.totalAmount;
            secondData = rowData.totalAmount;
        } else if (option.colModel.name == "cardAmount") {
            firstData = firstRowData.cardAmount;
            secondData = rowData.cardAmount;
        } else if (option.colModel.name == "cashAmount") {
            firstData = firstRowData.cashAmount;
            secondData = rowData.cashAmount;
        } else if (option.colModel.name == "etcAmount") {
            firstData = firstRowData.etcAmount;
            secondData = rowData.etcAmount;
        } else if (option.colModel.name == "dscntAmount") {
            firstData = firstRowData.dscntAmount;
            secondData = rowData.dscntAmount;
        }

        var diffValue = getDiffValue(firstData, secondData);
        var percentage = getPercentage(diffValue, secondData);

        html = "<div class='txtRight h5'>" + money;
        html += "<br><span class='txtBlueGreen h6 txtNormal'>" + toKRW(diffValue) + " (" + percentage + ")" + "</span></div>";
    }
    return html;
}

getDiffValue = function(first, second) {
    var diffValue = first - second;
    return diffValue;
}

getPercentage = function(first, second) {
    if (!second) {
        return 0 + "%";
    }
    var percentage = Math.round((first / second) * 100);
    return percentage + "%";
}

toKRW = function(amount) {
    return "\\" + amount.toLocaleString();
}

getDayLabel = function(dateString) {
    dateString = dateString.replace(/[.]/g, '-');
    var day = new Date(dateString).getDay();
    return "(" + dayOfWeek[day] + ")";
}

setSortable = function(value) {
    var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
    if (!value) {
        for (var i = 0; i < colModel.length; i++) {
            colModel[i]['sortable'] = false;
        }
        $(".ui-jqgrid-sortable").removeClass("ui-jqgrid-sortable");
        $(".s-ico > span:not(.ui-state-disabled)").parent().hide();
        $("#jqgridData").jqGrid('setGridParam', 'colModel', colModel);
        return;
    }

    for (var i = 0; i < colModel.length; i++) {
        colModel[i]['sortable'] = true;
    }
    $("#jqgridData").jqGrid('setGridParam', 'colModel', colModel);
    $(".ui-th-column > div").addClass("ui-jqgrid-sortable");
    $(".s-ico > span:not(.ui-state-disabled)").parent().show();
}