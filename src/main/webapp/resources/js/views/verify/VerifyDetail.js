/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var metaData = ""; // 서브메타데이터 정보

$(document).ready(function(){
    contentInfo();
});


contentInfo = function(){
    callByGet("/api/contents?contsSeq="+$("#contsSeq").val(), "contentsInfoSuccess", "NoneForm", "contentsInfoFail");
}

contentsInfoSuccess = function(data) {
    if(data.resultCode == "1000") {
        var item = data.result.contentsInfo;
        var coverImg = data.result.contentsInfo.coverImg;

        $("#contsTitle, #headTitle").html(item.contsTitle);
        $("#contsSubTitle").html(item.contsSubTitle);
        $("#contsDesc").html(item.contsDesc);
        $("#contsId").html(item.contsID);
        $("#contCtgNm").html(item.contCtgNm);
        $("#cretrID").html(maskId(item.cretrID));
        $("#mbrNm").html(item.mbrNm);
        $("#contsVer").html(item.contsVer);
        var str = item.rcessWhySbst;
        var changeNbsp = replaceAll(str,"\u0020", "&nbsp;");
        var rcessWhySbst = "";

        if (item.sttusVal == "04") {
            rcessWhySbst = 'popAlertLayer("'+changeNbsp+"\",'','"+getMessage("contents.table.refuse.verify.popup")+"',true)";
            $("#sttusView").after("<span id='refuseConfirm' onclick="+rcessWhySbst+"  >("+getMessage("contents.table.refuse.verify")+")</span>");
            $("#sttusView").text(item.sttus);
            $("#sttusView").addClass("boxStateRed").removeClass("boxStateBlack");
        } else {
            rcessWhySbst = "";
            if (item.sttusVal != "01") {
                if (item.sttusVal == "06") {
                    $("#sttusView").text(getMessage("contents.table.state.verify.success"));
                } else {
                    $("#sttusView").text(item.sttus);
                }

                $("#sttusBtn").hide();
            } else {
                $("#sttusView").text(item.sttus);
                $("#sttusView").addClass("boxStateBlue").removeClass("boxStateBlack");
            }
        }

        $("#cp").html(item.cp);
        $("#cntrctDt").html(item.cntrctStDt+" ~ "+item.cntrctFnsDt);
        $("#cretDt").html(item.cretDt);
        $("#amdDt").html(item.amdDt);
        $("#amdrID").html(maskId(item.amdrID));
        $("#maxAvlNop").html(item.maxAvlNop);

        var genreHtml = "";
        $(data.result.contentsInfo.genreList).each(function(i,item){
            genreHtml += item.genreNm+"/";
        });
        $("#genreList").html(genreHtml.slice(0,-1));

        if (item.metaData != "") {
            metaData = item.metaData.replace(/&#034;/gi, "\"");
            buttonHtml = "<input type=\"button\" class=\"btnDefaultWhite btnMetadata\" style=\"width:180px;\" value=\"" + getMessage("submeta.confirm") + "\" onclick=\"showMetadataPopLayer()\">";
            $("#contCtgNm").append("&nbsp;&nbsp;" + buttonHtml);
        } else {
            $(".btnMetadataSub").remove();
        }

        var serviceHtml = "";
        $(data.result.contentsInfo.serviceList).each(function(i,item){
            serviceHtml += item.svcNm+"/";
        });
        $("#serviceList").html(serviceHtml.slice(0,-1));

        $("#fileType").html(item.fileType);
        if (item.fileType == "VIDEO") {
            if(item.firstCtgNm.match(getMessage("common.liveon")) != null){ // 첫번째 카테고리 명이 '영상_LiveOn'일때
                $("#contsDesc").parents("tr").after("<tr style='display:table-row'><th>"+getMessage("common.run.streaming.url")+"</th><td><span class='article' id='streamingUrl'>"+item.exeFilePath+"</span></td></tr>");
            }
            $(".exeFilePath").hide();
            $(".contentInfo").show();
        }
        $("#exeFilePath").html(item.exeFilePath);
//        $("#videoName").html(item.videoName); 없음
//        if(item.contentsXMLName){
//            $("#contentsXmlName").html(""+item.contentsXMLName+"<a href='" + makeAPIUrl("/api/fileDownload/"+item.contentsXMLSeq) +"' class='fontblack'>다운로드</a>");
//        }
        $("#coverImg").html("<a href=\""+makeAPIUrl(coverImg.coverImgPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(coverImg.coverImgPath,"img")+"' title=\""+coverImg.fileSeq+"\"></a>");
        $("#coverImg").justifiedGallery({
            rowHeight : 220,
            lastRow : 'nojustify',
            margins : 3
        });
        var videoHtml = "";
        var metadataHtml = "";
        var contsHtml = "";

        $(data.result.contentsInfo.videoList).each(function(i,item){
            videoHtml += "<p><span class='float_l'>"+(i+1)+". "+item.fileNm+"("+item.fileSize+"Byte)</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            metadataHtml += "<p><span class='float_l'>"+item.metadataInfo.fileNm+"</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.metadataInfo.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            if(item.metadataInfo.fileNm == ""){
                metadataHtml = "";
            }
//            contsHtml += "<p><span class='float_l'>"+item.contsXmlInfo.fileNm+"</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.contsXmlInfo.fileSeQ) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            contsHtml = "";
        });

        $(data.result.contentsInfo.prevList).each(function(i,item){
            $("#prevList").html("<p><span class='float_l'>"+(i+1)+". "+item.fileNm+"("+item.fileSize+"Byte)</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>");
        });

//        $("#pakageDown").attr("onclick","pageMove('/api/package/"+item.seq+"')");
        var thumbnailHtml = "";
        $(data.result.contentsInfo.thumbnailList).each(function(i,item){
            thumbnailHtml += "<a href=\""+makeAPIUrl(item.thumbnailPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(item.thumbnailPath,"img")+"' title=\""+item.fileSeq+"\"></a>";
        });
        $("#thumbnailList").html(thumbnailHtml);
        $("#thumbnailList").justifiedGallery({
            rowHeight : 120,
            lastRow : 'nojustify',
            margins : 3
        });
        $(".swipebox").swipebox();
        $("#widthSetting").css("width", "10%");

        var fileHtml = "";

        if (videoHtml == "") {
            $("#videoList").parents("tr").hide();
            $("#metadataName").parents("tr").hide();
            $(data.result.contentsInfo.fileList).each(function(i,item){
                fileHtml = "<p><span class='float_l'>"+item.fileNm+"&nbsp;</span><a href=\"" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "\" class=\"fontblack btnDownloadGray\">[다운로드]</a></p>";
                $("#file").html(fileHtml);
            });
        } else {
            $("#videoList").html(videoHtml);
            $("#metadataName").html(metadataHtml);
            if(metadataHtml == "") {
                $("#metadataName").parents("tr").hide();
            }
            $("#contsInfo").html(contsHtml);
            $(".contsInfo").hide();
            $("#file").parents("tr").hide();
        }

        // 현재 페이지의 모든 이미지에 대해 img 표시 사이즈로 썸네일 이미지를 불러와 교체 (공통함수)
        imgToThumbImg();

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/verify#page=1&sttus=&svcSeq=&verify=result");
    }
}

contentsInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/verify#page=1&sttus=&svcSeq=&verify=result");
}

function contentsSttusChange(){
    popConfirmLayer(getMessage("msg.contents.verify.confirm"), function(){
        formData("NoneForm" , "contsSeq", $("#contsSeq").val());
        formData("NoneForm" , "sttus", "02");
        callByPut("/api/contents" , "contentsSttusChangeSuccess", "NoneForm");
    });
}

contentsSttusChangeSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("msg.common.complte.response"),"/contents/"+$("#contsSeq").val());
    }else if(data.resultCode == "1011"){
        formDataDeleteAll("NoneForm");
        popAlertLayer(getMessage("msg.common.update.fail"));
    }
}

/* blockUI 닫기
 * parameter: x
 * return: x
 */
function closeMetaLayer() {
    $("body").css("overflow-y" , "visible");
    $("#submetadataTableArea").mCustomScrollbar('destroy');
    $.unblockUI();
}

/* blockUI 열기
 * parameter: x
 * return: x
 */
function showMetadataPopLayer() {
    callByGet("/api/codeInfo?comnCdCtg=CONTS_SUB_META", "showMetadataConts", "NoneForm");
}

/* 서브 메타데이터 상세보기 */
showMetadataConts = function(data) {
    if(data.resultCode == "1000"){
        var tableHtml = "";
        var metaVal = "";
        $("#detailSubmetadata").html("");
        $(data.result.codeList).each(function(i, item) {
            var metaObj = JSON.parse(metaData);
            for (key in metaObj) {
                if (key == item.comnCdValue) {
                    if (item.comnCdNm.indexOf(getMessage("submeta.time")) != -1 && metaObj[key] != "") {
                        metaVal = metaObj[key] + " " + getMessage("common.minute");
                    } else if(item.comnCdNm.indexOf(getMessage("submeta.rt")) != -1){
                        // #a8484는 작은 따옴표(') #q0808은 큰 따옴표(")로 DB 저장
                        if(metaObj[key].match("#a8484") != null || metaObj[key].match("#q0808") != null){
                            metaObj[key] = metaObj[key].replace("#a8484", "\'");
                            metaObj[key] = metaObj[key].replace("#q0808", "\"");
                        }
                        metaVal = metaObj[key];
                    } else {
                        metaVal = metaObj[key];
                    }
                }
            }

            if (metaVal != "") { //메타데이터가 없으면 조회 리스트에 추가하지 않음
                tableHtml += "<tr><th>" + item.comnCdNm + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">" + "</th><td>" + metaVal + "</td>";
                metaVal = "";
            }
            tableHtml += "</tr>";
        });

        $("#detailSubmetadata").append(tableHtml);
    } else {
        $("#detailSubmetadata").html("");
    }
    $("#popupDetailSubmetadata").css({"left":"-1000px","top":"-1000px"}).show();
    var popupHeight = $("#popupDetailSubmetadata").height();
    var popupWidth = $("#popupDetailSubmetadata").width();
    $("#popupDetailSubmetadata").hide();
    popLayerDiv("popupDetailSubmetadata", popupWidth, popupHeight, true);
    $("#submetadataTableArea").mCustomScrollbar({ axis : "y", theme:"inset-3" });
}