/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var readyPage = false;
var apiUrl = makeAPIUrl("/api/contents");
var lastBind = false;
var gridState;
var tempState;
var hashSvc;
var hashVerify;
var hashSttus;
var selTab =0;
var cretrIdOption;
var ctgOption;
var totalPage = 0;
var isStatusDetailMode = false;
var shouldPreventReloadDetailMode = false;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
})
$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");

            var hasStatusDetailHash = checkUndefined(findGetParameter(str_hash,"detail"));
            var isStatusDetailMode = (hasStatusDetailHash === "true");
            shouldPreventReloadDetailMode = (isStatusDetailMode && shouldPreventReloadDetailMode);
            if (shouldPreventReloadDetailMode) {
                shouldPreventReloadDetailMode = false;
                return;
            }
            setStatusDetailMode(isStatusDetailMode);

            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashVerify = checkUndefined(findGetParameter(str_hash,"verify"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashSttus = checkUndefined(findGetParameter(str_hash,"sttus"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#selectBox").val(hashSttus);
            $("#svcList").val(hashSvc);
            $("#verify").val(hashVerify);

            if (selTab == 2) {
                reloadStatus();
                return;
            }

            jQuery("#jqgridData").trigger("reloadGrid");

        } else {
            gridState = "READY";
        }
    });
})
$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashVerify = checkUndefined(findGetParameter(str_hash,"verify"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#verify").val(hashVerify);
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        var hasStatusDetailHash = checkUndefined(findGetParameter(str_hash, "detail"));
        setStatusDetailMode(hasStatusDetailHash === "true");

        if (hashVerify == "" || hashVerify == "verify" || hashVerify == null) {
            hashVerify = "verify";
            apiUrl = makeAPIUrl("/api/contents");
            selTab = 0;
        } else if (hashVerify == "stat") {
            apiUrl = makeAPIUrl("/api/verifyRecord");
            selTab = 2;
        } else {
            apiUrl = makeAPIUrl("/api/contents");
            selTab = 1;
        }

        gridState = "NONE";
    }

    svcList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

function keywordSearch(){
    gridState = "SEARCH";

    // 검색어 없이 조회한 경우 검수 이력 상세보기 화면에서 나간것으로 판단
    var shouldExitStatusDetailMode = (isStatusDetailMode && selTab == 2 && $("#keyword").val() == "");
    if (shouldExitStatusDetailMode) {
        exitStatusDetail();
        return;
    }

    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {
        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "contsID":break;
            case "contsTitle":break;
            case "contCtgNm":break;
            case "cretrNm":break;
            default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }

        cretrIdOption = $("#target option[value='CRETR_ID']");
        ctgOption = $("#target option[value='CTG_NM']");
    }
    $("#target").val(searchField);
}
var noData = false;
svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
                svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);
        noData = false;
        contentStateList();
    } else {
        noDatanoData = true;
        contentStateList();
    }
}

contentStateList = function(){
    callByGet("/api/codeInfo?comnCdCtg=CONTS_STTUS&verify="+$("#verify").val(),"contentStateListSuccess","NoneForm");
}
contentStateListSuccess = function(data){
    if(data.resultCode == "1000"){
        var sttusListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.codeList).each(function(i,item) {
            sttusListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNm+"</option>";
        });
        $("#selectBox").html(sttusListHtml);
        if (!readyPage) {
            readyPage = true;
            tabSet();
            contentList();
        }
    } else {
        if (!readyPage) {
            readyPage = true;
            tabSet();
            contentList();
        }
    }
}

var stateVal;

function tabVal(seq,e){
    setStatusDetailMode(false);
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
    $(e).parent("div").removeClass("tabOff").addClass("tabOn").css({ 'pointer-events': 'none' });

    selTab = seq;

    $("#target").val("");
    $("#selectBox").val("");
    $("#keyword").val("");
    $("#svc").val("");

    totalPage = 0;

    jQuery("#jqgridData").jqGrid('GridUnload');
    tabSet();
    contentStateList();
    $("#target").html("");
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
}

modelSet = function(){
    var ccm = $("#jqgridData").getGridParam();

    tabSet();

    $("#jqgridData").setGridWidth($("#contents").width());
    $("#jqgridData").jqGrid('setGridParam',ccm);

    if (selTab == 0) {
        $("#jqgridData").jqGrid("showCol", ["cb"]);
    } else if (selTab == 2) {
        $("#jqgridData").jqGrid("hideCol", ["cb"]);
    }

    $("#jqgridData").setGridWidth($("#contents").width());
    $("#jqgridData").jqGrid('setGridParam',ccm);
}

var option_common = {
    url:apiUrl,
    datatype: "json", // 데이터 타입 지정
    jsonReader : {
        page: "result.currentPage",
        total: "result.totalPage",
        root: "result.contentsList",
        records: "result.totalCount",
        repeatitems: false
    },
    colNames:[
        getMessage("column.title.num"),
        getMessage("column.title.contentID"),
        getMessage("column.title.contentNm"),
        getMessage("table.cp"),
        getMessage("column.title.category"),
        getMessage("column.title.register"),
        getMessage("table.progressStatus"),
        getMessage("column.title.updateDay"),
        getMessage("column.title.contsSeq")
    ],
    colModel: [
        {name:"num", index: "num", align:"center", width:25,hidden:false},
        {name:"contsID", index:"CONTS_ID", align:"center",},
        {name:"contsTitle", index:"CONTS_TITLE", align:"center",formatter:pointercursor},
        {name:"cpNm", index:"CP_NM", align:"center"},
        {name:"contCtgNm", index:"CTG_NM", align:"center"},
        {name:"cretrNm", index:"CRETR_ID", align:"center"},
        {name:"sttus", index:"STTUS", align:"center"},
        {name:"vrfRqtDt", index:"VRF_RQT_DT", align:"center"},
        {name:"contsSeq", index:"CONTS_SEQ", hidden:true}
   ],
    sortorder: "desc",
    height: "auto",
    autowidth: true, // width 자동 지정 여부
    rowNum: 10, // 페이지에 출력될 칼럼 개수
    pager: "#pageDiv", // 페이징 할 부분 지정
    viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
    sortname: "VRF_RQT_DT",
    multiselect: false,
    beforeRequest:function(){
        tempState = gridState;
        var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

        if (gridState == "TABVAL") {
            searchField = "";
            searchString = "";
        } else if (gridState != "SEARCH") {
            $("#target").val(searchField);
            $("#keyword").val(searchString);
        }

        if (isNaN(page)) {
           page =1;
        }

        if (totalPage > 0 && totalPage < page) {
            page = 1;
        }

        searchField = checkUndefined($("#target").val());
        searchString = checkUndefined($("#keyword").val());
        hashSvc = checkUndefined($("#svcList").val());
        hashVerify = checkUndefined($("#verify").val());
        hashSttus = checkUndefined($("#selectBox").val());
        hashVerify = checkUndefined($("#verify").val());

        if (hashVerify == "" || hashVerify == "verify" || hashVerify == null) {
            hashVerify = "verify";
            apiUrl = makeAPIUrl("/api/contents");
            selTab = 0;
        } else if (hashVerify == "stat") {
            apiUrl = makeAPIUrl("/api/verifyRecord");
            hashSvc = "";
            hashSttus = "";
            selTab = 2;
        } else {
            apiUrl = makeAPIUrl("/api/contents");
            hashSvc = "";
            selTab = 1;
        }

        modelSet();

        $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
        $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

        if (gridState == "HASH"){
            myPostData.page = page;
            tempState = "READY";
        } else {
            if(tempState == "SEARCH"){
               myPostData.page = 1;
            } else {
                tempState = "";
            }
        }

        if(gridState == "NONE"){
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            myPostData.page = page;

            if (searchField != null) {
                tempState = "SEARCH";
            } else {
                tempState = "READY";
            }
        }

        if(searchField != null && searchField != "" && searchString != ""){
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
        }else{
            delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
        }

        gridState = "GRIDBEGIN";
        myPostData.sttus = hashSttus;
        if (hashSvc != null && hashSvc != "") {
            myPostData.svcSeq = hashSvc;
        } else {
            myPostData.svcSeq = "";
        }

        if (hashVerify != null && hashVerify != "") {
            myPostData.verify = hashVerify;
        } else {
            myPostData.verify = "";
        }

        if (noData || ($("#svcList option").index() == -1 && selTab == 0)) {
            myPostData.page = 0;
            hashSvc = "";
            popAlertLayer(getMessage("info.service.nodata.msg"));
        }

        $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page });
    },
    loadComplete: function (data) {
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        //session
        if(sessionStorage.getItem("last-url") != null){
            sessionStorage.removeItem('state');
            sessionStorage.removeItem('last-url');
        }

        //Search 필드 등록
        searchFieldSet();

        if (data.resultCode == 1000) {
            totalPage = data.result.totalPage;

            if (page != data.result.currentPage) {
                tempState = "";
            }
        } else {
            tempState = "";
        }

        if (isNaN(page) || noData) {
            page =1;
            $(".ui-pg-input").val("1");
        }

        /*뒤로가기*/
        var hashlocation = "page="+$(this).context.p.page+"&sttus="+hashSttus+"&svcSeq="+hashSvc+"&verify="+$("#verify").val();
        if ($("#verify").val() == "stat") {
            hashlocation = "page="+$(this).context.p.page+"&svcSeq="+hashSvc+"&verify="+$("#verify").val();
            if (isStatusDetailMode) {
                hashlocation += "&detail=true";
                shouldPreventReloadDetailMode = true;
            }
        }

        if($(this).context.p.postData._search && $("#target").val() != null){
            hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
        }

        gridState = "GRIDCOMPLETE";
        document.location.hash = hashlocation;

        if (tempState != "" || str_hash == hashlocation) {
            gridState = "READY";
        }

        $(this).find("td").each(function(){
            if ($(this).index() == 5) { // 구분
                var str = $(this).text();

                if(str == getMessage("contents.table.state.exhibition.success")){
                    str = "<span class='viewIcon'>"+getMessage("contents.table.state.verify.success") + "</span>";
                }
                $(this).html(str);
            }
        })

        if (selTab == 2) {
            cretrIdOption.remove();
            ctgOption.remove();
        } else {
            $("#target").append(ctgOption);
            $("#target").append(cretrIdOption);
        }

      //pageMove Max
        $('.ui-pg-input').on('keyup', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
        });

        $(this).find(".pointer").parent("td").addClass("pointer");
    },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
    onCellSelect: function(rowId, columnId, cellValue, event){
        if(columnId == 2 && hashVerify == "verify"){
            var list = $("#jqgridData").jqGrid('getRowData', rowId);
            sessionStorage.setItem("last-url", location);
            sessionStorage.setItem("state", "view");
            pageMove("/verify/"+list.contsSeq+"/edit");
          } else if (hashVerify == "stat" && columnId == 1) {
              if (isStatusDetailMode) {
                  return;
              }
              $("#target").val("CONTS_ID");
              var str = cellValue;
              var newText = str.replace(/<(\/span|span)([^>]*)>/gi,"");
              $("#keyword").val(newText);
              enterStatusDetail();
          } else if(hashVerify == "result" && columnId == 2) {
              var list = $("#jqgridData").jqGrid('getRowData', rowId);
              sessionStorage.setItem("last-url", location);
              sessionStorage.setItem("state", "view");
              pageMove("/verify/"+list.contsSeq);
          }
//           else {
//              $(this).removeClass("pointer");
//          }
    }
}

contentList = function(){
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}


deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}


tabSet = function(){
    option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentsList",
            records: "result.totalCount",
            repeatitems: false
    };
    option_common['colNames'] = [
        getMessage("column.title.num"),
        getMessage("column.title.contentID"),
        getMessage("column.title.contentNm"),
        getMessage("table.cp"),
        getMessage("column.title.category"),
        getMessage("column.title.register"),
        getMessage("table.progressStatus"),
        getMessage("column.title.updateDay"),
        getMessage("column.title.contsSeq")
    ];

    switch(selTab){
    case 0:
        $(".tab").eq(0).show();
        $(".tab").eq(1).hide();
        $("#verify").val("verify");
        $("#svc").show();
        $(".rsvGrpPos").show();
        option_common['url'] = makeAPIUrl("/api/contents");
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:true},
            {name:"contsID", index:"CONTS_ID", align:"center",sortable:true},
            {name:"contsTitle", index:"CONTS_TITLE", align:"center",formatter:pointercursor,sortable:true},
            {name:"cpNm", index:"CP_NM", align:"center"},
            {name:"contCtgNm", index:"CTG_NM", align:"center",sortable:true},
            {name:"cretrNm", index:"CRETR_ID", align:"center",sortable:true},
            {name:"sttus", index:"STTUS", align:"center",sortable:true},
            {name:"vrfRqtDt", index:"VRF_RQT_DT", align:"center",sortable:true},
            {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:true},
        ];
        if (noData) {
            popAlertLayer(getMessage("info.service.nodata.msg"));
        }
        break;
    case 1:
        $(".tab").eq(0).hide();
        $(".tab").eq(1).show();
        $("#verify").val("result");
        $("#svc").hide();
        option_common['url'] = makeAPIUrl("/api/contents");
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:true},
            {name:"contsID", index:"CONTS_ID", align:"center",sortable:true},
            {name:"contsTitle", index:"CONTS_TITLE", align:"center",formatter:pointercursor,sortable:true},
            {name:"cpNm", index:"CP_NM", align:"center"},
            {name:"contCtgNm", index:"CTG_NM", align:"center",sortable:true},
            {name:"cretrNm", index:"CRETR_ID", align:"center",sortable:true},
            {name:"sttus", index:"STTUS", align:"center",sortable:true},
            {name:"vrfRqtDt", index:"VRF_RQT_DT", align:"center",sortable:true},
            {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:true}
        ];
        $(".rsvGrpPos").show();
        break;
    case 2:
        $(".tab").eq(0).show();
        $(".tab").eq(1).hide();
        $("#verify").val("stat");
        $(".rsvGrpPos").hide();
        $("#svc").hide();
        option_common['url'] = makeAPIUrl("/api/verifyRecord");
        option_common['colNames'] = [
            getMessage("column.title.num"),
            getMessage("column.title.contentID"),
            getMessage("column.title.contentNm"),
            getMessage("table.cp"),
            getMessage("column.title.register"),
            getMessage("table.progressStatus"),
            getMessage("column.title.updateDay"),
            getMessage("column.title.contsSeq")
        ];
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:25,hidden:false,sortable:true},
            {name:"contsID", index:"CONTS_ID", align:"center",formatter:pointercursor,sortable:true},
            {name:"contsTitle", index:"CONTS_TITLE", align:"center",sortable:true},
            {name:"cpNm", index:"CP_NM", align:"center"},
            {name:"cretrNm", index:"CRETR_ID", align:"center",sortable:true},
            {name:"sttus", index:"STTUS", align:"center",sortable:true},
            {name:"vrfRqtDt", index:"VRF_RQT_DT", align:"center",sortable:true},
            {name:"contsSeq", index:"CONTS_SEQ", hidden:true,sortable:true}
        ];
        if (isStatusDetailMode) {
            delete option_common['colModel'][1].formatter;
        }
        break;
    }

    option_common['rowNum'] = $("#limit").val();

}

function setStatusDetailMode(value) {
    isStatusDetailMode = value;
}

function enterStatusDetail() {
    gridState = "SEARCH";
    setStatusDetailMode(true);
    reloadStatus();
}

function exitStatusDetail() {
    setStatusDetailMode(false);
    reloadStatus();
}

function reloadStatus() {
    jQuery("#jqgridData").jqGrid('GridUnload');
    tabSet();
    jQuery("#jqgridData").jqGrid(option_common).trigger("reloadGrid");
}
