/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var readyPage = false;
var apiUrl = makeAPIUrl("/api/contract");
var lastBind = false;
var gridState;
var tempState;
var hashCp;
var hashStore;
var hashSvc;
var hashTab;
var selTab =0;
var totalPage = 0;
var isStatusDetailMode = false;
var shouldPreventReloadDetailMode = false;
var noData = false;
var stateVal;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
})
$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (noData) {
            return;
        }


        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");

            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashTab = checkUndefined(findGetParameter(str_hash,"pageType"));
            hashCp = checkUndefined(findGetParameter(str_hash,"cpSeq"));
            hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

            $("#pageType").val(hashTab);
            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#cpList").val(hashCp);
            $("#storList").val(hashStore);
            $("#svcList").val(hashSvc);

            jQuery("#jqgridData").trigger("reloadGrid");

        } else {
            gridState = "READY";
        }
    });
})
$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashTab = checkUndefined(findGetParameter(str_hash,"pageType"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        hashCp = checkUndefined(findGetParameter(str_hash,"cpSeq"));
        hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

        if (typeof hashTab == "undefined"){
            hashTab = "contract";
        }

        $("#pageType").val(hashTab);
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        if (hashTab == "" || hashTab == "contract" || hashTab == null) {
            hashTab = "contract";
            apiUrl = makeAPIUrl("/api/contract");
            selTab = 0;
        } else {
            hashTab = "billing";
            apiUrl = makeAPIUrl("/api/billing");
            selTab = 1;
        }

        gridState = "NONE";
    }
    cpList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

cpList = function(){
    callByGet("/api/cp?rows=10000","cpListSuccess","NoneForm");
}

cpListSuccess = function(data){
    if (data.resultCode == "1000") {
        var cpListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.cpList).each(function(i, item) {
            cpListHtml += "<option value=\""+item.cpSeq+"\">"+item.cpNm+"</option>";
        });
        $("#cpList").html(cpListHtml);
        $("#cpList").val(hashCp);
        if ($("#cpSeq").val() != 0) {
            $("#cpList").val($("#cpSeq").val());
        }


    }
    if (!readyPage) {
        tabSet();
        svcList();
    }

    if ($("#mbrSe").val() != "01" && $("#mbrSe").val() != "02" ) {
        $("#cpList option").not("[value="+$("#cpSeq").val()+"]").remove();
        $("#cpList").attr('disabled', 'true');
    }
}


svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
                svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }

        if (hashSvc != "" && hashSvc != null) {
            $("#svcList > option[value=" + hashSvc + "]").attr("selected","true");
            hashSvc = "";
        }

        storList();
    } else {
        $("#svcList").html("<option value=''>"+getMessage("select.all")+"</option>");
        $("#storList").html("<option value=''>"+getMessage("select.all")+"</option>");
        contractList();
    }
}


storList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"storListSuccess","NoneForm");
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        checkedName = "";
        var memberStoreListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.storeNmList).each(function(i, item) {
            memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(memberStoreListHtml);
        $("#storList").attr("disabled", false);
        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 서비스 변경 불가
            $("#storList").attr('disabled', true);
        } else {
            $("#storList").attr('disabled', false);
        }



        if (hashStore != "" && hashStore != null) {
            $("#storList > option[value=" + hashStore + "]").attr("selected","true");
            hashStore = "";
        }


        if (!readyPage) {
            readyPage = true;
            tabSet();
        }

        contractList();
    } else {
        $("#storList").attr('disabled', true);
        if ($("#svcList").val() == "") {
            $("#storList").html("<option value=''>"+getMessage("select.all")+"</option>");

            if (!readyPage) {
                readyPage = true;
                tabSet();
            }

            contractList();
        } else {
            $("#storList").html("");
            contractList();
            popAlertLayer(getMessage("info.store.nodata.msg"));
        }


    }
}


function tabVal(seq,e){
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
    $(e).parent("div").removeClass("tabOff").addClass("tabOn").css({ 'pointer-events': 'none' });

    selTab = seq;

    $("#target").val("");
    $("#selectBox").val("");
    $("#cpList").val("");
    $("#svcList").val("");
    $("#storList").val("");
    $("#keyword").val("");

    totalPage = 0;
    if ($("#storList option").length == 0 && selTab == 1) {
        $("#storList").html("<option value=''>"+getMessage("select.all")+"</option>");
        $("#storList").css("disabled",true);
    } else {
        $("#storList").css("disabled",false);
    }

    $('#jqgridData').jqGrid('setGridParam', {jsonReader :""});
    jQuery("#jqgridData").jqGrid('GridUnload');
    tabSet();
    $("#target").html("");
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
}

function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {
        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "cpNm":break;
            case "svcNm":break;
            case "storNm":break;
            default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $(".target_bak").remove();
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

modelSet = function(){
    var ccm = $("#jqgridData").getGridParam();

    tabSet();

    $("#jqgridData").setGridWidth($("#contents").width());
    $("#jqgridData").jqGrid('setGridParam',ccm);

    if (selTab == 0) {
        $("#jqgridData").jqGrid("showCol", ["cb"]);
    } else if (selTab == 2) {
        $("#jqgridData").jqGrid("hideCol", ["cb"]);
    }

    $("#jqgridData").setGridWidth($("#contents").width());
    $("#jqgridData").jqGrid('setGridParam',ccm);
}

var option_common = {
    url:apiUrl,
    datatype: "json", // 데이터 타입 지정
    jsonReader : {
        page: "result.currentPage",
        total: "result.totalPage",
        root: "result.cpList",
        records: "result.totalCount",
        repeatitems: false
    },
    colNames:[getMessage("column.title.num"),getMessage("contents.cpNm"),getMessage("column.admission.count"),getMessage("column.billing.inputYn"),getMessage("contents.contract.price"),getMessage("table.updatedate"),getMessage("column.title.cpSeq")],
    colModel: [
        {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:true},
        {name:"cpNm", index:"CP_NM", align:"center",sortable:true,formatter:pointercursor},
        {name:"contsCount", index:"CONTS_COUNT", align:"center",sortable:true},
        {name:"count", index:"COUNT", align:"center",sortable:true},
        {name:"totPrice", index:"TOT_PRICE", align:"center",sortable:true},
        {name:"buyinAmdDt", index:"BUYIN_AMD_DT", align:"center",sortable:true},
        {name:"cpSeq", index:"CP_SEQ", hidden:true,sortable:true}
   ],
    sortorder: "desc",
    height: "auto",
    autowidth: true, // width 자동 지정 여부
    rowNum: 10, // 페이지에 출력될 칼럼 개수
    pager: "#pageDiv", // 페이징 할 부분 지정
    viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
    sortname: "VRF_RQT_DT",
    multiselect: false,
    beforeRequest:function(){
        tempState = gridState;
        var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

        if (gridState == "TABVAL") {
            searchField = "";
            searchString = "";
        } else if (gridState != "SEARCH") {
            $("#target").val(searchField);
            $("#keyword").val(searchString);
        }

        if (isNaN(page)) {
           page =1;
        }

        searchField = checkUndefined($("#target").val());
        searchString = checkUndefined($("#keyword").val());
        hashCp = checkUndefined($("#cpList").val());
        hashStore = checkUndefined($("#storList").val());
        hashSvc = checkUndefined($("#svcList").val());
/*
        if (hashVerify == "" || hashVerify == "verify" || hashVerify == null) {
            hashVerify = "verify";
            apiUrl = makeAPIUrl("/api/contents");
            selTab = 0;
        } else if (hashVerify == "stat") {
            apiUrl = makeAPIUrl("/api/verifyRecord");
            hashSvc = "";
            selTab = 2;
        } else {
            apiUrl = makeAPIUrl("/api/contents");
            hashSvc = "";
            selTab = 1;
        }
*/
        modelSet();

        $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
        $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

        if (gridState == "HASH"){
            myPostData.page = page;
            tempState = "READY";
        } else {
            if(tempState == "SEARCH"){
               myPostData.page = 1;
            } else {
                tempState = "";
            }
        }

        if(gridState == "NONE"){
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            myPostData.page = page;
            if (searchField != null) {
                tempState = "SEARCH";
            } else {
                tempState = "";
            }
        }

        if(searchField != null && searchField != "" && searchString != ""){
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
        }else{
            delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
        }

        gridState = "GRIDBEGIN";
        if (hashCp != null && hashCp != "") {
            myPostData.cpSeq = hashCp;
        } else {
            myPostData.cpSeq = "";
        }
        if (hashSvc != null && hashSvc != "") {
            myPostData.svcSeq = hashSvc;
        } else {
            myPostData.svcSeq = "";
        }

        if (hashStore != null && hashStore != "") {
            myPostData.storSeq = hashStore;
        } else {
            myPostData.storSeq = "";
        }

        switch(selTab) {
        case 0:
            if ($("#cpList option").length == 0){
                noData = true;
            } else {
                noData = false;
            }
            break;
        case 1:
            if ($("#storList option").length == 0){
                noData = true;
            } else {
                noData = false;
            }
            break;
        }

        if (noData) {
            myPostData.page = 0;
            hashCp = "";
            switch(selTab) {
            case 0:
                popAlertLayer(getMessage("info.cp.nodata.msg"));
                break;
            case 1:

                break;
            }
        }

        $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page });
    },
    loadComplete: function (data) {
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));
        var pageMove = false;

        //session
        if(sessionStorage.getItem("last-url") != null){
            sessionStorage.removeItem('state');
            sessionStorage.removeItem('last-url');
        }

        //Search 필드 등록
        searchFieldSet();

        if (data.resultCode == 1000) {
            totalPage = data.result.totalPage;
            if (totalPage < page) {
                page = 1;
                pageMove = true;
            }

            if (page != data.result.currentPage) {
                tempState = "";
            }
        } else {
            tempState = "";
            if (page != 1) {
                page = 1;
                pageMove = true;
            }
        }

        if ((isNaN(page) || noData) && tempState != "") {
            page =1;
            pageMove = true;
        }


        /*뒤로가기*/
        var hashlocation;

        if (!pageMove) {
            page = $(this).context.p.page;
        }

        hashlocation = "page="+page+"&pageType="+hashTab;

        if (hashTab == "billing") {
            hashlocation += "&svcSeq="+hashSvc + "&storSeq="+hashStore;
        } else {
            hashlocation += "&cpSeq="+hashCp;

        }


        if($(this).context.p.postData._search && $("#target").val() != null){
            hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
        }

        gridState = "GRIDCOMPLETE";

        document.location.hash = hashlocation;


        if ( (tempState != ""  && tempState != "SEARCH") || str_hash == hashlocation) {
            gridState = "READY";
        }

        if (pageMove) {
            gridState = "HASH";
        }

        $(this).find("td").each(function(){
            if ($(this).index() == 3) {
                var str = $(this).text();
                if (str != '') {
                    if (str == "0") {
                        str = "N (" + str + getMessage("common.case")+")";
                    } else {
                        str = "Y (" + str + getMessage("common.case")+")";
                    }
                    $(this).text(str);
                }
            } else if ($(this).index() == 4) {
                var str = $(this).text();
                str =str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
                if (str != "") {
                    str += " " + getMessage("common.price.unit");
                }
                $(this).text(str);
            } if ($(this).index() == 5) { // 구분
            }
        })

      //pageMove Max
        $('.ui-pg-input').on('keyup', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
        });

        $(this).find(".pointer").parent("td").addClass("pointer");
    },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
    onCellSelect: function(rowId, columnId, cellValue, event){
        if (columnId == 1 && hashTab == "contract") {
            var list = $("#jqgridData").jqGrid('getRowData', rowId);
            sessionStorage.setItem("last-url", location);
            sessionStorage.setItem("state", "view");

            pageMove("/contract/"+list.cpSeq);
        }else if(columnId == 2 && hashTab == "billing"){
            var list = $("#jqgridData").jqGrid('getRowData', rowId);
            sessionStorage.setItem("last-url", location);
            sessionStorage.setItem("state", "view");

            pageMove("/billing/"+list.storSeq);
        }
    }
}

tabSet = function(){
    switch(selTab){
    case 0:
        option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.cpList",
            records: "result.totalCount",
            repeatitems: false
        };
        option_common['colNames'] = [getMessage("column.title.num"),getMessage("contents.cpNm"),getMessage("column.admission.count"),getMessage("column.billing.inputYn"),getMessage("contents.contract.price"),getMessage("table.updatedate"),getMessage("column.title.cpSeq")];
        $(".tab").eq(0).show();
        $(".tab").eq(1).hide();
        $(".rsvSelect#cp").show();
        $(".rsvSelect:not(#cp)").hide();
        hashTab = "contract";
        apiUrl = makeAPIUrl("/api/contract");
        option_common['url'] = makeAPIUrl("/api/contract");
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:true},
            {name:"cpNm", index:"CP_NM", align:"center",sortable:true,formatter:pointercursor},
            {name:"contsCount", index:"CONTS_COUNT", align:"center",sortable:true},
            {name:"count", index:"COUNT", align:"center",sortable:true},
            {name:"totPrice", index:"TOT_PRICE", align:"center",sortable:true},
            {name:"buyinAmdDt", index:"BUYIN_AMD_DT", align:"center",sortable:true},
            {name:"cpSeq", index:"CP_SEQ", hidden:true,sortable:true}
        ];
        break;
    case 1:
        $(".rsvSelect#cp").hide();
        $(".rsvSelect:not(#cp)").show();
        option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.billingList",
            records: "result.totalCount",
            repeatitems: false
        };
        option_common['colNames'] = [getMessage("column.title.num"), getMessage("store.comn.serviceNm"), getMessage("store.comn.name"), getMessage("column.billing.inputYn"), getMessage("billing.realUse.price"), getMessage("table.updatedate"), getMessage("column.title.svcSeq"), getMessage("column.title.storSeq") ];
        $(".tab").eq(0).hide();
        $(".tab").eq(1).show();
        hashTab = "billing";
        apiUrl = makeAPIUrl("/api/billing");
        option_common['url'] = makeAPIUrl("/api/billing");
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:true},
            {name:"svcNm", index:"SVC_NM", align:"center",sortable:true},
            {name:"storNm", index:"STOR_NM", align:"center",sortable:true,formatter:pointercursor},
            {name:"count", index:"COUNT", align:"center",sortable:true},
            {name:"totPrice", index:"TOT_PRICE", align:"center",sortable:true},
            {name:"regDt", index:"REG_DT", align:"center",sortable:true},
            {name:"svcSeq", index:"SVC_SEQ", hidden:true,sortable:true},
            {name:"storSeq", index:"STOR_SEQ", hidden:true,sortable:true}
        ];
        break;
    }

    option_common['rowNum'] = $("#limit").val();

}

contractList = function(){
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}


deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}
