/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var readyPage = false;
var apiUrl = makeAPIUrl("/api/billingDetail");
var lastBind = false;
var gridState;
var tempState;
var totalPage = 0;
var noData = false;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
})
$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (noData) {
            return;
        }

        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");

            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            $("#target").val(searchField);
            $("#keyword").val(searchString);

            jQuery("#jqgridData").trigger("reloadGrid");

        } else {
            gridState = "READY";
        }
    });
})
$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });

    listBack($(".btnCancelView"));

    contractList();
});


function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {
        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "ratContNo":
                searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
                break;
            case "contsTitle":
                searchHtml += "<option value=\"CONTS_TITLE\">"+getMessage("table.contentsTitle")+"</option>";
                break;
            default:continue;
            }

        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $(".target_bak").remove();
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

var option_common = {
    url:apiUrl,
    datatype: "json", // 데이터 타입 지정
    jsonReader : {
        page: "result.currentPage",
        total: "result.totalPage",
        root: "result.billingDetailList",
        records: "result.totalCount",
        repeatitems: false
    },
    colNames:[
        getMessage("column.title.num"),
        getMessage("contents.contract.no"),
        getMessage("contents.contractCnt"),
        getMessage("contents.contract.form"),
        getMessage("contents.contract.date"),
        getMessage("contents.contract.context"),
        getMessage("contents.contract.price"),
        getMessage("table.reger"),
        getMessage("table.updatedate"),
        getMessage("billing.useYn"),
        getMessage("contents.contract.price"),
        getMessage("column.title.contsCount"),
        getMessage("common.useCount"),
        getMessage("contents.contract.no")
    ],
    colModel: [
        {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:true},
        {name:"ratContNo", index:"RAT_CONT_NO", align:"center",sortable:true},
        {name:"contsTitle", index:"CONTS_COUNT", align:"center",sortable:true,classes : 'ui-ellipsis',fixed: true,width:200},
        {name:"ratContType", index:"RAT_CONT_TYPE", align:"center",sortable:true,cellattr: typeTitleAttr},
        {name:"regDt", index:"REG_DT", align:"center",sortable:true},
        {name:"setVal", index:"SET_VAL", align:"center",sortable:true,cellattr: setValTitleAttr},
        {name:"usePrice", index:"USE_PRICE", align:"center",sortable:true,cellattr: usePriceTitleAttr},
        {name:"cretrNm", index:"CRETR_NM", align:"center",sortable:true},
        {name:"amdDt", index:"AMD_DT", align:"center",sortable:true},
        {name:"ratYn", index:"RAT_YN", hidden:true,sortable:true},
        {name:"totPrice", index:"TOT_PRICE", hidden:true,sortable:true},
        {name:"contsCount", index:"CONTS_COUNT", hidden:true,sortable:true},
        {name:"useCount", index:"USE_COUNT", hidden:true,sortable:true},
        {name:"ratSeq", index:"RAT_SEQ", hidden:true,sortable:true}
   ],
    sortorder: "desc",
    height: "auto",
    autowidth: true, // width 자동 지정 여부
    rowNum: 10, // 페이지에 출력될 칼럼 개수
    pager: "#pageDiv", // 페이징 할 부분 지정
    viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
    sortname: "RAT_SEQ",
    multiselect: false,
    beforeRequest:function(){
        tempState = gridState;
        var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

        if (gridState != "SEARCH") {
            $("#target").val(searchField);
            $("#keyword").val(searchString);
        }

        if (isNaN(page)) {
           page =1;
        }

        searchField = checkUndefined($("#target").val());
        searchString = checkUndefined($("#keyword").val());

        if (gridState == "HASH"){
            myPostData.page = page;
            tempState = "READY";
        } else {
            if(tempState == "SEARCH"){
               myPostData.page = 1;
            } else {
                tempState = "";
            }
        }

        if(gridState == "NONE"){
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            myPostData.page = page;
            if (searchField != null) {
                tempState = "SEARCH";
            } else {
                tempState = "";
            }
        }

        if(searchField != null && searchField != "" && searchString != ""){
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
        }else{
            delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
        }

        myPostData.storSeq = $("#storSeq").val();

        gridState = "GRIDBEGIN";

        if (noData) {
            myPostData.page = 0;
            popAlertLayer(getMessage("info.cp.nodata.msg"));
        }

        $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page,storSeq: myPostData.storSeq });
    },
    loadComplete: function (data) {
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));
        var pageMove = false;

        //session
        if(sessionStorage.getItem("last-url") != null){
            sessionStorage.removeItem('state');
            sessionStorage.removeItem('last-url');
        }

        //Search 필드 등록
        searchFieldSet();

        if ($("#marketNm").text() == "") {
            $("#marketNm").text(data.result.marketNm);
        }

        if (data.resultCode == 1000) {
            totalPage = data.result.totalPage;
            if (totalPage < page) {
                page = 1;
                pageMove = true;
            }

            if (page != data.result.currentPage) {
                tempState = "";
            }

        } else {
            tempState = "";
            if (page != 1) {
                page = 1;
                pageMove = true;
            }
        }

        if ((isNaN(page) || noData) && tempState != "") {
            page =1;
            pageMove = true;
        }


        /*뒤로가기*/
        var hashlocation;
        if (pageMove) {
            hashlocation = "page="+page;
        } else {
            hashlocation = "page="+$(this).context.p.page;
        }


        if($(this).context.p.postData._search && $("#target").val() != null){
            hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
        }

        gridState = "GRIDCOMPLETE";

        document.location.hash = hashlocation;


        if ( (tempState != ""  && tempState != "SEARCH") || str_hash == hashlocation) {
            gridState = "READY";
        }

        if (pageMove) {
            gridState = "HASH";
        }

        $(this).find("td").each(function(){
            var rowId = $(this).parents("tr").attr("id");
            var list = $("#jqgridData").jqGrid('getRowData', rowId);
            if (typeof rowId == "undefined") {
                return;
            }
            if ($(this).index() == 2){
                var str = list.contsCount+getMessage("common.count");
                if (parseInt(list.contsCount) > 0) {
                    str += "(" + list.contsTitle +")";
                }
                $(this).html("<span class='pointer'>"+str+"</span>");
            } else if ($(this).index() == 3) {
                var str = "";

                if (list.ratYn == "N") {
                    str = getMessage("billing.type.free");
                } else if (list.ratContType == "T"){
                    str = getMessage("billing.Hourly");
                } else if (list.ratContType == "C") {
                    str = getMessage("excute.count.limit.noSpace");
                } else {
                    str = getMessage("billing.type.lump");
                }

                $(this).text(str);
            } else if ($(this).index() == 4) {
                $(this).html($(this).text().replace(" ~ ","<br /> ~"));
            } else if ($(this).index() == 5) {
                var str = $(this).text();
                var price = list.totPrice;
                price =price.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
                if (price != "") {
                    price += " " + getMessage("common.price.unit");
                }
                if (list.ratYn == "N") {
                    str = "-";
                } else if (list.ratContType == "T" || list.ratContType == getMessage("billing.Hourly")){
                    str += getMessage("common.minute") + ", " + price;
                } else if (list.ratContType == "C" || list.ratContType == getMessage("excute.count.limit.noSpace")){
                    str += getMessage("common.case") +  ", "+ price;
                } else {
                    str = price;
                }

                $(this).text(str);

            } else if ($(this).index() == 6) {
                var str = $(this).text();
                str =str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
                if (str != "") {
                    str += " " + getMessage("common.price.unit");
                }


                if (list.ratYn == "N") {
                    str = "-";
                } else if (list.ratContType == "T" || list.ratContType == getMessage("billing.Hourly")){

                } else if (list.ratContType == "C" || list.ratContType == getMessage("excute.count.limit.noSpace")){
                    if (list.useCount > list.setVal) {
                        $(this).css("color","red");
                    }
                } else {
                    str = "-";
                }


                $(this).text(str);
            }
        });

      //pageMove Max
        $('.ui-pg-input').on('keyup', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
        });

        $(this).find(".pointer").parent("td").addClass("pointer");
    },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
    onCellSelect: function(rowId, columnId, cellValue, event){
        if (columnId == 2) {
            var list = $("#jqgridData").jqGrid('getRowData', rowId);
            sessionStorage.setItem("last-url", location);
            sessionStorage.setItem("state", "view");

            pageMove("/billingDetail/"+list.ratSeq);
        }
    }
}

function typeTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";

    if (rawObject.ratYn == "N") {
        retVal = getMessage("billing.type.free");
    } else if (rawObject.ratContType == "T"){
        retVal = getMessage("billing.Hourly");
    } else if (rawObject.ratContType == "C") {
        retVal = getMessage("excute.count.limit.noSpace");
    } else {
        retVal = getMessage("billing.type.lump");
    }

    return 'title="' + retVal  + '"';
}


function setValTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = String(rawObject.setVal);
    var price = String(rawObject.totPrice);
    price =price.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    if (price != "") {
        price += " " + getMessage("common.price.unit");
    }
    if (rawObject.ratYn == "N") {
        retVal = "-";
    } else if (rawObject.ratContType == "T" || rawObject.ratContType == getMessage("billing.Hourly")){
        retVal += getMessage("common.minute") + ", " + price;
    } else if (rawObject.ratContType == "C" || rawObject.ratContType == getMessage("excute.count.limit.noSpace")){
        retVal += getMessage("common.case") +  ", "+ price;
    } else {
        retVal = price;
    }

    return 'title="' + retVal  + '"';
}

function usePriceTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = String(rawObject.usePrice);
    retVal =retVal.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');

    if (retVal != "") {
        retVal += " " + getMessage("common.price.unit");
    }

    if (rawObject.ratYn == "N") {
        retVal = "-";
    } else if (rawObject.ratContType == "T" || rawObject.ratContType == getMessage("billing.Hourly")){

    } else if (rawObject.ratContType == "C" || rawObject.ratContType == getMessage("excute.count.limit.noSpace")){

    } else {
        retVal = "-";
    }

    return 'title="' + retVal  + '"';
}


contractList = function(){
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}


