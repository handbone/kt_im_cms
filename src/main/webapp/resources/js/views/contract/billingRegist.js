/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var arr = new Array();
var totalCount;
var addList = new Array();
$(document).ready(function(){
    var now=new Date();
    listBackConfirmRegist($(".btnCancelView"));
    $("#ratContStDt").val(dateString((new Date(now.getTime()))));
    $("#ratContFnsDt").val(dateString((new Date(now.getTime()))));

    $(".remainingPrc").keyup(function(e) {
        $(this).val($(this).val().replace(/[^\d]+/g, ''));
        $(this).val(numberWithCommas($(this).val()));
    });

    $("#ratContStDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#ratContFnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#ratContFnsDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#ratContStDt").datepicker("option","maxDate", selected)
        }
    });

    setkeyup();

    checkboxChange();

    $('.remaining').each(function() {
        var $maximumCount = $(this).attr("max") * 1;
        var $input = $(this);
        var update = function() {
            var now = $maximumCount - $input.val().length;
            // 사용자가 입력한 값이 제한 값을 초과하는지를 검사한다.
            if (now < 0) {
                var str = $input.val();
                popAlertLayer(getMessage("common.input.max.limit.first.msg") + ' ' + $maximumCount + getMessage("common.input.max.limit.second.msg"));
                $input.val(str.substr(0, $maximumCount));
                $input.val($input.val().replace(/[\<>&\"']/gi, ''));
                now = 0;
            }
        }
        $input.bind('input keyup paste', function() {
            setTimeout(update, 0)
        });
        update();
    });


    /* list 클릭 시, 옵션값 변경*/
    $("body").click(function( event ) {
        var table = $(event.target).parents("table");
        var tContent = $(event.target).parent().parent();
        if(table.hasClass("listBox") == false){
            return;
        }
        if(event.target.nodeName == "TD" && tContent.is("tbody") || ($(event.target).parents("td").hasClass("chkBox") && !$(event.target).hasClass("btnSmallGray")) || ($(event.target).parents("td").hasClass("contsTitle") && !$(event.target).hasClass("btnSmallGray"))){
            var target = $(event.target).parents("tr").toggleClass("checked");
            var type = false;
            if(target.hasClass("checked")) type = true;
            $(target).find("input").attr("checked",type);
        }else{
            if(tContent.is("thead")){
                table.find("input.allcheck").trigger("click");
            }
        }
    });


    $("input[name=ratYn]").change(function(){
        timeSetChk();
        if ($(this).val() == "Y") {
            $(".content").find("input:not(#ratContNo):not(#cntSeachBtn):not(#ratContStDt):not(#ratContFnsDt):not([name=ratYn]):not(.btnNormal)").attr( 'disabled', false );
        } else {
            $(".content").find("input:disabled").attr('disabled',false);
            $(".content").find("input:not(#ratContNo):not(#cntSeachBtn):not(#ratContStDt):not(#ratContFnsDt):not([name=ratYn]):not(.btnNormal)").attr( 'disabled', true );
            $("#runLmtCnt").val(null);
            $("#runPerPrc").val(null);
            $("#ratTime").val(null);
            $("#ratPrc").val(null);
            $("#lmsmpyPrc").val(null);
        }
    });

    $("input[name=ratCntYn]").change(function(){
        if ($("input[name=ratCntYn]:checked").val() == "Y") {
            $("input[name=ratTimeYn][value=N]").attr("checked",true);
            $("input[name=ratLumpYn][value=N]").attr("checked",true);
        } else {
            $("#runLmtCnt").val(null);
            $("#runPerPrc").val(null);
        }
        $("input[name=ratTimeYn][value=N]").attr("checked",true);
        $("input[name=ratLumpYn][value=N]").attr("checked",true);
        $("#ratTime").val(null);
        $("#ratPrc").val(null);
        $("#lmsmpyPrc").val(null);
        timeSetChk();
    });

    $("input[name=ratTimeYn]").change(function(){
        if ($("input[name=ratTimeYn]:checked").val() == "Y") {
            $("input[name=ratCntYn][value=N]").attr("checked",true);
            $("input[name=ratLumpYn][value=N]").attr("checked",true);
            $("#runLmtCnt").val(null);
            $("#runPerPrc").val(null);
            $("#lmsmpyPrc").val(null);
        } else {
            $("#ratTime").val(null);
            $("#ratPrc").val(null);
        }
        timeSetChk();
    });

    $("input[name=ratLumpYn]").change(function(){
        if ($("input[name=ratLumpYn]:checked").val() == "Y") {
            $("input[name=ratTimeYn][value=N]").attr("checked",true);
            $("input[name=ratCntYn][value=N]").attr("checked",true);
            $("#runLmtCnt").val(null);
            $("#runPerPrc").val(null);
            $("#ratTime").val(null);
            $("#ratPrc").val(null);
        } else {
            $("#lmsmpyPrc").val(null);
        }
        timeSetChk();
    });



    storInfo();
    storConsList();
});


/*
 * 콘텐츠 목록 체크박스 또는 client 콘텐츠 목록이 변경 될 경우
 * parameter: type(구분자-content, client 콘텐츠)
 * return: x
 */
function checkboxChange(type){
    var allCheck = false;

    // 콘텐츠 목록 / client 콘텐츠 목록 체크 박스 바뀔 경우
    if(type == "contents" || type == "vrContents"){
        allCheck = true;
        $("input:checkbox[name="+type+"Seq]").each(function(){
            if(!this.checked){
                allCheck = false;
                return;
            }
        });
    }
    if(allCheck){
        $("#"+type+"AllCb").attr("checked", true);
    } else{
        $("#"+type+"AllCb").attr("checked", false);
    }
}



/*
* 콘텐츠 목록 / client 콘텐츠 전체 체크 변경될 경우
* parameter: type(구분자-content, client 콘텐츠)
* return: x
*/
function checkboxAll(type){
    if($("#"+type+"AllCb").is(":checked")){
        $("input:checkbox[name="+type+"Seq]").attr("checked", true).parents("tr").addClass("checked");
        $(".hide").removeClass("checked");
    } else {
        $("input:checkbox[name="+type+"Seq]").attr("checked", false).parents("tr").removeClass("checked");
    }
}


timeSetChk = function(){
    $(".content").find("input[type=radio][value=N]:checked").parents("tr").find("input:not([type=radio])").attr('disabled', true);
    $(".content").find("input[type=radio][value=Y]:checked").parents("tr").find("input:not([type=radio])").attr('disabled', false);


    if ($("input[name=ratTimeYn]:checked").val() == "Y" && $("input[name=ratYn]:checked").val() == "Y") {
        $(".content").find("#ratTime").attr('disabled', false);
    } else {
        $(".content").find("#ratTime").attr('disabled', true);
    }

}


storInfo = function(){
    callByGet("/api/store?storSeq="+$("#storSeq").val(),"storInfoSuccess","NoneForm");
}

storInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        $("#svcSeq").val(data.result.storeInfo.svcSeq);
        $("#svcNm").val(data.result.storeInfo.svcNm);
        $("#storNm").val(data.result.storeInfo.storNm);
    } else {
        popAlertLayer(getMessage("common.bad.request.msg"), pageMove("/contract"));
    }
}

storConsList = function(){
    callByGet("/api/contents?sttus=05&rows=10000&delYn=N&cntrctFnsDt=NOW&svcSeq="+$("#svcSeq").val(),"storConsListSuccess","NoneForm");
}


var noData = false;
function storConsListSuccess(data){
    arr = new Array();

    if(data.resultCode == "1000"){
        var contentsHtml = "";

        totalCount = data.result.totalCount;

        $(data.result.contentsList).each(function(i,item){
            contentsHtml += "<tr id=\"contentDetail"+(i+1)+"\"><td class=\"chkBox width50\"><input type=\"checkbox\" value=\""+item.contsSeq+"\" name=\"contsSeq\" class=\"d_block\"></td>";
            contentsHtml += "<td class=\"contsTitle\"><div class='ellipsis' style='width:100%'>"+item.contsTitle+"</div></td>";
            contentsHtml += "<td class=\"itemW30 contsCtgNm align_l\">["+item.firstCtgNm+"]"+item.secondCtgNm+"</td>";
            contentsHtml += "<td class=\"cretDt itemW20\">"+item.cretDt+"<span class='cntrctFnsDt d_none'>"+item.cntrctFnsDt+"</span></td></tr>";
            arr.push({contsTitle: item.contsTitle,contsSeq: item.contsSeq,firstCtgNm:item.firstCtgNm,secondCtgNm : item.secondCtgNm,cntrctFnsDt : item.cntrctFnsDt});
        });

        $("#selectContents").html(contentsHtml);
        checkScroll();

    }else{
        noData = true;
        popAlertLayer(getMessage("cms.release.msg.noData.conts"));
        $("#selectContents").html("");
        $("#selectContents").html("<tr class='noData'><td colspan=3 align=center height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
        $("#selectVRContents").html("");
        $("#selectVRContents").html("<tr class='noData'><td colspan=3 align=center height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
        return;
    }
    noData = false;
    checkContent();
}

setList = function(){
    var str = "";
    addList = new Array();
    $(".chkBox").find("input").attr("checked",false);
    $(".checked").removeClass("checked");
    $("#selectVRContents tr.rowConts").each(function(){
        if (str != "") {
            str += ",";
        }
        str += $(this).find(".contsTitle").text();


        if ($(this).css("display") != "none") {
            addList.push($(this).find("input").val());
        }
    });

    $("#contsTitle").val(str);


}
resetList = function(){
    $(".chkBox").find("input").attr("checked",false);
    $(".checked").removeClass("checked");


    setTimeout(function(){
        $("#selectVRContents tr.rowConts").each(function(){
            if (jQuery.inArray($(this).find("input").val(), addList ) == -1){
                $(this).find("input[name=groupContentsSeq]").attr("checked",true);
                $(this).addClass("checked");
            }
        });

        $(".noData").remove();

        $("input:checkbox[name=groupContentsSeq]:checked").each(function(){
            $("input[type=checkbox][value="+this.value+"]").parents("tr").removeClass("hide").show();
            $("input[type=checkbox][value="+this.value+"]").prop("checked",false);
            this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
        });
        $("#groupContentsAllCb").attr("checked", false);
        if($("#selectVRContents").find("tr:not(.noData):not(.hide)").text() == ""){
            $("#selectVRContents").html("<tr class='noData'><td colspan=3 align=center height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
        }

        $("#selectContents tr:not(.noData):not(.hide)").each(function(){
            if (jQuery.inArray($(this).find("input").val(), addList ) != -1){
                $(this).find("input[name=contsSeq]").attr("checked",true);
                $(this).addClass("checked");
            }
        });
        insertContentsInfo();

        addList = new Array();
    }, 400);
}
//해당 VR에 컨텐츠 정보 삭제
function deleteContentsInfo(){
    if (noData) {
        return;
    }

    $(".noData").remove();

    $("input:checkbox[name=groupContentsSeq]:checked").each(function(){
        $("input[type=checkbox][value="+this.value+"]").parents("tr").removeClass("hide").show();
        $("input[type=checkbox][value="+this.value+"]").prop("checked",false);
        this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
    });
    $("#groupContentsAllCb").attr("checked", false);
    if($("#selectVRContents").find("tr:not(.noData):not(.hide)").text() == ""){
        $("#selectVRContents").html("<tr class='noData'><td colspan=3 align=center height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
    }

    checkScroll();
}


//해당 VR에 컨텐츠 정보 등록
function insertContentsInfo(){
    $("#selectVRContents").find("tr").each(function(){
        if ($(this).text() == "") {
            $(this).remove();
        }
    });

    var VRContentsHtml = "";
    var contsSeq = "";
    var contsTitle = "";
    var ctgNm ="";
    var cretDt = "";
    var cntrctFnsDt = "";
    var insertFlag;
    for(var num=1; num<totalCount+1; num++){
        if($("#contentDetail"+num+" input:checkbox:checked").val()){
            insertFlag = true;
            contsSeq = $("#contentDetail"+num+" input:checkbox").val();
            contsTitle = $("#contentDetail"+num+" .contsTitle .ellipsis").html();
            ctgNm = $("#contentDetail"+num+" .contsCtgNm").html();
            cretDt = $("#contentDetail"+num+" .cretDt").html();
            cntrctFnsDt = $("#contentDetail"+num+" .cntrctFnsDt").html();

            $("input:checkbox[name=groupContentsSeq]").each(function(){
                if(contsSeq == $(this).val()){
                    insertFlag = false;
                    return;
                }
            });

            if(insertFlag){
                VRContentsHtml += "<tr class='rowConts'><td class=\"chkBox width50\"><input type=\"checkbox\" value=\""+contsSeq+"\" name=\"groupContentsSeq\" class=\"d_block\"></td>";
                VRContentsHtml += "<td class=\"contsTitle\">"+contsTitle+"</td>";
                VRContentsHtml += "<td class=\"itemW40 contsCtgNm align_l\">"+ctgNm+"<span class='cntrctFnsDt d_none'>"+cntrctFnsDt+"</span></td>";
            }
        }
    }
    $("#selectContents > .checked").hide().removeClass("checked").addClass("hide");
    if (VRContentsHtml == "" && $("#selectVRContents").find(".noData").index() == 0) {
        return;
    } else {
        $(".noData").remove();
        if($("#selectVRContents").find("tr").text() == getMessage("cms.release.msg.noData") && VRContentsHtml.length > 0){
            $("#selectVRContents").html("");
         }

        $("#contsAllCb").prop("checked",false);
        $("#selectVRContents").append(VRContentsHtml);
    }

    checkScroll();

}

function checkContent(){
    $(".noData").remove();
    $("tbody tr").each(function(){
        if ($(this).html() == "") {
            $(this).remove();}
        }
    );
    $("#selectVRContents tr.rowConts").each(function(){
        var obj = $("#selectContents input[value='"+$(this).find(".chkBox input").val()+"']");
        if ($(obj).index() == -1) {
            $(this).addClass("hide").hide();
        } else {
            $(this).removeClass("hide").show();
        }
    });

    $("#selectVRContents tr.rowConts").each(function(){
        $("#selectContents input[value='"+$(this).find(".chkBox input").val()+"']").prop("checked",false).parents("tr").addClass("hide").hide();
    });

    if($("#selectVRContents tr.rowConts").length == $("#selectVRContents .hide").length){
        $("#selectVRContents").append("<tr class='noData'><td colspan=3 align=center height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
    }

    checkScroll();
}


function checkScroll(){
    $(".scroll-table").each(function(i){
        if (i == 0) {
            console.log($(this).width());
            if ($(this).find(".tableList").height() > 311) {
                $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"126px"});
                $(this).find(".tableList").children("thead").find("th:eq(3)").css({"width":"84px","padding-right":"16px"});
            } else {
                $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"30%"});
                $(this).find(".tableList").children("thead").find("th:eq(3)").css({"width":"20%","padding-right":"0px"});
            }
        } else {
            if ($(this).find(".tableList").height() > 311) {
                $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"168px","padding-right":"16px"});
            } else {
                $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"40%","padding-right":"0px"})
            }
        }

    })
}


searchOnConts = function(){
    if (noData) {
        return;
    }

    var searchConts = $("#searchKeyword").val();
    var filter = searchConts.toUpperCase();

    $("#selectContents").find("tr").each(function(){
        if (!$(this).hasClass("hide")) {
            if ($(this).find(".ellipsis").text().toUpperCase().indexOf(filter) > -1) {
                $(this).css("display","inline-table");
            } else {
                $(this).css("display","none");
            }
        }
    })

    checkScroll();
}

function searchOnContsReset() {
    $("#searchKeyword").val("");
    searchOnConts();
}

ratContNoChk = function(){
    callByGet("/api/store?storSeq="+$("#storSeq").val()+"&ratContNo="+$.trim($("#ratContNo").val()),"ratContNoChkSuccess","NoneForm");
}

ratContNoChkSuccess = function(data){
    if(data.resultCode == "1000"){
        if (data.result.storeInfo.ratContCnt == 0) {
            billingRegistPost();
        } else {
            popAlertLayer(getMessage("msg.overlay.contractNo"));
        }
    } else {
        popAlertLayer(getMessage("common.bad.request.msg"),pageMove("/contract"));
    }
}




function billingRegist(){
    var str = $.trim($("#ratContNo").val());

    if (str == "") {
        popAlertLayer(getMessage("msg.input.BuyinNo"));
        return;
    }

    ratContNoChk();
}

billingRegistPost = function(){
    if ($("#contsTitle").val() == "") {
        popAlertLayer(getMessage("msg.contract.selConts"));
        return;
    }


    if (typeof $("input[name=ratYn]:checked").val() == "undefined") {
        popAlertLayer(getMessage("msg.sel.ratYn"));
        return;
    }

    if ($("#ratYn_Y").attr("checked") == "checked" && $("#ratLumpYn_Y").attr("checked") != "checked" && $("#ratCntYn_Y").attr("checked") != "checked" &&$("#ratTimeYn_Y").attr("checked") != "checked") {
        popAlertLayer(getMessage("msg.sel.buyinType"));
        return;
    }
    var selContType;
    var selContTypeVal;
    if ($("#ratYn_N").attr("checked") == "checked") {
        selContType = "None";
        selContTypeVal = "N";
    } else if ($("input[name=ratCntYn]:checked").val() == "Y") {
        selContType = "ratCntYn";
        selContTypeVal = "C";
    } else if ($("input[name=ratTimeYn]:checked").val() == "Y") {
        selContType = "ratTimeYn";
        selContTypeVal = "T";
    } else if ($("input[name=ratLumpYn]:checked").val() == "Y") {
        selContType = "ratLumpYn";
        selContTypeVal = "L";
    } else {
        selContType = "None";
        selContTypeVal = "N";
    }

    if (selContType != "None") {
        if($("input[name="+selContType+"]").parents("tr").find("input[value=]").length >= 1) {
            popAlertLayer(getMessage("msg.input.buyinInfo"));
            return;
        } else if (selContType == "ratTimeYn" && $("#ratTime option:selected").val() == "0") {
            popAlertLayer(getMessage("msg.sel.ratTime"));
            return;
        }
    }


    var contsSeqs = "";
    var overDateCnt = "";
    $("#selectVRContents tr.rowConts").each(function(){
        if ($("#ratContFnsDt").val() > $(this).find(".cntrctFnsDt").text()) {
            overDateCnt += $(this).find(".contsTitle").text() + " (" +$(this).find(".cntrctFnsDt").text() +")";
            overDateCnt += "<br />";
        }

        if ($(this).css("display") != "none") {
            contsSeqs += $(this).find("input").val() + ",";
        }

    });

    if (overDateCnt != "") {
        popAlertLayer(overDateCnt + getMessage("msg.confirm.cntrctFnsDt"));
        return;
    }

    /*실행 횟수*/
    var runLmtCnt = 0;
    /*횟수당 금액*/
    var runPerPrc = 0;
    /*실행 시간*/
    var ratTime = 0;
    /*시간당 금액*/
    var ratPrc = 0;
    /*일시금 금액*/
    var lmsmpyPrc = 0;

    popConfirmLayer(getMessage('common.regist.msg'), function(){

        formData("NoneForm" , "svcSeq", $("#svcSeq").val());
        formData("NoneForm" , "storSeq", $("#storSeq").val());
        formData("NoneForm" , "ratContNo", $("#ratContNo").val());
        formData("NoneForm" , "ratYn", $("input[name=ratYn]:checked").val());
        formData("NoneForm" , "ratContStDt", $("#ratContStDt").val());
        formData("NoneForm" , "ratContFnsDt", $("#ratContFnsDt").val());
        formData("NoneForm" , "ratContType", selContTypeVal);


        switch(selContTypeVal){
        case "N"://무료
            break;
        case "C"://횟수형
            runLmtCnt = $("#runLmtCnt").val();
            runPerPrc = $("#runPerPrc").val().replace(/[^\d]+/g, '');
            break;
        case "L"://일시금 지불
            lmsmpyPrc = $("#lmsmpyPrc").val().replace(/[^\d]+/g, '');
            break;
        case "T"://시간형
            ratTime = $("#ratTime").val();
            ratPrc = $("#ratPrc").val().replace(/[^\d]+/g, '');
            break;
        }


        formData("NoneForm" , "runLmtCnt", runLmtCnt);
        formData("NoneForm" , "runPerPrc", runPerPrc);
        formData("NoneForm" , "ratTime", ratTime);
        formData("NoneForm" , "ratPrc", ratPrc);
        formData("NoneForm" , "lmsmpyPrc", lmsmpyPrc);
        formData("NoneForm" , "contsSeqs", contsSeqs.slice(0,-1));
        callByPost("/api/billingDetail" , "billingRegistSuccess", "NoneForm","InsertFail");
        $("#loading").show();
    });
}

function billingRegistSuccess(data){
    $("#loading").hide();
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.insert"), "/billing/"+$("#storSeq").val());
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }

    formDataDeleteAll("NoneForm");
}

InsertFail = function(data){
    $("#loading").hide();
    popAlertLayer(getMessage("fail.common.insert"));
    formDataDeleteAll("NoneForm");
}

function numberWithCommas(x) {
    return x.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}