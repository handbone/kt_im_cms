/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var Citem_ori;
var ratContNo_ori;
var fnsDt_ori;
$(document).ready(function(){
    billingInfo();
    $("#ratTime").attr( 'disabled', true );
    $(".content").find("input[type=text]").attr('disabled',true);
    $(".content").find("input[type=radio]").attr('disabled',true);

    listBack($(".btnList"));
});


billingInfo = function(){
    callByGet("/api/billingDetail?ratSeq="+$("#ratSeq").val(), "billingInfoSuccess", "NoneForm", "billingInfoFail");
}

billingInfoSuccess = function(data) {
    if(data.resultCode == "1000") {
        var item = data.result.billingInfo;
        var marketNm = data.result.marketNm;

        Citem_ori = data.result.billingInfo.contsList.slice();
        ratContNo_ori = item.ratContNo;
        fnsDt_ori = item.ratContFnsDt;

        $("#marketNm").text(marketNm);
        $("#ratContNo").text(item.ratContNo);
        $("#popRatContNo").text(item.ratContNo);

        var titleObj = item.contsTitle.split(",");
        var titleHtml = "";
        $(titleObj).each(function(i){
            titleHtml += "<span style='display: inline-block; float: left;'>";
            titleHtml += titleObj[i];
            if(i != titleObj.length - 1) {
                titleHtml += ",&nbsp;";
            }
            titleHtml += "</span>";
        });

        $("#contsTitle").html(titleHtml);
        $("#ratContStDt").val(item.ratContStDt);
        $("#ratContFnsDt").val(item.ratContFnsDt);
        $("input[name=ratYn][value="+item.ratYn+"]").attr("checked",true);
        $("#storSeq").val(item.storSeq);
        $("#svcSeq").val(item.svcSeq)
        if (item.ratYn == "N") {
            $("input[type=radio][value=N]").attr("checked",true);
        } else {
            $("#runLmtCnt").val(item.runLmtCnt);
            $("#runPerPrc").val(item.runPerPrc);
            $("#ratTime").val(item.ratTime);
            $("#ratPrc").val(item.ratPrc);
            $("#lmsmpyPrc").val(item.lmsmpyPrc);


            switch(item.ratContType) {
            //계약 타입 [ 횟수 ]
            case "C":
                $("input[type=radio][name=ratCntYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratCntYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            //계약 타입 [ 일시금 ]
            case "L":
                $("input[type=radio][name=ratLumpYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratLumpYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            //계약 타입 [ 시간 ]
            case "T":
                $("input[type=radio][name=ratTimeYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratTimeYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            }
        }

        $(".remainingPrc").each(function(e) {
            $(this).val($(this).val().replace(/[^\d]+/g, ''));
            $(this).val(numberWithCommas($(this).val()));
        });

    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/billing/"+$("#storSeq").val());
    }
}

billingInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/billing/"+$("#storSeq").val());
}

function numberWithCommas(x) {
    return x.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

showHistory = function(){
    $.blockUI({
        message: $('#openHistoryModify'),
        css:{border:'0px',cursor:'default',width:'auto',height:'470px',top:"calc(50% - 235px)",left:"calc(50% - 510px)"}
    });

    if (fistPopup){
        $("#openHistoryModify").data({
            'originalLeft': $("#openHistoryModify").css('left'),
            'origionalTop': $("#openHistoryModify").css('top')
        });
        fistPopup = false;
        $("#openHistoryModify").draggable();
    } else {
        $("#popupWinBig").css({
            'left': $("#openHistoryModify").data('originalLeft'),
            'top': $("#openHistoryModify").data('origionalTop')
        });
    }
    billingHistory();
}


billingHistory = function() {
    var columnNames = [
        getMessage("column.title.num"),
        getMessage("contents.contractCnt.nospace"),
        getMessage("contents.contract.form"),
        getMessage("contents.contract.date"),
        getMessage("common.content"),
        getMessage("common.amdrId.nospace"),
        getMessage("store.comn.update.date"),
        "ratYn",
        "totPrice",
        "contsTitle",
        "ratSeq",
        "ratContHstSeq"
    ];

    $("#jqgridHstData").jqGrid({
        url:makeAPIUrl("/api/billingHistory?ratSeq="+$("#ratSeq").val()),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.billingHistoryList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames:columnNames,
        colModel:[
            {name:"num", index: "num", align:"center", width:25,hidden:true,sortable:true},
            {name:"contsCount", index:"CONTS_COUNT", align:"center",sortable:true,cellattr: countTitleAttr},
            {name:"ratContType", index:"RAT_CONT_TYPE", align:"center",sortable:true},
            {name:"regDt", index:"REG_DT", align:"center",sortable:true},
            {name:"setVal", index:"SET_VAL", align:"center",sortable:true},
            {name:"amdrId", index:"AMDR_ID", align:"center",sortable:true},
            {name:"amdDt", index:"AMD_DT", align:"center",sortable:true},
            {name:"ratYn", index:"RAT_YN", hidden:true,sortable:true},
            {name:"totPrice", index:"TOT_PRICE", hidden:true,sortable:true},
            {name:"contsTitle", index:"CONTS_TITLE", hidden:true,sortable:true},
            {name:"ratSeq", index:"RAT_SEQ", hidden:true,sortable:true},
            {name:"ratContHstSeq", index:"RAT_CONT_HST_SEQ", hidden:true,sortable:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageHstDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "RAT_CONT_HST_SEQ",
            sortorder: "desc",
            height: "auto",
            loadComplete: function (data) {
                $(this).find("td").each(function(){
                    var rowId = $(this).parents("tr").attr("id");
                    var list = $("#jqgridHstData").jqGrid('getRowData', rowId);
                    if (typeof rowId == "undefined") {
                        return;
                    }
                    if ($(this).index() == 1){
                        var str = list.contsCount+getMessage("common.count");
                        $(this).text(str);
                    } else if ($(this).index() == 2) {
                        var str = "";

                        if (list.ratYn == "N") {
                            str = getMessage("billing.type.free");
                        } else if (list.ratContType == "T"){
                            str = getMessage("billing.Hourly");
                        } else if (list.ratContType == "C") {
                            str = getMessage("excute.count.limit.noSpace");
                        } else {
                            str = getMessage("billing.type.lump");
                        }

                        $(this).text(str);
                    } else if ($(this).index() == 3) {
                        $(this).html($(this).text().replace(" ~ ","<br /> ~"));
                    } else if ($(this).index() == 4) {
                        var str = $(this).text();
                        var price = list.totPrice;
                        price =price.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
                        if (price != "") {
                            price += " " + getMessage("common.price.unit");
                        }
                        if (list.ratYn == "N") {
                            str = "-";
                        } else if (list.ratContType == "T" || list.ratContType == getMessage("billing.Hourly")){
                            str += getMessage("common.minute") + ", " + price;
                        } else if (list.ratContType == "C" || list.ratContType == getMessage("excute.count.limit.noSpace")){
                            str += getMessage("common.case") +  ", "+ price;
                        } else {
                            str = price;
                        }

                        $(this).text(str);

                    }
                });

                //pageMove Max
                $('.ui-pg-input').on('keyup', function() {
                    this.value = this.value.replace(/\D/g, '');
                    if (this.value > $("#jqgridHstData").getGridParam("lastpage")) this.value = $("#jqgridHstData").getGridParam("lastpage");
                });

            }
    });
    jQuery("#jqgridHstData").jqGrid('navGrid','#pageHstDiv',{del:false,add:false,edit:false,search:false});
}
contractEditPost = function(){
    popConfirmLayer(getMessage("msg.confirm.contractDate.end"), function(){
        contractEditPostTo();
    });
}

contractEditPostTo = function(){
    var selContType;
    var selContTypeVal;
    if ($("#ratYn_N").attr("checked") == "checked") {
        selContType = "None";
        selContTypeVal = "N";
    } else if ($("input[name=ratCntYn]:checked").val() == "Y") {
        selContType = "ratCntYn";
        selContTypeVal = "C";
    } else if ($("input[name=ratTimeYn]:checked").val() == "Y") {
        selContType = "ratTimeYn";
        selContTypeVal = "T";
    } else if ($("input[name=ratLumpYn]:checked").val() == "Y") {
        selContType = "ratLumpYn";
        selContTypeVal = "L";
    } else {
        selContType = "None";
        selContTypeVal = "N";
    }

    if (selContType != "None") {
        if($("input[name="+selContType+"]").parents("tr").find("input[value=]").length >= 1) {
            popAlertLayer(getMessage("msg.input.buyinInfo"));
            return;
        } else if (selContType == "ratTimeYn" && $("#ratTime option:selected").val() == "0") {
            popAlertLayer(getMessage("msg.sel.ratTime"));
            return;
        }
    }


    var contsSeqs = "";

    $(Citem_ori).each(function(i){
        contsSeqs += Citem_ori[i].contsSeq + ",";
    });
    /*실행 횟수*/
    var runLmtCnt = 0;
    /*횟수당 금액*/
    var runPerPrc = 0;
    /*실행 시간*/
    var ratTime = 0;
    /*시간당 금액*/
    var ratPrc = 0;
    /*일시금 금액*/
    var lmsmpyPrc = 0;

    var now=new Date();
    $("#ratContFnsDt").val(dateString((new Date(now.getTime()))));

    formData("NoneForm" , "ratSeq", $("#ratSeq").val());
    formData("NoneForm" , "svcSeq", $("#svcSeq").val());
    formData("NoneForm" , "storSeq", $("#storSeq").val());
    formData("NoneForm" , "ratContNo", ratContNo_ori);
    formData("NoneForm" , "ratYn", $("input[name=ratYn]:checked").val());
    formData("NoneForm" , "ratContStDt", $("#ratContStDt").val());
    formData("NoneForm" , "ratContFnsDt", $("#ratContFnsDt").val());
    formData("NoneForm" , "ratContType", selContTypeVal);


    switch(selContTypeVal){
    case "N"://무료
        break;
    case "C"://횟수형
        runLmtCnt = $("#runLmtCnt").val();
        runPerPrc = $("#runPerPrc").val().replace(/[^\d]+/g, '');
        break;
    case "L"://일시금 지불
        lmsmpyPrc = $("#lmsmpyPrc").val().replace(/[^\d]+/g, '');
        break;
    case "T"://시간형
        ratTime = $("#ratTime").val();
        ratPrc = $("#ratPrc").val().replace(/[^\d]+/g, '');
        break;
    }


    formData("NoneForm" , "runLmtCnt", runLmtCnt);
    formData("NoneForm" , "runPerPrc", runPerPrc);
    formData("NoneForm" , "ratTime", ratTime);
    formData("NoneForm" , "ratPrc", ratPrc);
    formData("NoneForm" , "lmsmpyPrc", lmsmpyPrc);
    formData("NoneForm" , "contsSeqs", contsSeqs.slice(0,-1));

    /*데이터 변경 체크*/
    if (fnsDt_ori == $("#ratContFnsDt").val()) {
        formData("NoneForm" , "changed", false);
    } else {
        formData("NoneForm" , "changed", true);
        fnsDt_ori = $("#ratContFnsDt").val();
    }


    callByPut("/api/billingDetail" , "contractEditSuccess", "NoneForm","EditFail");
    $("#loading").show();

}

function contractEditSuccess(data){
    $("#loading").hide();
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.update"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }

    formDataDeleteAll("NoneForm");
}


EditFail = function(data){
    $("#loading").hide();
    popAlertLayer(getMessage("fail.common.update"));
    formDataDeleteAll("NoneForm");
}


/* 배열 내 값 검색 */
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}

/* 배열 내 값 검색 삭제*/
function removeObjects(arry, property, num) {
    for (var i in arry) {
        if (arry[i][property] == num)
            arry.splice(i, 1);
    }
}

function countTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = rawObject.contsTitle;

    return 'title="' + retVal  + '"';
}


function pageMoveStorList(){
    var url = '/billing/'+$("#storSeq").val();
    pageMove(url);
}
