/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var btnType = "request";
var limitTimeSeconds;
var timerFunction;
var failCount = 0;

$(document).ready(function() {
    $(window).on('beforeunload', function() {
        stopOtpTimerIfNeeded();
        callByPostSync("/api/logout");
    });

    $(".otpNumber").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            verifyOtpCode();
        }
    });

    $("#btnResend").click(function(e) {
        init();
        requestOtpCode();
    });


    $(".otpCancel").click(function(e) {
        pageMove("/");
    });

    init();
});

init = function() {
    stopOtpTimerIfNeeded();

    limitTimeSeconds = 180; // 3minute
    setBtnType("request");
    $(".otpNumber").val("");
    $("#otpLimitTime").text("");
}

setBtnType = function(value) {
    btnType = value;
    if (!btnType) {
        btnType = "request";
    }

    $(".otpBtn").off("click");
    if (btnType == "request") {
        $(".otpNumber").attr("disabled", true);
        $(".otpBtn").on("click", function() {
            requestOtpCode();
        });
        $(".otpCount").hide();
    } else {
        $(".otpNumber").attr("disabled", false);
        $(".otpBtn").on("click", function() {
            verifyOtpCode();
        });
        $(".otpCount").show();
    }

    var btnValue = (btnType == "request") ? getLoginMessage("login.button.request.auth") : getLoginMessage("login.button.verify.auth");
    $(".otpBtn").val(btnValue);
}

requestOtpCode = function() {
    callByGet("/api/login/auth", "didReceiveOtpCode", "NoneForm", "didNotReceiveOtpCode");
    formDataDeleteAll("NoneForm");
}

didReceiveOtpCode = function(data) {
    if (data.resultCode != "1000") {
        popAlertLayer(getLoginMessage("fail.login.auth.send"));
        return;
    }
    popAlertLayer(getLoginMessage("success.login.auth.send"));
    setBtnType("verify");
    startOtpTimer();

    if (data.otpCode && data.otpCode != undefined) {
        $(".otpNumber").val(data.otpCode);
    }
}

didNotReceiveOtpCode = function(data) {
    popAlertLayer(getLoginMessage("login.session.expired"), "/");
}

startOtpTimer = function() {
    var remainingTime = getTime(limitTimeSeconds);
    $("#otpLimitTime").text(remainingTime);
    stopOtpTimerIfNeeded();
    timerFunction = setInterval(calculateOtpRemainingTime, 1000);
}

calculateOtpRemainingTime = function() {
    limitTimeSeconds--;
    var remainingTime = getTime(limitTimeSeconds);
    $("#otpLimitTime").text(remainingTime);
    if (limitTimeSeconds <= 0) {
        popAlertLayer(getLoginMessage("fail.login.auth.timeover"));
        init();
    }
}

stopOtpTimerIfNeeded = function() {
    if (!timerFunction) {
        return;
    }
    clearInterval(timerFunction);
    delete timerFunction;
}

getTime = function(timeSeconds) {
    var remainingTime = "";
    var minutes = parseInt(timeSeconds / 60);
    var seconds = parseInt(timeSeconds % 60);
    if (seconds < 10) {
        seconds = "0" + seconds;
    }

    remainingTime = minutes + ":" + seconds;
    return remainingTime;
}

verifyOtpCode = function() {
    var otpCode = $(".otpNumber").val();
    if (!otpCode) {
        popAlertLayer(getLoginMessage("login.alert.auth.msg"), "", "", "");
        return;
    }

    formData("NoneForm", "otpCode", otpCode);
    callByPost("/api/login/auth", "didVerify", "NoneForm", "didNotVerify");
    formDataDeleteAll("NoneForm");
}

didVerify= function(data) {
    if (data.resultCode != "1000") {
        processForError(data);
        return;
    }
    var memberSec = data.result.mbrSe;

    
    var uri = makeAPIUrl("/dashboard/contents/verify");

    if (memberSec == "05") {
        uri = makeAPIUrl("/dashboard/contents/registered");
    }

    if ($("#requestUri").val()) {
        uri = makeAPIUrl($("#requestUri").val());
    }
    location.href = uri;
}

didNotVerify = function(data) {
    var result = JSON.parse(data.responseText);
    processForError(result);
}

processForError = function(data) {
    failCount++;

    var msgId = "fail.login.auth"
    var redirectUrl = "";
    if (failCount >= 3) {
        redirectUrl = "/";
        msgId = "fail.login.max.auth";
    }

    if (data != undefined && data.resultMsg == "session is disconnected") {
        redirectUrl = "/";
        msgId = "login.session.expired";
    }
    popAlertLayer(getLoginMessage(msgId), redirectUrl);
    init();
}

function popAlertLayer(msg, url, targetId) {
    setVisibilitySweetAlert(true);
    var content = document.createElement("div");
    content.innerHTML = msg;

    if (url) {
        swal({
            title: getLoginMessage("login.alert.msg"),
            content: content,
            closeOnClickOutside: false,
            closeOnEsc: false,
        })
        .then(
                function(value){
                    if(value){
                        swal.close();
                        pageMove(url);
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    } else {
        swal({
            title: getLoginMessage("login.alert.msg"),
            content: content,
        })
        .then(
                function(value) {
                    if (targetId) {
                        document.getElementById(targetId).focus();
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    }
}