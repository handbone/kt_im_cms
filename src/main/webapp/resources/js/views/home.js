/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    contentsList();
    verifyList();
    digitalAsList();
});

contentsList = function(){
    callByGet("/api/contents?rows=5&offset=0" , "contentsListSuccess", "NoneForm");
}

contentsListSuccess = function(data){
    if(data.resultCode == "1000"){
        var contentsListHtml = "";
        $(data.result.contentsList).each(function(i,item) {
            contentsListHtml += "<tr>";
            contentsListHtml += "<td align=\"center\"><a href=\"/contents/"+item.seq+"\" class=\"fontblack\">"+item.title+"</a></td>";
            contentsListHtml += "<td align=\"center\">"+item.memberName+"("+item.memberId+")</td>";
            contentsListHtml += "<td class=\"date\">"+item.regDate.split(" ")[0]+"</td>";
            contentsListHtml += "</tr>";
        });

        $("#contentsList").html(contentsListHtml);
    }else if(data.resultCode == "1010"){
        $("#contentsList").html("<tr><td colspan='3' align='center'>"+getMessage("cms.release.msg.noData")+"</td></tr>");
    }
}



verifyList = function(){
    callByGet("/api/verify?rows=5&offset=0&status=4", "verifyListSuccess", "NoneForm");
}

verifyListSuccess = function(data){
    var statusHtml = getMessage("contents.table.verify.complete");
    if(data.resultCode == "1000"){
        var verifyListHtml = "";
        var no = data.result.totalCount-$("#offset").val();
        $(data.result.verifyList).each(function(i,item) {
            verifyListHtml += "<tr>";
            verifyListHtml += "<td align=\"center\"><a href=\"/verify/"+item.seq+"\" class=\"fontblack\">"+item.title+"</a></td>";
            verifyListHtml += "<td align=\"center\">"+item.memberName+"("+item.memberId+")</td>";
            verifyListHtml += "<td class=\"date\">"+item.regDate.split(" ")[0]+"</td>";
            verifyListHtml += "<td align=\"center\">"+statusHtml+"</td>";
            verifyListHtml += "</tr>";
            no--;
        });
        $("#verifyList").html(verifyListHtml);
    }else if(data.resultCode == "1010"){
        $("#verifyList").html("<tr><td colspan='4' align='center'>"+getMessage("cms.release.msg.noData")+"</td></tr>");
    }
}


digitalAsList = function(){
    callByGet("/api/digitalAs?rows=5&offset=0", "digitalAsListSuccess", "NoneForm");
}

digitalAsListSuccess = function(data){
    if(data.resultCode == "1000"){
        var digitalAsListHtml = "";
        var no = data.result.totalCount-$("#offset").val();
        $(data.result.digitalAsList).each(function(i,item) {
            var processFlag = "";
            if(item.confirm == "Y"){
                processFlag = "처리완료";
            }else{
                processFlag = "처리중";
            }
            digitalAsListHtml += "<tr>";
            digitalAsListHtml += "<td align=\"center\"><a href=\"/digitalAs/"+item.seq+"\" class=\"fontblack\">"+item.didId+"</a></td>";
            digitalAsListHtml += "<td class=\"date\">"+processFlag+"</td>";
            digitalAsListHtml += "<td class=\"date\">"+item.regDate+"</td>";
            digitalAsListHtml += "</tr>";
            no--;
        });
        $("#digitalAsList").html(digitalAsListHtml);
    }else if(data.resultCode == "1010"){

        $("#digitalAsList").html("<tr><td colspan='3' align='center'>"+getMessage("cms.release.msg.noData")+"</td></tr>");
    }
}