/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    $(window).on('beforeunload', function() {
        callByPostSync("/api/logout", '', '', '');
    });

    $("#btnChangePwd").click(function(e) {
        updatePassword();
    });
});

updatePassword = function() {
    var oldPwd = $("#oldPassword").val();
    if (!oldPwd) {
        popAlertLayer(getLoginMessage("login.password.msg"), '', "oldPassword");
        return;
    }

    var pwd1 = $("#updatePwd1").val();
    if (!pwd1) {
        popAlertLayer(getLoginMessage("login.newPassword.msg"), '', "updatePwd1");
        return;
    }

    var validResult = validatePassword(pwd1, $("#mbrId").val());
    var isValid = (validResult === "valid");
    if (!isValid) {
        if (validResult === "continuousChar") {
            msg = getLoginMessage("login.alert.continuous.character.msg");
        } else if (validResult === "containsIdPart") {
            msg = getLoginMessage("login.alert.cotains.id.msg");
        } else {
            msg = getLoginMessage("login.alert.password.msg");
        }
        popAlertLayer(msg, '', "updatePwd1");
        return;
    }

    var pwd2 = $("#updatePwd2").val();
    if (!pwd2) {
        popAlertLayer(getLoginMessage("login.newPasswordConfirm.msg"), '', "updatePwd2");
        return;
    }

    if (pwd1 != pwd2) {
        $("#updatePwd2").val("");
        popAlertLayer(getLoginMessage("login.alert.passwordConfirm.msg"), '', "updatePwd2");
        return;
    }

    formData("NoneForm", "oldPwd", oldPwd);
    formData("NoneForm", "pwd1", pwd1);
    formData("NoneForm", "pwd2", pwd2);
    callByPost("/api/member/password", "didUpdatePassword", "NoneForm", "didNotUpdatePassword");
    formDataDeleteAll("NoneForm");
}

didUpdatePassword = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getLoginMessage("success.login.change.pwd"), "/auth");
    } else {
        popAlertLayer(data.resultMsg);
    }
}

didNotUpdatePassword = function(data) {
    var result = JSON.parse(data.responseText);
    var msg = getLoginMessage(result.resultMsg);
    if (result.resultMsg.indexOf("invalid parameter") != -1) {
        msg = getLoginMessage("fail.login.invalid.password");
    }
    if (!msg) {
        msg = getLoginMessage("fail.login.server.error");
    }
    popAlertLayer(msg);
}

function popAlertLayer(msg, url, targetId) {
    setVisibilitySweetAlert(true);
    var content = document.createElement("div");
    content.innerHTML = msg;

    if (url) {
        swal({
            title: getLoginMessage("login.alert.msg"),
            content: content,
            closeOnClickOutside: false,
            closeOnEsc: false,
        })
        .then(
                function(value){
                    if(value){
                        swal.close();
                        pageMove(url);
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    } else {
        swal({
            title: getLoginMessage("login.alert.msg"),
            content: content,
        })
        .then(
                function(value) {
                    if (targetId) {
                        document.getElementById(targetId).focus();
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    }
}