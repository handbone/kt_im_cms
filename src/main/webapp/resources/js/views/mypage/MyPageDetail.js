/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var didCheckId = false;

$(document).ready(function(){

    $(".btnModify").click(function(e) {
        cofirmMove('/mypage/edit');
    });

    $(".btnDelete").click(function(e) {
        confirmDelete();
    });

    $(".btnHisModify").click(function(e) {
        showChangedHistoryPopup();
    });

    $("#popupHistory .btnClose").click(function(e) {
        closeChangedHistoryPopup();
    });

    getMemberDetail();
});

getMemberDetail = function() {
    callByGet("/api/member?seq=" + $("#seq").val(), "didRecieveMemberDetail");
}

didRecieveMemberDetail = function(data) {
    if (data.resultCode != "1000") {
        return;
    }

    var result = data.result.memberInfo;
    $("#memberName").text(result.mbrNm);
    $("#memberId").text(result.mbrId);
    $("#memberSection").text(result.mbrSeNm);
    $("#cretrId").text(result.cretrId);
    $("#cretDt").text(result.cretDt);
    $("#amdrId").text(result.amdrId);
    $("#amdDt").text(result.amdDt);
    $("#memberTelNo").text(result.mbrTelNo);
    var memberPhoneList = "";
    $(result.mbrMphonList).each(function(i, item){
        if (i != 0) {
            memberPhoneList += "<br />";
        }
        memberPhoneList += item.mbrMphonNo;
    });
    $("#memberPhoneNo").html(memberPhoneList);
    $("#memberEmail").text(result.mbrEmail);
    if (result.mbrSe != "06") {
        $("#memberIpadr").parent().show();
        var ipadr = "<span>" + result.ipadr + "</span>";
        if (result.ipadr2) {
            ipadr += "<br><span>" + result.ipadr2 + "</span>";
        }
        $("#memberIpadr").html(ipadr);
    }
    $("#memberStatusName").text(result.mbrSttusNm);

    if (result.mbrSttus != "03") {
        $(".btnModify").show();
        $(".btnDelete").show();
    }
    if (result.mbrSe == "01" || result.mbrSe == "02" || result.mbrSe == "03")
        return;

    var extensionName = "No Name";
    var extensionValue = "No Value1";

    if (result.mbrSe == "04") {
        extensionName = getMessage("login.input.service");
        extensionValue = result.svcNm;
    } else if (result.mbrSe == "05") {
        extensionName = getMessage("login.input.store");
        extensionValue = result.storNm;
    } else if (result.mbrSe == "06") {
        extensionName = getMessage("login.input.cp");
        extensionValue = result.cpNm;
    }

    var extensionInfoHtml = "";
    extensionInfoHtml += "<tr><th>" + extensionName + "</th>";
    extensionInfoHtml += "<td>" + extensionValue + "</td></tr>";

    $("#memberSection").parent().after(extensionInfoHtml);
}

confirmDelete = function() {
    var content = $("#memberId").text() + " " + getMessage("member.manage.alert.delete.confirm");
    popConfirmLayer(content, function() {
        callByDelete("/api/member?seq=" + $("#seq").val(), "didDeleteUser", "NoneForm", "didNotDeleteUser");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("button.confirm"));
}

didDeleteUser = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"));
        logout();
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

didNotDeleteUser = function(data) {
    popAlertLayer(getMessage("fail.common.delete"));
}

getMemberHistory = function() {
    callByGet("/api/member/history?seq=" + $("#seq").val(), "didRecieveMemberHistory");
}

didRecieveMemberHistory = function(data) {
    var memberHistoryHtml = "<li><span>" + getMessage("common.nodata.msg") + "</span></li>";
    if (data.resultCode == "1000") {
        memberHistoryHtml = "";
        $(data.result.memberHistoryList).each(function(i, item) {
            memberHistoryHtml += "<li><span>" + item.changeCol + " " + getMessage("title.update") + "</span><span class='lengthM floatRight'>" + item.cretDt + "</span></li>";
        });
    }

    $("#popupHistory .historyGrp ul").html(memberHistoryHtml);
    openPopupLayer("popupHistory");
}

showChangedHistoryPopup = function() {
    getMemberHistory();
}

closeChangedHistoryPopup = function() {
    closePopupLayer();
}

openPopupLayer = function(name) {
    popLayerDiv(name, 500, 330, true);
}

closePopupLayer = function() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}