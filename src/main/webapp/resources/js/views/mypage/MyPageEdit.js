/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var memberInfo = {};
var mailOriInfo= "";
var delTelList = new Array();
$(document).ready(function() {
    $(".tableForm input[type='text']").blur(function() {
        if (this.id == "memberEmailId" || this.id == "memberEmailDomain") {
            if (!$("#memberEmailId").val() && !$("#memberEmailDomain").val()) {
                $("#memberEmailIdMsg").text("");
                return;
            }

            if (this.id == "memberEmailId" && !$("#memberEmailDomain").val()) {
                return;
            }

            if (this.id == "memberEmailDomain") {
                validate(document.getElementById("memberEmailId"), false);
                return;
            }
        } else if ($(this).hasClass("memberPhoneNo")) {
            validatePhon(this, false);
        } else {
            if (!$(this).val()) {
                $("#" + this.id + "Msg").text("");
                return;
            }
        }

        validate(this, false);
    });

    $("#btnUpdate").click(function(e) {
        confirmModify();
    });

    $("#contents .btnCancel").click(function(e) {
        pageMove('/mypage');
    });

    $("#btnPwd").click(function(e) {
        showPasswordChangePopup();
    });

    $("#popupWinidpw .btnModify").click(function(e) {
        updatePassword();
    });

    $("#popupWinidpw .btnCancel").click(function(e) {
        closePasswordChangePopup();
    });

    $("#memberDomainList").change(function(e) {
        changeMemberEmailBox(this);
    });

    $("#memberTelNo, #memberPhoneNo").keyup(function(e) {
        var value = $(this).val();
        $(this).val(value.replace(/[^0-9\-]/gi,''));
    });

    $("#memberEmailDomain").keyup(function() {
        var domain = $("#memberDomainList option:selected").val();
        if (!domain) {
            return;
        }

        if ($("#memberDomainList option:selected").val() != $("#memberEmailDomain").val()) {
            $("#memberDomainList option:eq(0)").prop("selected", true);
        }
    });

    addEmailDomainList(document.getElementById("memberDomainList"));

    $("#mPlus").click(function(){
        $("#memberPhoneNoCol").append("<div class='margin_2'><p class='memberPhoneBox'><input type='text' id='memberPhoneNo"+$(".memberPhoneNo").size()+"' class='inputBox lengthL memberPhoneNo' value=''><input type='hidden' id='memberPhoneNoOri"+$(".memberPhoneNo").size()+"' /><input type='button' class='rightIconMius rightBtn' onclick='removePhonNo(this)'><br><span class='cautionTxt'></span></p></div>");
        $("#memberPhoneNo"+($(".memberPhoneNo").size()-1)).blur(function() {
           if ($(this).hasClass("memberPhoneNo")) {
                validatePhon(this, false);
            }
            validate(this, false);
        });
        $("#mPlus").css("opacity","0.2").attr( 'disabled', true );
    });

});

updateMemberDetail = function() {
    callByGet("/api/member?seq=" + $("#seq").val() + "&getType=edit&path=mypage", "didUpdateMemberDetail");
}

didUpdateMemberDetail = function(data) {
    if (data.resultCode != "1000") {
        return;
    }

    var result = data.result.memberInfo;

    mailOriInfo = result.mbrEmail;

    $("#memberId").text(result.mbrId);
    $("#memberName").val(result.mbrNm);
    $("#memberTelNo").val(result.mbrTelNo);

    $(result.mbrMphonList).each(function(i, item){
        if (i != 0) {
            $("#mPlus").css("opacity","0.2").attr('disabled', true );
            $("#memberPhoneNoCol").append("<div class='margin_2'><p class='memberPhoneBox'><input type='text' id='memberPhoneNo"+i+"' class='inputBox lengthL memberPhoneNo' value="+item.mbrMphonNo+"><input type='hidden' id='memberPhoneNoOri"+i+"' value='"+item.mbrMphonNo+"'/><input type='hidden' class='oriTelList' value="+(i+1)+" /><input type='button' class='rightIconMius rightBtn' onclick='removePhonNo(this)'><br><span class='cautionTxt'></span></p></div>");
            $("#memberPhoneNo"+i).blur(function(e) {
                if (!$(this).val()) {
                    if (restoreTextIfNeeded(e.target)) {
                        return;
                    } else {
                        validatePhon(this, false);
                    }
                } else {
                    validatePhon(this, false);
                }
            });
        } else {
            $(".memberPhoneNo").eq(i).val(item.mbrMphonNo);
            $("#memberPhoneNoOri0").val(item.mbrMphonNo);
        }
    });
    var emailArr = result.mbrEmail.split("@");
    $("#memberEmailId").val(emailArr[0]);
    $("#memberEmailDomain").val(emailArr[1]);
    $("#memberSection").text(result.mbrSeNm);
    $("#memberIpadr").parent().parent().show();
    $("#memberIpadr").val(result.ipadr);
    $("#memberIpadr2").parent().parent().show();
    $("#memberIpadr2").val(result.ipadr2);

    memberInfo = data.result.memberInfo;

    initForm();
}

initForm = function() {
    var svcNm = memberInfo.svcNm || getMessage("select.nothing");
    $("#memberService").text(svcNm);
    $("#memberService").parent().parent().show();

    var storNm = memberInfo.storNm || getMessage("select.nothing");
    $("#memberStore").text(storNm);
    $("#memberStore").parent().parent().show();
}

function confirmModify() {
    if (!validateForm()) {
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        modifyMember();
    }, null, getMessage("button.confirm"));
}

function validateForm() {
    var isValid = true;
    $(".tableForm input[type='text']").each(function(i, item) {
        if ($(this).hasClass("memberPhoneNo")) {
            if (!validatePhon(item, true)) {
                isValid = false;
                return false;
            }
        } else {
            if (!validate(item, true)) {
                isValid = false;
                return false;
            }
        }

    });

    if (isValid) {
        if ($("#memberIpadr").val() == $("#memberIpadr2").val()) {
            isValid = false;
            popAlertLayer(getMessage("login.alert.same.ipAddress.msg"), '', '', '', "memberIpadr2");
        }
    }
    return isValid;
}

function validate(obj, usePopup) {
    var id = obj.id;
    var msg = "";
    var msgId = "#" + id + "Msg";

    $(msgId).hide();
    var value = $(obj).val();
    if (!usePopup) {
        if (id == "memberEmailId") {
            value += $("#memberEmailDomain").val();
        }
    }
    if (value.length <= 0) {
        if (id == "memberIpadr2") {
            return true;
        }
        msg = getMessage("login.alert.required.msg");
        if (usePopup) {
            popAlertLayer(msg, '', '', '', id);
        } else {
            $(msgId).text(msg);
            $(msgId).show();
        }
        return false;
    }

    var isValid = true;
    switch (id) {
    case "memberName":
        isValid =  validateName($("#memberName").val());
        if (!isValid) {
            msg = getMessage("login.alert.name.msg");
        }
        break;
    case "memberTelNo":
        isValid = validateTelNumber($("#memberTelNo").val());
        if (!isValid) {
            msg = getMessage("login.alert.telNo.msg");
        } else {
            $("#memberTelNo").val(phoneNumberFormatter($("#memberTelNo").val()));
        }
        break;
    case "memberPhoneNo":
        isValid = validatePhoneNumber($("#memberPhoneNo").val());
        if (!isValid) {
            msg = getMessage("login.alert.phoneNo.msg");
        } else {
            $("#memberPhoneNo").val(phoneNumberFormatter($("#memberPhoneNo").val()));
        }
        break;
    case "memberEmailId":
        if (!usePopup) {
            isValid =  validateEmail($("#memberEmailId").val() + "@" + $("#memberEmailDomain").val());
        } else {
            isValid =  validateEmailId($("#memberEmailId").val());
        }
        if (!isValid) {
            msg = getMessage("login.alert.email.msg");
        }
        break;
    case "memberEmailDomain":
        if (usePopup) {
            isValid =  validateEmailDomain($("#memberEmailDomain").val());
        }
        if (!isValid) {
            msg = getMessage("login.alert.email.msg");
        }
        break;
    case "memberIpadr":
        isValid =  validateIpAddress($("#memberIpadr").val());
        if (!isValid) {
            msg = getMessage("login.alert.first.ipAddress.msg");
            if ($("#memberIpadr2Msg").text()) {
                msg = msg + " ";
            }
        }
        break;
    case "memberIpadr2":
        isValid =  validateIpAddress($("#memberIpadr2").val());
        if (!isValid) {
            msg = getMessage("login.alert.second.ipAddress.msg");
            if ($("#memberIpadrMsg").text()) {
                msg = " " + msg;
            }
        } else {
            if ($("#memberIpadr").val() == $("#memberIpadr2").val()) {
                isValid = false;
                msg = getMessage("login.alert.same.ipAddress.msg");
            }
        }
        break;
    default:
        console.log("Nothing id = " + id);
        break;
    }

    if (!isValid) {
        if (usePopup) {
            popAlertLayer(msg, '', '', '', id);
        } else {
            $(msgId).text(msg);
            $(msgId).show();
        }
    }
    return isValid;
}

function validatePhon(obj, usePopup) {
    var msg = "";
    var msgSpan = $(obj).parents("p").find("span");

    $(msgSpan).hide();
    var value = $(obj).val();

    if (value.length <= 0) {
        msg = getMessage("login.alert.required.msg");
        if (usePopup) {
            popAlertLayerObj(msg, '', '', '', obj);
        } else {
            $(msgSpan).text(msg);
            $(msgSpan).show();
        }
        return false;
    }

    var isValid = true;
    isValid = validatePhoneNumber($(obj).val());
    if (!isValid) {
        msg = getMessage("login.alert.phoneNo.msg");
    } else {
        $(obj).val(phoneNumberFormatter($(obj).val()));
    }

    if (!isValid) {
        if (usePopup) {
            popAlertLayerObj(msg, '', '', '', obj);
        } else {
            $(msgSpan).text(msg);
            $(msgSpan).show();
        }
    }
    return isValid;
}

function modifyMember() {
    var telNo = phoneNumberFormatter($("#memberTelNo").val());

    var phoneNo = "";

    for (var i=0; i<$(".memberPhoneNo").size(); i++) {
        if (phoneNo != "") {
            phoneNo += ";";
        }
        phoneNo += phoneNumberFormatter($(".memberPhoneNo").eq(i).val());
        if (typeof $("#memberPhoneNo"+i).parents("p").find(".oriTelList").val() != "undefined") {
            phoneNo+= "="+$("#memberPhoneNo"+i).parents("p").find(".oriTelList").val();
        }
    }



    formData("NoneForm", "seq", memberInfo.mbrSeq);
    formData("NoneForm", "se", memberInfo.mbrSe);
    formData("NoneForm", "svcSeq", memberInfo.svcSeq);
    formData("NoneForm", "storSeq", memberInfo.storSeq);
    formData("NoneForm", "cpSeq", memberInfo.cpSeq);
    formData("NoneForm", "delphone", delTelList.toString());
    formData("NoneForm", "name", $("#memberName").val());
    formData("NoneForm", "tel", telNo);
    formData("NoneForm", "phone", phoneNo);

    if (mailOriInfo != ($("#memberEmailId").val() + "@" + $("#memberEmailDomain").val())){
        formData("NoneForm", "mailChgYn", "Y");
    } else {
        formData("NoneForm", "mailChgYn", "N");
    }

    formData("NoneForm", "email", $("#memberEmailId").val() + "@" + $("#memberEmailDomain").val());
    formData("NoneForm", "ipadr", $("#memberIpadr").val());
    formData("NoneForm", "ipadr2", $("#memberIpadr2").val());

    callByPut("/api/member", "didModifyMember", "NoneForm", "didNotModifyMember");
    formDataDeleteAll("NoneForm");
}

function didModifyMember(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/mypage");
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("login.unavailableEmail.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function didNotModifyMember(data) {
    popAlertLayer(getMessage("fail.common.update"));
}

function showPasswordChangePopup () {
    openPopupLayer("popupWinidpw");
}

function closePasswordChangePopup () {
    closePopupLayer();
}

function openPopupLayer(name) {
    popLayerDiv(name, 500, 330, true);
}

closePopupLayer = function() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();

    $("#oldPassword").val("");
    $("#updatePwd1").val("");
    $("#updatePwd2").val("");
}

updatePassword = function() {
    var oldPwd = $("#oldPassword").val();
    if (!oldPwd) {
        popAlertLayer(getMessage("login.password.msg"), '', '', '', "oldPassword");
        return;
    }

    var pwd1 = $("#updatePwd1").val();
    if (!pwd1) {
        popAlertLayer(getMessage("login.newPassword.msg"), '', '', '', "updatePwd1");
        return;
    }

    var validResult = validatePassword(pwd1, $("#memberId").text());
    var isValid = (validResult === "valid");
    if (!isValid) {
        if (validResult === "continuousChar") {
            msg = getMessage("login.alert.continuous.character.msg");
        } else if (validResult === "containsIdPart") {
            msg = getMessage("login.alert.cotains.id.msg");
        } else {
            msg = getMessage("login.alert.password.msg");
        }
        popAlertLayer(msg, '', '', '', "updatePwd1");
        return;
    }

    var pwd2 = $("#updatePwd2").val();
    if (!pwd2) {
        popAlertLayer(getMessage("login.newPasswordConfirm.msg"), '', '', '', "updatePwd2");
        return;
    }

    if (pwd1 != pwd2) {
        $("#updatePwd2").val("");
        popAlertLayer(getMessage("login.alert.passwordConfirm.msg"), '', '', '', "updatePwd2");
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("NoneForm", "seq", $("#seq").val());
        formData("NoneForm", "oldPwd", oldPwd);
        formData("NoneForm", "pwd1", pwd1);
        formData("NoneForm", "pwd2", pwd2);
        callByPost("/api/member/password", "didUpdatePassword", "NoneForm", "didNotUpdatePassword");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("common.confirm"));
}

didUpdatePassword = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.login.password.update"));
        closePasswordChangePopup();
    } else {
        popAlertLayer(data.resultMsg);
    }
}

didNotUpdatePassword = function(data) {
    var result = JSON.parse(data.responseText);
    var msg = getMessage(result.resultMsg);
    if (result.resultMsg.indexOf("invalid parameter") != -1) {
        msg = getMessage("fail.login.invalid.password");
    }
    if (!msg) {
        msg = getMessage("fail.login.server.error");
    }
    popAlertLayer(msg);
}

changeMemberEmailBox = function(obj) {
    $("#memberEmailDomain").val(obj.value);
}

function initFindUserInfo(data) {
    $("#loading").hide();
    if (data.resultCode != "1000") {
        $(".inputBox").val("");
        popAlertLayer(getMessage("common.bad.request.msg"), "/mypage");
    } else {
        updateMemberDetail();
    }
}

function initNotFindUserInfo(data) {
    $("#loading").hide();
    $(".inputBox").val("");
    popAlertLayer(getMessage("common.bad.request.msg"), "/mypage");
//    back
}

function cofirmMove(mvPage) {
    pageMove(mvPage);
}


function removePhonNo(e) {
    var textObj = $(e).parents("p").find(".oriTelList");
    if (typeof $(textObj).val() != "undefined") {
        delTelList.push($(textObj).val());
    }
    $(e).index(".memberPhoneNo")

    $(e).parents("p").remove();
    $("#mPlus").css("opacity","1").attr( 'disabled', false );
}