/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var NAME = 0;
var ID = 1;

$(document).ready(function() {
    limitInputTitle("genreName");

    $("#firstCategoryList ul").on("click", function(e) {
        if (!e.target || e.target.nodeName.toLowerCase() != "li") {
            return;
        }

        var oldValue =  $("#firstCategoryList ul li.selected").attr("data");
        var newValue = $(e.target).attr("data");
        if (oldValue == newValue) {
            return;
        }

        if ($("#firstCategoryList ul li.selected").hasClass("selected")) {
            $("#firstCategoryList ul li.selected").removeClass("selected");
        }
        $(e.target).addClass("selected");
        getGenreList();
    });

    $("#genreList ul").on("click", function(e) {
        if (!e.target || e.target.nodeName.toLowerCase() != "li") {
            return;
        }

        var oldValue =  $("#genreList ul li.selected").attr("data");
        var newValue = $(e.target).attr("data");
        if (oldValue == newValue) {
            return;
        }

        if ($("#genreList ul li.selected").hasClass("selected")) {
            $("#genreList ul li.selected").removeClass("selected");
        }
        $(e.target).addClass("selected");
        updateContentsList();
    });

    initContentList();

    resizeJqGridWidth("jqgridData", "gridArea");

    getCategoryList();

    setkeyup();
});

getCategoryList = function() {
    formData("NoneForm", "GroupingYN", "Y");
    callByGet("/api/contentsCategory" , "didReceiveCategoryList", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didReceiveCategoryList = function(data) {
    if (data.resultCode != "1000") {
        clearGridData();
        return;
    }

    var firstCategoryListHtml = "";

    $(data.result.firstCtgList).each(function(i,item) {
        firstCategoryListHtml += "<li data='" + item.firstCtgID + "'>" + item.firstCtgNm + "(" + item.firstCtgID + ")</li>";
    });

    $("#firstCategoryList ul").html(firstCategoryListHtml);
    $("#firstCategoryList ul li:eq(0)").addClass("selected");

    getGenreList();
}

getGenreList = function() {
    formData("NoneForm", "firstCtgId", $("#firstCategoryList ul li.selected").attr("data"));
    callByGet("/api/contents/genre" , "didReceiveGenreList", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didReceiveGenreList = function(data) {
    var genreListHtml = "";

    if (data.result) {
        $(data.result.genreList).each(function(i,item) {
            genreListHtml += "<li data='" + item.genreSeq + "'>" + item.genreNm + "(" + item.genreId + ")</li>";
        });
    }

    $("#genreList ul").html(genreListHtml);
    $("#genreList ul li:eq(0)").addClass("selected");

    if (data.resultCode != "1000") {
        clearGridData();
        $("#genreList ul").html(genreListHtml);
        return;
    }

    updateContentsList();
}

initContentList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("contents.table.id"),
        getMessage("contents.table.name"),
        getMessage("table.genre"),
        getMessage("table.reger"),
        getMessage("table.progressStatus"),
        getMessage("table.regdate"),
        "seq"
    ];
    $("#jqgridData").jqGrid({
        datatype: "json", // 데이터 타입 지정
        jsonReader : { 
            page: "result.currentPage", 
            total: "result.totalPage", 
            root: "result.contentsList", 
            records: "result.totalCount",
            repeatitems: false, 
            id: "seq_user"
        },
        colNames: columnNames,
        colModel:[
            {name:"num", index: "num", align:"center", width:40, hidden:true},
            {name:"contsID", index:"CONTS_ID", align:"center"},
            {name:"contsTitle", index: "CONTS_TITLE", align:"center", classes:"pointer", formatter:pointercursor},
            {name:"genreNm", index:"GENRE_NM", align:"center", sortable:false},
            {name:"cretrNm", index:"CRETR_ID", align:"center"},
            {name:"sttus", index:"STTUS", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"contsSeq", index:"seq", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: 10, // 페이지에 출력될 칼럼 개수
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "contsSeq",
            sortorder: "desc",
            height: "auto",
            multiselect: false,
            postData: {
                genreSeq: $("#genreList ul li.selected").attr("data")
            },

            beforeRequest:function() {

            },
            /*
             * parameter: x
             * jqGrid 그리기가 끝나면 함수 실행
             */
            loadComplete: function() {
                //session
                if (sessionStorage.getItem("last-url") != null) {
                    sessionStorage.removeItem('state');
                    sessionStorage.removeItem('last-url');
                }

                $(this).find("td").each(function(){
                    if ($(this).index() == 5) { // 구분
                        var str = $(this).text();

                        if(str == getMessage("contents.table.state.exhibition.success")){
                            str = "<span class='viewIcon'>"+getMessage("contents.table.state.verify.success") + "</span>";
                        }
                        $(this).html(str);
                    }
                });
            },
            loadError: function (jqXHR, textStatus, errorThrown) {
                clearGridData();
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event) {
                // 제목 셀 클릭시
                if (columnId == 2) {
                    sessionStorage.setItem("last-url", location);
                    sessionStorage.setItem("state", "view");

                    var list = $("#jqgridData").jqGrid('getRowData', rowId);
                    pageMove("/contents/"+list.contsSeq);
                }
            }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

function updateContentsList() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            url:makeAPIUrl("/api/contents"),
            postData:{
                genreSeq: $("#genreList ul li.selected").attr("data")
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

/* blockUI 열기
 * parameter: name(blockUI를 설정할 ID값)
 * return: x
 */
function showGenrePopup(type) {
    if (!$("#firstCategoryList ul li").length || !$("#firstCategoryList ul li.selected").length) {
        popAlertLayer(getMessage("contents.category.alert.add.firstCategory"));
        return;
    }

    if (type == "update") {
        if (!$("#genreList ul li").length || !$("#genreList ul li.selected").length) {
            popAlertLayer(getMessage("contents.genre.alert.no.update"));
            return;
        }
    }

    var firstCategoryInfo = splitText($("#firstCategoryList ul li.selected").text());
    $("#firstCategoryName").text(firstCategoryInfo[NAME]);
    $("#firstCategoryId").text(firstCategoryInfo[ID]);

    var disabled = (type == "update");
    var popupTitle = getMessage("contents.genre.regist.msg");
    var btnSaveHtml = "<input id='btnSave' type='button' class='btnNormal btnWrite' value='" + getMessage("button.create.confirm")  + "' onclick=\"confirmGenreInsert()\">";

    if (type == "update") {
        var genreInfo = splitText($("#genreList ul li.selected").text());
        $("#genreName").val(xssChk(genreInfo[NAME]));
        $("#genreId").replaceWith("<span id='genreId'>" + genreInfo[ID] + "</span>");

        popupTitle = getMessage("contents.genre.modify.msg");
        btnSaveHtml = "<input id='btnSave' type='button' class='btnNormal btnWrite' value='" + getMessage("button.update.confirm")  + "' onclick=\"confirmGenreUpdate()\">";
    } else {
        $("#genreId").replaceWith("<input id='genreId' type='text' class='inputBox lengthL' maxlength='2' onkeyup='categoryValueValidationCheck(this)'>");
    }

    $(".popupTitle").text(popupTitle);
    $("#btnSave").replaceWith(btnSaveHtml);

    popLayerDiv("genreListForm", 500, 300, true);
}

/* blockUI 닫기
 * parameter: x
 * return: x
 */
function closeGenrePopup(){
    $("body").css("overflow-y" , "visible");
    $.unblockUI();

    $("#genreListForm input[type=text]").val("");
}

function categoryValueValidationCheck(object) {
    if (object.value) {
        var re = /^[A-Z]+$/;
        if(!re.test(object.value)){
            popAlertLayer(getMessage("common.only.upper.msg"), '', '', '', object.id);
            object.value = "";
            return;
        }
    }
}

function confirmGenreInsert() {
    if (!$("#firstCategoryId").text()) {
        popAlertLayer(getMessage("contents.category.alert.firstCategory.id"));
        return;
    }

    if (!$("#genreName").val()) {
        popAlertLayer(getMessage("contents.genre.alert.name"), '', '', '', "genreName");
        return;
    }

    if (!validateMaxLength($("#genreName").val(), 50)) {
        popAlertLayer(getMessage("contents.genre.alert.name.maxLength"), "", "", "", "genreName");
        return;
    }

    if (!$("#genreId").val()) {
        popAlertLayer(getMessage("contents.genre.alert.id"), '', '', '', "genreId");
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        insertGenre();
    }, null, getMessage("button.confirm"));
}

function insertGenre() {
    formData("NoneForm", "firstCtgId", $("#firstCategoryId").text());
    formData("NoneForm", "genreNm", $("#genreName").val());
    formData("NoneForm", "genreId", $("#genreId").val());

    callByPost("/api/contents/genre", "didInsertGenre", "NoneForm", "didNotInsertGenre");
    formDataDeleteAll("NoneForm");
}

function didInsertGenre(data) {
    if(data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"), "/contents/genre");
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("contents.genre.alert.duplication"));
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

function didNotInsertGenre(data) {
    popAlertLayer(getMessage("fail.common.insert"));
}

function confirmGenreUpdate() {
    if (!$("#genreName").val()) {
        popAlertLayer(getMessage("contents.genre.alert.name"), '', '', '', "genreName");
        return;
    }

    if (!validateMaxLength($("#genreName").val(), 50)) {
        popAlertLayer(getMessage("contents.genre.alert.name.maxLength"), "", "", "", "genreName");
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        updateGenre();
    }, null, getMessage("button.confirm"));
}

function updateGenre() {
    var genreSeq = $("#genreList ul li.selected").attr("data");
    formData("NoneForm", "genreSeq", genreSeq);
    formData("NoneForm", "genreNm", $("#genreName").val());

    callByPut("/api/contents/genre", "didUpdateGenre", "NoneForm", "didNotUpdateGenre");
    formDataDeleteAll("NoneForm");
}

function didUpdateGenre(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/contents/genre");
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("contents.genre.alert.duplication"));
    } else if (data.resultCode == "1013") {
        popAlertLayer(getMessage("contents.genre.alert.nochange"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function didNotUpdateGenre(data) {
    popAlertLayer(getMessage("fail.common.update"));
}

function confirmDeleteGenre() {
    if (!$("#firstCategoryList ul li").length || !$("#firstCategoryList ul li.selected").length) {
        popAlertLayer(getMessage("contents.category.alert.add.firstCategory"));
        return;
    }

    if (!$("#genreList ul li").length || !$("#genreList ul li.selected").length) {
        popAlertLayer(getMessage("contents.genre.alert.no.delete"));
        return;
    }
    var categoryInfo = splitText($("#firstCategoryList ul li.selected").text());
    var genreInfo = splitText($("#genreList ul li.selected").text());
    popConfirmLayer(categoryInfo[NAME] + " > " + genreInfo[NAME] + " " + getMessage("contents.genre.delete.msg"), function() {
        deleteGenre();
    }, null, getMessage("button.confirm"));
}

function deleteGenre() {
    var genreSeq = $("#genreList ul li.selected").attr("data");
    formData("NoneForm", "genreSeq", genreSeq);

    callByDelete("/api/contents/genre", "didDeleteGenre", "NoneForm", "didNotDeleteGenre");
    formDataDeleteAll("NoneForm");
}

function didDeleteGenre(data) {
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"), "/contents/genre");
    } else if (data.resultCode == "1013") {
        popAlertLayer(getMessage("contents.category.fail.genre.delete"));
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

function didNotDeleteGenre(data) {
    popAlertLayer(getMessage("fail.common.delete"));
}

function splitText(text) {
    var splitIndex = text.lastIndexOf("(");
    var dataArr = [];
    dataArr.push(text.substring(0, splitIndex));
    dataArr.push(text.substring(splitIndex).replace(/[()]/g, ""));
    return dataArr;
}

function clearGridData() {
    $("#jqgridData").clearGridData();
    $("#sp_1_pageDiv").text(1);
}