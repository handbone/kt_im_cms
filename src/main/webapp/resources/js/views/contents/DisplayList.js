/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var modelHidden = true;
var hashSttus;
var hashDisplay;
var selTab = 0;
var gridTotalWidth = 0;
var apiUrl = makeAPIUrl("/api/contentsDisplay");
var gridState;
var tempState;

// 노출콘텐츠관리 화면에서 진행상태(승인완료/전시완료) 변경 시 reload 여부 확인
var isChangeState = false;

var maxRcmdCount = 8;
var rcmdContsSeqArray = new Array();
var dispOnContsArray = new Array();
var rcmdContsIndex = -1;
var isChange = false;
var clickChgBtn = false;

var totalPage = 0;

$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSttus = checkUndefined(findGetParameter(str_hash,"sttus"));
            hashDisplay = checkUndefined(findGetParameter(str_hash,"display"));
            if (hashDisplay == "display" || hashDisplay == null) {
                if ($("#display").val() == "history") {
                    totalPage = 0;
                    $("#jqgridDispHstData").jqGrid("GridUnload");
                    $("#jqgridDispHstData").hide();

                    $("#rsvSelect").show();
                    $("#jqgridDispData").show();
                    $("#display").val(hashDisplay);
                    contentStateList();
                } else if ($("#display").val() == "rcmd") {
                    totalPage = 0;
                    isChange = false;
                    $("#rcmdTitleBar").hide();
                    $("#tableArea").hide();
                    $("#rcmdBtnGrp").hide();

                    $("#rsvSelect").show();
                    $("#gridArea").show();
                    $("#jqgridDispData").show();
                    $("#dispBtnGrp").show();
                    $("#display").val(hashDisplay);
                    contentStateList();
                } else {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                    $("#sttusSelbox").val(hashSttus);
                    jQuery("#jqgridDispData").trigger("reloadGrid");
                }
            } else if (hashDisplay == "rcmd") {
                if ($("#display").val() == "display") {
                    totalPage = 0;
                    $("#rsvSelect").hide();
                    $("#dispBtnGrp").hide();
                    $("#jqgridDispData").jqGrid("GridUnload");
                    $("#jqgridDispData").hide();
                    $("#gridArea").hide();

                    $("#rcmdTitleBar").show();
                    $("#tableArea").show();
                    $("#rcmdBtnGrp").show();
                    $("#display").val(hashDisplay);
                    recommendContentsList();
                } else if ($("#display").val() == "history") {
                    totalPage = 0;
                    $("#jqgridDispHstData").jqGrid("GridUnload");
                    $("#jqgridDispHstData").hide();
                    $("#gridArea").hide();
                    $("#dispBtnGrp").hide();

                    $("#rcmdTitleBar").show();
                    $("#tableArea").show();
                    $("#rcmdBtnGrp").show();
                    $("#display").val(hashDisplay);
                    recommendContentsList();
                } else {
                    recommendContentsList();
                }
            } else {
                if ($("#display").val() == "display") {
                    totalPage = 0;
                    $("#rsvSelect").hide();
                    $("#btnDspOn").hide();
                    $("#btnDspOff").hide();
                    $("#jqgridDispData").jqGrid("GridUnload");
                    $("#jqgridDispData").hide();

                    $("#jqgridDispHstData").show();
                    $("#display").val(hashDisplay);
                    contentDisplayHistoryList();
                } else if ($("#display").val() == "rcmd") {
                    totalPage = 0;
                    isChange = false;
                    $("#rcmdTitleBar").hide();
                    $("#tableArea").hide();
                    $("#rcmdBtnGrp").hide();

                    $("#gridArea").show();
                    $("#jqgridDispHstData").show();
                    $("#dispBtnGrp").show();
                    $("#btnDspOn").hide();
                    $("#btnDspOff").hide();
                    $("#display").val(hashDisplay);
                    contentDisplayHistoryList();
                } else {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                    jQuery("#jqgridDispHstData").trigger("reloadGrid");
                }
            }
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashDisplay = checkUndefined(findGetParameter(str_hash,"display"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#display").val(hashDisplay);
        $("#target").val(searchField);
        $("#keyword").val(searchString);
        isChange = false;

        gridState = "NONE";

        if (hashDisplay == "display" || hashDisplay == null) {
            $("#rcmdTitleBar").hide();
            $("#tableArea").hide();
            $("#rcmdBtnGrp").hide();

            $("#gridArea").show();
            $("#jqgridDispHstData").jqGrid("GridUnload");
            $("#jqgridDispHstData").hide();

            $("#rsvSelect").show();
            $("#jqgridDispData").show();
            $("#dispBtnGrp").show();
            $("#display").val(hashDisplay);

            contentStateList();
        } else if (hashDisplay == "rcmd") {
            $("#rsvSelect").hide();
            $("#dispBtnGrp").hide();
            $("#jqgridDispData").jqGrid("GridUnload");
            $("#jqgridDispData").hide();

            $("#jqgridDispHstData").jqGrid("GridUnload");
            $("#jqgridDispHstData").hide();

            $("#gridArea").hide();

            $("#rcmdTitleBar").show();
            $("#tableArea").show();
            $("#rcmdBtnGrp").show();

            $("#display").val(hashDisplay);

            recommendContentsList();
        } else if (hashDisplay == "history"){
            $("#rcmdTitleBar").hide();
            $("#tableArea").hide();
            $("#rcmdBtnGrp").hide();

            $("#rsvSelect").hide();
            $("#dispBtnGrp").show();
            $("#btnDspOn").hide();
            $("#btnDspOff").hide();
            $("#gridArea").show();
            $("#jqgridDispData").jqGrid("GridUnload");
            $("#jqgridDispData").hide();

            $("#jqgridDispHstData").show();
            $("#display").val(hashDisplay);

            contentDisplayHistoryList();
        }
    } else {
        if ($("#display").val() == "display") {
            $("#rcmdTitleBar").hide();
            $("#tableArea").hide();
            $("#rcmdBtnGrp").hide();

            $("#gridArea").show();
            $("#jqgridDispHstData").jqGrid("GridUnload");
            $("#jqgridDispHstData").hide();

            $("#rsvSelect").show();
            $("#jqgridDispData").show();
            $("#dispBtnGrp").show();

            contentStateList();
        } else if ($("#display").val() == "rcmd") {
            $("#rsvSelect").hide();
            $("#dispBtnGrp").hide();
            $("#jqgridDispData").jqGrid("GridUnload");
            $("#jqgridDispData").hide();

            $("#jqgridDispHstData").jqGrid("GridUnload");
            $("#jqgridDispHstData").hide();

            $("#gridArea").hide();

            $("#rcmdTitleBar").show();
            $("#tableArea").show();
            $("#rcmdBtnGrp").show();

            recommendContentsList();
        } else if ($("#display").val() == "history") {
            $("#rcmdTitleBar").hide();
            $("#tableArea").hide();
            $("#rcmdBtnGrp").hide();

            $("#rsvSelect").hide();
            $("#dispBtnGrp").show();
            $("#btnDspOn").hide();
            $("#btnDspOff").hide();
            $("#gridArea").show();
            $("#jqgridDispData").jqGrid("GridUnload");
            $("#jqgridDispData").hide();

            $("#jqgridDispHstData").show();

            contentDisplayHistoryList();
        }
    }

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });

    $("body").click(function(e) {
        if (selTab == 1) {
            var nodeNm = (event.target) ? event.target.nodeName : e.target.nodeName;
            if((nodeNm != "TD" && nodeNm != "TR" && nodeNm != "TABLE" && nodeNm != "INPUT" && nodeNm != "TH" && nodeNm != "IMG")
                    && $(".popupWinLarge").css("display") != "block") {
                $("#rcmdContents tr").removeClass("selTr");
            } else if ($(".popupWinLarge").css("display") == "block"
                && (nodeNm != "TD" && nodeNm != "TR" && nodeNm != "TABLE" && nodeNm != "INPUT" && nodeNm != "TH" && nodeNm != "SELECT")) {
                $("#dispOnContents tr").removeClass("selTr");
            }
        }
    });
});

contentStateList = function() {
    callByGet("/api/codeInfo?comnCdCtg=CONTS_STTUS&display="+$("#display").val(),"contentStateListSuccess","NoneForm");
}

contentStateListSuccess = function(data) {
    if(data.resultCode == "1000"){
        var sttusListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            sttusListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNm+"</option>";
        });
        $("#sttusSelbox").html(sttusListHtml);

        if (document.location.hash) {
            var str_hash = document.location.hash.replace("#","");
            hashSttus = findGetParameter(str_hash,"sttus");
            $("#sttusSelbox > option[value=" + hashSttus + "]").attr("selected","true");
        }

        if ($("#sttusSelbox option:selected").val() == "05") {
            $("#btnDspOn").show();
            $("#btnDspOff").hide();
        } else {
            $("#btnDspOn").hide();
            $("#btnDspOff").show();
        }

        contentsDisplayList();
    }
}

contentStateChange = function() {
    isChangeState = true;
    jqGridDispReload();

    if ($("#sttusSelbox option:selected").val() == "05") {
        $("#btnDspOn").show();
        $("#btnDspOff").hide();
        $("#btnContsManage").hide();
    } else {
        $("#btnDspOn").hide();
        $("#btnDspOff").show();
        $("#btnContsManage").show();
    }
}

jqGridDispReload = function() {
    var searchSttus = $("#sttusSelbox option:selected").val();

    if (searchSttus != "") {
        searchKey = "?sttus=" + searchSttus;
    } else {
        searchKey = "?sttus=";
    }

    jQuery("#jqgridDispData").setGridParam({"url": apiUrl+searchKey,page:1});
    jQuery("#jqgridDispData").trigger("reloadGrid");
}

contentsDisplayList = function() {
    var colNameList = [
        getMessage("table.thumbnail"),
        getMessage("table.contentsTitle"),
        getMessage("table.category"),
        getMessage("table.reger"),
        getMessage("table.progressStatus"),
        getMessage("table.updatedate"),
        "fileSeq",
        "filePath",
        "orginlFileNm",
        "contsDesc",
        "firstCtgNm",
        "secondCtgNm",
        "cretrID",
        "cretrNm",
        getMessage("table.contentsId"),
        "contsSeq"
    ];

    $("#jqgridDispData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentDisplayList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:colNameList,
        colModel: [
            {name:"thumbnail", index: "thumbnail", align:"center", title:false, resizable:true},
            {name:"contsTitle", index:"CONTS_TITLE", align:"center", formatter:pointercursor},
            {name:"contsCtgNm", index:"CONTS_CTG_NM", align:"center", cellattr: editCellCtgTitleAttr},
            {name:"cretr", index:"CRETR", align:"center", cellattr: editCellCretrTitleAttr},
            {name:"sttusVal", index:"STTUS_VAL", align:"center", cellattr: editCellSttusTitleAttr},
            {name:"amdDt", index:"AMD_DT", align:"center"},
            {name:"fileSeq", index:"FILE_SEQ", hidden:true},
            {name:"filePath", index:"FILE_PATH", hidden:true},
            {name:"orginlFileNm", index:"ORGINL_FILE_NM", hidden:true},
            {name:"contsDesc", index:"CONTS_DESC", hidden:true},
            {name:"firstCtgNm", index:"FIRST_CTG_NM", hidden:true},
            {name:"secondCtgNm", index:"SECOND_CTG_NM", hidden:true},
            {name:"cretrID", index:"CRETR_ID", hidden:true},
            {name:"cretrNm", index:"CRETR_NM", hidden:true},
            {name:"contsID", index:"CONTS_ID", hidden:true},
            {name:"contsSeq", index:"CONTS_SEQ", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        pager: "#dispPageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "CONTS_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: true,
        beforeRequest:function() {
            var myPostData = $('#jqgridDispData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState == "TABVAL") {
                searchField = "";
                searchString = "";
            } else if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);

                // 노출콘텐츠관리 화면에서 진행상태 변경 시 이전 검색 영역 초기화
                if (isChangeState) {
                    $("#target").val("");
                    $("#keyword").val("");
                    isChangeState = false;
                }
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());

            hashSttus = checkUndefined($("#sttusSelbox").val());
            hashDisplay = "display"
            apiUrl = makeAPIUrl("/api/contentsDisplay");
            selTab = 0;

            $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
            $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });
            setHeaderUse();

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if(tempState == "TABVAL" || tempState == "SEARCH") {
                    myPostData.page = 1;
                    page = 1;
                }
                if (tempState != "SEARCH") {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString; myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "") {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            myPostData.sttus = hashSttus;
            myPostData.display = hashDisplay;
            $('#jqgridDispData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page});
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            //session
            if (sessionStorage.getItem("last-url") != null) {
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            // Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /* 뒤로가기 */
            var hashlocation = "page=" + $(this).context.p.page + "&sttus=" + $("#sttusSelbox option:selected").val() + "&display="+$("#display").val();
            if ($(this).context.p.postData._search && $("#target").val() != null) {
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            $(this).find("td").each(function() {
                if (typeof $(this).parents("tr").attr("tabindex") != "undefined") {
                    if ($(this).index() == 1) { // 썸네일
                        var filePath = $(this).parent("tr").find("td:eq(8)").text();
                        var fileNm = $(this).parent("tr").find("td:eq(9)").text();
                        var consDesc = $(this).parent("tr").find("td:eq(10)").text();
                        // 썸네일 사이즈 지정하여 호출, 지정된 썸네일 사이즈의 파일이 없을 경우 원본 이미지를 로드함.
                        var thumbImgSizeStr = "&width=100&height=75";
                        $(this).html("<img class='displayImg' style='min-width:100px;height:75px;background-image:url("
                                + makeAPIUrl(filePath + thumbImgSizeStr, "img") + "),url(" + $("#contextPath").val() + "/resources/image/img_empty.png); "
                                + "background-size: 100%; background-repeat: no-repeat; background-position: center; background-width: 50%;"
                                + " vertical-align: middle; margin: 5px 0px;'>");
                        $(this).append("<div class=\"balloonGrpCustom\"><div class=\"balloon\">" + consDesc + "</div></div>");
                    } else if ($(this).index() == 3) { // 카테고리
                        // 첫번째 카테고리 명과 두번째 카테고리 명 병합 ex) 게임(FPS)
                        var ctgNm = $(this).parent("tr").find("td:eq(11)").text() + "(" + $(this).parent("tr").find("td:eq(12)").text() + ")";
                        $(this).html(ctgNm);
                    } else if ($(this).index() == 4) { // 등록자
                        var cretr = $(this).parent("tr").find("td:eq(14)").text() + "(" + $(this).parent("tr").find("td:eq(13)").text() + ")";
                        $(this).html(cretr);
                    } else if ($(this).index() == 5) { // 진행상태
                        if ($(this).text() == "05") { // 승인완료
                            $(this).html(getMessage("display.progress.status.approval"));
                        } else if ($(this).text() == "06") { // 전시완료)
                            $(this).html("<span class='viewIcon'>"+getMessage("display.progress.status.approval") + "</span>");
                        }
                    }
                }
            });
            $( ".displayImg" ).bind({
                mouseenter: function() {
                    $(this).next("div").css({visibility:"visible",left:$(this).offset().left+$(this).width(),top:$(this).offset().top});
                },
                mouseleave: function() {
                    $(this).next("div").css({visibility:"hidden"});
                }
            });

            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridDispData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridDispData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event) {
            // 콘텐츠 ID 선택시
            if(columnId == 2){
              var list = $("#jqgridDispData").jqGrid('getRowData', rowId);
              sessionStorage.setItem("last-url", location);
              sessionStorage.setItem("state", "view");
              pageMove("/display/"+list.contsSeq+"/edit");
            }
        }
    });
    jQuery("#jqgridDispData").jqGrid('navGrid','#dispPageDiv',{del:false,add:false,edit:false,search:false});
}

function editCellCtgTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = rawObject.firstCtgNm + "(" + rawObject.secondCtgNm + ")";
    return 'title="' + retVal  + '"';
}

function editCellCretrTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = rawObject.cretrNm + "(" + rawObject.cretrID + ")";
    return 'title="' + retVal  + '"';
}

function editCellSttusTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";
    if (rawObject.sttusVal == '05') {
        retVal = getMessage("display.progress.status.approval");
    } else if (rawObject.sttusVal == '06') {
        retVal = getMessage("display.history.display.on");
    }

    return 'title="' + retVal  + '"';
}

contentDisplayHistoryList = function() {
    var colNameList = [
        getMessage("table.num"),
        getMessage("table.contentsId"),
        getMessage("table.contentsTitle"),
        getMessage("table.reger"),
        getMessage("table.status"),
        getMessage("table.updatedate"),
        "cretrID",
        "cretrNm",
        "contsHstSeq"
    ];

    $("#jqgridDispHstData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentDisplayHistoryList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:colNameList,
        colModel: [
            {name:"num", index: "num", align:"center", width:40},
            {name:"contsID", index:"CONTS_ID", align:"center"},
            {name:"contsTitle", index:"CONTS_TITLE", align:"center"},
            {name:"cretr", index:"CRETR", align:"center", cellattr: editCellTitleAttr},
            {name:"actcSttus", index:"ACTC_STTUS", align:"center", cellattr: editCellTitleAttr},
            {name:"amdDt", index:"AMD_DT", align:"center"},
            {name:"cretrID", index:"CRETR_ID", hidden:true},
            {name:"cretrNm", index:"CRETR_NM", hidden:true},
            {name:"contsHstSeq", index:"CONTS_HST_SEQ", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        pager: "#dispHstPageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "CONTS_HST_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: false,
        beforeRequest:function() {
            tempState = gridState;
            var myPostData = $('#jqgridDispHstData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState == "TABVAL") {
                searchField = "";
                searchString = "";
            } else if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());

            hashDisplay = "history"
            apiUrl = makeAPIUrl("/api/contentsDisplayHistory");
            selTab = 2;

            $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
            $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

            setHeaderUse();

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if (tempState == "TABVAL" || tempState == "SEARCH") {
                    myPostData.page = 1;
                    page = 1;
                }
                if (tempState != "SEARCH") {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString; myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "") {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";
            myPostData.display = hashDisplay;
            $('#jqgridDispHstData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page});
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            // session
            if (sessionStorage.getItem("last-url") != null) {
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            // Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /*뒤로가기*/
            var hashlocation = "page=" + $(this).context.p.page + "&display="+$("#display").val();
            if ($(this).context.p.postData._search && $("#target").val() != null) {
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            $(this).find("td").each(function() {
                if (typeof $(this).parents("tr").attr("tabindex") != "undefined") {
                    if ($(this).index() == 3) { // 등록자
                        var cretr = "";
                        if ($(this).parent("tr").find("td:eq(6)").text() == "SYSTEM") {
                            cretr = $(this).parent("tr").find("td:eq(6)").text();
                        } else {
                            cretr = $(this).parent("tr").find("td:eq(7)").text() + "(" + $(this).parent("tr").find("td:eq(6)").text() + ")";
                        }
                        $(this).html(cretr);
                    } else if ($(this).index() == 4) { // 상태
                        if ($(this).text() == '08') { // 추천제외
                            $(this).html(getMessage("display.history.rcmd.off"));
                        } else if ($(this).text() == '06') { // 전시완료
                            $(this).html(getMessage("display.history.display.on"));
                        } else if ($(this).text() == '05') { // 전시제외
                            $(this).html(getMessage("display.history.display.off"));
                        } else { // 추천등록
                            $(this).html(getMessage("display.history.rcmd.on"));
                        }
                    }
                }
            });

            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridDispHstData", "gridArea");
        },
        afterInsertRow: function (rowid, rowData, rowelem) {
            // ROW 변경이 필요할 경우 코드 적용
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridDispHstData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event) {
            // 컬럼 선택 이벤트 필요시 코드 적용
        }
    });
    jQuery("#jqgridDispHstData").jqGrid('navGrid','#dispHstPageDiv',{del:false,add:false,edit:false,search:false});
}

function editCellTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";
    switch(val) {
    case '08':
        retVal = getMessage("display.history.rcmd.off");
        break;
    case '07':
        retVal = getMessage("display.history.rcmd.on");
        break;
    case '06':
        retVal = getMessage("display.history.display.on");
        break;
    case '05':
        retVal = getMessage("display.history.display.off");
        break;
    default:
        if (rawObject.cretrID == "SYSTEM") {
            retVal = rawObject.cretrID;
        } else {
            retVal = rawObject.cretrNm + "(" + rawObject.cretrID + ")";
        }
        break;
    }

    return 'title="' + retVal  + '"';
}

function keywordSearch() {
    gridState = "SEARCH";
    if ($("#display").val() == 'display') {
//        $("#jqgridDispData").trigger("reloadGrid");
        jqGridDispReload();
    } else {
        $("#jqgridDispHstData").trigger("reloadGrid");
    }
}

searchFieldSet = function() {
    var colModel = "";
    var colNames = "";
    var searchHtml = "";

    if ($("#target > option").length == 0) {
        if ($("#display").val() == 'display') {
            colModel = $("#jqgridDispData").jqGrid('getGridParam', 'colModel');
            colNames = $("#jqgridDispData").jqGrid('getGridParam', 'colNames');

            for (var i=0; i<colModel.length; i++) {
                //검색제외추가
                switch(colModel[i].name) {
                case "contsID":break;
                case "contsTitle":break;
                case "contsCtgNm":break;
                case "cretr":break;
                default:continue;
                }
                searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
            }
            $("#target").html(searchHtml);
            if ($("#target option").index() == 0) {
                $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
                $(".target_bak").parent(".selectBox").css("display","table");
            } else {
                $(".target_bak").remove();
            }
        } else {
            colModel = $("#jqgridDispHstData").jqGrid('getGridParam', 'colModel');
            colNames = $("#jqgridDispHstData").jqGrid('getGridParam', 'colNames');

            for (var i=0; i<colModel.length; i++) {
                //검색제외추가
                switch(colModel[i].name) {
                case "contsID":break;
                case "contsTitle":break;
                default:continue;
                }
                searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
            }
            $("#target").html(searchHtml);
            if ($("#target option").index() == 0) {
                $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
                $(".target_bak").parent(".selectBox").css("display","table");
            } else {
                $(".target_bak").remove();
            }
        }
    }
    $("#target").val(searchField);
}

function tabVal(seq, e) {
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
    $(e).parent("div").removeClass("tabOff").addClass("tabOn").css({ 'pointer-events': 'none' });

    selTab = seq;
    $("#target").html("");
    $("#keyword").val("");

    totalPage = 0;

    isChange = false;

    if (selTab == 0) { // 노출 콘텐츠 관리
        $("#display").val("display");
        $("#rcmdTitleBar").hide();
        $("#tableArea").hide();
        $("#rcmdBtnGrp").hide();

        $("#gridArea").show();
        $("#jqgridDispHstData").jqGrid("GridUnload");
        $("#jqgridDispHstData").hide();

        $("#rsvSelect").show();
        $("#jqgridDispData").show();
        $("#dispBtnGrp").show();

        contentStateList();
    } else if (selTab == 1) { // 메인 노출 콘텐츠
        $("#display").val("rcmd");
        $("#sttusSelbox").val("");
        $("#rsvSelect").hide();
        $("#dispBtnGrp").hide();
        $("#jqgridDispData").jqGrid("GridUnload");
        $("#jqgridDispData").hide();

        $("#jqgridDispHstData").jqGrid("GridUnload");
        $("#jqgridDispHstData").hide();
        $("#gridArea").hide();

        $("#rcmdTitleBar").show();
        $("#tableArea").show();
        $("#rcmdBtnGrp").show();

        gridState = "READY";
        var hashlocation = "&display="+$("#display").val();
        document.location.hash = hashlocation;
    } else if (selTab == 2) { // 노출 완료 이력
        $("#display").val("history");
        $("#rcmdTitleBar").hide();
        $("#tableArea").hide();
        $("#rcmdBtnGrp").hide();

        $("#sttusSelbox").val("");
        $("#rsvSelect").hide();
        $("#gridArea").show();
        $("#jqgridDispData").jqGrid("GridUnload");
        $("#jqgridDispData").hide();
        $("#dispBtnGrp").show();
        $("#btnDspOn").hide();
        $("#btnDspOff").hide();

        $("#jqgridDispHstData").show();

        contentDisplayHistoryList();
    }
}

// 콘텐츠 전시
changeDisplayState = function(state) {
    var contsSeqList = "";
    var contsTitleList = "";
    var selCnt = 0;

    for (var i = 1; i<$("#limit").val()+1; i++) {
        if ($("#jqg_jqgridDispData_" + i).prop("checked")) {
            var selContsSeq = $("#jqgridDispData").jqGrid('getRowData', i).contsSeq;
            var selContsTitle = $("#jqgridDispData").jqGrid('getRowData', i).contsTitle;
            contsSeqList += selContsSeq + ",";
            contsTitleList += selContsTitle + ", ";
            selCnt++;
        }
    }

    if (selCnt == 0) {
        popAlertLayer(getMessage("display.content.not.select.msg"));
        return;
    }

    if (state == "displayOn") {
        popConfirmLayer(contsTitleList.slice(0, -2) + " " + getMessage("display.content.display.on.confirm.msg"), function() {
            updateDispSttus("true", contsSeqList);
        }, null, getMessage("common.confirm"));
    } else {
        popConfirmLayer(contsTitleList.slice(0, -2) + " " + getMessage("display.content.display.off.confirm.msg"), function() {
            updateDispSttus("false", contsSeqList);
        }, null, getMessage("common.confirm"));
    }
}

updateDispSttus = function(dispSttus, contsSeqList) {
    formData("NoneForm", "dispOn", dispSttus);
    formData("NoneForm", "contsSeqList", contsSeqList.slice(0, -1));
    callByPut("/api/contentsDisplay", "displayChangeStateSuccess", "NoneForm", "displayChangeStateFail");
}

displayChangeStateSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        if (data.result.dispOn == "Y") {
            popAlertLayer(getMessage("display.content.display.on.success.msg"));
            jqGridDispReload();
        } else {
            popAlertLayer(getMessage("display.content.display.off.success.msg")/*, $(location).attr('pathname') + "" + $(location).attr('search')*/);
            jqGridDispReload();
        }
    } else {
        popAlertLayer(getMessage("display.content.fail.msg"));
    }
}

displayChangeStateFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("display.content.fail.msg"));
}

pageMoveRecommendContents = function() {
    var totalCount = $("#jqgridDispData").getGridParam("records");
    if (totalCount > 0) {
        pageMove('/recommend');
    } else {
        popAlertLayer(getMessage("display.content.display.on.list.empty.msg"));
    }
}

//추천 콘텐츠 리스트 정보 가져오기
recommendContentsList = function() {
    selTab = 1;

    $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
    $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

    callByGet("/api/recommendContents", "recommendContentsListSuccess", "NoneForm");
}

recommendContentsListSuccess = function(data) {
    rcmdContsSeqArray = new Array();
    var recommendContentHtml = "";
    if (data.resultCode == "1000") {
        $(data.result.recommendContentList).each(function(i, item) {
            recommendContentHtml += "<tr>";
            recommendContentHtml += "<td class=\"disp-ellipsis\">" + item.contsTitle + "</td>";
            recommendContentHtml += "<td class=\"disp-ellipsis\">[" + item.firstCtgNm + "]" + item.secondCtgNm + "</td>";
            recommendContentHtml += "<td>" + item.amdDt + "</td>";
            recommendContentHtml += "<td><input type=\"button\" class=\"btnNormalWhite\"  onclick=\"dispOnContsList(this)\" value=\"" + getMessage("button.change") + "\">&nbsp;"
                                    + "<input type=\"button\" class=\"btnNormalWhite\" onClick=\"deleteRcmdConts(this)\" value=\"" + getMessage("button.delete") + "\"></td>";
            recommendContentHtml += "<td><input type=\"hidden\" name=\"rcmdContsSeq\" value=\"" + item.contsSeq + "\"></td>";
            recommendContentHtml += "</tr>";

            rcmdContsSeqArray.push({contsSeq: item.contsSeq});
        });

        $("#rcmdContents").html(recommendContentHtml);

        var rcmdContsCnt = $("#rcmdContents tr").length;
        if (rcmdContsCnt < maxRcmdCount) {
            recommendContentHtml = "";
            for (var i = rcmdContsCnt; i < maxRcmdCount; i++) {
                recommendContentHtml += "<tr>";
                recommendContentHtml += "<td></td>";
                recommendContentHtml += "<td></td>";
                recommendContentHtml += "<td></td>";
                recommendContentHtml += "<td><input type=\"button\" class=\"btnNormalWhite\"  onclick=\"dispOnContsList(this)\" value=\""
                                        + getMessage("button.addition") + "\"></td>";
                recommendContentHtml += "<td></td>";
                recommendContentHtml += "</tr>";
            }

            if (recommendContentHtml != "") {
                $("#rcmdContents").append(recommendContentHtml);
            }
        }
    } else {
        recommendContentHtml = "";
        for (var i = 0; i < maxRcmdCount; i++) {
            recommendContentHtml += "<tr>";
            recommendContentHtml += "<td></td>";
            recommendContentHtml += "<td></td>";
            recommendContentHtml += "<td></td>";
            recommendContentHtml += "<td><input type=\"button\" class=\"btnNormalWhite\"  onclick=\"dispOnContsList(this)\" value=\""
                                    + getMessage("button.addition") + "\"></td>";
            recommendContentHtml += "<td></td>";
            recommendContentHtml += "</tr>";
        }

        $("#rcmdContents").html(recommendContentHtml);
    }

    $("#rcmdContents td").on("click", function() {
        if ($(this).parents("tr").hasClass("selTr")) {
            // cell click 후 추가 또는 변경 버튼 클릭 시 cell select 상태가 해제되는 오류로 코드 추가
            if (!clickChgBtn) {
                $("#rcmdContents tr").removeClass("selTr");
            } else {
                clickChgBtn = false;
            }
        } else {
            $("#rcmdContents tr").removeClass("selTr");
            $(this).parents("tr").addClass("selTr");
        }
    });

    dispOnContsListInfo();
}

dispOnContsListInfo = function() {
    callByGet("/api/contentsDisplay?list=dispOnList", "dispOnContsListSuccess", "NoneForm");
}

dispOnContsListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var displayOncontentHtml = "";

        $(data.result.contentDisplayOnList).each(function(i, item) {
            var isRcmd = false;
            if (rcmdContsSeqArray.length > 0) {
                for (var j = 0; j < rcmdContsSeqArray.length; j++) {
                    var rcmdContsSeq = rcmdContsSeqArray[j].contsSeq;
                    if (rcmdContsSeq == item.contsSeq) {
                        isRcmd = true;
                        break;
                    }
                }

                if (!isRcmd) {
                    displayOncontentHtml += "<tr>";
                    displayOncontentHtml += "<td class=\"disp-ellipsis\">" + item.contsTitle + "</td>";
                    displayOncontentHtml += "<td class=\"disp-ellipsis\">[" + item.firstCtgNm + "]" + item.secondCtgNm + "</td>";
                    displayOncontentHtml += "<td>" + item.amdDt + "</td>";
                    displayOncontentHtml += "<td></td>";
                    displayOncontentHtml += "<td><input type=\"hidden\" name=\"rcmdContsSeq\" value=\"" + item.contsSeq + "\"></td>";
                    displayOncontentHtml += "</tr>";
                }
            } else {
                displayOncontentHtml += "<tr>";
                displayOncontentHtml += "<td class=\"disp-ellipsis\">" + item.contsTitle + "</td>";
                displayOncontentHtml += "<td class=\"disp-ellipsis\">[" + item.firstCtgNm + "]" + item.secondCtgNm + "</td>";
                displayOncontentHtml += "<td>" + item.amdDt + "</td>";
                displayOncontentHtml += "<td></td>";
                displayOncontentHtml += "<td><input type=\"hidden\" name=\"rcmdContsSeq\" value=\"" + item.contsSeq + "\"></td>";
                displayOncontentHtml += "</tr>";
            }
        });

        $("#dispOnContents").html(displayOncontentHtml);

        $("#dispOnContents td").on("click", function() {
            if ($(this).parents("tr").hasClass("selTr")) {
                $("#dispOnContents tr").removeClass("selTr");
            } else {
                $("#dispOnContents tr").removeClass("selTr");
                $(this).parents("tr").addClass("selTr");
            }
        });
    }
}

//추천 콘텐츠 우선 순위 관리 - Up
recommendContentsRankUp = function() {
    if ($("#rcmdContents").find(".selTr").index() == -1) {
        popAlertLayer(getMessage("display.content.not.select.msg"));
        return;
    }

    var rcmdContent = $("#rcmdContents").find(".selTr");
    if (rcmdContent.find("input[value='" + getMessage("button.addition") + "']").length > 0
        || rcmdContent.prev().find("input[value='" + getMessage("button.addition") + "']").length > 0) {
        return;
    }

    // 변경 여부를 true로 설정
    isChange = true;
    rcmdContent.prev().before(rcmdContent);
}

//추천 콘텐츠 우선 순위 관리 - Down
recommendContentsRankDown = function() {
    if ($("#rcmdContents").find(".selTr").index() == -1) {
        popAlertLayer(getMessage("display.content.not.select.msg"));
        return;
    }

    var rcmdContent = $("#rcmdContents").find(".selTr");
    if (rcmdContent.find("input[value='" + getMessage("button.addition") + "']").length > 0
        || rcmdContent.next().find("input[value='" + getMessage("button.addition") + "']").length > 0) {
        return;
    }

    // 변경 여부를 true로 설정
    isChange = true;
    rcmdContent.next().after(rcmdContent);
}

function dispOnContsList(e) {
    // cell click 후 추가 또는 변경 버튼 클릭 시 cell select 상태가 해제되는 오류로 코드 추가
    if (!clickChgBtn) {
        clickChgBtn = true;
    }
    rcmdContsIndex = $(e).parents("tr").index();

    // 이전 검색결과 초기화
    searchDispOnContsReset();

    $.blockUI({
        message: $('.popupWinLarge'),
        //onOverlayClick: $.closePopup(),
        css:{border:'0px',cursor:'default',top:'8%',left:'28%',width:'800px',height:'auto'}
    });

    checkScroll();
}

function deleteRcmdConts(e) {
    // 변경 여부를 true로 설정
    isChange = true;

    // 삭제될 추천 콘텐츠 정보 저장하여 삭제 후 전시완료 콘텐츠 리스트에 추가
    var rcmdConts = $(e).parents("tr");

    $(e).parents("tr").remove();

    var recommendContentHtml = "";
    recommendContentHtml += "<tr>";
    recommendContentHtml += "<td></td>";
    recommendContentHtml += "<td></td>";
    recommendContentHtml += "<td></td>";
    recommendContentHtml += "<td><input type=\"button\" class=\"btnNormalWhite\"  onclick=\"dispOnContsList(this)\" value=\""
                            + getMessage("button.addition") + "\"></td>";
    recommendContentHtml += "<td></td>";
    recommendContentHtml += "</tr>";

    $("#rcmdContents").append(recommendContentHtml);

    // 삭제된 추천 콘텐츠 정보에서 버튼 정보를 삭제하고 전시완료 콘텐츠 리스트에 추가
    rcmdConts.children().eq(2).removeClass("hiddenFont"); // 전시완료 콘텐츠 목록으로 append 할 때 등록일 숨김 처리한 것을 원복
    rcmdConts.children().eq(3).html("");
    var dispConts = "<tr>"+rcmdConts.html()+"</tr>"
    $("#dispOnContents").append(dispConts);

    $("#rcmdContents td").off("click");
    $("#dispOnContents td").off("click");

    $("#rcmdContents td").on("click", function() {
        if ($(this).parents("tr").hasClass("selTr")) {
            // cell click 후 추가 또는 변경 버튼 클릭 시 cell select 상태가 해제되는 오류로 코드 추가
            if (!clickChgBtn) {
                $("#rcmdContents tr").removeClass("selTr");
            } else {
                clickChgBtn = false;
            }
        } else {
            $("#rcmdContents tr").removeClass("selTr");
            $(this).parents("tr").addClass("selTr");
        }
    });

    $("#dispOnContents td").on("click", function() {
        if ($(this).parents("tr").hasClass("selTr")) {
            $("#dispOnContents tr").removeClass("selTr");
        } else {
            $("#dispOnContents tr").removeClass("selTr");
            $(this).parents("tr").addClass("selTr");
        }
    });
}

function closeDispOnContsListPopup(){
    $.unblockUI();
    if ($("#dispOnContents").find(".selTr").index() > -1) {
        // 추천 콘텐츠가 지정되어 있지 않은 항목에서 추가 시 비어 있는 항목 중 맨 처음 항목에 추천 콘텐츠가 들어갈 수 있도록 인덱스 조회.
        var emptyIndex = $("#rcmdContents").find("input[value='" + getMessage("button.addition") + "']").eq(0).parents("tr").index();
        if (rcmdContsIndex > emptyIndex) {
            var tr = $("#rcmdContents tr").eq(rcmdContsIndex);
            tr.removeClass();
            rcmdContsIndex = emptyIndex;
        }

        // 추천 콘텐츠 목록의 선택된 콘텐츠를 저장하고 버튼 영역은 삭제
        var rcmdConts = $("#rcmdContents tr").eq(rcmdContsIndex);
        rcmdConts.removeClass();

        var btnHtml = "<input type=\"button\" class=\"btnNormalWhite\"  onclick=\"dispOnContsList(this)\" value=\""
            + getMessage("button.change") + "\">&nbsp;<input type=\"button\" class=\"btnNormalWhite\" onClick=\"deleteRcmdConts(this)\" value=\""
            + getMessage("button.delete") + "\">";

        $("#rcmdContents tr").eq(rcmdContsIndex).replaceWith($("#dispOnContents").find(".selTr"));
        $("#rcmdContents tr").eq(rcmdContsIndex).children().eq(2).addClass("hiddenFont"); // 전시완료 콘텐츠의 등록일을 숨김 처리
        $("#rcmdContents tr").eq(rcmdContsIndex).children().eq(3).html(strConv(btnHtml));

        if (rcmdConts.eq(0).text() != "") {
            rcmdConts.children().eq(2).removeClass("hiddenFont"); // 전시완료 콘텐츠 목록으로 append 할 때 등록일 숨김 처리한 것을 원복
            $("#dispOnContents").append(rcmdConts);
        }

        $("#rcmdContents td").off("click");
        $("#dispOnContents td").off("click");

        $("#rcmdContents td").on("click", function() {
            if ($(this).parents("tr").hasClass("selTr")) {
                // cell click 후 추가 또는 변경 버튼 클릭 시 cell select 상태가 해제되는 오류로 코드 추가
                if (!clickChgBtn) {
                    $("#rcmdContents tr").removeClass("selTr");
                } else {
                    clickChgBtn = false;
                }
            } else {
                $("#rcmdContents tr").removeClass("selTr");
                $(this).parents("tr").addClass("selTr");
            }
        });

        $("#dispOnContents td").on("click", function() {
            if ($(this).parents("tr").hasClass("selTr")) {
                $("#dispOnContents tr").removeClass("selTr");
            } else {
                $("#dispOnContents tr").removeClass("selTr");
                $(this).parents("tr").addClass("selTr");
            }
        });

        // 변경 여부를 true로 설정
        isChange = true;
    }

    // cell click 후 추가 또는 변경 버튼 클릭 시 cell select 상태가 해제되는 오류로 코드 추가
    if (clickChgBtn) {
        clickChgBtn = false;
    }
}

function searchDispOnContsReset() {
    $("#searchKeyword").val("");
    searchDispOnConts();
}

function searchDispOnConts() {
    // 선택된 항목을 선택 정보를 해제
    $("#dispOnContents").find(".selTr").removeClass("selTr");

    var searchConts = $("#searchKeyword").val();
    var filter = searchConts.toUpperCase();
    var table = document.getElementById("dispOnContents");
    var tr = table.getElementsByTagName("tr");
    var td;
    for (var i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
        }
    }

    checkScroll();
}

checkScroll = function() {
    $(".disp-scroll-table").each(function() {
        if ($(this).find(".tableList").height() > 276) {
            $(this).find(".tableList").children("thead").find("th:eq(0)").css("width","304px !important");
            $(this).find(".tableList").children("thead").find("th:eq(1)").css("width","202px !important");
            $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"184px !important","padding-right":"16px"});
        } else {
            $(this).find(".tableList").children("thead").find("th:eq(0)").css("width","320px !important");
            $(this).find(".tableList").children("thead").find("th:eq(2)").css("width","218px");
            $(this).find(".tableList").children("thead").find("th:eq(3)").css({"width":"200px","padding-right":"0px"});
        }
    })
}

function registRecommendContents() {
    if (!isChange) {
        popAlertLayer(getMessage("display.recommend.not.change.msg"));
        return;
    }

    popConfirmLayer(getMessage("display.recommend.change.regist.confirm.msg"), function() {
        var rcmdContsSeq = "";
        $("#rcmdContents tr").each(function() {
            var seqVal = $(this).children().find("input[name='rcmdContsSeq']").val();
            if (seqVal != undefined) {
                rcmdContsSeq += seqVal + ",";
            }
        });

        formData("NoneForm" , "rcmdContsSeq", rcmdContsSeq.slice(0,-1));
        $("input[name=_method]").val("POST");
        callByPost("/api/recommendContents", "registRecommendContentsSuccess", "NoneForm", "registRecommendContentsFail");
    }, null, getMessage("common.confirm"));
}

registRecommendContentsSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayerFnc(getMessage("success.common.insert"), reload);
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

registRecommendContentsFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

function cancelRegist() {
    if (!isChange) {
        popAlertLayer(getMessage("display.recommend.not.change.msg"));
        return;
    }

    popConfirmLayer(getMessage("display.recommend.change.cancel.confirm.msg"), function() {
        location.reload();
    }, null, getMessage("common.confirm"));
}

/*
function myFunction() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      if (td) {
        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }
*/