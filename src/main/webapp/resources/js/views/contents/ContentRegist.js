/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var FILE_VIDEO;
var FILE_THUMBNAIL;
var FILE_METADATA;
var FILE_VIDEO;
var FILE_PREV;
var FILE_PREVMETA;
var FILE_PREVCONT;
var FILE_COVERIMG;
var FILE_CONTENTS;
var FILE_FILE1;
var FILE_SETTINGS    = {
        // Backend Settings
//        upload_url                        : makeAPIUrl("/api/fileProcess"),
        upload_url                        : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                        : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler        : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                    : -110,
        button_cursor                    : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
    };

//swfupload - cover
coverImgUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'coverImg'};
    var file_settings ={};
    file_settings.button_placeholder_id = "coverImg_btn_placeholder";
    file_settings.button_width = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.jpg;*.jpeg;*.png;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "20MB";
    file_settings.button_image_url = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true, file_settings, FILE_SETTINGS);
    FILE_COVERIMG = new SWFUpload(file_settings);
}

//swfupload - thumbnail
thumbnailUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'thumbnail'};
    var file_settings = {};
    file_settings.button_placeholder_id = "thumbnail_btn_placeholder";
    file_settings.button_width = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.jpg;*.jpeg;*.png;",
    file_settings.file_queue_limit =  "0";
    file_settings.file_size_limit =  "20MB";
    file_settings.button_image_url = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true, file_settings, FILE_SETTINGS);
    FILE_THUMBNAIL = new SWFUpload(file_settings);
}
//swfupload - video
videoUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'video'};
    var file_settings ={};
    file_settings.button_placeholder_id = "video_btn_placeholder";
    file_settings.button_width = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.mp4;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true, file_settings, FILE_SETTINGS);
    FILE_VIDEO = new SWFUpload(file_settings);
}

//swfupload - PREV
prevUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'prev'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "prev_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.mp4;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_PREV  = new SWFUpload(file_settings);
}

//swfupload - metadata
prevmetaUploadBtn = function(seq){
    //File Upload screenshot
    var temp = {fileSe : 'prevmeta', fileSeq : seq};
    var file_settings   ={};
    file_settings.button_placeholder_id = "prevmeta_btn_placeholder"+seq;
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.xml;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_PREVMETA   = new SWFUpload(file_settings);
}

//swfupload - metadata
metadataUploadBtn = function(seq){
    //File Upload screenshot
    var temp = {fileSe : 'metadata', fileSeq : seq};
    var file_settings ={};
    file_settings.button_placeholder_id = "metadata_btn_placeholder"+seq;
    file_settings.button_width = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.xml;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true, file_settings, FILE_SETTINGS);
    FILE_METADATA = new SWFUpload(file_settings);
}


//swfupload - file1
fileUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'file'};
    var file_settings ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.zip;*.exe;",
    file_settings.file_queue_limit =  "1";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true, file_settings, FILE_SETTINGS);
    FILE_FILE1 = new SWFUpload(file_settings);
}

$(document).ready(function(){
    var now=new Date();

    $("#cntrctStDt").val(dateString((new Date(now.getTime()))));
    $("#cntrctFnsDt").val(dateString((new Date(now.getTime()))));

    $(".datepicker").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
    });

    contsCtgList();
    coverImgUploadBtn();
    videoUploadBtn();
    thumbnailUploadBtn();
    fileUploadBtn();
    CPList();
    GenreList();
    ServiceList();
    prevUploadBtn();
});

contsCtgList = function(){
    formData("NoneForm", "GroupingYN", "Y");
    if($("#firstCtg option:selected").val()){
        formData("NoneForm", "firstCtgNm", encodeURI($("#firstCtg option:selected").val()));
    }
    callByGet("/api/contentsCategory" , "contsCtgListSuccess", "NoneForm");
}

var _firstLoading = true;
var curYear = new Date().getFullYear(); // 현재 날짜의 년도(ContentsID에서 사용)
contsCtgListSuccess = function(data){
    formDataDelete("NoneForm", "GroupingYN");
    formDataDelete("NoneForm", "firstCtgNm");
    if(data.resultCode == "1000"){
        var contsCtgListHtml = "";
        var firstCtgNmListHtml= "";

        $(data.result.firstCtgList).each(function(i,item) {
            firstCtgNmListHtml += "<option value='"+item.firstCtgNm+"' data-id='"+item.firstCtgId+"'>"+item.firstCtgNm+"</option>";
        });

        if(_firstLoading){
            $("#firstCtg").html(firstCtgNmListHtml);
            _firstLoading = false;
        }

        $(data.result.contsCtgList).each(function(i,item) {
            if(i == 0){
                $("#contsID").text(item.firstCtgID + item.secondCtgID + curYear + prependZero(item.ctgCnt, 5));
            }
            contsCtgListHtml += "<option value='"+item.contsCtgSeq+"' name=\"" + item.firstCtgID + item.secondCtgID + item.ctgCnt + "\">"+item.secondCtgNm+"</option>";
        });

        // 첫번째 카테고리 명이 '영상'이 아닐때
        if ($("#firstCtg option:selected").val() != getMessage("contents.category.video")) {
            $(".attrPart").hide();
            $(".gamePart").show();
            $(".videoPart").hide();
        } else if ($("#firstCtg option:selected").val() == getMessage("contents.category.video")) { // 첫번째 카테고리 명이 '영상'일때
            $(".attrPart").hide();
            $(".gamePart").hide();
            $(".videoPart").show();
            $(".exeFilePath").hide();
            $("#exeFilePath").val("");
        }
        $("#secondCtg").html(contsCtgListHtml);

    }
}

// 카테고리 변경시 콘텐츠 ID 최신화
contentsIDRefresh = function(){
    var categoryInfo = $("#secondCtg option:selected").attr("name");
    $("#contsID").text(categoryInfo.slice(0,2) + curYear + prependZero(categoryInfo.slice(2), 5));
}

// 콘텐츠 ID 년도 뒷부분 숫자 앞 빈칸 0으로 채우기
function prependZero(num, len) {
    while(num.toString().length < len) {
        num = "0" + num;
    }
    return num;
}

contentsRegister = function(){
    if($("#contsTitle").val().trim() == ""){
        popAlertLayer(getMessage("msg.contents.input.contsNm"));
        return;
    }
    if($("#coverImgArea").children().length <= 0){
        popAlertLayer(getMessage("msg.contents.input.coverImg"));
        return;
    }
    if($("#thumbnailArea").children().length <= 0){
        popAlertLayer(getMessage("msg.contents.input.thumnail"));
        return;
    }
    if($("#contsSubTitle").val().trim() == ""){
        popAlertLayer(getMessage("msg.contents.input.desc"));
        return;
    }
    if($("#contsDesc").val().trim() == ""){
        popAlertLayer(getMessage("msg.contents.input.detail.desc"));
        return;
    }

    if($("#firstCtg option:selected").val() == getMessage("contents.category.video")){
        if($("#videoArea").children().length <= 0){
            popAlertLayer(getMessage("msg.contents.input.video"));
            return;
        }

        if($("#prevArea").children().length <= 0){
            popAlertLayer(getMessage("msg.contents.input.video"));
            return;
        }
    }


    var genre="";
    $("#genreList .buttonBoxOn").each(function(i, item){
        genre += $(this).attr("data-seq") + ",";
    });

    var service="";
    $("#serviceList .buttonBoxOn").each(function(i, item){
        service += $(this).attr("data-seq") + ",";
    });

    if(!genre){
        popAlertLayer(getMessage("msg.contents.select.genre"));
        return;
    }

    if ($("#firstCtg option:selected").val() == getMessage("contents.category.video")) {
        formData("NoneForm" , "fileType", "VIDEO");
    } else {
        formData("NoneForm" , "fileType", $("#exefileType option:selected").val());
    }
    formData("NoneForm" , "contsTitle", $("#contsTitle").val());
    formData("NoneForm" , "contsCtgSeq", $("#secondCtg option:selected").val());
    formData("NoneForm" , "contsID", $("#contsID").text());
    formData("NoneForm" , "exeFilePath", $("#exeFilePath").val());
    formData("NoneForm" , "contsSubTitle", $("#contsSubTitle").val());
    formData("NoneForm" , "contsDesc", $("#contsDesc").val());
    formData("NoneForm" , "maxAvlNop", $("#maxAvlNop").val());
    formData("NoneForm" , "genre", genre.slice(0, -1));
    formData("NoneForm" , "service", service.slice(0, -1));
    formData("NoneForm" , "cpSeq", $("#cpList option:selected").val());
    formData("NoneForm" , "cntrctStDt", $("#cntrctStDt").val());
    formData("NoneForm" , "cntrctFnsDt", $("#cntrctFnsDt").val());

    callByPost("/api/contents" , "contentsInsertSuccess", "NoneForm");
}


contentsInsertSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if(data.resultCode == "1000"){
        $("#contsSeq").val(data.result.contsSeq);
        uploadS();
    }
}

function uploadS(){
    if(thumbnailUploadTemp != undefined){
        thumbnailUploadTemp.startUpload();
    }
    if(videoUploadTemp != undefined){
        videoUploadTemp.startUpload();
    }

    if(prevUploadTemp != undefined){
        prevUploadTemp.startUpload();
    }

    $(prevmetaUploadTemp).each(function(){
        this.startUpload();
    });

    $(metadataUploadTemp).each(function(){
        this.startUpload();
    });
    if(coverImgUploadTemp != undefined){
        coverImgUploadTemp.startUpload();
    }
    if(fileTemp != undefined){
        fileTemp.startUpload();
    }
}
/*모든파일등록성공시 호출 (xml 등록)*/
contentsXmlCreate = function(){
    if($("#firstCtg option:selected").val() != getMessage("contents.category.video")) {
        popAlertLayer(getMessage("success.common.insert"), "/contents");
    } else {
        CALL_COUNT++;
        CALL_TOT_COUNT++;
        var videoFormData = "";
        $("table[id ^= videoItem]").each(function(i,item){
            videoFormData += $(this).find("input[name=videoPath]").val()+",";
            videoFormData += $(this).find("input[name=metadataPath]").val()+"GNB";
        });

        formData("NoneForm" , "contsSeq", $("#contsSeq").val());
        formData("NoneForm" , "contsID", $("#contsID").text());
        formData("NoneForm" , "videoFormData", videoFormData.slice(0,-3));
        callByPost("/api/contentsXml", "contentsXmlSuccess", "NoneForm" );
    }
}

contentsXmlSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.insert"), "/contents");
    }
}
fileInsertSuccess = function(data){
    CALL_SUCCESS_COUNT++;
    jsCallComplete();
}

fileQueueDelete = function(id){
    $("#"+id).html("");
    $("#"+id).hide("");

    if(id == "thumbnailArea"){
        thumbnailUploadTemp.cancelUpload();
    } else if(id == "coverImgArea"){
        coverImgUploadTemp.cancelUpload();
    } else if(id == "fileArea"){
        fileTemp.cancelUpload();
    } else if(id == "videoArea"){
        videoUploadTemp.cancelUpload();
    }
}

function GenreList(){
    callByGet("/api/contents/genre?firstCtgId="+$("#firstCtg").find(':selected').data("id"), "GenreListSuccess");
}

function GenreListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $(data.result.codeList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\""+item.comnCdSeq+"\" value=\""+item.comnCdNM+"\" class=\"buttonBox\"/ onclick=\"buttonBoxToggle(this);\"> ";
        });
        $("#genreList").append(menuHtml);
    }
}

function ServiceList(){
    callByGet("/api/service?searchType=list", "ServiceListSuccess");
}

function ServiceListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $(data.result.serviceNmList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\""+item.svcSeq+"\" value=\""+item.svcNm+"\" class=\"buttonBox\"/ onclick=\"buttonBoxToggle(this);\"> ";
        });
        $("#serviceList").append(menuHtml);
    }
}

function CPList(){
    callByGet("/api/cp?rows=10000", "CPListSuccess");
}

function CPListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "<option value=''>== "+getMessage("table.select")+" ==</option>";
        $(data.result.cpList).each(function(i, item){
            menuHtml += "<option value=\""+item.cpSeq+"\">"+item.cpNm+"</option>";
        });
        $("#cpList").append(menuHtml);
    }
}

function buttonBoxToggle(e){
    $(e).toggleClass("buttonBox");
    $(e).toggleClass("buttonBoxOn");
}