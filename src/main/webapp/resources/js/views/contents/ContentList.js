/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hashReady = false;
var searchField;
var searchString;
var readyPage = false;
var setSttus = false;
var hashSttus;
var apiUrl = makeAPIUrl("/api/contents");
var gridState;
var totalPage = 0;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
})

$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSttus = checkUndefined(findGetParameter(str_hash,"sttus"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#selectBox").val(hashSttus);
            contentList();

        } else {
            gridState = "READY";
        }
    });
})

$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashSttus = checkUndefined(findGetParameter(str_hash,"sttus"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);

        if (readyPage) {
            $("#selectBox").val(hashSttus);
        } else {
            setSttus = true;
        }

        gridState = "NONE";

        $("#keyword").val(searchString);
    }

    contentStateList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

contentList = function(){
    option_common['rowNum'] = $("#limit").val();
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

contentStateList = function(){
    callByGet("/api/codeInfo?comnCdCtg=CONTS_STTUS&remoteCode=06","contentStateListSuccess","NoneForm");
}
contentStateListSuccess = function(data){
    if(data.resultCode == "1000"){
        var sttusListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.codeList).each(function(i,item) {
            sttusListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNm+"</option>";
        });
        if( !readyPage ) {
            readyPage = true;
        }

        $("#selectBox").html(sttusListHtml);

        if (setSttus){
            $("#selectBox").val(hashSttus);
        }

        contentList();
    }
}

function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        var searchHtml = "";

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "contsID":break;
            case "contsTitle":break;
            case "contCtgNm":break;
            case "cretrNm":break;
            default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

var option_common = {
    url:apiUrl,
    datatype: "json", // 데이터 타입 지정
    jsonReader : {
        page: "result.currentPage",
        total: "result.totalPage",
        root: "result.contentsList",
        records: "result.totalCount",
        repeatitems: false
        },
    colNames:[
        getMessage("column.title.num"),
        getMessage("table.thumbnail"),
        getMessage("column.title.contentID"),
        getMessage("column.title.contentNm"),
        getMessage("column.title.category"),
        getMessage("column.title.register"),
        getMessage("table.progressStatus"),
        getMessage("column.title.cretDt"),
        getMessage("column.title.contsSeq"),
        "filePath"
    ],
    colModel:[
        {name:"num", index: "num", align:"center", width:20,hidden:true},
        {name:"thumbnail", index: "thumbnail", align:"center", title:false, resizable:true},
        {name:"contsID", index:"CONTS_ID", align:"center"},
        {name:"contsTitle", index:"CONTS_TITLE", align:"center",formatter:pointercursor},
        {name:"contCtgNm", index:"CTG_NM", align:"center"},
        {name:"cretrNm", index:"CRETR_ID", align:"center"},
        {name:"sttus", index:"STTUS", align:"center"},
        {name:"cretDt", index:"CRET_DT", align:"center"},
        {name:"contsSeq", index:"CONTS_SEQ", hidden:true},
        {name:"filePath", index:"FILE_PATH", hidden:true},
    ],
    autowidth: true, // width 자동 지정 여부
    rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
    //rowList:[10,20,30],
    pager: "#pageDiv", // 페이징 할 부분 지정
    viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
    sortname: "contsSeq",
    sortorder: "desc",
//          caption:"편성 목록",
    height: "auto",
    multiselect: false,
    beforeRequest:function(){
        tempState = gridState;
        var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

        if (gridState != "SEARCH") {
            $("#target").val(searchField);
            $("#keyword").val(searchString);
        }

        if (isNaN(page)) {
            page =1;
        }

        if (totalPage > 0 && totalPage < page) {
            page = 1;
        }

        searchField = checkUndefined($("#target").val());
        searchString = checkUndefined($("#keyword").val());
        hashSttus = checkUndefined($("#selectBox").val());

        if (gridState == "HASH"){
            myPostData.page = page;
            tempState = "READY";
        } else {
            if(tempState == "SEARCH"){
                myPostData.page = 1;
            } else {
                tempState = "";
            }
        }

        if(gridState == "NONE"){
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            myPostData.page = page;

            if (searchField != null) {
                tempState = "SEARCH";
            } else {
                tempState = "READY";
            }
        }

        if(searchField != null && searchField != "" && searchString != ""){
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
        }else{
            delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
        }

        gridState = "GRIDBEGIN";

        if (hashSttus != null && hashSttus != "") {
            myPostData.sttus = hashSttus;
        } else {
            myPostData.sttus = "";
        }
        $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page });
    },
    loadComplete: function (data) {
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        //session
        if(sessionStorage.getItem("last-url") != null){
            sessionStorage.removeItem('state');
            sessionStorage.removeItem('last-url');
        }

        //Search 필드 등록
        searchFieldSet();

        if (data.resultCode == 1000) {
            totalPage = data.result.totalPage;

            if (page != data.result.currentPage) {
                tempState = "";
            }
        } else {
            tempState = "";
        }

        /*뒤로가기*/
        var hashlocation = "page="+$(this).context.p.page+"&sttus="+$("#selectBox option:selected").val();
        if($(this).context.p.postData._search && $("#target").val() != null){
            hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
        }

        gridState = "GRIDCOMPLETE";
        document.location.hash = hashlocation;

        if (tempState != "" || str_hash == hashlocation) {
            gridState = "READY";
        }

        $(this).find("td").each(function(){
            if (typeof $(this).parents("tr").attr("tabindex") != "undefined") {
                if ($(this).index() == 1) { // 썸네일
                    var filePath = $(this).parent("tr").find("td:eq(9)").text();
                    console.log(">> filePath : " + filePath);
                    // 썸네일 사이즈 지정하여 호출, 지정된 썸네일 사이즈의 파일이 없을 경우 원본 이미지를 로드함.
                    var thumbImgSizeStr = "&width=100&height=75";
                    $(this).html("<img class='displayImg' style='min-width:100px;height:75px;background-image:url("
                            + makeAPIUrl(filePath + thumbImgSizeStr, "img") + "),url(" + $("#contextPath").val() + "/resources/image/img_empty.png); "
                            + "background-size: 100%; background-repeat: no-repeat; background-position: center; background-width: 50%;"
                            + " vertical-align: middle; margin: 5px 0px;'>");
                } else if ($(this).index() == 6) { // 구분
                    var str = $(this).text();

                    if(str == getMessage("contents.table.state.exhibition.success")){
                        str = "<span class='viewIcon'>"+getMessage("contents.table.state.verify.success") + "</span>";
                    }
                    $(this).html(str);
                }
            }
        });

        //pageMove Max
        $('.ui-pg-input').on('keyup', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
        });

        $(this).find(".pointer").parent("td").addClass("pointer");
    },
    /*
     * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
     * 해당 로우의 각 셀마다 이벤트 생성
     */
    onCellSelect: function(rowId, columnId, cellValue, event){
        // 콘텐츠 ID 선택시
        if(columnId == 3){
            var list = $("#jqgridData").jqGrid('getRowData', rowId);
            sessionStorage.setItem("last-url", location);
            sessionStorage.setItem("state", "view");
            pageMove("/contents/"+list.contsSeq);
        }
     }
}
