﻿/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

/*
 * This is an example of how to cancel all the files queued up.  It's made somewhat generic.  Just pass your SWFUpload
 * object in to this method and it loops through cancelling the uploads.
 */
/*
 * custom setting options
 *
 */
/*
function cancelQueue(instance) {
    document.getElementById(instance.customSettings.cancelButtonId).disabled = true;
    instance.stopUpload();
    var stats;

    do {
        stats = instance.getStats();
        instance.cancelUpload();
    } while (stats.files_queued !== 0);

}
*/

/* **********************
   Event Handlers
   These are my custom event handlers to make my
   web application behave the way I went when SWFUpload
   completes different tasks.  These aren't part of the SWFUpload
   package.  They are part of my application.  Without these none
   of the actions SWFUpload makes will show up in my application.
   ********************** */
var uploadingCnt = 0;
var uploadQue = 0;
var totalQue = 0;
var videoUploadTemp;
var thumbnailUploadTemp;
var metadataUploadTemp = new Array();
var coverImgUploadTemp;
var contentsUploadTemp;
var fileTemp;
var file2Temp;
var prevUploadTemp;
var prevmetaUploadTemp  = new Array();
var prevcontUploadTemp;

var prevmetadataSeq = 0;
var metadataSeq = 0;

var uploadErrorList = new Array();

function fileDialogStart() {
    /* I don't need to do anything here */
    this.setFilePostName("fileupload");
    this.addPostParam("imgMode", this.customSettings.fileSe);
}
function fileQueued(file) {

    var pattern = /[\u3131-\u314e|\u314f-\u3163|\uac00-\ud7a3]/g;
//    if(pattern.test(file.name)){
//        alert("한글이름으로 된 파일은 업로드 하실수 없습니다.");
//        return;
//    }

    if (file.name.indexOf("\0") != -1 || file.name.indexOf(";") != -1 || file.name.indexOf("./") != -1 || file.name.indexOf(".\\") != -1) {
        popAlertLayer(getMessage("info.upload.failed.special.word.msg"));
        this.cancelUpload(file.id);
        return;
    }

    if (file.name.length > 50) {
        popAlertLayer(getMessage("info.upload.failed.max.length.msg"));
        this.cancelUpload(file.id);
        return;
    }
    try {
        // You might include code here that prevents the form from being submitted while the upload is in
        // progress.  Then you'll want to put code in the Queue Complete handler to "unblock" the form
        //var progress = new FileProgress(file, this.customSettings.progressTarget);
        //progress.setStatus("Pending...");
        //progress.toggleCancel(true, this);
        if ( this.customSettings.fileSe == "thumbnail") {
            $("#thumbnailArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">파일명 : "+file.name+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('thumbnailArea', '" + file.id + "')\" value=\"삭제\"/></td></tr></table>");
            $("#thumbnailArea").show();
            thumbnailUploadTemp = this;
        } else if(this.customSettings.fileSe =="prevmeta") {
            if(this.customSettings.filePath != undefined){
                this.addPostParam("filePath", this.customSettings.filePath);
            }
            $("#prevmetaName"+this.customSettings.fileSeq).html("<p>파일명 : "+file.name+"</p>");
            prevmetaUploadTemp.push(this);
        } else if(this.customSettings.fileSe =="metadata") {
            $("#metadataName"+this.customSettings.fileSeq).html("<p>파일명 : "+file.name+"</p>");
            metadataUploadTemp.push(this);
        } else if(this.customSettings.fileSe =="coverImg") {
            $("#coverImgArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">파일명 : "+file.name+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('coverImgArea', '" + file.id + "')\" value=\"삭제\"/></td></tr></table>");
            $("#coverImgArea").show();
            coverImgUploadTemp = this;
        } else if(this.customSettings.fileSe =="file") {
            $("#fileArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">파일명 : "+file.name+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('fileArea', '" + file.id + "')\" value=\"삭제\"/></td></tr></table>");
            $("#fileArea").show();
            fileTemp = this;
        } else if(this.customSettings.fileSe =="video") {
            metadataSeq++;
            var videoHtml = "";
            videoHtml += "<table class=\"videoForm\" id=\"videoItem"+metadataSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">"+getMessage("table.name")+"</td><td class=\"content\" colspan=\"3\">"+file.name+"</td></tr>";
            videoHtml += "          <input type=\"hidden\" name=\"videoPath\" id=\"video"+file.id+"\">";
            videoHtml += "          <input type=\"hidden\" name=\"metadataPath\" id=\"metadata"+metadataSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">메타데이터</td><td class=\"content\" colspan=\"3\">&nbsp;&nbsp;<div style=\"display:inline-block;\" id=\"metadataName"+metadataSeq+"\"></div>&nbsp;&nbsp;<div id=\"metadata_btn_placeholder"+metadataSeq+"\"></div></td></tr>";
            videoHtml += "</table>";
            $("#videoArea").append(videoHtml);
            $("#videoArea").show();
            metadataUploadBtn(metadataSeq);
            videoUploadTemp = this;
        } else if (this.customSettings.fileSe =="notice") {
            $("#fileArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">파일명 : "+file.name+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete(this, '" + file.id + "')\" value=\"삭제\"/></td></tr></table>");
            $("#fileArea").show();
            fileTemp = this;
        } else if (this.customSettings.fileSe =="svcNotice") {
            $("#fileArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">파일명 : "+file.name+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete(this, '" + file.id + "')\" value=\"삭제\"/></td></tr></table>");
            $("#fileArea").show();
            fileTemp = this;
        } else if (this.customSettings.fileSe =="qnaAns") {
            $("#fileArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">파일명 : "+file.name+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete(this, '" + file.id + "')\" value=\"삭제\"/></td></tr></table>");
            $("#fileArea").show();
            fileTemp = this;
        } else if (this.customSettings.fileSe =="version") {
            $("#fileArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">파일명 : "+file.name+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete(this, '" + file.id + "')\" value=\"삭제\"/></td></tr></table>");
            $("#fileArea").show();
            fileTemp = this;
        } else {
            prevmetadataSeq++;
            var videoHtml = "";
            videoHtml += "<table class=\"prevForm\" id=\"prevItem"+prevmetadataSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">"+getMessage("table.name")+"</td><td class=\"content\" colspan=\"3\">"+file.name+"</td></tr>";
            videoHtml += "          <input type=\"hidden\" name=\"videoPath\" id=\"prev"+file.id+"\">";
            videoHtml += "          <input type=\"hidden\" name=\"metadataPath\" id=\"prevmeta"+prevmetadataSeq+"\">";
            videoHtml += "  <tr style='display:none'><td class=\"sub\">"+getMessage("contents.table.metadata")+"</td><td class=\"content\" colspan=\"3\">&nbsp;&nbsp;<div style=\"display:inline-block;\" id=\"prevmetaName"+prevmetadataSeq+"\"></div>&nbsp;&nbsp;<div id=\"prevmeta_btn_placeholder"+prevmetadataSeq+"\"></div></td></tr>";
            videoHtml += "</table>";
            $("#prevArea").append(videoHtml);
            $("#prevArea").show();
            prevmetaUploadBtn(prevmetadataSeq);
            prevUploadTemp = this;
        }
    } catch (ex) {
        this.debug(ex);
    }

}

function fileQueueError(file, errorCode, message) {
    try {
        if (this.customSettings.fileSe == "photo") {
            $("#"+this.customSettings.progressTarget).css("cursor","default");
            $(".blockUI.blockOverlay").css("cursor","default");
            $.unblockUI();
        }
        if (errorCode === SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT) {
            if ( this.customSettings.fileSe == "photo" ) {
                //popAlert(getMsg("tit_6034"),getMsgV1("err_6017", SCREENSHOT_MAX_SIZE));
                // file queue 삭제
                while ( this.getStats().files_queued != 0 ) {
                    this.cancelUpload();
                }
                return;
            }
        }
        if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
            if (message === 0) {
                popAlertLayer(getMessage("common.file.upload.limit.msg"));
            } else {
                if (message > 1) {
                    popAlertLayer(getMessage(message + "common.multi.file.upload.msg"));
                } else {
                    popAlertLayer(getMessage("common.only.one.file.upload.msg"));
                }
            }
            //alert("You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file.")));
            return;
        }

        //var progress = new FileProgress(file, this.customSettings.progressTarget);
        //progress.setError();
        //progress.toggleCancel(false);

        switch (errorCode) {
        case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
            //progress.setStatus("File is too big.");
            this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
            //progress.setStatus("Cannot upload Zero Byte files.");
            this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
            //progress.setStatus("Invalid File Type.");
            this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
            alert("You have selected too many files.  " +  (message > 1 ? "You may only add " +  message + " more files" : "You cannot add any more files."));
            break;
        default:
            if (file !== null) {
                //progress.setStatus("Unhandled Error");
            }
            this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        }
    } catch (ex) {
        this.debug(ex);
    }
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
    try {
        //this.startUpload();
    } catch (ex)  {
        this.debug(ex);
    }
}
function uploadStart(file) {
    $("#uploadText").html(file.name+" 등록 중...");
    $("#divCenter").css("display", "block");

    this.addPostParam("contsSeq", $("#contsSeq").val());
    return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
    var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);
    $("#uploadPercent").html(percent+"%");
    $("#barWidth").attr("width", percent+"%");
}
var addPhotoSeqs = "";
function uploadSuccess(file, serverData) {
    var data  = $.parseJSON(serverData);
    if(data.resultCode == "1000"){
        $("#orginlFileNm").val(data.result.fileInfo.orginlFileNm);
        $("#fileDir").val(data.result.fileInfo.fileDir);
        $("#fileSize").val(file.size);
        $("#fileExt").val(data.result.fileInfo.fileExt);
        $("#streFileNm").val(data.result.fileInfo.streFileNm);
        /*var TimeH = 3600*Number($("#hour").val());
        var TimeM = 60*Number($("#min").val());
        $("#runTime").val(TimeH+TimeM);*/
        if (this.customSettings.fileSe == "thumbnail") {
            $("#fileSe").val("img");
        } else if(this.customSettings.fileSe == "coverImg") {
            $("#fileSe").val("cover");
        } else if(this.customSettings.fileSe == "metadata") {
            $("#metadata"+this.customSettings.fileSeq).val(data.result.fileInfo.fileDir);
            $("#fileSe").val("metadata");
        } else if(this.customSettings.fileSe =="file") {
            $("#fileSe").val("file");
        } else if(this.customSettings.fileSe =="file2") {
            $("#fileSe").val("file");
        } else if(this.customSettings.fileSe =="video") {
            $("#video"+file.id).val(data.result.fileInfo.fileDir);
            $("#fileSe").val("video");
        } else if(this.customSettings.fileSe =="prev") {
            $("#prev"+file.id).val(data.result.fileInfo.fileDir);
            $("#fileSe").val("prev");
        } else if(this.customSettings.fileSe == "prevmeta") {
            $("#prevmeta"+this.customSettings.fileSeq).val(data.result.fileInfo.fileDir);
            $("#fileSe").val("prev");
        } else if(this.customSettings.fileSe =="notice") {
            $("#fileSe").val("notice");
        } else if(this.customSettings.fileSe =="svcNotice") {
            $("#fileSe").val("svcNotice");
        } else if(this.customSettings.fileSe =="qnaAns") {
            $("#fileSe").val("qnaAns");
        } else if(this.customSettings.fileSe =="version") {
            $("#fileSe").val("version");
        } else {
            $("#prev"+file.id).val(data.result.fileInfo.fileDir);
            $("#fileSe").val("prev");
        }
        callByPost("/api/fileProcess", "fileInsertSuccess", "fileForm");
    } else {
        addErrorFile({ name : file.name, message : '' });
        if (typeof window["fileInsertFail"] === "function") {
            window["fileInsertFail"].apply();
        }
    }
}

function uploadComplete(file) {
    try {
        /*  I want the next upload to continue automatically so I'll call startUpload here */
        if (this.getStats().files_queued === 0) {
            //document.getElementById(this.customSettings.cancelButtonId).disabled = true;
            //$("#"+this.customSettings.uploadButtonLayer).css("display","none");
            //$("#"+this.customSettings.progressTarget).css("display",  "none");
            $("#divCenter").hide();
        } else {
            this.startUpload();
        }
    } catch (ex) {
        this.debug(ex);
    }

}

function uploadError(file, errorCode, message) {
    //console.log(JSON.stringify(file,null,4));
    try {
        //var progress = new FileProgress(file, this.customSettings.progressTarget);
        //progress.setError();
        //progress.toggleCancel(false);

        if ( errorCode == SWFUpload.UPLOAD_ERROR.FILE_CANCELLED) {
            if (this.customSettings.fileSe != "screenshot") {
                $("#"+this.customSettings.progressTarget).css("cursor","default");
                $(".blockUI.blockOverlay").css("cursor","default");
                $.unblockUI();
            }
            return;
        }
        //console.log(errorCode);
        //console.log(message);
        switch ( message ) {
            case "401"      : {
                //document.location.href = _context + "/auth/signin"; // 로그인 페이지로 이동
                break;
            }
            case "403"      : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0012"));
                break;
            }
            case "404"      : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0013"));
                break;
            }
            case "400"      : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0014"));
                break;
            }
            case "500"      : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0016"));
                break;
            }
            default     : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0010")+"\nErrorMessage: "+message+", ErrorCode: "+errorCode);
                break;
            }
        }
        switch (errorCode) {
        case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
            //console.log("1");
            //progress.setStatus("Upload Error: " + message);
            this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
            console.log("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.MISSING_UPLOAD_URL:
            //console.log("2");
            //progress.setStatus("Configuration Error");
            this.debug("Error Code: No backend file, File name: " + file.name + ", Message: " + message);
            console.log("Error Code: No backend file, File name: " + file.name + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
            //console.log("3");
            //progress.setStatus("Upload Failed.");
            this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.IO_ERROR:
            //console.log("4");
            //progress.setStatus("Server (IO) Error");
            this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
            console.log("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
            //console.log("5");
            //progress.setStatus("Security Error");
            this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
            console.log("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
            //console.log("6");
            //progress.setStatus("Upload limit exceeded.");
            this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND:
            //console.log("7");
            //progress.setStatus("File not found.");
            this.debug("Error Code: The file was not found, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: The file was not found, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
            //console.log("8");
            //progress.setStatus("Failed Validation.  Upload skipped.");
            this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
            //console.log("9");
            if (this.getStats().files_queued === 0) {
                //document.getElementById(this.customSettings.cancelButtonId).disabled = true;
            }
            //progress.setStatus("Cancelled");
            //progress.setCancelled();
            break;
        case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
            //console.log("10");
            //progress.setStatus("Stopped");
            break;
        default:
            //console.log("11");
            //progress.setStatus("Unhandled Error: " + error_code);
            this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        }

        addErrorFile({ name : file.name, message : getErrorMessage(errorCode) });

        if (typeof window["fileInsertFail"] === "function") {
            window["fileInsertFail"].apply();
        }

    } catch (ex) {
        this.debug(ex);
    }
}

addErrorFile = function(item) {
    uploadErrorList.push(item);
}

hasErrorFiles = function() {
    return uploadErrorList.length >= 1;
}

getErrorMessage = function(errorCode) {
    var message = "";
    switch (errorCode) {
    case SWFUpload.UPLOAD_ERROR.IO_ERROR:
        message = getMessage("info.upload.failed.wrong.path.msg");
        break;
    default:
        break;
    }

    if (message !== "") {
        message = "(" + message + ")";
    }
    return message;
}

getErrorMessages = function() {
    var message = "";
    if (uploadErrorList.length < 1) {
        return message;
    }

    var fileNames = "";
    $(uploadErrorList).each(function(i, item) {
        fileNames += item.name + item.message;
        if (fileNames !== "" && i < uploadErrorList.length - 1) {
            fileNames += ", ";
        }
    });

    if (fileNames !== "") {
        message += "<br><br>" +  fileNames + " " + getMessage("info.upload.failed.msg");
    }
    uploadErrorList = [];

    return message;
}