/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var FILE_VIDEO;
var FILE_THUMBNAIL;
var FILE_METADATA;
var FILE_COVERIMG;
var FILE_CONTENTS;
var FILE_FILE1;
var FILE_PREV;
var FILE_PREVMETA;
var FILE_PREVCONT;
var FILE_SETTINGS   = {
        // Backend Settings
        //upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
    };

//swfupload - cover
coverImgUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'coverImg'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "coverImg_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.jpg;*.jpeg;*.png;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "20MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_COVERIMG   = new SWFUpload(file_settings);
}

//swfupload - thumbnail
thumbnailUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'thumbnail'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "thumbnail_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.jpg;*.jpeg;*.png;",
    file_settings.file_queue_limit =  "0";
    file_settings.file_size_limit =  "20MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_THUMBNAIL  = new SWFUpload(file_settings);
}

//swfupload - video
videoUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'video'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "video_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.mp4;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_VIDEO  = new SWFUpload(file_settings);
}

//swfupload - PREV
prevUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'prev'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "prev_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.mp4;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_PREV  = new SWFUpload(file_settings);
}

//swfupload - metadata
prevmetaUploadBtn = function(seq){
    //File Upload screenshot
    var temp = {fileSe : 'prevmeta', fileSeq : seq};
    var file_settings   ={};
    file_settings.button_placeholder_id = "prevmeta_btn_placeholder"+seq;
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.xml;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_PREVMETA   = new SWFUpload(file_settings);
}

//swfupload - metadata
metadataUploadBtn = function(seq){
    //File Upload screenshot
    var temp = {fileSe : 'metadata', fileSeq : seq};
    var file_settings   ={};
    file_settings.button_placeholder_id = "metadata_btn_placeholder"+seq;
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.xml;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_METADATA   = new SWFUpload(file_settings);
}

//swfupload - file1
file1UploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'file'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.zip;*.exe;",
    file_settings.file_queue_limit =  "1";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_FILE1  = new SWFUpload(file_settings);
}

var categorySeq;

$(document).ready(function(){
    var now=new Date();

    $("#cntrctStDt").val(dateString((new Date(now.getTime()))));
    $("#cntrctFnsDt").val(dateString((new Date(now.getTime()))));

    $(".datepicker").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
    });

    contsCtgList();
    coverImgUploadBtn();
    videoUploadBtn();
    thumbnailUploadBtn();
    file1UploadBtn();
    CPList();
    GenreList();
    ServiceList();
    contentStateList();
    prevUploadBtn();
});
var ctgVal = "none";
contsCtgList = function(ctgSeq){
    formData("NoneForm", "GroupingYN", "Y");

    ctgSeq = typeof ctgSeq !== 'undefined' ? ctgSeq : "none";

    if($("#firstCtg option:selected").val()){
        formData("NoneForm", "firstCtgNm", encodeURI($("#firstCtg option:selected").val()));
    }
    callByGet("/api/contentsCategory" , "contsCtgListSuccess", "NoneForm");
    if (ctgSeq != "none") {
        ctgVal = ctgSeq;
    } else {
        ctgVal = "none";
    }

}


var _firstLoading = true;
var curYear = new Date().getFullYear(); // 현재 날짜의 년도(ContentsID에서 사용)
contsCtgListSuccess = function(data){
    formDataDelete("NoneForm", "GroupingYN");
    formDataDelete("NoneForm", "firstCtgNm");
    if(data.resultCode == "1000"){
        var contsCtgListHtml = "";
        var firstCtgNmListHtml= "";

        $(data.result.firstCtgList).each(function(i,item) {
            firstCtgNmListHtml += "<option value='"+item.firstCtgNm+"'>"+item.firstCtgNm+"</option>";
        });

        if(_firstLoading){
            $("#firstCtg").html(firstCtgNmListHtml);
            _firstLoading = false;
        }

        $(data.result.contsCtgList).each(function(i,item) {
            if(i == 0){
                $("#contsID").text(item.firstCtgID + item.secondCtgID + curYear + prependZero(item.ctgCnt, 5));
            }
            contsCtgListHtml += "<option value='"+item.contsCtgSeq+"' name=\"" + item.firstCtgID + item.secondCtgID + item.ctgCnt + "\">"+item.secondCtgNm+"</option>";
        });

        // 첫번째 카테고리 명이 '영상'이 아닐때
        if ($("#firstCtg option:selected").val() != getMessage("contents.category.video")) {
            $(".attrPart").hide();
            $(".gamePart").show();
            $(".videoPart").hide();
        } else if ($("#firstCtg option:selected").val() == getMessage("contents.category.video")) { // 첫번째 카테고리 명이 '영상'일때
            $(".attrPart").hide();
            $(".gamePart").hide();
            $(".videoPart").show();
        }
        $("#secondCtg").html(contsCtgListHtml);
        if(ctgVal != "none") {
            $("#secondCtg").val(ctgVal);
        }

    }
}

//카테고리 변경시 콘텐츠 ID 최신화
contentsIDRefresh = function(){
    var categoryInfo = $("#secondCtg option:selected").attr("name");
    $("#contsID").text(categoryInfo.slice(0,2) + curYear + prependZero(categoryInfo.slice(2), 5));
}

// 콘텐츠 ID 년도 뒷부분 숫자 앞 빈칸 0으로 채우기
function prependZero(num, len) {
    while(num.toString().length < len) {
        num = "0" + num;
    }
    return num;
}

contentsXmlCreate = function(){
    if($("#firstCtg option:selected").val() != getMessage("contents.category.video")) {
        popAlertLayer(getMessage("success.common.insert"), "/contents");
    } else {
        CALL_COUNT++;
        CALL_TOT_COUNT++;
        var videoFormData = "";
        $("table[id ^= videoItem]").each(function(i,item){
            videoFormData += $(this).find("input[name=videoPath]").val()+",";
            videoFormData += $(this).find("input[name=metadataPath]").val()+"GNB";
        });
        formData("NoneForm" , "contsSeq", $("#contsSeq").val());
        formData("NoneForm" , "contsID", $("#contsID").text());
        formData("NoneForm" , "videoFormData", videoFormData.slice(0,-3));
        callByPost("/api/contentsXml", "contentsXmlSuccess", "NoneForm" );
    }
}

contentsXmlSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.insert"), "/contents");
    }
}
fileInsertSuccess = function(data){
    CALL_SUCCESS_COUNT++;
    jsCallComplete();
}

fileQueueDelete = function(id){
    $("#"+id).html("");
    $("#"+id).hide("");

    if(id == "thumbnailArea"){
        thumbnailUploadTemp.cancelUpload();
    } else if(id == "coverImgArea"){
        coverImgUploadTemp.cancelUpload();
    } else if(id == "fileArea"){
        fileTemp.cancelUpload();
    } else if(id == "videoArea"){
        videoUploadTemp.cancelUpload();
    }
}

function GenreList(){
    callByGet("/api/contents/genre?firstCtgId=E", "GenreListSuccess");
}

function GenreListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $(data.result.codeList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\""+item.comnCdSeq+"\" value=\""+item.comnCdNM+"\" class=\"buttonBox\"/ onclick=\"buttonBoxToggle(this);\"> ";
        });
        $("#genreList").append(menuHtml);
    }
}

function ServiceList(){
    callByGet("/api/service?searchType=list", "ServiceListSuccess");
}

function ServiceListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $(data.result.serviceNmList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\""+item.svcSeq+"\" value=\""+item.svcNm+"\" class=\"buttonBox\"/ onclick=\"buttonBoxToggle(this);\"> ";
        });
        $("#serviceList").append(menuHtml);
    }
}

function CPList(){
    callByGet("/api/cp?rows=10000", "CPListSuccess");
}

function CPListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $(data.result.cpList).each(function(i, item){
            menuHtml = "<option value=\""+item.cpSeq+"\">"+item.cpNm+"</option>";
            $("#cpList").append(menuHtml);
        });
    }
}

function buttonBoxToggle(e){
    $(e).toggleClass("buttonBox");
    $(e).toggleClass("buttonBoxOn");
}

contentInfo = function(){
    callByGet("/api/contents?contsSeq="+$("#contsSeq").val(), "contentsInfoSuccess");
}
contentsInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.contentsInfo;
        categorySeq = item.contsCtgSeq;
        $("#contsTitle, #headTitle").val(item.contsTitle);
        $("#contsSubTitle").val(item.contsSubTitle);
        $("#contsDesc").val(xssChk(item.contsDesc));
        $("#contCtgNm").val(item.contCtgNm);
        $("#cretrID").val(item.cretrID);
        $("#mbrNm").val(item.mbrNm);
        $("#contsVer").val(item.contsVer);
        $("#sttus").text(item.sttus);

        $("#firstCtg").val(item.firstCtgNm);
        contsCtgList(item.contsCtgSeq)
        if (item.sttus != getMessage("contents.table.regist.ing")) {
            $("#sttusBtn").val(item.sttus);
            $("#sttusBtn").css("background-color","rgb(255, 54, 54)");
            $("#sttusBtn").css("border","solid 1px #ff0707");
            $("#sttus a").css("color","red");

            $("#sttusBtn").attr("disabled",true);

        } else {
            $("#sttus a").css("color","#388FCE");
        }
        $("#cp").html(item.cp);
        $("#cntrctDt").html(item.cntrctStDt+" ~ "+item.cntrctFnsDt);
        $("#cretDt").html(item.cretDt);
        $("#amdDt").html(item.amdDt);
        $("#amdrID").html(item.amdrID);
        $("#exeFilePath").val(item.exeFilePath);
        $("#version").text(item.version);

        /** 장르 리스트 번호들 */
        var genre="";

        $(data.result.contentsInfo.genreList).each(function(i,item){
            $("#genreList").find("input[data-seq="+item.genreSeq+"]").removeClass('buttonBox').addClass('buttonBoxOn');
        });

        /** 서비스 리스트 번호들 */
        var service="";
        $(data.result.contentsInfo.serviceList).each(function(i,item){
            $("#serviceList").find("input[data-seq="+item.svcSeq+"]").removeClass('buttonBox').addClass('buttonBoxOn');
        });

        $("#exefileType").val(item.fileType);
        $("#maxAvlNop").val(item.maxAvlNop);

        var thumbnailHtml = "";
        thumbnailHtml += "<table class=\"fileTable\">";
        $(item.thumbnailList).each(function(i,item){
            thumbnailHtml += "<tr id=\"thumbnail"+item.fileSeq+"\"><td><p class=\"fileName\" title=\""+item.thumbnailNm+"\">"+getMessage("contents.file.name")+" : "+item.thumbnailNm+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"thumbnailDelete('"+item.fileSeq+"')\" value="+getMessage("button.delete")+"></td></tr>";
        });

        $("#thumbnailArea").html(thumbnailHtml);

//            $("#videoName").html(item.videoName);
//        if(item.contentsXMLName){
//            $("#contentsXmlName").html(""+item.contentsXMLName+"<a href='/api/fileDownload/"+item.contentsXMLSeq+"' class='fontblack'>다운로드</a>");
//        }
        var coverImgHtml = "<table class=\"fileTable\"><tr><td><p class=\"fileName\" title=\""+item.coverImg.coverImgNm+"\">"+getMessage("contents.file.name")+" : "+item.coverImg.coverImgNm+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"savedfileDelete('coverImgArea', "+item.coverImg.fileSeq+")\" value="+getMessage("button.delete")+"></td></tr></table>";
        $("#coverImgArea").html(coverImgHtml);
        var metadataHtml = "";
        $(data.result.contentsInfo.videoList).each(function(i,item){
            var videoHtml = "";
            metadataSeq++;
            videoHtml += "<table class=\"videoForm\" id=\"videoItem"+item.fileSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">"+getMessage("table.name")+"</td><td class=\"content\" colspan=\"3\">"+item.fileNm+"&nbsp;&nbsp;<input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"videoDelete('"+item.fileSeq+"')\" value="+getMessage("button.delete")+"></td></tr>";
            videoHtml += "          <input type=\"hidden\" name=\"videoPath\" value=\""+item.filePath+"\" >";
            videoHtml += "          <input type=\"hidden\" name=\"metadataPath\" id=\"metadata"+metadataSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">"+getMessage("contents.table.metadata")+"</td><td class=\"content\" colspan=\"3\"><div style=\"display:inline-block;\" id=\"metadataName"+metadataSeq+"\"></div>&nbsp;&nbsp;<div id=\"metadata_btn_placeholder"+metadataSeq+"\"></div></td></tr>";
            videoHtml += "</table>";
            $("#videoArea").append(videoHtml);
            $("table[id=videoItem"+item.fileSeq+"]").find("input[name=metadataPath]").val(item.metadataInfo.filePath);
            $("#metadataName"+metadataSeq).html(getMessage("contents.file.name")+" : "+item.metadataInfo.fileNm);
            metadataUploadBtn(metadataSeq,item.metadataInfo.filePath);
        });

        $(data.result.contentsInfo.prevList).each(function(i,item){
            prevmetadataSeq++;
            var videoHtml = "";
            videoHtml += "<table class=\"prevForm\" id=\"prevItem"+prevmetadataSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">"+getMessage("table.name")+"</td><td class=\"content\" colspan=\"3\">"+item.fileNm+"</td></tr>";
            videoHtml += "          <input type=\"hidden\" name=\"videoPath\" id=\"prev"+item.filePath+"\">";
            videoHtml += "          <input type=\"hidden\" name=\"metadataPath\" id=\"prevmeta"+prevmetadataSeq+"\">";
            videoHtml += "  <tr style='display:none'><td class=\"sub\">"+getMessage("contents.table.metadata")+"</td><td class=\"content\" colspan=\"3\">&nbsp;&nbsp;<div style=\"display:inline-block;\" id=\"prevmetaName"+prevmetadataSeq+"\"></div>&nbsp;&nbsp;<div id=\"prevmeta_btn_placeholder"+prevmetadataSeq+"\"></div></td></tr>";
            videoHtml += "</table>";

            $("#prevArea").append(videoHtml);
            $("#prevArea").show();
            prevmetaUploadBtn(prevmetadataSeq);
            prevUploadTemp = this;
        });

        var fileHtml = "";
        $(data.result.contentsInfo.fileList).each(function(i,item){
            fileHtml = "<table class=\"fileTable\"><tr><td><p class=\"fileName\" title=\""+item.fileNm+"\">"+getMessage("contents.file.name")+" : "+item.fileNm+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"savedfileDelete('fileArea', "+item.fileSeq+")\" value="+getMessage("button.delete")+"></td></tr></table>";
            $("#fileArea").html(fileHtml);
        });
        listBack($(".btnList"));
    }
}

function contentsSttusChange(){
    formData("NoneForm" , "contsSeq", $("#contsSeq").val());
    formData("NoneForm" , "sttus", "02");
    callByPut("/api/contents" , "contentsSttusChangeSuccess", "NoneForm");
}

contentsSttusChangeSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("msg.common.complte.response"),"/contents/"+$("#contsSeq").val());
    }else if(data.resultCode == "1011"){
        formDataDeleteAll("NoneForm");
        popAlertLayer(getMessage("msg.common.update.fail"));
    }
}

contentStateList = function(){
    callByGet("/api/codeInfo?comnCdCtg=CONTS_STTUS","contentStateListSuccess","NoneForm");
}
contentStateListSuccess = function(data){
    if(data.resultCode == "1000"){
        var sttusListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            sttusListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNM+"</option>";
        });
        $("#sttusList").html(sttusListHtml);
    }
    contentInfo();
}
var thumbnaildelSeq="";
thumbnailDelete = function(seq){
    $("#thumbnail"+seq).parents(".fileTable").remove();
    thumbnaildelSeq += seq+",";
}

savedfileDelete = function(id, seq){
    $("#"+id).html("");
    $("#"+id).hide("");
    thumbnaildelSeq += seq+",";
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

videoDelete = function(seq){
    document.getElementById("videoItem"+seq).remove();
    thumbnaildelSeq += seq+",";
}

function uploadS(){
    if(thumbnailUploadTemp == undefined && videoUploadTemp == undefined  && coverImgUploadTemp == undefined && metadataUploadTemp.length == 0 && fileTemp == undefined){
        contentsXmlCreate();
    }
    if(thumbnailUploadTemp != undefined){
        thumbnailUploadTemp.startUpload();
    }
    if(videoUploadTemp != undefined){
        videoUploadTemp.startUpload();
    }
    $(metadataUploadTemp).each(function(){
        this.startUpload();
    });
    if(coverImgUploadTemp != undefined){
        coverImgUploadTemp.startUpload();
    }
    if(fileTemp != undefined){
        fileTemp.startUpload();
    }
}

contentsXmlCreate = function(){
    if($("#firstCtg option:selected").val() != getMessage("contents.category.video")) {
        popAlertLayer(getMessage("success.common.update"), "/contents/"+$("#contsSeq").val());
    } else {
        CALL_COUNT++;
        CALL_TOT_COUNT++;
        var videoFormData = "";
        $("table[id ^= videoItem]").each(function(i,item){
            videoFormData += $(this).find("input[name=videoPath]").val()+",";
            videoFormData += $(this).find("input[name=metadataPath]").val()+"GNB";
        });
        formData("NoneForm" , "contsSeq", $("#contsSeq").val());
        formData("NoneForm" , "#contsID", $("#contsID").text());
        formData("NoneForm" , "videoFormData", videoFormData.slice(0,-3));
        callByPost("/api/contentsXml", "contentsXmlSuccess", "NoneForm" );
    }
}

contentsXmlSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.update"), "/contents/"+$("#contsSeq").val());
    }
}
fileInsertSuccess = function(data){
    CALL_SUCCESS_COUNT++;
    jsCallComplete1();
}

//콘텐츠 ID 년도 뒷부분 숫자 앞 빈칸 0으로 채우기
function prependZero(num, len) {
    while(num.toString().length < len) {
        num = "0" + num;
    }
    return num;
}

fileQueueDelete = function(id){
    $("#"+id).html("");
    $("#"+id).hide("");

    if(id == "thumbnailArea"){
        thumbnailUploadTemp.cancelUpload();
    } else if(id == "coverImgArea"){
        coverImgUploadTemp.cancelUpload();
    } else if(id == "fileArea"){
        fileTemp.cancelUpload();
    } else if(id == "videoArea"){
        videoUploadTemp.cancelUpload();
    }
}



contentsEdit = function(){
    if($("#contsTitle").val().trim() == ""){
        popAlertLayer(getMessage("msg.contents.input.contsNm"));
        return;
    }
    if($("#coverImgArea").children().length <= 0){
        popAlertLayer(getMessage("msg.contents.input.coverImg"));
        return;
    }
    if($("#thumbnailArea").children().length <= 0){
        popAlertLayer(getMessage("msg.contents.input.thumnail"));
        return;
    }
    if($("#contsSubTitle").val().trim() == ""){
        popAlertLayer(getMessage("msg.contents.input.desc"));
        return;
    }
    if($("#contsDesc").val().trim() == ""){
        popAlertLayer(getMessage("msg.contents.input.detail.desc"));
        return;
    }

    if($("#firstCtg option:selected").val() == getMessage("contents.category.video")){
        if($("#videoArea").children().length <= 0){
            popAlertLayer(getMessage("msg.contents.input.video"));
            return;
        }
    }


    var genre="";
    $("#genreList .buttonBoxOn").each(function(i, item){
        if (genre == "") {
            genre += $(this).attr("data-seq");
        } else {
            genre += "," + $(this).attr("data-seq");
        }
    });

    var service="";
    $("#serviceList .buttonBoxOn").each(function(i, item){
        if (service == "") {
            service += $(this).attr("data-seq");
        } else {
            service += "," + $(this).attr("data-seq");
        }
    });

    if(!genre){
        popAlertLayer(getMessage("msg.contents.select.genre"));
        return;
    }

    if($("#versionModify").is(":checked")){
        formData("NoneForm" , "versionModify", "Y");
    }

    if($("#firstCtg option:selected").val() == getMessage("contents.category.video")){
        if($("#videoArea").children().length <= 0){
            popAlertLayer(getMessage("msg.contents.input.video"));
            return;
        }
    }
    if($("#firstCtg option:selected").val() != getMessage("contents.category.video")) {
        formData("NoneForm" , "fileType", $("#exefileType option:selected").val());
    }
    if ($("#firstCtg option:selected").val() == getMessage("contents.category.video")) {
        formData("NoneForm" , "fileType", "VIDEO");
    } else {
        formData("NoneForm" , "fileType", $("#exefileType option:selected").val());
    }

    // 카테고리가 달라졌을 경우에만 contentsID 수정
    if(categorySeq != $("#secondCtg option:selected").val()){
       formData("NoneForm" , "contsID", $("#contsID").text());
    }

    formData("NoneForm" , "contsSeq", $("#contsSeq").val());
    formData("NoneForm" , "contsTitle", $("#contsTitle").val());
    formData("NoneForm" , "contsCtgSeq", $("#secondCtg option:selected").val());
    formData("NoneForm" , "contsID", $("#contsID").text());
    formData("NoneForm" , "exeFilePath", $("#exeFilePath").val());
    formData("NoneForm" , "contsSubTitle", $("#contsSubTitle").val());
    formData("NoneForm" , "contsDesc", $("#contsDesc").val());
    formData("NoneForm" , "maxAvlNop", $("#maxAvlNop").val());
    formData("NoneForm" , "genre", genre);
    formData("NoneForm" , "service", service);
    formData("NoneForm" , "cpSeq", $("#cpList option:selected").val());
    formData("NoneForm" , "cntrctStDt", $("#cntrctStDt").val());
    formData("NoneForm" , "cntrctFnsDt", $("#cntrctFnsDt").val());
    formData("NoneForm" , "dThumbnailSeqs", thumbnaildelSeq.slice(0,-1));

    callByPut("/api/contents" , "contentsUpdateSuccess", "NoneForm");
}

contentsUpdateSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if(data.resultCode == "1000"){
        uploadS();
    }
}

