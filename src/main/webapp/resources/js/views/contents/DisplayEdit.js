/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var sttus = "";
var metaData = ""; // 서브메타데이터 정보

$(document).ready(function() {
    limitInputTitle("rcessWhySbst");

    contentInfo();
    sttusChange();
});

contentStateList = function(sttusVal) {
    callByGet("/api/codeInfo?comnCdCtg=CONTS_STTUS&display=display&sttusVal="+sttusVal,"contentStateListSuccess","NoneForm");
}

contentStateListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var sttusListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            sttusListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNm+"</option>";
        });
        $("#sttus").html(sttusListHtml);
        $("#sttus > option[value=" + sttus + "]").attr("selected","true");
    }
}

contentInfo = function() {
    callByGet("/api/contents?contsSeq=" + $("#contsSeq").val() + "&reqType=display", "contentsInfoSuccess", "NoneForm", "contentsInfoFail");
}

contentsInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.contentsInfo;
        var coverImg = data.result.contentsInfo.coverImg;1

        $("#contsTitle, #headTitle").html(item.contsTitle);
        $("#contsSubTitle").html(item.contsSubTitle);
        $("#contsDesc").html(item.contsDesc);
        $("#contsId").html(item.contsID);

        //$("#contCtgNm").html("<div class='contsBox align_c float_l' >"+item.firstCtgNm+"</div><div class='contsBox align_c float_l' >"+item.secondCtgNm+"</div>");
        $("#contCtgNm").html(item.contCtgNm);

        var serviceHtml = "";
        $(data.result.contentsInfo.serviceList).each(function(i,item) {
            //serviceHtml += "<div class='contsBox align_c float_l' >"+item.svcNm+"</div>";
            if (serviceHtml != "") {
                serviceHtml += " / ";
            }
            serviceHtml += item.svcNm;
        });
        $("#serviceList").html(serviceHtml);


        $("#cretrID").html(item.cretrID);
        $("#mbrNm").html(item.mbrNm);
        $("#contsVer").html(item.contsVer);

        $("#sttus").val(item.sttus);

        $("#cp").html(item.cp);
        $("#cntrctDt").html(item.cntrctStDt+" ~ "+item.cntrctFnsDt);
        $("#cretDt").html(item.cretDt);
        $("#amdDt").html(item.amdDt);
        $("#amdrID").html(item.amdrID);
        $("#maxAvlNop").html(item.maxAvlNop);

        var genreHtml = "";
        $(data.result.contentsInfo.genreList).each(function(i,item) {
            genreHtml += item.genreNm+"/";
        });
        $("#genreList").html(genreHtml.slice(0,-1));

        if (item.metaData != "") {
            metaData = item.metaData.replace(/&#034;/gi, "\"");
            buttonHtml = "<input type=\"button\" class=\"btnDefaultWhite btnMetadata\" style=\"width:180px;\" value=\"" + getMessage("submeta.confirm") + "\" onclick=\"showMetadataPopLayer()\">";
            $("#contCtgNm").append("&nbsp;&nbsp;" + buttonHtml);
        } else {
            $(".btnMetadataSub").remove();
        }

        var serviceHtml = "";
        $(data.result.contentsInfo.serviceList).each(function(i,item) {
            //serviceHtml += "<div class='contsBox align_c float_l' >"+item.svcNm+"</div>";
            if (serviceHtml != "") {
                serviceHtml += " / ";
            }
            serviceHtml += item.svcNm;
        });
        $("#serviceList").html(serviceHtml);

        $("#fileType").html(item.fileType);
        if (item.fileType == "VIDEO") {
            if(item.firstCtgNm.match(getMessage("common.liveon")) != null){ // 첫번째 카테고리 명이 '영상_LiveOn'일때
                $("#contsDesc").parents("tr").after("<tr style='display:table-row'><th>"+getMessage("common.run.streaming.url")+"</th><td><span class='article' id='streamingUrl'>"+item.exeFilePath+"</span></td></tr>");
            }
            $(".exeFilePath").hide();
            $(".contentInfo").show();
        }
        $("#exeFilePath").html(item.exeFilePath);
        $("#coverImg").html("<a href=\""+makeAPIUrl(coverImg.coverImgPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(coverImg.coverImgPath,"img")+"' title=\""+coverImg.fileSeq+"\"></a>");
        $("#coverImg").justifiedGallery({
            rowHeight : 220,
            lastRow : 'nojustify',
            margins : 3
        });
        var videoHtml = "";
        var metadataHtml = "";
        var contsHtml = "";

        $(data.result.contentsInfo.videoList).each(function(i,item) {
            videoHtml += "<p><span class='float_l'>"+(i+1)+". "+item.fileNm+"("+item.fileSize+"Byte)</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            metadataHtml += "<p><span class='float_l'>"+item.metadataInfo.fileNm+"</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.metadataInfo.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            if(item.metadataInfo.fileNm == ""){
                metadataHtml = "";
            }
//            contsHtml += "<p>"+item.contsXmlInfo.fileNm+"<a href='" + makeAPIUrl("/api/fileDownload/"+item.contsXmlInfo.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            contsHtml = "";
        });

        $(data.result.contentsInfo.prevList).each(function(i,item) {
            $("#prevList").html("<p><span class='float_l'>"+(i+1)+". "+item.fileNm+"("+item.fileSize+"Byte)</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>");
        });

        var thumbnailHtml = "";
        $(data.result.contentsInfo.thumbnailList).each(function(i,item) {
            thumbnailHtml += "<a href=\""+makeAPIUrl(item.thumbnailPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(item.thumbnailPath,"img")+"' title=\""+item.fileSeq+"\"></a>";
        });
        $("#thumbnailList").html(thumbnailHtml);
        $("#thumbnailList").justifiedGallery({
            rowHeight : 120,
            lastRow : 'nojustify',
            margins : 3
        });
        $(".swipebox").swipebox();
        $("#widthSetting").css("width", "10%");

        var fileHtml = "";

        if (videoHtml == "") {
            $("#videoList").parents("tr").hide();
            $("#metadataName").parents("tr").hide();
            $(".contsInfo").hide();
            $(data.result.contentsInfo.fileList).each(function(i,item){
                fileHtml = "<p><span class='float_l'>"+item.fileNm+"</span>&nbsp;<a href=\"" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "\" class=\"fontblack float_l btnDownloadGray\">"+getMessage("common.string.download")+"</a></p>";
                $("#file").html(fileHtml);
            });
        } else {
            $("#videoList").html(videoHtml);
            $("#metadataName").html(metadataHtml);
            if(metadataHtml == "") {
                $("#metadataName").parents("tr").hide();
            }
            $("#contsInfo").html(contsHtml);
            $(".contsInfo").hide();

            $("#file").parents("tr").hide();
        }

        sttus = item.sttusVal;
        contentStateList(sttus);

        // 현재 페이지의 모든 이미지에 대해 img 표시 사이즈로 썸네일 이미지를 불러와 교체 (공통함수)
        imgToThumbImg();

        listBack($(".btnCancel"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/display");
    }
}

contentsInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/display");
}

function contentsSttusChange() {
    if ($("#sttus").val() == '04' && $("#rcessWhySbst").val() == "") {
        popAlertLayer(getMessage("msg.contents.refuse.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        updateSttus();
    }, null, getMessage("common.confirm"));
}

updateFail = function(data){
    popAlertLayer(getMessage("fail.common.update"));
    formDataDeleteAll("NoneForm");
}

contentsSttusChangeSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        if ($("#usesAutoSend").val() == "Y") {
            var options = { type : "single", section : "rejectConts", seq : $("#contsSeq").val() };
            sendSms(options);
        }

        popAlertLayer(getMessage("success.common.update"), "/display");
    } else {
        popAlertLayer(getMessage("msg.common.update.fail"));
    }
}

updateSttus = function() {
    formData("NoneForm", "contsSeq", $("#contsSeq").val());
    formData("NoneForm", "sttus", $("#sttus").val());
    formData("NoneForm", "prevSttus", sttus);
    formData("NoneForm", "rcessWhySbst", $("#rcessWhySbst").val());
    callByPut("/api/contents" , "contentsSttusChangeSuccess", "NoneForm");
}

function sttusChange() {
    $("#rcessWhySbst").val("");
    if ($("#sttus").val() != '04') {
        $("#rcessWhySbst").parents("tr").hide();
    } else {
        $("#rcessWhySbst").parents("tr").show();
    }
}

/* blockUI 닫기
 * parameter: x
 * return: x
 */
function closeMetaLayer() {
    $("body").css("overflow-y" , "visible");
    $("#submetadataTableArea").mCustomScrollbar('destroy');
    $.unblockUI();
}

/* blockUI 열기
 * parameter: x
 * return: x
 */

function showMetadataPopLayer() {
    callByGet("/api/codeInfo?comnCdCtg=CONTS_SUB_META", "showMetadataConts", "NoneForm");
}

/* 서브 메타데이터 상세보기 */
showMetadataConts = function(data) {
    if(data.resultCode == "1000"){
        var tableHtml = "";
        var metaVal = "";
        $("#detailSubmetadata").html("");
        $(data.result.codeList).each(function(i, item) {
            var metaObj = JSON.parse(metaData);
            for (key in metaObj) {
                if (key == item.comnCdValue) {
                    if (item.comnCdNm.indexOf(getMessage("submeta.time")) != -1 && metaObj[key] != "") {
                        metaVal = metaObj[key] + " " + getMessage("common.minute");
                    } else if(item.comnCdNm.indexOf(getMessage("submeta.rt")) != -1){
                        // #a8484는 작은 따옴표(') #q0808은 큰 따옴표(")로 DB 저장
                        if(metaObj[key].match("#a8484") != null || metaObj[key].match("#q0808") != null){
                            metaObj[key] = metaObj[key].replace("#a8484", "\'");
                            metaObj[key] = metaObj[key].replace("#q0808", "\"");
                        }
                        metaVal = metaObj[key];
                    } else {
                        metaVal = metaObj[key];
                    }
                }
            }

            if (metaVal != "") { //메타데이터가 없으면 조회 리스트에 추가하지 않음
                tableHtml += "<tr><th>" + item.comnCdNm + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">" + "</th><td>" + metaVal + "</td>";
                metaVal = "";
            }
            tableHtml += "</tr>";
        });

        $("#detailSubmetadata").append(tableHtml);
    } else {
        $("#detailSubmetadata").html("");
    }
    $("#popupDetailSubmetadata").css({"left":"-1000px","top":"-1000px"}).show();
    var popupHeight = $("#popupDetailSubmetadata").height();
    var popupWidth = $("#popupDetailSubmetadata").width();
    $("#popupDetailSubmetadata").hide();
    popLayerDiv("popupDetailSubmetadata", popupWidth, popupHeight, true);
    $("#submetadataTableArea").mCustomScrollbar({ axis : "y", theme:"inset-3" });
}