/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var NAME = 0;
var ID = 1;

var didRecieveFirstCategoryList = false;

$(document).ready(function() {
    limitInputTitle("firstFormFirstCategoryName,secondFormFirstCategoryName");

    setkeyup();

    $("#firstCategoryList ul").click(function(e) {
        if (!e.target || e.target.nodeName.toLowerCase() != "li") {
            return;
        }
        var oldValue = $("#firstCategoryList ul li.selected").attr("data");
        var newValue = $(e.target).attr("data");
        if (oldValue == newValue) {
            return;
        }

        if ($("#firstCategoryList ul li.selected").hasClass("selected")) {
            $("#firstCategoryList ul li.selected").removeClass("selected");
        }
        $(e.target).addClass("selected");
        getCategoryList();
    });

    $("#secondCategoryList ul").click(function(e) {
        if (!e.target || e.target.nodeName.toLowerCase() != "li") {
            return;
        }
        var oldValue = $("#secondCategoryList ul li.selected").attr("data");
        var newValue = $(e.target).attr("data");
        if (oldValue == newValue) {
            return;
        }

        if ($("#secondCategoryList ul li.selected").hasClass("selected")) {
            $("#secondCategoryList ul li.selected").removeClass("selected");
        }
        $(e.target).addClass("selected");
        updateContentsList();
    });

    initContentList();

    resizeJqGridWidth("jqgridData", "gridArea");

    getCategoryList();
});

getCategoryList = function() {
    formData("NoneForm", "GroupingYN", "Y");
    var firstCategoryID = $("#firstCategoryList ul li.selected").attr("data");
    if (firstCategoryID) {
        formData("NoneForm", "firstCtgID", firstCategoryID);
    }
    callByGet("/api/contentsCategory" , "didReceiveCategoryList", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didReceiveCategoryList = function(data) {
    var firstCategoryListHtml = "";
    var secondCategoryListHtml = "";

    if (data.result) {
        $(data.result.contsCtgList).each(function(i,item) {
            secondCategoryListHtml += "<li data=" + item.contsCtgSeq + ">" + item.secondCtgNm + "(" + item.secondCtgID + ")</li>";
        });
    }

    $("#secondCategoryList ul").html(secondCategoryListHtml);
    $("#secondCategoryList ul li:eq(0)").addClass("selected");

    if (!didRecieveFirstCategoryList) {
        didRecieveFirstCategoryList = true;

        if (data.result) {
            $(data.result.firstCtgList).each(function(i,item) {
                firstCategoryListHtml += "<li data='" + item.firstCtgID + "'>" + item.firstCtgNm + "(" + item.firstCtgID + ")</li>";
            });
        }

        $("#firstCategoryList ul").html(firstCategoryListHtml);
        $("#firstCategoryList ul li:eq(0)").addClass("selected");
    }

    if (data.resultCode != "1000") {
        clearGridData();
        return;
    }

    updateContentsList();
}

initContentList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("contents.table.id"),
        getMessage("contents.table.name"),
        getMessage("table.category"),
        getMessage("table.reger"),
        getMessage("table.progressStatus"),
        getMessage("table.regdate"),
        "seq"
    ];
    $("#jqgridData").jqGrid({
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentsList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames: columnNames,
        colModel:[
            {name:"num", index: "num", align:"center", width:40, hidden:true},
            {name:"contsID", index:"CONTS_ID", align:"center"},
            {name:"contsTitle", index: "CONTS_TITLE", align:"center", classes:"pointer", formatter:pointercursor},
            {name:"contCtgNm", index:"CTG_NM", align:"center", sortable:false},
            {name:"cretrNm", index:"CRETR_ID", align:"center"},
            {name:"sttus", index:"STTUS", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"contsSeq", index:"seq", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: 10, // 페이지에 출력될 칼럼 개수
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "contsSeq",
            sortorder: "desc",
            height: "auto",
            multiselect: false,
            postData: {
                contsCtgSeq: $("#secondCategoryList ul li.selected").attr("data")
            },
            beforeRequest:function() {

            },
            /*
             * parameter: x
             * jqGrid 그리기가 끝나면 함수 실행
             */
            loadComplete: function() {
                //session
                if (sessionStorage.getItem("last-url") != null) {
                    sessionStorage.removeItem('state');
                    sessionStorage.removeItem('last-url');
                }

                $(this).find("td").each(function(){
                    if ($(this).index() == 5) { // 구분
                        var str = $(this).text();

                        if(str == getMessage("contents.table.state.exhibition.success")){
                            str = "<span class='viewIcon'>"+getMessage("contents.table.state.verify.success") + "</span>";
                        }
                        $(this).html(str);
                    }
                });
            },
            loadError: function (jqXHR, textStatus, errorThrown) {
                clearGridData();
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event) {
                // 제목 셀 클릭시
                if (columnId == 2) {
                    sessionStorage.setItem("last-url", location);
                    sessionStorage.setItem("state", "view");

                    var list = $("#jqgridData").jqGrid('getRowData', rowId);
                    pageMove("/contents/"+list.contsSeq);
                }
            }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

function updateContentsList() {
    $("#jqgridData").jqGrid("setGridParam",
        {
        url:makeAPIUrl("/api/contents"),
        postData:{
                contsCtgSeq: $("#secondCategoryList ul li.selected").attr("data")
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

/* blockUI 열기
 * parameter: name(blockUI를 설정할 ID값)
 * return: x
 */
function categoryInsertFormShow(name) {
    if (name == "firstCategoryForm") {
        $("#firstCategoryForm .popupTitle").text(getMessage("contents.category.firstCategory.regist.msg"));
        $("#firstFormFirstCategoryId").replaceWith("<input id='firstFormFirstCategoryId' type='text' class='inputBox lengthL' maxlength='1' onkeyup='categoryValueValidationCheck(this)'>");
        var btnSave = "<input id='btn_saveFirstCategory' type='button' class='btnNormal btnWrite' value='" + getMessage("button.create.confirm") + "' onclick=\"confirmCategoryInsert('first')\" />";
        $("#btn_saveFirstCategory").replaceWith(btnSave);
    } else if (name == "secondCategoryForm") {
        if (!$("#firstCategoryList ul li").length || !$("#firstCategoryList ul li.selected").length) {
            popAlertLayer(getMessage("contents.category.alert.add.firstCategory"));
            return;
        }

        $("#secondCategoryForm .popupTitle").text(getMessage("contents.category.secondCategory.regist.msg"));
        var firstCategoryInfo = splitText($("#firstCategoryList ul li.selected").text());
        $("#secondFormFirstCategoryName").text(firstCategoryInfo[NAME]);
        $("#secondFormFirstCategoryId").text(firstCategoryInfo[ID]);
        $("#secondFormSecondCategoryId").replaceWith("<input id='secondFormSecondCategoryId' type='text' class='inputBox lengthL' maxlength='1' onkeyup='categoryValueValidationCheck(this)'>");

        var btnSave = "<input id='btn_saveSecondCategory' type='button' class='btnNormal btnWrite' value='" + getMessage("button.create.confirm") + "' onclick=\"confirmCategoryInsert('second')\" />";
        $("#btn_saveSecondCategory").replaceWith(btnSave);
    }

    popLayerDiv(name, 520, 320, true);
}

function categoryUpdateFormShow(name) {
    var firstCategoryInfo = splitText($("#firstCategoryList ul li.selected").text());
    if (name == "firstCategoryForm") {
        if (!$("#firstCategoryList ul li").length || !$("#firstCategoryList ul li.selected").length) {
            popAlertLayer(getMessage("contents.category.alert.firstCategory.no.update"));
            return;
        }

        $("#firstCategoryForm .popupTitle").text(getMessage("contents.category.firstCategory.modify.msg"));
        $("#firstFormFirstCategoryName").val(firstCategoryInfo[NAME]);
        $("#firstFormFirstCategoryId").replaceWith("<span id='firstFormFirstCategoryId'>" + firstCategoryInfo[ID] + "</span>");

        var btnSave = "<input id='btn_saveFirstCategory' type='button' class='btnNormal btnWrite' value='" + getMessage("button.update.confirm") + "' onclick=\"confirmCategoryUpdate('first')\">";
        $("#btn_saveFirstCategory").replaceWith(btnSave);
    } else if (name == "secondCategoryForm") {
        if (!$("#firstCategoryList ul li").length || !$("#firstCategoryList ul li.selected").length) {
            popAlertLayer(getMessage("contents.category.alert.add.firstCategory"));
            return;
        }

        if (!$("#secondCategoryList ul li").length || !$("#secondCategoryList ul li.selected").length) {
            popAlertLayer(getMessage("contents.category.alert.secondCategory.no.update"));
            return;
        }

        var secondCategoryInfo = splitText($("#secondCategoryList ul li.selected").text());
        if (secondCategoryInfo[NAME] == 'basic' || secondCategoryInfo[ID] == '1') {
            popAlertLayer(getMessage("contents.category.prevent.update.default"));
            return;
        }
        $("#secondCategoryForm .popupTitle").text(getMessage("contents.category.secondCategory.modify.msg"));

        $("#secondFormFirstCategoryName").text(firstCategoryInfo[NAME]);
        $("#secondFormFirstCategoryId").text(firstCategoryInfo[ID]);

        $("#secondFormSecondCategoryName").val(secondCategoryInfo[NAME]);
        $("#secondFormSecondCategoryId").replaceWith("<span id='secondFormSecondCategoryId'>" + secondCategoryInfo[ID] + "</span>");

        var btnSave = "<input id='btn_saveSecondCategory' type='button' class='btnNormal btnWrite' value='" + getMessage("button.update.confirm") + "' onclick=\"confirmCategoryUpdate('second')\">";
        $("#btn_saveSecondCategory").replaceWith(btnSave);
    }

    popLayerDiv(name, 500, 300, true);
}

/* blockUI 닫기
 * parameter: x
 * return: x
 */
function popLayerClose(){
    $("body").css("overflow-y" , "visible");
    $.unblockUI();

    $("#firstCategoryForm input[type=text], #secondCategoryForm input[type=text]").val("");
}

function categoryValueValidationCheck(object) {
    if (object.value) {
        var re = /^[A-Z]+$/;
        if(!re.test(object.value)){
            popAlertLayer(getMessage("common.upper.msg"), '', '', '', object.id);
            object.value = "";
            return;
        }
    }
}

function confirmCategoryInsert(type) {
    if (type == "first") {
        if(!$("#" + type + "FormFirstCategoryName").val()){
            popAlertLayer(getMessage("contents.category.alert.firstCategory.name"));
            return;
        }

        if (!validateMaxLength($("#" + type + "FormFirstCategoryName").val(), 50)) {
            popAlertLayer(getMessage("contents.category.alert.name.maxLength"), "", "", "", type + "FormFirstCategoryName");
            return;
        }

        if(!$("#" + type + "FormFirstCategoryId").val()){
            popAlertLayer(getMessage("contents.category.alert.firstCategory.id"));
            return;
        }
    } else if(type == 'second'){
        if(!$("#" + type + "FormSecondCategoryName").val()){
            popAlertLayer(getMessage("contents.category.alert.secondCategory.name"));
            return;
        }

        if (!validateMaxLength($("#" + type + "FormSecondCategoryName").val(), 50)) {
            popAlertLayer(getMessage("contents.category.alert.name.maxLength"), "", "", "", type + "FormSecondCategoryName");
            return;
        }

        if(!$("#" + type + "FormSecondCategoryId").val()){
            popAlertLayer(getMessage("contents.category.alert.secondCategory.id"));
            return;
        }
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        categoryInsert(type);
    }, null, getMessage("button.confirm"));
}

function categoryInsert(type) {
    if (type == "first") {
        formData("NoneForm", "firstCtgNm", $("#" + type + "FormFirstCategoryName").val());
        formData("NoneForm", "firstCtgID", $("#" + type + "FormFirstCategoryId").val());
    } else if(type == 'second'){
        var firstCategoryInfo = splitText($("#firstCategoryList ul li.selected").text());
        formData("NoneForm", "firstCtgNm", firstCategoryInfo[NAME]);
        formData("NoneForm", "firstCtgID", firstCategoryInfo[ID]);

        formData("NoneForm", "secondCtgNm", $("#" + type + "FormSecondCategoryName").val());
        formData("NoneForm", "secondCtgID", $("#" + type + "FormSecondCategoryId").val());
    }

    callByPost("/api/contentsCategory", "categoryInsertSuccess", "NoneForm", "categoryInsertFail");
    formDataDeleteAll("NoneForm");
}

function categoryInsertSuccess(data){
    if(data.resultCode == "1000"){
        if ($("#usesAutoSend").val() == "Y" && data.insertType === "second") {
            var options = { type : "multiple", section : "registCtg", seq : data.contsCtgSeq };
            sendSms(options);
        }

        popAlertLayer(getMessage("success.common.insert"), "/contents/category");
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("contents.category.alert.duplication"));
    } else if (data.resultCode == "1013") {
        popAlertLayer(getMessage("contents.category.alert.second.duplication"));
        $("#secondFormSecondCategoryId").val(data.secondCtgId);
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

function categoryInsertFail(data){
    popAlertLayer(getMessage("fail.common.insert"));
}

function confirmCategoryUpdate(type) {
    if (type == 'first') {
        if(!$("#" + type + "FormFirstCategoryName").val()){
            popAlertLayer(getMessage("contents.category.alert.secondCategory.name"));
            return;
        }

        if (!validateMaxLength($("#" + type + "FormFirstCategoryName").val(), 50)) {
            popAlertLayer(getMessage("contents.category.alert.name.maxLength"), "", "", "", type + "FormFirstCategoryName");
            return;
        }
    } else if (type == 'second') {
        if(!$("#" + type + "FormSecondCategoryName").val()){
            popAlertLayer(getMessage("contents.category.alert.secondCategory.name"));
            return;
        }

        if (!validateMaxLength($("#" + type + "FormSecondCategoryName").val(), 50)) {
            popAlertLayer(getMessage("contents.category.alert.name.maxLength"), "", "", "", type + "FormSecondCategoryName");
            return;
        }
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        categoryUpdate(type);
    }, null, getMessage("button.confirm"));
}

function categoryUpdate(type) {
    var firstCategoryInfo = splitText($("#firstCategoryList ul li.selected").text());
    formData("NoneForm", "firstCtgNm", firstCategoryInfo[NAME]);
    formData("NoneForm", "firstCtgID", firstCategoryInfo[ID]);

    if (type == 'second') {
        var secondCategoryInfo = splitText($("#secondCategoryList ul li.selected").text());
        formData("NoneForm", "secondCtgNm", secondCategoryInfo[NAME]);
        formData("NoneForm", "secondCtgID", secondCategoryInfo[ID]);
    }

    if (type == 'first') {
        formData("NoneForm", "contCtgNm", $("#" + type + "FormFirstCategoryName").val());
    } else if (type == 'second') {
        formData("NoneForm", "contCtgNm", $("#" + type + "FormSecondCategoryName").val());
    }

    callByPut("/api/contentsCategory", "categoryUpdateSuccess", "NoneForm", "categoryUpdateFail");
    formDataDeleteAll("NoneForm");
}

function categoryUpdateSuccess(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.update"), "/contents/category");
    } else if (data.resultCode == "1011" || data.resultCode == "1013") {
        popAlertLayer(getMessage("contents.category.alert.duplication"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function categoryUpdateFail(data){
    popAlertLayer(getMessage("fail.common.update"));
}

function confirmDeleteCategory(type) {
    if (!$("#firstCategoryList ul li").length || !$("#firstCategoryList ul li.selected").length) {
        popAlertLayer(getMessage("contents.category.alert.add.firstCategory"));
        return;
    }

    if (!$("#secondCategoryList ul li").length || !$("#secondCategoryList ul li.selected").length) {
        popAlertLayer(getMessage("contents.category.alert.secondCategory.no.delete"));
        return;
    }

    var firstCategoryInfo = splitText($("#firstCategoryList ul li.selected").text());
    var title = firstCategoryInfo[NAME];
    if (type == 'second') {
        var secondCategoryInfo = splitText($("#secondCategoryList ul li.selected").text());
        title += " > " + secondCategoryInfo[NAME] + " ";
    }

    var msg = title + getMessage("contents.category.delete.msg");
    var totalCount = $("#secondCategoryList ul li").length;
    if (totalCount <= 1) {
        msg = title + getMessage("contents.category.alert.firstCategory.delete") + " " + getMessage("contents.category.delete.msg");
    }

    popConfirmLayer(msg, function() {
        categoryDelete();
    }, null, getMessage("button.confirm"));
}

function categoryDelete() {
    var contsCtgSeq = $("#secondCategoryList ul li.selected").attr("data");
    formData("NoneForm", "contsCtgSeq", contsCtgSeq);
    callByDelete("/api/contentsCategory", "categoryDeleteSuccess", "NoneForm", "categoryDeleteFail");

    formDataDeleteAll("NoneForm");
}

function categoryDeleteSuccess(data) {
    if (data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"), "/contents/category");
    } else if (data.resultCode == "1012") {
        popAlertLayer(getMessage("contents.category.fail.second.delete"));
    } else if (data.resultCode == "1013") {
        popAlertLayer(getMessage("contents.category.fail.first.delete"));
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

function categoryDeleteFail(data) {
    popAlertLayer(getMessage("fail.common.delete"));
}

function categoryDeleteFail(data) {
    popAlertLayer(getMessage("fail.common.delete"));
}

function splitText(text) {
    var splitIndex = text.lastIndexOf("(");
    var dataArr = [];
    dataArr.push(text.substring(0, splitIndex));
    dataArr.push(text.substring(splitIndex).replace(/[()]/g, ""));
    return dataArr;
}

function clearGridData() {
    $("#jqgridData").clearGridData();
    $("#sp_1_pageDiv").text(1);
}