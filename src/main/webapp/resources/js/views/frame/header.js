/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    $("#ckb_all").click(function(){
        $("input:checkbox[name='child_ckb']").attr("checked", $(this).is(":checked"));
    });

    $(".numberValueCheck").keyup(function(){
        var numberRegular = /^[0-9]+$/;
        if(!numberRegular.test(this.value) && this.value != ""){
            popAlertLayer(getMessage("fail.common.only.number.msg"));
            this.value = "";
            return;
        }
    });

    $(".koreanValueCheck").keyup(function(){
        var koreanRegular = /^[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]+$/;
        if(!koreanRegular.test(this.value) && this.value != ""){
            popAlertLayer(getMessage("login.alert.kor.length.msg"));
            this.value = "";
            return;
        }
    });
});

/*
 * 로그아웃
 * */

function logout() {
    callByPost("/api/logout", "logoutSuccess", "");
}

function logoutSuccess(data){
    if(data.resultCode == "1000") {
        popAlertLayer(getMessage("login.msg.logout"), "/");
    }
}