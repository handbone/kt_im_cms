/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var d_Height = document.body.clientHeight;
var left_View_Height = $("#leftGrp > ul").height();
$(window).resize(function(){
    scrollUI();
});

$(document).ready(function(){
    /* 초기 leftMenu display */
    viewSubMenu();
    $(".leftMenu").click(function(){
        var menuObj = $(this);
        if (!$(this).hasClass("leftMenuOn")) {
            $(".leftMenu").removeClass("leftMenuOn");
            $(this).addClass("leftMenuOn");
            $(".leftSubMenu").slideUp("fast", function(){});
            while(1) {
                if ($(menuObj).next().hasClass('leftSubMenu') == false) {
                    break;
                } else {
                    $(menuObj).next().slideDown("fast", function(){
                        clearTimeout(window.resizedFinished);
                        window.resizedFinished = setTimeout(function(){
                            scrollUI();
                        }, 150);
                    });
                }

                menuObj =$(menuObj).next();
            }
        } else {
            $(".leftMenu").removeClass("leftMenuOn");
            while(1) {
                if ($(menuObj).next().hasClass('leftSubMenu') == false) {
                    break;
                } else {
                    $(menuObj).next().slideUp("fast", function(){});
                }

                menuObj =$(menuObj).next();
            }
        }

    });

});

// popConfirmLayer 안에서 popAlertLayer를 사용할 때 isMulti 여부 체크
function popAlertLayer(msg, url,title,centerBtn, targetId, isMulti){
    if (isMulti) {
        setVisibilitySweetAlert(true, true);
    } else {
        setVisibilitySweetAlert(true);
    }
    title = typeof title !== 'undefined' ? title : getMessage("common.noice");
    centerBtn = typeof centerBtn !== 'undefined' ? centerBtn : false;

    if(url){
        swal({
            title: title,
            text: msg,
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: getMessage("common.confirm")
        })
        .then(
            function(value){
                if(value){

                    swal.close();
                    if (typeof url == "function") {
                        url();
                    } else {
                        pageMove(url);
                    }
                    setVisibilitySweetAlert(false);
                } else {
                    swal({
                        title: getMessage("common.noice"),
                        text: msg,
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        buttons: getMessage("common.confirm")
                    }).then(
                        function(value) {
                            setVisibilitySweetAlert(false);
                        }
                    );
                }
            }
        );
    } else {
        swal({
            title: title,
            text: msg,
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: getMessage("common.confirm")
        })
        .then(
                function(value) {
                    if (targetId) {
                        document.getElementById(targetId).focus();
                    }
                    if (isMulti) {
                        setVisibilitySweetAlert(false, false);
                    } else {
                        setVisibilitySweetAlert(false);
                    }
                }
        );
    }

    if (centerBtn) {
        $(".swal-footer").css("text-align","center");
    } else {
        $(".swal-footer").css("text-align","right");
    }

    $(".swal-text").html($(".swal-text").text());
}

function popAlertLayerObj(msg, url,title,centerBtn, target){
    setVisibilitySweetAlert(true);
    title = typeof title !== 'undefined' ? title : getMessage("common.noice");
    centerBtn = typeof centerBtn !== 'undefined' ? centerBtn : false;

    if(url){
        swal({
            title: title,
            text: msg,
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: getMessage("common.confirm")
        })
        .then(
            function(value){
                if(value){

                    swal.close();
                    if (typeof url == "function") {
                        url();
                    } else {
                        pageMove(url);
                    }
                    setVisibilitySweetAlert(false);
                } else {
                    swal({
                        title: getMessage("common.noice"),
                        text: msg,
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        buttons: getMessage("common.confirm")
                    }).then(
                        function(value) {
                            setVisibilitySweetAlert(false);
                        }
                    );
                }
            }
        );
    } else {
        swal({
            title: title,
            text: msg,
            closeOnClickOutside: false,
            closeOnEsc: false,
            buttons: getMessage("common.confirm")
        })
        .then(
                function(value) {
                    if (target) {
                        $(target).focus();
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    }

    if (centerBtn) {
        $(".swal-footer").css("text-align","center");
    } else {
        $(".swal-footer").css("text-align","right");
    }

    $(".swal-text").html($(".swal-text").text());
}

var multiCnt = 0;
function popConfirmLayer(msg, fnc, end, title, isMulti){
    if (isMulti) {
        multiCnt++;
        if (multiCnt == 1) {
            // isMulti true인 상태에서 popConfirmLayer를 다중 호출 시 생기는 에러 방지
            setVisibilitySweetAlert(true, true, true);
            multiCnt = 0;
        } else {
            setVisibilitySweetAlert(true, true);
        }
    } else {
        setVisibilitySweetAlert(true);
    }
    title = typeof title !== 'undefined' ? title : getMessage("common.noice");
    swal({
        buttons: {
            confirm: getMessage("common.confirm"),
            cancel: getMessage("button.reset")
        },
        title: title,

        closeOnClickOutside: false,
        closeOnEsc: false,
        text: msg
    })
    .then(
        function(value){
            if(value){
                fnc();
            } else {
                if(end){
                    end();
                }
            }
            if (isMulti) {
                if (value) {
                    setVisibilitySweetAlert(false, true);
                } else {
                    setVisibilitySweetAlert(false, false);
                }
            } else {
                setVisibilitySweetAlert(false);
            }
        }
    );
    $(".swal-text").html($(".swal-text").text());
}

function popAlertLayerFnc(msg, fnc, url){
    setVisibilitySweetAlert(true);
    title = typeof title !== 'undefined' ? title : getMessage("common.noice");
    centerBtn = typeof centerBtn !== 'undefined' ? centerBtn : false;
    url = typeof url !== 'undefined' ? url : false;

    swal({
        title: title,
        text: msg,
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons: getMessage("common.confirm")
    })
    .then(
            function(value) {
                if(value){
                    if (url) {
                        hrefReload(url);
                    } else {
                        fnc();
                    }
                }
                setVisibilitySweetAlert(false);
            }
    );

    $(".swal-text").html($(".swal-text").text());

hrefReload = function(url){
    location.reload(url);
}}

reload = function(){
    location.reload();
}

function viewSubMenu(){
    $(".leftSubMenu").css("display","none");

    var nextObj = $(".leftMenuOn").next();

    while(1){
        if ($(nextObj).hasClass("leftSubMenu")) {
            $(nextObj).css("display", "block");
            nextObj = $(nextObj).next();
        } else {
            break;
        }
    }

}
function scrollUI(){
    d_Height = document.body.clientHeight - ($(".logo").height() + $(".logo").next("li").height()+ 72.000318);
//    left_View_Height = $("#leftGrp > ul").height();

    var h = 0;
    $(".leftMenuGrp > ul > li").each(function(){
        if ($(this).css("display") != "none") {
            if ($(this).hasClass("leftSubMenu")) {
                h += 38;
            } else {
                h += 51;
            }
        }
    });
    left_View_Height = h;

    if(d_Height < left_View_Height){
        $(".leftMenuGrp > ul").css({"overflow-y":"scroll","height":d_Height});
        $(".leftMenuGrp > ul").focus();
    } else {
        $(".leftMenuGrp > ul").css({"overflow":"auto","height":"auto"});
    }
}

/* 이용약관 및 개인정보 처리방침 팝업 */
function showPolicyPopup(name) {
    popLayerDiv(name, 500, 300, true);

    if (name == "popupWin") {
        $("#termsContent").html(getTermInfo());
    } else {
        $("#policyContent").html(getPolicyInfo());
    }
}

function showPolicyPopupClose() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}


function cofirmMove(mvPage) {
    if(empty(glo_url)) {
        return;
    }

    var chkUrl = glo_url.replace(ContextPath,"");

    if (chkUrl == "/mypage/edit") {
        pageMove(mvPage);
        return;
    }
    $("#cofirmPwd").val("");
    popLayerDiv("popupComfirm",0,0,true);
    $("#popupComfirm .btnOk").attr("onclick","chkPasswd('"+mvPage+"')");
}
var pageVal;
function chkPasswd(mvPage){
    if ($("#cofirmPwd").val().trim() == null || $("#cofirmPwd").val().trim() == "") {
        popAlertLayer(getMessage("msg.common.passwd.put"));
        return;
    }

    pageVal = mvPage;
    formData("NoneForm" , "type", "comfirmPassword");
    formData("NoneForm" , "pwd", $("#cofirmPwd").val());
    callByGet("/api/member/find" , "didFindUserInfo", "NoneForm", "didNotFindUserInfo");
    formDataDeleteAll("NoneForm");
}

function didFindUserInfo(data) {
    $("#loading").hide();
    if (data.resultCode == "1000") {
        var item = data.result.memberInfo;
        sessionStorage.setItem("cofirm-Pwd",item.mbrPwd);
       pageMove(pageVal);
    } else {
        popAlertLayer(getMessage("fail.member.notMatched.oldPassword"));
    }
}

function didNotFindUserInfo(data) {
    $("#loading").hide();
    var result = JSON.parse(data.responseText);
    popAlertLayer(getLoginMessage(result.resultMsg));
}