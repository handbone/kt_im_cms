/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var barChartData;
var maxValue = 0;

var content_status = {
    0 : "total",
    1 : "save",
    2 : "request",
    3 : "verify",
    4 : "reject",
    5 : "complete"
}

var content_msg_id = {
    0 : "select.all",
    1 : "contents.table.state.regist",
    2 : "contents.table.state.verify.Waiting",
    3 : "contents.table.state.verify.ing",
    4 : "contents.table.state.verify.Companion",
    5 : "contents.table.state.verify.success"
}

var chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

$(document).ready(function() {
    $(".conTitle.tabOff").click(function() {
        pageMove("/dashboard/contents/registered");
    });

    $("#serviceList").change(function() {
        changeServiceList();
    });

    getServiceList();
});

initBarChartData = function() {
    maxValue = 0;
    barChartData = {
        labels: [],
        datasets: [{
            label: getMessage("select.all"),
            backgroundColor: chartColors.blue,
            borderColor: chartColors.blue,
            borderWidth: 1,
            data: []
        }, {
            label: getMessage("contents.table.state.regist"),
            backgroundColor: chartColors.orange,
            borderColor: chartColors.orange,
            borderWidth: 1,
            data: []
        }, {
            label: getMessage("contents.table.state.verify.Waiting"),
            backgroundColor: chartColors.grey,
            borderColor: chartColors.grey,
            borderWidth: 1,
            data: []
        }, {
            label: getMessage("contents.table.state.verify.ing"),
            backgroundColor: chartColors.yellow,
            borderColor: chartColors.yellow,
            borderWidth: 1,
            data: []
        }, {
            label: getMessage("contents.table.state.verify.Companion"),
            backgroundColor: chartColors.purple,
            borderColor: chartColors.purple,
            borderWidth: 1,
            data: []
        }, {
            label: getMessage("contents.table.state.verify.success"),
            backgroundColor: chartColors.green,
            borderColor: chartColors.green,
            borderWidth: 1,
            data: []
        }]
    };

    if (window.myBar) {
        window.myBar.data.labels = barChartData.labels;
        window.myBar.data.datasets = barChartData.datasets;
        window.myBar.options.scales.yAxes[0].ticks.max = maxValue;
    }
}

getServiceList = function() {
    callByGet("/api/service?searchType=list", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("info.nodata.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        return;
    }

    var serviceListHtml = "";
    $(data.result.serviceNmList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    getTotalContents();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
}

changeServiceList = function() {
    getTotalContents();
}

getTotalContents = function() {
    formData("NoneForm" , "type", "totalContents");
    formData("NoneForm" , "svcSeq", $("#serviceList option:selected").val());
    callByGet("/api/statistics", "didReceiveTotalContentsInfo", "NoneForm", "didNotReceiveTotalContentsInfo");
    formDataDeleteAll("NoneForm");
}

didReceiveTotalContentsInfo = function(data) {
    initBarChartData();

    if (data.resultCode != "1000") {
        if (window.myBar) {
            window.myBar.update();
        }

        popAlertLayer(getMessage("cms.release.msg.noData"));
        return;
    }

    $(data.result.totalContentsStatusList).each(function(i, item) {
        barChartData.labels.push(item.cpNm);
        for (var index = 0; index < barChartData.datasets.length; ++index) {
            var value = item[content_status[index]];
            barChartData.datasets[index].data.push(value);
            if (maxValue < value) {
                maxValue = value;
            }
        }
    });

    drawBarChart();
}

didNotReceiveTotalContentsInfo = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
}

drawBarChart = function() {
    if (!window.myBar) {
        var titleName = getMessage("dashboard.contents.verify.status");
        var options = {
            type: 'bar',
            data: barChartData,
            options: {
                maintainAspectRatio :false,
                scaleBeginAtZero: true,
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return data['labels'][tooltipItem[0]['index']];
                        },
                        label: function(tooltipItem, data) {
                            var text = [];
                            $(data.datasets).each(function(i, item) {
                                text.push("  -  " + getMessage(content_msg_id[i]) + " : " + item.data[tooltipItem.index]);
                            });
                            return text;
                        }
                    },
                    titleFontSize : 16,
                    titleFontColor : '#0066ff',
                    bodyFontSize : 12,
                    displayColors : false
                },
                title : {
                    text : titleName,
                    display : true,
                    fontSize : 18
                },
                legend : {
                    position : 'bottom',
                    onClick : null
                },
                scales : {
                    yAxes : [{
                            ticks: {
                                max : getYaxedMaxValue(maxValue),
                                fontFamily: "Montserrat",
                                beginAtZero: true,
                                fixedStepSize: fixedStepSizeE(maxValue)
                            }
                    }],
                    xAxes : [{
                        gridLines : {
                            display :true,
                            lineWidth : 1,
                            color : "rgba(0,0,0,0.80)"
                        }
                    }]
                }
            }
        };

        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, options);
    } else {
        window.myBar.data.labels = barChartData.labels;
        window.myBar.data.datasets = barChartData.datasets;
        window.myBar.options.scales.yAxes[0].ticks.max = getYaxedMaxValue(maxValue);
        window.myBar.options.scales.yAxes[0].ticks.fixedStepSize = fixedStepSizeE(maxValue);
        window.myBar.update();
    }
}

getYaxedMaxValue = function(max) {
    max = parseFloat(max);
    var key = [30,100,200,300,500,1000];
    var value =[5,10,20,30,50,100];
    var answer;
    for(var i=0;i<key.length;i++) {
        if(key[i] >= max) {
            answer = key[i];
            return answer;
        }
        
    }
    if(max > 1000) {
        answer = Math.ceil(parseFloat(max) * 1.1);
        return answer;
    }

}

fixedStepSizeE = function(max) {
    max = parseFloat(max);
    var key = [30,100,200,300,500,1000];
    var value =[5,10,20,30,50,100];
    var answer;
    for(var i=0;i<key.length;i++) {
        if(key[i] >= max) {
            answer = value[i];
            return answer;
        }
        
    }
    if(max > 1000) {
        answer = 200;
        return answer;
    }
}
