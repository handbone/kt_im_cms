/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var visitChart;
var tagChart;

var startDate;
var endDate;

var ticketRgData = new Array();
var maxValue_1 = 0;
var maxValue_2 = 0;
var dataLables = new Array(); 
var graphColors = [
    'rgb(214, 107, 125)',
    'rgb(148, 160, 229)',
    'rgb(154, 229, 160)',
    'rgb(182, 223, 140)',
    'rgb(227, 223, 183)',
    'rgb(203, 148, 104)',
    'rgb(178, 112, 114)',
    'rgb(117, 112, 151)',
    'rgb(107, 186, 152)',
    'rgb(107, 100, 115)'
];

var popupId;

$(document).ready(function() {
    $("#serviceList").change(function() {
        changeServiceList();
    });

    $("#storeList").change(function() {
        changeStoreList();
    });

    for (var i=0; i < 24; i++) {
        var t = "0";
        if (i >= 10) {
            t = i;
        } else {
            t += i;
        }
        dataLables.push(t + getMessage("common.hour"));
    }
    dataLables.sort();

    var now = new Date();
    startDate = dateString(new Date(now.getTime())) + " 00:00";
    endDate = dateTimeString(new Date(now.getTime()));

    var mbrSe = $("#memberSec").val();
    var disabledServiceSelectBox = (mbrSe == "01" || mbrSe == "02") ? false : true;
    var disabledStoreSelectBox = (mbrSe == "01" || mbrSe == "02" || mbrSe == "04") ? false : true;

    $("#serviceList").attr("disabled", disabledServiceSelectBox);
    $("#storeList").attr("disabled", disabledStoreSelectBox);

    if (mbrSe == "05") {
        getNotice();
    }

    getServiceList();
});

showNoticePopup = function(result) {
    var noticeInfo = result.noticeDetail;
    popupId = 'hnnpop' + noticeInfo.noticeSeq;
    if (typeof $.cookie(popupId) != "undefined") {
        return;
    }

    var noticePopupHtml = "<div id='noticeWindow' style='display:none'>";
    noticePopupHtml += "<div class='noticeTitle'><div id='title' style='white-space: nowrap; padding: 5px;'><span style='text-overflow: ellipsis; overflow: hidden; width: 100%; left:0px;'>" + noticeInfo.noticeTitle + "</span></div>";
    noticePopupHtml += "<div class='btnCloseWin dragon'><a href='javascript:;'><img src='" + makeAPIUrl("/resources/image/btn_close_small.png") + "'></a></div>";
    noticePopupHtml += "</div>";
    noticePopupHtml += "<div class='noticeTxt dragon'>";
    noticePopupHtml += "<p id='regDate' style='padding: 5px;font-size: 12px;text-align: right;border-bottom: inherit;background: rgba(179, 175, 175, 0.25);'>" + getMessage("column.title.cretDt") + " : " + noticeInfo.regDt + "</p>";
    noticePopupHtml += "<div id='notiSbst' style='border: 2px ridge rgba(255, 247, 247, 0.25); height: 190px; max-height: 190px; overflow-y: auto'>";
    noticePopupHtml += "<div style='padding: 5px; min-height: 150px'>"
    noticePopupHtml += noticeInfo.noticeSbst;
    //noticePopupHtml += strConv(xssChk(noticeInfo.noticeSbst));
    noticePopupHtml += "</div>";
    noticePopupHtml += "<div id='fileList'></div>";
    noticePopupHtml += "</div>";
    noticePopupHtml += "</div>";
    noticePopupHtml += "<div class='btnGrp dragon'>";
    noticePopupHtml += "<input type='checkbox' id='pop-day' name='pop-day'/><label for='pop-day'>" + getMessage("common.today.block.popup") + "</label>";
    noticePopupHtml += "<input type='button' class='btnNormal btnCancel btnCenter' value='" + getMessage("button.close") + "'>";
    noticePopupHtml += "</div>";
    noticePopupHtml += "</div>";
    $("#wrapper").append(noticePopupHtml);

    var popupX = parseFloat(document.body.clientWidth / 2) - parseFloat($("#noticeWindow").width() / 2);
    var popupY = parseFloat(document.body.clientHeight / 2) - parseFloat($("#noticeWindow").height() / 2);
    $("#noticeWindow").css({"left":popupX,"top":popupY});
    $("#noticeWindow").draggable({
        cancel: ".dragon"
    });

    $("#noticeWindow .btnCloseWin, #noticeWindow .btnCancel").bind("click",function() {
        if ($("#pop-day").is(":checked")) { 
            $.cookie(popupId, '1', { expires: 1, path : '/' });
        }
        $("#notiSbst").mCustomScrollbar('destroy');
        $("#noticeWindow").css({"min-height":"auto","min-height":"auto"}).slideUp(200);
    });

    var fileListHtml = "";
    $(result.fileList).each(function(i,item) {
        fileListHtml += "<p style='padding: 5px;font-size: 11px;'>" + item.fileName + "&nbsp;<a href='" + makeAPIUrl("/api/fileDownload/" + item.fileSeq) + "' class='fontblack'>[" + getMessage("common.string.download") + "]</a></p>";
    });

    $("#fileList").html(fileListHtml);

    if (fileListHtml != "") {
        $("#fileList").css("border-top", "1px solid #c5c5c5");
    }

    // 팝업 창 크기에 맞춰 이미지 크기 조절
    $("img").css("max-width", "100%");
    $("img").css("height", "auto");

    $("#notiSbst").mCustomScrollbar({ axis : "y", theme:"inset-3" });

    $("#noticeWindow").show();

    moveTitle();
}

var startAnimate;
var againAnimate;
function moveTitle(){
    var divWidthBefore = $("#title").find("span").width();
    $("#title").find("span").css('width','auto');

    var spanWidth = divWidthBefore - $("#title").find("span").width();
    $("#title").find("span").width(divWidthBefore);

    clearTimeout(startAnimate);
    clearTimeout(againAnimate);
    if (spanWidth < 0) {
        startAnimate = setTimeout(function(){
            $("#title").find("span").css({"text-overflow":"inherit","overflow": "visible","width": "auto","left":"0px"});
            $("#title").find("span").animate({left: spanWidth},2000,"linear");
        }, 1500);
        againAnimate = setTimeout(function(){
            $("#title").find("span").animate({left:0},0);
            $("#title").find("span").css({"text-overflow":"ellipsis","overflow": "hidden","width": "100%","left":"0px"});
            moveTitle();
        }, 5000);
    }
}

getServiceList = function() {
    callByGet("/api/service?searchType=list", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("info.nodata.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        $("#storeList").html("");
        return;
    }

    var serviceListHtml = "";
    $(data.result.serviceNmList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        $("#serviceList option:eq(0)").prop("selected", true);
    }

    getStoreList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
    $("#storeList").html("");
}

function getStoreList() {
    formData("NoneForm" , "searchType", "list");
    formData("NoneForm" , "svcSeq", $("#serviceList option:selected").val());
    callByGet("/api/store", "didReceiveStoreList", "NoneForm", "didNotReceiveStoreList");
    formDataDeleteAll("NoneForm");
}

didReceiveStoreList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            initInfo();
            msg = getMessage("info.nodata.msg");
        }
        popAlertLayer(msg);
        $("#storeList").html("");
        return;
    }

    var storeListHtml = "";
    $(data.result.storeNmList).each(function(i, item) {
        storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
    });
    $("#storeList").html(storeListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#storSeq").val() != 0) {
        $("#storeList option").not("[value='"+ $("#storSeq").val() + "']").remove();
        $("#storeList option[value='"+ $("#storSeq").val() + "']").prop("selected", true);
        $("#storeList").attr("disabled", true);
    } else {
        $("#storeList option:eq(0)").prop("selected", true);
    }

    getInfo();
}

didNotReceiveStoreList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#storeList").html("");
}

changeServiceList = function() {
    getStoreList();
}

changeStoreList = function() {
    getInfo();
}

getInfo = function() {
    getTodayVisitorInfo();
    getVisitorInfo();
    getSoldTicketInfo();
}

getTodayVisitorInfo = function() {
    formData("NoneForm" , "type", "today");
    formData("NoneForm" , "storSeq", $("#storeList option:selected").val());
    callByGet("/api/statistics", "didReceiveTodayVisitorInfo", "NoneForm", "didNotReceiveTodayVisitorInfo");
    formDataDeleteAll("NoneForm");
}

didReceiveTodayVisitorInfo = function(data) {
    if (data.resultCode != "1000" && data.resultCode != "1010") {
        popAlertLayer(getMessage("fail.common.select"));
        return;
    }
    updateVisitorStatistics(data);
}

didNotReceiveTodayVisitorInfo = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
}

getVisitorInfo = function() {
    formData("NoneForm" , "type", "visitor");
    formData("NoneForm" , "storSeq", $("#storeList option:selected").val());
    formData("NoneForm" , "dateType", "hour");
    formData("NoneForm" , "startDate", startDate);
    formData("NoneForm" , "endDate", endDate);

    callByGet("/api/statistics", "didReceiveVisitorInfo", "NoneForm", "didNotReceiveVisitorInfo");
    formDataDeleteAll("NoneForm");
}

didReceiveVisitorInfo = function(data) {
    if (data.resultCode != "1000") {
        updateVisitorGraph();
        return;
    } 
    
    $(data.result.visitorInfoList).each(function(i, item) {
		 var value = item.count;
		if (maxValue_1 < value) {
          maxValue_1 = value;
      }
  });

    var mapObj = parseGraphData(data.result.visitorInfoList);

    var chartData = getChartData(mapObj);
    updateVisitorGraph(chartData);
}

didNotReceiveVisitorInfo = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
}

getSoldTicketInfo = function() {
    formData("NoneForm" , "type", "ticket");
    formData("NoneForm" , "storSeq", $("#storeList option:selected").val());
    formData("NoneForm" , "dateType", "hour");
    formData("NoneForm" , "startDate", startDate);
    formData("NoneForm" , "endDate", endDate);

    callByGet("/api/statistics", "didReceiveSoldTicketInfo", "NoneForm", "didNotReceiveSoldTicketInfo");
    formDataDeleteAll("NoneForm");
}

didReceiveSoldTicketInfo = function(data) {
    if (data.resultCode != "1000") {
    	maxValue_2 = 1;    	
        updateSoldTicketInfo();
        return;
    }
    
    $(data.result.ticketInfoList).each(function(i, item) {
		 var value = item.count;
		if (maxValue_2 < value) {
           maxValue_2 = value;
       }
   });
    
    ticketRgData = data.result.ticketInfoList;
    var ticketRgDatalegnth = ticketRgData.length;
    if (ticketRgDatalegnth > 0) {
    	for (var i=0; i<=ticketRgDatalegnth; i++){
    		removeObjects(ticketRgData,'count',0);
    	}
    }
    var mapObj = parseGraphData(ticketRgData);
    
    updateSoldTicketStatistics(mapObj);
    if (ticketRgData.length == 0) {
    	updateSoldTicketStatistics(null);
	} else {
		updateSoldTicketStatistics(mapObj);
	}
    var chartData = getChartData(mapObj);
    updateSoldTicketGraph(chartData);
}

didNotReceiveSoldTicketInfo = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
}

parseGraphData = function(graphData) {
    var mapObj = {};
    if (!graphData) {
        return mapObj;
    }
    var colorIndex = 0;
    $(graphData).each(function(i, item) {
        var key = item.name;
        if (mapObj[key] == undefined) {
            var value = {
                backgroundColor : getBackgroundColor(colorIndex),
                data : {},
                totalCount : 0
            };
            colorIndex++;

            for (var i = 0; i < dataLables.length; i++) {
                var dataLable = dataLables[i];
                value.data[dataLable] = 0;
            }
            mapObj[key] = value;
        }

        var dateTime = item.data;
        if (dateTime) {
            var hour = parseInt(dateTime.toString().substr(8,2)) || 0;
            if (hour < 10) {
                hour = "0" + hour;
            }
            hour += getMessage("common.hour");

            var obj = mapObj[key];
            if (obj.data[hour] != undefined) {
                obj.data[hour] = item.count;
                obj.totalCount += item.count;
            }
        }
    });

    return mapObj;
}

getChartData = function(mapObj) {
    var chartData = new Array();
    if (!Object.keys(mapObj).length) {
        return chartData;
    }

    Object.keys(mapObj).forEach(function(key) {
        var dataObj = mapObj[key];
        var dataArr = new Array();
        Object.keys(dataObj.data).forEach(function(dataKey) {
            dataArr.push(dataObj.data[dataKey]);
        });

        chartData.push({
            type: 'line',
            label: key,
            fill:false,
            backgroundColor: dataObj.backgroundColor,
            data: dataArr,
        });
    })
    return chartData;
}

updateVisitorStatistics = function(data) {
    var todayHtml = "";
    if (!data) {
        todayHtml = "<tr><th>" + getMessage("common.nodata.msg") + "</th></tr>";
        $(".statsToday > .statsTicketTable").html(todayHtml);
        return;
    }
    var enteringVisitorCount = 0;
    var leftVisitorCount = 0;
    var totalCount = 0;

    if (data.resultCode == "1000") {
        $(data.result.todayInfoList).each(function(i, item) {
            if (item.title == 'Y') {
                enteringVisitorCount += item.count;
            } else {
                leftVisitorCount += item.count;
            }
            totalCount += item.count;
        });
    }

    todayHtml += "<tr><th width='50%'>" + getMessage("dashboard.store.totalVisit") + "</th><td width='50%'>" + totalCount + "</td></tr>";
    todayHtml += "<tr><th width='50%'>" + getMessage("dashboard.store.leave") + "</th><td width='50%'>" + leftVisitorCount + "</td></tr>";
    todayHtml += "<tr><th width='50%'>" + getMessage("dashboard.store.entering") + "</th><td width='50%'>" + enteringVisitorCount + "</td></tr>";

    $(".statsToday > .statsTicketTable").html(todayHtml);
}

updateVisitorGraph = function(chartData) {
    if (!chartData) {
        chartData = new Array();
        maxValue_1 = 1;
    }
    var titleName = getMessage("stats.visitor.graph");
    if (!visitChart) {
        var options = {
            type: 'line',
            data: {
                labels: dataLables,
                datasets: chartData
            },
            options: {
                tooltips: {
                    intersect: false,
                },
                title: {
                    text:titleName,
                    display: true,
                    fontSize: 18
                },
                legend: {
                    position:'bottom'
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                fontFamily: "Montserrat",
                                max : getYaxedMaxValue(maxValue_1),
                                beginAtZero: true,
                                fixedStepSize: fixedStepSizeE(maxValue_1)
                            }
                    }]
                }
            }
        };

        var ctx = document.getElementById("container").getContext("2d");;
        visitChart = new Chart(ctx, options);
    } else {
        visitChart.data.datasets = chartData;
        visitChart.update();
    }
}

updateSoldTicketInfo = function(data) {
    if (!data) {
        updateSoldTicketStatistics();
        updateSoldTicketGraph();
        return;
    }

    var mapObj = parseGraphData(data.result.productUseList);
    updateSoldTicketStatistics(mapObj);

    var chartData = getChartData(mapObj);
    updateSoldTicketGraph(chartData);
}

updateSoldTicketStatistics = function(graphDataMap) {
    var tagStatisticsHtml = "";
    if (!graphDataMap) {
        tagStatisticsHtml = "<tr><th>" + getMessage("common.nodata.msg") + "</th></tr>";
    } else {
        Object.keys(graphDataMap).forEach(function(key) {
            var dataObj = graphDataMap[key];
            tagStatisticsHtml += "<tr><th width='50%'>" + key + "</th><td width='50%'>" + dataObj.totalCount + "</td></tr>";
        });
    }
    $(".statsTicket > .statsTicketTable").html(tagStatisticsHtml);
}

updateSoldTicketGraph = function(chartData) {
    if (!chartData) {
        chartData = new Array();
        maxValue_2 = 1;
    }

    var titleName = getMessage("stats.soldTicket.graph");
    if (!tagChart) {
        var options = {
            type: 'line',
            data: {
                labels: dataLables,
                datasets: chartData
            },
            options: {
                tooltips: {
                    intersect: false,
                },
                title: {
                    text:titleName,
                    display: true,
                    fontSize: 18
                },
                legend: {
                    position:'bottom'
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                fontFamily: "Montserrat",
                                max : getYaxedMaxValue(maxValue_2),
                                beginAtZero: true,
                                fixedStepSize: fixedStepSizeE(maxValue_2)
                            }
                    }]
                }
            }
        };

        var ctx = document.getElementById("container2").getContext("2d");;
        tagChart = new Chart(ctx, options);
    } else {
        tagChart.data.datasets = chartData;
        tagChart.update();
    }
}

initInfo = function() {
    updateVisitorStatistics();
    updateVisitorGraph();

    updateSoldTicketInfo();
}

getNotice = function() {
    callByGet("/api/notice?popupView=store","didReceiveNotice","NoneForm");
}

didReceiveNotice = function(data) {
    if (data.resultCode == "1000") {
        showNoticePopup(data.result);
    }
}

getBackgroundColor = function(colorIndex) {
    if (colorIndex >= 0 && colorIndex < graphColors.length) {
        return graphColors[colorIndex];
    }

    var randomR = Math.floor((Math.random() * 130) + 100);
    var randomG = Math.floor((Math.random() * 130) + 100);
    var randomB = Math.floor((Math.random() * 130) + 100);

    var graphBackground = "rgb("
            + randomR + ", "
            + randomG + ", "
            + randomB + ")";
    return graphBackground;
}

/* 배열 내 값 검색 삭제*/
function removeObjects(arry, property, num) {
    for (var i in arry) {
        if (arry[i][property] == num)
            arry.splice(i, 1);
    }
}


getYaxedMaxValue = function(max) {
    max = parseFloat(max);
    var key = [30,100,200,300,500,1000];
    var value =[5,10,20,30,50,100];
    var answer;
    for(var i=0;i<key.length;i++) {
        if(key[i] >= max) {
            answer = key[i];
            return answer;
        }
        
    }
    if(max > 1000) {
        answer = Math.ceil(parseFloat(max) * 1.1);
        return answer;
    }

}

fixedStepSizeE = function(max) {
    max = parseFloat(max);
    var key = [30,100,200,300,500,1000];
    var value =[5,10,20,30,50,100];
    var answer;
    for(var i=0;i<key.length;i++) {
        if(key[i] >= max) {
            answer = value[i];
            return answer;
        }
        
    }
    if(max > 1000) {
        answer = 200;
        return answer;
    }
}