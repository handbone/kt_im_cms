/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var categoryBar;
var categoryChartData;

var chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(98, 139, 211)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

$(document).ready(function() {

    $("#btnUsedContents").click(function() {
        pageMove("/statistics/contents/used");
    });

    $(".conTitle.tabOff").click(function() {
        pageMove("/dashboard/contents/verify");
    });

    $("#serviceList").change(function() {
        changeServiceList();
    });

    $("#storeList").change(function() {
        updateContents();
    });

    $("#mostContentsContainer").css("min-height", 290);

    getServiceList();
});

initCategoryChartData = function(update) {
    categoryChartData = {
        labels: [],
        datasets: [{
            label: getMessage("select.all"),
            backgroundColor: chartColors.blue,
            borderColor: chartColors.blue,
            borderWidth: 1,
            data: []
        }]
    };
}

getServiceList = function() {
    callByGet("/api/service?searchType=list", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        return;
    }

    var serviceListHtml = "";
    $(data.result.serviceNmList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#svcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#svcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#svcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        $("#serviceList option:eq(0)").prop("selected", true);
    }

    getStoreList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
}

getStoreList = function() {
    formData("NoneForm" , "searchType", "list");
    formData("NoneForm" , "svcSeq", $("#serviceList option:selected").val());
    callByGet("/api/store", "didReceiveStoreList", "NoneForm", "didNotReceiveStoreList");
    formDataDeleteAll("NoneForm");
}

didReceiveStoreList = function(data) {
    if (data.resultCode != "1000" && data.resultCode != "1010") {
        popAlertLayer(getMessage("fail.common.select"));
        $("#storeList").html("");
        return;
    }

    if (data.resultCode == "1010") {
        popAlertLayer(getMessage("common.nostore.msg"));
    }

    var storeListHtml = "<option value='0'>" + getMessage("select.all") + "</option>";
    if (data.result) {
        $(data.result.storeNmList).each(function(i, item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
    }
    $("#storeList").html(storeListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#storSeq").val() != 0) {
        $("#storeList option").not("[value='"+ $("#storSeq").val() + "']").remove();
        $("#storeList option[value='"+ $("#storSeq").val() + "']").prop("selected", true);
        $("#storeList").attr("disabled", true);
    } else {
        $("#storeList option:eq(0)").prop("selected", true);
    }

    updateContents();
}

didNotReceiveStoreList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#storeList").html("");
}

updateContents = function() {
    getContentsByCategory();
    getMostPopularContents();
}

changeServiceList = function() {
    getStoreList();
}

getContentsByCategory = function() {
    initCategoryChartData();

    formData("NoneForm" , "type", "contentsByCategory");
    formData("NoneForm" , "svcSeq", $("#serviceList option:selected").val());
    callByGet("/api/statistics", "didReceiveContentsByCategoryInfo", "NoneForm", "didNotReceiveContentsByCategoryInfo");
    formDataDeleteAll("NoneForm");
}

didReceiveContentsByCategoryInfo = function(data) {
    if (data.result) {
        $(data.result.contentsByCategoryList).each(function(i, item) {
            categoryChartData.labels.push(item.ctgNm);
            categoryChartData.datasets[0].data.push(item.count);
        });
        drawCategoryChart();
    } else {
        setNoDataText("categoryContainer");
    }
}

didNotReceiveContentsByCategoryInfo = function(data) {
    setNoDataText("categoryContainer");
}

getMostPopularContents = function() {
    formData("NoneForm" , "type", "mostPopularContents");
    formData("NoneForm" , "svcSeq", $("#serviceList option:selected").val());
    formData("NoneForm" , "storSeq", $("#storeList option:selected").val());
    callByGet("/api/statistics", "didReceiveMostPopularContentsInfo", "NoneForm", "didNotReceiveMostPopularContentsInfo");
    formDataDeleteAll("NoneForm");
}

didReceiveMostPopularContentsInfo = function(data) {
    if (data.result) {
        var maxCount = 0;
        $('#mostContentsContainer').html("");
        $(data.result.mostPopularContentsList).each(function(i, item) {
            $('#mostContentsContainer').append("<div class='barChartBox'><div class='barContainer'><div class='barPercentBox'></div></div></div>");
            if (maxCount == 0) {
                maxCount = item.contsTotalPlayCount;
            }
            var totalTime = getTimeString(item.contsTotalPlayTime);
            var avgTime = getMinutePercent(item.contsAvgPlayTime);
            var perPxHeight = (((item.contsTotalPlayCount / maxCount) * 100)/100) * 151;

            $(".barPercentBox").eq(i).animate({opacity: 1, height: ((item.contsTotalPlayCount / maxCount) * 100) + "%"}, 1000, function() {}).append("<p class='barUseStatTxt'>" + item.contsTotalPlayCount + "회<br> (" + totalTime + ")</p>");
            if (perPxHeight <= 30 || isNaN(perPxHeight)) {
                $(".barPercentBox").eq(i).find(".barUseStatTxt").css("top",perPxHeight - 30);
            }
            $(".barChartBox").eq(i).append("<p class='barUseStatBottmTxt'>" + item.contsTitle + "<br />(평균 " + avgTime + "분)</p>");
        });
    } else {
        setNoDataText("mostContentsContainer");
    }
}

didNotReceiveMostPopularContentsInfo = function(data) {
    setNoDataText("mostContentsContainer");
}

drawCategoryChart = function(chartData) {
        var titleName = getMessage("dashboard.contents.regist.category");
        var options = {
            type: 'horizontalBar',
            data: categoryChartData,
            options: {
                responsive: true,
                maintainAspectRatio :false,
                tooltips : {
                    enabled : true
                },
                title : {
                    text : titleName,
                    display : true,
                    fontSize : 16,
                    padding : 25
                },
                legend : {
                    display : false
                },
                scales : {
                    yAxes : [{
                        gridLines : {
                            display :true,
                            lineWidth : 1,
                            color : "rgba(0,0,0,0.80)"
                        }
                    }],
                    xAxes : [{
                        ticks: {
                            beginAtZero: true,
                            fixedStepSize: 1
                        }
                    }]
                }
            }
        };

        $('#categoryContainer').html('<canvas id="canvasCategory"></canvas>');
        var ctx = document.getElementById("canvasCategory").getContext("2d");
        if (categoryBar) {
            delete categoryBar;
        }
        categoryBar = new Chart(ctx, options);
}

setNoDataText = function(id) {
    $("#" + id).html("<span>" + getMessage("info.nodata.msg") + "</span>");
}

getTimeString = function(dataString) {
    var timeString = "0s";
    if (!dataString) {
        return timeString;
    }

    var time = parseInt(dataString);
    if (time >= 3600) {
        timeString = parseInt(time / 60 / 60) + "h" + parseInt((time / 60) % 60) + "m" + parseInt(time % 60) + "s";
    } else if (3600 > time && time >= 60) {
        timeString = parseInt((time / 60) % 60) + "m" + parseInt(time % 60) + "s";
    } else if (60 > time) {
        timeString = time + "s";
    }
    return timeString;
}

getMinutePercent = function(dataString) {
    var timeString = "0";
    if (!dataString) {
        return timeString;
    }

    var time = parseInt(dataString);
    return (time / 60).toFixed(2) ;
}