/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var arr = new Array();
var cprtSvcSeq = 0;
$(document).ready(function(){
    cprtSvcInfo();
});

cprtSvcInfo = function(){
    callByGet("/api/cprtMng?cprtSvcSeq="+$("#cprtSvcSeq").val(), "cprtSvcInfoSuccess", "NoneForm", "cprtSvcInfoFail");
}

cprtSvcInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.banrCprtInfo;
        cprtSvcSeq = item.cprtSvcSeq;
        $("#svcNm").text(item.svcNm);
        $("#cprtSvcNm").text(item.cprtSvcNm);

        $("#svcSeq").val(item.svcSeq);

        $("#menuUrl").text(item.menuUrl);
        var preView = "";
        if (!empty(item.cprtImgList)) {
            $("#fileNm").text(item.cprtImgList[0].imgNm);
            preView = "<a href=\""+makeAPIUrl(item.cprtImgList[0].imgPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(item.cprtImgList[0].imgPath,"img")+"' title=\""+item.cprtImgList[0].fileSeq+"\"></a>";
        }

        if (!empty(item.cprtThumbImg)) {
            $("#fileThumNm").text(item.cprtThumbImg[0].imgNm);
            preView += "<a href=\""+makeAPIUrl(item.cprtThumbImg[0].imgPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(item.cprtThumbImg[0].imgPath,"img")+"' title=\""+item.cprtThumbImg[0].fileSeq+"\"></a>";
        }
        $("#imgPreview").html(preView);
        $("#imgPreview").justifiedGallery({
            rowHeight : 220,
            lastRow : 'nojustify',
            margins : 3
        });


        $(".swipebox").swipebox();




        $("input[type=radio][name=perdYn][value="+item.perdYn+"]").attr("checked", true);
        $("#stDt").val(item.stDt);
        $("#fnsDt").val(item.fnsDt);



        $("input[type=radio][name=useYn][value="+item.useYn+"]").attr("checked", true);



        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/cprtSvc");
    }
}

cprtSvcInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/cprtSvc");
}
deleteCprtSvc = function(){
    var delSeqs = cprtSvcSeq;
    var delNmList = $("#cprtSvcNm").text();

    popConfirmLayer(getMessage("common.delete.msg"), function() {
        formData("NoneForm" , "delSeqs", delSeqs);
        callByDelete("/api/cprtMng", "deleteCprtSvcSuccess", "NoneForm","deleteFail");
    }, null, getMessage("common.confirm"));

}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteCprtSvcSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"),"/onlineService/cprtSvc#page=1"+"&perdSttus="+"&svcSeq="+$("#svcSeq").val());
    }
}

