/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hashSvc;
var now=new Date();
$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
    }
    setOlSvcList();
    limitInputTitle("cprtSvcNm");

    $("#stDt").val(dateString((new Date(now.getTime()))));
    $("#fnsDt").val(dateString((new Date(now.getTime()))));

    $("#stDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#fnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#fnsDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#stDt").datepicker("option","maxDate", selected)
        }
    });

    setkeyup();

    $("input[name=perdYn]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "Y") {
            $("input[name=perdDt]").attr("disabled",false).css("background-color","#ffffff");
        } else {
            $("input[name=perdDt]").attr("disabled",true).css("background-color","#f5f5f5");
        }
    });



    fileFormInsert("cprtImg","CPRT_IMG",-1,-1,"cprtImg","imgPreview","png,jpg,bmp,jpeg");
    fileFormInsert("cprtThumImg","CPRT_IMG_T",-1,-1,"cprtThumImg","imgThumPreview","png,jpg,gif,bmp,jpeg");
    fileFormAllView();
});

/* 초기 서비스 영역 SET*/
setOlSvcList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON", "setOlSvcListSuccess", '', "failOlSvcList");
}
/* 초기 서비스 영역 DATA SUCESS */
setOlSvcListSuccess = function(data){
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#svcList").html("");
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + parseInt(item.comnCdValue) + "\">" + item.comnCdNm + "</option>";
    });
    $("#svcList").html(serviceListHtml);
    if ($("#memberSec").val() == "04" ) {
        // 05 : 매장 관리자, 서비스 변경 불가
        $("#svcList").attr('disabled', 'true');
        $("#svcList").val($("#svcSeq").val());
    } else if (hashSvc != null && hashSvc != "") {
        $("#svcList").val(hashSvc);
    }

}
/* 초기 서비스 영역 DATA FAIL */
failOlSvcList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#svcList").html("");
}

function makePageMove(e){
    var mvPage = "/onlineService/cprtSvc#page=1";
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");

        mvPage += "&svcSeq=" + checkUndefined(findGetParameter(str_hash,"svcSeq"));
    }
    pageMove(mvPage);
}



function cprtSvcRegist() {
    if ($("#svcList").val() == 0) {
        popAlertLayer(getMessage("common.noservice.msg"));
    }

    if ($("#cprtSvcNm").val().trim() == "") {
        popAlertLayer(getMessage("banr.use.campNm.empty.msg"));
        $("#cprtSvcNm").focus();
        return;
    }

    if ($("#menuUrl").val().trim() == "") {
        popAlertLayer(getMessage("banr.use.menuURl.empty.msg"));
        $("#menuUrl").focus();
        return;
    }

    formData("comnty_write_from", "cprtSvcNm", $("#cprtSvcNm").val().trim());
    formData("comnty_write_from", "svcSeq", $("#svcList").val());
    formData("comnty_write_from", "perdYn", $("input[name=perdYn]:checked").val());
    formData("comnty_write_from", "stDt", $("#stDt").val());
    formData("comnty_write_from", "fnsDt", $("#fnsDt").val());
    formData("comnty_write_from", "menuUrl", $("#menuUrl").val());
    formData("comnty_write_from", "useYn", $("input[name=useYn]:checked").val());

    for(var i =0; i < fileSet.length; i++){
        var setVar = fileSet[i];

        if (setVar.fileSe == "CPRT_IMG") {
            $("#cprtImg input[data-value="+i+"]").attr("name","cprtImg");
        } else {
            $("input[data-value="+i+"]").attr("name","cprtThumImg");
        }
    }


    callByMultipart("/api/cprtMng", "cprtSvcRegistSuccess", "comnty_write_from", "insertFail");

//    popConfirmLayer(getMessage("common.regist.msg"), function() {
//
//    }, null, getMessage("common.confirm"));
}

insertFail = function(data){
    if (!fileAccss && typeof data.resultCode == "undefined") {
        popAlertLayer(getMessage("common.check.file.Position"));
    } else if (data.resultCode == "1500") {
        popAlertLayer(getMessage("info.upload.failed.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();

}

cprtSvcRegistSuccess = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"),"/onlineService/cprtSvc#page=1"+"&perdSttus="+"&svcSeq="+$("#svcList").val());
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
        $("#comnty_write_from > input[type=hidden]").remove();
    }

}

