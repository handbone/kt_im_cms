/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var now=new Date();
var cprtSvcSeq = 0;
var svcSeq = 0;
var delFileSeqs = 0;
var cprtImgSeq = 0;
var cprtThumImgSeq = 0;
$(document).ready(function() {
    limitInputTitle("cprtSvcNm");

    $("#stDt").val(dateString((new Date(now.getTime()))));
    $("#fnsDt").val(dateString((new Date(now.getTime()))));

    $("#stDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#fnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#fnsDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#stDt").datepicker("option","maxDate", selected)
        }
    });

    setkeyup();

    $("input[name=perdYn]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "Y") {
            $("input[name=perdDt]").attr("disabled",false).css("background-color","#ffffff");
        } else {
            $("input[name=perdDt]").attr("disabled",true).css("background-color","#f5f5f5");
        }
    });


    fileFormInsert("cprtImg","CPRT_IMG",-1,-1,"cprtImg","imgPreview","png,jpg,bmp,jpeg");
    fileFormInsert("cprtThumImg","CPRT_IMG_T",-1,-1,"cprtThumImg","imgThumPreview","png,jpg,gif,bmp,jpeg");
    fileFormAllView();

    cprtSvcInfo();
});

cprtSvcInfo = function(){
    callByGet("/api/cprtMng?cprtSvcSeq="+$("#cprtSvcSeq").val(), "cprtSvcInfoSuccess", "NoneForm", "cprtSvcInfoFail");
}

cprtSvcInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.banrCprtInfo;
        cprtSvcSeq = item.cprtSvcSeq;
        svcSeq = item.svcSeq;
        $("#svcNm").text(item.svcNm);
        $("#cprtSvcNm").text(item.cprtSvcNm);

        if (!empty(item.cprtImgList)) {
            cprtImgSeq = item.cprtImgList[0].fileSeq;
            $("#imgPreview").html("<img src='"+makeAPIUrl(item.cprtImgList[0].imgPath,"img")+"' title=\""+item.cprtImgList[0].fileSeq+"\">");
        }

        if (!empty(item.cprtThumbImg)) {
            cprtThumImgSeq = item.cprtThumbImg[0].fileSeq;
            $("#imgThumPreview").html("<img src='"+makeAPIUrl(item.cprtThumbImg[0].imgPath,"img")+"' title=\""+item.cprtThumbImg[0].fileSeq+"\">");
        }


        $("input[type=radio][name=perdYn][value="+item.perdYn+"]").attr("checked", true).css("background-color","#f5f5f5");
        if (item.perdYn == "Y") {
            $("input[name=perdDt]").attr("disabled",false).css("background-color","#ffffff");
        }
        $("#stDt").val(item.stDt);
        $("#fnsDt").val(item.fnsDt);
        $("#menuUrl").val(item.menuUrl);
        $("input[type=radio][name=useYn][value="+item.useYn+"]").attr("checked", true);

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/cprtSvc");
    }
}

cprtSvcInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/cprtSvc");
}


function makePageMove(e){
    var mvPage = "/onlineService/cprtSvc#page=1&svcSeq="+svcSeq;
    pageMove(mvPage);
}



function cprtSvcEdit() {
    if ($("#menuUrl").val().trim() == "") {
        popAlertLayer(getMessage("banr.use.menuURl.empty.msg"));
        $("#menuUrl").focus();
        return;
    }


    delFileSeqs = "";
    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("comnty_write_from", "cprtSvcSeq", cprtSvcSeq);
        formData("comnty_write_from", "perdYn", $("input[name=perdYn]:checked").val());
        formData("comnty_write_from", "stDt", $("#stDt").val());
        formData("comnty_write_from", "fnsDt", $("#fnsDt").val());
        formData("comnty_write_from", "useYn", $("input[name=useYn]:checked").val());
        formData("comnty_write_from", "menuUrl", $("#menuUrl").val());

        for(var i =0; i < fileSet.length; i++){
            var setVar = fileSet[i];
            if (setVar.fileSe == "CPRT_IMG" && cprtImgSeq != 0 && setVar.chgCnt > 0) {
                delFileSeqs += cprtImgSeq;
            } else if (setVar.fileSe == "CPRT_IMG_T" && cprtImgSeq != 0 && setVar.chgCnt > 0){
                if (delFileSeqs != "") {
                    delFileSeqs += ",";
                }
                delFileSeqs += cprtThumImgSeq;
            }
        }
        if (delFileSeqs != "") {
            formData("comnty_write_from", "delFileSeqs", delFileSeqs);
        }

        for(var i =0; i < fileSet.length; i++){
            var setVar = fileSet[i];

            if (setVar.fileSe == "CPRT_IMG") {
                $("#cprtImg input[data-value="+i+"]").attr("name","cprtImg");
            } else {
                $("input[data-value="+i+"]").attr("name","cprtThumImg");
            }
        }

        formData("comnty_write_from", "_method", "PUT");
        callByMultipart("/api/cprtMng", "cprtSvcUpdateSuccess", "comnty_write_from", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data){
    if (data.resultCode == "1500") {
        popAlertLayer(getMessage("info.upload.failed.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();

}

cprtSvcUpdateSuccess = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"),"/onlineService/cprtSvc#page=1"+"&perdSttus="+"&svcSeq="+svcSeq);
    } else {
        popAlertLayer(getMessage("fail.common.update"));
        $("#comnty_write_from > input[type=hidden]").remove();
    }

}




