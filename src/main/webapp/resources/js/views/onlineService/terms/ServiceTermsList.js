/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var readyPage = false;
var gridState;
var tempState;
var searchField;
var searchString;
var hashSvc;
var hashStplt;
var apiUrl;
var totalPage = 0;
var resultReset = false;

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            if(searchField != null) {
                searchField = searchField;
            } else {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            }
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashStplt = checkUndefined(findGetParameter(str_hash,"comnCdValue"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            serviceList();
        } else if (gridState == "GRIDCOMPLETE") {
            var str_hash = document.location.hash.replace("#","");
            var searchStr = checkUndefined(findGetParameter(str_hash,"searchString"));
            if (checkUndefined(findGetParameter(str_hash,"searchString")) == null) {
                searchStr = "";
            }
            if (!resultReset) {
                if (searchStr != $("#keyword").val()) {
                    $("#keyword").val(checkUndefined(findGetParameter(str_hash,"searchString")));
                    keywordSearch();
                }
            }
            resultReset = false;
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    serviceList();

    $("#keyword").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

var noServiceData = false;
serviceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON","svcListSuccess","NoneForm");
}

svcListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var svcListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            svcListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#svcSelbox").html(svcListHtml);

        if (document.location.hash != "") {
            var str_hash = document.location.hash.replace("#","");
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            $("#svcSelbox").val(hashSvc);
        }
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").val($("#svcSeq").val());
            $("#svcSelbox").attr('disabled', true);
        }
        svcTermsSelList($("#svcSelbox").val(), false);
    } else {
        noServiceData = true;
        svcTermsSelList(0, false);
    }
}

svcTermsReload = function() {
    nosvcTermsData = false;
    svcTermsSelList($("#svcSelbox").val(), true);
}

var nosvcTermsData = false;
svcTermsSelList = function(searchSvc, isReload){
    if (searchSvc != null && searchSvc != 0) {
        searchSvc = "&svcSeq=" + searchSvc;
    } else {
        searchSvc = "&svcSeq=-1";
    }

    if (isReload) {
        callByGet("/api/codeInfo?comnCdCtg=SVC_TERMS", "reloadsvcTermsListSuccess", "NoneForm");
    } else {
        callByGet("/api/codeInfo?comnCdCtg=SVC_TERMS", "svcTermsListSuccess", "NoneForm");
    }
}

svcTermsListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var svcTermsInfoListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            svcTermsInfoListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#stpltSelbox").html(svcTermsInfoListHtml);
        $("#stpltSelbox").attr("disabled", false);

        if (document.location.hash != "") {
            var str_hash = document.location.hash.replace("#","");
            hashStplt = checkUndefined(findGetParameter(str_hash,"comnCdValue"));
            $("#stpltSelbox").val(hashStplt);
        }
    } else {
        $("#stpltSelbox").html("");
        $("#stpltSelbox").attr("disabled", true);
        nosvcTermsData = true;
    }

    var searchStplt = $("#stpltSelbox").val();
    if (searchStplt != null && searchStplt != 0) { // 선택된 약관 값으로 reload
        apiUrl = makeAPIUrl("/api/serviceTerms?comnCdValue=" + searchStplt);
    } else {
        apiUrl = makeAPIUrl("/api/serviceTerms?comnCdValue=-1");
    }

    if (!readyPage) {
        readyPage = true;
        svcTermsList();
    } else {
        jQuery("#jqgridData").setGridParam({"url": apiUrl});
        jQuery("#jqgridData").trigger("reloadGrid");
    }
}

reloadsvcTermsListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var svcTermsInfoListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            svcTermsInfoListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#stpltSelbox").html(svcTermsInfoListHtml);
        $("#stpltSelbox").attr("disabled", false);
    } else {
        $("#stpltSelbox").html("");
        $("#stpltSelbox").attr("disabled", true);
        nosvcTermsData = true;
    }
    jqGridReload();
}

jqGridReload = function() {
    usingSvcTerms = "";
    comingSvcTerms = "";
    searchField = null;
    var searchStplt = $("#stpltSelbox").val();
    if (searchStplt != null && searchStplt != 0) {
        searchStplt = "?comnCdValue=" + searchStplt;
    } else {
        searchStplt = "?comnCdValue=-1";
    }

    jQuery("#jqgridData").setGridParam({"url": makeAPIUrl("/api/serviceTerms" + searchStplt), page: 1});
    jQuery("#jqgridData").trigger("reloadGrid");
}

var usingSvcTerms = "";
var comingSvcTerms = "";
var date = new Date().format("yyyy-MM-dd");
svcTermsList = function(){
    var colNameList = [
        getMessage("table.num"),
        getMessage("svcterms.table.version"),
        getMessage("svcterms.table.register"),
        getMessage("svcterms.table.mandYn"),
        getMessage("svcterms.table.useYn"),
        getMessage("svcterms.table.amdDt"),
        "stpltSeq",
        "stDt",
        "fnsDt",
        "perdYn"
    ];

    $("#jqgridData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.svcTermsInfoList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:colNameList,
        colModel:[
            {name:"num", index: "num", align:"center", width:40},
            {name:"stpltVer", index: "STPLT_VER", align:"center", width:300, formatter:pointercursor},
            {name:"cretrNm", index:"CRETR_NM", align:"center"},
            {name:"mandYn", index:"MAND_YN", align:"center"},
            {name:"useYn", index:"USE_YN", align:"center"},
            {name:"regDt", index: "REG_DT", align:"center", width:200},
            {name:"stpltSeq", index:"STPLT_SEQ", hidden:true},
            {name:"stDt", index:"ST_DT", hidden:true},
            {name:"fnsDt", index:"FNS_DT", hidden:true},
            {name:"perdYn", index:"PERD_YN", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "STPLT_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: true,
        beforeRequest:function() {
            tempState = gridState;
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            if (searchField != null) {
                searchField = searchField;
            } else {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            }
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());
            hashSvc = checkUndefined($("#svcSelbox").val());
            hashStplt = checkUndefined($("#stpltSelbox").val());

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if (tempState == "SEARCH") {
                    myPostData.page = 1;
                } else {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }
            if (searchField != null && searchField != "" && searchString != "" && !resultReset) {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                if (resultReset) {
                    $("#target").val("");
                    $("#keyword").val("");
                    searchField = null;
                    searchString = "";
                }
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            if (hashSvc != null && hashSvc != "") {
                myPostData.svcSeq = hashSvc;
            } else {
                myPostData.svcSeq = "";
            }

            if (hashStplt != null && hashStplt != "") {
                myPostData.comnCdValue = hashStplt;
            } else {
                myPostData.comnCdValue = "";
            }

            if (noServiceData) {
                hashSvc = "";
                hashStplt = "";
                popAlertLayer(getMessage("info.service.nodata.msg"));
            } else if (nosvcTermsData) {
                hashStplt = "";
                popAlertLayer(getMessage("info.svcterms.nodata.msg"));
            }

            if(resultReset){
                myPostData.page = 1;
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData, page: myPostData.page });
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            // session
            if(sessionStorage.getItem("last-url") != null){
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            // Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /*뒤로가기*/
            var hashlocation = "page=" + $(this).context.p.page + "&svcSeq=" + hashSvc + "&comnCdValue=" + hashStplt;
            if($(this).context.p.postData._search && $("#target").val() != null){
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (!resultReset) {
                if (tempState != "" || str_hash == hashlocation) {
                    gridState = "READY";
                }
            }

            var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
            var chgUseNm = "";
            for (var i=0; i<colModel.length; i++) {
                var colYn = $("#jqgridData").jqGrid('getRowData', i).useYn;
                var colNum = $("#jqgridData").jqGrid('getRowData', i).num;
                if (colYn == 'Y') {
                    var colDataStDt = $("#jqgridData").jqGrid('getRowData', i).stDt;
                    var colDataFnsDt = $("#jqgridData").jqGrid('getRowData', i).fnsDt;
                    var colDataPerd = $("#jqgridData").jqGrid('getRowData', i).perdYn;
                    if (date < colDataStDt && colDataPerd != "N") {
                        if ($("#jqgridData").jqGrid('getRowData', i).useYn == "Y") {
                            comingSvcTerms += colNum + ",";
                            chgUseNm += i + ",";
                        }
                    } else if (date > colDataFnsDt && colDataPerd != "N") {
                        if ($("#jqgridData").jqGrid('getRowData', i).useYn == "Y") {
                            chgUseNm += i + ",";
                        }
                    } else {
                        usingSvcTerms = colNum;
                    }
                }
            }
            chgUseNm = chgUseNm.substr(0,chgUseNm.length-1);
            chgUseNm = chgUseNm.split(",");
            for (var j = 0; j < chgUseNm.length; j++) {
                jQuery("#jqgridData").setCell(chgUseNm[j], "useYn", 'N');
            }

            //pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
            });

            //포인터
            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event){
            // 제목 셀 클릭시
            if (columnId == 2) {
                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/onlineService/terms/" + list.stpltSeq);
            }
        }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

// 클릭 기능

function svcTermsListDelete(){
    var stpltSeqList = "";
    var stpltNmList = "";
    var useYnList = "";
    var numList = "";
    var delCnt = 0;

    for (var i = 1; i < $("#limit").val() + 1; i++) {
        if ($("#jqg_jqgridData_" + i).prop("checked")) {
            var selStpltSeq = $("#jqgridData").jqGrid('getRowData', i).stpltSeq;
            var selStpltNm = $("#jqgridData").jqGrid('getRowData', i).stpltVer;
            var selUseYn = $("#jqgridData").jqGrid('getRowData', i).useYn;
            var selStdt = $("#jqgridData").jqGrid('getRowData', i).stDt;
            var selNum = $("#jqgridData").jqGrid('getRowData', i).num;
            stpltSeqList += selStpltSeq + ",";
            stpltNmList += selStpltNm + ", ";
            useYnList += selUseYn + ",";
            numList += selNum + ",";
            delCnt++;
        }
    }

    if (delCnt == 0) {
        popAlertLayer(getMessage("svcterms.delete.not.select.msg"));
        return;
    }

    if (useYnList.indexOf("Y") != -1) {
        popAlertLayer(getMessage("svcterms.delete.not.y.msg"));
        return;
    }

    var delList = numList.slice(0, -1).split(",");
    var comingList = comingSvcTerms.slice(0, -1).split(",");
    if (usingSvcTerms == "" && comingSvcTerms != "") {
        if (delList.length >= comingList.length) {
            for (var i = 0; i < delList.length; i++) {
                for (var j = 0; j < delList.length; j++) {
                    if (delList[i] == comingList[j]) {
                        popAlertLayer(getMessage("svcterms.delete.not.y.msg"));
                        return;
                    }
                }
            }
        } else {
            for (var i = 0; i < comingList.length; i++) {
                for (var j = 0; j < comingList.length; j++) {
                    if (comingList[i] == delList[j]) {
                        popAlertLayer(getMessage("svcterms.delete.not.y.msg"));
                        return;
                    }
                }
            }
        }
    }

    popConfirmLayer(stpltNmList.slice(0, -2) + getMessage("svcterms.delete.confirm.msg"), function() {
        formData("NoneForm" , "stpltSeqList", stpltSeqList.slice(0, -1));
        callByDelete("/api/serviceTerms", "serviceTermsListDeleteSuccess", "NoneForm","deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

serviceTermsListDeleteSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), function(){location.reload();});
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

function keywordSearch() {
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

searchFieldSet =function() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');
        var searchHtml = "";
        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name){
            case "stpltVer":
                break;
            case "cretrNm":
                break;
            default:
                continue;
            }
            searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

function moveToRegistView() {
    var mvPage = "";

    if ($("#svcSelbox").val() == null) {
        popAlertLayer(getMessage("info.service.nodata.msg"));
        return;
    }

    mvPage = "?svcSeq=" + $("#svcSelbox").val() + "&stpltType=" + $("#stpltSelbox").val();

    pageMove("/onlineService/terms/regist" + mvPage);
}

function pageReset() {
    resultReset = true;
}