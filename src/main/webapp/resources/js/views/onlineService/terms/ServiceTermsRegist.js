/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var changeStpltSeq = "";
var isExistSvcTerms = false;
var stopStpltYn = "";

$(document).ready(function(){
    serviceList();

    var now=new Date();
    $("#startDt").val(dateString((new Date(now.getTime()))));
    $("#endDt").val(dateString((new Date(now.getTime()))));

    $("#startDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#endDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#endDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#startDt").datepicker("option","maxDate", selected);
        }
    });

    $("#regNm").html($("#name").val());

    limitInputTitle("svcTermsVer");

    setkeyup();

    $("#perdN").click(function() {
        $("#startDt").attr("disabled", true);
        $("#endDt").attr("disabled", true);
    });
    $("#perdY").click(function() {
        $("#startDt").attr("disabled", false);
        $("#endDt").attr("disabled", false);
    });
});

serviceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON","svcListSuccess","NoneForm");
}

svcListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var svcListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            svcListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#svcSelbox").html(svcListHtml);
        $("#svcSelbox").val($("#svcSeq").val());

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").attr('disabled', true);
        }
        svcTermsSelList();
    }
}

svcTermsSelList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SVC_TERMS" ,"svcTermsSelListSuccess","NoneForm");
}

svcTermsSelListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var svcTermsInfoListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            svcTermsInfoListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#stpltSelbox").html(svcTermsInfoListHtml);
        $("#stpltSelbox").val($("#stpltType").val());
    }  else {
        $("#stpltSelbox").html("");
        $("#stpltSelbox").attr("disabled", true);
        popAlertLayer(getMessage("info.svcterms.nodata.msg"));
    }
}

function registSvcTerms() {
    if ($("#svcTermsVer").val().trim() == "") {
        popAlertLayer(getMessage("svcterms.detail.version.empty.msg"));
        return;
    }

    if ($("#startDt").val() == "") {
        popAlertLayer(getMessage("svcterms.display.period.startdate.empty.msg"));
        return;
    }

    if ($("#endDt").val() == "") {
        popAlertLayer(getMessage("svcterms.display.period.enddate.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.svcTermsSbst.getData().length < 1) {
        popAlertLayer(getMessage("svcterms.detail.sbst.empty.msg"));
        return;
    }

    isRightSvcTermsConts();

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        if (changeStpltSeq != "" || stopStpltYn == "Y") {
            popAlertLayer(getMessage("svcterms.exist.msg"), null, null, null, null, true);
            $(".swal-button").click(function() {
                registSvcTermsFunc();
            });
        } else if(isExistSvcTerms) {
            popAlertLayer(getMessage("svcterms.1st.regist"), null, null, null, null, true);
            $(".swal-button").click(function() {
                registSvcTermsFunc();
            });
        } else {
            registSvcTermsFunc();
        }
    }, null, getMessage("common.confirm"), true);
}

isRightSvcTermsConts = function() {
    callByGet("/api/serviceTerms?svcSeq=" + $("#svcSelbox").val() + "&comnCdValue=" + $("#stpltSelbox").val() + "&existVer=Y",
            "svcTermsContsSuccess", "NoneForm");
}

var date = new Date().format("yyyy-MM-dd");
svcTermsContsSuccess = function(data) {
    if (data.resultCode == "1000") {
        var svcTermsVerList = [];
        var svcTermsDateList = [];
        var svcTermsUseYnList = [];
        var existStpltSeq = "";
        var existStpltStDt = "";
        var existStpltVer = "";
        var count = "";
        var repeat = "";

        var svcTermsVersion = $("#svcTermsVer").val().trim();
        for (var a = 0; a < svcTermsVersion.length; a++) {
            if (!isNaN(svcTermsVersion.charAt(a)) && repeat == "") {
                count = a;
                repeat++;
            }
        }
        svcTermsVersion = svcTermsVersion.substring(count);

        $(data.result.svcTermsExistInfoList).each(function(i,item) {
            if (item.stpltVer != "") {
                repeat = "";
                for (var a = 0; a < item.stpltVer.length; a++) {
                    if (!isNaN(item.stpltVer.charAt(a)) && repeat == "") {
                        count = a;
                        repeat++;
                    }
                }
                svcTermsVerList.push(item.stpltVer.substring(count));
            }
            svcTermsDateList.push(item.stDt);
            svcTermsUseYnList.push(item.useYn);
            if (item.useYn == "Y") {
                repeat = "";
                existStpltSeq += item.stpltSeq + ",";
                existStpltStDt += item.stDt + ",";
                if (item.stpltVer != "") {
                    for (var a = 0; a < item.stpltVer.length; a++) {
                        if (!isNaN(item.stpltVer.charAt(a)) && repeat == "") {
                            count = a;
                            repeat++;
                        }
                    }
                    existStpltVer += item.stpltVer.substring(count) + ",";
                }
            }
        });
        for (var i = 0; i < svcTermsVerList.length; i++) {
            if (svcTermsVersion != "") {
                repeat = "";
                if (svcTermsVerList[i] == svcTermsVersion) {
                    popAlertLayer(getMessage("svcterms.exist.svcTerms.stpltVer.msg"));
                    return;
                }
            }
            if (svcTermsUseYnList[i] == "Y" && $(":input:radio[name=perdYn]:checked").val() == "Y") {
                if (svcTermsDateList[i] == $("#startDt").val()) {
                    popAlertLayer(getMessage("svcterms.exist.period.msg"));
                    return;
                } else if (svcTermsDateList[i] != "" && svcTermsDateList[i] < $("#startDt").val()) {
                    for (var j = 0; j < svcTermsVerList.length; j++) {
                        if (svcTermsVerList[j] > svcTermsVersion) {
                            popAlertLayer(getMessage("svcterms.check.svcTerms.stpltVer.msg"));
                            return;
                        }
                    }
                }
            }
        }

        if ($(":input:radio[name=useYn]:checked").val() == "Y") {
            existStpltSeq = existStpltSeq.substr(0,existStpltSeq.length-1);
            existStpltStDt = existStpltStDt.substr(0,existStpltStDt.length-1);
            existStpltVer = existStpltVer.substr(0,existStpltVer.length-1);
            var arrStpltSeq = existStpltSeq.split(",");
            var arrStpltStDt = existStpltStDt.split(",");
            var arrStpltVer = existStpltVer.split(",");
            var compareStpltVer = existStpltVer.split(",");
            for (var q = 0; q < arrStpltStDt.length; q++) {
                if (arrStpltStDt[q] > $("#startDt").val() && $(":input:radio[name=perdYn]:checked").val() == "Y") {
                    popAlertLayer(getMessage("svcterms.check.svcTerms.period.msg"));
                    return;
                }
            }
            arrStpltVer.sort(function(a,b) {
                return a-b;
            });
            if (svcTermsVersion < arrStpltVer[0]) {
                popAlertLayer(getMessage("svcterms.check.svcTerms.stpltVer.msg"));
                return;
            }
            if (arrStpltSeq.length == 1) { // 사용중인 약관이 한개일때
                if ($(":input:radio[name=perdYn]:checked").val() == "N") {
                    //사용중인 약관은 중지되고 새로운 약관이 게시
                    changeStpltSeq = arrStpltSeq[0];
                } else {
                    if ($("#startDt").val() == date) { // 시작일이 오늘일 경우 기존의 약관을 중지처리
                        changeStpltSeq = arrStpltSeq[0];
                    } else {
                        changeStpltSeq = "";
                    }
                }
            } else if (arrStpltSeq.length > 1) { // 사용중인 약관이 두개 이상일때
                if ($(":input:radio[name=perdYn]:checked").val() == "N") {
                    if (svcTermsVersion > arrStpltVer[arrStpltVer.length-1]){
                        stopStpltYn = "Y";
                    } else if (svcTermsVersion > arrStpltVer[0]) {
                        for (var b = 0; b < compareStpltVer.length; b++) {
                            if (compareStpltVer[b] == arrStpltVer[0]) {
                                changeStpltSeq = arrStpltSeq[b]
                            }
                        }
                    }
                }
            }
        }
    } else if (data.resultCode == "1010") { // 등록된 약관이 없을 때
        changeStpltSeq = "";
        isExistSvcTerms = true;
    }
}

function registSvcTermsFunc() {
    CKEDITOR.instances.svcTermsSbst.updateElement();
    $("#svcTermsSbst").val(CKEDITOR.instances.svcTermsSbst.getData());

    formData("NoneForm", "svcSeq", $("#svcSelbox").val());
    formData("NoneForm", "stpltType", $("#stpltSelbox").val());
    formData("NoneForm", "stpltVer", $("#svcTermsVer").val());
    formData("NoneForm", "stpltSbst", $("#svcTermsSbst").val());
    if (changeStpltSeq == "") {
        formData("NoneForm", "changeStpltSeq", 0);
    } else {
        formData("NoneForm", "changeStpltSeq", changeStpltSeq);
    }
    if (stopStpltYn == "Y") {
        formData("NoneForm", "stopStpltYn", stopStpltYn);
    }

    if (isExistSvcTerms) {
        formData("NoneForm", "perdYn", "N");
    } else {
        if ($(":input:radio[name=perdYn]:checked").val() == "N") {
            formData("NoneForm", "perdYn", "N");
        } else {
            formData("NoneForm", "perdYn", "Y");
            formData("NoneForm", "stDt", $("#startDt").val() + " 00:00:00");
            formData("NoneForm", "fnsDt", $("#endDt").val() + " 23:59:59");
        }
    }
    formData("NoneForm", "useYn", $(":input:radio[name=useYn]:checked").val());
    formData("NoneForm", "mandYn", $(":input:radio[name=mandYn]:checked").val());

    callByPost("/api/serviceTerms", "registSvcTermsSuccess", "NoneForm", "insertFail");
}

insertFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

registSvcTermsSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"), "/onlineService/terms#page=1&svcSeq=" + $("#svcSelbox").val() + "&comnCdValue=" + $("#stpltSelbox").val());
        /*
        if (isExistSvcTerms) {
            popAlertLayer(getMessage("svcterms.1st.regist"), null, null, null, null, true);
            $(".swal-button").click(function() {
                popAlertLayer(getMessage("success.common.insert"), "/onlineService/terms#page=1&svcSeq=" + $("#svcSelbox").val() + "&comnCdValue=" + $("#stpltSelbox").val());
            });
        } else {
            popAlertLayer(getMessage("success.common.insert"), "/onlineService/terms#page=1&svcSeq=" + $("#svcSelbox").val() + "&comnCdValue=" + $("#stpltSelbox").val());
        }
        */
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

function cancelRegistSvcTerms() {
    var mvPage = "/onlineService/terms#page=1";
    if ($("#svcSelbox").val() != 0) {
        mvPage += "&svcSeq=" + $("#svcSelbox").val();
    }
    if ($("#stpltSelbox").val() != 0) {
        mvPage += "&comnCdValue=" + $("#stpltSelbox").val();
    }
    popConfirmLayer(getMessage("common.cancel.msg"));
    $(".swal-button--confirm").click(function() {
        pageMove(mvPage);
    });
}