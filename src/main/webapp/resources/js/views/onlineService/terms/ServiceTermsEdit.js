/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var svcTermsUseYn = "";
var svcTermsSvcSeq = "";
var svcTermsType = "";
var svcTermsVer = "";
var svcTermsStDt = "";
var svcTermsFnsDt = "";
var stopStpltYn = "";
var svcTermsPerdYn = "";
var changeStpltSeq = "";

$(document).ready(function() {
    serviceList();

    var now=new Date();
    $("#startDt").val(dateString((new Date(now.getTime()))));
    $("#endDt").val(dateString((new Date(now.getTime()))));

    $("#startDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#endDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#endDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#startDt").datepicker("option","maxDate", selected);
        }
    });

    $("#regNm").html($("#name").val());

    limitInputTitle("svcTermsVer");

    setkeyup();

    $("#perdN").click(function() {
        $("#startDt").attr("disabled", true);
        $("#endDt").attr("disabled", true);
    });
    $("#perdY").click(function() {
        $("#startDt").attr("disabled", false);
        $("#endDt").attr("disabled", false);
    });
});

serviceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON","svcListSuccess","NoneForm");
}

svcListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var svcListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            svcListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#svcSelbox").html(svcListHtml);

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").attr('disabled', true);
        }
    }
    svcTermsInfo();
}

svcTermsInfo = function() {
    formData("NoneForm", "stpltSeq", $("#stpltSeq").val());
    callByGet("/api/serviceTerms", "svcTermsInfoSuccess", "NoneForm", "svcTermsInfoFail");
}

svcTermsInfoSuccess = function(data) {
    if (data.resultCode = "1000") {
        var item = data.result.svcTermsInfo;
        svcTermsUseYn = item.useYn;
        svcTermsSvcSeq = item.svcSeq;
        svcTermsType = item.stpltType;
        svcTermsVer = item.stpltVer;
        svcTermsStDt = item.stDt;
        svcTermsFnsDt = item.fnsDt;
        svcTermsPerdYn = item.perdYn;
        $("#svcSelbox").val(item.svcSeq);
        $("#stpltType").html(item.stpltTypeNm);
        $("#svcTermsVer").val(svcTermsVer);

        var svcTermsSbst = strConv(item.stpltSbst);
        svcTermsSbst = strConv(svcTermsSbst);
        CKEDITOR.instances.svcTermsSbst.setData(svcTermsSbst);

        if (item.mandYn == "Y") {
            $("#mandY").prop("checked", true);
        } else {
            $("#mandN").prop("checked", true);
        }

        if (item.perdYn == "Y") {
            $("#perdY").prop("checked", true);
            $("#startDt").attr("disabled", false);
            $("#endDt").attr("disabled", false);
            $("#startDt").val(item.stDt);
            $("#endDt").val(item.fnsDt);
        } else {
            $("#perdN").prop("checked", true);
        }

        if (item.useYn == "Y") {
            $("#useY").prop("checked", true);
        } else {
            $("#useN").prop("checked", true);
        }
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineService/terms");
    }
}

svcTermsInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/onlineService/terms");
}

function editSvcTerms() {
    if ($("#svcTermsVer").val().trim() == "") {
        popAlertLayer(getMessage("svcterms.detail.version.empty.msg"));
        return;
    }

    if ($("#startDt").val() == "") {
        popAlertLayer(getMessage("svcterms.display.period.startdate.empty.msg"));
        return;
    }

    if ($("#endDt").val() == "") {
        popAlertLayer(getMessage("svcterms.display.period.enddate.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.svcTermsSbst.getData().length < 1) {
        popAlertLayer(getMessage("svcterms.detail.sbst.empty.msg"));
        return;
    }

    isRightSvcTermsConts();

    if (svcTermsUseYn == "Y") {
        if ($(":input:radio[name=useYn]:checked").val() == "N") {
            popAlertLayer(getMessage("svcterms.delete.not.y.msg"));
            return;
        }
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        if (changeStpltSeq != "" || stopStpltYn == "Y") {
            popAlertLayer(getMessage("svcterms.exist.msg"), null, null, null, null, true);
            $(".swal-button").click(function() {
                editSvcTermsFunc();
            });
        } else {
            editSvcTermsFunc();
        }
    }, null, getMessage("common.confirm"), true);
}

isRightSvcTermsConts = function() {
    callByGet("/api/serviceTerms?svcSeq=" + $("#svcSelbox").val() + "&comnCdValue=" + svcTermsType + "&existVer=Y", "svcTermsContsSuccess", "NoneForm");
}

var date = new Date().format("yyyy-MM-dd");
svcTermsContsSuccess = function(data) {
    if (data.resultCode == "1000") {
        var svcTermsVerList = [];
        var svcTermsDateList = [];
        var svcTermsUseYnList = [];
        var existStpltSeq = "";
        var existStpltStDt = "";
        var existStpltVer = "";
        var count = "";
        var repeat = "";
        var svcTermsVerNum = "";

        var svcTermsVersion = $("#svcTermsVer").val().trim();
        for (var a = 0; a < svcTermsVersion.length; a++) {
            if (!isNaN(svcTermsVersion.charAt(a)) && repeat == "") {
                count = a;
                repeat++;
            }
        }
        svcTermsVersion = svcTermsVersion.substring(count);

        $(data.result.svcTermsExistInfoList).each(function(i,item) {
            if (item.stpltVer != "") {
                repeat = "";
                for (var a = 0; a < item.stpltVer.length; a++) {
                    if (!isNaN(item.stpltVer.charAt(a)) && repeat == "") {
                        count = a;
                        repeat++;
                    }
                }
                svcTermsVerList.push(item.stpltVer.substring(count));
            }
            svcTermsDateList.push(item.stDt);
            svcTermsUseYnList.push(item.useYn);
            if (item.useYn == "Y") {
                repeat = "";
                existStpltSeq += item.stpltSeq + ",";
                existStpltStDt += item.stDt + ",";
                if (item.stpltVer != "") {
                    for (var a = 0; a < item.stpltVer.length; a++) {
                        if (!isNaN(item.stpltVer.charAt(a)) && repeat == "") {
                            count = a;
                            repeat++;
                        }
                    }
                    existStpltVer += item.stpltVer.substring(count) + ",";
                }
            }
        });
        if (svcTermsSvcSeq == $("#svcSelbox").val()) {
            svcTermsVerNum = "";
            repeat = "";
            for (var a = 0; a < svcTermsVer.length; a++) {
                if (!isNaN(svcTermsVer.charAt(a)) && repeat == "") {
                    count = a;
                    repeat++;
                }
            }
            svcTermsVerNum = svcTermsVer.substring(count);
        }
        for (var i = 0; i < svcTermsVerList.length; i++) {
            if (svcTermsVersion != "") {
                if (svcTermsVerList[i] == svcTermsVersion) {
                    if (svcTermsSvcSeq == $("#svcSelbox").val()) {
                        if (svcTermsVerNum != svcTermsVersion) {
                            popAlertLayer(getMessage("svcterms.exist.svcTerms.stpltVer.msg"));
                            return;
                        }
                    } else if (svcTermsSvcSeq != $("#svcSelbox").val()) {
                        popAlertLayer(getMessage("svcterms.exist.svcTerms.stpltVer.msg"));
                        return;
                    }
                }
            }
            if (svcTermsUseYnList[i] == "Y" && $(":input:radio[name=perdYn]:checked").val() == "Y") {
                if (svcTermsDateList[i] == $("#startDt").val()) {
                    if (svcTermsSvcSeq != $("#svcSelbox").val()) {
                        popAlertLayer(getMessage("svcterms.exist.period.msg"));
                        return;
                    }
                    /*
                    if (svcTermsSvcSeq == $("#svcSelbox").val()) {
                        if (svcTermsStDt != $("#startDt").val()) {
                            popAlertLayer(getMessage("svcterms.exist.period.msg"));
                            return;
                        }
                    }
                    */
                } else if (svcTermsDateList[i] < $("#startDt").val() && svcTermsStDt != $("#startDt").val()) {
                    for (var j = 0; j < svcTermsVerList.length; j++) {
                        if (svcTermsSvcSeq == $("#svcSelbox").val()) {
                            if (svcTermsVerNum != svcTermsVersion) {
                                popAlertLayer(getMessage("svcterms.check.svcTerms.stpltVer.msg"));
                                return;
                            }
                        }
                        if (svcTermsVerList[j] > svcTermsVersion) {
                            if (!(svcTermsStDt == "" && svcTermsFnsDt == "" && $("#startDt").val() <= date && $("#endDt").val() >= date)) {
                                popAlertLayer(getMessage("svcterms.check.svcTerms.stpltVer.msg"));
                                return;
                            }
                        }
                    }
                }
            }
        }

        if ($(":input:radio[name=useYn]:checked").val() == "Y") {
            existStpltSeq = existStpltSeq.substr(0,existStpltSeq.length-1);
            existStpltStDt = existStpltStDt.substr(0,existStpltStDt.length-1);
            existStpltVer = existStpltVer.substr(0,existStpltVer.length-1);
            var arrStpltSeq = existStpltSeq.split(",");
            var arrStpltStDt = existStpltStDt.split(",");
            var arrStpltVer = existStpltVer.split(",");
            var compareStpltVer = existStpltVer.split(",");
            var isChangeDate = false;
            for (var q = 0; q < arrStpltStDt.length; q++) {

                if (arrStpltStDt[q] > $("#startDt").val() && $(":input:radio[name=perdYn]:checked").val() == "Y") {
                    if ($("#startDt").val() != date) {
                        changeStpltSeq = "";
                        if ($("#startDt").val() != svcTermsStDt && svcTermsStDt != "") {
                            popAlertLayer(getMessage("svcterms.check.svcTerms.period.msg"));
                            return;
                        }
                    } else if ($("#startDt").val() == date) {
                        isChangeDate = true;
                    }
                }
            }
            arrStpltVer.sort(function(a,b) {
                return a-b;
            });
            if (svcTermsVersion < arrStpltVer[0]) {
                popAlertLayer(getMessage("svcterms.check.svcTerms.stpltVer.msg"));
                return;
            }
            if (arrStpltSeq.length == 1) { // 사용중인 약관이 한개일때
                if (svcTermsUseYn == "N") {
                    if ($(":input:radio[name=perdYn]:checked").val() == "N") {
                        //사용중인 약관은 중지되고 새로운 약관이 게시
                        changeStpltSeq = arrStpltSeq[0];
                    } else {
                        if ($("#startDt").val() == date) { // 시작일이 오늘일 경우 기존의 약관을 중지처리
                            changeStpltSeq = arrStpltSeq[0];
                        } else {
                            changeStpltSeq = "";
                        }
                    }
                }
            } else if (arrStpltSeq.length > 1) { // 사용중인 약관이 두개 이상일때
                if (svcTermsUseYn == "N") {
                    if ($(":input:radio[name=perdYn]:checked").val() == "N") {
                        if (svcTermsVersion > arrStpltVer[arrStpltVer.length-1]){
                            stopStpltYn = "Y";
                        } else if (svcTermsVersion > arrStpltVer[0]) {
                            for (var b = 0; b < compareStpltVer.length; b++) {
                                if (compareStpltVer[b] == arrStpltVer[0]) {
                                    changeStpltSeq = arrStpltSeq[b];
                                } else {
                                    changeStpltSeq = "";
                                }
                            }
                        }
                    }
                } else {
                    if (isChangeDate || ($(":input:radio[name=perdYn]:checked").val() == "N" && svcTermsPerdYn == "Y")) {
                        for (var b = 0; b < compareStpltVer.length; b++) {
                            if (compareStpltVer[b] == arrStpltVer[0]) {
                                changeStpltSeq = arrStpltSeq[b];
                            }
                        }
                    } else if ($(":input:radio[name=perdYn]:checked").val() == "Y" && $("#startDt").val() > date) {
                        changeStpltSeq = "";
                    }
                }
            }
        }
    }
}

function editSvcTermsFunc() {
    CKEDITOR.instances.svcTermsSbst.updateElement();
    $("#svcTermsSbst").val(CKEDITOR.instances.svcTermsSbst.getData());

    formData("NoneForm", "stpltSeq", $("#stpltSeq").val());
    formData("NoneForm", "svcSeq", $("#svcSelbox").val());
    formData("NoneForm", "stpltType", svcTermsType);
    formData("NoneForm", "stpltVer", $("#svcTermsVer").val());
    formData("NoneForm", "stpltSbst", $("#svcTermsSbst").val());
    if (changeStpltSeq == "") {
        formData("NoneForm", "changeStpltSeq", 0);
    } else {
        formData("NoneForm", "changeStpltSeq", changeStpltSeq);
    }

    if (stopStpltYn == "Y") {
        formData("NoneForm", "stopStpltYn", stopStpltYn);
    }

    if ($(":input:radio[name=perdYn]:checked").val() == "N") {
        formData("NoneForm", "perdYn", "N");
    } else {
        formData("NoneForm", "perdYn", "Y");
        formData("NoneForm", "stDt", $("#startDt").val() + " 00:00:00");
        formData("NoneForm", "fnsDt", $("#endDt").val() + " 23:59:59");
    }
    formData("NoneForm", "useYn", $(":input:radio[name=useYn]:checked").val());
    formData("NoneForm", "mandYn", $(":input:radio[name=mandYn]:checked").val());

    callByPut("/api/serviceTerms", "editSvcTermsSuccess", "NoneForm", "updateFail");
}

updateFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

editSvcTermsSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/onlineService/terms#page=1&svcSeq=" + $("#svcSelbox").val() + "&comnCdValue=" + svcTermsType);
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function cancelEditSvcTerms() {
    var mvPage = "/onlineService/terms#page=1";
    if ($("#svcSelbox").val() != 0) {
        mvPage += "&svcSeq=" + $("#svcSelbox").val();
    }
    if (svcTermsType != 0) {
        mvPage += "&comnCdValue=" + svcTermsType;
    }

    popConfirmLayer(getMessage("common.cancel.msg"));
    $(".swal-button--confirm").click(function() {
        pageMove(mvPage);
    });
}