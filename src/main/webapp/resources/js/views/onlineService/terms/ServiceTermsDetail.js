/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var svcTermsUseYn = "";
var svcSeq = "";
var stpltType = "";
var stDt = "";

$(document).ready(function() {
    svcTermsInfo();
});

svcTermsInfo = function() {
    callByGet("/api/serviceTerms?stpltSeq=" + $("#stpltSeq").val() , "svcTermsInfoSuccess", "NoneForm", "svcTermsInfoFail");
}

svcTermsInfoSuccess = function(data) {
    if (data.resultCode = "1000") {
        var item = data.result.svcTermsInfo;
        var mandHtml = "";
        var periodHtml = "";
        var useYnHtml = "";
        svcTermsUseYn = item.useYn;
        svcSeq = item.svcSeq;
        stpltType = item.stpltType;
        stDt = item.stDt;

        $("#stpltReger").html(item.cretrNm);
        $("#svcNm").html(item.svcNm);
        $("#stpltSection").html(item.stpltTypeNm);
        $("#stpltVer").html(item.stpltVer);
        $("#stpltContent").html(item.stpltSbst);
        if (item.mandYn == "Y") {
            mandHtml = "<input type=\"radio\" value='Y' name='mandYn' id=\"mandYn\" checked disabled><label>" + getMessage("svcterms.mandYn.yes")
            + "</label>&nbsp;<input type=\"radio\" value='N' name='mandYn' id=\"mandYn\" disabled><label>" + getMessage("svcterms.mandYn.no") + "</label>";
        } else {
            mandHtml = "<input type=\"radio\" value='Y' name='mandYn' id=\"mandYn\" disabled><label>" + getMessage("svcterms.mandYn.yes")
            + "</label>&nbsp;<input type=\"radio\" value='N' name='mandYn' id=\"mandYn\" checked disabled><label>" + getMessage("svcterms.mandYn.no") + "</label>";
        }
        $("#stpltMandYn").html(mandHtml);
        if (item.perdYn == "Y") {
            periodHtml = item.stDt + "~" + item.fnsDt;
            $("#stpltPerd").html(periodHtml);
        } else {
            $("#stpltPerd").html(getMessage("svcterms.perdYn.no"));
        }
        if (item.useYn == "Y") {
            useYnHtml = "<input type=\"radio\" value='Y' name='useYn' id=\"useYn\" checked disabled><label>" + getMessage("common.sttus.display")
                    + "</label>&nbsp;<input type=\"radio\" value='N' name='useYn' id=\"useYn\" disabled><label>" + getMessage("common.sttus.stop") + "</label>";
        } else {
            useYnHtml = "<input type=\"radio\" value='Y' name='useYn' id=\"useYn\" disabled><label>" + getMessage("common.sttus.display")
            + "</label>&nbsp;<input type=\"radio\" value='N' name='useYn' id=\"useYn\" checked disabled><label>" + getMessage("common.sttus.stop") + "</label>";
        }
        $("#stpltUseYn").html(useYnHtml);
        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineService/terms");
    }
}

svcTermsInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/onlineService/terms");
}

var date = new Date().format("yyyy-MM-dd");
svcTermsDeleteCheck = function() {
    if (svcTermsUseYn == "Y") {
        if (stDt == date || stDt < date) {
            popAlertLayer(getMessage("svcterms.delete.not.y.msg"));
            return;
        } else {
            callByGet("/api/serviceTerms?svcSeq=" + svcSeq + "&comnCdValue=" + stpltType + "&existVer=Y&useYn=Y",
                "svcTermsSuccess", "NoneForm");
        }
    } else {
        svcTermsDelete();
    }
}

svcTermsSuccess = function(data) {
    var delSvcTerms = false;
    if (data.resultCode == "1000") {
        var stDtList = "";
        $(data.result.svcTermsExistInfoList).each(function(i,item) {
            stDtList += item.stDt + ",";
        });
        stDtList = stDtList.slice(0, -1).split(",");
        for (var i = 0; i < stDtList.length; i++) {
            if (stDtList[i] == date || stDtList[i] < date || stDtList[i] == "") {
                delSvcTerms = true;
            }
        }
        if (!delSvcTerms) {
            popAlertLayer(getMessage("svcterms.delete.not.y.msg"));
            return;
        } else {
            svcTermsDelete();
        }
    }
}

svcTermsDelete = function() {
    popConfirmLayer($("#stpltVer").text() + getMessage("svcterms.delete.confirm.msg"), function() {
        formData("NoneForm" , "stpltSeqList", $("#stpltSeq").val());
        callByDelete("/api/serviceTerms", "svcTermsDeleteSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

svcTermsDeleteSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/onlineService/terms#page=1&svcSeq=" + svcSeq + "&comnCdValue=" + stpltType);
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}