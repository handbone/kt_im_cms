/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hashSvc;
var hashPlcy;
var now=new Date();
$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
        hashPlcy= checkUndefined(findGetParameter(str_hash,"plcySeq"));
    }
    setOlSvcList();
    limitInputTitle("banrCampNm,btnTitle,banrTitle,banrSubTitle");

    $("#stDt").val(dateString((new Date(now.getTime()))));
    $("#fnsDt").val(dateString((new Date(now.getTime()))));

    $("#stDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#fnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#fnsDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#stDt").datepicker("option","maxDate", selected)
        }
    });

    setkeyup();

    $("input[name=perdYn]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "Y") {
            $("input[name=perdDt]").attr("disabled",false).css("background-color","#ffffff");
        } else {
            $("input[name=perdDt]").attr("disabled",true).css("background-color","#f5f5f5");
        }
    });

    $("input[name=btnTitleChk]").change(function() {
        var chkVal = $(this).attr("checked");
        if (chkVal == "checked") {
            $("#btnTitle").attr({"disabled":false,"readonly" : false});
        } else {
            $("#btnTitle").attr({"disabled":true,"readonly" : true});
        }
    });

    $("input[name=banrTitleChk]").change(function() {
        var chkVal = $(this).attr("checked");
        if (chkVal == "checked") {
            $("#banrTitle").attr({"disabled":false,"readonly" : false});
        } else {
            $("#banrTitle").attr({"disabled":true,"readonly" : true});
        }
    });

    $("input[name=banrSubTitleChk]").change(function() {
        var chkVal = $(this).attr("checked");
        if (chkVal == "checked") {
            $("#banrSubTitle").attr({"disabled":false,"readonly" : false});
        } else {
            $("#banrSubTitle").attr({"disabled":true,"readonly" : true});
        }
    });


    fileFormInsert("banrImg","BANR_IMG",-1,-1,"banrImg","imgPreview","png,jpg,gif,bmp,jpeg");
    fileFormAllView();
});

/* 초기 서비스 영역 SET*/
setOlSvcList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON", "setOlSvcListSuccess", '', "failOlSvcList");
}
/* 초기 서비스 영역 DATA SUCESS */
setOlSvcListSuccess = function(data){
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#svcList").html("");
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + parseInt(item.comnCdValue) + "\">" + item.comnCdNm + "</option>";
    });
    $("#svcList").html(serviceListHtml);
    if ($("#memberSec").val() == "04" ) {
        // 05 : 매장 관리자, 서비스 변경 불가
        $("#svcList").attr('disabled', 'true');
        $("#svcList").val($("#svcSeq").val());
    } else if (hashSvc != null && hashSvc != "") {
        $("#svcList").val(hashSvc);
    }

    banrInPlcyListF();
}
/* 초기 서비스 영역 DATA FAIL */
failOlSvcList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#svcList").html("");
}

banrInPlcyListF = function(){
    callByGet("/api/plcyMng?page=-1&rows=1&svcSeq="+$("#svcList").val(),"banrPlcyListSuccess","NoneForm","failPlcyList");

}

banrPlcyListSuccess = function(data){
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noPlcy.msg");
        }
        popAlertLayer(msg);
        $("#banrInPlcyList").html("");
        return;
    }

    var plcyListHtml = "";
    $(data.result.plcyList).each(function(i, item) {
        plcyListHtml += "<option data-fileType="+item.fileTypeALL+" data-sizeHght="+item.sizeHght+" data-sizeWdth="+item.sizeWdth+" value=\"" + item.plcySeq + "\">" + item.terrNm + "</option>";
    });

    $("#banrInPlcyList").html(plcyListHtml);

    if (hashPlcy != null && hashPlcy != "") {
        $("#banrInPlcyList").val(hashPlcy);
    }


    setPlcyFile();
}

failPlcyList = function(data){
    popAlertLayer(getMessage("fail.common.select"));
    $("#banrInPlcyList").html("");
}

function makePageMove(e){
    var mvPage = "/onlineService/banr#page=1";
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");

        mvPage += "&svcSeq=" + checkUndefined(findGetParameter(str_hash,"svcSeq"));
        mvPage += "&plcySeq=" + checkUndefined(findGetParameter(str_hash,"plcySeq"));
    }
    pageMove(mvPage);
}



function banrRegist() {
    if ($("#banrCampNm").val().trim() == "") {
        popAlertLayer(getMessage("banr.use.campNm.empty.msg"));
        $("#banrCampNm").focus();
        return;
    }

    if ($("#banrInPlcyList option").length == 0) {
        popAlertLayer(getMessage("common.noPlcy.msg"));
        return;
    }

    if (fileSet[0].filePath == "") {
        popAlertLayer(getMessage("banr.input.Img.msg"));
        return;
    }

    if ($("#linkUrl").val().trim() != "" && !validateUrl($("#linkUrl").val())) {
        popAlertLayer(getMessage("common.url.error.msg"));
        return;
    }

    var useTitleType = "";
    if ($("input[name=btnTitleChk]").attr("checked") == "checked") {
        useTitleType += "BT";
        if ($("#btnTitle").val().trim() == "") {
            popAlertLayer(getMessage("banr.use.btnTitle.empty.msg"));
            $("#btnTitle").focus();
            return;
        }
    }
    if ($("input[name=banrTitleChk]").attr("checked") == "checked"){
        if (useTitleType != "") {
            useTitleType +=",";
        }
        useTitleType += "BNT";
        if ($("#banrTitle").val().trim() == "") {
            popAlertLayer(getMessage("banr.use.barnTitle.empty.msg"));
            $("#banrTitle").focus();
            return;
        }
    }
    if ($("input[name=banrSubTitleChk]").attr("checked") == "checked"){
        if (useTitleType != "") {
            useTitleType +=",";
        }
        useTitleType += "BST";
        if ($("#banrSubTitle").val().trim() == "") {
            popAlertLayer(getMessage("banr.use.banrSubTitle.empty.msg"));
            $("#banrSubTitle").focus();
            return;
        }
    }
    $("#comnty_write_from > input[type=hidden]").remove();

    formData("comnty_write_from", "banrCampNm", $("#banrCampNm").val().trim());
    formData("comnty_write_from", "svcSeq", $("#svcList").val());
    formData("comnty_write_from", "plcySeq",$("#banrInPlcyList").val());
    formData("comnty_write_from", "perdYn", $("input[name=perdYn]:checked").val());
    formData("comnty_write_from", "stDt", $("#stDt").val());
    formData("comnty_write_from", "fnsDt", $("#fnsDt").val());
    formData("comnty_write_from", "linkUrl", $("#linkUrl").val());
    formData("comnty_write_from", "linkUri", $("#linkUri").val());
    formData("comnty_write_from", "useTitleType", useTitleType);
    formData("comnty_write_from", "btnTitle", $("#btnTitle").val());
    formData("comnty_write_from", "banrTitle", $("#banrTitle").val());
    formData("comnty_write_from", "banrSubTitle", $("#banrSubTitle").val());
    formData("comnty_write_from", "useYn", $("input[name=useYn]:checked").val());
    callByMultipart("/api/banrMng", "banrRegistSuccess", "comnty_write_from", "insertFail");

//    popConfirmLayer(getMessage("common.regist.msg"), function() {
//
//    }, null, getMessage("common.confirm"));
}

insertFail = function(data){
    if (!fileAccss && typeof data.resultCode == "undefined") {
        popAlertLayer(getMessage("common.check.file.Position"));
    } else if (data.resultCode == "1500") {
        if (data.resultMsg == "Over File Size") {
            popAlertLayer(getMessage("info.upload.fileSize.over.msg"));
        } else {
            popAlertLayer(getMessage("info.upload.failed.msg"));
        }
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();

}

banrRegistSuccess = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"),"/onlineService/banr#page=1"+"&plcySeq="+$("#banrInPlcyList").val()+"&perdSttus="+"&svcSeq="+$("#svcList").val());
    } else {
        if (data.resultMsg == "Over File Size") {
            popAlertLayer(getMessage("info.upload.fileSize.over.msg"));
        } else {
            popAlertLayer(getMessage("fail.common.insert"));
        }
    }
    $("#comnty_write_from > input[type=hidden]").remove();

}

setPlcyFile = function(){
    for(var i =0; i < fileSet.length; i++){
        var setVar = fileSet[i];
        setVar.lmtHeight = $("#banrInPlcyList option:selected").data("sizehght");
        setVar.lmtWidth = $("#banrInPlcyList option:selected").data("sizewdth");
        setVar.accessType = $("#banrInPlcyList option:selected").data("filetype");
    }
}


