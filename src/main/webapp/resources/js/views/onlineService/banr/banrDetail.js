/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var arr = new Array();
var banrSeq = 0;
$(document).ready(function(){
    banrInfo();
});

banrInfo = function(){
    callByGet("/api/banrMng?banrSeq="+$("#banrSeq").val(), "banrInfoSuccess", "NoneForm", "banrInfoFail");
}

banrInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.banrInfo;
        banrSeq = item.banrSeq;
        $("#svcNm").text(item.svcNm);
        $("#banrCampNm").text(item.banrCampNm);

        $("#svcSeq").val(item.svcSeq);
        $("#plcySeq").val(item.plcySeq);

        $("#terrNm").text(item.terrNm);
        if (!empty(item.banrImg)) {
            $("#fileNm").text(item.banrImg.imgNm);
            $("#imgPreview").html("<a href=\""+makeAPIUrl(item.banrImg.imgPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(item.banrImg.imgPath,"img")+"' title=\""+item.banrImg.fileSeq+"\"></a>");
            $("#imgPreview").justifiedGallery({
                rowHeight : 220,
                lastRow : 'nojustify',
                margins : 3
            });
            $(".swipebox").swipebox();
        }




        $("input[type=radio][name=perdYn][value="+item.perdYn+"]").attr("checked", true);
        $("#stDt").val(item.stDt);
        $("#fnsDt").val(item.fnsDt);


        $("#linkUrl").text(item.linkUrl);
        $("#linkUri").text(item.linkUri);

        var useTitleType = item.useTitleType;
        if (useTitleType.indexOf('BT') != -1){
            $("input[name=btnTitleChk]").attr("checked", true);
        }
        $("#btnTitle").text(item.btnTitle);
        if (useTitleType.indexOf('BNT') != -1){
            $("input[name=banrTitleChk]").attr("checked", true);
        }
        $("#banrTitle").text(item.banrTitle);
        if (useTitleType.indexOf('BST') != -1){
            $("input[name=banrSubTitleChk]").attr("checked", true);
        }
        $("#banrSubTitle").text(item.banrSubTitle);


        $("input[type=radio][name=useYn][value="+item.useYn+"]").attr("checked", true);



        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/banr");
    }
}

banrInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/banr");
}
deleteBanr = function(){
    var delSeqs = banrSeq;
    var delNmList = $("#banrCampNm").text();

    popConfirmLayer(getMessage("common.delete.msg"), function() {
        formData("NoneForm" , "delSeqs", delSeqs);
        callByDelete("/api/banrMng", "deleteBanrSuccess", "NoneForm","deleteFail");
    }, null, getMessage("common.confirm"));

}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteBanrSuccess = function(data){
    if(data.resultCode == "1000"){

        popAlertLayer(getMessage("success.common.delete"),"/onlineService/banr#page=1"+"&plcySeq="+$("#plcySeq").val()+"&perdSttus="+"&svcSeq="+$("#svcSeq").val());
    }
}

