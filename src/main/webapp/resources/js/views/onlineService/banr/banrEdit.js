/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var now=new Date();
var banrSeq = 0;
var plcySeq = 0;
var svcSeq = 0;
var delFileSeqs = 0;
$(document).ready(function() {
    limitInputTitle("banrCampNm,btnTitle,banrTitle,banrSubTitle");

    $("#stDt").val(dateString((new Date(now.getTime()))));
    $("#fnsDt").val(dateString((new Date(now.getTime()))));

    $("#stDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#fnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#fnsDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#stDt").datepicker("option","maxDate", selected)
        }
    });

    setkeyup();

    $("input[name=perdYn]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "Y") {
            $("input[name=perdDt]").attr("disabled",false).css("background-color","#ffffff");
        } else {
            $("input[name=perdDt]").attr("disabled",true).css("background-color","#f5f5f5");
        }
    });

    $("input[name=btnTitleChk]").change(function() {
        var chkVal = $(this).attr("checked");
        if (chkVal == "checked") {
            $("#btnTitle").attr({"disabled":false,"readonly" : false});
        } else {
            $("#btnTitle").attr({"disabled":true,"readonly" : true});
        }
    });

    $("input[name=banrTitleChk]").change(function() {
        var chkVal = $(this).attr("checked");
        if (chkVal == "checked") {
            $("#banrTitle").attr({"disabled":false,"readonly" : false});
        } else {
            $("#banrTitle").attr({"disabled":true,"readonly" : true});
        }
    });

    $("input[name=banrSubTitleChk]").change(function() {
        var chkVal = $(this).attr("checked");
        if (chkVal == "checked") {
            $("#banrSubTitle").attr({"disabled":false,"readonly" : false});
        } else {
            $("#banrSubTitle").attr({"disabled":true,"readonly" : true});
        }
    });


    fileFormInsert("banrImg","BANR_IMG",-1,-1,"banrImg","imgPreview","png,jpg,gif,bmp,jpeg");
    fileFormAllView();

    banrInfo();
});

banrInfo = function(){
    callByGet("/api/banrMng?banrSeq="+$("#banrSeq").val(), "banrInfoSuccess", "NoneForm", "banrInfoFail");
}

banrInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.banrInfo;
        banrSeq = item.banrSeq;
        plcySeq = item.plcySeq;
        svcSeq = item.svcSeq;
        $("#svcNm").text(item.svcNm);
        $("#banrCampNm").text(item.banrCampNm);

        $("#terrNm").text(item.terrNm);
        if (!empty(item.banrImg)) {
            delFileSeqs = item.banrImg.fileSeq;
            $("#imgPreview").html("<img src='"+makeAPIUrl(item.banrImg.imgPath,"img")+"' title=\""+item.banrImg.fileSeq+"\">");
        }

        $("input[type=radio][name=perdYn][value="+item.perdYn+"]").attr("checked", true).css("background-color","#f5f5f5");
        if (item.perdYn == "Y") {
            $("input[name=perdDt]").attr("disabled",false).css("background-color","#ffffff");;
        }
        $("#stDt").val(item.stDt);
        $("#fnsDt").val(item.fnsDt);


        $("#linkUrl").val(item.linkUrl);
        $("#linkUri").val(item.linkUri);

        var useTitleType = item.useTitleType;
        if (useTitleType.indexOf('BT') != -1){
            $("input[name=btnTitleChk]").attr("checked", true);

        } else {
            $("input[name=btnTitleChk]").attr("checked", false);
            $("#btnTitle").attr({"disabled":true,"readonly" : true});
        }
        if (useTitleType.indexOf('BNT') != -1){
            $("input[name=banrTitleChk]").attr("checked", true);

        } else {
            $("input[name=banrTitleChk]").attr("checked", false);
            $("#banrTitle").attr({"disabled":true,"readonly" : true});
        }

        if (useTitleType.indexOf('BST') != -1){
            $("input[name=banrSubTitleChk]").attr("checked", true);
        } else {
            $("input[name=banrSubTitleChk]").attr("checked", false);
            $("#banrSubTitle").attr({"disabled":true,"readonly" : true});
        }
        $("#btnTitle").val(item.btnTitle);
        $("#banrTitle").val(item.banrTitle);
        $("#banrSubTitle").val(item.banrSubTitle);

        $("input[type=radio][name=useYn][value="+item.useYn+"]").attr("checked", true);

        plcyInfo();
        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/banr");
    }
}

banrInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/banr");
}

plcyInfo = function() {
    callByGet("/api/plcyMng?plcySeq="+plcySeq, "plcyInfoSuccess", "NoneForm", "plcyInfoFail");
}

plcyInfoSuccess = function(data) {
    if(data.resultCode == "1000"){
        var item = data.result.plcyInfo;

        for(var i =0; i < fileSet.length; i++){
            var setVar = fileSet[i];
            setVar.lmtHeight = item.sizeHght;
            setVar.lmtWidth = item.sizeWdth;
            setVar.accessType = item.fileTypeALL;
        }
    }
}

plcyInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/onlineService/banr");
}

function makePageMove(e){
    var mvPage = "/onlineService/banr#page=1";
    if ($("#svcSeq").val() != 0) {
        mvPage += "&svcSeq=" + $("#svcSeq").val();
    } else {
        mvPage += "&svcSeq=" + svcSeq;
    }
//    mvPage += "&plcySeq=" + plcySeq;

    pageMove(mvPage);
}

function banrEdit() {
    var nonChgFile = false;
    if (fileSet[0].filePath == "" && $("#imgPreview > img").length == 0) {
        popAlertLayer(getMessage("banr.input.Img.msg"));
        return;
    } else if (fileSet[0].filePath == "") {
        nonChgFile = true;
    }

    if ($("#linkUrl").val().trim() != "" && !validateUrl($("#linkUrl").val())) {
        popAlertLayer(getMessage("common.url.error.msg"));
        return;
    }

    var useTitleType = "";
    if ($("input[name=btnTitleChk]").attr("checked") == "checked") {
        useTitleType += "BT";
        if ($("#btnTitle").val().trim() == "") {
            popAlertLayer(getMessage("banr.use.btnTitle.empty.msg"));
            $("#btnTitle").focus();
            return;
        }
    }
    if ($("input[name=banrTitleChk]").attr("checked") == "checked"){
        if (useTitleType != "") {
            useTitleType +=",";
        }
        useTitleType += "BNT";
        if ($("#banrTitle").val().trim() == "") {
            popAlertLayer(getMessage("banr.use.barnTitle.empty.msg"));
            $("#banrTitle").focus();
            return;
        }
    }
    if ($("input[name=banrSubTitleChk]").attr("checked") == "checked"){
        if (useTitleType != "") {
            useTitleType +=",";
        }
        useTitleType += "BST";
        if ($("#banrSubTitle").val().trim() == "") {
            popAlertLayer(getMessage("banr.use.banrSubTitle.empty.msg"));
            $("#banrSubTitle").focus();
            return;
        }
    }
    $("#comnty_write_from > input[type=hidden]").remove();


    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("comnty_write_from", "banrSeq", banrSeq);
        formData("comnty_write_from", "perdYn", $("input[name=perdYn]:checked").val());
        formData("comnty_write_from", "stDt", $("#stDt").val());
        formData("comnty_write_from", "fnsDt", $("#fnsDt").val());
        formData("comnty_write_from", "linkUrl", $("#linkUrl").val());
        formData("comnty_write_from", "linkUri", $("#linkUri").val());
        formData("comnty_write_from", "useTitleType", useTitleType);
        formData("comnty_write_from", "btnTitle", $("#btnTitle").val());
        formData("comnty_write_from", "banrTitle", $("#banrTitle").val());
        formData("comnty_write_from", "banrSubTitle", $("#banrSubTitle").val());
        formData("comnty_write_from", "useYn", $("input[name=useYn]:checked").val());

        if (!nonChgFile) {
            formData("comnty_write_from", "delFileSeqs", delFileSeqs);
        }
        formData("comnty_write_from", "_method", "PUT");
        callByMultipart("/api/banrMng", "banrUpdateSuccess", "comnty_write_from", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data){
    if (data.resultCode == "1500") {
        if (data.resultMsg == "Over File Size") {
            popAlertLayer(getMessage("info.upload.fileSize.over.msg"));
        } else {
            popAlertLayer(getMessage("info.upload.failed.msg"));
        }
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();

}

banrUpdateSuccess = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"),"/onlineService/banr#page=1"+"&plcySeq="+plcySeq+"&perdSttus="+"&svcSeq="+svcSeq);
    } else {
        if (data.resultMsg == "Over File Size") {
            popAlertLayer(getMessage("info.upload.fileSize.over.msg"));
        } else {
            popAlertLayer(getMessage("fail.common.update"));
        }
        $("#comnty_write_from > input[type=hidden]").remove();
    }

}

setPlcyFile = function(){
    for(var i =0; i < fileSet.length; i++){
        var setVar = fileSet[i];
        setVar.lmtHeight = $("#banrInPlcyList option:selected").data("sizehght");
        setVar.lmtWidth = $("#banrInPlcyList option:selected").data("sizewdth");
        setVar.accessType = $("#banrInPlcyList option:selected").data("filetype");
    }
}


