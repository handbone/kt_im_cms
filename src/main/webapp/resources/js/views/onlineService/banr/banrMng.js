/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var apiUrl = makeAPIUrl("/api/banrMng");
var lastBind = false;
var gridState;
var hashSvc;
var hashPerdSttus;
var hashPlcy;
var totalPage = 0;
var noneData = false;
var delChk = false;
var resultReset = false;
$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
});

$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            gridState = "HASH";
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashPerdSttus = checkUndefined(findGetParameter(str_hash,"perdSttus"));
            hashPlcy = checkUndefined(findGetParameter(str_hash,"plcySeq"));
            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#svcList").val(hashSvc);
            $("#statusList").val(hashPerdSttus);

            banrInPlcyList();
        } else if (gridState == "GRIDCOMPLETE") {
            var str_hash = document.location.hash.replace("#","");
            var searchStr = checkUndefined(findGetParameter(str_hash,"searchString"));
            if (checkUndefined(findGetParameter(str_hash,"searchString")) == null) {
                searchStr = "";
            }
            if (!resultReset) {
                if (searchStr != $("#keyword").val()) {
                    $("#keyword").val(checkUndefined(findGetParameter(str_hash,"searchString")));
                    keywordSearch();
                }
            }
            resultReset = false;
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
        hashPerdSttus = checkUndefined(findGetParameter(str_hash,"perdSttus"));
        hashPlcy= checkUndefined(findGetParameter(str_hash,"plcySeq"));

        $("#target").val(searchField);
        $("#keyword").val(searchString);
        $("#statusList").val(hashPerdSttus);
    }

    setOlSvcList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });

    if ($("#memberSec").val() == "05" ) {
        // 05 : 매장 관리자, 등록 및 삭제 불가, 조회만 가능
        $(".btnDelete").hide();
        $(".btnWrite").hide();
    }
});

/* 초기 서비스 영역 SET*/
setOlSvcList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON", "setOlSvcListSuccess", '', "failOlSvcList");
}
/* 초기 서비스 영역 DATA SUCESS */
setOlSvcListSuccess = function(data){
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#svcList").html("");
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + parseInt(item.comnCdValue) + "\">" + item.comnCdNm + "</option>";
    });
    $("#svcList").html(serviceListHtml);
    if ($("#memberSec").val() == "04" ) {
        // 05 : 매장 관리자, 서비스 변경 불가
        $("#svcList").attr('disabled', 'true');
        $("#svcList").val($("#svcSeq").val());
    } else if (hashSvc != null && hashSvc != "") {
        $("#svcList").val(hashSvc);
    }

    gridState = "NONE";

    banrInPlcyList();
}
/* 초기 서비스 영역 DATA FAIL */
failOlSvcList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#svcList").html("");
}


banrInPlcyList = function(){
    callByGet("/api/plcyMng?page=-1&rows=1&svcSeq="+$("#svcList").val(),"banrPlcyListSuccess","NoneForm","failPlcyList");

}

banrPlcyListSuccess = function(data){
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noPlcy.msg");
        }
        popAlertLayer(msg);
        $("#banrInPlcyList").html("");
        noneData = false;
        banrList();
        return;
    }

    var plcyListHtml = "<option value='0'>전체</option>";
    $(data.result.plcyList).each(function(i, item) {
        plcyListHtml += "<option value=\"" + item.plcySeq + "\">" + item.terrNm + "</option>";
    });
    $("#banrInPlcyList").html(plcyListHtml);

    if (hashPlcy != null && hashPlcy != "") {
        $("#banrInPlcyList").val(hashPlcy);
    }

    noneData = true;
    banrList();


}

failPlcyList = function(data){
    popAlertLayer(getMessage("fail.common.select"));
    $("#banrInPlcyList").html("");
}

function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {

        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');
        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "banrCampNm":break;
            default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
        });
        $("#svcList").html(svcListHtml);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }
    }

}

var stateVal;

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

function banrList() {
    $("#jqgridData").jqGrid({
        url: apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.banrList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:[
                getMessage("column.title.num"),
                getMessage("banr.campainNm.noSpace"),
                getMessage("common.position"),
                getMessage("common.regist.image.noSpace"),
                getMessage("common.display.period.noSpace"),
                getMessage("table.reger"),
                getMessage("table.regdate"),
                getMessage("table.status"),
                getMessage("column.title.perdYn"),
                getMessage("column.title.imgPath"),
                getMessage("column.title.banrSeq"),
                "isImgFile"
         ],
        colModel:[
            {name:"num", index: "BANR_SEQ", align:"center", width:30},
            {name:"banrCampNm", index:"BANR_CAMP_NM", align:"center",formatter:pointercursor},
            {name:"terrNm", index:"TERR_NM", align:"center"},
            {name:"banrImgs", index:"BNAR_IMGS", align:"center",sortable:false},
            {name:"perdDt", index:"PERD_DT", align:"center"},
            {name:"cretrNm", index:"CRETR_NM", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"perdSttus", index:"PERD_STTUS", align:"center"},
            {name:"perdYn", index:"PERD_YN", hidden:true},
            {name:"banrImg.imgPath", index:"IMG_PATH", hidden:true},
            {name:"banrSeq", index:"BANR_SEQ", hidden:true},
            {name:"isImgFile", index:"IS_IMG_FILE", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "BANR_SEQ",
            sortorder: "desc",
//          caption:"편성 목록",
            height: "auto",
            multiselect: true,
            beforeRequest:function(){
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                if (!noneData) {
                    page = 0;
                } else {
                    if (gridState == "GRIDCOMPLETE") {
                        page = myPostData.page;
                    } else if (gridState != "READY") {
                        if (isNaN(page) || (page == 0 && noneData)) {
                            page =1;
                         }

                         if (totalPage > 0 && totalPage < page) {
                             page = 1;
                         }
                    } else {
                        page = myPostData.page;
                    }

                }

                hashSvc = checkUndefined($("#svcList").val());
                hashPerdSttus = checkUndefined($("#statusList").val());
                hashPlcy = checkUndefined($("#banrInPlcyList").val());
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

                if (gridState != "SEARCH") {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                }


                searchField = checkUndefined($("#target").val());
                searchString = checkUndefined($("#keyword").val());

                if(gridState == "NONE"){
                    searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                    searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                    myPostData.page = page;

                    if (searchField != null) {
                        tempState = "SEARCH";
                    } else {
                        tempState = "READY";
                    }
                }

                if(searchField != null && searchField != "" && searchString != "" && !resultReset){
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                }else{
                    if (resultReset) {
                        $("#target").val("");
                        $("#keyword").val("");
                    }
                    delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
                }

                if (hashSvc != null && hashSvc != "") {
                    myPostData.svcSeq = hashSvc;
                } else {
                    myPostData.svcSeq = "";
                }

                if (hashPerdSttus != null && hashPerdSttus != "") {
                    myPostData.perdSttus = hashPerdSttus;
                } else {
                    myPostData.perdSttus = "";
                }

                if (hashPlcy != null && hashPlcy != "") {
                    myPostData.plcySeq = hashPlcy;
                } else {
                    myPostData.plcySeq = "";
                }

                myPostData.perdSttus = $("#statusList").val();
                myPostData.page = page;

                if(resultReset){
                    myPostData.page = 1;
                }

                $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: page });
            },
            loadComplete: function (data) {
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

              //session
                if(sessionStorage.getItem("last-url") != null){
                    sessionStorage.removeItem('state');
                    sessionStorage.removeItem('last-url');
                }

                /* 정렬 하는 컬럼 Header 만  마우스 커서 pointer S*/
                var grids = $("#jqgridData");
                var cm = grids[0].p.colModel;
                $.each(grids[0].grid.headers, function(index, value) {
                    var cmi = cm[index], colName = cmi.name;
                    if(!cmi.sortable) {
                        $('div.ui-jqgrid-sortable',value.el).css({cursor:"default"});
                    }
                });
                /* 정렬 하는 컬럼 Header 만  마우스 커서 pointer E*/

                //Search 필드 등록
                searchFieldSet();
                var nPage =$(this).context.p.page;
                if (data.resultCode == 1000) {
                    totalPage = data.result.totalPage;

                    if (page != data.result.currentPage) {
                        tempState = "";
                    }
                } else {
                    tempState = "";
                    if (delChk) {
                        delChk = false;
                        if (nPage == 1 || nPage == 0) {
                            nPage = 1;
                            jQuery("#jqgridData").trigger("reloadGrid");
                        }
                    }
                }


                /*뒤로가기*/
                var hashlocation = "page="+nPage+"&svcSeq="+$("#svcList option:selected").val()+"&perdSttus="+$("#statusList").val()+"&plcySeq="+$("#banrInPlcyList option:selected").val();
                hashlocation = hashlocation.replace("undefined","");

                if($(this).context.p.postData._search && $("#target").val() != null){
                    hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
                }

                gridState = "GRIDCOMPLETE";

                document.location.hash = hashlocation;

                if (!resultReset) {
                    if (tempState != "" || str_hash == hashlocation) {
                        gridState = "READY";
                    }
                }

                //표기 형식 변경
                $(this).find("td").each(function() {
                   if ($(this).index() == 4) {
                       $(this).addClass("imgDisplay");
                        if ($(this).parents("tr").index() > 0) {
                            var imgPaths = $("#jqgridData").jqGrid('getRowData', $(this).parents("tr").index())["banrImg.imgPath"];
                            var isFile = $("#jqgridData").jqGrid('getRowData', $(this).parents("tr").index()).isImgFile;
                            if (isFile == "true") {
                                $(this).html("<a href='javascript:openPopups3("+$(this).parents("tr").index()+");'>" +
                                        "<img style='background-size: 100% 100%; background-repeat: no-repeat; background-position: center;" +
                                        " background-image:url("+ makeAPIUrl(imgPaths,"img") + ");' title=''></a>");
                            } else {
                                $(this).html("<a href='javascript:openPopups3("+$(this).parents("tr").index()+");'>" +
                                        "<img style='background-size: 100% 100%; background-repeat: no-repeat; background-position: center;" +
                                        " background-image:url(" + $('#contextPath').val() + "/resources/image/img_empty.png);' title=''></a>");
                            }
                        }
                   } else if ($(this).index() == 5) {
                       if ($(this).parents("tr").index() > 0) {
                           var perdYn = $("#jqgridData").jqGrid('getRowData', $(this).parents("tr").index()).perdYn;
                           if (perdYn == "N"){
                               $(this).text(getMessage("common.unlimit.word"));

                           } else {
                               var str = $(this).text().split("~");
                               $(this).html("<p style='display: inline-block;'>"+str[0].trim()+"</p>"+"<p style='display: inline-block;'> ~ "+str[1].trim()+"</p>");
                           }

                       }
                    } else if ($(this).index() == 8) {
                        var str = $(this).text();

                        if (str != '') {
                            switch(str) {
                            case 'OVER_OUT': str = getMessage("common.sttus.overOut");
                                break;
                            case 'WAIT': str = getMessage("common.sttus.wait");
                                break;
                            case 'DISPLAY': str = getMessage("common.sttus.display");
                                break;
                            case 'STOP': str =  getMessage("common.sttus.stop");
                                break;
                            }

                            $(this).text(str);
                        }
                    }
                });

                //pageMove Max
                $('.ui-pg-input').on('keyup', function() {
                    this.value = this.value.replace(/\D/g, '');
                    if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
                });

                $(this).find(".pointer").parent("td").addClass("pointer");
                resizeJqGridWidth("jqgridData", "gridArea");
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event){
                // 콘텐츠 ID 선택시
                if(columnId == 2){
                    var list = $("#jqgridData").jqGrid('getRowData', rowId);
                    sessionStorage.setItem("last-url", location);
                    sessionStorage.setItem("state", "view");
                    pageMove("/onlineService/banr/"+list.banrSeq);
                } else if (columnId == 4) { // 등록 이미지 컬럼 선택 시 해당 row 체크되지 않도록 함.
                    $("#jqgridData").jqGrid('setSelection', rowId).prop('checkbox', false);
                }
            }
    }).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}


deleteBanr = function(){
    var delSeqs = "";
    var delNmList = "";
    var delCnt = 0;

    for (var i = 1; i<$("#limit").val()+1; i++) {
        if ($("#jqg_jqgridData_" + i).prop("checked")) {
            var selBanrSeq = $("#jqgridData").jqGrid('getRowData', i).banrSeq;
            var selBanrCampNm = $("#jqgridData").jqGrid('getRowData', i).banrCampNm;
            delSeqs += selBanrSeq + ",";
            delNmList += selBanrCampNm + ", "
            delCnt++;
        }
    }

    if (delCnt == 0) {
        popAlertLayer(getMessage("common.delete.not.select.msg"));
        return;
    }
//    delNmList.slice(0, -2) + product.delete.confirm.msg
    popConfirmLayer(getMessage("common.delete.msg"), function() {
        formData("NoneForm" , "delSeqs", delSeqs.slice(0, -1));
        callByDelete("/api/banrMng", "deleteBanrSuccess", "NoneForm","deleteFail");
    }, null, getMessage("common.confirm"));

}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteBanrSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"));
        formDataDeleteAll("NoneForm");
        delChk = true;
        jQuery("#jqgridData").trigger("reloadGrid");
    }
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function makePageMove(e){
    if ($("#banrInPlcyList option").index() != -1) {
        pageMove("/onlineService/banr/regist#svcSeq="+$("#svcList").val()+"&plcySeq="+$("#banrInPlcyList").val());
    } else {
        popAlertLayer(getMessage("common.noPlcy.msg"));
    }
}

function pageReset(){
    resultReset = true;
}

