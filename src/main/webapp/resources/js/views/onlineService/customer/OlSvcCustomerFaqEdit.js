/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var faqType; // svcSeq
var targetType;
var faqCtg;

$(document).ready(function(){
    limitInputTitle("title");

    setkeyup();

    $("#regNm").html($("#memberNm").val());

    getSvcFaqInfo();
});

getSvcFaqInfo = function() {
    formData("NoneForm", "svcFaqSeq", $("#svcFaqSeq").val());
    formData("NoneForm", "searchType", "edit");
    callByGet("/api/svcFaq?svcType=ON", "svcFaqInfoSuccess", "NoneForm", "svcFaqInfoFail");
}

svcFaqInfoSuccess = function(data) {
    formDataDeleteAll("NoneForm");

    if (data.resultCode != "1000") {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#currSvcSeq").val());
        return;
    }

    var faqDetail = data.result.faqDetail;
    faqType = faqDetail.svcFaqType;
    targetType = faqDetail.targetType;
    faqCtg = faqDetail.svcFaqCtg

    $("#title").val(faqDetail.svcFaqTitle);

    var strFaqSbst = strConv(faqDetail.svcFaqSbst);
    strFaqSbst = strConv(strFaqSbst);
    CKEDITOR.instances.sbst.setData(strFaqSbst);

    if (faqDetail.delYn == "N") {
        $("input[name=useYn]:input[value='Y']").attr("checked", true);
    } else {
        $("input[name=useYn]:input[value='N']").attr("checked", true);
    }

    listBack($(".btnCancel"));

    getServiceList();
}

svcFaqInfoFail = function() {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#currSvcSeq").val());
}

getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        $(".btnModify").hide();
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#memberSvcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#memberSvcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#memberSvcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        $("#serviceList").val(faqType);
    }

    getTargetTypeList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
    $(".btnModify").hide();
}

getTargetTypeList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_TARGET_SE", "didReceiveTargetTypeList", '', "didNotReceiveTargetTypeList");
}

didReceiveTargetTypeList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.no.target.type.msg");
        }
        popAlertLayer(msg);
        $("#targetTypeList").html("");
        $(".btnModify").hide();
        return;
    }

    var listHtml = "";
    $(data.result.codeList).each(function(i, item) {
        listHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#targetTypeList").html(listHtml);
    $("#targetTypeList").val(targetType);

    getFaqCategoryList();
}

didNotReceiveTargetTypeList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#targetTypeList").html("");
    $(".btnModify").hide();
}

getFaqCategoryList = function() {
    callByGet("/api/codeInfo?comnCdCtg=OLSVC_FAQ_CTG", "faqCategoryListSuccess","NoneForm", "faqCategoryListFail");
}

faqCategoryListSuccess = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.no.ctg.msg");
        }
        popAlertLayer(msg);
        $("#faqCtgList").html("");
        $(".btnModify").hide();
        return;
    }

    var faqCategoryListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        faqCategoryListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#faqCtgList").html(faqCategoryListHtml);
    $("#faqCtgList").val(faqCtg);
}

faqCategoryListFail = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#faqCtgList").html("");
    $(".btnModify").hide();
}

function updateFaq() {
    if ($("#title").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.sbst.getData().length < 1) {
        popAlertLayer(getMessage("common.content.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        CKEDITOR.instances.sbst.updateElement();
        $("#sbst").val(CKEDITOR.instances.sbst.getData());

        formData("NoneForm", "svcFaqSeq", $("#svcFaqSeq").val());
        formData("NoneForm", "svcFaqType", $("#serviceList option:selected").val());
        formData("NoneForm", "targetType", $("#targetTypeList option:selected").val());
        formData("NoneForm", "svcFaqCtg", $("#faqCtgList option:selected").val());
        formData("NoneForm", "svcFaqTitle", $("#title").val());
        formData("NoneForm", "svcFaqSbst", $("#sbst").val());

        var isPost = "N";
        if ($("input:radio[name=useYn]:checked").val() == "Y") {
            isPost = "Y";
        }
        formData("NoneForm", "isPost", isPost);

        callByPut("/api/svcFaq?svcType=ON", "updateSvcFaqSuccess", "NoneForm", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateSvcFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#serviceList option:selected").val());
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

updateFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

function returnToPage() {
    pageMove("/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#currSvcSeq").val());
}
