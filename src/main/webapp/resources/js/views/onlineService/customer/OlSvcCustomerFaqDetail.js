/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    svcFaqInfo();
});

svcFaqInfo = function() {
    formData("NoneForm", "svcFaqSeq", $("#svcFaqSeq").val());
    callByGet("/api/svcFaq?svcType=ON", "svcFaqInfoSuccess", "NoneForm", "svcFaqInfoFail");
}

svcFaqInfoSuccess = function(data) {
    formDataDelete("NoneForm", "svcFaqSeq");
    if (data.resultCode != "1000") {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#currSvcSeq").val());
        return;
    }

    var faqDetail = data.result.faqDetail;
    $("#regNm").html(faqDetail.cretrNm);
    $("#svcNm").html(faqDetail.svcFaqTypeNm);
    $("#regDt").html(faqDetail.regDt);
    $("#targetType").html(faqDetail.targetTypeNm);
    $("#faqCtg").html(faqDetail.svcFaqCtgNm);
    $("#title").html(faqDetail.svcFaqTitle);
    $("#sbst div").html(faqDetail.svcFaqSbst);
    //$("#sbst div").html(xssChk(faqDetail.svcFaqSbst));
    //$("#sbst div").html($("#sbst").text());

    if (faqDetail.delYn == "N") {
        $("input[name=useYn]:input[value='Y']").attr("checked", true);
    } else {
        $("input[name=useYn]:input[value='N']").attr("checked", true);
    }

    // 마스터 관리자, CMS 관리자 또는 동일 서비스의 서비스 관리자가 쓴 글은 편집 가능
    if ($("#memberSec").val() == '01' || $("#memberSec").val() == '02' || faqDetail.cretrMbrSe == $("#memberSec").val()) {
        $("#btnModify").show();
        $("#btnDelete").show();
    }

    listBack($(".btnList"));
}

svcFaqInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#currSvcSeq").val());
}

function deleteFaq() {
    popConfirmLayer(getMessage("faq.delete.confirm.msg"), function() {
        formData("NoneForm" , "delSvcFaqSeqList", $("#svcFaqSeq").val());
        callByDelete("/api/svcFaq?svcType=ON", "deleteSvcFaqSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteSvcFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#currSvcSeq").val());
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

moveToEdit = function() {
    pageMove("/onlineSvc/customer/faq/" + $("#svcFaqSeq").val() + "/edit?svcSeq=" + $("#currSvcSeq").val());
}
