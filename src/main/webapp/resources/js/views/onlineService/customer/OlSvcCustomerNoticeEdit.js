/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var svcList = null;
var appType = "";
var noticeType;
var targetType;
var delPopupImgFileSeqs = "";
var delAttachFileSeqs = "";
var delFileSeqs = "";
var hasFile = false; // 콘솔 로그 확인용
var stDt = "";
var fnsDt = "";

var ATTCH_FILE;
var FILE_SETTINGS   = {
        // Backend Settings
//      upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),
        post_params: {},


        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
};

$(document).ready(function(){
    limitInputTitle("title");

    var now=new Date();
    $("#startDt").val(dateString((new Date(now.getTime()))));
    $("#endDt").val(dateString((new Date(now.getTime()))));

    $("#startDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#endDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#endDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#startDt").datepicker("option","maxDate", selected)
        }
    });

    setkeyup();

    getSvcNoticeInfo();

    attachFileUploadBtn();

    /* 노출/비노출인 경우 사용 코드
    fileFormInsert("popupImg", "NOTI_POPUP", -1, -1, "popupImg", "imgPreview", "png,jpg,bmp,jpeg");
    fileFormAllView();

    $("input:checkbox[id='popup']").change(function() {
        if ($("input:checkbox[id='popup']").is(":checked")) {
            $("#popupImgFile").show();
            $("#popupUrlInput").show();
            $("#popupPerdDt").show();
        } else {
            $("#popupImgFile").hide();
            $("#popupUrlInput").hide();
            $("#popupPerdDt").hide();
        }
    });
    */

    fileFormInsert("popupImg", "NOTI_POPUP", -1, -1, "popupImg", "imgPreview", "png,jpg,bmp,jpeg");
    fileFormAllView();
    $(".inputFile").attr("disabled", true);

    $("input:checkbox[id='popup']").change(function() {
        if ($("input:checkbox[id='popup']").is(":checked")) {
            $(".inputFile").attr("disabled", false);
            $("#popupUrl").attr("disabled", false);
            $("#popupUrl").attr("readonly", false);
            $("input:radio[name='popupPerd']").attr("disabled", false);
        } else {
            $(".inputFile").attr("disabled", true);
            $("#popupUrl").attr("disabled", true);
            $("#popupUrl").attr("readonly", true);
            $("input:radio[name='popupPerd']").attr("disabled", true);

            // 팝업 체크 해제 시 작성된 내용 초기화
            fileReSetPath(0,"",$("input[type=file][class=inputFile]"),"",false);
            $("input[type=file][class=inputFile]").trigger("onchange");

            $("#popupUrl").val("");
            $("#startDt").val(dateString((new Date(now.getTime()))));
            $("#endDt").val(dateString((new Date(now.getTime()))));
            $("input:radio[name=popupPerd][value=N]").attr("checked", true);
            $("#startDt").attr("disabled", true);
            $("#endDt").attr("disabled", true);
        }
    });

    $("input:radio[name=popupPerd]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "Y") {
            $("#startDt").attr("disabled", false);
            $("#endDt").attr("disabled", false);
        } else {
            $("#startDt").attr("disabled", true);
            $("#endDt").attr("disabled", true);
        }
    });

    listBack($(".btnCancel"));
});

getSvcNoticeInfo = function() {
    formData("NoneForm", "svcNoticeSeq", $("#svcNoticeSeq").val());
    formData("NoneForm", "searchType", "edit");
    formData("NoneForm", "svcType", "ON");
    callByGet("/api/svcNotice", "infoSuccess", "NoneForm", "infoFail");
}

infoSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode != "1000") {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/customer#page=1&display=notice&svcSeq=" + $("#currSvcSeq").val());
        return;
    }

    var noticeDetail = data.result.noticeDetail;
    $("#regNm").html(noticeDetail.cretrNm);

    noticeType = noticeDetail.svcSeq;

    $("#dispTypeList").val(noticeDetail.dispType);

    targetType = noticeDetail.targetType;

    $("#rankList").val(noticeDetail.prefRank);
    $("#title").val(noticeDetail.orignlNoticeTitle);

    var strNoticeSbst = strConv(noticeDetail.noticeSbst);
    strNoticeSbst = strConv(strNoticeSbst);
    CKEDITOR.instances.sbst.setData(strNoticeSbst);

    if (noticeDetail.popupViewYn == "Y") {
        $("input:checkbox[id='popup']").prop("checked", true);
        // 팝업 이미지는 하나만 존재함.
        if (data.result.popupImgFileList.length > 0) {
            $(data.result.popupImgFileList).each(function(i,item) {
                delPopupImgFileSeqs = item.fileSeq + ",";
                $("#imgPreview").html("<img src='"+makeAPIUrl(item.filePath,"img")+"' title=\""+item.fileSeq+"\">");
            });
        }

        $(".inputFile").attr("disabled", false);

        $("#popupUrl").val(noticeDetail.popupUrl);
        $("#popupUrl").attr("disabled", false);
        $("#popupUrl").attr("readonly", false);

        $("input:radio[name='popupPerd']").attr("disabled", false);
        $("input[type=radio][name=popupPerd][value="+noticeDetail.popupPerdYn+"]").attr("checked", true);
        if (noticeDetail.popupPerdYn == "Y") {
            $("#startDt").val(noticeDetail.popupViewStDt);
            $("#startDt").attr("disabled", false);
            $("#endDt").val(noticeDetail.popupViewFnsDt);
            $("#endDt").attr("disabled", false);

            if (noticeDetail.popupViewStDt != "") {
                stDt = noticeDetail.popupViewStDt
            }

            if (noticeDetail.popupViewFnsDt != "") {
                fnsDt = noticeDetail.popupViewFnsDt
            }
        }
        $("#popupPerdDt").show();
    }

    if (noticeDetail.postViewYn == 'Y') {
        $("input:checkbox[id='post']").prop("checked", true);
    }

    if (noticeDetail.delYn == "Y") {
        $("input[name='useYn'][value='N']").attr("checked", true);
    } else {
        $("input[name='useYn'][value='Y']").attr("checked", true);
    }

    var fileListHtml = "";
    $(data.result.attachFileList).each(function(i, item) {
        fileListHtml += "<table class=\"fileTable\"><tr><td><p class=\"fileName\" title=\"" + item.fileName
                    + "\">" + getMessage("common.filename") + " : "+ item.fileName + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\""
                    + " onclick=\"savedfileDelete(this, " + item.fileSeq + ")\" value=\"" + getMessage("button.delete") + "\"></td></tr></table>";
    });
    $("#fileArea").html(fileListHtml);

    getServiceList();

    listBack($(".btnCancel"));
}

infoFail = function() {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/customer#page=1&display=notice&svcSeq=" + $("#currSvcSeq").val());
}

getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        $(".btnWrite").hide();
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#memberSvcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#memberSvcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#memberSvcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        $("#serviceList").val(noticeType);
    }

    svcList = data.result.codeList;
    $(data.result.codeList).each(function(i, item) {
        if (item.comnCdValue == $("#serviceList").val()) {
            appType = item.appType;
            return false;
        }
    });

    if (appType == "MA") {
        $("#dispType").show();
        $("dispTypeList").val(dispType);
    } else {
        $("#dispType").hide();
    }

    getTargetTypeList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
    $(".btnWrite").hide();
}

getTargetTypeList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_TARGET_SE", "didReceiveTargetTypeList", '', "didNotReceiveTargetTypeList");
}

didReceiveTargetTypeList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.no.target.type.msg");
        }
        popAlertLayer(msg);
        $("#targetTypeList").html("");
        $(".btnWrite").hide();
        return;
    }

    var listHtml = "";
    $(data.result.codeList).each(function(i, item) {
        listHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#targetTypeList").html(listHtml);
    $("#targetTypeList").val(targetType);
}

didNotReceiveTargetTypeList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#targetTypeList").html("");
    $(".btnWrite").hide();
}

function chgService() {
    var svcSeq = $("#serviceList").val();
    $(svcList).each(function(i, item) {
        if (item.comnCdValue == svcSeq) {
            appType = item.appType;
            return false;
        }
    });

    if (appType == "MA") {
        $("#dispType").show();
    } else {
        $("#dispType").hide();
    }
}

//swfupload - attch_file
attachFileUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'svcNotice'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.pdf;*.hwp;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.png;*.jpg;*.jpeg;*.bmp;*.gif",
    file_settings.file_queue_limit =  "0";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    ATTCH_FILE  = new SWFUpload(file_settings);
}

savedfileDelete = function(e, fileSeq){
    $(e).parents("table.fileTable").remove();
    delAttachFileSeqs += fileSeq + ",";
}

fileQueueDelete = function(e, fileId){
    $(e).parents("table.fileTable").remove();
    fileTemp.cancelUpload(fileId);
}

function updateNotice() {
    if ($("#title").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if (!$("input:checkbox[id='popup']").is(":checked") && CKEDITOR.instances.sbst.getData().length < 1) {
        popAlertLayer(getMessage("common.content.empty.msg"));
        return;
    }

    if ($("input:checkbox[id='popup']").is(":checked") && CKEDITOR.instances.sbst.getData().length < 1
            && fileSet[0].filePath == "" && $("#imgPreview > img").length == 0) {
        popAlertLayer(getMessage("service.popup.content.and.file.empty.msg"));
        return;
    }

    if ($("input:checkbox[id='popup']").is(":checked") && $("#popupUrl").val().trim() != "" && !validateUrl($("#popupUrl").val())) {
        popAlertLayer(getMessage("service.popup.url.error.msg"));
        $("#popupUrl").focus();
        return;
    }

    /*
    if (fileSet[0].filePath == "" && $("#imgPreview > img").length == 0) {
        popAlertLayer(getMessage("banr.input.Img.msg"));
        return;
    }
    */

    var chgFile = false;
    if (fileSet[0].filePath != "") {
        chgFile = true;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        CKEDITOR.instances.sbst.updateElement();
        $("#sbst").val(CKEDITOR.instances.sbst.getData());

        formData("comnty_write_from", "svcNoticeSeq", $("#svcNoticeSeq").val());
        formData("comnty_write_from", "svcNoticeType", $("#serviceList option:selected").val());

        // 해당 서비스에서 미러링앱 여부가 N일 경우 기본값으로 SA로 저장
        var dispType = "SA";
        if (appType == "MA") {
            dispType = $("#dispTypeList option:selected").val();
        }
        formData("comnty_write_from", "dispType", dispType);

        formData("comnty_write_from", "targetType", $("#targetTypeList option:selected").val());
        formData("comnty_write_from", "prefRank", $("#rankList option:selected").val());
        formData("comnty_write_from", "svcNoticeTitle", $("#title").val());
        formData("comnty_write_from", "svcNoticeSbst", $("#sbst").val());

        var postViewYn = "N";
        if ($("input:checkbox[id='post']").is(":checked")) {
            postViewYn = "Y";
        }
        formData("comnty_write_from", "postViewYn", postViewYn);

        var popupViewYn = "N";
        if ($("input:checkbox[id='popup']").is(":checked")) {
            popupViewYn = "Y";
            formData("comnty_write_from", "popupViewYn", popupViewYn);

            // 수정화면에서 기존 데이터에 popup 이미지가 존재할 경우 delPopupImgFileSeqs이 기본적으로 값을 가지게 되므로 해당 값을 조건으로 사용
            if (delPopupImgFileSeqs != "") {
                // 이전 등록된 파일이 있었을 경우 변경사항
                if (chgFile) {
                    formData("comnty_write_from", "chgFile", "Y");
                } else {
                    formData("comnty_write_from", "chgFile", "N");
                    delPopupImgFileSeqs = "";
                }
            } else {
                // 신규로 팝업 이미지 파일을 등록할 경우 변경사항
                if (fileSet[0].filePath != "") {
                    hasFile = true;
                    formData("comnty_write_from", "hasFile", "Y");
                } else {
                    formData("comnty_write_from", "hasFile", "N");
                }
            }

            if ($("#popupUrl").val().trim().length > 0) {
                formData("comnty_write_from", "popupUrl", $("#popupUrl").val());
            }

            if ($("input:radio[name=popupPerd]:checked").val() == "Y") {
                formData("comnty_write_from", "popupPerdYn", "Y");
                formData("comnty_write_from", "popupViewStDt", $("#startDt").val() + " 00:00:00");
                formData("comnty_write_from", "popupViewFnsDt", $("#endDt").val() + " 23:59:59");
            } else {
                formData("comnty_write_from", "popupPerdYn", "N");
            }
        } else {
            formData("comnty_write_from", "popupViewYn", popupViewYn);
            formData("comnty_write_from", "chgFile", "N");
            formData("comnty_write_from", "hasFile", "N");
            formData("comnty_write_from", "popupUrl", "");
            formData("comnty_write_from", "popupPerdYn", "");
            if (stDt != "") {
                formData("comnty_write_from", "popupViewStDt", "0000-00-00 00:00:00");
            }
            if (fnsDt != "") {
                formData("comnty_write_from", "popupViewFnsDt", "0000-00-00 00:00:00");
            }
        }

        var isPost = "N";
        if ($("input:radio[name=useYn]:checked").val() == "Y") {
            isPost = "Y";
        }
        formData("comnty_write_from", "isPost", isPost);

        delFileSeqs = delPopupImgFileSeqs + delAttachFileSeqs;
        formData("comnty_write_from", "delFileSeqs", delFileSeqs.slice(0, -1));

        formData("comnty_write_from", "_method", "PUT");
        formData("comnty_write_from", "svcType", "ON");
        callByMultipart("/api/svcNotice", "updateSvcNoticeSuccess", "comnty_write_from", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data) {
    if (!fileAccss && typeof data.resultCode == "undefined") {
        popAlertLayer(getMessage("common.check.file.Position"));
    } else if (data.resultCode == "1500") {
        popAlertLayer(getMessage("info.upload.failed.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();

    if (fileTemp != undefined) {
        while (fileTemp.getStats().files_queued != 0) {
            fileTemp.cancelUpload();
        }
    }
}

updateSvcNoticeSuccess = function(data) {
    if (data.resultCode == "1000") {
        $("#contsSeq").val($("#svcNoticeSeq").val());
        uploadAttachFiles();
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();
}

uploadAttachFiles = function() {
    if (fileTemp != undefined) {
        if (Brower.safari || Brower.safari) {
            fileTemp.addPostParam("id",$("#id").val());
            fileTemp.addPostParam("otpCreateTime",$("#otpCreateTime").val());
        }
        fileTemp.startUpload();
    } else {
        popAlertLayer(getMessage("success.common.update"), "/onlineSvc/customer#page=1&display=notice&svcSeq=" + $("#serviceList option:selected").val());
    }
}

fileFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("msg.common.file.upload.fail"));
}

fileInsertSuccess = function(data) {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertFail = function() {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertSuccessPopup = function() {
    var msg = getMessage("success.common.update");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }

    popAlertLayer(msg, "/onlineSvc/customer#page=1&display=notice&svcSeq=" + $("#serviceList option:selected").val());
}

function returnToPage() {
    pageMove("/onlineSvc/customer#page=1&display=notice&svcSeq=" + $("#currSvcSeq").val());
}
