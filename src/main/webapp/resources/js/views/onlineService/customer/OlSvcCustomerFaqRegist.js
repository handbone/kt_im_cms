/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    limitInputTitle("title");

    setkeyup();

    $("#regNm").html($("#memberNm").val());

    getServiceList();
});

getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        $(".btnWrite").hide();
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#memberSvcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#memberSvcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#memberSvcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        $("#serviceList").val($("#currSvcSeq").val());
    }

    getTargetTypeList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
    $(".btnWrite").hide();
}

getTargetTypeList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_TARGET_SE", "didReceiveTargetTypeList", '', "didNotReceiveTargetTypeList");
}

didReceiveTargetTypeList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.no.target.type.msg");
        }
        popAlertLayer(msg);
        $("#targetTypeList").html("");
        $(".btnWrite").hide();
        return;
    }

    var listHtml = "";
    $(data.result.codeList).each(function(i, item) {
        listHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#targetTypeList").html(listHtml);

    getFaqCategoryList();
}

didNotReceiveTargetTypeList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#targetTypeList").html("");
    $(".btnWrite").hide();
}

getFaqCategoryList = function() {
    callByGet("/api/codeInfo?comnCdCtg=OLSVC_FAQ_CTG", "faqCategoryListSuccess","NoneForm", "faqCategoryListFail");
}

faqCategoryListSuccess = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.no.ctg.msg");
        }
        popAlertLayer(msg);
        $("#faqCtgList").html("");
        $(".btnWrite").hide();
        return;
    }

    var faqCategoryListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        faqCategoryListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#faqCtgList").html(faqCategoryListHtml);
}

faqCategoryListFail = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#faqCtgList").html("");
    $(".btnWrite").hide();
}

function registFaq() {
    if ($("#title").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.sbst.getData().length < 1) {
        popAlertLayer(getMessage("common.content.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        CKEDITOR.instances.sbst.updateElement();
        $("#sbst").val(CKEDITOR.instances.sbst.getData());

        formData("NoneForm", "svcFaqType", $("#serviceList option:selected").val());
        formData("NoneForm", "targetType", $("#targetTypeList option:selected").val());
        formData("NoneForm", "svcFaqCtg", $("#faqCtgList option:selected").val());
        formData("NoneForm", "svcFaqTitle", $("#title").val());
        formData("NoneForm", "svcFaqSbst", $("#sbst").val());

        var isPost = "N";
        if ($("input:radio[name=useYn]:checked").val() == "Y") {
            isPost = "Y";
        }
        formData("NoneForm", "isPost", isPost);

        callByPost("/api/svcFaq?svcType=ON", "registSvcFaqSuccess", "NoneForm", "insertFail");
    }, null, getMessage("common.confirm"));
}

registSvcFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"), "/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#serviceList option:selected").val());
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

insertFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

function returnToPage() {
    pageMove("/onlineSvc/customer#page=1&display=faq&svcSeq=" + $("#currSvcSeq").val());
}
