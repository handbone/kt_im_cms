/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    svcNoticeInfo();
});

svcNoticeInfo = function() {
    formData("NoneForm", "svcNoticeSeq", $("#svcNoticeSeq").val());
    callByGet("/api/svcNotice?svcType=ON", "svcNoticeInfoSuccess", "NoneForm", "svcNoticeInfoFail");
}

svcNoticeInfoSuccess = function(data) {
    formDataDelete("NoneForm", "svcNoticeSeq");
    if (data.resultCode == "1000") {
        var noticeDetail = data.result.noticeDetail;
        $("#regNm").html(noticeDetail.cretrNm);
        $("#svcNm").html(noticeDetail.svcNm);
        $("#dispType").html(noticeDetail.dispTypeNm);
        $("#targetType").html(noticeDetail.targetTypeNm);
        $("#regDt").html(noticeDetail.regDt);
        $("#prefRank").html(noticeDetail.prefRank);
        $("#title").html(noticeDetail.noticeTitle);
        $("#sbst div").html(noticeDetail.noticeSbst);
        //$("#sbst div").html(xssChk(noticeDetail.noticeSbst));
        //$("#sbst div").html($("#sbst").text());

        if ($("#memberSec").val() == '01' || $("#memberSec").val() == '02' || noticeDetail.cretrMbrSe == $("#memberSec").val()) {
            $("#btnModify").show();
            $("#btnDelete").show();
        }

        if (noticeDetail.popupViewYn == 'Y') {
            $("input[name='popupNoti']:input[value='popup']").attr("checked", true);
            //$("#popupImgRegn").show();
            //$("#popupUrlRegn").show();
            //$("#popupPerdRegn").show();

            if (data.result.popupImgFileList.length > 0) {
                $(data.result.popupImgFileList).each(function(i,item) {
                    var btnHtml = item.fileName + "&nbsp;" + "<input type=\"button\""
                            + " class=\"btnDefaultWhite btnSmall\" style=\"width:100px;\" value=\""
                            + getMessage("button.view") + "\" onclick=\"openPopups5('" + item.filePath + "')\">";
                    $("#popupImgNm").html(btnHtml);
                });
            }

            $("#popupUrl").html(noticeDetail.popupUrl);

            if (noticeDetail.popupPerdYn == 'N') { // 팝업노출기간 미설정
                $("#popupDispDt").html(getMessage("common.not.set"));
            } else {
                $("#popupDispDt").html(noticeDetail.popupViewStDt + " ~ " + noticeDetail.popupViewFnsDt);
            }
        }

        if (noticeDetail.postViewYn == 'Y') {
            $("input[name='popupNoti']:input[value='post']").attr("checked", true);
        }

        if (noticeDetail.delYn == 'N') {
            $("#useYn").html(getMessage("common.sttus.display"));
        } else {
            $("#useYn").html(getMessage("common.sttus.stop"));
        }

        var fileListHtml = "";
        if (data.result.attachFileList.length > 0) {
            $(data.result.attachFileList).each(function(i,item) {
                fileListHtml += "<p><span class='float_l'>" + item.fileName + "</span><a href='" + makeAPIUrl("/api/fileDownload/" + item.fileSeq) + "' class='fontblack btnDownloadGray'>[" + getMessage("common.download") + "]</a></p>";
            });
        }

        if (fileListHtml != "") {
            $("#attachFile").show();
            $("#files").html(fileListHtml);
        }

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/customer#page=1&display=notice&svcSeq=" + $("#currSvcSeq").val());
    }
}

svcNoticeInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/customer#page=1&display=notice&svcSeq=" + $("#currSvcSeq").val());
}

function deleteNotice() {
    popConfirmLayer(getMessage("notice.delete.confirm.msg"), function() {
        formData("NoneForm" , "delSvcNoticeSeqList", $("#svcNoticeSeq").val());
        callByDelete("/api/svcNotice?svcType=ON", "deleteSvcNoticeSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

deleteSvcNoticeSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/onlineSvc/customer#page=1&display=notice&svcSeq="  + $("#currSvcSeq").val());
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

moveToEdit = function() {
    pageMove("/onlineSvc/customer/notice/" + $("#svcNoticeSeq").val() + "/edit?svcSeq=" + $("#currSvcSeq").val());
}