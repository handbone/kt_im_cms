/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var readyPage = false;
var gridState;
var tempState;
var hashSvc;
var apiUrl= makeAPIUrl("/api/svcApp");
var totalPage = 0;
var appSeqsList = "";
var orderNumsList = "";
var repeat = "";

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

            getServiceList();
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

        gridState = "NONE";
    }

    getServiceList();
});

getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#memberSvcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#memberSvcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#memberSvcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        if (document.location.hash) {
            var strHash = document.location.hash.replace("#", "");
            hashSvc = findGetParameter(strHash, "svcSeq");
            $("#serviceList").val(hashSvc);
        } else {
            $("#serviceList option:eq(0)").prop("selected", true);
        }
    }

    serviceAppList();
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
}

serviceAppList = function() {
    var colNameList = [
        getMessage("table.num"),
        getMessage("table.disp.order"),
        getMessage("table.app.nm"),
        getMessage("table.contentsTitle.space"),
        getMessage("table.package.nm"),
        getMessage("table.main.image"),
        getMessage("table.store.url"),
        "URI",
        "appSeq",
        getMessage("column.title.imgPath"),
        "isFile"
    ];

    $("#jqgridData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.appList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:colNameList,
        colModel:[
            {name:"num", index: "num", align:"center", width:40},
            {name:"orderNum", index:"ORDER_NUM", align:"center"},
            {name:"appNm", index:"APP_NM", align:"center", formatter:pointercursor},
            {name:"contsTitle", index:"CONTS_TITLE", align:"center"},
            {name:"pkgNm", index:"PKG_NM", align:"center"},
            {name:"appImg", index:"APP_IMG", align:"center"},
            {name:"storUrl", index:"STOR_URL", align:"center"},
            {name:"uri", index:"URI", align:"center"},
            {name:"appSeq", index:"APP_SEQ", hidden:true},
            {name:"appImgPath", index:"APP_IMG_PATH", hidden:true},
            {name:"isFile", index:"IS_FILE", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: 10000, // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        //pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: false, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "ORDER_NUM",
        sortorder: "asc",
        height: "auto",
        multiselect: true,
        beforeRequest:function() {
            tempState = gridState;
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            appSeqsList = "";

            hashSvc = checkUndefined($("#serviceList").val());

            if (gridState == "HASH") {
                tempState = "READY";
            } else {
                tempState = "";
            }

            if (gridState == "NONE") {
                tempState = "READY";
            }

            gridState = "GRIDBEGIN";

            if (hashSvc != null && hashSvc != "") {
                myPostData.svcSeq = hashSvc;
            } else {
                myPostData.svcSeq = "";
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            // session
            if(sessionStorage.getItem("last-url") != null){
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            tempState = "";

            // 뒤로가기
            var hashlocation = "page=" + $(this).context.p.page + "&svcSeq=" + hashSvc;
            if($(this).context.p.postData._search && $("#target").val() != null){
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            repeat = this;
            $(this).find("td").each(function() {
                if ($(this).index() == 2) {
                    if ($(this).parents("tr").index() > 0) {
                        var orderNum = $("#jqgridData").jqGrid('getRowData', $(this).parents("tr").index()).orderNum;
                        if (orderNum <= 0) {
                            $(this).html("<input type=\"text\" class=\"inputBox width50 txtCenter orderNums\" value=''>");
                        } else {
                            $(this).html("<input type=\"text\" class=\"inputBox width50 txtCenter orderNums\" value=\"" + orderNum + "\">");
                        }
                        appSeqsList += $("#jqgridData").jqGrid('getRowData', $(this).parents("tr").index()).appSeq + ",";
                    }
                } else if ($(this).index() == 6) { // 메인 이미지
                    $(this).addClass("imgDisplay");
                    if ($(this).parents("tr").index() > 0) {
                        var imgPaths = $("#jqgridData").jqGrid('getRowData', $(this).parents("tr").index())["appImgPath"];
                        var isFile = $("#jqgridData").jqGrid('getRowData', $(this).parents("tr").index()).isFile;
                        if (isFile == "true") {
                            $(this).html("<a href='javascript:openPopups6("+$(this).parents("tr").index()+");'>" +
                                    "<img style='background-size: 100% 100%; background-repeat: no-repeat; background-position: center;" +
                                    " background-image:url("+ makeAPIUrl(imgPaths,"img") + ");' title=''></a>");
                        } else {
                            $(this).html("<a href='javascript:openPopups6("+$(this).parents("tr").index()+");'>" +
                                    "<img style='background-size: 100% 100%; background-repeat: no-repeat; background-position: center;" +
                                    " background-image:url(" + $('#contextPath').val() + "/resources/image/img_empty.png);' title=''></a>");
                        }
                    }
               }
            });

            //pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
            });

            //포인터
            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event){
            // 매장명 셀 클릭시
            if (columnId == 3) {
                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/onlineSvc/main/app/" + list.appSeq + "?svcSeq=" + $("#serviceList option:selected").val());
            } else if (columnId == 6) { // 메인 이미지 컬럼 선택 시 해당 row 체크되지 않도록 함.
                $("#jqgridData").jqGrid('setSelection', rowId).prop('checkbox', false);
            }
        }
    });
    //jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

updateList = function() {
    appSeqsList = "";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function deleteApps() {
    var appSeqs = appSeqsList.slice(0, -1);
    appSeqs = appSeqs.split(",");
    var svcAppSeqs = "";
    var svcAppNms = "";
    var delCnt = 0;

    for (var i = 0; i < appSeqs.length + 1; i++) {
        if ($("#jqg_jqgridData_" + i).prop("checked")) {
            var selAppSeq = $("#jqgridData").jqGrid('getRowData', i).appSeq;
            var selAppNm = $("#jqgridData").jqGrid('getRowData', i).appNm;
            svcAppSeqs += selAppSeq + ",";
            svcAppNms += selAppNm + ", ";
            delCnt++;
        }
    }

    if (delCnt == 0) {
        popAlertLayer(getMessage("service.common.delete.not.select.msg"));
        return;
    }

    popConfirmLayer(svcAppNms.slice(0, -2) + getMessage("service.common.delete.confirm.msg"), function() {
        formData("NoneForm" , "svcSeq", $("#serviceList").val());
        formData("NoneForm" , "delAppSeqs", svcAppSeqs.slice(0, -1));
        callByDelete("/api/svcApp", "deleteAppsSuccess", "NoneForm", "deleteAppsFail");
    }, null, getMessage("common.confirm"));
}

deleteAppsFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

deleteAppsSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), function(){location.reload();});
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

function moveToRegist() {
    if ($("#serviceList").val() == null) {
        popAlertLayer(getMessage("info.service.nodata.msg"));
        return;
    }

    pageMove("/onlineSvc/main/app/regist?svcSeq=" + $("#serviceList option:selected").val());
}

function storageOrderNums() {
    orderNumsList = "";
    var chkInvalid = false;
    $(repeat).find("td").each(function() {
        if ($(this).index() == 2) {
            if ($(this).parents("tr").index() > 0) {
                var orderNum = $(this).children(".orderNums").val();
                if (orderNum != "" && orderNum == 0) {
                    chkInvalid = true;
                    return false;
                }
                if (orderNum == "") {
                    orderNumsList += "-1,";
                } else {
                    orderNumsList += $(this).children(".orderNums").val() + ",";
                }
            }
        }
    });

    if (chkInvalid) {
        popAlertLayer(getMessage("service.common.order.num.input.zero.msg"));
        orderNumsList = "";
        return;
    }

    var orderNumArr = orderNumsList.slice(0, -1).split(",");
    for (var i = 0; i < orderNumArr.length; i++) {
        for (var j = 0; j < i; j++) {
            if (orderNumArr[i] == "") {
                continue;
            }
            if (orderNumArr[i] > 0 && orderNumArr[i] == orderNumArr[j]) {
                popAlertLayer(getMessage("service.common.exist.ordernum"));
                return;
            }
        }
    }

    popConfirmLayer(getMessage("common.save.msg"), function() {
        formData("NoneForm", "appSeqs", appSeqsList.slice(0, -1));
        formData("NoneForm", "orderNums", orderNumsList.slice(0, -1));
        callByPut("/api/svcApp", "storageOrderNumsSuccess", "NoneForm", "storageOrderNumsFail");
    }, null, getMessage("common.confirm"));
}

storageOrderNumsSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.storage"), function(){location.reload();});
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("service.common.exist.ordernum"));
    } else {
        popAlertLayer(getMessage("fail.common.storage"));
    }
}

storageOrderNumsFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.storage"));
}