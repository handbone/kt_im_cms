/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    getServiceList();

    limitInputTitle("appNm,contsTitle");

    setkeyup();

    $("input[name=type]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "APP") {
            $("#contsTitle").attr("disabled", true);
            $("#contsTitle").attr("readonly", true);
        } else {
            $("#contsTitle").attr("disabled", false);
            $("#contsTitle").attr("readonly", false);
        }
    });

    fileFormInsert("mainImg", "APP_IMG", -1, -1, "mainImg", "imgPreview", "png,jpg,bmp,jpeg");
    fileFormAllView();
});

getServiceList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON", "didReceiveServiceList", '', "didNotReceiveServiceList");
}

didReceiveServiceList = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#serviceList").html("");
        $(".btnWrite").hide();
        return;
    }

    var serviceListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        serviceListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#serviceList").html(serviceListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#memberSvcSeq").val() != 0) {
        $("#serviceList option").not("[value='"+ $("#memberSvcSeq").val() + "']").remove();
        $("#serviceList option[value='"+ $("#memberSvcSeq").val() + "']").prop("selected", true);
        $("#serviceList").attr("disabled", true);
    } else {
        $("#serviceList").val($("#currSvcSeq").val());
    }
}

didNotReceiveServiceList = function(data) {
    popAlertLayer(getMessage("fail.common.select"));
    $("#serviceList").html("");
    $(".btnWrite").hide();
}

function registApp() {
    if ($("#appNm").val().trim() == "") {
        popAlertLayer(getMessage("service.common.app.name.empty.msg"));
        $("#appNm").focus();
        return;
    }

    if ($("input[name=type]:checked").val() == "CONTS" && $("#contsTitle").val().trim() == "") {
        popAlertLayer(getMessage("service.common.contents.title.empty.msg"));
        $("#contsTitle").focus();
        return;
    }

    if ($("#pkgNm").val().trim() == "") {
        popAlertLayer(getMessage("service.common.packge.name.empty.msg"));
        $("#pkgNm").focus();
        return;
    }

    if ($("#storUrl").val().trim() == "") {
        popAlertLayer(getMessage("service.common.store.url.empty.msg"));
        $("#storUrl").focus();
        return;
    }

    if (!validateUrl($("#storUrl").val())) {
        popAlertLayer(getMessage("common.store.url.error.msg"));
        $("#storUrl").focus();
        return;
    }

    if ($("#uri").val().trim() == "") {
        popAlertLayer(getMessage("service.common.uri.empty.msg"));
        $("#uri").focus();
        return;
    }

    if (fileSet[0].filePath == "") {
        popAlertLayer(getMessage("service.common.main.image.empty.msg"));
        $("#mainImg").focus();
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        formData("comnty_write_from", "svcSeq", $("#serviceList option:selected").val());
        formData("comnty_write_from", "type", $("input[name=type]:checked").val());
        formData("comnty_write_from", "appNm", $("#appNm").val());

        if ($("input[name=type]:checked").val() == "CONTS") {
            formData("comnty_write_from", "contsTitle", $("#contsTitle").val());
        }

        formData("comnty_write_from", "pkgNm", $("#pkgNm").val());
        formData("comnty_write_from", "storUrl", $("#storUrl").val());
        formData("comnty_write_from", "uri", $("#uri").val());

        callByMultipart("/api/svcApp", "appRegistSuccess", "comnty_write_from", "insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data){
    if (!fileAccss && typeof data.resultCode == "undefined") {
        popAlertLayer(getMessage("common.check.file.Position"));
    } else if (data.resultCode == "1500") {
        popAlertLayer(getMessage("info.upload.failed.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();

}

appRegistSuccess = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"),"/onlineSvc/main/app#svcSeq=" + $("#serviceList").val());
    } else if (data.resultCode == "1011" && data.resultMsg == "exist pkgNm") {
        popAlertLayer(getMessage("service.common.exist.package.name.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();
}

returnToPage = function() {
    pageMove("/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
}

