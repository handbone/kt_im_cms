/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var delFileSeqs = "";
var typeVal = "";
var isChgPkgNm = false;
var orignlPkgNm = "";

$(document).ready(function() {
    //getServiceList();
    getServiceAppInfo();

    limitInputTitle("appNm,contsTitle");

    $("#pkgNm").change(function(){
        isChgPkgNm = true;
    });


    // TODO : 패키지명 입력 형태 정규식 구현 필요
    // TODO : 스토어 URL, URI 입력 형태 정규식 구현 필요

    setkeyup();

    /* TODO : 수정 시에는 변경 불가능
    $("input[name=type]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "APP") {
            $("#contsTitle").attr("disabled", true);
        } else {
            $("#contsTitle").attr("disabled", false);
        }
    });
    */

    fileFormInsert("mainImg", "APP_IMG", -1, -1, "mainImg", "imgPreview", "png,jpg,bmp,jpeg");
    fileFormAllView();
});

getServiceAppInfo = function() {
    formData("NoneForm", "appSeq", $("#appSeq").val());
    callByGet("/api/svcApp", "infoSuccess", "NoneForm", "infoFail");
}

infoSuccess = function(data) {
    formDataDelete("NoneForm", "appSeq");
    if (data.resultCode != "1000") {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
        return;
    }

    var appInfo = data.result.appInfo;
    $("#svcNm").html(appInfo.svcNm);
    typeVal = appInfo.type;
    $("input[name=type]:input[value='"+appInfo.type+"']").attr("checked", true);
    $("#appNm").val(appInfo.appNm);
    $("#contsTitle").val(appInfo.contsTitle);

    if ($("input:radio[name=type]:checked").val() == "CONTS") {
        $("#contsTitle").attr("disabled", false);
        $("#contsTitle").attr("readonly", false);
    }

    orignlPkgNm = appInfo.pkgNm;
    $("#pkgNm").val(appInfo.pkgNm);
    $("#storUrl").val(appInfo.storUrl);
    $("#uri").val(appInfo.uri);

    $(data.result.appImgFileList).each(function(i,item){
        delFileSeqs += item.fileSeq;
        $("#imgPreview").html("<img src='"+makeAPIUrl(item.filePath,"img")+"' title=\""+item.fileSeq+"\">");
    });
}

infoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
}

function updateApp() {
    if ($("#appNm").val().trim() == "") {
        popAlertLayer(getMessage("service.common.app.name.empty.msg"));
        $("#appNm").focus();
        return;
    }

    if ($("input[name=type]:checked").val() == "CONTS" && $("#contsTitle").val().trim() == "") {
        popAlertLayer(getMessage("service.common.contents.title.empty.msg"));
        $("#contsTitle").focus();
        return;
    }

    if ($("#pkgNm").val().trim() == "") {
        popAlertLayer(getMessage("service.common.packge.name.empty.msg"));
        $("#pkgNm").focus();
        return;
    }

    if ($("#storUrl").val().trim() == "") {
        popAlertLayer(getMessage("service.common.store.url.empty.msg"));
        $("#storUrl").focus();
        return;
    }

    if (!validateUrl($("#storUrl").val())) {
        popAlertLayer(getMessage("common.store.url.error.msg"));
        $("#storUrl").focus();
        return;
    }

    if ($("#uri").val().trim() == "") {
        popAlertLayer(getMessage("service.common.uri.empty.msg"));
        $("#uri").focus();
        return;
    }

    var nonChgFile = false;
    if (fileSet[0].filePath == "" && $("#imgPreview > img").length == 0) {
        popAlertLayer(getMessage("service.common.main.image.empty.msg"));
        return;
    }

    if (fileSet[0].filePath == "") {
        nonChgFile = true;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("comnty_write_from", "currSvcSeq", $("#currSvcSeq").val());
        formData("comnty_write_from", "appSeq", $("#appSeq").val());
        formData("comnty_write_from", "type", typeVal);
        formData("comnty_write_from", "appNm", $("#appNm").val());

        if ($("input[name=type]:checked").val() == "CONTS") {
            formData("comnty_write_from", "contsTitle", $("#contsTitle").val());
        }

        if (isChgPkgNm && orignlPkgNm != $("#pkgNm").val()) {
            formData("comnty_write_from", "pkgNm", $("#pkgNm").val());
        }

        formData("comnty_write_from", "storUrl", $("#storUrl").val());
        formData("comnty_write_from", "uri", $("#uri").val());

        if (!nonChgFile) {
            formData("comnty_write_from", "delFileSeqs", delFileSeqs);
        }
        formData("comnty_write_from", "_method", "PUT");

        callByMultipart("/api/svcApp", "appUpdateSuccess", "comnty_write_from", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data){
    if (!fileAccss && typeof data.resultCode == "undefined") {
        popAlertLayer(getMessage("common.check.file.Position"));
    } else if (data.resultCode == "1500") {
        popAlertLayer(getMessage("info.upload.failed.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();

}

appUpdateSuccess = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"),"/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
    } else if (data.resultCode == "1011" && data.resultMsg == "exist pkgNm") {
        popAlertLayer(getMessage("service.common.exist.package.name.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
    $("#comnty_write_from > input[type=hidden]").remove();
}

returnToPage = function() {
    pageMove("/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
}

