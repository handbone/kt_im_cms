/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var delFileSeqs = "";

$(document).ready(function(){
    getServiceAppInfo();
});

getServiceAppInfo = function() {
    formData("NoneForm", "appSeq", $("#appSeq").val());
    callByGet("/api/svcApp", "infoSuccess", "NoneForm", "infoFail");
}

infoSuccess = function(data) {
    formDataDelete("NoneForm", "appSeq");
    if (data.resultCode != "1000") {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
        return;
    }

    var appInfo = data.result.appInfo;
    $("#svcNm").html(appInfo.svcNm);
    $("input[name=type]:input[value='"+appInfo.type+"']").attr("checked", true);
    $("#appNm").html(appInfo.appNm);
    $("#contsTitle").html(appInfo.contsTitle);
    $("#pkgNm").html(appInfo.pkgNm);
    $("#storUrl").html(appInfo.storUrl);
    $("#uri").html(appInfo.uri);

    // 앱 이미지는 현재 하나만 등록됨.
    $(data.result.appImgFileList).each(function(i,item){
        delFileSeqs = item.fileSeq;
        $("#fileNm").html(item.fileName);
        $("#imgPreview").html("<a href=\""+makeAPIUrl(item.filePath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(item.filePath,"img")+"' title=\""+item.fileSeq+"\"></a>");
        $("#imgPreview").justifiedGallery({
            rowHeight : 220,
            lastRow : 'nojustify',
            margins : 3
        });
        $(".swipebox").swipebox();
    });
}

infoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
}

deleteApp = function(){
    var delAppSeqs = $("#appSeq").val();
    var svcSeq = $("#currSvcSeq").val();

    popConfirmLayer(getMessage("common.delete.msg"), function() {
        formData("NoneForm", "svcSeq", $("#currSvcSeq").val());
        formData("NoneForm", "delAppSeqs", $("#appSeq").val());
        formData("NoneForm", "svcSeq", $("#currSvcSeq").val());
        callByDelete("/api/svcApp", "deleteAppSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));

}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteAppSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"), "/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
    }
}

moveToEdit = function() {
    pageMove("/onlineSvc/main/app/" + $("#appSeq").val() + "/edit?svcSeq=" + $("#currSvcSeq").val());
}

returnToList = function() {
    pageMove("/onlineSvc/main/app#svcSeq=" + $("#currSvcSeq").val());
}

