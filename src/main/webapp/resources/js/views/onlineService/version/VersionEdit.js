/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var svcType;
var appType;
var svcAppType;

$(document).ready(function() {
    svcType = $("#svcType").val().slice(0,2);
    versionInfo();

    setkeyup();
});

versionInfo = function() {
    callByGet("/api/svcAppVersion?svcAppVerSeq=" + $("#svcAppVerSeq").val()+"&svcType="+svcType+"&type="+appType, "versionInfoSuccess", "NoneForm", "versionInfoFail");
}

versionInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.svcAppVersionInfo;

        $("#regrNm").html(item.regrNm);
        $("#svcNm").html(item.svcNm);
        $("#svcSeq").html(item.svcSeq);
        $("input[name=appType]:input[value='"+item.appType+"']").attr("checked", true);
        $("input[name=appType]").attr('disabled','disabled');
        $("input[name=saAppType]").attr('disabled','disabled');
        $("#verNm").html(item.svcAppVer);
        $("#dlStoreType").html(item.dlStoreType);
        $("#dlStorePath").val(xssChk(item.dlStorePath));
        var uptSbst = item.uptSbst
        uptSbst = uptSbst.split('<br />').join('\n');
        $("#uptSbst").html(uptSbst);
        $("input[name=useYn]:input[value='"+item.useYn+"']").attr("checked", true);
        $("#svcSeq").val(item.svcSeq);

        listBack($(".btnCancel"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/version");
    }
    svcList();
}

versionInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/version");
}

svcList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON","svcListSuccess","NoneForm");
}

svcListSuccess = function(data) {
    if (data.resultCode == "1000") {
        appTypeList = data.result.codeList;
        $(data.result.codeList).each(function(i, t) {
            if (t.comnCdValue == $("#svcSeq").val()) {
                appType = t.appType;
                return false;
            }
        });
        if(appType == "MA") {
            $("#appTypeMA").css("display", "");
            svcAppType = $("input[name=appType]:checked").val();
        } else {
            $("#appTypeSA").css("display", "");
            svcAppType = $("input[name=saAppType]:checked").val();
        }
    }
}

updateVersion = function() {
    var svcAppVerSeq = $("#svcAppVerSeq").val();

    if ($("#dlStorePath").val().trim() == "") {
        popAlertLayer(getMessage("version.insert.dlStorePath.msg"));
        $("#dlStorePath").focus();
        return;
    }

    if (!validateUrl($("#dlStorePath").val())) {
        popAlertLayer(getMessage("common.store.url.error.msg"));
        $("#dlStorePath").focus();
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("NoneForm", "svcAppVerSeq", $("#svcAppVerSeq").val());
        formData("NoneForm", "svcSeq", $("#svcSeq").val());
        formData("NoneForm", "dlStorePath", $("#dlStorePath").val());
        formData("NoneForm", "uptSbst", $("#uptSbst").val().replace(/(?:\r\n|\r|\n)/g, '<br />'));
        formData("NoneForm", "useYn", $("input[name=useYn]:checked").val());
        callByPut("/api/svcAppVersion?svcType="+svcType, "updateVersionSuccess", "NoneForm","updateFail");
    }, null, getMessage("common.confirm"));
}

updateVersionSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"),"/onlineSvc/version#page=1&svcSeq="+$("#svcSeq").val()+"&type="+svcAppType);
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

updateFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}
