/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var svcType;
var appType;
var svcAppType;

$(document).ready(function() {
    svcType = $("#svcType").val().slice(0,2);
    versionInfo();
});

versionInfo = function() {
    callByGet("/api/svcAppVersion?svcAppVerSeq=" + $("#svcAppVerSeq").val()+"&svcType="+svcType, "versionInfoSuccess", "NoneForm", "versionInfoFail");
}

versionInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.svcAppVersionInfo;

        $("#regrNm").html(item.regrNm);
        $("#svcNm").html(item.svcNm);
        $("input[name=appType]:input[value='"+item.appType+"']").attr("checked", true);
        $("input[name=appType]").attr('disabled','disabled');
        $("input[name=saAppType]").attr('disabled','disabled');
        $("#verNm").html(item.svcAppVer);
        $("#dlStoreType").html(item.dlStoreType);
        $("#dlStorePath").html(item.dlStorePath);
        $("#uptSbst").html(item.uptSbst);
        $("input[name=useYn]:input[value='"+item.useYn+"']").attr("checked", true);
        $("input[name=useYn]").attr('disabled','disabled');
        $("#svcSeq").val(item.svcSeq);

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/version");
    }
    svcList();
}

svcList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON","svcListSuccess","NoneForm");
}

svcListSuccess = function(data) {
    if (data.resultCode == "1000") {
        appTypeList = data.result.codeList;
        $(data.result.codeList).each(function(i, t) {
            if (t.comnCdValue == $("#svcSeq").val()) {
                appType = t.appType;
                return false;
            }
        });
        if(appType == "MA") {
            $("#appTypeMA").css("display", "");
            svcAppType = $("input[name=appType]:checked").val();
        } else {
            $("#appTypeSA").css("display", "");
            svcAppType = $("input[name=saAppType]:checked").val();
        }
    }
}
versionInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/onlineSvc/version");
}

deleteVersion = function() {
    var svcAppVerSeq = $("#svcAppVerSeq").val();

    if (svcAppVerSeq == "") {
        popAlertLayer(getMessage("cms.release.msg.noData"));
        return;
    }

    popConfirmLayer(getMessage("msg.versions.confirm.delete.detail"), function(){
        formData("NoneForm", "delAppVerSeqList", svcAppVerSeq);
        callByDelete("/api/svcAppVersion?svcType="+svcType, "deleteVersionSuccess", "NoneForm","deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteVersionSuccess = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"),"/onlineSvc/version#page=1&svcSeq="+$("#svcSeq").val()+"&type="+svcAppType+"&svcType="+svcType);
    }
}

deleteFail = function(data) {
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}
