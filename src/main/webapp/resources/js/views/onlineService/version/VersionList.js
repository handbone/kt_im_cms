/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var hashSvc;
var hashType;
var apiUrl = makeAPIUrl("/api/svcAppVersion");
var gridState;
var tempState;
var selTab = 0;
var totalPage = 0;
var svcType;
var resultReset = false;

var appTypeList = null;
var appType = "";

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashType = checkUndefined(findGetParameter(str_hash,"type"));

            if (hashType == "SA" || hashType == null) {
                if ($("#type").val() == "MA") {
                    totalPage = 0;
                    $("#jqgridMaData").jqGrid("GridUnload");
                    $("#jqgridMaData").hide();
                    $("#jqgridSaData").show();
                    $("#type").val(hashType);
                    $("#svcList").val(hashSvc);

                    serviceAppList();
                } else {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                    jQuery("#jqgridSaData").trigger("reloadGrid");
                }
            } else {
                if ($("#type").val() == "SA") {
                    totalPage = 0;
                    $("#jqgridSaData").jqGrid("GridUnload");
                    $("#jqgridSaData").hide();
                    $("#jqgridMaData").show();
                    $("#type").val(hashType);
                    $("#svcList").val(hashSvc);

                    mirroringAppList();
                } else {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                    jQuery("#jqgridMaData").trigger("reloadGrid");
                }
            }
        } else if (gridState == "GRIDCOMPLETE") {
            var str_hash = document.location.hash.replace("#","");
            var searchStr = checkUndefined(findGetParameter(str_hash,"searchString"));
            if (checkUndefined(findGetParameter(str_hash,"searchString")) == null) {
                searchStr = "";
            }
            if (!resultReset) {
                if (searchStr != $("#keyword").val()) {
                    $("#keyword").val(checkUndefined(findGetParameter(str_hash,"searchString")));
                    keywordSearch();
                }
            }
            resultReset = false;
        }else {
            gridState = "READY";
        }
    });
});

$(document).ready(function(){
    svcType = $("#svcType").val().slice(0,2);
    if (document.location.hash) {
        var str_hash = document.location.hash.replace("#","");
        hashType = checkUndefined(findGetParameter(str_hash,"type"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#type").val(hashType);
        $("#target").val(searchField);
        $("#keyword").val(searchString);
        gridState = "NONE";

        if (hashType == "SA" || hashType == null) {
            $("#jqgridMaData").jqGrid("GridUnload");
            $("#jqgridMaData").hide();
            $("#jqgridSaData").show();
            $("#svcList").val(hashSvc);
            $("#type").val(hashType);
        } else {
            $("#jqgridSaData").jqGrid("GridUnload");
            $("#jqgridSaData").hide();
            $("#jqgridMaData").show();
            $("#svcList").val(hashSvc);
            $("#type").val(hashType);
        }
    }

    getSvcList($("#type").val());

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

getSvcList = function(type){
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON&appType="+type,"getSvcListSuccess","NoneForm");
}

getSvcListSuccess = function(data){
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#svcList").html("");
        return;
    }

    var svcListHtml = "";
    if ($("#memberSec").val() != "04" && $("#memberSec").val() != "05") {
        svcListHtml += "<option value=''>" + getMessage("select.all") + "</option>";
    }

    $(data.result.codeList).each(function(i,item) {
        svcListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNm+"</option>";
    });
    $("#svcList").html(svcListHtml);

    if (!isAdministrator($("#memberSec").val()) && $("#memberSvcSeq").val() != 0) {
        $("#svcList option").not("[value='"+ $("#memberSvcSeq").val() + "']").remove();
        $("#svcList option[value='"+ $("#memberSvcSeq").val() + "']").prop("selected", true);
        $("#svcList").attr("disabled", true);
    } else {
        if (document.location.hash) {
            var strHash = document.location.hash.replace("#", "");
            hashSvc = findGetParameter(strHash, "svcSeq");
            $("#svcList").val(hashSvc);
        } else {
            $("#svcList option:eq(0)").prop("selected", true);
        }
    }

    appTypeList = data.result.codeList;
    $(data.result.codeList).each(function(i, item) {
        if (item.comnCdValue == $("#svcList").val()) {
            appType = item.appType;
            return false;
        }
    });

    if ($("#type").val() == "SA") {
      serviceAppList();
    } else {
      mirroringAppList();
    }
}

serviceAppList = function() {
    var columnNames = [
        getMessage("column.title.num"),
        getMessage("common.service"),
        getMessage("version.table.name"),
        getMessage("version.table.store"),
        getMessage("version.table.store.path"),
        getMessage("table.reger"),
        getMessage("table.regdate"),
        getMessage("version.table.useYn"),
        getMessage("column.title.svcAppVerSeq"),
        "appType",
        "svcType"
    ];

    $("#jqgridSaData").jqGrid({
        url: apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.svcAppVersionList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:columnNames,
        colModel: [
            {name:"num", index: "num", align:"center", width:30},
            {name:"svcNm", index:"SVC_NM", align:"center",formatter:pointercursor, width:130},
            {name:"svcAppVer", index:"SVC_APP_VER", align:"center", width:90},
            {name:"dlStoreType", index:"DL_STORE_TYPE", align:"center", width:90},
            {name:"dlStorePath", index:"DL_STORE_PATH", align:"center"},
            {name:"regrNm", index:"MBR_NM", align:"center", width:100},
            {name:"regDt", index:"REG_DT", align:"center", width:100},
            {name:"useYn", index:"USE_YN", align:"center", width:100},
            {name:"svcAppVerSeq", index:"SVC_APP_VER_SEQ", hidden:true},
            {name:"appType", index:"APP_TYPE", hidden:true},
            {name:"svcType", index:"SVC_TYPE", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        pager: "#saPageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "SVC_APP_VER_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: true,
        beforeRequest:function(){
            tempState = gridState;
            var myPostData = $('#jqgridSaData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
               page =1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState == "TABVAL") {
                searchField = "";
                searchString = "";
            } else if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }

            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());

            hashType = "SA";
            var svcSeq = $("#svcList").val();

            if (svcSeq == null || svcSeq == 0) {
                hashSvc = 0;
            } else {
                hashSvc = svcSeq;
            }

            apiUrl = makeAPIUrl("/api/svcAppVersion?appType="+hashType+"&svcType="+svcType);

            $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
            $(".conTitleGrp div:eq(" + selTab + ")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if(tempState == "TABVAL" || tempState == "SEARCH"){
                    myPostData.page = 1;
                    page = 1;
                }
                if (tempState != "SEARCH") {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

                myPostData._search = true;
                myPostData.searchField = searchField;
                myPostData.searchString = searchString;
                myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "" && !resultReset) {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                if (resultReset) {
                    $("#target").val("");
                    $("#keyword").val("");
                }
                delete myPostData.searchString;
                delete myPostData.searchField;
                myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            myPostData.svcType = "ON";
            myPostData.svcSeq = hashSvc;
            myPostData.type = hashType;

            if(resultReset){
                myPostData.page = 1;
            }

            $('#jqgridSaData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page });
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            //session
            if(sessionStorage.getItem("last-url") != null) {
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            //Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /*뒤로가기*/
            var hashlocation = "page="+$(this).context.p.page+"&svcSeq="+$("#svcList").val()+"&type="+$("#type").val();
            if ($(this).context.p.postData._search && $("#target").val() != null) {
                hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            if (!resultReset) {
                if (tempState != "" || str_hash == hashlocation) {
                    gridState = "READY";
                }
            }

            // Master 관리자, CMS 관리자가 아닌 경우 checkbox selectAll 비활성화
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02') {
                $("#cb_jqgridMaData").attr("disabled", true);
            }

            $(this).find("td").each(function() {
                if ($(this).index() == 0) {
                    // 권한이 맞지 않는 글일 경우 삭제할 수 없도록 checkbox 비활성화
                    var cretrMbrSe = $(this).parent("tr").find("td:eq(10)").text();

                    if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02' && (cretrMbrSe < Number($("#memberSec").val()))) {
                        $(this).parent("tr").find("td > input.cbox").attr("disabled", true);
                    }
                } else if ($(this).index() == 8) { // 사용유무
                    var str = $(this).text();

                    if (str == 'Y') {
                        str = getMessage("version.useYn.y");
                    } else if (str == 'N') {
                        str = getMessage("version.useYn.n");
                    }
                    $(this).html(str);
                }
            });

          //pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridSaData").getGridParam("lastpage")) this.value = $("#jqgridSaData").getGridParam("lastpage");
            });

            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridSaData", "tableArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridSaData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event) {
            var list = $("#jqgridSaData").jqGrid('getRowData', rowId);
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02'
                && (list.cretrMbrSe < Number($("#memberSec").val()))) {
                $('#jqgridSaData').jqGrid('setSelection', rowId, false);
            }

            // 서비스 셀 선택시
            if (columnId == 2) {
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/onlineSvc/version/"+list.svcAppVerSeq);
              }
        }
    });
    jQuery("#jqgridSaData").jqGrid('navGrid','#saPageDiv',{del:false,add:false,edit:false,search:false});
}

mirroringAppList = function(data) {
    var columnNames = [
        getMessage("column.title.num"),
        getMessage("common.service"),
        getMessage("version.table.name"),
        getMessage("version.table.store"),
        getMessage("version.table.store.path"),
        getMessage("table.reger"),
        getMessage("table.regdate"),
        getMessage("version.table.useYn"),
        getMessage("column.title.svcAppVerSeq"),
        "appType",
        "svcType"
    ];

    $("#jqgridMaData").jqGrid({
        url: apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.svcAppVersionList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:columnNames,
        colModel: [
            {name:"num", index: "num", align:"center", width:30},
            {name:"svcNm", index:"SVC_NM", align:"center",formatter:pointercursor, width:130},
            {name:"svcAppVer", index:"SVC_APP_VER", align:"center", width:90},
            {name:"dlStoreType", index:"DL_STORE_TYPE", align:"center", width:90},
            {name:"dlStorePath", index:"DL_STORE_PATH", align:"center"},
            {name:"regrNm", index:"MBR_NM", align:"center", width:100},
            {name:"regDt", index:"REG_DT", align:"center", width:100},
            {name:"useYn", index:"USE_YN", align:"center", width:100},
            {name:"svcAppVerSeq", index:"SVC_APP_VER_SEQ", hidden:true},
            {name:"appType", index:"APP_TYPE", hidden:true},
            {name:"svcType", index:"SVC_TYPE", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        pager: "#maPageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "SVC_APP_VER_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: true,
        beforeRequest:function(){
            tempState = gridState;
            var myPostData = $('#jqgridMaData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
               page =1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState == "TABVAL") {
                searchField = "";
                searchString = "";
            } else if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }

            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());

            hashType = "MA";
            var svcSeq = $("#svcList").val();

            if (svcSeq == null || svcSeq == 0) {
                hashSvc = 0;
            } else {
                hashSvc = svcSeq;
            }

            apiUrl = makeAPIUrl("/api/svcAppVersion?appType="+hashType+"&svcType="+svcType);
            selTab = 1;

            $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
            $(".conTitleGrp div:eq(" + selTab + ")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if (tempState == "TABVAL" || tempState == "SEARCH") {
                    myPostData.page = 1;
                }
                if (tempState != "SEARCH") {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

                myPostData._search = true;
                myPostData.searchField = searchField;
                myPostData.searchString = searchString;
                myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "" && !resultReset) {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                if (resultReset) {
                    $("#target").val("");
                    $("#keyword").val("");
                }
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            myPostData.svcType = "ON";
            myPostData.svcSeq = hashSvc;
            myPostData.type = hashType;

            if(resultReset){
                myPostData.page = 1;
            }

            $('#jqgridMaData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page });
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            //session
            if (sessionStorage.getItem("last-url") != null) {
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            //Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /*뒤로가기*/
            var hashlocation = "page="+$(this).context.p.page+"&svcSeq="+$("#svcList").val()+"&type="+$("#type").val();
            if ($(this).context.p.postData._search && $("#target").val() != null) {
                hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            if (!resultReset) {
                if (tempState != "" || str_hash == hashlocation) {
                    gridState = "READY";
                }
            }

            // Master 관리자, CMS 관리자가 아닌 경우 checkbox selectAll 비활성화
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02') {
                $("#cb_jqgridMaData").attr("disabled", true);
            }

            $(this).find("td").each(function() {
                if ($(this).index() == 0) {
                    // 권한이 맞지 않는 글일 경우 삭제할 수 없도록 checkbox 비활성화
                    var cretrMbrSe = $(this).parent("tr").find("td:eq(10)").text();

                    if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02' && (cretrMbrSe < Number($("#memberSec").val()))) {
                        $(this).parent("tr").find("td > input.cbox").attr("disabled", true);
                    }
                } else if ($(this).index() == 8) { // 사용유무
                    var str = $(this).text();

                    if (str == 'Y') {
                        str = getMessage("version.useYn.y");
                    } else if (str == 'N') {
                        str = getMessage("version.useYn.n");
                    }
                    $(this).html(str);
                }
            });

          //pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridMaData").getGridParam("lastpage")) this.value = $("#jqgridMaData").getGridParam("lastpage");
            });

            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridMaData", "tableArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridMaData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event){
            var list = $("#jqgridMaData").jqGrid('getRowData', rowId);
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02'
                && (list.cretrMbrSe < Number($("#memberSec").val()))) {
                $('#jqgridMaData').jqGrid('setSelection', rowId, false);
            }

            // 서비스 셀 선택시
            if (columnId == 2) {
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/onlineSvc/version/"+list.svcAppVerSeq);
              }
        }
    });
    jQuery("#jqgridMaData").jqGrid('navGrid','#maPageDiv',{del:false,add:false,edit:false,search:false});
}

function jqGridReload() {
    if ($("#type").val() == "SA") {
        jQuery("#jqgridSaData").jqGrid("GridUnload");

        var svcSeq = $("#svcList").val();
        $(appTypeList).each(function(i, item) {
            if (item.comnCdValue == svcSeq) {
                appType = item.appType;
                return false;
            }
        });

        serviceAppList();
    } else {
        $("#jqgridMaData").jqGrid("GridUnload");

        var svcSeq = $("#svcList").val();
        $(appTypeList).each(function(i, item) {
            if (item.comnCdValue == svcSeq) {
                appType = item.appType;
                return false;
            }
        });

        mirroringAppList();
    }
}

function keywordSearch(){
    gridState = "SEARCH";
    if ($("#type").val() == 'SA') {
        jQuery("#jqgridSaData").trigger("reloadGrid");
    } else {
        jQuery("#jqgridMaData").trigger("reloadGrid");
    }
}

searchFieldSet = function() {
    var colModel = "";
    var colNames = "";
    var searchHtml = "";

    if ($("#target > option").length == 0) {
        if ($("#type").val() == 'SA') {
            colModel = $("#jqgridSaData").jqGrid('getGridParam', 'colModel');
            colNames = $("#jqgridSaData").jqGrid('getGridParam', 'colNames');

            for (var i = 0; i < colModel.length; i++) {
                //검색제외추가
                switch(colModel[i].name) {
                case "svcAppVer":break;
                default:continue;
                }
                searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
            }
            $("#target").html(searchHtml);
            if ($("#target option").index() == 0) {
                $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
                $(".target_bak").parent(".selectBox").css("display","table");
            } else {
                $(".target_bak").remove();
            }
        } else {
            colModel = $("#jqgridMaData").jqGrid('getGridParam', 'colModel');
            colNames = $("#jqgridMaData").jqGrid('getGridParam', 'colNames');

            for (var i = 0; i < colModel.length; i++) {
                //검색제외추가
                switch(colModel[i].name) {
                case "svcAppVer":break;
                default:continue;
                }
                searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
            }
            $("#target").html(searchHtml);
            if ($("#target option").index() == 0) {
                $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
                $(".target_bak").parent(".selectBox").css("display","table");
            } else {
                $(".target_bak").remove();
            }
        }
    }
    $("#target").val(searchField);
}

function tabVal(seq,e){
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn");
    $(e).parent("div").addClass("tabOn").css({ 'pointer-events': 'none' });

    selTab = seq;

    $("#target").val("");
    $("#keyword").val("");

    totalPage = 0;

    if (selTab == 0) {  // 서비스 앱
        $("#type").val("SA");
        $("#jqgridMaData").jqGrid("GridUnload");
        $("#jqgridMaData").hide();
        $("#jqgridSaData").show();
    } else {    // 미러링 앱
        $("#type").val("MA");
        $("#jqgridSaData").jqGrid("GridUnload");
        $("#jqgridSaData").hide();
        $("#jqgridMaData").show();
    }

    $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
    $(".conTitleGrp div:eq(" + selTab + ")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

    getSvcList($("#type").val());
}

deleteVersion = function(){
    var delAppVerSeqList = "";
    var svcAppVerList = "";
    var svcNmList = "";
    var delCnt = 0;
    var svcList="";

    if (selTab == 0) {  // SA 탭
        for(var i=1; i<$("#limit").val()+1; i++){
            if($("#jqg_jqgridSaData_"+i).prop("checked")){
                var svcAppVerSeq = $("#jqgridSaData").jqGrid('getRowData', i).svcAppVerSeq;
                var svcAppVer = $("#jqgridSaData").jqGrid('getRowData', i).svcAppVer.replace(/(<([^>]+)>)/ig,"");
                var svcNm = $("#jqgridSaData").jqGrid('getRowData', i).svcNm;

                if (svcAppVerList != "") {
                    svcAppVerList += ", ";
                }

                delAppVerSeqList += svcAppVerSeq + ",";
                svcList += (svcNm + " " + svcAppVer + ",");
                delCnt++;
            }
        }

        if (delCnt == 0) {
            popAlertLayer(getMessage("version.no.select.content.msg"));
            return;
        }

        popConfirmLayer(svcList.slice(0,-1) +" "+getMessage("msg.versions.confirm.delete"), function(){
            formData("NoneForm" , "delAppVerSeqList", delAppVerSeqList.slice(0,-1));
            formData("NoneForm" , "svcList", svcList.slice(0,-1));
            callByDelete("/api/svcAppVersion?svcType="+svcType, "deleteVersionSuccess", "NoneForm","deleteFail");
        },null,getMessage("common.confirm"));
    } else {    // MA 탭
        for(var i=1; i<$("#limit").val()+1; i++){
            if($("#jqg_jqgridMaData_"+i).prop("checked")){
                var svcAppVerSeq = $("#jqgridMaData").jqGrid('getRowData', i).svcAppVerSeq;
                var svcAppVer = $("#jqgridMaData").jqGrid('getRowData', i).svcAppVer.replace(/(<([^>]+)>)/ig,"");
                var svcNm = $("#jqgridMaData").jqGrid('getRowData', i).svcNm;

                if (svcAppVerList != "") {
                    svcAppVerList += ", ";
                }

                delAppVerSeqList += svcAppVerSeq + ",";
                svcList += (svcNm + " " + svcAppVer + ",");
                delCnt++;
            }
        }

        if (delCnt == 0) {
            popAlertLayer(getMessage("version.no.select.content.msg"));
            return;
        }

        popConfirmLayer(svcList.slice(0,-1) +" "+getMessage("msg.versions.confirm.delete"), function(){
            formData("NoneForm" , "delAppVerSeqList", delAppVerSeqList.slice(0,-1));
            formData("NoneForm" , "svcList", svcList.slice(0,-1));
            callByDelete("/api/svcAppVersion?svcType="+svcType, "deleteVersionSuccess", "NoneForm","deleteFail");
        },null,getMessage("common.confirm"));
    }
}

deleteFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("msg.common.delete.fail"));
}

deleteVersionSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"),"/onlineSvc/version#page=1&svcSeq="+$("#svcList").val()+"&type="+$("#type").val()+"&svcType="+svcType);
        if ($("#type").val() == "SA") {
            jQuery("#jqgridSaData").trigger("reloadGrid");
        } else {
            jQuery("#jqgridMaData").trigger("reloadGrid");
        }
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

function makePageMove(e){
    if (selTab == 0) {  // SA 탭
        if ($("#svcList option").index() != -1) {
            pageMove("/onlineSvc/version/regist#svcSeq="+$("#svcList").val()+"&type="+$("#type").val());
        }
    } else {
        if ($("#svcList option").index() != -1) {
            pageMove("/onlineSvc/version/regist#svcSeq="+$("#svcList").val()+"&type="+$("#type").val());
        }
    }
}

function pageReset(){
    resultReset = true;
}