/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var str_hash = document.location.hash.replace("#","");
var hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
var svcType;

var appTypeList = null;
var appType = "";
var type = "";

$(document).ready(function() {
    svcType = $("#svcType").val().slice(0,2);

    limitInputTitle("verNm");

    setkeyup();
    svcList();
    dlStoreTypeList();
});

svcList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SERVICE&comnCdValue=ON","svcListSuccess","NoneForm");
}
svcListSuccess = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.noservice.msg");
        }
        popAlertLayer(msg);
        $("#svcList").html("");
        $(".btnWrite").hide();
        return;
    }

    var svcListHtml = "";
    $(data.result.codeList).each(function(i,item) {
            svcListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNm+"</option>";
    });
    $("#svcList").html(svcListHtml);

    if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
        // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
        $("#svcList").attr('disabled', 'true');
    }

    if (hashSvc != "" && hashSvc != null) {
        $("#svcList > option[value=" + hashSvc + "]").attr("selected","true");
        hashSvc = "";
    }

    appTypeList = data.result.codeList;
    $(data.result.codeList).each(function(i, item) {
        if (item.comnCdValue == $("#svcList").val()) {
            appType = item.appType;
            return false;
        }
    });

    if (appType == "MA") {
        $("#appTypeMA").show();
        $("#appTypeSA").hide();
    } else {
        $("#appTypeMA").hide();
        $("#appTypeSA").show();
    }
}

dlStoreTypeList = function() {
    callByGet("/api/codeInfo?comnCdCtg=APP_DL_STORE","dlStoreTypeListSuccess","NoneForm");
}

dlStoreTypeListSuccess = function(data) {
    if (data.resultCode != "1000") {
        var msg = getMessage("fail.common.select");
        if (data.resultCode == "1010") {
            msg = getMessage("common.no.target.type.msg");
        }
        popAlertLayer(msg);
        $("#dlStoreType").html("");
        $(".btnWrite").hide();
        return;
    }

    var dlStoreTypeListHtml = "<option value=''>== " + getMessage("version.dl.store.type.select") + " ==</option>";
    $(data.result.codeList).each(function(i,item) {
        dlStoreTypeListHtml += "<option value='"+item.comnCdValue+"'>"+item.comnCdNm+"</option>";
    });
    $("#dlStoreType").html(dlStoreTypeListHtml);
}

verRegister = function() {
    var optionVal = $(this).find("select option:selected").val();

    if ($("#verNm").val().trim() == "") {
        popAlertLayer(getMessage("msg.version.insert.verNm"));
        $("#verNm").focus();
        return;
    }

    if ($("#dlStoreType input").length == 0 && $("#dlStoreType option:selected").val() == "") {
        popAlertLayer(getMessage("version.insert.dlStoreType.msg"));
        $("#dlStoreType").focus();
        return;
    }

    if ($("#dlStorePath").val().trim() == "") {
        popAlertLayer(getMessage("version.insert.dlStorePath.msg"));
        $("#dlStorePath").focus();
        return;
    }

    if (!validateUrl($("#dlStorePath").val())) {
        popAlertLayer(getMessage("common.store.url.error.msg"));
        $("#dlStorePath").focus();
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        formData("NoneForm" ,"regrId", $("#regrId").val());
        formData("NoneForm" ,"svcSeq", $("#svcList option:selected").val());
        formData("NoneForm" ,"appType", $("input[name=appType]:checked").val());
        formData("NoneForm" ,"svcAppVer", $("#verNm").val());
        formData("NoneForm" ,"dlStoreType", $("#dlStoreType option:selected").val());
        formData("NoneForm" ,"dlStorePath", $("#dlStorePath").val());
        formData("NoneForm" ,"uptSbst", $("#uptSbst").val().replace(/(?:\r\n|\r|\n)/g, '<br />')); // \r 캐리지 리턴 문자 \n 줄 바꿈 문자 (둘 다 엔터를 뜻함)
        formData("NoneForm" ,"useYn", $("input[name=useYn]:checked").val());

        callByPost("/api/svcAppVersion?svcType="+svcType, "verRegisterSuccess", "NoneForm","insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data) {
    popAlertLayer(getMessage("fail.common.insert"));
}

verRegisterSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    type = $("input[name=appType]:checked").val();
    if (data.resultCode == "1000") {
        var retUrl = "/onlineSvc/version#page=1&svcSeq=" + $("#svcList").val()+"&type="+type;
        popAlertLayer(getMessage("success.common.insert"), retUrl);
    } else if (data.resultCode == "1011") {
        if (data.resultMsg == "exist verNm") {
            popAlertLayer(getMessage("common.version.name.exist.msg"));
        } else {
            popAlertLayer(getMessage("fail.common.insert"));
        }
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

function jqGridReload() {
    var svcSeq = $("#svcList").val();
    $(appTypeList).each(function(i, item) {
        if (item.comnCdValue == svcSeq) {
            appType = item.appType;
            return false;
        }
    });

    if (appType == "MA") {
        $("#appTypeMA").show();
        $("#appTypeSA").hide();
    } else {
        $("#appTypeMA").hide();
        $("#appTypeSA").show();
    }
}

function makeBakMove(){
    var str_hash = document.location.hash.replace("#","");
    var bakSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
    pageMove("/onlineSvc/version#svcSeq="+bakSvc);
}
