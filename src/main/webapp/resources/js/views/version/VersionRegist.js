/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var str_hash = document.location.hash.replace("#","");
var hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
var hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
var ATTCH_FILE;
var FILE_SETTINGS   = {
        // Backend Settings
//      upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
};

//swfupload - attch_file
fileUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'version'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.hwp;*.doc;*.txt;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.png;*.jpg;*.jpeg;*.bmp;*.gif",
    file_settings.file_queue_limit =  "0";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    ATTCH_FILE  = new SWFUpload(file_settings);
}

var totalCount;
var checkedName;
var oriName;
var arr = new Array();
$(document).ready(function(){
    limitInputTitle("verNm");

    setkeyup();
    svcList();
    verTypeList();
    fileUploadBtn();
});

svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
                svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }

        if (hashSvc != "" && hashSvc != null) {
            $("#svcList > option[value=" + hashSvc + "]").attr("selected","true");
            hashSvc = "";
        }
        storList();
    } else {
        popAlertLayer(getMessage("common.bad.request.msg"), makeBakMove());
    }
}

storList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"storListSuccess","NoneForm");
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        checkedName = "";
        var memberStoreListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(memberStoreListHtml);
        $("#storList").attr("disabled", false);
        if ($("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#storList").attr('disabled', 'true');
        }
        if (hashStore != "" && hashStore != null) {
            $("#storList > option[value=" + hashStore + "]").attr("selected","true");
            hashStore = "";
        }
        $("input").unbind( "click" );
    }   else {
        $("#storList").html("");
        $("#storList").attr("disabled", true);
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("input:not(.btnCancel):not(.btnLogout)").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });

    }
}

verTypeList = function(){
    callByGet("/api/codeInfo?comnCdCtg=VERSION","verTypeListSuccess","NoneForm");
}

verTypeListSuccess = function(data){
    if(data.resultCode == "1000"){
        var verTypListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            verTypListHtml += "<option value='"+item.comnCdValue+"'>"+item.comnCdNm+"</option>"
            $("#verType").html(verTypListHtml);
        })
    } else if(data.resultCode == "1010"){
    }
}

verRegister = function(){
    if($("#verNm").val().trim() == ""){
        popAlertLayer(getMessage("msg.version.insert.verNm"));
        return;
    }
    if($("#storList").val() == ""){
        popAlertLayer(getMessage("msg.contents.select.store"));
        return;
    }

    if($("#verHst").val().trim() == ""){
        popAlertLayer(getMessage("msg.versions.insert.verHst"));
        return;
    }

    if($("#verType").val() == ""){
        popAlertLayer(getMessage("msg.version.select.verType"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        formData("NoneForm" ,"storSeq", $("#storList option:selected").val());
        formData("NoneForm" ,"svcSeq", $("#svcList option:selected").val());
        formData("NoneForm" ,"verNm", $("#verNm").val());
        formData("NoneForm" ,"verType", $("#verType option:selected").val());
        formData("NoneForm" ,"verHst", $("#verHst").val());
        callByPost("/api/version" , "verRegisterSuccess", "NoneForm","InsertFail");
    }, null, getMessage("common.confirm"));
}

InsertFail = function(data){
    popAlertLayer(getMessage("fail.common.insert"));
}


verRegisterSuccess = function(data){
    if(data.resultCode == "1000"){
        $("#contsSeq").val(data.result.versionInsert.verSeq);
        uploadAttachFiles();
    }
    formDataDeleteAll("NoneForm");
}

uploadAttachFiles = function() {
    if(fileTemp != undefined){
        fileTemp.startUpload();
    } else {
        popAlertLayer(getMessage("success.common.insert"), "/version#page=1&storSeq="+$("#storList").val()+"&svcSeq="+$("#svcList").val());
    }
}

fileInsertSuccess = function(data) {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    contentsXmlCreate();
}

fileInsertFail = function() {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    contentsXmlCreate();
}

/*모든파일등록성공시 호출 (xml 등록)*/
contentsXmlCreate = function(){
    var msg = getMessage("success.common.insert");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }

    popAlertLayer(msg, "/version#page=1&storSeq="+$("#storList").val()+"&svcSeq="+$("#svcList").val());
}


filesubmitSuccess = function(data){
    popAlertLayer(getMessage("success.common.insert"),"/version#page=1&storSeq="+$("#storList").val()+"&svcSeq="+$("#svcList").val());
}


InsertFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

fileFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("msg.common.file.upload.fail"));
}

fileQueueDelete = function(e, fileId){
    $(e).parents("table.fileTable").remove();
    fileTemp.cancelUpload(fileId);
}

function makeBakMove(){
    var str_hash = document.location.hash.replace("#","");
    var bakStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
    var bakSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
    pageMove("/version#svcSeq="+bakSvc+"&storSeq="+bakStore);
}