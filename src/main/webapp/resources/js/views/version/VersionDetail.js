/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */


var arr = new Array();
$(document).ready(function(){
    verInfo();

    if ($("#memberSec").val() == "05" ) {
        // 05 : 매장 관리자, 수정 및 삭제 불가, 조회만 가능
        $(".btnModify").hide();
        $(".btnDelete").hide();
    }
});

verInfo = function(){
    callByGet("/api/version?verSeq="+$("#verSeq").val(), "verInfoSuccess", "NoneForm", "verInfoFail");
}

verInfoSuccess = function(data) {
    if(data.resultCode == "1000") {
        var item = data.result.versionInfo;
        var fileList = data.result.versionInfo.fileList;
        $("#verNm").html(item.verNm);
        $("#cretDt").html(item.cretDt);
        var fileInfo = "";
        if (fileList != "") {
            fileInfo += "<div class='verFileList'><div> " +getMessage("version.table.verfile") +" : </div>";
        }
        $(fileList).each(function(i,item) {
            if (item.fileSeq != 0) {
                fileInfo += "<span class='float_l verfile'>"+item.orginlFileNm+ "</span><a href=\"" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "\" class='fontblack btnDownloadGray'>[다운로드]</a>";
            }
        });
        if (fileList != "") {
            fileInfo+="</div>";
        }

        $("#verHst").html(fileInfo+ getMessage("version.table.type")+ " : "+ item.comnCdNm  +"<br /><br />" +getMessage("version.table.updateHst")+" : " + item.verHst);

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"),"/version");
    }
}

verInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/version");
}

deleteVersion = function(){
    var verSeq = $("#verSeq").val();

    if(verSeq == ""){
        popAlertLayer(getMessage("cms.release.msg.noData"));
        return;
    }

    popConfirmLayer(getMessage("msg.versions.confirm.delete.detail"), function(){
        formData("NoneForm" , "verSeqs", verSeq);
        callByDelete("/api/version", "deleteVersionSuccess", "NoneForm","deleteFail");
    },null,getMessage("common.confirm"));
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteVersionSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"),"/version#page=1&storSeq="+$("#storSeq").val()+"&svcSeq="+$("#svcSeq").val());
    }
}

