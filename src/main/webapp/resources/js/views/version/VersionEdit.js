/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var FILE_VIDEO;
var FILE_THUMBNAIL;
var FILE_METADATA;
var FILE_COVERIMG;
var FILE_CONTENTS;
var FILE_FILE1;
var FILE_PREV;
var FILE_PREVMETA;
var FILE_PREVCONT;
var FILE_SETTINGS   = {
        // Backend Settings
        //upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
    };

//swfupload - file1
file1UploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'version'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.hwp;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.png;*.jpg;*.jpeg;*.bmp;*.gif",
    file_settings.file_queue_limit =  "0";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_FILE1  = new SWFUpload(file_settings);
}

var totalCount;
var checkedName;
var oriName;
var arr = new Array();
var oriSvcSeq = 0;
var oriStorSeq = 0;
var pageReady = false;
$(document).ready(function(){
    limitInputTitle("verNm");

    setkeyup();
    verTypeList();
    file1UploadBtn();
});

verInfo = function(){
    callByGet("/api/version?verSeq="+$("#verSeq").val(), "verInfoSuccess", "NoneForm", "verInfoFail");
}

verInfoSuccess = function(data) {
    if(data.resultCode == "1000") {
        var item = data.result.versionInfo;
        var fileList = data.result.versionInfo.fileList;
        $("#verNm").val(xssChk(item.verNm));
        $("#svcNm").text(item.svcNm);
        oriSvcSeq = item.svcSeq;
        oriStorSeq = item.storSeq;
        storList();
        $("#verType option:contains("+item.comnCdNm+")").attr("selected",true);

        $("#verHst").html(item.verHst);

        var fileHtml = "";
        $(fileList).each(function(i,item){
            fileHtml += "<table class=\"fileTable\"><tr><td><p class=\"fileName\" title=\""+item.orginlFileNm+"\">"+getMessage("contents.file.name")+" : "+item.orginlFileNm+"</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"savedfileDelete(this,'fileArea', "+item.fileSeq+")\" value="+getMessage("button.delete")+"></td></tr></table>";

        });
        $("#fileArea").html(fileHtml);

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"),"/version#page=1&storSeq="+$("#storList").val()+"&svcSeq="+oriSvcSeq);
    }
}

verInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/version");
}

storList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+oriSvcSeq,"storListSuccess","NoneForm");
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        checkedName = "";
        var memberStoreListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(memberStoreListHtml);
        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 서비스 변경 불가
            $("#storList").attr('disabled', 'true');
        }


        if (!pageReady) {
            pageReady = true;
            $("#storList").val(oriStorSeq);
        };
    }  else {
        $("#storList").html("");
    }
}

verTypeList = function(){
    callByGet("/api/codeInfo?comnCdCtg=VERSION","verTypeListSuccess","NoneForm");
}

verTypeListSuccess = function(data){
    if(data.resultCode == "1000"){
        var verTypListHtml = "";
        $(data.result.codeList).each(function(i,item) {
            verTypListHtml += "<option value='"+item.comnCdValue+"'>"+item.comnCdNm+"</option>"
            $("#verType").html(verTypListHtml);
        })
    } else if(data.resultCode == "1010"){
    }

    verInfo();
}

verUpdate = function(){
    if($("#verNm").val().trim() == ""){
        popAlertLayer(getMessage("msg.version.insert.verNm"));
        return;
    }
    if($("#storList").val() == ""){
        popAlertLayer(getMessage("msg.contents.select.store"));
        return;
    }

    if($("#verType").val() == ""){
        popAlertLayer(getMessage("msg.version.select.verType"));
        return;
    }

    if($("#verHst").val().trim() == ""){
        popAlertLayer(getMessage("msg.versions.insert.verHst"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("NoneForm" ,"verSeq", $("#verSeq").val());
        formData("NoneForm" ,"storSeq", $("#storList option:selected").val());
        formData("NoneForm" ,"svcSeq", oriSvcSeq);
        formData("NoneForm" ,"verNm", $("#verNm").val());
        formData("NoneForm" ,"verType", $("#verType option:selected").val());
        formData("NoneForm" ,"verHst", $("#verHst").val());
        formData("NoneForm" ,"dfileSeq", fileSeqs);
        callByPut("/api/version" , "verUpdateSuccess", "NoneForm","updateFail");
    }, null, getMessage("common.confirm"));
}

verUpdateSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if(data.resultCode == "1000"){
        $("#contsSeq").val($("#verSeq").val());
        uploadS();
    }
}



versionfiledeleteSuccess = function(data) {
    if(data.resultCode == "1000"){
        if (typeof($("input[type=file]")[0].files[0]) != "undefined") {
            verfileInput();
        } else {
            popAlertLayer(getMessage("success.common.update"),"/version#page=1&storSeq="+$("#storList").val()+"&svcSeq="+oriSvcSeq);
        }
    }
}


filesubmitSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.update"),"/version#page=1&storSeq="+$("#storList").val()+"&svcSeq="+oriSvcSeq);
    }
}


updateFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

fileFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("msg.common.file.upload.fail"));
}

verfileDel = function() {
    $("input[name=_method]").val("DELETE");
    formData("NoneForm" , "verSeq", $("#verSeq").val());
    callByDelete("/api/versionfile","versionfiledeleteSuccess","NoneForm");
}
verfileInput = function() {
    $("input[name=_method]").val("POST");
    formData("file" , "verSeq", $("#verSeq").val());
    callByMultipart("/api/versionfile","filesubmitSuccess", "file","fileFail");
}

var fileSeqs="";
savedfileDelete = function(e,id, seq){
    $(e).parents(".fileTable").remove();
    fileSeqs += seq+",";
}




function uploadS(){
    if(fileTemp == undefined){
        contentsXmlCreate();
    }
    if(fileTemp != undefined){
        fileTemp.startUpload();
    }
}

fileInsertSuccess = function(data) {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    contentsXmlCreate();
}

fileInsertFail = function() {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    contentsXmlCreate();
}

fileQueueDelete = function(e, fileId){
    $(e).parents(".fileTable").remove();
    fileTemp.cancelUpload(fileId);
}

contentsXmlCreate = function(){
    var msg = getMessage("success.common.update");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }

    popAlertLayer(msg,"/version#page=1&storSeq="+$("#storList").val()+"&svcSeq="+oriSvcSeq);
}

