/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var readyPage = false;
var apiUrl = makeAPIUrl("/api/version");
var lastBind = false;
var gridState;
var hashSvc;
var hashStore;
var totalPage = 0;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
});

$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#svcList").val(hashSvc);
            setSvcList();

        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);
    }

    svcList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });

    if ($("#memberSec").val() == "05" ) {
        // 05 : 매장 관리자, 등록 및 삭제 불가, 조회만 가능
        $(".btnDelete").hide();
        $(".btnWrite").hide();
    }
});

function setStorList(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

        $("#svcList").val(hashSvc);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }
        storList(hashStore);

        gridState = "NONE";
    } else {
        storList(-1);
    }
}

function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {

        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');
        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "verNm":break;
            case "comnCdNm":break;
            default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
                svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }
    }

    setStorList();
}

var stateVal;
storList = function(val){
    stateVal = val;
    var svcListVal = $("#svcList").val();
    if (svcListVal == null) {
        svcListVal = -1
    }
    callByGet("/api/store?searchType=list&svcSeq="+svcListVal,"storListSuccess","NoneForm");
}
var noStorData = false;
storListSuccess = function(data){
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(storeListHtml);
        $("#storList").attr("disabled", false);
        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 서비스 변경 불가
            $("#storList").attr('disabled', 'true');
        }

        if (stateVal != -1) {
            $("#storList").val(stateVal);
        }

        if (!readyPage) {
            readyPage = true;
            versionList();
        } else {
            jQuery("#jqgridData").trigger("reloadGrid");
        }
        $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").unbind( "click" );
    } else {
        $("#storList").html("");
        $("#storList").attr("disabled", true);
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
        jQuery('#jqgridData').jqGrid('clearGridData');
        noStorData = true;
        versionList();
    }
}

setSvcList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"setSvcListSuccess","NoneForm");
}

setSvcListSuccess = function(data){
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(storeListHtml);
        $("#storList").val(hashStore);
        jQuery("#jqgridData").trigger("reloadGrid");
    }
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

function versionList() {
    var multiSel = false;
    if ($("#memberSec").val() != "05" ) {
        multiSel = true;
    }

    $("#jqgridData").jqGrid({
        url: apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.versionList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:[
                getMessage("column.title.num"),
                getMessage("version.table.name"),
                getMessage("version.table.type"),
                getMessage("table.regdate"),
                getMessage("column.title.verSeq")
         ],
        colModel:[
            {name:"num", index: "num", align:"center", width:20},
            {name:"verNm", index:"VER_NM", align:"center",formatter:pointercursor},
            {name:"comnCdNm", index:"COMN_CD_NM", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"verSeq", index:"VER_SEQ", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "verSeq",
            sortorder: "desc",
//          caption:"편성 목록",
            height: "auto",
            multiselect: multiSel,
            beforeRequest:function(){
                tempState = gridState;
                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

                if (gridState == "TABVAL") {
                    searchField = "";
                    searchString = "";
                } else if (gridState != "SEARCH") {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                }

                if (isNaN(page)) {
                   page =1;
                }

                if (totalPage > 0 && totalPage < page) {
                    page = 1;
                }

                searchField = checkUndefined($("#target").val());
                searchString = checkUndefined($("#keyword").val());
                hashSvc = checkUndefined($("#svcList").val());
                hashStore = checkUndefined($("#storList").val());

                if (gridState == "HASH"){
                    myPostData.page = page;
                    tempState = "READY";
                } else {
                    if(tempState == "SEARCH"){
                       myPostData.page = 1;
                    } else {
                        tempState = "";
                    }
                }

                if(gridState == "NONE"){
                    searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                    searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                    myPostData.page = page;

                    if (searchField != null) {
                        tempState = "SEARCH";
                    } else {
                        tempState = "READY";
                    }
                }

                if(searchField != null && searchField != "" && searchString != ""){
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                }else{
                    delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
                }

                gridState = "GRIDBEGIN";

                if (hashSvc != null && hashSvc != "") {
                    myPostData.svcSeq = hashSvc;
                } else {
                    myPostData.svcSeq = "";
                }

                if (hashStore != null && hashStore != "") {
                    myPostData.storSeq = hashStore;
                } else {
                    myPostData.storSeq = "";
                }

                if (noStorData || $("#storList option").index() == -1) {
                    if ($("#storList option").index() == -1) {
                        popAlertLayer(getMessage("info.store.nodata.msg"));
                    }
                    myPostData.page = 0;
                    hashStore = "";
                    noStorData = false;
                }

                $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page });
            },
            loadComplete: function (data) {
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                //session
                if(sessionStorage.getItem("last-url") != null){
                    sessionStorage.removeItem('state');
                    sessionStorage.removeItem('last-url');
                }

                //Search 필드 등록
                searchFieldSet();

                if (data.resultCode == 1000) {
                    totalPage = data.result.totalPage;

                    if (page != data.result.currentPage) {
                        tempState = "";
                    }
                } else {
                    tempState = "";
                }

                if (hashStore == null){
                    hashStore = "";
                }

                /*뒤로가기*/
                var hashlocation = "page="+$(this).context.p.page+"&storSeq="+$("#storList option:selected").val()+"&svcSeq="+$("#svcList option:selected").val()+"&type="+$("#type").val();
                if($(this).context.p.postData._search && $("#target").val() != null){
                    hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
                }

                gridState = "GRIDCOMPLETE";
                document.location.hash = hashlocation;

                if (tempState != "" || str_hash == hashlocation) {
                    gridState = "READY";
                }

              //pageMove Max
                $('.ui-pg-input').on('keyup', function() {
                    this.value = this.value.replace(/\D/g, '');
                    if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
                });

                $(this).find(".pointer").parent("td").addClass("pointer");
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event){
                // 콘텐츠 ID 선택시
                if (multiSel) {
                    if(columnId == 2){
                        var list = $("#jqgridData").jqGrid('getRowData', rowId);
                        sessionStorage.setItem("last-url", location);
                        sessionStorage.setItem("state", "view");
                        pageMove("/version/"+list.verSeq);
                      }
                } else {
                    if(columnId == 1){
                        var list = $("#jqgridData").jqGrid('getRowData', rowId);
                        sessionStorage.setItem("last-url", location);
                        sessionStorage.setItem("state", "view");
                        pageMove("/version/"+list.verSeq);
                      }
                }
            }
    }).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

deleteVersion = function(){
    var verSeq = "";
    var verNms = "";
    for(var i=1; i<$("#limit").val()+1; i++){
        if($("#jqg_jqgridData_"+i).prop("checked")){
            var list = $("#jqgridData").jqGrid('getRowData', i);
            var verNm = $("#jqgridData").jqGrid('getRowData', i).verNm.replace(/(<([^>]+)>)/ig,"");
            if (verNms != "") {
                verNms += ", ";
            }

            verSeq += list.verSeq + ",";
            verNms += verNm;
        }
    }


    if(verSeq == "" || verSeq == 0){
        popAlertLayer(getMessage("cms.release.msg.noData"));
        return;
    }

    popConfirmLayer(verNms +" "+getMessage("msg.versions.confirm.delete"), function(){
        formData("NoneForm" , "verSeqs", verSeq.slice(0,-1));
        callByDelete("/api/version", "deleteVersionSuccess", "NoneForm","deleteFail");
    },null,getMessage("common.confirm"));
}


deleteVersionSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"),"/version#page=1&storSeq="+$("#storList").val()+"&svcSeq="+$("#svcList").val());
    }
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function makePageMove(e){
    if ($("#storList option").index() != -1) {
        pageMove("/version/regist#svcSeq="+$("#svcList").val()+"&storSeq="+$("#storList").val());
    }
}