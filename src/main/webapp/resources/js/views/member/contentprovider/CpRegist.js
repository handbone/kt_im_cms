/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    serviceList();

    limitInputTitle("cpName");

    $("#zipcode").bind("keyup",function(){
        re = /[ㄱ-ㅎㄲ-ㅆㅏ-ㅢㅣ가-힣a-zA-Z~!@\#$%^&*\()\=+_';.,":[]|]/gi;  //한글,영문,숫자
        var temp=$("#zipcode").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#zipcode").val(temp.replace(re,"")); }
    });

    $("#basAddr").bind("keyup",function(){
        re = /[~!@\#$%^&*<>\()\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#basAddr").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#basAddr").val(temp.replace(re,""));
        }
    });

    $("#dtlAddr").bind("keyup",function(){
        re = /[~!@\#$%^&*<>\()\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#dtlAddr").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#dtlAddr").val(temp.replace(re,""));
        }
    });

    var now=new Date();
    $("#contStDt").val(dateString((new Date(now.getTime()))));
    $("#contFnsDt").val(dateString((new Date(now.getTime()))));

    $("#contStDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#contFnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#contFnsDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#contStDt").datepicker("option","maxDate", selected)
        }
    });

    setkeyup();
});

function openDaumPostcode(zip1,addr,addr_sub) {
    new daum.Postcode({
        oncomplete: function(data) {
            document.getElementById(zip1).value = data.zonecode;
            if (data.userSelectedType === 'R') 
            {
                document.getElementById(addr).value = data.roadAddress;
            }
            else
            {
                document.getElementById(addr).value = data.jibunAddress;
            }
            document.getElementById(addr_sub).focus();
        }
    }).open();
}

serviceList = function() {
    callByGet("/api/service?searchType=list","serviceListSuccess","NoneForm");
}

serviceListSuccess = function(data) {
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $(data.result.serviceNmList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\"" + item.svcSeq + "\" value=\"" + item.svcNm + "\" class=\"buttonBox\"/ onclick=\"buttonBoxToggle(this);\"> ";
        });
        $("#serviceList").append(menuHtml);
    } else {
        popAlertLayer(getMessage("info.service.nodata.msg"), "/cp");
    }
}

function buttonBoxToggle(e){
    $(e).toggleClass("buttonBox");
    $(e).toggleClass("buttonBoxOn");
}

function cpRegist() {
    if (checkNull($("#cpName").val()) == "") {
        popAlertLayer(getMessage("cp.name.empty.msg"));
        return;
    }

    var returnState = false;
    $(".telNo").each(function(){
        if (checkNull($(this).val()) == "") {
            returnState = true;
            return;
        }
    });
    if (returnState) {
        popAlertLayer(getMessage("store.tel.no.error.msg"));
        return;
    }

    var telnoStr = $(".telNo").eq(0).val() +"-"+$(".telNo").eq(1).val()+"-"+$(".telNo").eq(2).val();
    if (!validateTelNumber(telnoStr) && !validatePhoneNumber(telnoStr)) {
        popAlertLayer(getMessage("common.telno.error.msg"));
        return;
    }

    if (checkNull($("#zipcode").val()) == "" || checkNull($("#basAddr").val()) == "") {
        popAlertLayer(getMessage("store.address.empty.msg"));
        return;
    }

    $(".bizno").each(function(){
        if (checkNull($(this).val()) == "") {
            returnState = true;
            return;
        }
    });
    if (returnState) {
        popAlertLayer(getMessage("cp.bizno.empty.msg"));
        return;
    }

    var biznoStr = $(".bizno").eq(0).val() +"-"+$(".bizno").eq(1).val()+"-"+$(".bizno").eq(2).val();
    if (!validateBizNo(biznoStr)) {
        popAlertLayer(getMessage("common.bizno.error.msg"));
        return;
    }

    if (checkNull($("#contStDt").val()) == "") {
        popAlertLayer(getMessage("cp.contract.period.start.date.empty.msg"));
        return;
    }

    if (checkNull($("#contFnsDt").val()) == "") {
        popAlertLayer(getMessage("cp.contract.period.finish.date.empty.msg"));
        return;
    }

    var service = "";
    $("#serviceList .buttonBoxOn").each(function(i, item){
        service += $(this).attr("data-seq") + ",";
    });

    if (service == "") {
        popAlertLayer(getMessage("cp.service.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        formData("NoneForm", "cpNm", $("#cpName").val());
        formData("NoneForm", "cpTelNo", telnoStr);
        formData("NoneForm", "zipcode", $("#zipcode").val());
        formData("NoneForm", "basAddr", $("#basAddr").val());
        formData("NoneForm", "dtlAddr", $("#dtlAddr").val());
        formData("NoneForm", "bizno", biznoStr);
        formData("NoneForm", "service", service.slice(0, -1));
        formData("NoneForm", "contStDt", $("#contStDt").val() + " 00:00:00");
        formData("NoneForm", "contFnsDt", $("#contFnsDt").val() + " 23:59:59");

        callByPost("/api/cp" , "cpRegistSuccess", "NoneForm", "insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

cpRegistSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"), "/cp");
    } else if(data.resultCode == "1011") {
        if (data.resultMsg == "exist bizno") {
            popAlertLayer(getMessage("common.bizno.exist.msg"));
        } else if (data.resultMsg == "exist cpNm") {
            popAlertLayer(getMessage("common.cp.name.exist.msg"));
        } else {
            popAlertLayer(getMessage("fail.common.insert"));
        }
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

