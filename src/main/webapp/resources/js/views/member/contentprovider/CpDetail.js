/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    cpInfo();
});

cpInfo = function() {
    callByGet("/api/cp?cpSeq=" + $("#cpSeq").val() , "cpInfoSuccess", "NoneForm", "cpInfoFail");
}

cpInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.cpInfo;
        var serviceListItem = data.result.cpInfo.cpServiceList;

        $("#cpNm").html(item.cpNm);
        $("#cretrId").html(item.cretrId);
        $("#cretDt").html(item.cretDt);
        $("#amdrId").html(item.amdrId);
        $("#amdDt").html(item.amdDt);
        $("#cpTelNo").html(item.cpTelNo);
        $("#cpAddr").html("(" + item.zipcd + ") " + item.cpBasAddr);
        $("#bizNo").html(item.bizno);

        var serviceList = "";
        $(serviceListItem).each(function(i, service) {
            if (serviceList != "") {
                serviceList += ", ";
            }

            serviceList += service.svcNm;
        });
        $("#cprtSvc").html(serviceList);
        $("#contDt").html(item.contDt);

        if (item.useYn == "Y") {
            $("#useYnNm").html(getMessage("common.enable"));
        } else {
            $("#useYnNm").html(getMessage("common.disable"));
        }

        listBack($(".btnRight"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/cp");
    }
}

cpInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/cp");
}

function cpDelete() {
    popConfirmLayer($("#cpNm").text() + " " + getMessage("cp.delete.confirm.msg"), function() {
        formData("NoneForm", "cpSeq", $("#cpSeq").val());
        callByDelete("/api/cp" , "cpDeleteSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

cpDeleteSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/cp");
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

