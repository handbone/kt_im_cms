/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var didCheckId = false;
var listUrl = "/member/waiting";

$(document).ready(function(){

    $("#contents .btnOk").click(function(e) {
        showAcceptanceJoinPopup();
    });

    $("#popupWindow .btnOk").click(function(e) {
        acceptJoin();
    });

    $("#popupWindow .btnCancel").click(function(e) {
        closeAcceptanceJoinPopup();
    });

    $(".btnDeleteLong").click(function(e) {
        confirmDelete();
    });

    var options = {
        "obj" : $(".btnList"),
        "defaultUri" : "/member/waiting",
    };
    setListButton(options);

    getMemberDetail();
});

getMemberDetail = function() {
    formData("NoneForm", "seq", $("#seq").val());
    callByGet("/api/member", "didRecieveMemberDetail", "NoneForm", "didNotRecieveMemberDetail");
    formDataDeleteAll("NoneForm");
}

didRecieveMemberDetail = function(data) {
    if (data.resultCode != "1000") {
        popAlertLayer(getMessage("common.bad.request.msg"), function() { $(".btnList").trigger("click"); });
        return;
    }

    var result = data.result.memberInfo;
    if (result.mbrSe != "06" || result.mbrSttus != "01") {
        popAlertLayer(getMessage("common.bad.request.msg"), function() { $(".btnList").trigger("click"); });
        return;
    }

    $("#memberName").text(result.mbrNm);
    $("#memberId").text(result.mbrId);
    $("#memberSection").text(result.mbrSeNm);
    $("#cretDt").text(result.cretDt);
    $("#memberTelNo").text(result.mbrTelNo);
    $("#memberPhoneNo").text(result.mbrMphonNo);
    var memberPhoneList = "";
    $(result.mbrMphonList).each(function(i, item){
        if (i != 0) {
            memberPhoneList += "<br />";
        }
        memberPhoneList += item.mbrMphonNo;
    });
    $("#memberPhoneNo").html(memberPhoneList);
    $("#memberEmail").text(result.mbrEmail);
    $("#memberStatusName").text(result.mbrSttusNm);

    $(".popupTxtQ").text(result.mbrId + " " + getMessage("member.manage.waiting.accept.msg"));
}

didNotRecieveMemberDetail = function(data) {
    popAlertLayer(getMessage("common.bad.request.msg"), function() { $(".btnList").trigger("click"); });
}

showAcceptanceJoinPopup = function() {
    openPopupLayer("popupWindow");
}

closeAcceptanceJoinPopup = function() {
    closePopupLayer();
    $("input[name=proofDocumentYn]:checked").prop("checked", false);
}

openPopupLayer = function(name) {
    popLayerDiv(name, 500, 300, true);
}

closePopupLayer = function() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}

acceptJoin = function() {
    var hasProofDocumentYn = $("input[name=proofDocumentYn]:checked").val();
    if (hasProofDocumentYn != "Y") {
        popAlertLayer(getMessage("member.manage.alert.proofDocument.msg"));
        return;
    }

    formData("NoneForm", "seq", $("#seq").val());
    formData("NoneForm", "sttus", "02");
    formData("NoneForm", "docSubmYn", hasProofDocumentYn);
    callByPut("/api/member/status", "didUpdateMemberStatus", "NoneForm", "didNotUpdateMemberStatus");
    formDataDeleteAll("NoneForm");
}

didUpdateMemberStatus = function(data) {
    if (data.resultCode == "1000") {
        if ($("#usesAutoSend").val() == "Y") {
            var options = { type : "single", section : "confirmMember", seq : $("#seq").val() };
            sendSms(options);
        }

        popAlertLayer(getMessage("success.common.acceptance"), listUrl);
    } else {
        popAlertLayer(getMessage("fail.common.acceptance"));
    }
}

didNotUpdateMemberStatus = function(data) {
    popAlertLayer(getMessage("fail.common.update"));
}

confirmDelete = function() {
    var title = getMessage("member.manage.alert.delete.title");
    var content = $("#memberId").text() + " " + getMessage("member.manage.alert.delete.confirm");
    popConfirmLayer(content, function() {
        formData("NoneForm", "seq", $("#seq").val());
        formData("NoneForm", "type", "complete");
        callByDelete("/api/member", "didDeleteMember", "NoneForm", "didNotDeleteMember");
        formDataDeleteAll("NoneForm");
    }, null, title);
}

didDeleteMember = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), listUrl);
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

didNotDeleteMember = function(data) {
    popAlertLayer(getMessage("fail.common.delete"));
}