/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var didCheckId = false;
var isAvailableId  = false;

$(document).ready(function(){

    $(".tableForm input[type='text'], .tableForm input[type='password']").blur(function() {
        if (this.id == "memberEmailId" || this.id == "memberEmailDomain") {
            if (!$("#memberEmailId").val() && !$("#memberEmailDomain").val()) {
                $("#memberEmailIdMsg").text("");
                return;
            }

            if (this.id == "memberEmailId" && !$("#memberEmailDomain").val()) {
                return;
            }

            if (this.id == "memberEmailDomain") {
                validate(document.getElementById("memberEmailId"), false);
                return;
            }
        } else if ($(this).hasClass("memberPhoneNo")) {
            validatePhon(this, false);
        } else {
            if (!$(this).val()) {
                $("#" + this.id + "Msg").text("");
                return;
            }
        }

        validate(this, false);
    });

    $(".btnWrite").click(function(e) {
        confirmRegist();
    });

    $("#btnCheckId").click(function(e) {
        checkId();
    });

    $("#memberId").change(function(e) {
        didChangeId();
    });

    $("#memberSection").change(function(e) {
        changeMemberSection();
    });

    $("#memberDomainList").change(function(e) {
        changeMemberEmailBox(this);
    });

    $("#memberTelNo, .memberPhoneNo").keyup(function(e) {
        var value = $(this).val();
        $(this).val(value.replace(/[^0-9\-]/gi,''));
    });

    $("#memberEmailDomain").keyup(function() {
        var domain = $("#memberDomainList option:selected").val();
        if (!domain) {
            return;
        }

        if ($("#memberDomainList option:selected").val() != $("#memberEmailDomain").val()) {
            $("#memberDomainList option:eq(0)").prop("selected", true);
        }
    });

    var options = {
        "obj" : $(".btnCancel"),
        "defaultUri" : "/member"
    };
    setListButton(options);

    addEmailDomainList(document.getElementById("memberDomainList"));

    updateMemberSectionList();

    $("#mPlus").click(function(){
        $("#memberPhoneNoCol").append("<div class='margin_2'><p class='memberPhoneBox'><input type='text' class='inputBox lengthL memberPhoneNo' value=''><input type='button' class='rightIconMius rightBtn' onclick='removePhonNo(this)'><br><span class='cautionTxt'></span></p></div>");
        $(".margin_2:last-child").find("input[type=text]").blur(function() {
           if ($(this).hasClass("memberPhoneNo")) {
                validatePhon(this, false);
            }
            validate(this, false);
            $("#mPlus").css("opacity","0.2").attr( 'disabled', true );
        });
        $("#mPlus").css("opacity","0.2").attr( 'disabled', true );
    });


});

updateMemberSectionList = function() {
    callByGet("/api/codeInfo?comnCdCtg=MBR_SE", "didUpdateMemberSectionList");
}

didUpdateMemberSectionList = function(data) {
    if (data.resultCode != "1000") {
        return;
    }

    var memberSectionListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        if (!isAvailable(item.comnCdValue)) {
            return;
        }
        memberSectionListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });
    $("#memberSection").html(memberSectionListHtml);
    $("#memberSection option:eq(0)").prop("selected", true);

    // 서비스 관리자는 계정구분 selectbox 비활성화
    if ($("#memberSec").val() == "04") {
        $("#memberSection").attr("disabled", true);
    }

    changeMemberSection();
}

function changeMemberSection() {
    updateServiceList();
}

function updateServiceList() {
    var isDisabled = false;
    var shouldHasChangeEventListener = false;

    var section = $("#memberSection option:selected").val();
    switch (section) {
    case "01":
    case "02":
    case "03":
        isDisabled = true;
        break;
    case "04":
        break;
    case "05":
        shouldHasChangeEventListener = true;
        break;
    case "06":
        isDisabled = true;
        break;
    default:
        isDisabled = true;
        shouldHasChangeEventListener = false;
        break;
    }

    if (shouldHasChangeEventListener) {
        $("#memberService").on("change", function() {
            updateStoreList();
        });
    } else {
        $("#memberService").off("change");
    }

    if (isDisabled) {
        $("#memberService").attr("disabled", true);
        $("#memberService").html("<option value=\"\">" + getMessage("select.nothing") + "</option>");
        updateStoreList();
        return;
    }

    $("#memberService").removeAttr("disabled");
    callByGet("/api/service?searchType=list", "didUpdateServiceList");
}

didUpdateServiceList = function(data) {
    if (data.resultCode != "1000") {
        $("#memberService").html("");
        popAlertLayer(getMessage("info.service.nodata.msg"));
        $("#memberService").attr("disabled", true);
        $("#memberStore").attr("disabled", true);
        if ($("#memberSection").val() == "05") {
            $("#memberStore").html("");
        } else if ($("#memberSection").val() == "04") {
            $("#memberStore").html("<option value=\"\">" + getMessage("select.nothing") + "</option>");
            $("#memberStore").attr("disabled", true);
        }

        return;
    } else {
        $("#memberService").attr("disabled", false);
        $("#memberStore").attr("disabled", false);
    }

    var memberServiceListHtml = "";
    $(data.result.serviceNmList).each(function(i, item) {
        memberServiceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
    });
    $("#memberService").html(memberServiceListHtml);
    $("#memberService option:eq(0)").prop("selected", true);

    // 서비스 관리자는 서비스명 selectbox 비활성화
    if ($("#memberSec").val() == "04") {
        $("#memberService").attr("disabled", true);
    }

    updateStoreList();
}

function updateStoreList() {
    var isDisabled = false;
    var shouldHasChangeEventListener = false;

    var section = $("#memberSection option:selected").val();
    switch (section) {
    case "01":
    case "02":
    case "03":
    case "04":
    case "06":
        isDisabled = true;
        break;
    case "05":
        break;
    default:
        isDisabled = true;
        break;
    }

    if (isDisabled) {
        $("#memberStore").html("<option value=\"\">" + getMessage("select.nothing") + "</option>");
        $("#memberStore").attr("disabled", true);
        return;
    }

    $("#memberStore").removeAttr("disabled");
    formData("NoneForm", "searchType", "list");
    formData("NoneForm", "svcSeq", $("#memberService option:selected").val());
    callByGet("/api/store", "didUpdateStoreList", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didUpdateStoreList = function(data) {
    var isDisabled = false;


    var section = $("#memberSection option:selected").val();
    switch (section) {
    case "01":
    case "02":
    case "03":
    case "04":
    case "06":
        isDisabled = true;
        break;
    case "05":
        break;
    default:
        isDisabled = true;
        break;
    }

    if (data.resultCode != "1000") {
        $("#memberStore").attr("disabled", true);
        $("#memberStore").html("");
        popAlertLayer(getMessage("info.store.nodata.msg"));
        return;
    } else if (!isDisabled) {
        $("#memberStore").attr("disabled", false);
    }

    var memberStoreListHtml = "";
    $(data.result.storeNmList).each(function(i, item) {
        memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
    });
    $("#memberStore").html(memberStoreListHtml);
    $("#memberStore option:eq(0)").prop("selected", true);
}

function confirmRegist() {
    if (!validateForm()) {
        return;
    }

    if (!didCheckId) {
        popAlertLayer(getMessage("login.checkId.msg"));
        return;
    }

    if (!isAvailableId) {
        popAlertLayer(getMessage("login.unavailableId.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        register();
    }, null, getMessage("button.confirm"));
}

function register() {
    switch($("#memberSection").val()){
    case "04" :
        if ($("#memberService option").index() == -1){
            popAlertLayer(getMessage("fail.member.noData.store"));
            return;
        }
        break;
    case "05" :
        if ($("#memberStore option").index() == -1){
            popAlertLayer(getMessage("fail.member.noData.store"));
            return;
        }
        break;
    default: break;
    }


    var telNo = phoneNumberFormatter($("#memberTelNo").val());
    var phoneNo = "";
    for (var i=0; i<$(".memberPhoneNo").size(); i++) {
        if (phoneNo != "") {
            phoneNo += ";";
        }
        phoneNo += phoneNumberFormatter($(".memberPhoneNo").eq(i).val());
    }

    formData("NoneForm", "id", $("#memberId").val());
    formData("NoneForm", "pwd1", $("#memberPwd").val());
    formData("NoneForm", "pwd2", $("#memberPwdRe").val());
    formData("NoneForm", "se", $("#memberSection option:selected").val());
    formData("NoneForm", "svcSeq", $("#memberService option:selected").val());
    formData("NoneForm", "storSeq", $("#memberStore option:selected").val());
    formData("NoneForm", "name", $("#memberName").val());
    formData("NoneForm", "tel", telNo);
    formData("NoneForm", "phone", phoneNo);
    formData("NoneForm", "email", $("#memberEmailId").val() + "@" + $("#memberEmailDomain").val());
    formData("NoneForm", "ipadr", $("#memberIpadr").val());
    formData("NoneForm", "ipadr2", $("#memberIpadr2").val());

    callByPost("/api/member" , "didRegistUser", "NoneForm", "didNotRegistUser");
    formDataDeleteAll("NoneForm");
}

function didRegistUser(data) {
    if (data.resultCode == "1000") {
        if ($("#usesAutoSend").val() == "Y") {
            var options = { type : "single", section : "registMember", seq : data.result.mbrSeq, pwd : $("#memberPwd").val() };
            sendSms(options);
        }

        popAlertLayer(getMessage("success.common.insert"), "/member");
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage(data.resultMsg));
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

function didNotRegistUser(data) {
    popAlertLayer(getMessage("fail.common.insert"));
}

function checkId() {
    var isValid = validate(document.getElementById("memberId"));
    if (!isValid) {
        return;
    }

    callByGet("/api/member/check?id=" + $("#memberId").val(), "didReceiveCheckIdReuslt")
}

didReceiveCheckIdReuslt = function(data) {
    didCheckId = true;
    var msg = getMessage("login.availableId.msg");
    if (data.resultCode == "1010") {
        isAvailableId = true;
    } else {
        msg = getMessage("login.unavailableId.msg");
        $("#memberId").focus();
        isAvailableId = false;
    }
    popAlertLayer(msg);
}

function didChangeId() {
    didCheckId  = false;
    isAvailableId = false;
}

function validateForm() {
    var isValid = true;
    $(".tableForm input[type='text'], .tableForm input[type='password']").each(function(i, item) {
        if ($(this).hasClass("memberPhoneNo")) {
            if (!validatePhon(item, true)) {
                isValid = false;
                return false;
            }
        } else {
            if (!validate(item, true)) {
                isValid = false;
                return false;
            }
        }
    });

    if (isValid) {
        if ($("#memberIpadr").val() == $("#memberIpadr2").val()) {
            isValid = false;
            popAlertLayer(getMessage("login.alert.same.ipAddress.msg"), '', '', '', "memberIpadr2");
        }
    }
    return isValid;
}

function validate(obj, usePopup) {

    var id = obj.id;
    var msg = "";
    var msgId = "#" + id + "Msg";

    $(msgId).hide();
    var value = $(obj).val();
    if (!usePopup) {
        if (id == "memberEmailId") {
            value += $("#memberEmailDomain").val();
        }
    }
    if (value.length <= 0) {
        if (id == "memberIpadr2") {
            return true;
        }
        msg = getMessage("login.alert.required.msg");
        if (usePopup) {
            popAlertLayer(msg, '', '', '', id);
        } else {
            $(msgId).text(msg);
            $(msgId).show();
        }
        return false;
    }

    var isValid = true;
    switch (id) {
    case "memberId":
        isValid = validateId($("#memberId").val());
        if (!isValid) {
            msg = getMessage("login.alert.id.msg");
        }
        break;
    case "memberPwd":
        var validResult = validatePassword($("#memberPwd").val(), $("#memberId").val());
        isValid = (validResult === "valid");
        if (!isValid) {
            if (validResult === "continuousChar") {
                msg = getMessage("login.alert.continuous.character.msg");
            } else if (validResult === "containsIdPart") {
                msg = getMessage("login.alert.cotains.id.msg");
            } else {
                msg = getMessage("login.alert.password.msg");
            }
        }
        break;
    case "memberPwdRe":
        isValid = ($("#memberPwd").val() == $("#memberPwdRe").val());
        if (!isValid) {
            $("#memberPwdRe").val("");
            msg = getMessage("login.alert.passwordConfirm.msg");
        }
        break;
    case "memberName":
        isValid =  validateName($("#memberName").val());
        if (!isValid) {
            msg = getMessage("login.alert.name.msg");
        }
        break;
    case "memberTelNo":
        isValid = validateTelNumber($("#memberTelNo").val());
        if (!isValid) {
            msg = getMessage("login.alert.telNo.msg");
        } else {
            $("#memberTelNo").val(phoneNumberFormatter($("#memberTelNo").val()));
        }
        break;
    case "memberPhoneNo":
        isValid = validatePhoneNumber($("#memberPhoneNo").val());
        if (!isValid) {
            msg = getMessage("login.alert.phoneNo.msg");
        } else {
            $("#memberPhoneNo").val(phoneNumberFormatter($("#memberPhoneNo").val()));
        }
        break;
    case "memberEmailId":
        if (!usePopup) {
            isValid =  validateEmail($("#memberEmailId").val() + "@" + $("#memberEmailDomain").val());
        } else {
            isValid =  validateEmailId($("#memberEmailId").val());
        }
        if (!isValid) {
            msg = getMessage("login.alert.email.msg");
        }
        break;
    case "memberEmailDomain":
        if (usePopup) {
            isValid =  validateEmailDomain($("#memberEmailDomain").val());
        }
        if (!isValid) {
            msg = getMessage("login.alert.email.msg");
        }
        break;
    case "memberIpadr":
        isValid =  validateIpAddress($("#memberIpadr").val());
        if (!isValid) {
            msg = getMessage("login.alert.first.ipAddress.msg");
            if ($("#memberIpadr2Msg").text()) {
                msg = msg + " ";
            }
        }
        break;
    case "memberIpadr2":
        isValid =  validateIpAddress($("#memberIpadr2").val());
        if (!isValid) {
            msg = getMessage("login.alert.second.ipAddress.msg");
            if ($("#memberIpadrMsg").text()) {
                msg = " " + msg;
            }
        } else {
            if ($("#memberIpadr").val() == $("#memberIpadr2").val()) {
                isValid = false;
                msg = getMessage("login.alert.same.ipAddress.msg");
            }
        }
        break;
    default:
        console.log("Nothing id = " + id);
        break;
    }

    if (!isValid) {
        if (usePopup) {
            popAlertLayer(msg, '', '', '', id);
        } else {
            $(msgId).text(msg);
            $(msgId).show();
        }
    }
    return isValid;
}

function validatePhon(obj, usePopup) {
    var msg = "";
    var msgSpan = $(obj).parents("p").find("span");

    $(msgSpan).hide();
    var value = $(obj).val();

    if (value.length <= 0) {
        msg = getMessage("login.alert.required.msg");
        if (usePopup) {
            popAlertLayerObj(msg, '', '', '', obj);
        } else {
            $(msgSpan).text(msg);
            $(msgSpan).show();
        }
        return false;
    }

    var isValid = true;
    isValid = validatePhoneNumber($(obj).val());
    if (!isValid) {
        msg = getMessage("login.alert.phoneNo.msg");
    } else {
        $(obj).val(phoneNumberFormatter($(obj).val()));
    }

    if (!isValid) {
        if (usePopup) {
            popAlertLayerObj(msg, '', '', '', obj);
        } else {
            $(msgSpan).text(msg);
            $(msgSpan).show();
        }
    }
    return isValid;
}



function isAvailable(value) {
    var isAvailable = false;
    switch ($("#memberSec").val()) {
    case "01": // MASTER
        if (value == "06") {
            isAvailable = false;
        } else {
            isAvailable = true;
        }
        break;
    case "02": // CMS
        if (value == "01" || value == "06") {
            isAvailable = false;
        } else {
            isAvailable = true;
        }
        break;
    case "04": // Service 관리자
        if (value == "05") {
            isAvailable = true;
        } else {
            isAvailable = false;
        }
        break;
    default:
        break;
    }
    return isAvailable;
}

function changeMemberEmailBox(obj) {
    $("#memberEmailDomain").val(obj.value);
}

function removePhonNo(e) {
    $(e).parents("p").remove();
    $("#mPlus").css("opacity","1").attr( 'disabled', false );
}