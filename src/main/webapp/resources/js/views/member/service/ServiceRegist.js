/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var re;
$(document).ready(function(){
    limitInputTitle("cpName");

    $("#zipcd").bind("keyup",function(){
        re = /[ㄱ-ㅎㄲ-ㅆㅏ-ㅢㅣ가-힣a-zA-Z~!@\#$%^&*\()\=+_';.,":[]|]/gi;  //한글,영문,숫자
        var temp=$("#zipcd").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#zipcd").val(temp.replace(re,"")); }
    });

    $("#basAddr").bind("keyup",function(){
        re = /[~!@\#$%^&*<>\()\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#basAddr").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#basAddr").val(temp.replace(re,"")); }
    });

    $("#dtlAddr").bind("keyup",function(){
        re = /[~!@\#$%^&*\()<>\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#basAddr").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#dtlAddr").val(temp.replace(re,"")); }
    });

    // 서비스 구분 선택에 따른 미러링 앱 여부 선택 표시
    $("input[name=svcType]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "ON") {
            $("#mirrYn").show();
        } else {
            $("input[name=appType]:input[value='SA']").attr("checked", true);
            $("#mirrYn").hide();
        }
    });

    setkeyup();
});

function openDaumPostcode(zip1,addr,addr_sub) {
    new daum.Postcode({
        oncomplete: function(data) {
            document.getElementById(zip1).value = data.zonecode;
            if (data.userSelectedType === 'R') {
                document.getElementById(addr).value = data.roadAddress;
            } else {
                document.getElementById(addr).value = data.jibunAddress;
            }
            document.getElementById(addr_sub).focus();
        }
    }).open();
}

svcRegist = function(){
    if (checkNull($("#svcNm").val()) == "") {
        popAlertLayer(getMessage("fail.service.null.svcNm"));
        return;
    }
    var returnState = false;
    $(".telNo").each(function(){
        if (checkNull($(this).val()) == "") {
            returnState = true;
            return false;
        }
    });
    if (returnState) {
        popAlertLayer(getMessage("fail.service.null.telNo"));
        return;
    }

    var telnoStr = $(".telNo").eq(0).val() +"-"+$(".telNo").eq(1).val()+"-"+$(".telNo").eq(2).val();
    if (!validateTelNumber(telnoStr) && !validatePhoneNumber(telnoStr)) {
        popAlertLayer(getMessage("common.telno.error.msg"));
        return;
    }

    if (checkNull($("#zipcd").val()) == "") {
        popAlertLayer(getMessage("fail.service.null.zipcd"));
        return;
    }

    if (checkNull($("#basAddr").val()) == "") {
        popAlertLayer(getMessage("fail.service.null.addr"));
        return;
    }

    $(".bizno").each(function(){
        if (checkNull($(this).val()) == "") {
            returnState = true;
            return false;
        }
    });
    if (returnState) {
        popAlertLayer(getMessage("fail.service.null.bizno"));
        return;
    }

    var biznoStr = $(".bizno").eq(0).val() +"-"+$(".bizno").eq(1).val()+"-"+$(".bizno").eq(2).val();
    console.log(">> isValidBizno : " + validateBizNo(biznoStr));
    if (!validateBizNo(biznoStr)) {
        popAlertLayer(getMessage("common.bizno.error.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        formData("NoneForm", "svcNm", $("#svcNm").val());
        formData("NoneForm", "telNo", telnoStr);
        formData("NoneForm", "bizno", biznoStr);
        formData("NoneForm", "zipcd", $("#zipcd").val());
        formData("NoneForm", "basAddr", $("#basAddr").val());
        formData("NoneForm", "dtlAddr", $("#dtlAddr").val());
        formData("NoneForm", "svcType", $("input[name=svcType]:checked").val());
        formData("NoneForm", "appType", $("input[name=appType]:checked").val());

        callByPost("/api/service" , "serviceInsertSuccess", "NoneForm","InsertFail");
    }, null, getMessage("common.confirm"));
}

InsertFail = function(data){
    popAlertLayer(getMessage("fail.common.insert"));
    formDataDeleteAll("NoneForm");
}

serviceInsertSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"),"/service");
    } else if(data.resultCode == "1011") {
        if (data.resultMsg == "exist bizno") {
            popAlertLayer(getMessage("common.bizno.exist.msg"));
        } else if (data.resultMsg == "exist svcNm") {
            popAlertLayer(getMessage("common.service.name.exist.msg"));
        } else {
            popAlertLayer(getMessage("fail.common.insert"));
        }
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }

}

