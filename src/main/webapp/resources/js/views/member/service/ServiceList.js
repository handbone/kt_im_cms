/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var readyPage = false;
var apiUrl = makeAPIUrl("/api/service");
var lastBind = false;
var gridState;
var tempState;
var totalPage = 0;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
})
$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            jQuery("#jqgridData").trigger("reloadGrid");
        } else {
            gridState = "READY";
        }
    });
})
$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    svcList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

function svcList(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");

        gridState = "NONE";
    }
    $("#jqgridData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.serviceList",
            records: "result.totalCount",
            repeatitems: false
        },

        colNames:[getMessage("column.title.num"),getMessage("column.title.seviceNm"),getMessage("column.title.telNo"),getMessage("column.title.addr"),getMessage("column.title.useYn"),getMessage("column.title.mirrYn"),getMessage("column.title.cretDt"),getMessage("column.title.svcSeq")],
        colModel: [
            {name:"num", index: "num", align:"center", width:20,hidden:false},
            {name:"svcNm", index:"SVC_NM", align:"center",formatter:pointercursor},
            {name:"telNo", index:"TEL_NO", align:"center"},
            {name:"basAddr", index:"BAS_ADDR", align:"center"},
            {name:"useYn", index:"USE_YN", align:"center"},
            {name:"mirrYn", index:"MIRR_YN", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"svcSeq", index:"SVC_SEQ", hidden:true}
       ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "contsSeq",
            sortorder: "desc",
//          caption:"편성 목록",
            height: "auto",
            multiselect: false,
            beforeRequest:function(){
                tempState = gridState;
                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                if (isNaN(page)) {
                    page =1;
                }

                if (totalPage > 0 && totalPage < page) {
                    page = 1;
                }

                searchField = checkUndefined($("#target").val());
                searchString = checkUndefined($("#keyword").val());

                if (gridState == "HASH"){
                    myPostData.page = page;
                    tempState = "READY";
                } else {
                    if(tempState == "SEARCH"){
                        myPostData.page = 1;
                    } else {
                        tempState = "";
                    }
                }

                if(gridState == "NONE"){
                    searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                    searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                    myPostData.page = page;

                    if (searchField != null) {
                        tempState = "SEARCH";
                    } else {
                        tempState = "READY";
                    }
                }

                if(searchField != null && searchField != "" && searchString != ""){
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                }else{
                    delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
                }

                gridState = "GRIDBEGIN";

                $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData });
            },
            loadComplete: function (data) {
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                //session
                if(sessionStorage.getItem("last-url") != null){
                    sessionStorage.removeItem('state');
                    sessionStorage.removeItem('last-url');
                }

                //Search 필드 등록
                searchFieldSet();

                if (data.resultCode == 1000) {
                    totalPage = data.result.totalPage;

                    if (page != data.result.currentPage) {
                        tempState = "";
                    }
                } else {
                    tempState = "";
                }

                /*뒤로가기*/
                var hashlocation = "page="+$(this).context.p.page;
                if($(this).context.p.postData._search && $("#target").val() != null){
                    hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
                }

                $(this).find("td").each(function(){
                    if($(this).index() == 4 && $(this).text() != ""){ // 구분
                        var str = $(this).text();

                        if(str == "Y"){
                            str = getMessage("common.enable");
                        } else {
                            str = getMessage("common.disable");
                        }
                        $(this).html(str);
                    }
                })

                gridState = "GRIDCOMPLETE";
                document.location.hash = hashlocation;

                if (tempState != "" || str_hash == hashlocation) {
                    gridState = "READY";
                }

                $(this).find(".pointer").parent("td").addClass("pointer");
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event){
                // 콘텐츠 ID 선택시
                if(columnId == 1){
                  var list = $("#jqgridData").jqGrid('getRowData', rowId);
                  sessionStorage.setItem("last-url", location);
                  sessionStorage.setItem("state", "view");
                  pageMove("/service/"+list.svcSeq);
                }
            }
    }).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {

        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "svcNm":break;
            default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }


    }
    $("#target").val(searchField);
}

