/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var re;
$(document).ready(function(){
    limitInputTitle("cpName");

    $("#zipcd").bind("keyup",function(){
        re = /[ㄱ-ㅎㄲ-ㅆㅏ-ㅢㅣ가-힣a-zA-Z~!@\#$%^&*\()\=+_';.,":[]|]/gi;  //한글,영문,숫자
        var temp=$("#zipcd").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#zipcd").val(temp.replace(re,"")); }
    });

    $("#basAddr").bind("keyup",function(){
        re = /[~!@\#$%^&*<>\()\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#basAddr").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#basAddr").val(temp.replace(re,"")); }
    });

    $("#dtlAddr").bind("keyup",function(){
        re = /[~!@\#$%^&*\()<>\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#basAddr").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#dtlAddr").val(temp.replace(re,"")); }
    });

    // 서비스 구분 선택에 따른 미러링 앱 여부 선택 표시
    $("input[name=svcType]").change(function() {
        var radioValue = $(this).val();
        if (radioValue == "ON") {
            $("#mirrYn").show();
        } else {
            $("input[name=appType]:input[value='SA']").attr("checked", true);
            $("#mirrYn").hide();
        }
    });

    setkeyup();
});

function initFindUserInfo(data) {
    $("#loading").hide();
    if (data.resultCode != "1000") {
        $("#mbrTelNo").val("");
        $("#mbrMphonNo").val("");
        $("#mbrEmailId").val("");
        $("#mbrEmailDomain").val("");
        $("#domainList").val("");

        var mvP = sessionStorage.getItem("cofirm-last-url");

        if (typeof mvP == "undefined") {
            mvP = "/service";
        }
        $(".inputBox").val("");
        popAlertLayer(getMessage("common.bad.request.msg"), mvP);
    } else {
        svcInfo();
    }
}

function initNotFindUserInfo(data) {
    $("#loading").hide();

    var mvP = sessionStorage.getItem("cofirm-last-url");

    if (typeof mvP == "undefined") {
        mvP = "/service";
    }
    $(".inputBox").val("");
    popAlertLayer(getMessage("common.bad.request.msg"), mvP);
//    back
}

function openDaumPostcode(zip1,addr,addr_sub) {
    new daum.Postcode({
        oncomplete: function(data) {
            document.getElementById(zip1).value = data.zonecode;
            if (data.userSelectedType === 'R') {
                document.getElementById(addr).value = data.roadAddress;
            } else {
                document.getElementById(addr).value = data.jibunAddress;
            }
            document.getElementById(addr_sub).value = "";
            document.getElementById(addr_sub).focus();
        }
    }).open();
}

svcEdit = function(){
    if (checkNull($("#svcNm").val()) == "") {
        popAlertLayer(getMessage("fail.service.null.svcNm"));
        return;
    }
    var returnState = false;
    $(".telNo").each(function(){
        if (checkNull($(this).val()) == "") {
            returnState = true;
            return false;
        }
    });
    if (returnState) {
        popAlertLayer(getMessage("fail.service.null.telNo"));
        return;
    }

    var telnoStr = $(".telNo").eq(0).val() +"-"+$(".telNo").eq(1).val()+"-"+$(".telNo").eq(2).val();
    if (!validateTelNumber(telnoStr) && !validatePhoneNumber(telnoStr)) {
        popAlertLayer(getMessage("common.telno.error.msg"));
        return;
    }

    if (checkNull($("#zipcd").val()) == "") {
        popAlertLayer(getMessage("fail.service.null.zipcd"));
        return;
    }

    if (checkNull($("#basAddr").val()) == "") {
        popAlertLayer(getMessage("fail.service.null.addr"));
        return;
    }

    $(".bizno").each(function(){
        if (checkNull($(this).val()) == "") {
            returnState = true;
            return false;
        }
    });
    if (returnState) {
        popAlertLayer(getMessage("fail.service.null.bizno"));
        return;
    }

    var biznoStr = $(".bizno").eq(0).val() +"-"+$(".bizno").eq(1).val()+"-"+$(".bizno").eq(2).val();
    console.log(">> isValidBizno : " + validateBizNo(biznoStr));
    if (!validateBizNo(biznoStr)) {
        popAlertLayer(getMessage("common.bizno.error.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function(){
        formData("NoneForm", "svcSeq", $("#svcSeq").val());
        formData("NoneForm", "svcNm", $("#svcNm").val());
        formData("NoneForm", "telNo", telnoStr);
        formData("NoneForm", "bizno", biznoStr);
        formData("NoneForm", "zipcd", $("#zipcd").val());
        formData("NoneForm", "basAddr", $("#basAddr").val());
        formData("NoneForm", "dtlAddr", $("#dtlAddr").val());
        formData("NoneForm", "useYn", $("input[name=useYn]:checked").val());
        formData("NoneForm", "svcType", $("input[name=svcType]:checked").val());
        formData("NoneForm", "appType", $("input[name=appType]:checked").val());

        callByPut("/api/service" , "serviceEditSuccess", "NoneForm","updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data){
    popAlertLayer(getMessage("fail.common.update"));
    formDataDeleteAll("NoneForm");
}

serviceEditSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"),"/service");
    } else if(data.resultCode == "1011") {
        if (data.resultMsg == "exist bizno") {
            popAlertLayer(getMessage("common.bizno.exist.msg"));
        } else if (data.resultMsg == "exist svcNm") {
            popAlertLayer(getMessage("common.service.name.exist.msg"));
        } else {
            popAlertLayer(getMessage("fail.common.update"));
        }
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

svcInfo = function(){
    callByGet("/api/service?svcSeq="+$("#svcSeq").val() + "&getType=edit", "svcInfoSuccess", "NoneForm", "svcInfoFail");
}

svcInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.serviceInfo;

        $("#svcNm").val(xssChk(item.svcNm));
        $("#basAddr").val(item.basAddr);
        $("#dtlAddr").val(item.dtlAddr);
        $("#zipcd").val(item.zipcd);

        $("input[name=svcType]:input[value='"+item.svcType+"']").attr("checked", true);

        if (item.svcType == "ON") {
            $("#mirrYn").show();
            $("input[name=appType]:input[value='"+item.appType+"']").attr("checked", true);
        }

        /*
        var svcType = item.svcType;
        if (svcType == "ON") {
            svcType = getMessage("service.table.svcType.on");
            $("#mirrYn").show();
            $("#mirrYnVal").html(item.mirrYn);
        } else {
            svcType = getMessage("service.table.svcType.off");
        }
        $("#svcType").html(svcType);
        */

        $("input[name=useYn]:input[value='"+item.useYn+"']").attr("checked", true);

        var str = item.useYn;

        if (str == "Y") {
            str = getMessage("common.enable");
        } else {
            str = getMessage("common.disable");
        }
        $("#useYn").html(str);

        var str = item.telNo;
        var strVal = str.split("-");

        for(var i=0; i < strVal.length; i++){
            $(".telNo").eq(i).val(strVal[i]);
        }

        str = item.bizno;
        strVal = str.split("-");

        for(var i=0; i < strVal.length; i++){
            $(".bizno").eq(i).val(strVal[i]);
        }

        listBack($(".btnCancel"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/service");
    }
}

svcInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/service");
}
