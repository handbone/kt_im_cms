/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
$(document).ready(function(){
    svcInfo();
});


svcInfo = function(){
    callByGet("/api/service?svcSeq="+$("#svcSeq").val(), "svcInfoSuccess", "NoneForm", "svcInfoFail");
}

svcInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.serviceInfo;

        $("#svcNm").html(item.svcNm);
        $("#cretrId").html(item.cretrId);
        $("#cretDt").html(item.cretDt);
        $("#amdrId").html(item.amdrId);
        $("#amdDt").html(item.amdDt);
        var addr = "("+item.zipcd+") "+item.basAddr +" " +item.dtlAddr;
        $("#addr").html(addr);
        $("#bizno").html(item.bizno);

        var svcType = item.svcType;
        if (svcType == "ON") {
            svcType = getMessage("service.table.svcType.on");
            $("#mirrYn").show();
            $("#mirrYnVal").html(item.mirrYn);
        } else {
            svcType = getMessage("service.table.svcType.off");
        }
        $("#svcType").html(svcType);

        var str = item.useYn;

        if (str == "Y") {
            str = getMessage("common.enable");
        } else {
            str = getMessage("common.disable");
        }
        $("#useYn").html(str);
        $("#telNo").html(item.telNo);


        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/service");
    }
}

svcInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/service");
}

svcDelete = function(){
    popConfirmLayer(getMessage("msg.common.confirm.delete"), function(){
        formData("NoneForm", "svcSeq", $("#svcSeq").val());
        formData("NoneForm", "delYn", "Y");
        formData("NoneForm", "useYn", "N");
        callByDelete("/api/service", "svcDeleteSuccess", "NoneForm","deleteFail");
    });
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

svcDeleteSuccess = function(data){
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"),"/service/");
    } else {
        formDataDeleteAll("NoneForm");
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

