/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var memberInfo = {};
var didFirstLayout = false;
var changedField = {};
var mailOriInfo= "";
var delTelList = new Array();
$(document).ready(function() {

    $(".tableForm input[type='text']").blur(function(e) {
        if (!$(this).val()) {
            if (restoreTextIfNeeded(e.target)) {
                return;
            }
        }

        if (this.id == "memberEmailId" || this.id == "memberEmailDomain") {
            if (!$("#memberEmailId").val() && !$("#memberEmailDomain").val()) {
                $("#memberEmailIdMsg").text("");
                return;
            }

            if (this.id == "memberEmailId" && !$("#memberEmailDomain").val()) {
                return;
            }

            if (this.id == "memberEmailDomain") {
                validate(document.getElementById("memberEmailId"), false);
                return;
            }
        } else if ($(this).hasClass("memberPhoneNo")) {
            validatePhon(this, false);
        } else {
            if (!$(this).val()) {
                $("#" + this.id + "Msg").text("");
                return;
            }
        }

        validate(this, false);
    });

    $(".btnModify").click(function(e) {
        confirmModify();
    });

    $(".btnResetWhite").click(function(e) {
        confirmInitPassword();
    });

    $("input[type=text]").focus(function(e) {
        removeTextIfNeeded(e.target);
    });

    $("input[type=text]").keydown(function(e) {
        if (e.keyCode == 8) {
            addChangedField(e.target);
        }
    });

    $("input[type=text]").change(function(e) {
        addChangedField(e.target);
    });

    $("#memberSection").change(function(e) {
        changeMemberSection();
    });

    $("#memberDomainList").change(function(e) {
        changeMemberEmailBox(this);
    });

    $("#memberTelNo, .memberPhoneNo").keyup(function(e) {
        var value = $(this).val();
        $(this).val(value.replace(/[^0-9\-]/gi,''));
    });

    $("#memberEmailDomain").keyup(function() {
        var domain = $("#memberDomainList option:selected").val();
        if (!domain) {
            return;
        }

        if ($("#memberDomainList option:selected").val() != $("#memberEmailDomain").val()) {
            $("#memberDomainList option:eq(0)").prop("selected", true);
        }
    });

    var options = {
        "obj" : $(".btnCancel"),
        "defaultUri" : "/member"
    };
    setListButton(options);

    addEmailDomainList(document.getElementById("memberDomainList"));

    $("#mPlus").click(function(){
        $("#memberPhoneNoCol").append("<div class='margin_2'><p class='memberPhoneBox'><input type='text' id='memberPhoneNo"+$(".memberPhoneNo").size()+"' class='inputBox lengthL memberPhoneNo' value=''><input type='hidden' id='memberPhoneNoOri"+$(".memberPhoneNo").size()+"' /><input type='button' class='rightIconMius rightBtn' onclick='removePhonNo(this)'><br><span class='cautionTxt'></span></p></div>");
        addChangedField($("#memberPhoneNo"+($(".memberPhoneNo").size()-1)));
        $("#memberPhoneNo"+($(".memberPhoneNo").size()-1)).focus(function(e) {
            removeTextIfNeeded(e.target);
        });
        $("#memberPhoneNo"+($(".memberPhoneNo").size()-1)).blur(function() {
           if ($(this).hasClass("memberPhoneNo")) {
                validatePhon(this, false);
            }
            validate(this, false);
        });
        $("#memberPhoneNo"+($(".memberPhoneNo").size()-1)).change(function(e) {
            addChangedField(e.target);
        });
        $("#mPlus").css("opacity","0.2").attr( 'disabled', true );
    });


});

function initFindUserInfo(data) {
    $("#loading").hide();
    if (data.resultCode != "1000") {
        $("#mbrTelNo").val("");
        $("#mbrMphonNo").val("");
        $("#mbrEmailId").val("");
        $("#mbrEmailDomain").val("");
        $("#domainList").val("");

        var mvP = sessionStorage.getItem("cofirm-last-url");

        if (typeof mvP == "undefined") {
            mvP = "/member";
        }
        $(".inputBox").val("");
        popAlertLayer(getMessage("common.bad.request.msg"), mvP);
    } else {
        getMemberDetail();
    }
}

function initNotFindUserInfo(data) {
    $("#loading").hide();

    var mvP = sessionStorage.getItem("cofirm-last-url");

    if (typeof mvP == "undefined") {
        mvP = "/member";
    }
    $(".inputBox").val("");
    popAlertLayer(getMessage("common.bad.request.msg"), mvP);
//    back
}

getMemberDetail = function() {
    formData("NoneForm", "seq", $("#seq").val());
    formData("NoneForm", "getType", "edit");
    callByGet("/api/member", "didRecieveMemberDetail", "NoneForm", "didNotRecieveMemberDetail");
    formDataDeleteAll("NoneForm");
}

didRecieveMemberDetail = function(data) {
    if (data.resultCode != "1000") {
        popAlertLayer(getMessage("common.bad.request.msg"), function() { $(".btnCancel").trigger("click"); });
        return;
    }

    var result = data.result.memberInfo;
    if (result.mbrSttus != "02" && result.mbrSttus != "04") {
        popAlertLayer(getMessage("common.bad.request.msg"), function() { $(".btnCancel").trigger("click"); });
        return;
    }

    mailOriInfo = result.mbrEmail;

    $("#memberNameOri").val(xssChk(result.mbrNm));
    $("#memberTelNoOri").val(result.mbrTelNo);
    $("#memberPhoneNoOri").val(result.mbrMphonNo);
    var emailArr = result.mbrEmail.split("@");
    $("#memberEmailIdOri").val(emailArr[0]);
    $("#memberEmailDomainOri").val(emailArr[1]);
    if (result.mbrSe != "06") {
        $("#memberIpadrOri").val(result.ipadr);
        $("#memberIpadr2Ori").val(result.ipadr2);
    }

    $("#memberId").text(result.mbrId);
    $("#memberName").val(getMaskText(document.getElementById("memberName")));
    $("#memberTelNo").val(getMaskText(document.getElementById("memberTelNo")));
    $("#memberPhoneNo").val(getMaskText(document.getElementById("memberPhoneNo")));

    $(result.mbrMphonList).each(function(i, item){
        if (i != 0) {
            $("#mPlus").css("opacity","0.2").attr('disabled', true );
            $("#memberPhoneNoCol").append("<div class='margin_2'><p class='memberPhoneBox'><input type='text' id='memberPhoneNo"+i+"' class='inputBox lengthL memberPhoneNo' value="+item.mbrMphonNo+"><input type='hidden' id='memberPhoneNoOri"+i+"' value='"+item.mbrMphonNo+"'/><input type='hidden' class='oriTelList' value="+(i+1)+" /><input type='button' class='rightIconMius rightBtn' onclick='removePhonNo(this)'><br><span class='cautionTxt'></span></p></div>");
            $("#memberPhoneNo"+i).focus(function(e) {
                removeTextIfNeeded(e.target);
            });
            $("#memberPhoneNo"+i).blur(function(e) {
                if (!$(this).val()) {
                    if (restoreTextIfNeeded(e.target)) {
                        return;
                    } else {
                        validatePhon(this, false);
                    }
                } else {
                    validatePhon(this, false);
                }
            });

            $("#memberPhoneNo"+i).change(function(e) {
                addChangedField(e.target);
            });
        } else {
            $(".memberPhoneNo").eq(i).val(item.mbrMphonNo);
            $("#memberPhoneNoOri0").val(item.mbrMphonNo);
        }
    });

    $("#memberEmailId").val(getMaskText(document.getElementById("memberEmailId")));
    $("#memberEmailDomain").val($("#memberEmailDomainOri").val());
    if (result.mbrSe != "06") {
        $("#memberIpadr").parent().parent().show();
        $("#memberIpadr").val(getMaskText(document.getElementById("memberIpadr")));
        $("#memberIpadr2").parent().parent().show();
        $("#memberIpadr2").val(getMaskText(document.getElementById("memberIpadr2")));
    }

    memberInfo = data.result.memberInfo;

    initForm();
}

didNotRecieveMemberDetail = function(data) {
    popAlertLayer(getMessage("common.bad.request.msg"), function() { $(".btnCancel").trigger("click"); });
}

initForm = function() {
    // CP사 관계자 계정의 경우 계정구분 및 CP사 명 변경 불가
    var isContentProvider = (memberInfo.mbrSe == "06");
    if (isContentProvider) {
        $("#memberSection").attr("disabled", true);
        $("#memberSection").html("<option value=\"" + memberInfo.mbrSe + "\">" + memberInfo.mbrSeNm + "</option>");

        $("#memberCp").parent().parent().show();
        $("#memberCp").html("<option value=\"" + memberInfo.cpSeq + "\">" + memberInfo.cpNm + "</option>");
        return;
    }

    $("#memberService").parent().parent().show();
    $("#memberStore").parent().parent().show();
    updateMemberSectionList();
}

updateMemberSectionList = function() {
    callByGet("/api/codeInfo?comnCdCtg=MBR_SE", "didUpdateMemberSectionList");
}

didUpdateMemberSectionList = function(data) {
    if (data.resultCode != "1000") {
        return;
    }

    var memberSectionListHtml = "";
    $(data.result.codeList).each(function(i, item) {
        if (!isAvailable(item.comnCdValue)) {
            return;
        }
        memberSectionListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });

    $("#memberSection").html(memberSectionListHtml);

    if (!didFirstLayout) {
        $("#memberSection option[value=" + memberInfo.mbrSe + "]").prop("selected", true);
    }

    // 서비스 관리자일 경우 계정구분 selectbox 비활성화
    if ($("#memberSec").val() == "04") {
        $("#memberSection").attr("disabled", true);
    }

    updateServiceList();
}

function changeMemberSection() {
    updateServiceList();
}

function updateServiceList() {
    var isDisabled = false;
    var shouldHasChangeEventListener = false;

    var section = $("#memberSection option:selected").val();
    switch (section) {
    case "01":
    case "02":
    case "03":
        isDisabled = true;
        break;
    case "04":
        break;
    case "05":
        shouldHasChangeEventListener = true;
        break;
    case "06":
        isDisabled = true;
        break;
    default:
        isDisabled = true;
        shouldHasChangeEventListener = false;
        break;
    }

    if (shouldHasChangeEventListener) {
        $("#memberService").on("change", function() {
            updateStoreList();
        });
    } else {
        $("#memberService").off("change");
    }

    if (isDisabled) {
        $("#memberService").attr("disabled", true);
        $("#memberService").html("<option value=\"\">" + getMessage("select.nothing") + "</option>");
        updateStoreList();
        return;
    }

    $("#memberService").removeAttr("disabled");
    callByGet("/api/service?searchType=list", "didUpdateServiceList");
}

didUpdateServiceList = function(data) {
    if (data.resultCode != "1000") {
        $("#memberService").html("");
        popAlertLayer(getMessage("info.service.nodata.msg"));
        $("#memberService").attr("disabled", true);
        $("#memberStore").attr("disabled", true);
        if ($("#memberSection").val() == "05") {
            $("#memberStore").html("");
        } else if ($("#memberSection").val() == "04") {
            $("#memberStore").html("<option value=\"\">" + getMessage("select.nothing") + "</option>");
            $("#memberStore").attr("disabled", true);
        }

        return;
    } else {
        $("#memberService").attr("disabled", false);
        $("#memberStore").attr("disabled", false);
    }

    var memberServiceListHtml = "";
    $(data.result.serviceNmList).each(function(i, item) {
        memberServiceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
    });
    $("#memberService").html(memberServiceListHtml);
    if (!didFirstLayout) {
        $("#memberService option[value=" + memberInfo.svcSeq + "]").prop("selected", true);
    }

    // 서비스 관리자일 경우 서비스명 selectbox 비활성화
    if ($("#memberSec").val() == "04") {
        $("#memberService").attr("disabled", true);
    }

    updateStoreList();
}

function updateStoreList() {
    var isDisabled = false;
    var shouldHasChangeEventListener = false;

    var section = $("#memberSection option:selected").val();
    switch (section) {
    case "01":
    case "02":
    case "03":
    case "04":
    case "06":
        isDisabled = true;
        break;
    case "05":
        break;
    default:
        isDisabled = true;
        break;
    }

    if (isDisabled) {
        $("#memberStore").html("<option value=\"\">" + getMessage("select.nothing") + "</option>");
        $("#memberStore").attr("disabled", true);
        if (!didFirstLayout) {
            didFirstLayout = true;
        }
        return;
    }

    $("#memberStore").removeAttr("disabled");
    formData("NoneForm", "searchType", "list");
    formData("NoneForm", "svcSeq", $("#memberService option:selected").val());
    callByGet("/api/store", "didUpdateStoreList", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didUpdateStoreList = function(data) {
    var isDisabled = false;


    var section = $("#memberSection option:selected").val();
    switch (section) {
    case "01":
    case "02":
    case "03":
    case "04":
    case "06":
        isDisabled = true;
        break;
    case "05":
        break;
    default:
        isDisabled = true;
        break;
    }

    if (data.resultCode != "1000") {
        $("#memberStore").attr("disabled", true);
        $("#memberStore").html("");
        popAlertLayer(getMessage("info.store.nodata.msg"));
        return;
    } else if (!isDisabled) {
        $("#memberStore").attr("disabled", false);
    }

    var memberStoreListHtml = "";
    $(data.result.storeNmList).each(function(i, item) {
        memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
    });
    $("#memberStore").html(memberStoreListHtml);
    if (!didFirstLayout) {
        didFirstLayout = true;
        $("#memberStore option[value=" + memberInfo.storSeq + "]").prop("selected", true);
    }
}

function isAvailable(value) {
    var isAvailable = false;
    switch ($("#memberSec").val()) {
    case "01": // MASTER
        if (value == "06") {
            isAvailable = false;
        } else {
            isAvailable = true;
        }
        break;
    case "02": // CMS
        if (value == "01" || value == "02" || value == "06") {
            isAvailable = false;
        } else {
            isAvailable = true;
        }
        break;
    case "04": // Service 관리자
        if (value == "05") {
            isAvailable = true;
        } else {
            isAvailable = false;
        }
        break;
    default:
        break;
    }
    return isAvailable;
}

function confirmModify() {
    if (!validateForm()) {
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        modifyMember();
    }, null, getMessage("button.confirm"));
}

function validateForm() {
    var isValid = true;
    $(".tableForm input[type='text']").each(function(i, item) {
        if (changedField[$(item).attr("id")] == undefined) {
            return;
        }

        if ($(this).hasClass("memberPhoneNo")) {
            if (!validatePhon(item, true)) {
                isValid = false;
                return false;
            }
        } else {
            if (!validate(item, true)) {
                isValid = false;
                return false;
            }
        }
    });

    if (isValid) {
        if (memberInfo.mbrSe != "06") {
            if ($("#memberIpadr").val() == $("#memberIpadr2").val()) {
                isValid = false;
                popAlertLayer(getMessage("login.alert.same.ipAddress.msg"), '', '', '', "memberIpadr2");
            }
        }
    }
    return isValid;
}

function validate(obj, usePopup) {
    var id = obj.id;
    if (id == "memberIpadr" || id == "memberIpadr2") {
        if (memberInfo.mbrSe == "06") {
            return true;
        }
    }
    var msg = "";
    var msgId = "#" + id + "Msg";

    $(msgId).hide();
    var value = $(obj).val();
    if (!usePopup) {
        if (id == "memberEmailId") {
            if (changedField["memberEmailId"] == undefined) {
                value = $("#memberEmailIdOri").val()
            }
            var emailDomain = (changedField["memberEmailDomain"] == undefined) ? $("#memberEmailDomainOri").val() : $("#memberEmailDomain").val();
            value += emailDomain;
        }
    }
    if (value.length <= 0) {
        if (id == "memberIpadr2") {
            return true;
        }
        msg = getMessage("login.alert.required.msg");
        if (usePopup) {
            popAlertLayer(msg, '', '', '', id);
        } else {
            $(msgId).text(msg);
            $(msgId).show();
        }
        return false;
    }

    var isValid = true;
    switch (id) {
    case "memberName":
        isValid =  validateName($("#memberName").val());
        if (!isValid) {
            msg = getMessage("login.alert.name.msg");
        }
        break;
    case "memberTelNo":
        isValid = validateTelNumber($("#memberTelNo").val());
        if (!isValid) {
            msg = getMessage("login.alert.telNo.msg");
        } else {
            $("#memberTelNo").val(phoneNumberFormatter($("#memberTelNo").val()));
        }
        break;
    case "memberPhoneNo":
        isValid = validatePhoneNumber($("#memberPhoneNo").val());
        if (!isValid) {
            msg = getMessage("login.alert.phoneNo.msg");
        } else {
            $("#memberPhoneNo").val(phoneNumberFormatter($("#memberPhoneNo").val()));
        }
        break;
    case "memberEmailId":
        var emailId = (changedField["memberEmailId"] == undefined) ? $("#memberEmailIdOri").val() : $("#memberEmailId").val();
        var emailDomain = (changedField["memberEmailDomain"] == undefined) ? $("#memberEmailDomainOri").val() : $("#memberEmailDomain").val();
        if (!usePopup) {
            isValid =  validateEmail(emailId + "@" + emailDomain);
        } else {
            isValid =  validateEmailId(emailId);
        }
        if (!isValid) {
            msg = getMessage("login.alert.email.msg");
        }
        break;
    case "memberEmailDomain":
        var emailDomain = (changedField["memberEmailDomain"] == undefined) ? $("#memberEmailDomainOri").val() : $("#memberEmailDomain").val();
        if (usePopup) {
            isValid =  validateEmailDomain(emailDomain);
        }
        if (!isValid) {
            msg = getMessage("login.alert.email.msg");
        }
        break;
    case "memberIpadr":
        isValid =  validateIpAddress($("#memberIpadr").val());
        if (!isValid) {
            msg = getMessage("login.alert.first.ipAddress.msg");
            if ($("#memberIpadr2Msg").text()) {
                msg = msg + " ";
            }
        }
        break;
    case "memberIpadr2":
        isValid =  validateIpAddress($("#memberIpadr2").val());
        if (!isValid) {
            msg = getMessage("login.alert.second.ipAddress.msg");
            if ($("#memberIpadrMsg").text()) {
                msg = " " + msg;
            }
        } else {
            if ($("#memberIpadr").val() == $("#memberIpadr2").val()) {
                isValid = false;
                msg = getMessage("login.alert.same.ipAddress.msg");
            }
        }
        break;
    default:
        console.log("Nothing id = " + id);
        break;
    }

    if (!isValid) {
        if (usePopup) {
            popAlertLayer(msg, '', '', '', id);
        } else {
            $(msgId).text(msg);
            $(msgId).show();
        }
    }
    return isValid;
}

function validatePhon(obj, usePopup) {
    var msg = "";
    var msgSpan = $(obj).parents("p").find("span");

    $(msgSpan).hide();
    var value = $(obj).val();

    if (value.length <= 0) {
        msg = getMessage("login.alert.required.msg");
        if (usePopup) {
            popAlertLayerObj(msg, '', '', '', obj);
        } else {
            $(msgSpan).text(msg);
            $(msgSpan).show();
        }
        return false;
    }

    var isValid = true;
    isValid = validatePhoneNumber($(obj).val());
    if (!isValid) {
        msg = getMessage("login.alert.phoneNo.msg");
    } else {
        $(obj).val(phoneNumberFormatter($(obj).val()));
    }

    if (!isValid) {
        if (usePopup) {
            popAlertLayerObj(msg, '', '', '', obj);
        } else {
            $(msgSpan).text(msg);
            $(msgSpan).show();
        }
    }
    return isValid;
}



function modifyMember() {
    switch($("#memberSection").val()){
    case "04" :
        if ($("#memberService option").index() == -1){
            popAlertLayer(getMessage("fail.member.noData.store"));
            return;
        }
        break;
    case "05" :
        if ($("#memberStore option").index() == -1){
            popAlertLayer(getMessage("fail.member.noData.store"));
            return;
        }
        break;
    default: break;
    }

    var name = (changedField["memberName"] == undefined) ? $("#memberNameOri").val() : $("#memberName").val();
    var telNo = (changedField["memberTelNo"] == undefined) ? phoneNumberFormatter($("#memberTelNoOri").val()) : phoneNumberFormatter($("#memberTelNo").val());

    var phoneNo = "";

    for (var i=0; i<$(".memberPhoneNo").size(); i++) {
        if (phoneNo != "") {
            phoneNo += ";";
        }
        phoneNo += (changedField["memberPhoneNo"+i] == undefined) ? phoneNumberFormatter($("#memberPhoneNoOri"+i).val()) : phoneNumberFormatter($("#memberPhoneNo"+i).val());
        if (typeof $("#memberPhoneNo"+i).parents("p").find(".oriTelList").val() != "undefined") {
            phoneNo+= "="+$("#memberPhoneNo"+i).parents("p").find(".oriTelList").val();
        }
    }


    var emailId = (changedField["memberEmailId"] == undefined) ? $("#memberEmailIdOri").val() : $("#memberEmailId").val();
    var emailDomain = (changedField["memberEmailDomain"] == undefined) ? $("#memberEmailDomainOri").val() : $("#memberEmailDomain").val();
    var ipadr = (changedField["memberIpadr"] == undefined) ? $("#memberIpadrOri").val() : $("#memberIpadr").val();
    var ipadr2 = (changedField["memberIpadr2"] == undefined) ? $("#memberIpadr2Ori").val() : $("#memberIpadr2").val();

    formData("NoneForm", "seq", memberInfo.mbrSeq);
    formData("NoneForm", "se", $("#memberSection option:selected").val());
    formData("NoneForm", "svcSeq", $("#memberService option:selected").val());
    formData("NoneForm", "storSeq", $("#memberStore option:selected").val());
    formData("NoneForm", "cpSeq", $("#memberCp option:selected").val());

    formData("NoneForm", "name", name);
    formData("NoneForm", "tel", telNo);
    formData("NoneForm", "phone", phoneNo);
    formData("NoneForm", "delphone", delTelList.toString());
    formData("NoneForm", "email", emailId + "@" + emailDomain);
    formData("NoneForm", "ipadr", ipadr);
    formData("NoneForm", "ipadr2", ipadr2);

    if (mailOriInfo != (emailId + "@" + emailDomain)){
        formData("NoneForm", "mailChgYn", "Y");
    } else {
        formData("NoneForm", "mailChgYn", "N");
    }

    callByPut("/api/member", "didModifyMember", "NoneForm", "didNotModifyMember");
    formDataDeleteAll("NoneForm");
}

function didModifyMember(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/member");
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("login.unavailableEmail.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function didNotModifyMember(data) {
    popAlertLayer(getMessage("fail.common.update"));
}

function confirmInitPassword() {
    popConfirmLayer(getMessage("member.manage.init.password.msg"), function() {
        initPassword()
    }, null, getMessage("button.confirm"));
}

function initPassword() {
    $("#loading").show();
    formData("NoneForm", "seq", $("#seq").val());

    callByPost("/api/member/init" , "didInitPassword", "NoneForm", "didNotInitPassword");
    formDataDeleteAll("NoneForm");
}

function didInitPassword(data) {
    $("#loading").hide();
    if (data.resultCode == "1000") {
        if ($("#usesAutoSend").val() == "Y") {
            var options = { type : "single", section : "initMember", seq : memberInfo.mbrSeq };
            sendSms(options);
        }

        popAlertLayer(getMessage("success.member.init.password"));
    } else {
        popAlertLayer(getMessage(data.resultMsg));
    }
}

function didNotInitPassword(data) {
    $("#loading").hide();
    var result = JSON.parse(data.responseText);
    popAlertLayer(getMessage(result.resultMsg));
}

function changeMemberEmailBox(obj) {
    $("#memberEmailDomain").val(obj.value);
    addChangedField(document.getElementById("memberEmailDomain"));
}

function addChangedField(obj) {
    if (!obj) {
        return;
    }

    if (changedField[$(obj).attr("id")] != undefined) {
        return;
    }

    changedField[$(obj).attr("id")] = true;
}

function removeTextIfNeeded(obj) {
    if (!obj) {
        return;
    }

    var id = $(obj).attr("id");
    // 입력창 데이터가 변경되지 않은 경우에만 문자 삭제 처리
    if (changedField[id] != undefined) {
        return;
    }

    $(obj).val("");
}

function restoreTextIfNeeded(obj) {
    if (!obj) {
        return;
    }

    var id = $(obj).attr("id");
    // 입력창 데이터가 변경되지 않은 경우에만 문자 원복 처리
    if (changedField[id] != undefined) {
        return false;
    }

    $(obj).val(getMaskText(obj));
    return true;
}

function getMaskText(obj) {
    var text = "";
    if (!obj) {
        return "";
    }

    switch (obj.id) {
    case "memberName":
        text = maskName($("#memberNameOri").val());
        break;
    case "memberTelNo":
        text = maskTelNum($("#memberTelNoOri").val());
        break;
    case "memberPhoneNo":
        text = maskTelNum($("#memberPhoneNoOri").val());
        break;
    case "memberEmailId":
        text = maskId($("#memberEmailIdOri").val());
        break;
    case "memberEmailDomain":
        text = $("#memberEmailDomainOri").val();
        break;
    case "memberIpadr":
        text = maskIpAddr($("#memberIpadrOri").val());
        break;
    case "memberIpadr2":
        text = maskIpAddr($("#memberIpadr2Ori").val());
        break;
    default:
        if ($(obj).hasClass("memberPhoneNo")) {
            text = maskTelNum($(obj).next("input").val());
        }
        break;
    }
    return text;
}

function removePhonNo(e) {
    var textObj = $(e).parents("p").find(".oriTelList");
    if (typeof $(textObj).val() != "undefined") {
        delTelList.push($(textObj).val());
    }
    $(e).index(".memberPhoneNo")

    $(e).parents("p").remove();
    $("#mPlus").css("opacity","1").attr( 'disabled', false );
}