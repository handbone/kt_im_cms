/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;

var target;
var keyword;
var hashUseYn;

$(document).ready(function() {
    $(".btnSearch").click(function() {
        searchKeyword();
    });

    $("#keyword").keydown(function(event) {
        if (event.keyCode == 13) {
            searchKeyword();
        }
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    getIntegratedMemberList();
});

getIntegratedMemberList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("table.uid"),
        getMessage("table.name"),
        getMessage("table.joinRoute"),
        getMessage("login.input.email"),
        getMessage("table.useYn"),
        getMessage("table.regdate"),
        "seq"
    ];
    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/member/integrate"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.integratedMemberList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames: columnNames,
        colModel:[
            {name:"num", index: "num", align:"center", width:40},
            {name:"uid", index: "uid", width:150, align:"center"},
            {name:"mbrNm", index:"mbrNm", align:"center"},
            {name:"joinRoute", index:"joinRoute", align:"center"},
            {name:"mbrEmail", index:"mbrEmail", align:"center"},
            {name:"useYn", index:"useYn", align:"center"},
            {name:"cretDt", index:"cretDt", align:"center"},
            {name:"intgrateAthnMbrSeq", index:"intgrateAthnMbrSeq", hidden:true},
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: 10, // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "num", // 정렬 칼럼 지정
            sortorder: "desc", // 정렬 방법 지정
            height: "auto", // jqgrid 나올 높이
            multiselect: false, // 좌측 체크박스 사용여부
            postData: {
                useYn : $("#useYn option:selected").val(),
                type : "list"
            },
            beforeRequest:function() {
                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                if (hasChangedHash) {
                    var str_hash = document.location.hash.replace("#","");
                    var page = parseInt(findGetParameter(str_hash,"page"));
                    target = findGetParameter(str_hash,"target");
                    keyword = findGetParameter(str_hash,"keyword");
                    hashUseYn = findGetParameter(str_hash,"useYn");

                    myPostData.page = page;

                    $("#useYn").val(hashUseYn);
                    myPostData.useYn = $("#useYn option:selected").val();

                    if (keyword != null) {
                        myPostData.target = target;
                        myPostData.keyword = keyword;
                    } else {
                        delete myPostData.target;
                        delete myPostData.keyword;
                    }
                }

                $('#jqgridData').jqGrid('setGridParam', {postData: myPostData });
            },
            loadComplete: function() {
                didCompleteLoad(this);
            },
            loadError: function (jqXHR, textStatus, errorThrown) {
                didCompleteLoad(this);
                $("#jqgridData").clearGridData();
                $("#sp_1_pageDiv").text(1);
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event) {
            }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

function searchKeyword() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            search : true,
            postData:{
                target: $("#target").val(),
                keyword: $("#keyword").val(),
                useYn: $("#useYn option:selected").val()
            },
            page: 1
        }
    );
    $("#jqgridData").trigger("reloadGrid");
}

function setSearchField() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        var searchHtml = "";

        for (var i = 0; i < colModel.length; i++) {
            var name = colModel[i].name;
            if (name == "uid" || name == "mbrNm") {
                searchHtml += "<option value=\"" + name + "\">" + colNames[i] + "</option>";
            }
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
}

didCompleteLoad = function(obj) {
    setSearchField();

    //session
    if (sessionStorage.getItem("last-url") != null) {
        sessionStorage.removeItem('state');
        sessionStorage.removeItem('last-url');
    }
    if (hasChangedHash) {
        hasChangedHash = false;
        $("#target").val(target);
        $("#keyword").val(keyword);
        $("#useYn").val(hashUseYn);
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        var useYnValue = $("#useYn option:selected").val();
        if (useYnValue) {
            hashlocation += "&useYn=" + useYnValue;
        }

        var searchTarget = $("#target").val();
        var searchKeyword = $("#keyword").val();
        if ($(obj).context.p.search && searchKeyword) {
            hashlocation += "&target=" + searchTarget + "&keyword=" + searchKeyword;
        }
        document.location.hash = hashlocation;
    }
}