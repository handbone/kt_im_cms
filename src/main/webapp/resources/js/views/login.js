/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var failedLoginCount = 0;
var failedFindingIdCount = 0;
var failedFindingPwdCount = 0;

$(document).ready(function(){
    $("#mId, #mPwd").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            Login();
        }
    });

    if (getCookie("cmsIdSaved")) {
        $("#mId").val(getCookie("cmsIdSaved"));
        $("#idSaved").prop("checked", true);
    }

    $("#domainList").change(function(e) {
        changeEmailBox(this);
    });

    $("#findEmailDomain").keyup(function() {
        var domain = $("#domainList option:selected").val();
        if (!domain) {
            return;
        }

        if ($("#domainList option:selected").val() != $("#findEmailDomain").val()) {
            $("#domainList option:eq(0)").prop("selected", true);
        }
    });

    addEmailDomainList(document.getElementById("domainList"));
});

jsLogin = function(id, pwd) {
    if (failedLoginCount >= 5) {
        popAlertLayer(getLoginMessage("fail.login.alert.block"));
        return;
    }
    formData("NoneForm", "id", id);
    formData("NoneForm", "pwd", pwd);
    callByPost("/api/login", "jsLoginSuccess", "NoneForm", "jsLoginFail");
    formDataDeleteAll("NoneForm");
}

jsLoginSuccess= function(data) {
    if (data.resultCode == "1000") {
        if ($("#idSaved").prop("checked")) {
            setCookie("cmsIdSaved", $("#mId").val());
        } else {
            removeCookie("cmsIdSaved");
        }
        var uri = makeAPIUrl("/auth");
        if ($("#requestUri").val()) {
            uri = uri + "?uri=" + $("#requestUri").val();
        }
        location.href = uri;
    } else if(data.resultCode == "1010") {
        failedLoginCount++;
        var msg = getLoginMessage("fail.login");
        popAlertLayer(msg);
    } else if(data.resultCode == "1012" || data.resultCode == "1013") {
        failedLoginCount = 0;
        popAlertLayer(getLoginMessage(data.resultMsg));
    }
}

jsLoginFail= function(data) {
    popAlertLayer(getLoginMessage("fail.login.server.error"));
}

Login = function(data) {
    if ($("#mId").val() == "") {
        popAlertLayer(getLoginMessage("login.id.msg"), "", "mId");
        return;
    }

    if ($("#mPwd").val() == "") {
        popAlertLayer(getLoginMessage("login.password.msg"), "", "mPwd");
        return;
    }

    jsLogin($("#mId").val(), $("#mPwd").val());
}

function openFindMemberPopup(type) {
    var popupTitle;
    var popupType;
    var callbackFunction;
    if (type == "id") {
        popupTitle = getLoginMessage("login.findIdTitle.msg");
        popupType = getLoginMessage("login.findId.name.msg");
        callbackFunction = "findId";
    } else if (type == "password") {
        popupTitle = getLoginMessage("login.findPasswordTitle.msg");
        popupType = getLoginMessage("login.input.id");
        callbackFunction = "findPassword";
    }
    $("#findTitle").text(popupTitle);
    $("#findType").text(popupType);
    $("#findMember .btnSearch").off("click");
    $("#findMember .btnSearch").on("click", function() {
        window[callbackFunction]();
    });

    popLayerDiv("findMember", 500, 300, true);
}

/* blockUI 열기
 * parameter: name(blockUI를 설정할 ID값)
 * return: x
 */
function insertFormShow(name) {
    popLayerDiv(name, 500, 300, true);
}

/* blockUI 닫기
 * parameter: x
 * return: x
 */
function popLayerClose(){
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
    formCloseFnc();
}

function findId() {
    if (failedFindingIdCount >= 5) {
        popAlertLayer(getLoginMessage("fail.login.alert.findId.block"));
        return;
    }

    var name = $("#findId").val();
    if (!name) {
        popAlertLayer(getLoginMessage("login.name.msg"), "", "findId");
        return;
    }

    var emailId = $("#findEmailId").val();
    if (!emailId) {
        popAlertLayer(getLoginMessage("login.email.msg"), "", "findEmailId");
        return;
    }

    var emailDomain = $("#findEmailDomain").val();
    if (!emailDomain) {
        popAlertLayer(getLoginMessage("login.email.msg"), "", "findEmailDomain");
        return;
    }

    var email = emailId + "@" + emailDomain;

    formData("NoneForm" , "type", "id");
    formData("NoneForm" , "name", name);
    formData("NoneForm" , "email", email);

    $("#loading").show();
    callByGet("/api/member/find" , "didFindId", "NoneForm", "didNotFindId");
    formDataDeleteAll("NoneForm");
}

function didFindId(data) {
    $("#loading").hide();
    if (data.resultCode == "1000") {
        failedFindingIdCount = 0;
        popAlertLayer(getLoginMessage("success.login.findId") + "<br>ID : " + data.result.findInfo.id);
        popLayerClose();
    } else {
        failedFindingIdCount++;
        popAlertLayer(getLoginMessage(data.resultMsg));
    }
}

function didNotFindId(data) {
    $("#loading").hide();
    var result = JSON.parse(data.responseText);
    popAlertLayer(getLoginMessage(result.resultMsg));
}

function findPassword() {
    if (failedFindingPwdCount >= 5) {
        popAlertLayer(getLoginMessage("fail.login.findPassword"));
        return;
    }

    var id = $("#findId").val();
    if (!id) {
        popAlertLayer(getLoginMessage("login.id.msg"), "", "findId");
        return;
    }

    var emailId = $("#findEmailId").val();
    if (!emailId) {
        popAlertLayer(getLoginMessage("login.email.msg"), "", "findEmailId");
        return;
    }

    var emailDomain = $("#findEmailDomain").val();
    if (!emailDomain) {
        popAlertLayer(getLoginMessage("login.email.msg"), "", "findEmailDomain");
        return;
    }

    var email = emailId + "@" + emailDomain;

    formData("NoneForm" , "type", "password");
    formData("NoneForm" , "id", id);
    formData("NoneForm" , "email", email);

    $("#loading").show();
    callByGet("/api/member/find" , "didFindPassword", "NoneForm", "didNotFindPassword");
    formDataDeleteAll("NoneForm");
}

function didFindPassword(data) {
    $("#loading").hide();
    if (data.resultCode == "1000") {
        failedFindingPwdCount = 0;
        popAlertLayer(getLoginMessage("success.login.find"));
        popLayerClose();
    } else if (data.resultCode == "1012") {
        failedFindingPwdCount = 0;
        popAlertLayer(getLoginMessage(data.resultMsg));
    } else {
        failedFindingPwdCount++;
        popAlertLayer(getLoginMessage(data.resultMsg));
    }
}

function didNotFindPassword(data) {
    $("#loading").hide();
    var result = JSON.parse(data.responseText);
    popAlertLayer(getLoginMessage(result.resultMsg));
}

function formCloseFnc() {
    // 비밀번호 찾기 팝업
    $(".findForm input[type=text]").val("");
    $(".findForm select option:eq(0)").prop("selected", true);
}

function changeEmailBox(obj) {
    $("#findEmailDomain").val(obj.value);
}

function popAlertLayer(msg, url, targetId) {
    setVisibilitySweetAlert(true);
    var content = document.createElement("div");
    content.innerHTML = msg;

    if (url) {
        swal({
            title: getLoginMessage("login.alert.msg"),
            content: content,
            closeOnClickOutside: false,
            closeOnEsc: false,
        })
        .then(
                function(value){
                    if(value){
                        swal.close();
                        pageMove(url);
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    } else {
        swal({
            title: getLoginMessage("login.alert.msg"),
            content: content,
        })
        .then(
                function(value) {
                    if (targetId) {
                        document.getElementById(targetId).focus();
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    }
}