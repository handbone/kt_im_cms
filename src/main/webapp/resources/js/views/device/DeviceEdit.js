/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var totalCount;
var checkedName;
var oriName;
var pageReady = false;
var devStorSeq = 0;
var devFrmtnSeq = 0;
var arr = new Array();
$(document).ready(function(){
    limitInputTitle("devId");

    devTypeList();
    setkeyup();

    var timeVal = 5; var timeStart = 5; var timeEnd = 60;
    var playTimeHtml ="<option value=9999>"+getMessage("common.unlimit.uncount.msg")+"</option>";
    for(timeStart; timeStart<=timeEnd; timeStart+=timeVal){
        playTimeHtml += "<option value="+timeStart+">";
        var hour = Math.floor(timeStart/60);
        var time = timeStart%60;
        var timeTxt ="";
        if(hour != 0){
            timeTxt += hour + " "+getMessage("common.time")+" ";
        }

        if(time <10){
            if (time != 0) {
                timeTxt += "0"+time+getMessage("common.minute");
            }
        }else{
            timeTxt += time+getMessage("common.minute");
        }

        playTimeHtml += timeTxt+"</option>";
    }

    playTimeHtml += "<option value='120'>2 "+getMessage("common.time")+" </option>";
    playTimeHtml += "<option value='180'>3 "+getMessage("common.time")+" </option>";
    playTimeHtml += "<option value='userSet'>"+getMessage("common.userSet")+"</option>";
    $("#playTime").html(playTimeHtml);

    /* list 클릭 시, 옵션값 변경*/
    $("body").click(function( event ) {
        var table = $(event.target).parents("table");
        var tContent = $(event.target).parent().parent();
        if(table.hasClass("listBox") == false){
            return;
        }
        if((event.target.nodeName == "TD" && tContent.is("tbody")) || $(event.target).hasClass("ellipsis")){
            var target = $(event.target).parents("tr").toggleClass("checked");
            var type = false;
            if(target.hasClass("checked")) type = true;
            $(target).find("input").attr("checked",type);
        }else{
            if(tContent.is("thead")){
                table.find("input.allcheck").trigger("click");
            }
        }
    });

    $("#userSet").bind('input keyup paste', function() {
        if ($("#userSet").val() >= 9999) {
            popAlertLayer(getMessage("msg.device.too.setTime"));
            $("#userSet").val("");
        }
    });

    listBack($(".btnCancel"));
});

devInfo = function(){
    callByGet("/api/device?devSeq="+$("#devSeq").val()+"&getType=edit", "devInfoSuccess", "NoneForm", "devInfoFail");
}

var svcSeq = 0;
devInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.deviceInfo;
        $("#devId").val(xssChk(item.devId));
        $("#svcNm").text(item.svcNm);
        $("#storNm").html(item.storNm);
        $("#devIpadr").val(item.devIpadr);
        svcSeq = item.svcSeq;
        $("#playTime").val(item.playTime);

        if (item.playTime != 9999 && $("#playTime option:selected").index() == 0) {
            $("#playTime option[value=userSet]").attr('selected','selected');
            $("#userSet").val(item.playTime);
            userTimeSet();
        }
        $("#devType").val(item.devTypeNm);
        $("#macAdr").html(item.macAdr);

        devStorSeq = item.storSeq;
        devFrmtnSeq = item.frmtnSeq;
        oriName = xssChk(item.devId);
        checkedName = oriName;

        releaseList();

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"),"/device");
    }
}

devInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/device");
}

releaseList = function(){
    callByGet("/api/mms/release?svcSeq="+svcSeq+"&storSeq="+devStorSeq+"&rows=1000","releaseListSuccess","NoneForm");
}

releaseListSuccess = function(data){
    if(data.resultCode == "1000"){
        var frmtnHtml = "";
        $(data.result.releaseList).each(function(i,item) {
            frmtnHtml += "<option value="+item.frmtnSeq+">"+item.frmtnNm+"</option>"
            $("#frmtnList").html(frmtnHtml);
        })

        if (!pageReady) {
            pageReady = true;
            $("#frmtnList").val(devFrmtnSeq);
        }
    } else if(data.resultCode == "1010"){
    }
}

devTypeList = function(){
    callByGet("/api/contentsCategory?unique=on","devTypeListSuccess","NoneForm");
}

devTypeListSuccess = function(data){
    if(data.resultCode == "1000"){
        var devTypListHtml = "";
        $(data.result.contsCtgList).each(function(i,item) {
            devTypListHtml += "<option value='"+item.secondCtgNm+"'>"+item.secondCtgNm+"</option>"
            $("#devType").html(devTypListHtml);
        })
    } else if(data.resultCode == "1010"){
    }
    devInfo();
}

devIdDuplication = function(){
    if($("#devId").val().trim() == ""){
        popAlertLayer(getMessage("msg.device.insert.DevId"));
        return;
    }

    var devIdVal = xssChk($("#devId").val());
    if(oriName != devIdVal){
        callByGet("/api/device?devId="+$("#devId").val()+"&storSeq="+devStorSeq, "devIdDuplicationSuccess")
    }else{
        checkedName = devIdVal;
        popAlertLayer(getMessage("msg.device.useful.deviceId"));
    }
}
devIdDuplicationSuccess = function(data){
    if(data.resultCode == "1010"){
        popAlertLayer(getMessage("msg.device.useful.deviceId"));
        checkedName = $("#devId").val();
    }else{
        popAlertLayer(getMessage("msg.device.not.useful.deviceId"));
        $("#devId").val("").focus();
    }
}

devUpdate = function(){
    if($("#devId").val().trim() == ""){
        popAlertLayer(getMessage("msg.device.insert.DevId"));
        return;
    }

    var playTime = $("#playTime").val();
    if ($("#playTime").val() == "userSet") {
        if ($("#userSet").val().trim() == "") {
            popAlertLayer(getMessage("msg.device.no.playTime"));
            return;
        }
        playTime = $("#userSet").val().trim();
    }

    if(checkedName == $.trim($("#devId").val()) || $("#devId").val() ==  oriName){
        popConfirmLayer(getMessage("common.update.msg"), function() {
            formData("NoneForm" , "devSeq", $("#devSeq").val());
            formData("NoneForm" , "devId", $("#devId").val());
            formData("NoneForm" , "devTypeNm",$("#devType").val());
            formData("NoneForm" , "frmtnSeq", $("#frmtnList option:selected").val());
            formData("NoneForm" , "storSeq", devStorSeq);
            formData("NoneForm" , "devIpadr",$("#devIpadr").val());
            formData("NoneForm" , "playTime",playTime);
            callByPut("/api/device" , "devUpdateSuccess", "NoneForm","UpdateFail");
        }, null, getMessage("common.confirm"));
    }else{
        popAlertLayer(getMessage("msg.device.check.useful.deviceId"));
    }
}

UpdateFail = function(data){
    popAlertLayer(getMessage("fail.common.update"));
}


devUpdateSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.update"),"/device#page=1&storSeq="+devStorSeq+"&svcSeq="+svcSeq+"&type=list");
    }
    formDataDeleteAll("NoneForm");
}

function userTimeSet() {
    if ($("#playTime option:selected").val() == "userSet") {
        $("#userSetBox").show();
    } else {
        $("#userSetBox").hide();
    }

}