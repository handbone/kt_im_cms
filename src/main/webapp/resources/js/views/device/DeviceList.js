/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var readyPage = false;
var apiUrl = makeAPIUrl("/api/device");
var lastBind = false;
var gridState;
var hashSvc;
var hashStore;
var selTab =0;
var hashType;
var totalPage = 0;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
})
$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashType = checkUndefined(findGetParameter(str_hash,"type"));

            if (hashType == "useStat") {
                selTab = 0;
            } else if (hashType == "list") {
                selTab = 1;
            } else if (hashType == "downConts"){
                selTab = 2
            }

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#svcList").val(hashSvc);
            $("#type").val(hashType);

            setSvcList();
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashType = checkUndefined(findGetParameter(str_hash,"type"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#type").val(hashType);
        $("#keyword").val(searchString);

        if (hashType == "useStat") {
            selTab = 0;
        } else if (hashType == "list") {
            selTab = 1;
        } else if (hashType == "downConts"){
            selTab = 2
        }

        gridState = "NONE";
    }

    svcList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

function setStorList(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

        $("#svcList").val(hashSvc);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }

        if (hashStore == "null") {
            hashStore = "";
            popAlertLayer(getMessage("info.store.nodata.msg"));
            $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").bind("click", function(e) {
                popAlertLayer(getMessage("common.bad.request.msg"));
            });
            $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
        } else {
            //storList(hashStore); // 이중으로 호출되는 코드 제거
            $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").unbind( "click" );
        }

        storList(hashStore);

        gridState = "NONE";
    } else {
        storList(-1);
    }
}

function keywordSearch(){
    if (selTab == 0) {
        typeCheck();
        $(".btn_cat").show().parents(".rsvGrp").css("width","383px");
    } else {
        gridState = "SEARCH";
        $(".btn_cat").hide().parents(".rsvGrp").css("width","323px");
        jQuery("#jqgridData").trigger("reloadGrid");
    }
}

function searchFieldSet(){
    if (selTab != 0) {
        if ($("#target > option").length == 0) {

            var searchHtml = "";

            var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
            var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');
            for (var i=0; i<colModel.length; i++) {
                //검색제외추가
                switch(colModel[i].name) {
                case "devId":break;
                case "contsTitle":break;
                default:continue;
                }
                searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
            }
            $("#target").html(searchHtml);
            if ($("#target option").index() == 0) {
                $(".target_bak").remove();
                $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
                $(".target_bak").parent(".selectBox").css("display","table");
            } else {
                $(".target_bak").remove();
            }
        }
        $("#target").val(searchField);
    } else {
        $(".target_bak").remove();
    }
}

var noData = false;
svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
                svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }
        noData = false;
        setStorList();
    } else {
        noData = true;
        $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
        $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    }
}

var stateVal;
var noStorData = false;
storList = function(val){
    stateVal = val;
    noStorData = false;
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"storListSuccess","NoneForm");
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(storeListHtml);
        $("#storList").attr("disabled", false);
        if ($("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#storList").attr('disabled', 'true');
        }

        if (stateVal != -1) {
            $("#storList").val(stateVal);
        }
        noStorData = false;
        if (!readyPage) {
            readyPage = true;
            tabSet();
            if (selTab != 0) {
                contentList();
                $(".btn_cat").hide().parents(".rsvGrp").css("width","323px");
            } else {
                $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
                $(".conTitleGrp div:eq(0)").removeClass("tabOff").addClass("tabOn").css({ 'pointer-events': 'none' });
                $(".btn_cat").show().parents(".rsvGrp").css("width","383px");
                gridState = "GRIDCOMPLETE";
            }
        } else {
            if (selTab != 0) {
                $(".btn_cat").hide().parents(".rsvGrp").css("width","323px");
                jQuery("#jqgridData").trigger("reloadGrid");
            } else {
                var hashlocation;
                if (noData) {
                    hashlocation = "storSeq="+"&svcSeq="+"&type="+$("#type").val();
                } else {
                    hashlocation = "storSeq="+$("#storList option:selected").val()+"&svcSeq="+$("#svcList option:selected").val()+"&type="+$("#type").val();
                }
                document.location.hash = hashlocation;
                $(".btn_cat").show().parents(".rsvGrp").css("width","383px");
                typeCheck();
                gridState = "GRIDCOMPLETE";
            }
        }
        $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").unbind( "click" );
    } else {
        $("#storList").html("");
        $("#storList").attr("disabled", true);
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
        noStorData = true;
        $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');

        if (!readyPage) {
            readyPage = true;
            tabSet();
            if (selTab != 0) {
                contentList();
            } else {
                $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
                $(".conTitleGrp div:eq(0)").removeClass("tabOff").addClass("tabOn").css({ 'pointer-events': 'none' });
            }
        } else {
            if (selTab != 0) {
                $(".btn_cat").hide().parents(".rsvGrp").css("width","323px");
                jQuery("#jqgridData").trigger("reloadGrid");
            } else {
                $(".btn_cat").show().parents(".rsvGrp").css("width","383px");
                typeCheck();
            }
        }
    }
}

function tabVal(seq,e){
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
    $(e).parent("div").removeClass("tabOff").addClass("tabOn").css({ 'pointer-events': 'none' });

    selTab = seq;

    $("#target").val("");
    $("#keyword").val("");

    totalPage = 0;

    jQuery("#jqgridData").jqGrid('GridUnload');
    tabSet();
    $("#target").html("");
    if (selTab != 0) {
        $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    } else {
        gridState = "GRIDCOMPLETE";
    }
}

var option_common = {
    url:apiUrl,
    datatype: "json", // 데이터 타입 지정
    jsonReader : {
        page: "result.currentPage",
        total: "result.totalPage",
        root: "result.deviceList",
        records: "result.totalCount",
        repeatitems: false
    },
    colNames:[
        getMessage("column.title.num"),
        "Device ID",
        getMessage("table.storeName"),
        getMessage("column.title.release"),
        getMessage("table.regdate"),
        "devSeq"
    ],
    colModel:[
        {name:"num", index: "num", align:"center", width:20},
        {name:"devId", index:"DEV_ID", align:"center",formatter:pointercursor},
        {name:"storNm", index:"STOR_NM", align:"center"},
        {name:"frmtnNm", index:"FRMTN_NM", align:"center"},
        {name:"cretDt", index:"CRET_DT", align:"center"},
        {name:"devSeq", index: "DEV_SEQ", align:"center", width:20,hidden:true},
    ],
    sortorder: "desc",
    height: "auto",
    autowidth: true, // width 자동 지정 여부
    rowNum: 10, // 페이지에 출력될 칼럼 개수
    pager: "#pageDiv", // 페이징 할 부분 지정
    viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
    sortname: "devSeq",
    multiselect: true,
    beforeRequest:function(){
        tempState = gridState;
        var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

        if (gridState == "TABVAL") {
            searchField = "";
            searchString = "";
        } else if (gridState != "SEARCH") {
            $("#target").val(searchField);
            $("#keyword").val(searchString);
        }

        if (isNaN(page)) {
           page =1;
        }

        if (totalPage > 0 && totalPage < page) {
            page = 1;
        }

        searchField = checkUndefined($("#target").val());
        searchString = checkUndefined($("#keyword").val());
        hashSvc = checkUndefined($("#svcList").val());
        hashStore = checkUndefined($("#storList").val());
        hashType = checkUndefined($("#type").val());

        if (hashType == "list") {
            apiUrl = makeAPIUrl("/api/device");
            $("#jqgridData").jqGrid("showCol", ["cb"]);
        } else {
            apiUrl = makeAPIUrl("/api/deviceDownLog");
            $("#jqgridData").jqGrid("hideCol", ["cb"]);
        }

        $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
        $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

        if (gridState == "HASH"){
            myPostData.page = page;
            tempState = "READY";
        } else {
            if(tempState == "SEARCH"){
               myPostData.page = 1;
            } else {
                tempState = "";
            }
        }

        if(gridState == "NONE"){
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            myPostData.page = page;

            if (searchField != null) {
                tempState = "SEARCH";
            } else {
                tempState = "READY";
            }
        }

        if(searchField != null && searchField != "" && searchString != ""){
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
        }else{
            delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
        }

        gridState = "GRIDBEGIN";

        if (hashSvc != null && hashSvc != "") {
            myPostData.svcSeq = hashSvc;
        } else {
            myPostData.svcSeq = "";
        }

        if (hashStore != null && hashStore != "") {
            myPostData.storSeq = hashStore;
        } else {
            myPostData.storSeq = "";
        }

        myPostData.type = hashType;

        if (noData) {
            myPostData.page = 0;
            hashSvc = "";
            hashStore = "";
            popAlertLayer(getMessage("info.service.nodata.msg"));
        }

        if (noStorData || $("#storList option").index() == -1) {
            if ($("#svcList option").index() == -1) {
                popAlertLayer(getMessage("info.service.nodata.msg"));
            } else if ($("#storList option").index() == -1) {
                popAlertLayer(getMessage("info.store.nodata.msg"));
            }
            myPostData.page = 0;
            hashStore = "";
            //noStorData = false;
        }

        $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page });
    },
    loadComplete: function (data) {
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        //session
        if(sessionStorage.getItem("last-url") != null){
            sessionStorage.removeItem('state');
            sessionStorage.removeItem('last-url');
        }

        //Search 필드 등록
        searchFieldSet();

        if (data.resultCode == 1000) {
            totalPage = data.result.totalPage;

            if (page != data.result.currentPage) {
                tempState = "";
            }
        } else {
            tempState = "";
        }

        if (isNaN(page) || noData || noStorData) {
            page = 1;
            $(".ui-pg-input").val("1");
        }

        if (hashStore == null){
            hashStore = "";
        }

        /*뒤로가기*/
        var hashlocation = "page="+page+"&storSeq="+hashStore+"&svcSeq="+hashSvc+"&type="+$("#type").val();
        if($(this).context.p.postData._search && $("#target").val() != null){
            hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
        }

        gridState = "GRIDCOMPLETE";
        document.location.hash = hashlocation;

        if (tempState != "" || str_hash == hashlocation) {
            gridState = "READY";
        }

        //pageMove Max
        $('.ui-pg-input').on('keyup', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
        });

        $(this).find(".pointer").parent("td").addClass("pointer");
        $("#jqgridData").setGridWidth($("#contents").width());
    },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
    onCellSelect: function(rowId, columnId, cellValue, event){
        // 콘텐츠 ID 선택시
        if(columnId == 2){
            var list = $("#jqgridData").jqGrid('getRowData', rowId);
            sessionStorage.setItem("last-url", location);
            sessionStorage.setItem("state", "view");
            pageMove("/device/"+list.devSeq);
        }
    }
}

contentList = function(){
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

setSvcList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"setSvcListSuccess","NoneForm");
}

setSvcListSuccess = function(data){
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(storeListHtml);
        $("#storList").val(hashStore);

        $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
        $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

        jQuery("#jqgridData").jqGrid('GridUnload');
        tabSet();
        if (selTab != 0) {
            jQuery("#jqgridData").jqGrid(option_common).trigger("reloadGrid");
        }
    } else {
        noData = true;
    }
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

tabSet = function(){
    switch(selTab){
    case 0:
        $(".tab").eq(0).hide();
        $(".tab").eq(1).show();
        $("#type").val("useStat");
        gridState = "READY";
        var hashlocation;
        if (noData) {
            hashlocation = "storSeq="+"&svcSeq="+"&type="+$("#type").val();
        } else {
            hashlocation = "storSeq="+$("#storList option:selected").val()+"&svcSeq="+$("#svcList option:selected").val()+"&type="+$("#type").val();
        }

        document.location.hash = hashlocation;
        typeCheck();
        $(".CenterGrp").hide();
        $(".btn_cat").show().parents(".rsvGrp").css("width","383px");
        break;
    case 1:
        $(".tab").eq(0).show();
        $(".tab").eq(1).hide();
        $("#type").val("list");
        option_common['url'] = makeAPIUrl("/api/contents");
        option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.deviceList",
            records: "result.totalCount",
            repeatitems: false
        };
        option_common['colNames'] = [getMessage("column.title.num"), "Device ID", getMessage("table.storeName"), getMessage("column.title.release"), getMessage("table.regdate"), "devSeq"];
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:20},
            {name:"devId", index:"DEV_ID", align:"center",formatter:pointercursor},
            {name:"storNm", index:"STOR_NM", align:"center"},
            {name:"frmtnNm", index:"FRMTN_NM", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"devSeq", index: "DEV_SEQ", align:"center", width:20,hidden:true},
        ];

        option_common['sortname'] = "devSeq";
        option_common['onCellSelect'] = function(rowId, columnId, cellValue, event){
            // 콘텐츠 ID 선택시
            if(columnId == 2){
              var list = $("#jqgridData").jqGrid('getRowData', rowId);
              sessionStorage.setItem("last-url", location);
              sessionStorage.setItem("state", "view");
              pageMove("/device/"+list.devSeq);
            }
        }
        $(".CenterGrp").show();
        $(".CenterGrp .cellTable").show();
        $(".btn_cat").hide().parents(".rsvGrp").css("width","323px");
        break;
    case 2:
        $(".tab").eq(0).show();
        $(".tab").eq(1).hide();
        $("#type").val("downConts");
        option_common['url'] = makeAPIUrl("/api/deviceDownLog");
        option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.deviceDownLog",
            records: "result.totalCount",
            repeatitems: false
        };

        option_common['colNames'] = [getMessage("column.title.num"),getMessage("device.table.id"),getMessage("table.contentsTitle"),getMessage("table.date"),"contsDownSeq"];
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:20},
            {name:"devId", index:"DEV_ID", align:"center"},
            {name:"contsTitle", index:"CONTS_TITLE", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"contsDownSeq", index: "CONTS_DOWN_SEQ", align:"center", width:20,hidden:true},
        ];
        option_common['sortname'] = "amdDt";
        option_common['onCellSelect'] = function(rowId, columnId, cellValue, event){

        }
        $(".CenterGrp").show();
        $(".CenterGrp .cellTable").hide();
        $(".btn_cat").hide().parents(".rsvGrp").css("width","323px");
        break;
    }

    option_common['rowNum'] = $("#limit").val();
}

deleteDevice = function(){
    var devSeq = "";
    var devIds = "";
    for(var i=1; i<$("#limit").val()+1; i++){
        if($("#jqg_jqgridData_"+i).prop("checked")){
            var list = $("#jqgridData").jqGrid('getRowData', i);
            var devId = $("#jqgridData").jqGrid('getRowData', i).devId.replace(/(<([^>]+)>)/ig,"");
            if (devIds != "") {
                devIds += ", ";
            }

            devSeq += list.devSeq + ",";
            devIds += devId;
        }
    }

    if(devSeq == "" || devSeq == 0){
        popAlertLayer(getMessage("cms.release.msg.noData"));
        return;
    }

    popConfirmLayer(getMessage("msg.device.delete.confirm"), function(){
        formData("NoneForm" , "devSeqs", devSeq.slice(0,-1));
        callByDelete("/api/device", "deleteDeviceSuccess", "NoneForm","deleteFail");
    },null,getMessage("common.confirm"));
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteDeviceSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"),function(){location.reload()});
    }
}

/*Client 사용현황 관련*/
var port = "";
if(window.location.port != ""){
    port = ":"+window.location.port;
}
var type = 0; // 0 : 리스트 형태, 1 : 블럭 형태
var cateGoryListAry = new Array();
deviceConnectList = function(){
    if ($("#storList option").index() == -1) {
        $("#storList").html("");
        if ($("#svcList option").index() == -1){
            popAlertLayer(getMessage("info.service.nodata.msg"));
        } else {
            popAlertLayer(getMessage("info.store.nodata.msg"));
        }

        $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
        $(".content .tab:eq(1)").html("");
        return;
    }

    $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").unbind( "click" );

    formData("NoneForm", "storSeq", $("#storList option:selected").val());
    var searchStor = $("#storList option:selected").val();
    if(searchStor == 0){
        searchStor = "-1";
    }
    callByGet("/api/deviceConnect?storSeq="+searchStor, "deviceConnectListSuccess", "ConnectInfo");
}

deviceConnectListSuccess = function(data){
    formDataDelete("NoneForm", "storSeq");
    var deviceConnectListHtml = "";
    if(data.resultCode == "1000"){
        $(data.result.deviceConnectList).each(function(i,item) {
            var tagSize = item.useTagList.length;
            var str = item.devTypeNm;
            str = str.replace(" ","").toUpperCase();

            if (str.match("ATTRACTION")) {
                str = "Attraction";
            } else if (str.match(getMessage("device.table.soccer")) || str.match("SOCCER")) {
                str = "SOCCER";
            } else if (str.match(getMessage("device.table.basketball"))  || str.match("BASKETBALL")) {
                str = "BASKETBALL";
            } else if (str.match("FPS")) {
                str = "FPS";
            } else if (str.match("VRROOM") || str.match("VR_ROOM")) {
                str = "VRROOM";
            } else if (str.match(getMessage("device.table.race")) || str.match("RACE")) {
                str = "RECE";
            } else if (str.match("HADO")) {
                str = "HADO";
            } else {
                str = "ETC";
            }

            var imgPath = "";
            if (item.devUseYn == 'Y') {
                imgPath = "/resources/image/device_on.png";
            } else {
                imgPath = "/resources/image/device_off.png";
            }

            /*정렬 타입*/
            if(type){
                var useDev = item.devUseYn;
                var tab;
                if (useDev == 'Y') {
                    tab = "deviceBoxOn";
                } else {
                    tab = "deviceBoxOff";
                }

                deviceConnectListHtml += "<div class='deviceBoxOn'>";
                deviceConnectListHtml += "<div class='boxHead'>";
                deviceConnectListHtml += "<div class='boxHeadTitle'>"+item.devId+"</div>";
                deviceConnectListHtml += "<div class='boxHeadDevice'>";

                var typeNm = item.devTypeNm;
                var typeNmAry = typeNm.split("_");

                for(var z=0; z<typeNmAry.length; z++){
                    var splitStr ="";
                    if (z+1 != typeNmAry.length){
                        splitStr = "_";
                    }

                    deviceConnectListHtml += "<span class='inline_b'>"+typeNmAry[z]+splitStr+"</span>";
                }
                deviceConnectListHtml += "</div>"; /*boxHeadDevice*/

                deviceConnectListHtml += "<div class='boxHeadIcon'>";
                deviceConnectListHtml += "<img src='" + makeAPIUrl(imgPath) + "' onMouseOver=\"showDevInfo('LayerPopup_" + item.devSeq + "')\""
                                        + "style='cursor:pointer;' onMouseOut=\"hideDevInfo('LayerPopup_" + item.devSeq + "')\" style='cursor:pointer;'>"
                                        + "<div class='balloonGrp4' id='LayerPopup_" + item.devSeq + "' style='display:none;'>"
                                        + "<div class='balloon4' id='popup_" + item.devSeq + "' style='display: table;'>"
                                        + "</div>"
                                        + "</div>";

                deviceConnectListHtml += "</div>"; /*boxHeadIcon*/
                deviceConnectListHtml += "</div>"; /*boxHead*/

                /*devBody S*/
                if(tagSize == 0) {
                    deviceConnectListHtml += "<div class='boxBodyNo'>이용자가 없습니다.";
                } else {
                    deviceConnectListHtml += "<div class='boxBody'>";
                    deviceConnectListHtml += "<ul>";
                    deviceConnectListHtml += "<li class='scrollBody'>";
                    $(item.useTagList).each(function(j,Tagitem){

                        deviceConnectListHtml += "<span class='item2'>";
                        deviceConnectListHtml += "<a href='"+$("#contextPath").val()+"/reservate/"+Tagitem.tagSeq+"' title='"+Tagitem.tagId+"'>";
                        deviceConnectListHtml += Tagitem.tagId + "<br />";
                        deviceConnectListHtml += "("+getMessage("reservate.table.usageMinute")+" : "+Tagitem.usageMinute+getMessage("reservation.table.minute")+" ,";
                        deviceConnectListHtml += getMessage("reservate.space.rmndCnt")+" : "+Tagitem.rmndCnt+getMessage("common.cnt")+")";
                        deviceConnectListHtml += "</a>";
                        deviceConnectListHtml += "</span>";
                    });
                    deviceConnectListHtml += "</li>";

                    deviceConnectListHtml += "<li class='contsBody'>";
                    deviceConnectListHtml += "<span class='dcontent'>";

                    if(item.filePath != ""){
                        var imgUrl = $("#contextPath").val()+"/api/imgUrl?filePath="+item.filePath;
                        deviceConnectListHtml += "<img src="+imgUrl+"><br><span>"+item.contsTitle+"</span>";
                    }else{
                        deviceConnectListHtml += "<img src='" + makeAPIUrl("/resources/image/no_content.gif") + "'><br />"+item.contsTitle;
                        deviceConnectListHtml += "<span class='faOff'>콘텐츠가 없습니다.</span>";
                    }
                    deviceConnectListHtml += "</span>";
                    deviceConnectListHtml += "</li>";
                    deviceConnectListHtml += "</ul>";
                }

                /*devBody E*/

                deviceConnectListHtml += "</div>"; /*boxBody*/
                deviceConnectListHtml += "</div>"; /*deviceBox*/

                $(".content .tab:eq(1)").html(deviceConnectListHtml);

            } else {//type - 1 end
                deviceConnectListHtml = "<tr>";
                var tagSizeTemp = tagSize;
                if (tagSize == 0){
                    tagSizeTemp = 1;
                }

                deviceConnectListHtml += "<td rowspan="+tagSizeTemp+"><div style='position:relative; display:block' rowspan="+tagSizeTemp+">";

                deviceConnectListHtml += "<img src='" + makeAPIUrl(imgPath) + "' class='deviceBallon' onMouseOver=\"showDevInfo('LayerPopup_" + item.devSeq + "')\""
                                        + "style='cursor:pointer;' onMouseOut=\"hideDevInfo('LayerPopup_" + item.devSeq + "')\" style='cursor:pointer;'>"
                                        + "<div class='balloonGrp3' id='LayerPopup_" + item.devSeq + "' style='display:none;'>"
                                        + "<div class='balloon3' id='popup_" + item.devSeq + "' style='display: block;'>"
                                        + "</div></div></div></td>";

                if(tagSize ==0){
                    deviceConnectListHtml += "<td class='textGray'>"+item.devId+"</td>";
                    deviceConnectListHtml += "<td class='textGray'>"+item.devTypeNm+"</td>";
                } else {
                    deviceConnectListHtml += "<td rowspan="+tagSizeTemp+">"+item.devId+"</td>";
                    deviceConnectListHtml += "<td rowspan="+tagSizeTemp+">"+item.devTypeNm+"</td>";
                }

                if(tagSize == 0) {
                    deviceConnectListHtml += "<td class='textGray h5' colspan=3>이용자가 없습니다.</td>";
                } else {
                    var nClass = "";
                    if (tagSize > 1) {
                        nClass = "noBrd";
                    }

                    deviceConnectListHtml += "<td rowspan=1 class='"+nClass+"'>"+item.useTagList[0].tagId+"</td>";

                    if(item.useTagList[0].usageMinute == "9999") {
                        deviceConnectListHtml += "<td class='"+nClass+"'>제한없음</td>";
                    } else {
                        deviceConnectListHtml += "<td class='"+nClass+"'>"+item.useTagList[0].usageMinute+"분</td>";
                    }

                    if(item.useTagList[0].rmndCnt == "9999") {
                        deviceConnectListHtml += "<td class='"+nClass+"'>제한없음</td>";
                    } else {
                        deviceConnectListHtml += "<td class='"+nClass+"'>"+item.useTagList[0].rmndCnt+"회</td>";
                    }

                    deviceConnectListHtml += "</tr>";
                    if(item.useTagList.length > 1){
                        $(item.useTagList).each(function(j,Tagitem){
                            if(j!=0){
                                var nClass = "";
                                if (j != tagSize -1) {
                                    nClass = "class='noBrd'";
                                }

                                deviceConnectListHtml += "<tr>";
                                deviceConnectListHtml += "<td rowspan=1 "+nClass+">"+Tagitem.tagId+"</td>";
                                if (Tagitem.usageMinute == "9999") {
                                    deviceConnectListHtml += "<td "+nClass+">제한없음</td>";
                                } else {
                                    deviceConnectListHtml += "<td "+nClass+">"+Tagitem.usageMinute+"분</td>";
                                }
                                if (Tagitem.rmndCnt == "9999") {
                                    deviceConnectListHtml += "<td "+nClass+">제한없음</td>";
                                } else {
                                    deviceConnectListHtml += "<td "+nClass+">"+Tagitem.rmndCnt+"회</td>";
                                }

                                deviceConnectListHtml += "</tr>";
                            }
                        });
                    }
                }
                $("table."+str).append(deviceConnectListHtml).find(".noBrd").css("border-bottom","0px")
            }
        });
    } else {
        $(".content .tab:eq(1)").html("");
        popAlertLayer(getMessage("msg.device.noData"));
    }
}

var isProcessing = false;
var callShow = false;
showDevInfo = function(layerId) {
    if (isProcessing) {
        return;
    }

    if ($("#" + layerId).css("display") == "none") {
        isProcessing = true;
        callShow = true;
        var seq = layerId.replace("LayerPopup_", "");
        callByGetWithoutLoading("/api/deviceConnect?devSeq=" + seq, "connectDeviceUseDetailInfoSuccess", "NoneForm");
    }
}

hideDevInfo = function(layerId) {
    if (callShow) {
        callShow = false;
    }

    if ($("#" + layerId).css("display") == "block") {
        $("#" + layerId).hide();
    }
}

connectDeviceUseDetailInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.deviceUseDetailInfo;
        if (callShow) {
            $("#popup_" + item.devSeq).html("");
            var contHtml = "<p class='txtBold h4'>" + item.devId + "</p>"
                            + "<div style='display:table-row'>"
                            + "<div style='display: table-cell; white-space: nowrap;'>" + getMessage("device.use.detail.cpu.rate") + " : " +  "</div>";
            if (item.devUseCpuRate == "") {
                contHtml += "<div style='display: table-cell;'>" + item.devUseCpuRate + "</div>";
            } else {
                contHtml += "<div style='display: table-cell;'>" + item.devUseCpuRate + "%</div>";
            }

            contHtml += "</div>"
                        + "<div style='display:table-row'>"
                        + "<div style='display: table-cell; white-space: nowrap;'>" + getMessage("device.use.detail.usage.mem") + " : " +  "</div>"
                        + "<div style='display: table-cell;'>" + item.devUsageMem + "</div>"
                        + "</div>"
                        + "<div style='display:table-row'>"
                        + "<div style='display: table-cell; white-space: nowrap;'>" + getMessage("device.use.detail.strge.space") + " : " +  "</div>"
                        + "<div style='display: table-cell;'>" + item.devStrgeSpace + "</div>"
                        + "</div>";
            $("#popup_" + item.devSeq).append(contHtml);
            $("#LayerPopup_" + item.devSeq).show();
        }
        isProcessing = false;
    }
}

chageView = function(){
    var image = document.getElementById('chgBtn');
    if(type == 0) {
        type = 1;
        image.src = makeAPIUrl("/resources/image/btn_cat_list.gif");
    } else if(type == 1 ) {
        type = 0;
        image.src = makeAPIUrl("/resources/image/btn_cat_thum.gif");
    }

    categoryList();
}

categoryList = function(){
    cateGoryListAry = new Array();
    var storSeq = $("#storList").val();
    if ($("#storList option").index() == -1) {
        storSeq = -1;
    }
    callByGet("/api/contentsCategory?unique=on&storSeq="+storSeq, "categoryListSuccess", "NoneForm");
}

categoryListSuccess = function(data){
    $(".content .tab:eq(1)").html("");

    $(".content .tab:eq(1)").append("<div class='tableArea listArea'></div>");
    if(data.resultCode == "1000"){
        $(data.result.contsCtgList).each(function(i,item) {
            var str = item.secondCtgNm;
            str = str.replace(" ","").toUpperCase();

            if (str.match("ATTRACTION") || str.match(getMessage("device.table.Attraceion"))) {
                str = "Attraction";
            } else if (str.match(getMessage("device.table.soccer")) || str.match("SOCCER")) {
                str = "SOCCER";
            } else if (str.match(getMessage("device.table.basketball"))  || str.match("BASKETBALL")) {
                str = "BASKETBALL";
            } else if (str.match(getMessage("device.table.race")) || str.match("RACE")) {
                str = "RECE";
            }  else if (str.match("FPS")) {
                str = "FPS";
            } else if (str.match("VRROOM") || str.match("VR_ROOM")) {
                str = "VRROOM";
            } else if (str.match("HADO")) {
                str = "HADO";
            } else {
                str = "ETC";
            }

            var isExist = false;
            for (var i = 0; i < cateGoryListAry.length; i++) {
                if (str == cateGoryListAry[i]) {
                    isExist = true;
                }
            }

            cateGoryListAry.push(str);

            if (!isExist) {
                $(".tableArea").append("<table class='tableList deviceBoxlistGrp "+str+"'><thead>" +
                        "<tr>" +
                        "<th width='100px'>"+getMessage("device.connect.column.dev")+"</th>" +
                        "<th width=22%>Device ID</th>" +
                        "<th width=22%>"+getMessage("device.connect.column.devType")+"</th>" +
                        "<th width=30%>"+getMessage("device.connect.column.user")+"</th>" +
                        "<th width=10%>"+getMessage("device.connect.column.useTime")+"</th>" +
                        "<th width=10%>"+getMessage("device.connect.column.useCount")+"</th>" +
                        "</tr>" +
                        "<thead><tbody></tbody></table><br>");
            }
        });
        //deviceConnectList();
    }
    deviceConnectList();
}

typeCheck = function(){
    if (type) {
        deviceConnectList();
    } else {
        categoryList();
    }
}

function makePageMove(e){
    if ($("#storList option").index() != -1) {
        pageMove("/device/regist#svcSeq="+$("#svcList").val()+"&storSeq="+$("#storList").val());
    }
}
