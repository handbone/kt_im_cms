/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var arr = new Array();
$(document).ready(function(){
    devInfo();
});

devInfo = function(){
    callByGet("/api/device?devSeq="+$("#devSeq").val(), "devInfoSuccess", "NoneForm", "devInfoFail");
}

devInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.deviceInfo;
        $("#devId").html(item.devId);
        $("#cretrId").html(item.cretrId);
        $("#storNm").html(item.storNm);
        $("#frmtnNm").html(item.frmtnNm);
        $("#frmtnCnt").html(item.frmtnCnt);
        $("#devContsCnt").html(item.devContsCnt);
        $("#devType").html(item.devTypeNm);
        $("#storSeq").val(item.storSeq);
        $("#svcSeq").val(item.svcSeq);

        var playTime = item.playTime;
        var hour = Math.floor(playTime/60);
        var time = playTime%60;
        var timeTxt ="";
        if(hour != 0){
            timeTxt += hour + " 시간 ";
        }
        if(time <10){
                if(time != 0)timeTxt += "0"+time+"분";
        }else{timeTxt += time+"분";}
        if(playTime == 9999) timeTxt = "제한없음";
        $("#playTime").html(timeTxt);

        $("#devIpadr").html(item.devIpadr);
        var macHtml = item.macAdr;
        if (item.macAdr != "") {
            macHtml += "<span>&nbsp;&nbsp;<input type=\"button\" class=\"btnSmallWhite btnResetWhite\" onclick='macReset()' value="+getMessage("button.init")+"></span>"
            macHtml += "<div class=\"infotip txtRed txtBold btnHelp\">!<div class='balloonGrp'><div class='balloon'>"+getMessage("msg.device.infoTip.one")+"<br />"+getMessage("msg.device.infoTip.two")+"</div></div></div>";
        }
        $("#macAdr").html(macHtml);


        var devContsHtml = item.devContsCnt;
        if (item.devContsCnt != 0) {
            devContsHtml += "<span>&nbsp;&nbsp;<input type=\"button\" class=\"btnSmallWhite btnResetWhite\" onclick='contsUpdateDelete()' value="+getMessage("button.init")+"></span>"
            devContsHtml += "<div class=\"infotip txtRed txtBold btnHelp\">!<div class='balloonGrp'><div class='balloon'>"+getMessage("msg.device.infoTip.three")+"<br />"+getMessage("msg.device.infoTip.four")+"</div></div></div>";
        }
        $("#devContsCnt").html(devContsHtml);

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"),"/device");
    }
}

devInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"),"/device");
}

macReset = function(){
    popConfirmLayer(getMessage("msg.device.reset.macAddress.confirm"), function(){
        formData("NoneForm" , "devSeq", $("#devSeq").val());
        formData("NoneForm" , "macAdrReset","Y");
        callByPut("/api/device", "macResetSuccess", "NoneForm","updateFail");
    });
}

macResetSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("msg.device.reset.macAddress.success"));
        formDataDeleteAll("NoneForm");
        $("#macAdr").html("");
    }
}

updateFail = function(data){
    popAlertLayer(getMessage("msg.common.update.fail"));
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
}

function contsUpdateDelete(){
    popConfirmLayer(getMessage("msg.device.reset.contsList"), function(){
        formData("NoneForm", "devSeq", $("#devSeq").val());
        callByDelete("/api/deviceConts", "contsUpdateDeleteSuccess", "NoneForm");
    });
}

function contsUpdateDeleteSuccess(data){
    formDataDelete("NoneForm", "devSeq");

    if(data.resultCode == "1000"){
        popAlertLayerFnc(getMessage("msg.device.reset.contsList.success"),reload);
    } else if(data.resultCode == "1010"){
        popAlertLayer(getMessage("msg.device.reset.contsList.noData"));
    } else {
        popAlertLayer(getMessage("msg.device.reset.contsList.fail"));
    }
}

deleteDevice = function(){
    var devSeq = $("#devSeq").val();

    if(devSeq == ""){
        popAlertLayer(getMessage("cms.release.msg.noData"));
        return;
    }

    popConfirmLayer(getMessage("msg.device.delete.confirm"), function(){
        formData("NoneForm" , "devSeqs", devSeq);
        callByDelete("/api/device", "deleteDeviceSuccess", "NoneForm","deleteFail");
    },null,getMessage("common.confirm"));
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteDeviceSuccess = function(data){
    if(data.resultCode == "1000"){
        if(sessionStorage.getItem("state") == "view"){
            popAlertLayer(getMessage("success.common.delete"),function(){location.href=sessionStorage.getItem("last-url")});
        } else {
            popAlertLayer(getMessage("success.common.delete"),"/device#page=1&storSeq="+$("#storSeq").val()+"&svcSeq="+$("#svcSeq").val()+"&type=list");
        }
    }
}

