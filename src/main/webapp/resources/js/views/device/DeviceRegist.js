/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var totalCount;
var checkedName;
var oriName;
var arr = new Array();
var str_hash = document.location.hash.replace("#","");
var hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
var hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
$(document).ready(function(){
    limitInputTitle("devId");

    setkeyup();
    svcList();
    devTypeList();
    var timeVal = 5; var timeStart = 5; var timeEnd = 60;
    var playTimeHtml ="<option value=9999>"+getMessage("common.unlimit.uncount.msg")+"</option>";
    for(timeStart; timeStart<=timeEnd; timeStart+=timeVal){
        playTimeHtml += "<option value="+timeStart+">";
        var hour = Math.floor(timeStart/60);
        var time = timeStart%60;
        var timeTxt ="";
        if(hour != 0){
            timeTxt += hour + " "+getMessage("common.time")+" ";
        }

        if(time <10){
            if (time != 0) {
                timeTxt += "0"+time+getMessage("common.minute");
            }
        }else{
            timeTxt += time+getMessage("common.minute");
        }

        playTimeHtml += timeTxt+"</option>";
    }

    playTimeHtml += "<option value='120'>2 "+getMessage("common.time")+" </option>";
    playTimeHtml += "<option value='180'>3 "+getMessage("common.time")+" </option>";
    playTimeHtml += "<option value='userSet'>"+getMessage("common.userSet")+"</option>";

    $("#playTime").html(playTimeHtml);

    /* list 클릭 시, 옵션값 변경*/
    $("body").click(function( event ) {
        var table = $(event.target).parents("table");
        var tContent = $(event.target).parent().parent();
        if(table.hasClass("listBox") == false){
            return;
        }
        if((event.target.nodeName == "TD" && tContent.is("tbody")) || $(event.target).hasClass("ellipsis")){
            var target = $(event.target).parents("tr").toggleClass("checked");
            var type = false;
            if(target.hasClass("checked")) type = true;
            $(target).find("input").attr("checked",type);
        }else{
            if(tContent.is("thead")){
                table.find("input.allcheck").trigger("click");
            }
        }
    });

    $("#userSet").bind('input keyup paste', function() {
        if ($("#userSet").val() >= 9999) {
            popAlertLayer(getMessage("msg.device.too.setTime"));
            $("#userSet").val("");
        }
    });
});

svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
                svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }

        if (hashSvc != "" && hashSvc != null) {
            $("#svcList > option[value=" + hashSvc + "]").attr("selected","true");
            hashSvc = "";
        }

        storList();
    } else {
        popAlertLayer(getMessage("common.bad.request.msg"), makeBakMove());
    }
}

storList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"storListSuccess","NoneForm");
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        checkedName = "";
        var memberStoreListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(memberStoreListHtml);
        $("#storList").attr("disabled", false);
        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 서비스 변경 불가
            $("#storList").attr('disabled', 'true');
        }

        if (hashStore != "" && hashStore != null) {
            $("#storList > option[value=" + hashStore + "]").attr("selected","true");
            hashStore = "";
        }

        $("input").unbind( "click" );
        releaseList();
    } else {
        $("#storList").html("");
        $("#storList").attr("disabled", true);
        $("#frmtnList").html("");
        $("#frmtnList").attr("disabled", true);
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("input:not(.btnCancel):not(.btnLogout)").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });

    }
}

releaseList = function(){
    callByGet("/api/mms/release?svcSeq="+$("#svcList").val()+"&storSeq="+$("#storList").val()+"&rows=1000","releaseListSuccess","NoneForm");
}

releaseListSuccess = function(data){
    if(data.resultCode == "1000"){
        var frmtnHtml = "";
        $(data.result.releaseList).each(function(i,item) {
            frmtnHtml += "<option value="+item.frmtnSeq+">"+item.frmtnNm+"</option>"
            $("#frmtnList").html(frmtnHtml);
            $("#frmtnList").attr("disabled", false);
        })
    } else if(data.resultCode == "1010"){
    }
}

devTypeList = function(){
    callByGet("/api/contentsCategory?unique=on","devTypeListSuccess","NoneForm");
}

devTypeListSuccess = function(data){
    if(data.resultCode == "1000"){
        var devTypListHtml = "";
        $(data.result.contsCtgList).each(function(i,item) {
            devTypListHtml += "<option value='"+item.secondCtgNm+"'>"+item.secondCtgNm+"</option>"
            $("#devType").html(devTypListHtml);
        })
    } else if(data.resultCode == "1010"){
    }
}

devIdDuplication = function(){
    if($("#devId").val().trim() == ""){
        popAlertLayer(getMessage("msg.device.insert.DevId"));
        return;
    }

    callByGet("/api/device?devId="+$("#devId").val()+"&storSeq="+$("#storList").val(), "devIdDuplicationSuccess")
}

devIdDuplicationSuccess = function(data){
    if(data.resultCode == "1010"){
        popAlertLayer(getMessage("msg.device.useful.deviceId"));
        checkedName = $("#devId").val();
    }else{
        popAlertLayer(getMessage("msg.device.not.useful.deviceId"));
        $("#devId").val("").focus();
    }
}

devRegister = function(){
    if($("#devId").val().trim() == ""){
        popAlertLayer(getMessage("msg.device.insert.DevId"));
        return;
    }

    var playTime = $("#playTime").val();
    if ($("#playTime").val() == "userSet") {
        if ($("#userSet").val().trim() == "") {
            popAlertLayer(getMessage("msg.device.no.playTime"));
            return;
        }
        playTime = $("#userSet").val().trim();
    }

    if(checkedName == $.trim($("#devId").val())){
        formData("NoneForm" , "devId", $("#devId").val());
        formData("NoneForm" , "devTypeNm",$("#devType").val());
        formData("NoneForm" , "frmtnSeq", $("#frmtnList option:selected").val());
        formData("NoneForm" , "storSeq", $("#storList").val());
        formData("NoneForm" , "devIpadr",$("#devIpadr").val());
        formData("NoneForm" , "playTime",playTime);
        callByPost("/api/device" , "deviceRegisterSuccess", "NoneForm","InsertFail");
    } else {
        popAlertLayer(getMessage("msg.device.check.useful.deviceId"));
    }
}

InsertFail = function(data){
    popAlertLayer(getMessage("fail.common.insert"));
}


deviceRegisterSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.insert"),"/device#page=1&storSeq="+$("#storList").val()+"&svcSeq="+$("#svcList").val()+"&type=list");
    }
    formDataDeleteAll("NoneForm");
}

function userTimeSet() {
    if ($("#playTime option:selected").val() == "userSet") {
        $("#userSetBox").show();
    } else {
        $("#userSetBox").hide();
    }

}

function makeBakMove(){
    var str_hash = document.location.hash.replace("#","");
    var bakStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
    var bakSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
    pageMove("/device#svcSeq="+bakSvc+"&storSeq="+bakStore);
}


