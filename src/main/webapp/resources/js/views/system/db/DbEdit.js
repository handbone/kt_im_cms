/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    $(".btnModify").click(function() {
        confirmUpdateDbInfo();
    });

    $(".btnCancel").click(function() {
        pageMove('/system/db');
    });

    limitInputTitle("dbNm");

    $("#dbIp").bind("keyup",function(){
        re = /[\ㄱ-ㅎㄲ-ㅆㅏ-ㅢㅣ가-힣a-zA-Z{\}\[\]\/?,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
        var temp=$("#dbIp").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#dbIp").val(temp.replace(re,""));
        }
    });
    setkeyup();

    getDbInfo();
});

getDbInfo = function() {
    formData("NoneForm", "dbSeq", $("#dbSeq").val());
    callByGet("/api/db", "didReceiveDbInfo", "NoneForm", "didNotReceiveDbInfo");
    formDataDeleteAll("NoneForm");
}

didReceiveDbInfo = function(data) {
    if (data.resultCode != "1000") {
        popAlertLayer(getMessage("common.bad.request.msg"), "/system/db");
        return;
    }
    var result = data.result.dbInfo;
    $("#dbNm").val(xssChk(result.dbNm));
    $("#dbIp").val(result.dbIp);
    $("#dbTotSize").val(result.dbTotSize);
}

didNotReceiveDbInfo = function(data) {
    popAlertLayer(getMessage("common.bad.request.msg"), "/system/db");
}

confirmUpdateDbInfo = function() {
    if (!validateForm()) {
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("NoneForm" , "dbSeq", $("#dbSeq").val());
        formData("NoneForm" , "dbNm", $("#dbNm").val());
        formData("NoneForm" , "dbIp", $("#dbIp").val());
        formData("NoneForm" , "dbTotSize", $("#dbTotSize").val());
        callByPut("/api/db", "didUpdateDbInfo", "NoneForm", "didNotUpdateDbInfo");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("button.confirm"));
}

didUpdateDbInfo = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/system/db");
    } else if (data.resultCode == "1013") {
        popAlertLayer(getMessage("fail.common.nochange"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

didNotUpdateDbInfo = function(data) {
    popAlertLayer(getMessage("fail.common.update"));
}

validateForm = function() {
    var isValid = true;
    $("#contents input[type='text']").each(function(i, item) {
        if (!validate(item)) {
            isValid = false;
            return false;
        }
    });

    return isValid;
}

validate = function(obj) {
    var id = obj.id;
    var msg = "";

    var value = $(obj).val();
    if (value.length <= 0) {
        popAlertLayer(getMessage("login.alert.required.msg"), '', '', '', id);
        return false;
    }

    var isValid = true;
    switch (id) {
    case "dbNm":
        isValid = validateLength($("#dbNm").val(), 1, 20);
        if (!isValid) {
            msg = getMessage("db.manage.name.length");
        }
        break;
    case "dbIp":
        isValid = validateIpAddress($("#dbIp").val());
        if (!isValid) {
            msg = getMessage("login.alert.ipAddress.msg");
        }
        break;
    case "dbTotSize":
        isValid = validateOnlyNumber($("#dbTotSize").val());
        if (!isValid) {
            msg = getMessage("fail.common.only.number.msg");
        }
        break;
    default:
        console.log("Nothing id = " + id);
        break;
    }

    if (!isValid) {
        popAlertLayer(msg, '', '', '', id);
    }
    return isValid;
}