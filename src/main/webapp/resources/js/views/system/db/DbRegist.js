/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    $(".btnWrite").click(function() {
        confirmInsertDbInfo();
    });

    $(".btnCancel").click(function() {
        pageMove('/system/db');
    });

    limitInputTitle("dbNm");

    $("#dbIp").bind("keyup",function(){
        re = /[\ㄱ-ㅎㄲ-ㅆㅏ-ㅢㅣ가-힣a-zA-Z{\}\[\]\/?,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
        var temp=$("#dbIp").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#dbIp").val(temp.replace(re,""));
        }
    });

    setkeyup();
});

confirmInsertDbInfo = function() {
    if (!validateForm()) {
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        formData("NoneForm" , "dbNm", $("#dbNm").val());
        formData("NoneForm" , "dbIp", $("#dbIp").val());
        formData("NoneForm" , "dbTotSize", $("#dbTotSize").val());
        callByPost("/api/db", "didInsertDbInfo", "NoneForm", "didNotInsertDbInfo");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("button.confirm"));
}

didInsertDbInfo = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"), "/system/db");
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

didNotInsertDbInfo = function(data) {
    popAlertLayer(getMessage("fail.common.insert"));
}

validateForm = function() {
    var isValid = true;
    $("#contents input[type='text']").each(function(i, item) {
        if (!validate(item)) {
            isValid = false;
            return false;
        }
    });

    return isValid;
}

validate = function(obj) {
    var id = obj.id;
    var msg = "";

    var value = $(obj).val();
    if (value.length <= 0) {
        popAlertLayer(getMessage("login.alert.required.msg"), '', '', '', id);
        return false;
    }

    var isValid = true;
    switch (id) {
    case "dbNm":
        isValid = validateLength($("#dbNm").val(), 1, 20);
        if (!isValid) {
            msg = getMessage("db.manage.name.length");
        }
        break;
    case "dbIp":
        isValid = validateIpAddress($("#dbIp").val());
        if (!isValid) {
            msg = getMessage("login.alert.ipAddress.msg");
        }
        break;
    case "dbTotSize":
        isValid = validateOnlyNumber($("#dbTotSize").val());
        if (!isValid) {
            msg = getMessage("fail.common.only.number.msg");
        }
        break;
    default:
        console.log("Nothing id = " + id);
        break;
    }

    if (!isValid) {
        popAlertLayer(msg, '', '', '', id);
    }
    return isValid;
}