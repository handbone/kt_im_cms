/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    $("#dbList").change(function() {
        getDbInfo();
    });

    $(".btnWrite").click(function() {
        moveRegistDbInfoView();
    });

    $(".btnModify").click(function() {
        moveEditDbInfoView();
    });

    $(".btnDelete").click(function() {
        confirmDeleteDbInfo();
    });
    getFileSize();
    getDbList();
});

getFileSize = function() {
    formData("NoneForm" , "type", "file");
    callByGet("/api/db", "didReceiveFileSize", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didReceiveFileSize = function(data) {
    var result = 0;
    if (data.resultCode == "1000") {
        result = data.result.fileInfo.size;
    }
    graphCreate(result);
}

getDbList = function() {
    formData("NoneForm" , "type", "list");
    callByGet("/api/db", "didReceiveDbList", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didReceiveDbList = function(data) {
    if (data.resultCode == "1000") {
        var dbListHtml = "";
        $(data.result.dbList).each(function(i, item) {
            dbListHtml += "<option value=\"" + item.dbSeq + "\">" + item.dbNm + "</option>";
        });

        $("#dbList").html(dbListHtml);
        $("#dbList option:eq(0)").attr("selected", true);

        $(".tableArea").show();
        getDbInfo();
    } else {
        $(".noContent").show();
    }
}

getDbInfo = function() {
    callByGet("/api/db?dbSeq=" + $("#dbList option:selected").val(), "didReceiveDbInfo");
}

didReceiveDbInfo = function(data) {
    var result = data.result.dbInfo;
    if (data.resultCode == "1000") {
        $("#dbIp").html(result.dbIp);
        $("#dbTotSize").html(result.dbTotSize + "GB");
    } else {
        popAlertLayer(getMessage("fail.common.msg"));
    }
}

changeDbList = function() {
    getDbInfo();
}

moveRegistDbInfoView = function() {
    pageMove("/system/db/regist");
}

moveEditDbInfoView = function() {
    pageMove("/system/db/edit/" + $("#dbList option:selected").val());
}

confirmDeleteDbInfo = function() {
    popConfirmLayer(getMessage("common.delete.msg"), function() {
        formData("NoneForm" , "dbSeq", $("#dbList option:selected").val());
        callByDelete("/api/db", "didDeleteDbInfo", "NoneForm", "didNotDeleteDbInfo");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("button.confirm"));
}

didDeleteDbInfo = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/system/db");
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

didNotDeleteDbInfo = function(data) {
    popAlertLayer(getMessage("fail.common.delete"));
}

graphCreate = function(size) {
    var dataSize = Number((Number(size)/1024/1024).toFixed(2));

    var ctx = document.getElementById("container");
    ctx.height = 20;

    Chart.pluginService.register({
        beforeRender: function (chart) {
            if (chart.config.options.showAllTooltips) {
                // create an array of tooltips
                // we can't use the chart tooltip because there is only one tooltip per chart
                chart.pluginTooltips = [];
                chart.config.data.datasets.forEach(function (dataset, i) {
                    chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                        chart.pluginTooltips.push(new Chart.Tooltip({
                            _chart: chart.chart,
                            _chartInstance: chart,
                            _data: chart.data,
                            _options: chart.options.tooltips,
                            _active: [sector]
                        }, chart));
                    });
                });

                // turn off normal tooltips
                chart.options.tooltips.enabled = false;
            }
        },
        afterDraw: function (chart, easing) {
            if (chart.config.options.showAllTooltips) {
                // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                if (!chart.allTooltipsOnce) {
                    if (easing !== 1)
                        return;
                    chart.allTooltipsOnce = true;
                }

                // turn on tooltips
                chart.options.tooltips.enabled = true;
                Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                    tooltip.initialize();
                    tooltip.update();
                    // we don't actually need this since we are not animating tooltips
                    tooltip.pivot();
                    tooltip.transition(easing).draw();
                });
                chart.options.tooltips.enabled = false;
            }
        }
    })

    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: [getMessage("db.table.content.totalSize")],
            datasets: [{
                label: getMessage("db.manage.size"),
                data: [dataSize],
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)'
                        ],
                        borderWidth: 1
            }]
        },
        maintainAspectRatio: false,
        options: {
            showAllTooltips: true,
            legend:{
                display:false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        return Math.round(data['datasets'][0]['data'][tooltipItem['index']]/1000 * 100)/ 100+"GB";
                    },
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero:true,
                        max: 1800000,
                        min: 0,
                        callback: function(value) { 
                            return value/1000 + "GB";
                        },
                    }
                }]
            }
        }
    });
}