/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    limitInputTitle("codeName");
    setkeyup();

    $("#selectCodeSec").change(function() {
        getCodeDetail();
    });

    $("#btnWrite").click(function() {
        codeNameInsert();
    });
    getCodeSecInfo();
});

function getCodeSecInfo(){
    callByGet("/api/codeSecInfo", "getCodeSecInfoSuccess");
}

function getCodeSecInfoSuccess(data) {
    if (data.resultCode != "1000") {
        return;
    }

    var codeSectionHtml = "";
    $(data.result.codeList).each(function(i,item) {
        codeSectionHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
    });

    $("#selectCodeSec").html(codeSectionHtml);
    $("#selectCodeSec option:eq(0)").prop("selected", true);

    getCodeDetail();
}

function getCodeDetail() {
    formData("NoneForm", "comnCdCtg", $("#selectCodeSec option:selected").val());
    callByGet("/api/codeInfo", "getCodeDetailSuccess", "NoneForm"); 
    formDataDeleteAll("NoneForm");
}

function getCodeDetailSuccess(data) {
    $("#codeInfoDiv tr:nth-child(n + 3)").remove();

    if (data.resultCode != "1000") {
        return;
    }

    $("#detailCodeCount").attr("rowspan", (data.result.codeList.length + 1));
    var codeNameHtml = "";
    $(data.result.codeList).each(function(i, item) {
        codeNameHtml += "<tr>";
        codeNameHtml += "<td><input id='codeName" + item.comnCdSeq + "' type='text' class='inputBox itemW100 txtCenter remaining' max='50' value='" + item.comnCdNm + "'></td>";
        codeNameHtml += "<td><input id='codeValue" + item.comnCdSeq + "' type='text' class='inputBox width100 txtCenter' value='" + item.comnCdValue + "'></td>";
        codeNameHtml += "<td><input type='button' class='btnSmallGray' value='" + getMessage("button.update") + "' onclick='codeNameUpdate(" + item.comnCdSeq + ")'>";
        codeNameHtml += "&nbsp;<input type='button' class='btnSmallGray' value='" + getMessage("button.delete") + "' onclick='codeNameDelete(" + item.comnCdSeq + ")'></td>";
        codeNameHtml += "</tr>";
    });

    $("#codeInfoDiv").append(codeNameHtml);

    $(data.result.codeList).each(function(i, item) {
        $("#codeName" + item.comnCdSeq).bind("keyup",function(){
            re = /[\<>&\"']/gi;
            var temp=$("#codeName" + item.comnCdSeq).val();
            if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
                $("#codeName" + item.comnCdSeq).val(temp.replace(re,""));
            }
        });
    });

    setkeyup();
}

function codeNameInsert() {
    if (!$("#codeName").val()) {
        popAlertLayer(getMessage("code.manage.required.name.msg"), "", "", "", "codeName");
        return;
    }

    if (!validateMaxLength($("#codeName").val(), 50)) {
        popAlertLayer(getMessage("code.manage.name.maxLength.msg"), "", "", "", "codeName");
        return;
    }

    if(!$("#codeValue").val()) {
        popAlertLayer(getMessage("code.manage.required.value.msg"), "", "", "", "codeValue");
        return;
    }

    if (!validateMaxLength($("#codeValue").val(), 15)) {
        popAlertLayer(getMessage("code.manage.value.maxLength.msg"), "", "", "", "codeName");
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function(){
        formData("NoneForm", "comnCdCtg", $("#selectCodeSec option:selected").val());
        formData("NoneForm", "comnCdNm", $("#codeName").val());
        formData("NoneForm", "comnCdValue", $("#codeValue").val());

        callByPost("/api/codeInfo", "codeNameInsertSuccess", "NoneForm", "codeNameInsertFail");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("button.confirm"));
}

function codeNameInsertSuccess(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"));
        $("#codeName").val("");
        $("#codeValue").val("")
        getCodeDetail();
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("code.manage.exist.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

function codeNameInsertFail(data) {
    popAlertLayer(getMessage("fail.common.insert"));
}

function codeNameUpdate(codeSeq) {
    var codeNameId = "codeName" + codeSeq;
    var codeValueId = "codeValue" + codeSeq;
    if (!$("#" + codeNameId).val()) {
        popAlertLayer(getMessage("code.manage.required.name.msg"), "", "", "", codeNameId);
        return;
    }

    if (!validateMaxLength($("#" + codeNameId).val(), 50)) {
        popAlertLayer(getMessage("code.manage.name.maxLength.msg"), "", "", "", codeNameId);
        return;
    }

    if(!$("#" + codeValueId).val()) {
        popAlertLayer(getMessage("code.manage.required.value.msg"), "", "", "", codeValueId);
        return;
    }

    if (!validateMaxLength($("#" + codeValueId).val(), 15)) {
        popAlertLayer(getMessage("code.manage.value.maxLength.msg"), "", "", "", codeValueId);
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function(){
        formData("NoneForm", "comnCdSeq", codeSeq);
        formData("NoneForm", "comnCdNm", $("#"+ codeNameId).val());
        formData("NoneForm", "comnCdValue", $("#" + codeValueId).val());

        callByPut("/api/codeInfo", "codeNameUpdateSuccess", "NoneForm", "codeNameUpdateFail");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("button.confirm"));
}

function codeNameUpdateSuccess(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"));
        getCodeDetail();
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("code.manage.exist.msg"));
    } else if (data.resultCode == "1013") {
        popAlertLayer(getMessage("fail.common.nochange"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function codeNameUpdateFail(data) {
    popAlertLayer(getMessage("fail.common.update"));
}

function codeNameDelete(codeSeq) {
    if ($("#codeInfoDiv tr").length == 3) {
        popAlertLayer(getMessage("code.manage.dont.delete.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.delete.msg"), function(){
        formData("NoneForm", "comnCdSeq", codeSeq);
        callByDelete("/api/codeInfo", "codeNameDeleteSuccess", "NoneForm", "codeNameDeleteFail");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("button.confirm"));
}

function codeNameDeleteSuccess(data){
    if (data.resultCode != "1000") {
        popAlertLayer(getMessage("fail.common.delete"));
        return;
    }

    popAlertLayer(getMessage("success.common.delete"));
    getCodeDetail();
}

function codeNameDeleteFail(data) {
    popAlertLayer(getMessage("fail.common.delete"));
}