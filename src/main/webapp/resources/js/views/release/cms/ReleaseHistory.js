/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
$(document).ready(function(){
    setkeyup();
    historyInfo();
});


historyInfo = function(){
    callByGet("/api/cms/releaseRecord?frmtnSeq="+$("#frmtnSeq").val(), "historyInfoSuccess", "NoneForm", "historyInfoFail");
}

historyInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var ritem = data.result.releaseRecord;
        var contsList = data.result.recordList;

        $("#frmtnNm").html(ritem.frmtnNm);
        if (ritem.svcNm == "") {
            $("#svcNm").html(getMessage("select.all"));
        } else {
            $("#svcNm").html(ritem.svcNm);
        }

        $(contsList).each(function(i,item){
            var obj;
            if (item.frmtnSttus == "D") {
                obj = $(".tableList").eq(1);
            } else {
                obj = $(".tableList").eq(0);
            }

            $(obj).find("tbody").append(
                    "<tr>"+
                    "<td>"+item.contsTitle+"</td>"+
                    "<td class=\"itemW20\">"+item.cretDt+"</td>"+
                    "</tr>"
            );
        });
        var seq = 0;
        $(".tableList").each(function(i){
            if ($.trim($(this).find("tbody").html()) == ""){
                $(this).parents(".releaseScrollBoxs").remove();
                $("span.title").eq(seq).remove();
                seq--;
            }
            seq++;
        })

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
    }
}

historyInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
}

