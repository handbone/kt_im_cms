/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var modelHidden = true;
var readyPage = false;
var hashSvc;
var hashType;
var selTab =0;
var gridTotalWidth = 0;
var apiUrl = makeAPIUrl("/api/cms/release");
var lastBind = false;
var gridState;
var tempState;
var cretrIdOption;
var ctgOption;
var totalPage = 0;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
});
$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashType = checkUndefined(findGetParameter(str_hash,"type"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#svcList").val(hashSvc);
            jQuery("#jqgridData").trigger("reloadGrid");
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    svcList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

function releaseList(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashType = checkUndefined(findGetParameter(str_hash,"type"));
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
        $("#svcList").val(hashSvc);
        $("#type").val(hashType);

        gridState = "NONE";
    }
    $("#jqgridData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.releaseList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:[
            getMessage("column.title.num"),
            getMessage("column.title.release"),
            getMessage("column.title.contsCount"),
            getMessage("column.title.cretDt"),
            getMessage("column.title.frmtnSeq")],
        colModel: [
            {name:"num", index: "num", align:"center", width:25,hidden:false},
            {name:"frmtnNm", index:"FRMTN_NM", align:"center",formatter:pointercursor},
            {name:"contsCount", index:"CONTS_COUNT", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"frmtnSeq", index:"FRMTN_SEQ", hidden:true}
       ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "frmtnSeq",
            sortorder: "desc",
//          caption:"편성 목록",
            height: "auto",
            multiselect: true,
            beforeRequest:function(){
                tempState = gridState;
                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

                if (gridState != "SEARCH") {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                }

                if (isNaN(page)) {
                    page =1;
                }

                if (totalPage > 0 && totalPage < page) {
                    page = 1;
                }

                searchField = checkUndefined($("#target").val());
                searchString = checkUndefined($("#keyword").val());
                hashSvc = checkUndefined($("#svcList").val());
                hashType = checkUndefined($("#type").val());

                if (hashType == "list" || hashType == null) {
                    hashType = "list";
                    selTab = 0;
                } else if (hashType == "stat") {
                    selTab = 1;
                }

                modelSet();

                $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
                $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

                if (gridState == "HASH"){
                    myPostData.page = page;
                    tempState = "READY";
                } else {
                    if(tempState == "TABVAL" || tempState == "SEARCH"){
                        myPostData.page = 1;
                    }
                    if (tempState != "SEARCH") {
                        tempState = "";
                    }
                }

                if(gridState == "NONE"){
                    searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                    searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                    myPostData.page = page;

                    if (searchField != null) {
                        tempState = "SEARCH";
                    } else {
                        tempState = "READY";
                    }
                }

                if(searchField != null && searchField != "" && searchString != ""){
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                }else{
                    delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
                }

                gridState = "GRIDBEGIN";

                if (hashSvc != null && hashSvc != "") {
                    myPostData.svcSeq = hashSvc;
                } else {
                    myPostData.svcSeq = "";
                }

                myPostData.type = hashType;

                if (noData) {
                    myPostData.page = 0;
                    hashSvc = "";
                    popAlertLayer(getMessage("info.service.nodata.msg"));
                }

                $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData });
            },
            loadComplete: function (data) {
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                //session
                if(sessionStorage.getItem("last-url") != null){
                    sessionStorage.removeItem('state');
                    sessionStorage.removeItem('last-url');
                }

                //Search 필드 등록
                searchFieldSet();

                if (data.resultCode == 1000) {
                    totalPage = data.result.totalPage;

                    if (page != data.result.currentPage) {
                        tempState = "";
                    }
                } else {
                    tempState = "";
                }

                /*뒤로가기*/
                var hashlocation = "page="+$(this).context.p.page+"&svcSeq="+hashSvc+"&type="+$("#type").val();
                if($(this).context.p.postData._search && $("#target").val() != null){
                    hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
                }

                gridState = "GRIDCOMPLETE";
                document.location.hash = hashlocation;

                if (tempState != "" || str_hash == hashlocation) {
                    gridState = "READY";
                }

              //pageMove Max
                $('.ui-pg-input').on('keyup', function() {
                    this.value = this.value.replace(/\D/g, '');
                    if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
                });

                $(this).find(".pointer").parent("td").addClass("pointer");


            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event){
                // 콘텐츠 ID 선택시
                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");

                if(columnId == 2 && hashType == "list"){
                  var basicChk = "";
                  if ( list.frmtnSeq == 1) {
                      basicChk = "#svcSeq="+$("#svcList").val();
                  }
                  pageMove("/cms/release/"+list.frmtnSeq+"/edit"+basicChk);
                } else if(columnId == 2){
                  pageMove("/cms/release/"+list.frmtnSeq+"/stat");
                }
            }
    }).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}


var noData = false;
svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
        });
        $("#svcList").html(svcListHtml);
        noData = false;
        releaseList();
    } else {
        noData = true;
        releaseList();
    }
}

function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {

        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
                case "frmtnNm":break;
                default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }

        cretrIdOption = $("#target option[value='CRETR_ID']");
        ctgOption = $("#target option[value='CTG_NM']");

    }
    $("#target").val(searchField);
}

function tabVal(seq,e){
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn");
    $(e).parent("div").addClass("tabOn").css({ 'pointer-events': 'none' });
    selTab = seq;
    $("#selectBox").val("");

    $("#target").val("");
    $("#keyword").val("");

    totalPage = 0;

    switch(selTab){
    case 0:
        $("#type").val("list");
        $(".CenterGrp .cellTable").show();
        break;
    case 1:
        $("#type").val("stat");
        $(".CenterGrp .cellTable").hide();
        break;
    }
    // 190313 탭 간 이동 시 서비스명 유지하기 위해 주석처리 함.
    //$("#svcList").val("");

    jQuery("#jqgridData").trigger("reloadGrid");

}

modelSet = function(){
    var ccm = $("#jqgridData").getGridParam();
    switch(selTab){
    case 0:
        $("#type").val("list");
        $(".CenterGrp .cellTable").show();
        break;
    case 1:
        $("#type").val("stat");
        $(".CenterGrp .cellTable").hide();
        break;
    }

    if (selTab == 0) {
        $("#jqgridData").jqGrid("showCol", ["cb","contsCount"]);
    } else if (selTab == 1) {
        $("#jqgridData").jqGrid("hideCol", ["cb","contsCount"]);
    }



    $("#jqgridData").setGridWidth($("#contents").width());
    $("#jqgridData").jqGrid('setGridParam',ccm);
}

deleteRelease = function(){
    var frmtnSeq = "";
    var frmtnNms = "";
    var basicCheck = false;

    for(var i=1; i<$("#limit").val()+1; i++){
        if($("#jqg_jqgridData_"+i).prop("checked")){
            var list = $("#jqgridData").jqGrid('getRowData', i);
            var frmtnNm = $("#jqgridData").jqGrid('getRowData', i).frmtnNm.replace(/(<([^>]+)>)/ig,"");
            if (frmtnNms != "") {
                frmtnNms += ", ";
            }

            if (frmtnNm != "basic") {
                frmtnSeq += list.frmtnSeq + ",";
                frmtnNms += frmtnNm;
            } else {
                basicCheck = true;
            }
        }
    }
    if (basicCheck) {
        basicCheck = "<br>(basic "+getMessage("msg.release.dont.delFrmtn")+")";
    } else {
        basicCheck = "";
    }

    if(frmtnSeq == ""){
        popAlertLayer(getMessage("common.select.obj.delete.msg")+basicCheck);
        return;
    }

    popConfirmLayer(frmtnNms+" "+getMessage("common.release.delete.confirm")+basicCheck, function(){
        formData("NoneForm" , "frmtnSeqs", frmtnSeq.slice(0,-1));
        callByDelete("/api/cms/release", "deleteReleaseSuccess", "NoneForm","deleteFail");
    },null,getMessage("common.confirm"));
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteReleaseSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"),function(){location.reload()});
    }
}

moveToRegistView = function() {
    if ($("#svcList").val() == null) {
        popAlertLayer(getMessage("info.service.or.store.nodata.msg"));
        return;
    }

    pageMove("/cms/release/regist?svcSeq=" + $("#svcList").val());
}
