/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    svcFaqInfo();
});

svcFaqInfo = function() {
    formData("NoneForm", "svcFaqSeq", $("#svcFaqSeq").val());
    callByGet("/api/svcFaq", "svcFaqInfoSuccess", "NoneForm", "svcFaqInfoFail");
}

svcFaqInfoSuccess = function(data) {
    formDataDelete("NoneForm", "svcFaqSeq");
    if (data.resultCode == "1000") {
        var faqDetail = data.result.faqDetail;
        $("#faqTitleQsn").html("<span class='txtRed'>" + getMessage("common.question") + "</span><span class='faqDate'>"
                    + getMessage("common.section") + " : " + faqDetail.svcFaqTypeNm + "<span class='div'>|</span>"
                    + getMessage("common.category") + " : " + faqDetail.svcFaqCtgNm + "<span class='div'> | </span>"
                    + getMessage("common.reger") + " : " + faqDetail.cretrNm + "<span class='div'> | </span>"
                    + getMessage("common.regdate") + " : " + faqDetail.regDt + "</span>");
        $("#faqQsn").html(faqDetail.svcFaqTitle);
        $("#faqAns").html(faqDetail.svcFaqSbst);
        //$("#faqAns").html(xssChk(faqDetail.svcFaqSbst));
        //$("#faqAns").html($("#faqAns").text());

        if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02' && faqDetail.cretrMbrSe < $("#memberSec").val()) {
            $("#btnModify").hide();
            $("#btnDelete").hide();
        } else {
            // 매장 관리자인 경우 동일 매장의 관리자가 작성한 것이 아닐경우 수정 및 삭제 불가
            if ($("#memberSec").val() == "05" 
                && faqDetail.cretrMbrSvcSeq == $("#memberSvcSeq").val()
                && faqDetail.cretrMbrStorSeq != $("#memberStorSeq").val()) {
                $("#btnModify").hide();
                $("#btnDelete").hide();
            }
        }

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/servicecustomer#page=1&display=faq");
    }
}

svcFaqInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/servicecustomer#page=1&display=faq");
}

function deleteFaq() {
    popConfirmLayer(getMessage("faq.delete.confirm.msg"), function() {
        formData("NoneForm" , "delSvcFaqSeqList", $("#svcFaqSeq").val());
        callByDelete("/api/svcFaq", "deleteSvcFaqSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

deleteSvcFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/servicecustomer#page=1&display=faq");
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}