/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    limitInputTitle("faqTitle");

    setkeyup();

    faqTypeList();
});

//서비스 공지사항은 구분 항목을 각 서비스로 지정
faqTypeList = function() {
    callByGet("/api/service?searchType=list", "faqTypeListSuccess", "NoneForm", "faqTypeListFail");
}

faqTypeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var faqTypeListHtml = "";
        var mbrSe = $("#memberSec").val();
        if (mbrSe == "01" || mbrSe == "02") {
            faqTypeListHtml += "<option value=\"" + "0" + "\">" + "전체서비스" + "</option>";
        }
        $(data.result.serviceNmList).each(function(i, item) {
            faqTypeListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
        });
        $("#typeSelbox").html(faqTypeListHtml);
        if (mbrSe != "01" && mbrSe != "02") {
            $("#typeSelbox").attr('disabled', true);
        }

        faqCategoryList();
    } else {
        $(".btnWrite").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

faqTypeListFail = function(data) {
    $(".btnWrite").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

faqCategoryList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SVC_FAQ_CTG", "faqCategoryListSuccess","NoneForm", "faqCategoryListFail");
}

faqCategoryListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var faqCategoryListHtml = "";
        $(data.result.codeList).each(function(i, item) {
            faqCategoryListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#ctgSelbox").html(faqCategoryListHtml);
    } else {
        $(".btnWrite").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

faqCategoryListFail = function(data) {
    $(".btnWrite").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

function registFaq() {
    if ($("#faqTitle").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.faqSbst.getData().length < 1) {
        popAlertLayer(getMessage("faq.answer.sbst.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        CKEDITOR.instances.faqSbst.updateElement();
        $("#faqSbst").val(CKEDITOR.instances.faqSbst.getData());

        formData("NoneForm", "svcFaqTitle", $("#faqTitle").val());
        formData("NoneForm", "svcFaqType", $("#typeSelbox").val());
        formData("NoneForm", "svcFaqCtg", $("#ctgSelbox").val());
        formData("NoneForm", "svcFaqSbst", $("#faqSbst").val());

        callByPost("/api/svcFaq", "registSvcFaqSuccess", "NoneForm", "insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

registSvcFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"), "/servicecustomer#page=1&display=faq");
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}
