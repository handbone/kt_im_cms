/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    svcNoticeInfo();
});

svcNoticeInfo = function() {
    formData("NoneForm", "svcNoticeSeq", $("#svcNoticeSeq").val());
    callByGet("/api/svcNotice", "svcNoticeInfoSuccess", "NoneForm", "svcNoticeInfoFail");
}

svcNoticeInfoSuccess = function(data) {
    formDataDelete("NoneForm", "svcNoticeSeq");
    if (data.resultCode == "1000") {
        var noticeDetail = data.result.noticeDetail;
        $("#noticeTitle").html(noticeDetail.svcNoticeTitle + "<span class='noticeDate'>" + getMessage("common.section")
                                + " : " + noticeDetail.svcNoticeTypeNm + "<span class='div'>|</span>" + getMessage("common.reger")
                                + " : " + noticeDetail.cretrNm + "<span class='div'>|</span>" + getMessage("common.regdate")
                                + " : " + noticeDetail.regDt + "</span>");
        $("#noticeSbst").html(noticeDetail.svcNoticeSbst);
        //$("#noticeSbst").html(xssChk(noticeDetail.svcNoticeSbst));
        //$("#noticeSbst").html($("#noticeSbst").text());

        if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02' && noticeDetail.cretrMbrSe < $("#memberSec").val()) {
            $("#btnModify").hide();
            $("#btnDelete").hide();
        } else {
            // 매장 관리자인 경우 동일 매장의 관리자가 작성한 것이 아닐경우 수정 및 삭제 불가
            if ($("#memberSec").val() == "05" 
                && noticeDetail.cretrMbrSvcSeq == $("#memberSvcSeq").val()
                && noticeDetail.cretrMbrStorSeq != $("#memberStorSeq").val()) {
                $("#btnModify").hide();
                $("#btnDelete").hide();
            }
        }

        if (data.result.fileList.length == 0) {
            $("#fileGrp").hide();
        } else {
            var fileListHtml = "<table width='100%'><tr><td class='attatch'><div class='txtFileadd'>" + getMessage("common.attachfile") + " : </div></td><td class='attatchFile'>";
            $(data.result.fileList).each(function(i,item) {
                fileListHtml += "<p><span class='float_l'>" + item.fileName + "</span><a href='" + makeAPIUrl("/api/fileDownload/" + item.fileSeq) + "' class='fontblack btnDownloadGray'>[" + getMessage("common.download") + "]</a></p>";
            });
            fileListHtml += "</td></tr></table>";
            $("#fileGrp").html(fileListHtml);
        }

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/servicecustomer#page=1&display=notice");
    }
}

svcNoticeInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/servicecustomer#page=1&display=notice");
}

function deleteNotice() {
    popConfirmLayer(getMessage("notice.delete.confirm.msg"), function() {
        formData("NoneForm" , "delSvcNoticeSeqList", $("#svcNoticeSeq").val());
        callByDelete("/api/svcNotice", "deleteSvcNoticeSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

deleteSvcNoticeSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/servicecustomer#page=1&display=notice");
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}