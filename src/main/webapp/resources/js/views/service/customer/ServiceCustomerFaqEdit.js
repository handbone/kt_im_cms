/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var faqType;
var faqCtg;
var cretrMbrSe;

$(document).ready(function(){
    limitInputTitle("faqTitle");

    setkeyup();

    svcFaqInfo();
});

svcFaqInfo = function() {
    formData("NoneForm", "svcFaqSeq", $("#svcFaqSeq").val());
    formData("NoneForm", "searchType", "edit");
    callByGet("/api/svcFaq", "svcFaqInfoSuccess", "NoneForm", "svcFaqInfoFail");
}

svcFaqInfoSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        var faqDetail = data.result.faqDetail;
        faqType = faqDetail.svcFaqType;
        faqCtg = faqDetail.faqCtg;
        cretrMbrSe = faqDetail.cretrMbrSe;

        $("#faqTitle").val(faqDetail.svcFaqTitle);

        var strFaqSbst = strConv(faqDetail.svcFaqSbst);
        strFaqSbst = strConv(strFaqSbst);
        CKEDITOR.instances.faqSbst.setData(strFaqSbst);

        $("input[name='delYn'][value=" + faqDetail.delYn + "]").attr("checked", true);

        listBack($(".btnCancel"));

        faqTypeList();
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/servicecustomer#page=1&display=faq");
    }
}

svcFaqInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/servicecustomer#page=1&display=faq");
}

//서비스 공지사항은 구분 항목을 각 서비스로 지정
faqTypeList = function() {
    callByGet("/api/service?searchType=list", "faqTypeListSuccess", "NoneForm", "faqTypeListFail");
}

faqTypeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var faqTypeListHtml = "";
        if (cretrMbrSe == "01" || cretrMbrSe == "02") {
            faqTypeListHtml += "<option value=\"" + "0" + "\">" + "전체서비스" + "</option>";
        }
        $(data.result.serviceNmList).each(function(i, item) {
            faqTypeListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
        });
        $("#typeSelbox").html(faqTypeListHtml);
        $("#typeSelbox").val(faqType);
        if (cretrMbrSe != "01" && cretrMbrSe != "02") {
            $("#typeSelbox").attr('disabled', true);
        }

        faqCategoryList();
    } else {
        $(".btnModify").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

faqTypeListFail = function(data) {
    $(".btnModify").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

faqCategoryList = function() {
    callByGet("/api/codeInfo?comnCdCtg=SVC_FAQ_CTG", "faqCategoryListSuccess", "NoneForm", "faqCategoryListFail");
}

faqCategoryListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var faqCategoryListHtml = "";
        $(data.result.codeList).each(function(i, item) {
            faqCategoryListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#ctgSelbox").html(faqCategoryListHtml);
        $("#ctgSelbox").val(faqCtg);
    } else {
        $(".btnModify").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

faqCategoryListFail = function(data) {
    $(".btnModify").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

function updateFaq() {
    if ($("#faqTitle").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.faqSbst.getData().length < 1) {
        popAlertLayer(getMessage("faq.answer.sbst.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        CKEDITOR.instances.faqSbst.updateElement();
        $("#faqSbst").val(CKEDITOR.instances.faqSbst.getData());

        formData("NoneForm", "svcFaqSeq", $("#svcFaqSeq").val());
        formData("NoneForm", "svcFaqTitle", $("#faqTitle").val());
        formData("NoneForm", "svcFaqType", $("#typeSelbox").val());
        formData("NoneForm", "svcFaqCtg", $("#ctgSelbox").val());
        formData("NoneForm", "svcFaqSbst", $("#faqSbst").val());
        formData("NoneForm", "delYn", $('input:radio[name=delYn]:checked').val());

        callByPut("/api/svcFaq", "updateFaqSuccess", "NoneForm", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

updateFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/servicecustomer#page=1&display=faq");
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}