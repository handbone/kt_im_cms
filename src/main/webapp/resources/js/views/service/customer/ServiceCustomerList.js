/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var modelHidden = true;
var hashDisplay;
var selTab =0;
var gridTotalWidth = 0;
var apiUrl = makeAPIUrl("/api/svcNotice");
var gridState;
var tempState;
var totalPage = 0;

$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashDisplay = checkUndefined(findGetParameter(str_hash,"display"));
            if (hashDisplay == "notice" || hashDisplay == null) {
                if ($("#display").val() == "faq") {
                    totalPage = 0;
                    $("#jqgridFaqData").jqGrid("GridUnload");
                    $("#jqgridFaqData").hide();
                    $("#jqgridSvcNoticeData").show();
                    $("#display").val(hashDisplay);
                    serviceNoticeList();
                } else {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                    jQuery("#jqgridSvcNoticeData").trigger("reloadGrid");
                }
            } else {
                if ($("#display").val() == "notice") {
                    totalPage = 0;
                    $("#jqgridSvcNoticeData").jqGrid("GridUnload");
                    $("#jqgridSvcNoticeData").hide();
                    $("#display").val(hashDisplay);
                    $("#jqgridFaqData").show();
                    serviceFaqList();
                } else {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                    jQuery("#jqgridFaqData").trigger("reloadGrid");
                }
            }
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashDisplay = checkUndefined(findGetParameter(str_hash,"display"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#display").val(hashDisplay);
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";

        if (hashDisplay == "notice" || hashDisplay == null) {
            $("#jqgridFaqData").jqGrid("GridUnload");
            $("#jqgridFaqData").hide();
            $("#jqgridSvcNoticeData").show();
            $("#display").val(hashDisplay);
        } else {
            $("#jqgridSvcNoticeData").jqGrid("GridUnload");
            $("#jqgridSvcNoticeData").hide();
            $("#display").val(hashDisplay);
            $("#jqgridFaqData").show();
        }
    }

    if ($("#display").val() == "notice") {
        serviceNoticeList();
    } else {
        serviceFaqList();
    }

    $("#keyword").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

serviceNoticeList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("table.title"),
        getMessage("table.name"),
        getMessage("table.section"),
        getMessage("table.retvnum"),
        getMessage("table.file"),
        getMessage("table.regdate"),
        "svcNoticeType",
        "cretrMbrSe",
        "cretrMbrSvcSeq",
        "cretrMbrStorSeq",
        "delYn",
        "svcNoticeSeq"
    ];

    $("#jqgridSvcNoticeData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.noticeList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:columnNames,
        colModel: [
            {name:"num", index: "num", align:"center", width:40},
            {name:"svcNoticeTitle", index: "SVC_NOTICE_TITLE", align:"left",formatter:pointercursor, width:400},
            {name:"cretrNm", index:"CRETR_NM", align:"center"},
            {name:"svcNoticeTypeNm", index:"SVC_NOTICE_TYPE_NM", align:"center"},
            {name:"retvNum", index:"RETV_NUM", align:"center"},
            {name:"fileExist", index:"FILE", align:"center", cellattr: function(rowId, val, rowObject, cm, rdata) {return 'title=""';}},
            {name:"regDt", index:"REG_DT", align:"center", width:180},
            {name:"svcNoticeType", index:"SVC_NOTICE_TYPE", hidden:true},
            {name:"cretrMbrSe", index:"CRETR_MBR_SE", hidden:true},
            {name:"cretrMbrSvcSeq", index:"CRETR_MBR_SVC_SEQ", hidden:true},
            {name:"cretrMbrStorSeq", index:"CRETR_MBR_STOR_SEQ", hidden:true},
            {name:"delYn", index:"DEL_YN", hidden:true},
            {name:"svcNoticeSeq", index:"SVC_NOTICE_SEQ", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        pager: "#noticePageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "SVC_NOTICE_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: true,
        beforeRequest:function() {
            tempState = gridState;
            var myPostData = $('#jqgridSvcNoticeData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState == "TABVAL") {
                searchField = "";
                searchString = "";
            } else if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());

            hashSttus = checkUndefined($("#sttusSelbox").val());
            hashDisplay = "notice"
            apiUrl = makeAPIUrl("/api/svcNotice");
            selTab = 0;

            $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
            $(".conTitleGrp div:eq(" + selTab + ")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

            setHeaderUse();

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if(tempState == "TABVAL" || tempState == "SEARCH") {
                    myPostData.page = 1;
                    page = 1;
                }
                if (tempState != "SEARCH") {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString; myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "") {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            myPostData.display = hashDisplay;
            $('#jqgridSvcNoticeData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page});
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            //session
            if(sessionStorage.getItem("last-url") != null){
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            //Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /*뒤로가기*/
            var hashlocation = "page=" + $(this).context.p.page + "&display="+$("#display").val();
            if ($(this).context.p.postData._search && $("#target").val() != null) {
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            // Master 관리자, CMS 관리자가 아닌 경우 checkbox selectAll 비활성화
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02') {
                $("#cb_jqgridNoticeData").attr("disabled", true);
            }

            $(this).find("td").each(function(){
                if(typeof $(this).parents("tr").attr("tabindex") != "undefined"){
                    if ($(this).index() == 0) {
                        // 권한이 맞지 않는 글일 경우 삭제할 수 없도록 checkbox 비활성화
                        var cretrMbrSe = $(this).parent("tr").find("td:eq(9)").text();
                        var cretrMbrSvcSeq = $(this).parent("tr").find("td:eq(10)").text();
                        var cretrMbrStorSeq = $(this).parent("tr").find("td:eq(11)").text();

                        if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02' && (cretrMbrSe < Number($("#memberSec").val()))) {
                            $(this).parent("tr").find("td > input.cbox").attr("disabled", true);
                        } else {
                            if ($("#memberSec").val() == '04' && cretrMbrSvcSeq != $("#memberSvcSeq").val()) {
                                $(this).parent("tr").find("td > input.cbox").attr("disabled", true);
                            } else if ($("#memberSec").val() == '05' && cretrMbrStorSeq != $("#memberStorSeq").val()) {
                                $(this).parent("tr").find("td > input.cbox").attr("disabled", true);
                            } else {
                                $(this).parent("tr").find("td > input.cbox").attr("disabled", false);
                            }
                        }
                    } else if ($(this).index() == 2) {
                        if ($(this).parent("tr").find("td:eq(12)").text() == 'Y') { // 비활성 여부 확인
                            if ($(this).children("span.pointer").width() > ($(this).width() - 40)) {
                                // 비활성 상태 : 컬럼 크기에서 hidden 아이콘 값을 제외한 width 보다 내용이 클 경우
                                $(this).html("<span class='pointer' style='width: 90%; text-overflow: ellipsis; overflow: hidden; float: left;'>"
                                        + $(this).text() + "</span>" + "<img src='" + makeAPIUrl("/resources/image/icon_hidden.png") + "' style='width: 40px; height: 15px; vertical-align: top;'>");
                            } else {
                                $(this).html("<span class='pointer hiddenIcon'>" + $(this).text() + "</span>");
                            }
                        }
                    } else if ($(this).index() == 6) { // FILE
                        if ($(this).text() == "true") {
                            $(this).html("<img src='" + makeAPIUrl("/resources/image/icon_file.png") + "' style='width: 13px;vertical-align: middle;' />");
                        } else {
                            $(this).html("");
                        }
                    }
                }
            });

            //pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridSvcNoticeData").getGridParam("lastpage")) this.value = $("#jqgridSvcNoticeData").getGridParam("lastpage");
            });

            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridSvcNoticeData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridSvcNoticeData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event) {
            var list = $("#jqgridSvcNoticeData").jqGrid('getRowData', rowId);
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02'
                && ((list.cretrMbrSe < Number($("#memberSec").val()))
                    || ($("#memberSec").val() == '05' && list.cretrMbrStorSeq != $("#memberStorSeq").val()))) {
                $('#jqgridSvcNoticeData').jqGrid('setSelection', rowId, false);
            }
            // 콘텐츠 ID 선택시
            if(columnId == 2){
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/servicecustomer/notice/" + list.svcNoticeSeq);
            }
        }
    });
    jQuery("#jqgridSvcNoticeData").jqGrid('navGrid','#noticePageDiv',{del:false,add:false,edit:false,search:false});
}

serviceFaqList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("table.title"),
        getMessage("table.name"),
        getMessage("table.section"),
        getMessage("table.category"),
        getMessage("table.retvnum"),
        getMessage("table.regdate"),
        "svcFaqType",
        "svcFaqCtg",
        "cretrMbrSe",
        "cretrMbrSvcSeq",
        "cretrMbrStorSeq",
        "delYn",
        "svcFaqSeq"
    ];

    $("#jqgridFaqData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.faqList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:columnNames,
        colModel: [
            {name:"num", index: "num", align:"center", width:40},
            {name:"svcFaqTitle", index: "SVC_FAQ_TITLE", align:"left",formatter:pointercursor, width:400},
            {name:"cretrNm", index:"CRETR_NM", align:"center"},
            {name:"svcFaqTypeNm", index:"SVC_FAQ_TYPE_NM", align:"center"},
            {name:"svcFaqCtgNm", index:"SVC_FAQ_CTG_NM", align:"center"},
            {name:"retvNum", index:"RETV_NUM", align:"center"},
            {name:"regDt", index:"REG_DT", align:"center", width:180},
            {name:"svcFaqType", index:"SVC_FAQ_TYPE", hidden:true},
            {name:"svcFaqCtg", index:"SVC_FAQ_CTG", hidden:true},
            {name:"cretrMbrSe", index:"CRETR_MBR_SE", hidden:true},
            {name:"cretrMbrSvcSeq", index:"CRETR_MBR_SVC_SEQ", hidden:true},
            {name:"cretrMbrStorSeq", index:"CRETR_MBR_STOR_SEQ", hidden:true},
            {name:"delYn", index:"DEL_YN", hidden:true},
            {name:"svcFaqSeq", index:"SVC_FAQ_SEQ", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        pager: "#faqPageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "SVC_FAQ_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: true,
        beforeRequest:function() {
            tempState = gridState;
            var myPostData = $('#jqgridFaqData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState == "TABVAL") {
                searchField = "";
                searchString = "";
            } else if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());

            hashDisplay = "faq"
            apiUrl = makeAPIUrl("/api/svcFaq");
            selTab = 1;

            $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
            $(".conTitleGrp div:eq(" + selTab + ")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

            setHeaderUse();

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if (tempState == "TABVAL" || tempState == "SEARCH") {
                    myPostData.page = 1;
                }

                if (tempState != "SEARCH") {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString; myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "") {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";
            myPostData.display = hashDisplay;
            $('#jqgridFaqData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData, page: myPostData.page});
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            //session
            if(sessionStorage.getItem("last-url") != null){
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            //Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /*뒤로가기*/
            var hashlocation = "page=" + $(this).context.p.page + "&display="+$("#display").val();
            if ($(this).context.p.postData._search && $("#target").val() != null) {
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            // Master 관리자, CMS 관리자가 아닌 경우 checkbox selectAll 비활성화
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02') {
                $("#cb_jqgridFaqData").attr("disabled", true);
            }

            $(this).find("td").each(function(){
                if(typeof $(this).parents("tr").attr("tabindex") != "undefined"){
                    if ($(this).index() == 0) {
                        // 권한이 맞지 않는 글일 경우 삭제할 수 없도록 checkbox 비활성화
                        var cretrMbrSe = $(this).parent("tr").find("td:eq(10)").text();
                        var cretrMbrSvcSeq = $(this).parent("tr").find("td:eq(11)").text();
                        var cretrMbrStorSeq = $(this).parent("tr").find("td:eq(12)").text();
                        if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02' && (cretrMbrSe < Number($("#memberSec").val()))) {
                            $(this).parent("tr").find("td > input.cbox").attr("disabled", true);
                        } else {
                            if ($("#memberSec").val() == '04' && cretrMbrSvcSeq != $("#memberSvcSeq").val()) {
                                $(this).parent("tr").find("td > input.cbox").attr("disabled", true);
                            } else if ($("#memberSec").val() == '05' && cretrMbrStorSeq != $("#memberStorSeq").val()) {
                                $(this).parent("tr").find("td > input.cbox").attr("disabled", true);
                            } else {
                                $(this).parent("tr").find("td > input.cbox").attr("disabled", false);
                            }
                        }
                    } else if ($(this).index() == 2) {
                        if ($(this).parent("tr").find("td:eq(13)").text() == 'Y') { // 비활성 여부 확인
                            if ($(this).children("span.pointer").width() > ($(this).width() - 40)) {
                                // 비활성 상태 : 컬럼 크기에서 hidden 아이콘 값을 제외한 width 보다 내용이 클 경우
                                $(this).html("<span class='pointer' style='width: 90%; text-overflow: ellipsis; overflow: hidden; float: left;'>"
                                        + $(this).text() + "</span>" + "<img src='" + makeAPIUrl("/resources/image/icon_hidden.png") + "' style='width: 40px; height: 15px; vertical-align: top;'>");
                            } else {
                                $(this).html("<span class='pointer hiddenIcon'>" + $(this).text() + "</span>");
                            }
                        }
                    }
                }
            });

            //pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridFaqData").getGridParam("lastpage")) this.value = $("#jqgridFaqData").getGridParam("lastpage");
            });

            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridFaqData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridFaqData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event) {
            var list = $("#jqgridFaqData").jqGrid('getRowData', rowId);
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02'
                && ((list.cretrMbrSe < Number($("#memberSec").val()))
                    || ($("#memberSec").val() == '05' && list.cretrMbrStorSeq != $("#memberStorSeq").val()))) {
                $('#jqgridFaqData').jqGrid('setSelection', rowId, false);
            }
            // 콘텐츠 ID 선택시
            if(columnId == 2){
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/servicecustomer/faq/" + list.svcFaqSeq);
            }
        }
    });
    jQuery("#jqgridFaqData").jqGrid('navGrid','#faqPageDiv',{del:false,add:false,edit:false,search:false});
}

function keywordSearch() {
    gridState = "SEARCH";
    if ($("#display").val() == 'notice') {
        $("#jqgridSvcNoticeData").trigger("reloadGrid");
    } else {
        $("#jqgridFaqData").trigger("reloadGrid");
    }
}

searchFieldSet = function() {
    var colModel = "";
    var colNames = "";
    var searchHtml = "";

    if ($("#target > option").length == 0) {
        if ($("#display").val() == 'notice') {
            colModel = $("#jqgridSvcNoticeData").jqGrid('getGridParam', 'colModel');
            colNames = $("#jqgridSvcNoticeData").jqGrid('getGridParam', 'colNames');

            for (var i = 0; i < colModel.length; i++) {
                //검색제외추가
                switch(colModel[i].name) {
                case "svcNoticeTitle":break;
                case "cretrNm":break;
                default:continue;
                }
                searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
            }
            $("#target").html(searchHtml);
            if ($("#target option").index() == 0) {
                $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
                $(".target_bak").parent(".selectBox").css("display","table");
            } else {
                $(".target_bak").remove();
            }
        } else {
            colModel = $("#jqgridFaqData").jqGrid('getGridParam', 'colModel');
            colNames = $("#jqgridFaqData").jqGrid('getGridParam', 'colNames');

            for (var i = 0; i < colModel.length; i++) {
                //검색제외추가
                switch(colModel[i].name) {
                case "svcFaqTitle":break;
                case "cretrNm":break;
                case "svcFaqCtgNm":break;
                default:continue;
                }
                searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
            }
            $("#target").html(searchHtml);
            if ($("#target option").index() == 0) {
                $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
                $(".target_bak").parent(".selectBox").css("display","table");
            } else {
                $(".target_bak").remove();
            }
        }
    }
    $("#target").val(searchField);
}

function tabVal(seq, e) {
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn");
    $(e).parent("div").addClass("tabOn").css({ 'pointer-events': 'none' });

    selTab = seq;
    $("#target").html("");
    $("#keyword").val("");

    totalPage = 0;

    if (selTab == 0) { // 공지사항 탭
        $("#display").val("notice");
        $("#jqgridFaqData").jqGrid("GridUnload");
        $("#jqgridFaqData").hide();
        $("#jqgridSvcNoticeData").show();

        serviceNoticeList();
    } else { // FAQ 탭
        $("#display").val("faq");
        $("#jqgridSvcNoticeData").jqGrid("GridUnload");
        $("#jqgridSvcNoticeData").hide();
        $("#jqgridFaqData").show();

        serviceFaqList();
    }
}

function deletePost() {
    var postSeqList = "";
    var delCnt = 0;

    if (selTab == 0) { // 공지사항 탭
        for (var i = 1; i < $("#limit").val() + 1; i++) {
            if ($("#jqg_jqgridSvcNoticeData_" + i).prop("checked")) {
                var selSvcNoticeSeq = $("#jqgridSvcNoticeData").jqGrid('getRowData', i).svcNoticeSeq;
                postSeqList += selSvcNoticeSeq + ",";
                delCnt++;
            }
        }

        if (delCnt == 0) {
            popAlertLayer(getMessage("notice.delete.not.select.msg"));
            return;
        }

        popConfirmLayer(getMessage("notice.delete.list.confirm.msg"), function() {
            formData("NoneForm", "delSvcNoticeSeqList", postSeqList.slice(0, -1));
            callByDelete("/api/svcNotice", "deletePostSuccess", "NoneForm","deleteFail");
        }, null, getMessage("common.confirm"));
    } else { // FAQ 탭
        for (var i = 1; i < $("#limit").val() + 1; i++) {
            if ($("#jqg_jqgridFaqData_" + i).prop("checked")) {
                var selSvcFaqSeq = $("#jqgridFaqData").jqGrid('getRowData', i).svcFaqSeq;
                postSeqList += selSvcFaqSeq + ",";
                delCnt++;
            }
        }

        if (delCnt == 0) {
            popAlertLayer(getMessage("faq.delete.not.select.msg"));
            return;
        }

        popConfirmLayer(getMessage("faq.delete.list.confirm.msg"), function() {
            formData("NoneForm", "delSvcFaqSeqList", postSeqList.slice(0, -1));
            callByDelete("/api/svcFaq", "deletePostSuccess", "NoneForm","deleteFail");
        }, null, getMessage("common.confirm"));
    }
}

deleteFail = function() {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

deletePostSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayerFnc(getMessage("success.common.delete"),"hrefReload",$(location).attr('pathname')+"#display="+hashDisplay);
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

function createPost() {
    if (selTab == 0) { // 공지사항 탭
        pageMove("/servicecustomer/notice/regist");
    } else { // FAQ 탭
        pageMove("/servicecustomer/faq/regist");
    }
}

