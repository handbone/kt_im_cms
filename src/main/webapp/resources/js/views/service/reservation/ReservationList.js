/*
         * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var modelHidden = true;
var readyPage = false;
var hashType;
var hashSvc;
var hashStor;
var selTab =0;
var lastBind = false;

var year;
var month;
var day;
var calday = 0;

$(window).ready(function(){
    $(window).on('hashchange', function () {
        urlSet();
    });

})
$(document).ready(function(){
    limitInputTitle("cstmrNm");

    setkeyup();

    timeSet($("#startHour"),0,12,1,"hour");
    timeSet($("#startMinute"),0,60,5,"minute");
    timeSet($("#endHour"),0,12,1,"hour");
    timeSet($("#endMinute"),0,60,5,"minute");


    svcList();
    urlSet();
    dateSet();

    var cycleBoxHtml = "";
    var cycleSize = 7;
    var cycleCount = 10;

    for(var i=1; i<cycleSize; i++){
        var Minute = (cycleCount*i);
        var Hour = Math.floor(Minute/60);
        var timeString = (Hour == 0)? "" : Hour+" "+getMessage("reservation.table.time");

        if((Minute % 60) == 0){

        }else{
            if((Minute % 60) < 10) timeString += "0";
            timeString += (Minute % 60) + getMessage("reservation.table.minute");
        }

        cycleBoxHtml += "<option value=\""+Minute+"\">"+timeString+"</option>"
    }

    $("#cycleBox").html(cycleBoxHtml);

    $("#policy").html(getPolicyInfo());

    $(".only_num").keyup(function(e) {
        var value = $(this).val();
        $(this).val(value.replace(/[^0-9\-]/gi,''));
    });
});

function urlSet(){
    var hashlocation;
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashType = checkUndefined(findGetParameter(str_hash,"type"));
        $("#type").val(hashType);

        if (hashType == "seting") {
            selTab = 1;

            $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
            $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });

            switch(selTab){
            case 0:
                $("#type").val("list");
                $(".tab1").show();
                $(".tab2").hide();
                break;
            case 1:
                $("#type").val("seting");
                $(".tab1").hide();
                $(".tab2").show();
                break;
            }
        } else {
            $(".tab1").show();
            $(".tab2").hide();
        }
    }

    if ($(".tab"+(selTab+1)+" .storList").val() == null) {
        $("input:not(.btnCancel):not(.btnLogout)").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
    } else {
        $("input").unbind( "click" );
    }

    var hashlocation = "type="+$("#type").val();
    document.location.hash = hashlocation;

}

function tabVal(seq,e){
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    selTab = seq;

    $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
    $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });


    switch(selTab){
    case 0:
        $("#type").val("list");
        $(".tab1").show();
        $(".tab2").hide();
        $(".svcList > option[value=" + $(".tab2 .svcList").val() + "]").attr("selected","true");
        break;
    case 1:
        $("#type").val("seting");
        $(".tab1").hide();
        $(".tab2").show();
        $(".svcList > option[value=" + $(".tab1 .svcList").val() + "]").attr("selected","true");
        break;
    }

    storInfo();

    var hashlocation = "type="+$("#type").val();
    document.location.hash = hashlocation;
}

function dateSet(){
    /*금일날짜*/
    var re;
    var date = new Date();
    year = date.getFullYear();
    month = date.getMonth()+1;
    day = date.getDate();
    $(".year").val(year);
    $(".month").text(month);
    $(".day").text(day);
    $(".only_num").blur(function() {
    });

    $(".only_num").on('keyup',function(e){
        /*
        var value = $(this).val();
        $(this).val(value.replace(/[^0-9\-]/gi,''));

        var isValid = true;
        isValid = validatePhoneNumber($(".only_num").val());
        if (!isValid) {
        } else {
            $(".only_num").val(phoneNumberFormatter($(".only_num").val()));
        }
*/

//        re = /[ㄱ-ㅎㄲ-ㅆㅏ-ㅢㅣ가-힣a-zA-Z~!@\#$%^&*\()\=+_';.,":[]|]/gi;  //한글,영문,숫자
//        var temp=$(".only_num").val();
//        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
//            $(".only_num").val(temp.replace(re,"").trim());
//        }
//
//        if (!(event.keyCode >=37 && event.keyCode<=40)) {
//            var inputVal = $(this).val();
//            inputVal=inputVal.replace(/[^0-9]/gi,'');
//            if(inputVal.length>=11)
//            {
//                inputVal = inputVal.substr(0,11);
//                $(this).val(inputVal.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3'));
//            }else if(inputVal.length<=10 ){
//                if(inputVal.length==10)$(this).val(inputVal.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3'));
//                if(inputVal.length==10 && inputVal.substr(0,2)=='02')$(this).val(inputVal.replace(/(\d{2})(\d{4})(\d{4})/, '$1-$2-$3'));
//                if(inputVal.length==9)$(this).val(inputVal.replace(/(\d{2})(\d{3})(\d{4})/, '$1-$2-$3'));
//                if(inputVal.length==8)$(this).val(inputVal.replace(/(\d{4})(\d{4})/, '$1-$2'));
//                if(inputVal.length==5)$(this).val(inputVal.replace(/(\d{2})(\d{3})/, '$1-$2'));
//            }
//        }
    });

    $(".datepicker").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        defaultDate: 0,
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        maxDate: 0,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
    });
    $('.datepicker').datepicker().datepicker('setDate',0+'d');

    $('.next-day').on("click", function () {
        if(calday >= 0){
            popAlertLayer(getMessage("reservation.msg.moveNot.today"));
            return;
        }
        calday++;
        $('.datepicker').datepicker().datepicker('setDate',calday+'d');
        var date = $('.datepicker').datepicker("getDate");

        year = date.getFullYear();
        month = date.getMonth()+1;
        day = date.getDate();
        $(".year").val(year);
        $(".month").text(month);
        $(".day").text(day);

        storInfo();
    });

    $('.next-month').on("click", function () {
        var lastDay = ( new Date( year, month+1, 0) ).getDate();
        if(calday+lastDay >= 0){
            popAlertLayer(getMessage("reservation.msg.moveNot.today"));
            return;
        }
        calday+=lastDay;

        $('.datepicker').datepicker().datepicker('setDate',calday+'d');
        var date = $('.datepicker').datepicker("getDate");
        year = date.getFullYear();
        month = date.getMonth()+1;
        day = date.getDate();
        $(".year").val(year);
        $(".month").text(month);
        $(".day").text(day);
        storInfo();
    });

    $('.prev-day').on("click", function () {
        calday--;
        $('.datepicker').datepicker().datepicker('setDate',calday+'d');
        var date = $('.datepicker').datepicker("getDate");
        year = date.getFullYear();
        month = date.getMonth()+1;
        day = date.getDate();
        $(".year").val(year);
        $(".month").text(month);
        $(".day").text(day);

        storInfo();
    });
//
    $('.prev-month').on("click", function () {
        var lastDay = ( new Date( year, month-1, 0) ).getDate();
        calday-= lastDay;

        $('.datepicker').datepicker().datepicker('setDate',calday+'d');
        var date = $('.datepicker').datepicker("getDate");
        year = date.getFullYear();
        month = date.getMonth()+1;
        day = date.getDate();
        $(".year").val(year);
        $(".month").text(month);
        $(".day").text(day);

        storInfo();
    });


    $(".ui-datepicker-trigger").css("cursor","pointer");
    $(".datepicker").on('change', function (ev) {
        $(".datepicker").datepicker('show');
        year = $("#startDate").val().split("-")[0];
        month = $("#startDate").val().split("-")[1];
        day = $("#startDate").val().split("-")[2];


        var today = new Date();
        var dateString = year + "-"+month + "-"+day;
        var dateArray = dateString.split("-");
        var dateObj = new Date(dateArray[0], Number(dateArray[1])-1, dateArray[2]);
        var betweenDay = (today.getTime() - dateObj.getTime())/1000/60/60/24;
        calday = 0;
        calday -= parseInt(betweenDay);


        $(".year").val(year);
        $(".month").text(month);
        $(".day").text(day);
        cellRowCount =0;

        storInfo();
    });
}

svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}

svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
        });
        $(".svcList").html(svcListHtml);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $(".svcList").attr('disabled', 'true');
        }

        storList();
    }
}

storList = function(){
    if($(".tab"+(selTab+1)+" .svcList").val() == null){
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("#storList").html("");
        $(".tableReserve tbody").html("");
        $("input:not(.btnCancel):not(.btnLogout)").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
    }else {
        $("input").unbind( "click" );
        callByGet("/api/store?row=10000&svcSeq="+$(".tab"+(selTab+1)+" .svcList").val(),"storListSuccess","NoneForm");
    }
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        var cnt = 0;
        var memberStoreListHtml = "";
        $(data.result.storeList).each(function(i, item) {
            if (item.useYn == "Y") {
                cnt++;
                memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
            }
        });
        $(".storList").html(memberStoreListHtml);
        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 서비스 변경 불가
            $(".storList").attr('disabled', 'true');
        }
//
        if (cnt == 0) {
            $("#storList").html("");
            $(".tableReserve tbody").html("");
            $("input:not(.btnCancel):not(.btnLogout)").bind("click", function(e) {
                popAlertLayer(getMessage("common.bad.request.msg"));
            });
            popAlertLayer(getMessage("info.store.nodata.msg"));
        } else{
            $("input").unbind( "click" );
            storInfo();
        }


        $(".svcList").val($(".tab"+(selTab+1)+" .svcList").val());
    }  else {
        $(".storList").html("");
        $(".tableReserve tbody").html("");
        $("input:not(.btnCancel):not(.btnLogout)").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
        popAlertLayer(getMessage("info.store.nodata.msg"));
    }
}

storInfo = function(){
    if ($(".tab"+(selTab+1)+" .storList").val() == null) {
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("input:not(.btnCancel):not(.btnLogout)").bind("click", function() {
            popAlertLayer(getMessage("common.bad.request.msg"));
         });
    } else {
        $("input").unbind( "click" );
        callByGet("/api/store?storSeq="+$(".tab"+(selTab+1)+" .storList").val(),"storInfoSuccess","NoneForm");
    }
}
var now=new Date();
var nowHour = new Date(now.getTime()).getHours();       //현재 시간 시각 (24hour)
var nowMinute = new Date(now.getTime()).getMinutes();           //현재 시간 분

var arr = new Array();
var categorySeqs =new Array();
var categoryCount = [];
var cellRowCount = 0;
var classColor = "";
var useDevice;
storInfoSuccess = function(data){
    if (data.resultCode == "1000") {
        useDevice = data.result.useDevList;
        var item = data.result.reservationSetting;

        var startTime = (item.stTime).split(":");
        var endTime = (item.fnsTime).split(":");

        var cycleTime = item.rsrvTimeSetCycle;

        if (selTab == 1) {
            $("#cycleBox").val(cycleTime);
            (startTime[0] < 12)? $("#startTimeType").val("morning") : $("#startTimeType").val("afternoon");
            (endTime[0] < 12)? $("#endTimeType").val("morning") : $("#endTimeType").val("afternoon");
            timeReSet("start");
            timeReSet("end");

            $("#startHour").val(startTime[0]);
            $("#startMinute").val(startTime[1]);
            $("#endHour").val(endTime[0]);
            $("#endMinute").val(endTime[1]);
        } else {
            var startHour   = parseInt(startTime[0]) * 60;
            var startMinute = parseInt(startTime[1]);
            startTime = startHour + startMinute;

            var endHour     = (parseInt(endTime[0])) * 60;
            var endMinute   = parseInt(endTime[1]);
            endTime   =  endHour + endMinute;

            $(".tableReserve tbody").html("");
            cellRowCount = 0;
            while(1){
                var Cell = "";

                for(var j=0; j<2; j++){
                    Cell+= "<td class='txtCenter cellCell_"+j+"'>"
                    +"<input type='button' value='"+getMessage("reservation.table.reservation")+"' class='btnREV btnWhite btnLarge btnReserveWhite' onclick='openPopup("+cellRowCount+","+j+","+"0"+",this)' hidden>"
                    +"</td>";
                }

                var sHour = Math.floor(startTime/60);
                var sMinute = (startTime%60);

                startTime += cycleTime;

                var eHour = Math.floor(startTime/60);
                var eMinute = (startTime%60);

                var overTag = "";

                if(sHour < nowHour || calday < 0){
                    overTag = " overTime";
                }else if(sHour == nowHour){
                    if(sMinute<nowMinute){
                        overTag = " overTime";
                    }
                }
                if(sHour < 10){sHour = "0"+sHour}
                if(sMinute < 10){sMinute = "0"+sMinute}
                if(eHour < 10){eHour = "0"+eHour}
                if(eMinute < 10){eMinute = "0"+eMinute}

                var timeHed = "<td class='timeHed txtCenter'>"+" "+sHour+" : " + sMinute +" ~ " +eHour+" : " + eMinute+"</td>";

                $(".tableReserve tbody").append("<tr class='cellRow_"+cellRowCount+overTag+"'>"+timeHed+Cell+"</tr>");

                if(startTime+cycleTime > endTime) break;
                cellRowCount++;
            }
            $(".overTime > td").css("color","#ccc");
            reservationInfo();
        }
        $(".storList").val($(".tab"+(selTab+1)+" .storList").val());
    }
}

function timeSet(obj,s,e,t,type){
    var option ="";

    for(var i=s; i<e; i+=t){
        var timeValue = i;
        var timeStr = i;
        if(i>12 && type == "hour"){timeStr -= 12;}
        if(timeStr < 10){timeStr = "0"+timeStr; }
        if(i <10){timeValue = "0"+i}
        option += "<option value="+timeValue+">"+ timeStr + "</option>";
    }

    $(obj).html(option);
}

function timeReSet(type){
    if(type == "start"){
        ($("#startTimeType").val() == "morning")? timeSet($("#startHour"),0,12,1,"hour") : timeSet($("#startHour"),12,24,1,"hour");
    }else{
        ($("#endTimeType").val() == "morning")? timeSet($("#endHour"),0,12,1,"hour") : timeSet($("#endHour"),12,24,1,"hour");
    }
}

settingSave = function(){
    var startTime = $("#startHour").val() +":" + $("#startMinute").val();
    var endTime =   $("#endHour").val() +":" + $("#endMinute").val();
    var cycleMinute = $("#cycleBox option:selected").val();
    var startTotal = parseInt($("#startHour").val()) * 60 + parseInt($("#startMinute").val());
    var endTotal = parseInt($("#endHour").val()) * 60 + parseInt($("#endMinute").val());

    if($("#startHour").val() == "" || $("#startMinute").val() == ""){
        popAlertLayer(getMessage("reservation.msg.insert.stTime"));
    }else if($("#endHour").val() == "" || $("#endMinute").val() == ""){
        popAlertLayer(getMessage("reservation.msg.insert.fnsTime"));
    }else if(cycleMinute == ""){
        popAlertLayer(getMessage("reservation.msg.insert.timeCycle"));
    }else if(startTotal >= endTotal){
        popAlertLayer(getMessage("reservation.msg.incorrectTime"));
        return;
    }

    if($("#startHour").val() == "" || $("#startMinute").val() == "" || $("#endHour").val() == "" || $("#endMinute").val() == "" || cycleMinute == ""){ return;}

    popConfirmLayer(getMessage("common.update.msg"), function(){
        formData("NoneForm" , "type", "setting");
        formData("NoneForm" , "storSeq", $(".tab"+(selTab+1)+" .storList").val());
        formData("NoneForm" , "stTime", startTime);
        formData("NoneForm" , "fnsTime",endTime );
        formData("NoneForm" , "rsrvTimeSetCycle", cycleMinute);
        callByPut("/api/reservation" , "settingSaveSuccess", "NoneForm");
    });
}

settingSaveSuccess = function(data){
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"));
        $('.tabOff a').trigger('click');
        //storInfo();
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
    formDataDeleteAll("NoneForm");
}


reservationInfo = function(){
    var searchField = "?storSeq="+ $(".storList option:selected").val();
    searchField+= "&usageDate="+year+"-"+month+"-"+day;

    callByGet("/api/reservation"+searchField, "reservationInfoSuccess", "NoneForm");
}


reservationInfoSuccess =function(data){
    if(data.resultCode == "1000"){
        $(data.result.resveList).each(function(i,item) {
            for(var j=0; j<=cellRowCount; j++){
                    var infoHedTime = $("tbody > tr.cellRow_"+j).find(".timeHed").text().split("~");
                    var sdateInfo = (parseInt(infoHedTime[0].substr(1,2)) * 60) + parseInt(infoHedTime[0].substr(6,8));
                    var dateInfo = (parseInt(item.resveDt.split(":")[0]) * 60) + parseInt(item.resveDt.split(":")[1]);
                    var edateInfo = (parseInt(infoHedTime[1].substr(1,2)) * 60) + parseInt(infoHedTime[1].substr(6,8));

                    if(dateInfo >= sdateInfo){
                        if(dateInfo < edateInfo){
                            $("tbody > tr.cellRow_"+j +"> td.cellCell_"+ctgNmSeq(item.devCtgNm)).find("input.btnREV").remove();
                            $("tbody > tr.cellRow_"+j +"> td.cellCell_"+ctgNmSeq(item.devCtgNm)).append(
                                    "<div class='"+item.resveSeq+" pointer PMR PMROneHour "+colorSet(item.devCtgNm)+"' onclick='openPopup("+j+","+ctgNmSeq(item.devCtgNm)+","+item.resveSeq+",this)' style='display:none'>" +
                                    "<span class='cstmrNm'>"+item.cstmrNm +"</span><span> "+"<span class='cstmrTelNo'>"+item.cstmrTelNo+"</span>"+" ( "+ item.resveDt+" ) "+"</span>"+"</div>");
                            break;
                        }
                    }
            }
        });

    }else if(data.resultCode == "1010"){

    }

    var ctgSetAry = new Array();
    $(useDevice).each(function(i,item){
        var devTNm= item.devTypeNm;
        if (devTNm.match(getMessage("reservation.category.basketball")) || devTNm.match(getMessage("reservation.category.soccer"))) {
            if (devTNm.match(getMessage("reservation.category.basketball"))) {
                devTNm = "Basketball";
                ctgSetAry.push("Basketball");
            }

            if (devTNm.match(getMessage("reservation.category.soccer"))) {
                devTNm = "Soccer";
                ctgSetAry.push("Soccer");
            }

            $(".img"+devTNm).parents("table").find(".btnREV").show();
            $(".img"+devTNm).parents("table").find(".PMR").show();
        }
        if (ctgSetAry.indexOf("Basketball") == -1) {
            $(".cellCell_0").find(".btnREV").hide();
            $(".cellCell_0").find(".PMR").hide();
        }

        if (ctgSetAry.indexOf("Soccer") == -1) {
            $(".cellCell_1").find(".btnREV").hide();
            $(".cellCell_1").find(".PMR").hide();
        }

    })
}

function colorSet(devCtgNm){
    if (devCtgNm.match(getMessage("reservation.category.soccer"))) {
        classColor = "reserveFinal PMRGreen";
    } else if (devCtgNm.match(getMessage("reservation.category.basketball"))) {
        classColor = "reserveCancel PMRBlue";
    }
    /*
    switch(devCtgNm){
    case "FPS":
        classColor = "reserveFinal PMRGreen";
        break;
    case "HADO":
        classColor = "reserveCancel PMRBlue";
        break;
    }
    */

    return classColor;
}


function ctgNmSeq(devCtgNm){
    var seq =0;
    if (devCtgNm.match(getMessage("reservation.category.soccer"))) {
        seq = 1;
    } else if (devCtgNm.match(getMessage("reservation.category.basketball"))) {
        seq = 0;
    }
    /*
    switch(devCtgNm){
    case "HADO":       seq = 0;break;
    case "FPS":        seq = 1;break;
    }
    */
    return seq;
}


function ctgSeqNm(Seq){
    var seq =0;
    switch(Seq){
    case 0:         seq = getMessage("reservation.category.basketball");break;
    case 1:         seq = getMessage("reservation.category.soccer");break;
    }
    /*
    switch(Seq){
    case 0:         seq = "HADO";break;
    case 1:         seq = "FPS";break;
    }
    */
    return seq;
}


closePopup = function(){
    $.unblockUI();
}
var overView;
openPopup = function(row,category,resveSeq,e){
    var timeText = $("table tr.cellRow_"+row).find("td").first().text();
    $(".timeText").text(timeText);
    timeText = timeText.split("~")[0];
    var hour   = timeText.substr(1,2);
    var minute = timeText.substr(6,2);

    overView = false;

    now=new Date();
    nowHour = new Date(now.getTime()).getHours();       //현재 시간 시각 (24hour)
    nowMinute = new Date(now.getTime()).getMinutes();           //현재 시간 분
    $(".popupTitle").html(getMessage("table.reservation"));
    $(".txtPrivacy").parents("li").show();
    $("#agree").parents("li").show();
    $(".txtReserveQuest").show();
    $(".nm").css({"display":"initial","width":"auto"}).hide();
    $(".popupContent > ul li:eq(0) .nm").show();
    $(".btnGrp .btn").first().val(getMessage("common.confirm")).addClass("btnOk").removeClass("btnReserve");
    $(".btnGrp .btn").first().next().val(getMessage("button.reset"));

    timeText = hour+":"+minute;

    if($(e).parents("tr").hasClass("overTime")){
        overView = true;
    }else{
        if(hour < nowHour){
            $(e).parents("tr").addClass("overTime");
        }else if(hour == nowHour){
            if(parseInt(minute)<nowMinute){
                $(e).parents("tr").addClass("overTime");
                $(".overTime > td").css("color","#ccc");
                overView = true;
            }
        }
    }


    $(".devCtgNm").text(ctgSeqNm(category));
    $("#devCtgNm").val(ctgSeqNm(category));
    $("#usageDate").val(timeText);
    $("#resveSeq").val(resveSeq);
    $("#reserveView").hide();
    if(resveSeq == 0){
        reservationCheck();
    }else{
        callByGet("/api/reservation?resveSeq="+resveSeq, "reserveCustomInfoSuccess", "NoneForm");
    }

}

reservationCheck = function(){
    if(overView){
        popAlertLayer(getMessage("reservation.msg.notInsert.time"));
        return;
    }

    $('form').each(function(){
        this.reset();
        $(".btnGrp .btn").first().val(getMessage("common.confirm"));
        $(".stateMsg").text(" "+getMessage("reservation.confirm.insert"));
        if($("#reservation input[type=text]").css("display") == "none") $("#reservation input[type=text]").show().next("span").text("");
    });
    $(".popupContent ul li").removeClass("lineBottom");
    $(".popupContent ul li .nm").removeClass("textBold");
    $(".popupContent ul li:eq(0)").addClass("textBold");


    $.blockUI({
        message: $('#popupWinidsm'),
        css:{border:'0px',cursor:'default',top:'27%',width:'auto',height:'480px'}
    });
}


reserveCustomInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.resveInfo;
        $(".popupTitle").html(getMessage("reservation.table.detail"));
        $(".btnGrp .btn").first().val(getMessage("reservation.table.cancle")).addClass("btnReserve").removeClass("btnOk");
        $(".btnGrp .btn").first().next().val(getMessage("button.close"));
        $("#devCtgNm").val(item.devCtgNm);
        $("#cstmrNm").val(item.cstmrNm);
        $("#cstmrTelNo").val(item.cstmrTelNo);
        $(".txtPrivacy").parents("li").hide();
        $("#agree").parents("li").hide();
        $(".nm").css({"display":"inline-block","width":"160px"}).show();


        $("#reservation input[type=text]").each(function(i){
            var str = $(this).val();
            $(this).hide().next("span").text(str);
        });
        $("#reserveView").show();
        $(".txtReserveQuest").hide();


        $(".popupContent ul li").addClass("lineBottom");
        $(".popupContent ul li .nm").addClass("textBold");
        $(".popupContent ul li:eq(0)").removeClass("textBold");

        $.blockUI({
            message: $('#popupWinidsm'),
            onOverlayClick: $.unblockUI,
            css:{border:'0px',cursor:'default',top:'27%',width:'auto',height:'320px'}
        });

    }
}


insertReservate = function(e)
{
    if ($(".btnGrp .btn").first().val() == getMessage("common.confirm")) {
        if (!phoneChk()){
            popAlertLayer(getMessage("login.alert.phoneNo.msg"));
            return;
        }
        if ($("#cstmrNm").val().trim() == "") {
            popAlertLayer(getMessage("reservation.msg.insert.customNm"));
            return;
        } else if ($("#cstmrTelNo").val().trim() == "") {
            popAlertLayer(getMessage("reservation.msg.insert.telNo"));
            return;
        } else if ($("#agree").attr("checked") != "checked") {
            popAlertLayer(getMessage("reservation.msg.checked.agree"));
            return;
        }





        $("#storSeq").val($(".tab1 .storList").val());

        callByPost("/api/reservation", "reservationInsertSuccess", "reservation","InsertFail");
    } else {
        popConfirmLayer($("#cstmrNm").next("span").text()+" "+getMessage("reservation.confirm.cancle"), function(){
            $("#usageDate").val(year+"-"+month+"-"+day+" "+$("#usageDate").val());
            callByDelete("/api/reservation", "reservationDeleteSuccess", "reservation","deleteFail");
        },null,getMessage("common.confirm"));
    }

}


InsertFail = function(data){
    popAlertLayer(getMessage("fail.common.insert"));
    formDataDeleteAll("NoneForm");
}

deleteFail = function(data){
    popAlertLayer(getMessage("fail.common.delete"));
    formDataDeleteAll("NoneForm");
}

reservationInsertSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.resveInfo;
        popAlertLayer(getMessage("reservation.msg.success"));
        $("#agree").attr("checked",false);
        closePopup();
        var parentTr = $("td").filter(function() {return $.text([this]) == $(".timeText").eq(0).text(); }).parents("tr");
        var phoneText = "";

        colorSet($(".devCtgNm").text());

        $(parentTr).find(".cellCell_"+ctgNmSeq($("#devCtgNm").val())).find("input.btnREV").remove();

        $(parentTr).find(".cellCell_"+ctgNmSeq($("#devCtgNm").val())).append("<div class='"+item.resveSeq+" pointer PMR PMROneHour "+colorSet($("#devCtgNm").val())+"' onclick='openPopup("+$(parentTr).index()+","+ctgNmSeq($("#devCtgNm").val())+","+item.resveSeq+",this)'>" +
        "<span class='cstmrNm'>"+$("#cstmrNm").val() +"</span> <span> "+"<span class='cstmrTelNo'>"+$("#cstmrTelNo").val()+"</span>"+" ( "+ $(".timeText").eq(0).text().split("~")[0]+" ) "+" </span>"+"</div>");

        TableRowSet(1); TableRowSet(2);

        //$(parentTr).next("tr").find(".cellCell_"+categoryNameSeq($("#categoryName").val())).children(".client_"+$("#clientSeq").val()).removeClass("boxGray").addClass(classColor);
    }
}

reservationDeleteSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("reservation.msg.success.cancle"));
        closePopup();


        var parentTr = $("td").filter(function() {return $.text([this]) == $(".timeText").eq(0).text(); }).parents("tr");

        $("div."+$("#resveSeq").val()).remove();


        if($(parentTr).find(".cellCell_"+ctgNmSeq($("#devCtgNm").val())).children("div").length == 0){
            $(parentTr).find(".cellCell_"+ctgNmSeq($("#devCtgNm").val())).html("");


            $(parentTr).find(".cellCell_"+ctgNmSeq($("#devCtgNm").val())).html("<input type='button' value="+getMessage("reservation.table.reservation")+" class='btnREV btnWhite btnLarge btnReserveWhite' onclick='openPopup("+$(parentTr).index()+","+ctgNmSeq($("#devCtgNm").val())+",0,this)'>");
            $("input[name=_method]").remove();
        }

        TableRowSet(1); TableRowSet(2);
    }
}


function TableRowSet(categorySeq) {
//    var rows = $(".tableReserve tbody").children('tr');
//
//    for (var i = 0; i < rows.length; i++ ) {
//        var row = rows[i];
//        var nextrow = rows[i+1];
//        var removeDiv = jQuery(row).find("td");
//
//        var tdName = jQuery(row).find("td").eq(categorySeq).find(".cstmrNm").text();
//        var b_tdName = jQuery(nextrow).find("td").eq(categorySeq).find(".cstmrNm").text();
//        var tdPhone = jQuery(row).find("td").eq(categorySeq).find(".cstmrTelNo").text();
//        var b_tdPhone = jQuery(nextrow).find("td").eq(categorySeq).find(".cstmrTelNo").text();
//
//
//
//        if(tdName == b_tdName){
//            if(b_tdPhone == tdPhone){
//                if(jQuery(row).find("td").eq(categorySeq).find("div").hasClass("PMRBottom")){
//                    jQuery(row).find("td").eq(categorySeq).find("div").removeClass("PMRTop").removeClass("PMRBottom").addClass("PMRMiddle").find("span").hide();
//                    jQuery(nextrow).find("td").eq(categorySeq).find("div").removeClass("PMROneHour").addClass("PMRBottom").find("span").hide();
//                }else{
//                    jQuery(row).find("td").eq(categorySeq).find("div").removeClass("PMROneHour").addClass("PMRTop");
//                    jQuery(nextrow).find("td").eq(categorySeq).find("div").removeClass("PMROneHour").addClass("PMRBottom").find("span").hide();
//                }
//            }
//        }
//
//    }
}


function TableRowReSet() {
//    $("div.PMRBottom").addClass("PMROneHour");
//    $("div.PMRMiddle").addClass("PMROneHour");
//    $("div.PMRTop").addClass("PMROneHour");
//
//    $("div").removeClass("PMRBottom");
//    $("div").removeClass("PMRTop");
//    $("div").removeClass("PMRMiddle");
//
//    $("div.PMROneHour > span").show();

}

phoneChk = function(){
    var isValid = validatePhoneNumber($(".only_num").val());
    if (isValid) {
        $(".only_num").val(phoneNumberFormatter($(".only_num").val()));
    }

    return isValid;
}