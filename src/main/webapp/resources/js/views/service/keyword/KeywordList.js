/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    setkeyup();
    clearInputBoxSet();
    serviceList();
});

serviceList = function() {
    callByGet("/api/service?searchType=list", "serviceListSuccess", "NoneForm");
}

serviceListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var serviceListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
        });
        $("#svcSelbox").html(serviceListHtml);

        if (document.location.hash != "") {
            var str_hash = document.location.hash.replace("#","");
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            $("#svcSelbox").val(hashSvc);
        }

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").attr('disabled', 'true');
        }


        keywordList();
    }
}

keywordList = function() {
    callByGet("/api/keyword?type=RC&svcSeq="+$("#svcSelbox").val(), "keywordListSuccess", "NoneForm");
}

registKeywordList = function() {
    var keyList = "";
    var rankList = "";
    var typeList = "";
    var rank = 1;
    for (var i =0; i< $(".clearInput").length; i++) {
        if ($(".clearInput").eq(i).val().trim() != "") {
            if (keyList != "") {
                keyList += ",";
            }
            if (rankList != "") {
                rankList += ",";
            }
            if (typeList != "") {
                typeList += ",";
            }
            keyList += $(".clearInput").eq(i).val().trim();
            rankList += rank;
            typeList += "RC";
            rank++;
        }
    }


    formData("NoneForm" , "svcSeq", $("#svcSelbox").val());
    formData("NoneForm", "keywords", keyList);
    formData("NoneForm", "rankLists", rankList);
    formData("NoneForm", "typeLists", typeList);
    callByPut("/api/keyword" , "keywordUpdateSuccess", "NoneForm","UpdateFail");
}

keywordUpdateSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    keywordList();
}

UpdateFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}



keywordListSuccess = function(data) {
    $(".clearInput").val("").attr("value","");
    if (data.resultCode == "1000") {
        $(data.result.keywordList).each(function(i,item) {
            $(".clearInput").eq(i).val(item.keyword).attr("value",item.keyword);
        });
    }

    $('.clearInput').each(function(){
        $(this).parents("tr").find(".clearBtn").toggle(Boolean($(this).val()));
    });
}

clearInputBoxSet = function() {
    $('.clearInput').each(function(){
        $(this).parents("tr").find(".clearBtn").toggle(Boolean($(this).val()));
    });

    $('.clearInput').keyup(function(){
        $(this).parents("tr").find(".clearBtn").toggle(Boolean($(this).val()));
    });


    $(".clearBtn").click(function(){
        $(this).parents("tr").find(".clearInput").val('').focus();
        $(this).hide();
    });

    $(".clearInput").blur(function() {
        $(".clearInput").each(function(i){
            if ($(".clearInput").eq(i).val().trim() == "") {
                $(".clearInput").eq(i).attr("value","");
                $(".clearInput").eq(i).next(".clearBtn").toggle(Boolean($(this).val()));
            }
        });
    });

}

rankUp = function(e) {
    var nullIndex = $("input.clearInput:[value='']").eq(0).index(".clearInput"); //1,2,3,4..
    var currentIndex = $(e).parents("tr").find(".clearInput").index(".clearInput");
    var tempStr = "";
    var tempStr2 = "";

    if ($(e).parents("tr").find(".clearInput").val().trim() != "" && nullIndex >= 0) {
        if (currentIndex <= nullIndex) {
            tempStr = $(".clearInput").eq(currentIndex).val().trim();
            tempStr2 = $(".clearInput").eq(currentIndex-1).val().trim();
            $(".clearInput").eq(currentIndex).val(tempStr2);
            $(".clearInput").eq(currentIndex -1).val(tempStr);
        } else {
            for (var j = 0; j<7; j++) {
                for (var i = 0; i<7; i++) {
                    if ($(".clearInput").eq(i).val().trim() == "") {
                        $(".clearInput").eq(i).val($(".clearInput").eq(i+1).val());
                        $(".clearInput").eq(i+1).val("");
                    }
                }
            }
        }

    } else if (nullIndex < 0) {
        tempStr = $(".clearInput").eq(currentIndex).val();
        tempStr2 = $(".clearInput").eq(currentIndex-1).val();
        $(".clearInput").eq(currentIndex).val(tempStr2);
        $(".clearInput").eq(currentIndex -1).val(tempStr);
    }
    $(".clearInput").each(function(i){
        $(".clearInput").eq(i).text($(".clearInput").eq(i).val());
    });

}

rankDown = function(e) {
    var nullIndex = $("input.clearInput:[value='']").eq(0).index(".clearInput"); //1,2,3,4..
    var currentIndex = $(e).parents("tr").find(".clearInput").index(".clearInput");
    var tempStr = "";
    var tempStr2 = "";

    if ($(e).parents("tr").find(".clearInput").val().trim() != "" && nullIndex <= 7) {
        if (currentIndex <= nullIndex) {
            tempStr = $(".clearInput").eq(currentIndex).val().trim();
            tempStr2 = $(".clearInput").eq(currentIndex+1).val().trim();
            if (tempStr2 == "") {
                for (var j = 7; j<0; j--) {
                    for (var i = 7; i<0; i--) {
                        tempStr2 = $(".clearInput").eq(i).val();
                        $(".clearInput").eq(i).val($(".clearInput").eq(i-1).val());
                        $(".clearInput").eq(i-1).val(tempStr2);
                    }
                }
                return;
            }
            $(".clearInput").eq(currentIndex).val(tempStr2);
            $(".clearInput").eq(currentIndex +1).val(tempStr);
        }  else if (nullIndex < 0) {
            tempStr = $(".clearInput").eq(currentIndex).val();
            tempStr2 = $(".clearInput").eq(currentIndex+1).val();
            $(".clearInput").eq(currentIndex).val(tempStr2);
            $(".clearInput").eq(currentIndex +1).val(tempStr);
        } else {
            for (var j = 0; j<7; j++) {
                for (var i = 0; i<7; i++) {
                    if ($(".clearInput").eq(i).val().trim() == "") {
                        $(".clearInput").eq(i).val($(".clearInput").eq(i+1).val());
                        $(".clearInput").eq(i+1).val("");
                    }
                }
            }
        }
    }

    $(".clearInput").each(function(i){
        $(".clearInput").eq(i).text($(".clearInput").eq(i).val());
    });
}