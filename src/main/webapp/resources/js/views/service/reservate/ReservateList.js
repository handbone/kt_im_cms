/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var readyPage = false;
var gridState;
var tempState;
var searchField;
var searchString;
var hashSvc;
var hashStor;
var hashStartDt;
var hashEndDt;
var apiUrl;
var totalPage = 0;

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashStor = checkUndefined(findGetParameter(str_hash,"storSeq"));
            hashStartDt = checkUndefined(findGetParameter(str_hash,"startDt"));
            hashEndDt = checkUndefined(findGetParameter(str_hash,"endDt"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#startDt").val(hashStartDt);
            $("#endDt").val(hashEndDt);
            serviceList();
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        hashStartDt = checkUndefined(findGetParameter(str_hash,"startDt"));
        hashEndDt = checkUndefined(findGetParameter(str_hash,"endDt"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);
        $("#startDt").val(hashStartDt);
        $("#endDt").val(hashEndDt);

        gridState = "NONE";
    } else {
        var now=new Date();
        $("#startDt").val(dateString((new Date(now.getTime()))) + ' 00:00');
        $("#endDt").val(getEndTimeStamp());
    }

    $("#startDt").datetimepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        timeFormat:'HH:mm',
        controlType:'select',
        oneLine:true,
        stepMillisec:1000,
        stepMicrosec:1000,
        onSelect: function(selectedDate) {
            $('#endDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#endDt").datetimepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        timeFormat:'HH:mm',
        controlType:'select',
        oneLine:true,
        stepMillisec:1000,
        stepMicrosec:1000,
        onSelect: function(selected) {
            $("#startDt").datepicker("option","maxDate", selected)
        }
    });

    serviceList();

    $("#keyword").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

/* 현재 월의 첫째일로 설정 시 사용 코드
function getStartTimeStamp() {
    var d = new Date();
    var startStr = leadingZeros(d.getFullYear(), 4) + '-'
                + leadingZeros(d.getMonth() + 1, 2) + '-' + '01' + ' 00:00';

    return startStr;
}
*/

function getEndTimeStamp() {
    var d = new Date();

    var endStr = leadingZeros(d.getFullYear(), 4) + '-' + leadingZeros(d.getMonth() + 1, 2) + '-'
                + leadingZeros(d.getDate(), 2) + ' ' + leadingZeros(d.getHours(), 2) + ':'
                + leadingZeros(d.getMinutes(), 2);

    return endStr;
}

function leadingZeros(n, digits) {
    var zero = '';
    n = n.toString();

    if (n.length < digits) {
      for (i = 0; i < digits - n.length; i++)
        zero += '0';
    }
    return zero + n;
}

var noServiceData = false;
serviceList = function() {
    callByGet("/api/service?searchType=list", "serviceListSuccess", "NoneForm");
}

serviceListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var serviceListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
        });
        $("#svcSelbox").html(serviceListHtml);

        if (document.location.hash != "") {
            var str_hash = document.location.hash.replace("#","");
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            $("#svcSelbox").val(hashSvc);
        }

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").attr('disabled', 'true');
        }
        storeList($("#svcSelbox").val(), false);
    } else {
        noServiceData = true;
        storeList(0, false);
    }
}

var noStorData = false;
storeList = function(searchSvc, isReload) {
    if (searchSvc != null && searchSvc != 0) {
        searchSvc = "&svcSeq=" + searchSvc;
    } else {
        searchSvc = "&svcSeq=-1";
    }

    if (isReload) {
        callByGet("/api/store?searchType=list" + searchSvc, "reloadStoreListSuccess", "NoneForm");
    } else {
        callByGet("/api/store?searchType=list" + searchSvc, "storeListSuccess", "NoneForm");
    }
}

storeReload = function() {
    noStorData = false;
    storeList($("#svcSelbox").val(), true);
}

storeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i,item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storSelbox").html(storeListHtml);
        $("#storSelbox").attr("disabled", false);

        if (document.location.hash != "") {
            var str_hash = document.location.hash.replace("#","");
            hashStor = checkUndefined(findGetParameter(str_hash,"storSeq"));
            $("#storSelbox").val(hashStor);
        }

        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 매장 변경 불가
            $("#storSelbox").attr('disabled', 'true');
        }
    } else {
        $("#storSelbox").html("");
        $("#storSelbox").attr("disabled", true);
        noStorData = true;
    }

    var searchStore = $("#storSelbox").val();
    if (searchStore != null && searchStore != 0) {
        apiUrl = makeAPIUrl("/api/reservate?storSeq=" + searchStore);
    } else {
        apiUrl = makeAPIUrl("/api/reservate?storSeq=-1");
    }

    if (!readyPage) {
        readyPage = true;
        reservateList();
    } else {
        jQuery("#jqgridData").setGridParam({"url": apiUrl});
        jQuery("#jqgridData").trigger("reloadGrid");
    }
}

reloadStoreListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i,item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storSelbox").html(storeListHtml);
        $("#storSelbox").attr("disabled", false);
    } else {
        $("#storSelbox").html("");
        $("#storSelbox").attr("disabled", true);
        noStorData = true;
    }

    jqGridReload();
}

jqGridReload = function() {
    var searchStore = $("#storSelbox").val();
    if (searchStore != null && searchStore != 0) {
        searchStore = "?storSeq=" + searchStore;
    } else {
        searchStore = "?storSeq=-1";
    }

    jQuery("#jqgridData").setGridParam({"url": makeAPIUrl("/api/reservate" + searchStore), page: 1});
    jQuery("#jqgridData").trigger("reloadGrid");
}

reservateList = function(){
    var colNameList = [
        getMessage("table.num"),
        getMessage("reservate.table.tagId"),
        getMessage("reservate.table.useYn"),
        getMessage("reservate.table.usageMinute"),
        getMessage("reservate.table.rmndCnt"),
        getMessage("reservate.table.type"),
        getMessage("reservate.table.prodLmtCnt"),
        getMessage("reservate.table.prodNm"),
        getMessage("reservate.table.regDate"),
        "tagSeq"
    ];
    $("#jqgridData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.reservateList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames:colNameList,
        colModel:[
            {name:"num", index: "num", align:"center", width:40},
            {name:"tagId", index: "TAG_ID", align:"center", width:505, formatter:pointercursor},
            {name:"useYnNm", index: "USE_YN_NM",align:"center"},
            {name:"usageMinute", index: "RMND_TIME",align:"center", cellattr: editUsageMinuteTitleAttr},
            {name:"rmndCnt", index: "RMND_CNT",hidden:true},
            {name:"settlSeq", index: "SETTL_SEQ",align:"center", cellattr: editSettlTypeTitleAttr},
            {name:"prodLmtCnt", index: "PROD_LMT_CNT",hidden:true},
            {name:"prodNm", index: "PROD_NM",hidden:true},
            {name:"regDt", index:"REG_DT", align:"center"},
            {name:"tagSeq", index:"TAG_SEQ", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "TAG_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: false,
        beforeRequest:function() {
            tempState = gridState;
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());
            hashSvc = checkUndefined($("#svcSelbox").val());
            hashStor = checkUndefined($("#storSelbox").val());
            hashStartDt = checkUndefined($("#startDt").val());
            hashEndDt = checkUndefined($("#endDt").val());

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if (tempState == "SEARCH") {
                    myPostData.page = 1;
                } else {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchStartDt = checkUndefined(findGetParameter(str_hash,"startDt"));
                searchEndDt = checkUndefined(findGetParameter(str_hash,"endDt"));
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                myPostData.page = page;

                if (searchField != null || searchStartDt != null || searchEndDt != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "") {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            if (hashSvc != null && hashSvc != "") {
                myPostData.svcSeq = hashSvc;
            } else {
                myPostData.svcSeq = "";
            }

            if (hashStor != null && hashStor != "") {
                myPostData.storSeq = hashStor;
            } else {
                myPostData.storSeq = "";
            }

            if (hashStartDt != null && hashStartDt != "") {
                myPostData.startDt = hashStartDt;
            } else {
                myPostData.startDt = "";
            }

            if (hashEndDt != null && hashEndDt != "") {
                myPostData.endDt = hashEndDt;
            } else {
                myPostData.endDt = "";
            }

            if (noServiceData) {
                hashSvc = "";
                hashStor = "";
                popAlertLayer(getMessage("info.service.nodata.msg"));
            } else if (noStorData) {
                hashStor = "";
                popAlertLayer(getMessage("info.store.nodata.msg"));
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData, page: myPostData.page });
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            // session
            if(sessionStorage.getItem("last-url") != null){
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            // Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /* 뒤로가기 */
            var hashlocation = "page=" + $(this).context.p.page + "&svcSeq=" + hashSvc + "&storSeq=" + hashStor
                                    + "&startDt=" + $("#startDt").val() + "&endDt=" + $("#endDt").val();
            if($(this).context.p.postData._search && $("#target").val() != null){
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            // 표기 형식 변경
            $(this).find("td").each(function() {
                if ($(this).index() == 3) { // 잔여시간
                    var txt= $(this).text();
                    if (txt == "") {
                        return;
                    }

                    if (parseInt(txt) >= 9999) {
                        txt = "N/A";
                    } else {
                        txt = txt + getMessage("common.minute");
                    }

                    $(this).text(txt);
                } else if ($(this).index() == 5) { // 구분 (결제구분)
                    var txt= $(this).text();
                    if (txt == "") {
                        return;
                    }
                    if (txt == "0") {
                        $(this).html("Launcher");
                    } else {
                        $(this).html("POS");
                    }
                }
            });

            // pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
            });

            // 포인터
            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event){
            // 제목 셀 클릭시
            if (columnId == 1) {
                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/reservate/" + list.tagSeq);
            }
        }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

function editSettlTypeTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";
    if (val != "") {
        if (val == 0) {
            retVal = "Launcher";
        } else {
            retVal = "POS";
        }
    }

    return 'title="' + retVal  + '"';
}

function editUsageMinuteTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";
    if (val != "") {
        if (val >= 9999) {
            retVal = "N/A";
        } else {
            retVal = val + getMessage("common.minute");
        }
    }

    return 'title="' + retVal  + '"';
}

function reservateInfoDelete() {
    var tagSeqList = "";
    var tagIdList = "";
    var delCnt = 0;

    for (var i = 1; i<$("#limit").val()+1; i++) {
        if ($("#jqg_jqgridData_" + i).prop("checked")) {
            var selTagSeq = $("#jqgridData").jqGrid('getRowData', i).tagSeq;
            var selTagId = $("#jqgridData").jqGrid('getRowData', i).tagId;
            tagSeqList += selTagSeq + ",";
            tagIdList += selTagId + ", ";
            delCnt++;
        }
    }

    if (delCnt == 0) {
        popAlertLayer(getMessage("reservate.delete.not.select.msg"));
        return;
    }

    popConfirmLayer(tagIdList.slice(0, -2) + getMessage("reservate.delete.confirm.msg"), function() {
        formData("NoneForm" , "tagSeqList", tagSeqList.slice(0, -1));
        callByDelete("/api/reservate", "reservateInfoDeleteSuccess", "NoneForm","deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

reservateInfoDeleteSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), function(){location.reload();});
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

function keywordSearch() {
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

searchFieldSet =function() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');
        var searchHtml = "";
        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name){
            case "tagId":
                break;
            default:
                continue;
            }
            searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}
