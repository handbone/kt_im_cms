/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    reservateInfo();
});

reservateInfo = function() {
    callByGet("/api/reservate?tagSeq=" + $("#tagSeq").val() , "reservateInfoSuccess", "NoneForm", "reservateInfoFail");
}

var useCategoryName;
reservateInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.reservateInfo;
        var Citem = data.result.reservateInfo.usageCategoryCount;
        useCategoryName = item.devTypeNm;

        $("#tagId").html(item.tagId);
        $("#useYn").html(item.useYnNm);

        $("#minute").text(item.usageMinute + getMessage("common.minute"));
        $("#prodNm").html(item.prodNm);
        $("#rmndCnt").text(item.rmndCnt + getMessage("common.cnt"));
        var list = "";
        $(Citem).each(function(i, item) {
            if (item.categoryUsed == "N") {
                list += "<li><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                            + "</span><span>" + getMessage("common.useCnt") + "</span>[" + getMessage("common.unavailable") + "]</li>";
            } else {
                if (item.categoryCount == -1) {
                    var setLimitTxt = getMessage("common.unlimit.msg");
                    if (item.deduction == "N") {
                        setLimitTxt = getMessage("common.unlimit.uncount.msg");
                    }
                    list += "<li><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                                + "</span><span>" + getMessage("common.useCnt") + "</span>[" + setLimitTxt + "]</li>";
                } else {
                    list += "<li><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                                + "</span><span>" + getMessage("common.useCnt") + "</span>[" + item.categoryCount + " " + getMessage("common.cnt") + "]</li>";
                }
            }
        });
        $(".useCategoryCount").append(list);
        // 0 : 무제한 / -1 : 입장권 등으로 사용불가
        if (item.prodLmtCnt == 0) {
            $("#rmndCnt").text(getMessage("common.unlimit.word"));
        } else if (item.prodLmtCnt == -1) {
            $("#rmndCnt").text(getMessage("common.cannot.used"));
        }

//        if(item.prodTimeLmt == 0 || item.usageMinute == 9999){
//            $("#minute").html("N/A");
//        }
        if(item.usageMinute >= 9999){
            $("#minute").html("N/A"); // "N/A" ==> 제한없음
        }

        listBack($(".btnList"));

        $("#jqgridData").jqGrid({
            url:makeAPIUrl("/api/reservateUseContent?tagSeq="+$("#tagSeq").val()),
            datatype: "json", // 데이터 타입 지정
            jsonReader : {
                page: "result.currentPage",
                total: "result.totalPage",
                root: "result.useContentList",
                records: "result.totalCount",
                repeatitems: false,
                id: "seq_user"
            },
            colNames:[getMessage("table.num"), getMessage("reservate.table.useContent"), getMessage("reservate.table.device"), getMessage("reservate.table.stTime"),
                getMessage("reservate.table.fnsTime"), "seq"],
            colModel:[
                {name:"num", index: "num", align:"center", width:40},
                {name:"contsTitle", index:"CONTS_TITLE", align:"center"},
                {name:"devId", index: "DEV_ID", width:150, align:"center"},
                {name:"stTime", index: "ST_TIME", width:150, align:"center"},
                {name:"fnsTime", index: "FNS_TIME", width:150, align:"center"},
                {name:"contsUseTimeSeq", index:"CONTS_USE_TIME_SEQ", hidden:true}
                ],
                autowidth: true, // width 자동 지정 여부
                rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
                //rowList:[10,20,30],
                pager: "#pageDiv", // 페이징 할 부분 지정
                viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
                sortname: "CONTS_USE_TIME_SEQ",
                sortorder: "desc",
//              caption:"Client 설치 목록",
                height: "auto",
                multiselect: false,
                loadComplete: function () {
                    //pageMove Max
                    $('.ui-pg-input').on('keyup', function() {
                        this.value = this.value.replace(/\D/g, '');
                        if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
                    });
                }
        });
        jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/reservate");
    }
}

reservateInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/reservate");
}
