/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var prodLmtCnt = 0;
var cnt = 0;

$(document).ready(function(){
    reservateInfo();
    //usageProductlist();
    createDate($("#h_time_time"), 0, 24);
    createDate($("#m_time_time"), 0, 61);
});

reservateInfo = function() {
    callByGet("/api/reservate?tagSeq=" + $("#tagSeq").val() , "reservateInfoSuccess", "NoneForm", "reservateInfoFail");
}

var useCategoryName;
reservateInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.reservateInfo;
        var Citem = data.result.reservateInfo.usageCategoryCount;
        useCategoryName = item.devTypeNm;

        $("#tagNm").html(item.tagId);
        //$("#useYn").html(item.useYn);
        $('input:radio[name=tagUseSttus]:input[value='+ item.useYnCode + ']').attr("checked", true);
        if (item.useYnCode == '04' && item.usageMinute == 0) { // 사용완료
            $('input:radio[name=tagUseSttus]:input[value=05]').attr("checked", true);
        }

        if (item.useYnCode == '02') { // 반품
            $('input:radio[name=tagUseSttus]').attr("disabled", true);
            $("#m_time_time").attr("disabled",true);
            //$('input:radio[name=tagUseSttus]:input[value=02]').attr("checked", true);
        }

        //if (item.useYnCode == '03') { // 사용만료
        //    $('input:radio[name=tagUseSttus]:input[value=03]').attr("checked", true);
        //}

        // 사용중 또는 사용완료 또는 사용만료일 경우 미사용으로 변경 불가
        if ($("input[name='tagUseSttus']").val() =="04" || $("input[name='tagUseSttus']").val() =="05" || $("input[name='tagUseSttus']").val() =="03") {
            $("#useSttusNo").click(function(e){
                e.preventDefault();
                popAlertLayer(getMessage("reservate.alert.useSttus.error.msg"));
            });
        }

        $("#prodNm").html(item.prodNm);
        $("#rmndCnt").val(item.rmndCnt);
        $("#rmndCnt").parent().append("<span id='cnt'> " + getMessage("common.cnt") + "</span>");
        if (item.rmndCnt <= 0) {
            $("#rmndCnt").parents("span").hide();
        }

        $("#m_time_time").val(item.usageMinute);
        $("#m_time_time").parent().append("<span> " + getMessage("common.minute") + "</span>");
        $(".m_time_time").val(item.usageMinute);

        var list = "";
        $(Citem).each(function(i, item) {
            cnt++;
            if (item.categoryUsed == "N") {
                list += "<li class='lineBottom'><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                            + "</span><span>" + getMessage("common.useCnt") + "</span>["
                            + getMessage("common.unavailable") + "]</li>";
            } else {
                // item.prodLmtCnt가 1인 경우는 단일 사용권으로 현재 사용 중인 디바이스와 카테고리 정보가 일치하더라도 사용종료 버튼을 활성화 하지 않음.

                var setLimitTxt = getMessage("common.unlimit.msg");
                if (item.deduction == "N") {
                    setLimitTxt = getMessage("common.unlimit.uncount.msg");
                }

                if (item.prodLmtCnt != 1) {
                    if (useCategoryName == item.categoryName) {
                        if (item.categoryCount != -1) { // categoryCount가 -1인 경우는 무제한으로 설정
                            list += "<li class='lineBottom'><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                                        + "</span><span>" + getMessage("common.useCnt") + "</span><span  style='width: 150px; display: inline-block;'>["
                                        + item.categoryCount + " " + getMessage("common.cnt") + "]</span><input type='hidden' class='categorySeq' value="
                                        + item.categorySeq + " /></span><button onclick='clearUseClient(this)'>" + getMessage("common.useFns") + "</button></li>";
                        } else {
                            list += "<li class='lineBottom'><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                            + "</span><span>" + getMessage("common.useCnt") + "</span><span>[" + setLimitTxt
                            + "]</span><input type='hidden' class='categorySeq' value=" + item.categorySeq + " /></span><button onclick='clearUseClient(this)'>" + getMessage("common.useFns") + "</button></li>";
                        }
                    } else {
                        if (item.categoryCount != -1) { // categoryCount가 -1인 경우는 무제한으로 설정
                            list += "<li class='lineBottom'><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                                        + "</span><span>" + getMessage("common.useCnt") + "</span><span>[" + item.categoryCount + " " + getMessage("common.cnt")
                                        + "]</span><input type='hidden' class='categorySeq' value=" + item.categorySeq + " /></span></li>";
                        } else {
                            list += "<li class='lineBottom'><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                                        + "</span><span>" + getMessage("common.useCnt") + "</span><span>[" + setLimitTxt
                                        + "]</span><input type='hidden' class='categorySeq' value=" + item.categorySeq + " /></span></li>";
                        }
                    }
                } else {
                    if (item.categoryCount != -1) { // categoryCount가 -1인 경우는 무제한으로 설정
                        list += "<li class='lineBottom'><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                                    + "</span><span style='width: 150px; display: inline-block;'>" + getMessage("common.useCnt")
                                    + "</span><span>[" + item.categoryCount + " " + getMessage("common.cnt")
                                    + "]</span><input type='hidden' class='categorySeq' value=" + item.categorySeq + " /></span></li>";
                    } else {
                        list += "<li class='lineBottom'><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.categoryName
                                    + "</span><span style='width: 150px; display: inline-block;'>" + getMessage("common.useCnt")
                                    + "</span><span>[" + setLimitTxt
                                    + "]</span><input type='hidden' class='categorySeq' value=" + item.categorySeq + " /></span></li>";
                    }
                }
            }
        });
        $(".useCategoryCount").append(list);

        prodLmtCnt = item.prodLmtCnt;
        if (item.prodLmtCnt == 0) {
            $("#rmndCnt").hide();
            $("#cnt").hide();
            $("#rmndCnt").parent().append("<span class='infiniti'>" + getMessage("common.unlimit.word") + "</span>");
        } else if (item.prodLmtCnt == -1) {
            $("#rmndCnt").hide();
            $("#cnt").hide();
            $("#rmndCnt").parent().append("<span class='infiniti'>" + getMessage("common.cannot.used") + "</span>");
        }

//        if (item.prodTimeLmt == 0 || item.usageMinute == 9999) {
//            $("#m_time_time").hide();
//            $("#m_time_time").parent().html("N/A");
//        }
        if (item.usageMinute >= 9999) {
            $("#m_time_time").hide();
            $("#m_time_time").parent().html("N/A");
        }

        $("#storSeq").val(item.storSeq);
        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/reservate");
    }
}

reservateInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/reservate");
}

clearUseClient = function(e) {
    popConfirmLayer(getMessage("reservate.confirm.finish.device.msg"), function() {
        $(e).remove();
        formData("NoneForm", "tagId", $("#tagNm").text());
        formData("NoneForm", "storSeq", $("#storSeq").val());
        formData("NoneForm", "devSeq", 0);
        callByPut("/api/reservateUse", "clearUseClientSuccess", "NoneForm","clearFail");
    });
}

clearFail = function(data) {
    popAlertLayer(getMessage("fail.common.finish"));
}

clearUseClientSuccess = function(data) {
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.finish"));
    }
}

function createDate(object, start, last) {
    for (i = start; i < last; i++) {
        var time = i;
        if(i <10) time = '0' + time;
        object.append('<option value=' + i + '>' + time  + '</option>');
    }
}

function reservateInfoUpdate() {
    var temp = $(':radio[name="tagUseSttus"]:checked').val();
    var useSttus ="Y";
    var useYn ="Y";

    if (temp == '04') { // 사용중
    } else if (temp == '05') { // 사용완료
        useYn ="N";
    } else if (temp == '03') { // 사용만료
        useYn ="N";
        useSttus = "E";
    } else if (temp == '01') { // 미사용
        useSttus = "N";
    } else { //반품
        useSttus = "B";
        useYn ="N";
    }

    var rmndCnt = $("#rmndCnt").val();
    var usageHour = Math.floor($("#m_time_time").val() / 60);
    var usageMinute = $("#m_time_time").val() % 60;
    var changeYN = "Y";
    if ($("#m_time_time").val() == $(".m_time_time").val() || $(".minute").text() == "N/A") {
        usageHour = 0;
        usageMinute = 0;
        changeYN = "N";
    }

    var selCategorys = "";
    var selCategoryCounts = "";
    var selCategorySeq = "";

    if(prodLmtCnt != -1 && prodLmtCnt < rmndCnt){
        popAlertLayer(getMessage("reservate.alert.prodLmtCnt.errorS.msg") + " " + prodLmtCnt + getMessage("reservate.alert.prodLmtCnt.errorF.msg"));
        return;
    }

    if ($(".infiniti").text() == "") {
        $(".categoryName").each(function(i) {
            if ($(this).text() != "" && $(this).next("span").next("span").text() != "") {
                if (selCategorys != "") {
                    selCategorys += ",";
                    selCategoryCounts += ",";
                    selCategorySeq += ",";
                }
                selCategorys += $(this).text();
                selCategoryCounts += $("#rmndCnt").val();
                selCategorySeq += $(this).parent().find("input.categorySeq").val();

                var ary = selCategorys.split(",");
                if (ary.length != 1 && ary.length != $.unique(ary).length) {
                    popAlertLayer(getMessage("common.category.duplicate.msg"));
                    return;
                }
            }
        })
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("NoneForm" , "selCategorys",selCategorys);
        formData("NoneForm" , "selCategoryCounts",selCategoryCounts);
        formData("NoneForm" , "selCategorySeq",selCategorySeq);
        formData("NoneForm" , "tagSeq", $("#tagSeq").val());
        formData("NoneForm" , "rmndCnt", rmndCnt);
        formData("NoneForm" , "useYn",useYn);
        formData("NoneForm" , "usageHour", usageHour);
        formData("NoneForm" , "usageMinute", usageMinute);
        formData("NoneForm" , "timeChange", changeYN);
        formData("NoneForm" , "useSttus", useSttus);

        callByPut("/api/reservate", "reservateUpdateSuccess", "NoneForm","updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

reservateUpdateSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/reservate/" + $("#tagSeq").val());
    }
}

