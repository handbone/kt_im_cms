/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var readyPage = false;
var gridState;
var tempState;
var hashSvc;
var searchField;
var searchString;
var apiUrl;
var totalPage = 0;

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            serviceList();
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    serviceList();

    $("#keyword").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });

    if ($("#memberSec").val() == "05" ) {
        // 05 : 매장 관리자는 신규 매장 등록 불가
        $("#btn_regist").hide();
    }
});

var noData = false;
serviceList = function() {
    callByGet("/api/service?searchType=list", "serviceListSuccess", "NoneForm");
}

serviceListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var serviceListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
        });
        $("#svcSelbox").html(serviceListHtml);

        if (document.location.hash != "") {
            var str_hash = document.location.hash.replace("#","");
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            $("#svcSelbox").val(hashSvc);
        }

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").attr('disabled', 'true');
        }

        var searchSvc = $("#svcSelbox").val();
        if (searchSvc != null && searchSvc != 0) {
            apiUrl = makeAPIUrl("/api/store?svcSeq=" + searchSvc);
        } else {
            apiUrl = makeAPIUrl("/api/store?svcSeq=-1");
        }

        if (!readyPage) {
            readyPage = true;
            storeList();
        } else {
            jQuery("#jqgridData").setGridParam({"url": apiUrl});
            jQuery("#jqgridData").trigger("reloadGrid");
        }
    } else {
        noData = true;
        apiUrl = makeAPIUrl("/api/store?svcSeq=-1");
        storeList();
    }
}

storeList = function() {
    var colNameList = [
        getMessage("table.num"),
        getMessage("table.storeName"),
        getMessage("table.telNo"),
        getMessage("table.addr"),
        getMessage("table.useYn"),
        getMessage("table.regdate"),
        "storSeq"
    ];

    $("#jqgridData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.storeList",
            records: "result.totalCount",
            repeatitems: false
        },
        colNames:colNameList,
        colModel:[
            {name:"num", index: "num", align:"center", width:40},
            {name:"storNm", index:"STOR_NM", align:"center", formatter:pointercursor},
            {name:"storTelNo", index:"STOR_TEL_NO", align:"center"},
            {name:"storBasAddr", index:"STOR_BAS_ADDR", align:"center", width:360},
            {name:"useYn", index:"USE_YN", align:"center", cellattr: editCellTitleAttr},
            {name:"regDt", index:"REG_DT", align:"center"},
            {name:"storSeq", index:"STOR_SEQ", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "STOR_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: false,
        beforeRequest:function() {
            tempState = gridState;
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());
            hashSvc = checkUndefined($("#svcSelbox").val());

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if (tempState == "SEARCH") {
                    myPostData.page = 1;
                } else {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "") {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            if (hashSvc != null && hashSvc != "") {
                myPostData.svcSeq = hashSvc;
            } else {
                myPostData.svcSeq = "";
            }

            if (noData) {
                hashSvc = "";
                popAlertLayer(getMessage("info.service.nodata.msg"));
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData, page: myPostData.page });
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            // session
            if(sessionStorage.getItem("last-url") != null){
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            // Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /* 뒤로가기 */
            var hashlocation = "page=" + $(this).context.p.page + "&svcSeq=" + hashSvc;
            if($(this).context.p.postData._search && $("#target").val() != null){
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            $(this).find("td").each(function() {
                if ($(this).index() == 4) { // 사용유무
                    var str = $(this).text();

                    if (str == 'Y') {
                        str = getMessage("common.enable");
                    } else if (str == 'N') {
                        str = getMessage("common.disable");
                    }
                    $(this).html(str);
                }
            });

            //pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
            });

            //포인터
            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event){
            // 매장명 셀 클릭시
            if (columnId == 1) {
                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/store/" + list.storSeq);
            }
        }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

function editCellTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";
    if (val == "Y") {
        retVal = getMessage("common.enable")
    } else {
        retVal = getMessage("common.disable")
    }

    return 'title="' + retVal  + '"';
}

reloadStoreListSuccess = function(data) {
    if(data.resultCode == "1000"){
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i,item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storSelbox").html(storeListHtml);

        jqGridReload();
    }
}

function jqGridReload() {
    var searchSvc = $("#svcSelbox option:selected").val();
    if(searchSvc != 0){
        searchSvc = "?svcSeq=" + searchSvc;
    }else{
        searchSvc = "?svcSeq=-1";
    }

    jQuery("#jqgridData").setGridParam({"url": makeAPIUrl("/api/store" + searchSvc)});
    jQuery("#jqgridData").trigger("reloadGrid");
}

function keywordSearch() {
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

searchFieldSet = function() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');
        var searchHtml = "";
        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name){
            case "storNm":
                break;
            case "storTelNo":
                break;
            case "storAddr":
                break;
            default:
                continue;
            }
            searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

function moveToRegistView() {
    if ($("#svcSelbox").val() == null) {
        popAlertLayer(getMessage("info.service.nodata.msg"));
        return;
    }

    pageMove("/store/regist?svcSeq=" + $("#svcSelbox").val());
}