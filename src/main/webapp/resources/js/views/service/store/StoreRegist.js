/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    serviceList();

    limitInputTitle("storeName");

    $("#storeCode").keyup(function(event) {
        re = /[^A-Za-z0-9]/gi; //영문,숫자
        var temp=$("#storeCode").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#storeCode").val(temp.replace(re,""));
        }
    });

    $("#zipcode").bind("keyup",function() {
        re = /[ㄱ-ㅎㄲ-ㅆㅏ-ㅢㅣ가-힣a-zA-Z~!@\#$%^&*\()\=+_';.,":[]|]/gi;  //한글,영문,숫자
        var temp=$("#zipcode").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#zipcode").val(temp.replace(re,"")); }
    });

    $("#basAddr").bind("keyup",function() {
        re = /[~!@\#$%^&*<>\()\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#basAddr").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#basAddr").val(temp.replace(re,""));
        }
    });

    $("#dtlAddr").bind("keyup",function() {
        re = /[~!@\#$%^&*<>\()\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#dtlAddr").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#dtlAddr").val(temp.replace(re,""));
        }
    });

    setkeyup();
});

function openDaumPostcode(zip1,addr,addr_sub) {
    new daum.Postcode({
        oncomplete: function(data) {
            document.getElementById(zip1).value = data.zonecode;
            if (data.userSelectedType === 'R') 
            {
                document.getElementById(addr).value = data.roadAddress;
            }
            else
            {
                document.getElementById(addr).value = data.jibunAddress;
            }
            document.getElementById(addr_sub).focus();
        }
    }).open();
}

serviceList = function() {
    callByGet("/api/service?searchType=list","serviceListSuccess","NoneForm");
}

serviceListSuccess = function(data) {
    if(data.resultCode == "1000"){
        var serviceListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
        });
        $("#svcSelbox").html(serviceListHtml);
        $("#svcSelbox").val($("#svcSeq").val());

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").attr('disabled', 'true');
        }
    }
}

function storeRegist() {
    if (checkNull($("#storeName").val()) == "") {
        popAlertLayer(getMessage("store.name.empty.msg"));
        return;
    }

    var returnState = false;
    $(".telNo").each(function(){
        if (checkNull($(this).val()) == "") {
            returnState = true;
            return;
        }
    });
    if (returnState) {
        popAlertLayer(getMessage("store.tel.no.error.msg"));
        return;
    }

    var telnoStr = $(".telNo").eq(0).val() +"-"+$(".telNo").eq(1).val()+"-"+$(".telNo").eq(2).val();
    if (!validateTelNumber(telnoStr) && !validatePhoneNumber(telnoStr)) {
        popAlertLayer(getMessage("common.telno.error.msg"));
        return;
    }

    if (checkNull($("#zipcode").val()) == "" || checkNull($("#basAddr").val()) == "") {
        popAlertLayer(getMessage("store.address.empty.msg"));
        return;
    }

    if (checkNull($("#storeCode").val()) == "") {
        popAlertLayer(getMessage("store.code.empty.msg"));
        return;
    }

    var pattern = /^[A-Za-z]{1}[0-9]{4,5}$/;
    if (!pattern.test($("#storeCode").val())) {
        popAlertLayer(getMessage("store.code.input.error.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        formData("NoneForm", "svcSeq", $("#svcSelbox").val());
        formData("NoneForm", "storNm", $("#storeName").val());
        formData("NoneForm", "zipCode", $("#zipcode").val());
        formData("NoneForm", "basAddr", $("#basAddr").val());
        formData("NoneForm", "dtlAddr", $("#dtlAddr").val());
        formData("NoneForm", "storTelNo", telnoStr);
        formData("NoneForm", "storCode", $("#storeCode").val());

        callByPost("/api/store" , "storeRegistSuccess", "NoneForm", "insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

storeRegistSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        var retUrl = "/store#page=1&svcSeq=" + $("#svcSelbox").val();
        popAlertLayer(getMessage("success.common.insert"), retUrl);
    } else if(data.resultCode == "1011") {
        if (data.resultMsg == "exist storeCode") {
            popAlertLayer(getMessage("store.code.exist.msg"));
        } else if (data.resultMsg == "exist storNm") {
            popAlertLayer(getMessage("store.name.exist.msg"));
        } else {
            popAlertLayer(getMessage("fail.common.insert"));
        }
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

