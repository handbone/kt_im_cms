/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var serviceSeq = 0;

$(document).ready(function() {
    limitInputTitle("storeName");

    $("#storeCode").keyup(function(event) {
        re = /[^A-Za-z0-9]/gi; //영문,숫자
        var temp=$("#storeCode").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#storeCode").val(temp.replace(re,""));
        }
    });

    $("#zipcode").bind("keyup",function() {
        re = /[ㄱ-ㅎㄲ-ㅆㅏ-ㅢㅣ가-힣a-zA-Z~!@\#$%^&*\()\=+_';.,":[]|]/gi;  //한글,영문,숫자
        var temp=$("#zipcode").val();
        if(re.test(temp)){ //특수문자가 포함되면 삭제하여 값으로 다시셋팅
        $("#zipcode").val(temp.replace(re,"")); }
    });

    $("#basAddr").bind("keyup",function() {
        re = /[~!@\#$%^&*<>\()\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#basAddr").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#basAddr").val(temp.replace(re,""));
        }
    });

    $("#dtlAddr").bind("keyup",function() {
        re = /[~!@\#$%^&*<>\()\=+_'"]/gi;  //한글,영문,숫자
        var temp=$("#dtlAddr").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#dtlAddr").val(temp.replace(re,""));
        }
    });

    setkeyup();
});

function initFindUserInfo(data) {
    $("#loading").hide();
    if (data.resultCode != "1000") {
        $("#mbrTelNo").val("");
        $("#mbrMphonNo").val("");
        $("#mbrEmailId").val("");
        $("#mbrEmailDomain").val("");
        $("#domainList").val("");

        var mvP = sessionStorage.getItem("cofirm-last-url");

        if (typeof mvP == "undefined") {
            mvP = "/store";
        }
        $(".inputBox").val("");
        popAlertLayer(getMessage("common.bad.request.msg"), mvP);
    } else {
        storeInfo();
    }
}

function initNotFindUserInfo(data) {
    $("#loading").hide();

    var mvP = sessionStorage.getItem("cofirm-last-url");

    if (typeof mvP == "undefined") {
        mvP = "/store";
    }
    $(".inputBox").val("");
    popAlertLayer(getMessage("common.bad.request.msg"), mvP);
//    back
}

function openDaumPostcode(zip1,addr,addr_sub) {
    new daum.Postcode({
        oncomplete: function(data) {
            document.getElementById(zip1).value = data.zonecode;
            if (data.userSelectedType === 'R')
            {
                document.getElementById(addr).value = data.roadAddress;
            }
            else
            {
                document.getElementById(addr).value = data.jibunAddress;
            }
            document.getElementById(addr_sub).value = "";
            document.getElementById(addr_sub).focus();
        }
    }).open();
}

storeInfo = function() {
    callByGet("/api/store?storSeq=" + $("#storSeq").val() + "&getType=edit", "storeInfoSuccess", "NoneForm", "storeInfoFail");
}

storeInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.storeInfo;
        serviceSeq = item.svcSeq;
        $("#storeName").val(xssChk(item.storNm));
        $("#svcNm").html(item.svcNm);

        var str = item.storTelNo;
        var strVal = str.split("-");

        for(var i=0; i < strVal.length; i++){
            $(".telNo").eq(i).val(strVal[i]);
        }

        $("#zipcode").val(item.zipcd);
        $("#basAddr").val(item.storBasAddr);
        $("#dtlAddr").val(item.storDtlAddr);
        $("#storeCode").val(item.storCode);
        $("input[name='useYn'][value="+item.useYn+"]").attr("checked",true);

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/store");
    }
}

storeInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/store");
}

function storeUpdate() {
    if (checkNull($("#storeName").val()) == "") {
        popAlertLayer(getMessage("store.name.empty.msg"));
        return;
    }

    var returnState = false;
    $(".telNo").each(function(){
        if (checkNull($(this).val()) == "") {
            returnState = true;
            return;
        }
    });
    if (returnState) {
        popAlertLayer(getMessage("store.tel.no.error.msg"));
        return;
    }

    var telnoStr = $(".telNo").eq(0).val() +"-"+$(".telNo").eq(1).val()+"-"+$(".telNo").eq(2).val();
    if (!validateTelNumber(telnoStr) && !validatePhoneNumber(telnoStr)) {
        popAlertLayer(getMessage("common.telno.error.msg"));
        return;
    }

    if (checkNull($("#zipcode").val()) == "" || checkNull($("#basAddr").val()) == "") {
        popAlertLayer(getMessage("store.address.empty.msg"));
        return;
    }

    if (checkNull($("#storeCode").val()) == "") {
        popAlertLayer(getMessage("store.code.empty.msg"));
        return;
    }

    var pattern = /^[A-Za-z]{1}[0-9]{4,5}$/;
    if (!pattern.test($("#storeCode").val())) {
        popAlertLayer(getMessage("store.code.input.error.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("NoneForm", "storSeq", $("#storSeq").val());
        formData("NoneForm", "svcSeq", serviceSeq);
        formData("NoneForm", "storNm", $("#storeName").val());
        formData("NoneForm", "zipCode", $("#zipcode").val());
        formData("NoneForm", "basAddr", $("#basAddr").val());
        formData("NoneForm", "dtlAddr", $("#dtlAddr").val());
        formData("NoneForm", "storTelNo", telnoStr);
        formData("NoneForm", "storCode", $("#storeCode").val());
        formData("NoneForm", "useYn", $('input:radio[name=useYn]:checked').val());

        callByPut("/api/store" , "storeUpdateSuccess", "NoneForm", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

storeUpdateSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/store");
    } else if(data.resultCode == "1011") {
        if (data.resultMsg == "exist storeCode") {
            popAlertLayer(getMessage("store.code.exist.msg"));
        } else if (data.resultMsg == "exist storNm") {
            popAlertLayer(getMessage("store.name.exist.msg"));
        } else {
            popAlertLayer(getMessage("fail.common.insert"));
        }
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

