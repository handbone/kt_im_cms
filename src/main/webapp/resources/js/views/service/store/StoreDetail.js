/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    storeInfo();
});

storeInfo = function() {
    callByGet("/api/store?storSeq=" + $("#storSeq").val() , "storeInfoSuccess", "NoneForm", "storeInfoFail");
}

storeInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.storeInfo;
        $("#storNm").html(item.storNm);
        $("#svcNm").html(item.svcNm);
        $("#cretrId").html(item.cretrId);
        $("#cretDt").html(item.cretDt);
        $("#amdrId").html(item.amdrId);
        $("#amdDt").html(item.amdDt);
        $("#storTelNo").html(item.storTelNo);
        $("#storAddr").html("(" + item.zipcd + ") " + item.storBasAddr);
        $("#storCode").html(item.storCode);

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/store");
    }
}

storeInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/store");
}

