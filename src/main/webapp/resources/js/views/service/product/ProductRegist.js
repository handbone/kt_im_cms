/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var isReloadStor = false;

$(document).ready(function() {
    serviceList();

    $("#prodPrc").keyup(function(e) {
        $(this).val($("#prodPrc").val().replace(/[^\d]+/g, ''));
        $(this).val(numberWithCommas($("#prodPrc").val()));
    });

    limitInputTitle("prodNm,prodDesc,gsrProdCode");

    $('.remainingPrc').each(function() {
        var $maximumCount = $(this).attr("max") * 1;
        var $input = $(this);
        var update = function() {
            var now = $maximumCount - $input.val().length;
            // 사용자가 입력한 값이 제한 값을 초과하는지를 검사한다.
            if (now < 0) {
                var str = $input.val();
                popAlertLayer(getMessage("product.price.limit.msg"));
                $input.val(str.substr(0, $maximumCount));
                now = 0;
            }
        }
        $input.bind('input keyup paste', function() {
            setTimeout(update, 0)
        });
        update();
    });

    $("#userSet").bind('input keyup paste', function() {
        if ($("#userSet").val() >= 1441) {
            popAlertLayer(getMessage("msg.reservate.too.setTime"));
            $("#userSet").val("");
        }  else if ($("#userSet").val() <= 0) {
            popAlertLayer(getMessage("msg.device.zero.setTime"));
            $("#userSet").val("");
        }
    });

    $("#userProdLmtCnt").bind('input keyup paste', function() {
        if ($("#userProdLmtCnt").val() >= 99) {
            popAlertLayer(getMessage("msg.reservate.too.setLimitCount"));
            $("#userProdLmtCnt").val("");
        } else if ($("#userProdLmtCnt").val() <= 0) {
            popAlertLayer(getMessage("msg.reservate.zero.setLimitCount"));
            $("#userProdLmtCnt").val("");
        }
    });

    setkeyup();
});

serviceList = function() {
    callByGet("/api/service?searchType=list","serviceListSuccess","NoneForm");
}

serviceListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var serviceListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
        });
        $("#svcSelbox").html(serviceListHtml);
        $("#svcSelbox").val($("#svcSeq").val());

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").attr('disabled', 'true');
        }

        storeList($("#svcSelbox option:selected").val());
    }
}

storeList = function(searchSvc) {
    if (searchSvc != 0) {
        searchSvc = "&svcSeq=" + searchSvc;
    } else {
        searchSvc = "&svcSeq=-1";
    }

    callByGet("/api/store?searchType=list" + searchSvc,"storeListSuccess","NoneForm");
}

storeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storSelbox").html(storeListHtml);
        $("#storSelbox").attr("disabled", false);
        if (!isReloadStor) {
            $("#storSelbox").val($("#storSeq").val());
        }

        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 매장 변경 불가
            $("#storSelbox").attr('disabled', 'true');
        }

        useCategoryListInfo($("#storSelbox option:selected").val());
        $("input:not(.btnCancel):not(.btnLogout)").unbind("click");
    }  else {
        $("#storSelbox").html("");
        $("#storSelbox").attr("disabled", true);
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("input:not(.btnCancel):not(.btnLogout)").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
    }
}

useCategoryListInfo = function(storSeq) {
    $(".prodCategory").empty();
    callByGet("/api/productContsCategory?storSeq=" + storSeq, "productUseCategoryListInfoSuccess", "NoneForm");
}

productUseCategoryListInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var useCatorgyListHtml = "";
        $(data.result.productContsCategoryList).each(function(i, item) {
            useCatorgyListHtml += "<li>";
            useCatorgyListHtml += "<input type='hidden' class='categoryName' value='" + item.secondCtgNm + "'/>";
            useCatorgyListHtml += "<input type='hidden' class='categorySeq' value='0' />";
            useCatorgyListHtml += "<span class='device'>" + item.secondCtgNm + "</span>";
            useCatorgyListHtml += "<span class='useCnt'>" + getMessage("common.useCnt") + "</span>";
            useCatorgyListHtml += "<span class='pad10'><input type='text' class='categoryCount onlynum inputBox lengthSM' value='0'></span>";
            useCatorgyListHtml += "<span class='pad10'><select class='stateProduct inputBox lengthSM' onchange='categoryState(this)'>"
                                    + "<option value=0>" + getMessage("common.limit.count") + "</option>"
                                    + "<option value=1>" + getMessage("common.unlimit.msg") + "</option>"
                                    + "<option value=2>" + getMessage("common.unlimit.uncount.msg") + "</option>"
                                    + "<option value=3>" + getMessage("common.cannot.used") + "</option>"
                                    + "</select></span>"
            useCatorgyListHtml += "</li>";
        });
        $(".prodCategory").append(useCatorgyListHtml); // 조회된 사용카테고리 정보로 업데이트

        // 동적으로 카테고리 추가하여 .onlynum에 대한 재설정
        uncountSel();
        chkProdLmtCnt();
        keyup();
    }
}

function storeReload() {
    isReloadStor = true;
    storeList($("#svcSelbox option:selected").val());
}

function categoryReload() {
    useCategoryListInfo($("#storSelbox option:selected").val());
}

function numberWithCommas(x) {
    return x.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

categoryState = function(e) {
    if ($(e).val() == 0) {
        $(e).parents("li").children("span:eq(2)").find("input.categoryCount").attr("readonly",false);
    } else if ($(e).val() == 1) {
        $(e).parents("li").children("span:eq(2)").find("input.categoryCount").attr("readonly",true);
    } else {
        $(e).parents("li").children("span:eq(2)").find("input.categoryCount").attr("readonly",true);
    }
}

function productRegist() {
    if ($("#prodNm").val().trim() == "") {
        popAlertLayer(getMessage("product.use.prodNm.empty.msg"));
        return;
    }

    var playCount = $("#prodLmtCnt").val();

    if ($("#prodLmtCnt").val() == "userSet") {

        if ($("#userProdLmtCnt").val().trim() == "") {
            popAlertLayer(getMessage("msg.product.no.limitCount"));
            return;
        }

        playCount = $("#userProdLmtCnt").val().trim();
    }

    if ($("#prodTimeLmt").val() == "userSet") {
        if ($("#userSet").val().trim() == "") {
            popAlertLayer(getMessage("msg.product.no.useTime"));
            return;
        }
    }

    if ($("#prodPrc").val().trim() == "") {
        popAlertLayer(getMessage("product.price.empty.msg"));
        return;
    }

    if ($("#gsrProdCode").val().trim() == "") {
        popAlertLayer(getMessage("product.prod.gsr.code.empty.msg"));
        return;
    }

    if ($("#gsrProdCode").val().trim().length < 13) {
        popAlertLayer(getMessage("product.invalid.prod.code.msg"));
        return;
    }

    var selCategorys = "";
    var selCategoryCounts = "";
    var selDeduction = "";
    $(".categoryName").each(function(i){
        if ($(this).val() != "" && $(this).parents("li").find("input.categoryCount").val() != "") {
            if (selCategorys != "") {
                selCategorys += ",";
                selCategoryCounts += ",";
                selDeduction += ",";
            }
            selCategorys += $(this).val();
            if ($(this).parents("li").find(".stateProduct").val() == 1) {
                selCategoryCounts += -1;
                selDeduction += "Y";
            } else if ($(this).parents("li").find(".stateProduct").val() == 2) {
                selCategoryCounts += -1;
                selDeduction += "N";
            } else if ($(this).parents("li").find(".stateProduct").val() == 3) {
                selCategoryCounts += 0;
                selDeduction += "Y";
            } else {
                selCategoryCounts += $(this).parents("li").find("input.categoryCount").val();
                selDeduction += "Y";
            }
        }
    });

    var ary = selCategorys.split(",");
    if (ary.length != 1 && ary.length != $.unique(ary).length) {
        popAlertLayer(getMessage("product.duplicate.category.msg"));
        return;
    }
    var playTime = $("#prodTimeLmt").val();
    if ($("#prodTimeLmt").val() == "userSet") {
        if ($("#userSet").val().trim() == "") {
            popAlertLayer(getMessage("msg.product.no.useTime"));
            return;
        }
        playTime = $("#userSet").val().trim();
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        var prodPrc = $("#prodPrc").val().replace(/[^\d]+/g, '');
        var prodDesc = $("#prodDesc").val();
        prodDesc = prodDesc.replace(/(?:\r\n|\r|\n)/g, '<br />');

        formData("NoneForm", "selCategorys", selCategorys);
        formData("NoneForm", "selCategoryCounts", selCategoryCounts);
        formData("NoneForm", "selDeduction", selDeduction);

        formData("NoneForm", "prodNm", $("#prodNm").val());
        formData("NoneForm", "prodDesc", prodDesc);
        formData("NoneForm", "prodTimeLmt", playTime);
        formData("NoneForm", "prodLmtCnt", playCount);
        formData("NoneForm", "prodPrc", prodPrc);
        formData("NoneForm", "svcSeq", $("#svcSelbox").val());
        formData("NoneForm", "storSeq", $("#storSelbox").val());
        formData("NoneForm", "gsrProdCode", $("#gsrProdCode").val());

        callByPost("/api/product", "productRegistSuccess", "NoneForm", "insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

productRegistSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        var retUrl = "/product#page=1&svcSeq=" + $("#svcSelbox").val() + "&storSeq=" + $("#storSelbox").val();
        popAlertLayer(getMessage("success.common.insert"), retUrl);
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("product.exist.prod.gsr.code.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

function createDate(object, start, last) {
    object.append('<option value=0>' + getMessage("common.unlimit.msg") + '</option>');
    for (i = start; i < last; i++) {
        var time = i;
        object.append('<option value=' + i*60 + '>' + time  + ' ' + getMessage("common.time") + '</option>');
    }
}

uncountSel = function() {
    if ($("#prodTimeLmt").val() == "0") {
        $(".stateProduct[value=2]").val(1);
        $(".stateProduct option[value=2]").attr("disabled",true);
        $("#userSetBox").hide();
    } else {
        if ($("#prodTimeLmt").val() == "userSet") {
            $("#userSetBox").show();
        } else {
            $("#userSetBox").hide();
        }
        $(".stateProduct option[value=2]").attr("disabled",false);
    }
}

var chkProdLmtCnt = function () {

    if ($("#prodLmtCnt").val() == "userSet") {
        $("#userSetProdLmtCntBox").show();
    } else {
        $("#userSetProdLmtCntBox").hide();
    }

};
