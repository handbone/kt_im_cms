/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var readyPage = false;
var gridState;
var tempState;
var searchField;
var searchString;
var hashSvc;
var hashStor;
var apiUrl;
var totalPage = 0;

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashStor = checkUndefined(findGetParameter(str_hash,"storSeq"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            serviceList();
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    serviceList();

    $("#keyword").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

var noServiceData = false;
serviceList = function() {
    callByGet("/api/service?searchType=list", "serviceListSuccess", "NoneForm");
}

serviceListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var serviceListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            serviceListHtml += "<option value=\"" + item.svcSeq + "\">" + item.svcNm + "</option>";
        });
        $("#svcSelbox").html(serviceListHtml);

        if (document.location.hash != "") {
            var str_hash = document.location.hash.replace("#","");
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            $("#svcSelbox").val(hashSvc);
        }

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcSelbox").attr('disabled', 'true');
        }
        storeList($("#svcSelbox").val(), false);
    } else {
        noServiceData = true;
        storeList(0, false);
    }
}

var noStorData = false;
storeList = function(searchSvc, isReload) {
    if (searchSvc != null && searchSvc != 0) {
        searchSvc = "&svcSeq=" + searchSvc;
    } else {
        searchSvc = "&svcSeq=-1";
    }

    if (isReload) {
        callByGet("/api/store?searchType=list" + searchSvc, "reloadStoreListSuccess", "NoneForm");
    } else {
        callByGet("/api/store?searchType=list" + searchSvc, "storeListSuccess", "NoneForm");
    }
}

storeReload = function() {
    noStorData = false;
    storeList($("#svcSelbox").val(), true);
}

storeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i,item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storSelbox").html(storeListHtml);
        $("#storSelbox").attr("disabled", false);

        if (document.location.hash != "") {
            var str_hash = document.location.hash.replace("#","");
            hashStor = checkUndefined(findGetParameter(str_hash,"storSeq"));
            $("#storSelbox").val(hashStor);
        }

        if ($("#memberSec").val() == "05" ) {
            // 05 : 매장 관리자, 매장 변경 불가
            $("#storSelbox").attr('disabled', 'true');
        }
    } else {
        $("#storSelbox").html("");
        $("#storSelbox").attr("disabled", true);
        noStorData = true;
    }

    var searchStore = $("#storSelbox").val();
    if (searchStore != null && searchStore != 0) {
        apiUrl = makeAPIUrl("/api/product?storSeq=" + searchStore);
    } else {
        apiUrl = makeAPIUrl("/api/product?storSeq=-1");
    }

    if (!readyPage) {
        readyPage = true;
        productList();
    } else {
        jQuery("#jqgridData").setGridParam({"url": apiUrl});
        jQuery("#jqgridData").trigger("reloadGrid");
    }
}

reloadStoreListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i,item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storSelbox").html(storeListHtml);
        $("#storSelbox").attr("disabled", false);
    } else {
        $("#storSelbox").html("");
        $("#storSelbox").attr("disabled", true);
        noStorData = true;
    }

    jqGridReload();
}

jqGridReload = function() {
    var searchStore = $("#storSelbox").val();
    if (searchStore != null && searchStore != 0) {
        searchStore = "?storSeq=" + searchStore;
    } else {
        searchStore = "?storSeq=-1";
    }

    jQuery("#jqgridData").setGridParam({"url": makeAPIUrl("/api/product" + searchStore), page: 1});
    jQuery("#jqgridData").trigger("reloadGrid");
}

productList = function(){
    var colNameList = [
        getMessage("table.num"),
        getMessage("product.table.prodNm"),
        getMessage("product.table.time.limit"),
        getMessage("product.comn.tot.limit.count"),
        getMessage("product.table.use.price"),
        getMessage("table.useYn"),
        getMessage("table.regdate"),
        "prodSeq"
    ];

    $("#jqgridData").jqGrid({
        url:apiUrl,
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.productList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames:colNameList,
        colModel:[
            {name:"num", index: "num", align:"center", width:40},
            {name:"prodNm", index: "PROD_NM", align:"center", width:150, formatter:pointercursor},
            {name:"prodTimeLmt", index:"PROD_TIME_LMT", align:"center", cellattr: editTimeLmtTitleAttr},
            {name:"prodLmtCnt", index:"PROD_LMT_CNT", align:"center", cellattr: editLmtCntTitleAttr},
            {name:"prodPrc", index: "PROD_PRC", align:"center", width:150, cellattr: editPrcTitleAttr},
            {name:"useYn", index: "USE_YN", align:"center", width:150, cellattr: editUseYnTitleAttr},
            {name:"regDt", index:"REG_DT", align:"center"},
            {name:"prodSeq", index:"PROD_SEQ", hidden:true}
        ],
        autowidth: true, // width 자동 지정 여부
        rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
        //rowList:[10,20,30],
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "PROD_SEQ",
        sortorder: "desc",
        height: "auto",
        multiselect: true,
        beforeRequest:function() {
            tempState = gridState;
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            if (isNaN(page)) {
                page = 1;
            }

            if (totalPage > 0 && totalPage < page) {
                page = 1;
            }

            if (gridState != "SEARCH") {
                $("#target").val(searchField);
                $("#keyword").val(searchString);
            }
            searchField = checkUndefined($("#target").val());
            searchString = checkUndefined($("#keyword").val());
            hashSvc = checkUndefined($("#svcSelbox").val());
            hashStor = checkUndefined($("#storSelbox").val());

            if (gridState == "HASH") {
                myPostData.page = page;
                tempState = "READY";
            } else {
                if (tempState == "SEARCH") {
                    myPostData.page = 1;
                } else {
                    tempState = "";
                }
            }

            if (gridState == "NONE") {
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                myPostData.page = page;

                if (searchField != null) {
                    tempState = "SEARCH";
                } else {
                    tempState = "READY";
                }
            }

            if (searchField != null && searchField != "" && searchString != "") {
                myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            } else {
                delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
            }

            gridState = "GRIDBEGIN";

            if (hashSvc != null && hashSvc != "") {
                myPostData.svcSeq = hashSvc;
            } else {
                myPostData.svcSeq = "";
            }

            if (hashStor != null && hashStor != "") {
                myPostData.storSeq = hashStor;
            } else {
                myPostData.storSeq = "";
            }

            if (noServiceData) {
                hashSvc = "";
                hashStor = "";
                popAlertLayer(getMessage("info.service.nodata.msg"));
            } else if (noStorData) {
                hashStor = "";
                popAlertLayer(getMessage("info.store.nodata.msg"));
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData, page: myPostData.page });
        },
        loadComplete: function (data) {
            var str_hash = document.location.hash.replace("#","");
            var page = parseInt(findGetParameter(str_hash,"page"));

            // session
            if(sessionStorage.getItem("last-url") != null){
                sessionStorage.removeItem('state');
                sessionStorage.removeItem('last-url');
            }

            // Search 필드 등록
            searchFieldSet();

            if (data.resultCode == 1000) {
                totalPage = data.result.totalPage;

                if (page != data.result.currentPage) {
                    tempState = "";
                }
            } else {
                tempState = "";
            }

            /*뒤로가기*/
            var hashlocation = "page=" + $(this).context.p.page + "&svcSeq=" + hashSvc + "&storSeq=" + hashStor;
            if($(this).context.p.postData._search && $("#target").val() != null){
                hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
            }

            gridState = "GRIDCOMPLETE";
            document.location.hash = hashlocation;

            if (tempState != "" || str_hash == hashlocation) {
                gridState = "READY";
            }

            //표기 형식 변경
            $(this).find("td").each(function() {
                if ($(this).index() == 4) { // 이용횟수
                    var str = $(this).text();
                    if (str == 0 && str != '') {
                        $(this).text(getMessage("common.unlimit.word"));
                    } else if (str == -1 && str != '') {
                        $(this).text(getMessage("common.cannot.used"));
                    } else {
                        if (str != '') {
                            str += getMessage("common.cnt");
                        }
                        $(this).text(str);
                    }
                } else if ($(this).index() == 5) { // 이용금액
                    var str = $(this).text();
                    str =str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
                    if (str != "") {
                        str += " " + getMessage("common.price.unit");
                    }
                    $(this).text(str);
                } else if ($(this).index() == 3) { // 제한 시간
                    var str = $(this).text();
                    if (str == "0") {
                        str=getMessage("common.unlimit.uncount.msg"); // "N/A" == > 제한없음
                    } else if (str != '') {
                        str += " " + getMessage("common.minute");
                    }

                    $(this).text(str);
                } else if ($(this).index() == 6) { // 사용유무
                    var str = $(this).text();

                    if (str == 'Y') {
                        str = getMessage("common.enable");
                    } else if (str == 'N') {
                        str = getMessage("common.disable");
                    }
                    $(this).html(str);
                }
            });

            //pageMove Max
            $('.ui-pg-input').on('keyup', function() {
                this.value = this.value.replace(/\D/g, '');
                if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
            });

            //포인터
            $(this).find(".pointer").parent("td").addClass("pointer");

            resizeJqGridWidth("jqgridData", "gridArea");
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event){
            // 제목 셀 클릭시
            if (columnId == 2) {
                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");
                pageMove("/product/" + list.prodSeq);
            }
        }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

function editTimeLmtTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";
    if (val == '0') {
        retVal = "N/A";
    } else if (val != '') {
        retVal = val + " " + getMessage("common.minute");
    } else {
        retVal = val;
    }

    return 'title="' + retVal  + '"';
}

function editLmtCntTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";
    if (val == '0' && val != '') {
        retVal = getMessage("common.unlimit.word");
    } else if (val == '-1' && val != '') {
        retVal = getMessage("common.cannot.used");
    } else {
        if (val != '') {
            retVal = val + getMessage("common.cnt");
        } else {
            retVal = val;
        }
    }

    return 'title="' + retVal  + '"';
}

function editPrcTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = val.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    console.log(retVal);
    if (retVal != "") {
        retVal += getMessage("common.price.unit");
    }

    return 'title="' + retVal  + '"';
}

function editUseYnTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = "";
    if (val == "Y") {
        retVal = getMessage("common.enable")
    } else {
        retVal = getMessage("common.disable")
    }

    return 'title="' + retVal  + '"';
}

function productListDelete(){
    var prodSeqList = "";
    var prodNmList = "";
    var delCnt = 0;

    for (var i = 1; i<$("#limit").val()+1; i++) {
        if ($("#jqg_jqgridData_" + i).prop("checked")) {
            var selProdSeq = $("#jqgridData").jqGrid('getRowData', i).prodSeq;
            var selProdNm = $("#jqgridData").jqGrid('getRowData', i).prodNm;
            prodSeqList += selProdSeq + ",";
            prodNmList += selProdNm + ", "
            delCnt++;
        }
    }

    if (delCnt == 0) {
        popAlertLayer(getMessage("product.delete.not.select.msg"));
        return;
    }

    popConfirmLayer(prodNmList.slice(0, -2) + getMessage("product.delete.confirm.msg"), function() {
        formData("NoneForm" , "prodSeqList", prodSeqList.slice(0, -1));
        callByDelete("/api/product", "productListDeleteSuccess", "NoneForm","deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

productListDeleteSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), function(){location.reload();});
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}

function keywordSearch() {
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

searchFieldSet =function() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');
        var searchHtml = "";
        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name){
            case "prodNm":
                break;
            default:
                continue;
            }
            searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

function moveToRegistView() {
    var searchSvc = "";
    var searchStor = "";

    if ($("#svcSelbox").val() == null) {
        popAlertLayer(getMessage("info.service.nodata.msg"));
        return;
    }

    if ($("#storSelbox").val() == null) {
        popAlertLayer(getMessage("info.store.nodata.msg"));
        return;
    }

    searchSvc = "?svcSeq=" + $("#svcSelbox").val();
    searchStor = "&storSeq=" + $("#storSelbox").val();

    pageMove("/product/regist" + searchSvc + searchStor);
}