/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    productInfo();
});

productInfo = function() {
    callByGet("/api/product?prodSeq=" + $("#prodSeq").val() , "productInfoSuccess", "NoneForm", "productInfoFail");
}

productInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.productInfo;

        $("#svcNm").html(item.svcNm);
        $("#storNm").html(item.storNm);
        $("#prodNm").html(item.prodNm);

        $("#storSeq").val(item.storSeq);

        var prodLmtCnt = item.prodLmtCnt;
        if (prodLmtCnt == 0) {
            prodLmtCnt = getMessage("common.unlimit.word");
        } else if (prodLmtCnt == -1) {
            prodLmtCnt = getMessage("common.cannot.used");
        } else {
            prodLmtCnt += " " + getMessage("common.cnt");
        }
        $("#prodLmtCnt").html(prodLmtCnt);

        var prodTimeLmt = parseInt(item.prodTimeLmt);
        var timeStr = "";
        if (prodTimeLmt == 0) {
            timeStr = getMessage("common.unlimit.uncount.msg"); //"N/A" ==> 제한없음
        } else {
            timeStr = prodTimeLmt + getMessage("common.minute");
        }

        $("#prodTimeLmt").html(timeStr);

        var prodPrc = item.prodPrc.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
        if (prodPrc != "") {
            prodPrc += " " + getMessage("common.price.unit");
        }
        $("#prodPrc").html(prodPrc);

        var prodDesc = item.prodDesc;
        prodDesc = prodDesc.replace('&lt;br /&gt;',"<br />");
        prodDesc = prodDesc.replace(/<br\s?\/?>/g,"<br />");
        $("#prodDesc").html(prodDesc);

        $("#gsrProdCode").html(item.gsrProdCode);

        if (item.useYn == 'Y') {
            $("#useYn").html(getMessage("common.enable"));
        } else {
            $("#useYn").html(getMessage("common.disable"));
        }

        var productCategory = "";
        $(data.result.productCategory).each(function(i,item) {
            if (item.lmtCnt == -1) {
                var setLimitTxt = getMessage("common.unlimit.msg");
                if (item.deduction == "N") {
                    setLimitTxt = getMessage("common.unlimit.uncount.msg");
                }
                $(".prodCtgSet").append("<li><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.devCtgNm + "</span>[" + setLimitTxt + "]</li>");
            } else if (item.lmtCnt == 0) {
                $(".prodCtgSet").append("<li><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.devCtgNm + "</span>[" + getMessage("common.unavailable") + "]</li>");
            } else {
                $(".prodCtgSet").append("<li><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.devCtgNm + "</span>[" + item.lmtCnt + getMessage("common.cnt") + "]</li>");
            }
            /*
            if(item.lmtCnt == 0){
                $(".prodCtgSet").append("<li><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.devCtgNm + "</span>[" + getMessage("common.unavailable") + "]</li>");
            }else{
                $(".prodCtgSet").append("<li><span class='categoryName' style='width: 220px;display: inline-block;'>" + item.devCtgNm + "</span>[" + item.lmtCnt + getMessage("common.cnt") + "]</li>");
            }
            */
        });

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/product");
    }
}

productInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/product");
}

productDelete = function() {
    popConfirmLayer($("#prodNm").text() + getMessage("product.delete.confirm.msg"), function() {
        formData("NoneForm" , "prodSeqList", $("#prodSeq").val());
        callByDelete("/api/product", "productDeleteSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

productDeleteSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/product");
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}