/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var gsrProdCode;
var productCategory;

$(document).ready(function() {
    productInfo();

    $("#prodPrc").keyup(function(e) {
        $(this).val($("#prodPrc").val().replace(/[^\d]+/g, ''));
        $(this).val(numberWithCommas($("#prodPrc").val()));
    });

    limitInputTitle("prodNm,prodDesc,gsrProdCode");

    $('.remainingPrc').each(function() {
        var $maximumCount = $(this).attr("max") * 1;
        var $input = $(this);
        var update = function() {
            var now = $maximumCount - $input.val().length;
            // 사용자가 입력한 값이 제한 값을 초과하는지를 검사한다.
            if (now < 0) {
                var str = $input.val();
                popAlertLayer(getMessage("product.price.limit.msg"));
                $input.val(str.substr(0, $maximumCount));
                now = 0;
            }
        }
        $input.bind('input keyup paste', function() {
            setTimeout(update, 0)
        });
        update();
    });

    $("#userSet").bind('input keyup paste', function() {
        if ($("#userSet").val() >= 1441) {
            popAlertLayer(getMessage("msg.reservate.too.setTime"));
            $("#userSet").val("");
        } else if ($("#userSet").val() <= 0) {
            popAlertLayer(getMessage("msg.device.zero.setTime"));
            $("#userSet").val("");
        }
    });

    $("#userProdLmtCnt").bind('input keyup paste', function() {
        if ($("#userProdLmtCnt").val() >= 99) {
            popAlertLayer(getMessage("msg.reservate.too.setLimitCount"));
            $("#userProdLmtCnt").val("");
        } else if ($("#userProdLmtCnt").val() <= 0) {
            popAlertLayer(getMessage("msg.reservate.zero.setLimitCount"));
            $("#userProdLmtCnt").val("");
        }
    });

    setkeyup();
});

productInfo = function() {
    callByGet("/api/product?prodSeq=" + $("#prodSeq").val() , "productInfoSuccess", "NoneForm", "productInfoFail");
}

productInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.productInfo;

        $("#prodNm").val(xssChk(item.prodNm));
        $("#svcNm").html(item.svcNm);
        $("#storNm").html(item.storNm);

        $("#storSeq").val(item.storSeq);

        if (item.prodLmtCnt > 5 && item.prodLmtCnt < 99 ) {
            $("#prodLmtCnt option[value=userSet]").attr('selected', 'selected');
            $("#userProdLmtCnt").val(item.prodLmtCnt);
            chkProdLmtCnt();
        } else {
            $("#prodLmtCnt > option[value=" + item.prodLmtCnt + "]").attr("selected", "true");
        }

        var prodTimeLmt = item.prodTimeLmt;
        var h = parseInt(prodTimeLmt / 60);
        var m = parseInt(prodTimeLmt % 60);
        m += (h*60);
        $("#prodTimeLmt").val(m).attr("selected","selected");
        if (m != 9999 && $("#prodTimeLmt option:selected").index() == 0 && m != 0) {
            $("#prodTimeLmt option[value=userSet]").attr('selected','selected');
            $("#userSet").val(m);
            uncountSel();
        }


        $("#prodPrc").val(item.prodPrc.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,'));

        var prodDesc = item.prodDesc;
        prodDesc = prodDesc.replace(/<br\s?\/?>/g,"\n");

        $("#prodDesc").val(xssChk(prodDesc));

        productCategory = data.result.productCategory;

        $(data.result.productCategory).each(function(i, item) {
            $(".categoryName").each(function(j) {
                if($(".prodCategory li:eq(" + j + ") input.categoryName").val() == item.devCtgNm){
                    $(".prodCategory li:eq(" + j + ")").find("input.categoryCount").val(item.lmtCnt);
                    $(".prodCategory li:eq(" + j + ")").find("input.categorySeq").val(item.prodCtgSeq);
                    if(item.lmtCnt == 0){
                        categoryState($(".prodCategory li:eq(" + j + ")").find(".stateProduct").val(2));
                    }
                }
            })
        });

        $("input.categoryCount").each(function(i) {
            if ($(this).val() == 0 && $(this).parents("li").find(".stateProduct").val() == 0) {
                categoryState($(this).parents("li").find(".stateProduct").val(1));
            }
        });

        $("#gsrProdCode").val(item.gsrProdCode);
        gsrProdCode = item.gsrProdCode;
        $("input[name='useYn'][value="+item.useYn+"]").attr("checked",true);

        useCategoryInfo(item.storSeq);

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/product");
    }
}

productInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/product");
}

useCategoryInfo = function(seqVal) {
    $(".prodCategory").empty();
    var searchStorSeq = "?storSeq=" + seqVal;
    callByGet("/api/productContsCategory" + searchStorSeq, "productUseCategoryInfoSuccess", "NoneForm");
}

productUseCategoryInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var useCatorgyListHtml = "";
        $(data.result.productContsCategoryList).each(function(i, item) {
            useCatorgyListHtml += "<li>";
            useCatorgyListHtml += "<input type='hidden' class='categoryName' value='" + item.secondCtgNm + "'/>";
            useCatorgyListHtml += "<input type='hidden' class='categorySeq' value='0' />";
            useCatorgyListHtml += "<span class='device'>" + item.secondCtgNm + "</span>";
            useCatorgyListHtml += "<span class='useCnt'>" + getMessage("common.useCnt") + "</span>";
            useCatorgyListHtml += "<span class='pad10'><input type='text' class='categoryCount onlynum inputBox lengthSM' value='0' readonly='readonly'></span>";
            useCatorgyListHtml += "<span class='pad10'><select class='stateProduct inputBox lengthSM' onchange='categoryState(this)'>"
                                    + "<option value=0>" + getMessage("common.limit.count") + "</option>"
                                    + "<option value=1>" + getMessage("common.unlimit.msg") + "</option>"
                                    + "<option value=2>" + getMessage("common.unlimit.uncount.msg") + "</option>"
                                    + "<option value=3 selected='selected'>" + getMessage("common.cannot.used") + "</option>"
                                    + "</select></span>"
            useCatorgyListHtml += "</li>";
        });
        $(".prodCategory").append(useCatorgyListHtml);
        // 동적으로 카테고리 추가하여 .onlynum에 대한 재설정
        uncountSel();
        chkProdLmtCnt();
        keyup();
        initCategory();
    }
}

initCategory = function() {
    $(productCategory).each(function(i, item) {
        $(".categoryName").each(function(j) {
            if ($(".prodCategory li:eq(" + j + ") input.categoryName").val() == item.devCtgNm) {
                if (item.lmtCnt == -1) {
                    $(".prodCategory li:eq(" + j + ")").find("input.categoryCount").val(0);
                    $(".prodCategory li:eq(" + j + ")").find("input.categorySeq").val(item.prodCtgSeq);
                    if (item.deduction == "N") {
                        categoryState($(".prodCategory li:eq(" + j + ")").find(".stateProduct").val(2));
                    } else {
                        categoryState($(".prodCategory li:eq(" + j + ")").find(".stateProduct").val(1));
                    }

                } else if (item.lmtCnt == 0) {
                    $(".prodCategory li:eq(" + j + ")").find("input.categoryCount").val(item.lmtCnt);
                    $(".prodCategory li:eq(" + j + ")").find("input.categorySeq").val(item.prodCtgSeq);
                    categoryState($(".prodCategory li:eq(" + j + ")").find(".stateProduct").val(3));
                } else {
                    $(".prodCategory li:eq(" + j + ")").find("input.categoryCount").val(item.lmtCnt);
                    $(".prodCategory li:eq(" + j + ")").find("input.categorySeq").val(item.prodCtgSeq);
                    categoryState($(".prodCategory li:eq(" + j + ")").find(".stateProduct").val(0));
                }
            }
        });
    });

    $("input.categoryCount").each(function(i) {
        if ($(this).val() == 0 && $(this).parents("li").find(".stateProduct").val() == 0) {
            categoryState($(this).parents("li").find(".stateProduct").val(1));
        }
    });
}

categoryState = function(e) {
    if ($(e).val() == 0) {
        $(e).parents("li").children("span:eq(2)").find("input.categoryCount").attr("readonly",false);
    } else if ($(e).val() == 1 || $(e).val() == 2) {
        $(e).parents("li").children("span:eq(2)").find("input.categoryCount").attr("readonly",true);
    } else {
        $(e).parents("li").children("span:eq(2)").find("input.categoryCount").attr("readonly",true);
    }
}

function numberWithCommas(x) {
    return x.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function productsEdit(){
    if ($("#prodNm").val().trim() == "") {
        popAlertLayer(getMessage("product.use.prodNm.empty.msg"));
        return;
    }

    var playCount = $("#prodLmtCnt").val();

    if ($("#prodLmtCnt").val() == "userSet") {

        if ($("#userProdLmtCnt").val().trim() == "") {
            popAlertLayer(getMessage("msg.product.no.limitCount"));
            return;
        }

        playCount = $("#userProdLmtCnt").val().trim();
    }

    if ($("#prodTimeLmt").val() == "userSet") {
        if ($("#userSet").val().trim() == "") {
            popAlertLayer(getMessage("msg.product.no.useTime"));
            return;
        }
        playTime = $("#userSet").val().trim();
    }

    if ($("#prodPrc").val().trim() == "") {
        popAlertLayer(getMessage("product.price.empty.msg"));
        return;
    }

    if ($("#gsrProdCode").val().trim() == "") {
        popAlertLayer(getMessage("product.prod.gsr.code.empty.msg"));
        return;
    }

    if ($("#gsrProdCode").val().trim().length < 13) {
        popAlertLayer(getMessage("product.invalid.prod.code.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        var selCategorys = "";
        var selCategoryCounts = "";
        var selCategorySeq = "";
        var selDeduction = "";

        $(".categoryName").each(function(i) {
            if ($(this).parents("li").find("input.categoryCount").val() != "" /*&& $(this).parents("li").find(".stateProduct").val() != 1*/) {
                if (selCategorys != "") {
                    selCategorys += ",";
                    selCategoryCounts += ",";
                    selCategorySeq += ",";
                    selDeduction += ",";
                }
                selCategorys += $(this).val();
                if ($(this).parents("li").find(".stateProduct").val() == 1) {
                    selCategoryCounts += -1;
                    selDeduction += "Y";
                } else if ($(this).parents("li").find(".stateProduct").val() == 2) {
                    selCategoryCounts += -1;
                    selDeduction += "N";
                } else if ($(this).parents("li").find(".stateProduct").val() == 3) {
                    selCategoryCounts += 0;
                    selDeduction += "Y";
                } else {
                    selCategoryCounts += $(this).parents("li").find("input.categoryCount").val();
                    selDeduction += "Y";
                }
                selCategorySeq += $(this).parents("li").find("input.categorySeq").val();
            }
        });
        var playTime = $("#prodTimeLmt").val();
        if ($("#prodTimeLmt").val() == "userSet") {
            if ($("#userSet").val().trim() == "") {
                popAlertLayer(getMessage("msg.product.no.useTime"));
                return;
            }
            playTime = $("#userSet").val().trim();
        }

        var prodPrc = $("#prodPrc").val().replace(/[^\d]+/g, '');
        var prodDesc = $("#prodDesc").val();
        prodDesc = prodDesc.replace(/(?:\r\n|\r|\n)/g, '<br />');

        formData("NoneForm", "selCategorys", selCategorys);
        formData("NoneForm", "selCategoryCounts", selCategoryCounts);
        formData("NoneForm", "selCategorySeq", selCategorySeq);
        formData("NoneForm", "selDeduction", selDeduction);

        formData("NoneForm", "prodSeq", $("#prodSeq").val());
        formData("NoneForm", "prodNm", $("#prodNm").val());
        formData("NoneForm", "prodDesc", prodDesc);
        formData("NoneForm", "prodTimeLmt", playTime);
        formData("NoneForm", "prodLmtCnt", playCount);
        formData("NoneForm", "prodPrc", prodPrc);
        formData("NoneForm", "storSeq", $("#storSeq").val());

        if(gsrProdCode != $("#gsrProdCode").val()) {
            formData("NoneForm", "gsrProdCode", $("#gsrProdCode").val());
        } else {
            formData("NoneForm", "gsrProdCode", "");
        }
        formData("NoneForm", "useYn", $("input[name='useYn']:checked").val());

        callByPut("/api/product" , "productUpdateSuccess", "NoneForm","updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

productUpdateSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/product");
    } else if (data.resultCode == "1011"){
        popAlertLayer(getMessage("product.exist.prod.gsr.code.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function createDate(object,start,last){
    object.append('<option value=0>' + getMessage("common.unlimit.msg") + '</option>');
    for (i = start; i < last; i++) {
        var time = i;
        object.append('<option value=' + i*60 + '>' + time  + ' ' + getMessage("common.time") + '</option>');
    }
}

uncountSel = function() {
    if ($("#prodTimeLmt").val() == "0") {
        $(".stateProduct[value=2]").val(1);
        $(".stateProduct option[value=2]").attr("disabled",true);
        $("#userSetBox").hide();
    } else {
        if ($("#prodTimeLmt").val() == "userSet") {
            $("#userSetBox").show();
        } else {
            $("#userSetBox").hide();
        }
        $(".stateProduct option[value=2]").attr("disabled",false);
    }
}

var chkProdLmtCnt = function () {

    if ($("#prodLmtCnt").val() == "userSet") {
        $("#userSetProdLmtCntBox").show();
    } else {
        $("#userSetProdLmtCntBox").hide();
    }

};
