/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var Citem_ori;
var ratContNo_ori;
var fnsDt_ori;
$(document).ready(function(){
    billingInfo();
    $("#ratTime").attr( 'disabled', true );
    $(".content").find("input[type=text]").attr('disabled',true);
    $(".content").find("input[type=radio]").attr('disabled',true);

    listBack($(".btnList"));
});


billingInfo = function(){
    callByGet("/api/serviceContract?ratSeq="+$("#ratSeq").val(), "billingInfoSuccess", "NoneForm", "billingInfoFail");
}

billingInfoSuccess = function(data) {
    if(data.resultCode == "1000") {
        var item = data.result.serviceContractDetail;
        var marketNm = data.result.marketNm;

        $("#marketNm").text(marketNm);
        $("#ratContNo").text(item.ratContNo);
        var titleObj = item.contsTitle.split(",");
        var titleHtml = "";
        $(titleObj).each(function(i){
            titleHtml += "<span style='display: inline-block; float: left;'>";
            titleHtml += titleObj[i];
            if(i != titleObj.length - 1) {
                titleHtml += ",&nbsp;";
            }
            titleHtml += "</span>";
        });

        $("#contractConts").html(titleHtml);
        $("#ratContStDt").val(item.ratContStDt);
        $("#ratContFnsDt").val(item.ratContFnsDt);
        $("input[name=ratYn][value="+item.ratYn+"]").attr("checked",true);

        if (item.ratYn == "N") {
            $("input[type=radio][value=N]").attr("checked",true);
        } else {
            $("#runLmtCnt").val(item.runLmtCnt);
            $("#runPerPrc").val(item.runPerPrc);
            $("#ratTime").val(item.ratTime);
            $("#ratPrc").val(item.ratPrc);
            $("#lmsmpyPrc").val(item.lmsmpyPrc);


            switch(item.ratContType) {
            //계약 타입 [ 횟수 ]
            case "C":
                $("input[type=radio][name=ratCntYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratCntYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            //계약 타입 [ 일시금 ]
            case "L":
                $("input[type=radio][name=ratLumpYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratLumpYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            //계약 타입 [ 시간 ]
            case "T":
                $("input[type=radio][name=ratTimeYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratTimeYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            }
        }

        $(".remainingPrc").each(function(e) {
            $(this).val($(this).val().replace(/[^\d]+/g, ''));
            $(this).val(numberWithCommas($(this).val()));
        });
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/service/contract");
    }
}

billingInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/service/contract");
}

function numberWithCommas(x) {
    return x.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}