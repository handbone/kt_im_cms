/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var totalCount;
var checkedName;
var oriName;
var arr = new Array();
var str_hash = document.location.hash.replace("#","");
var hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
var hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
$(document).ready(function(){
    limitInputTitle("frmtnNm");

    setkeyup();
    checkboxChange();
    svcList();

    /* list 클릭 시, 옵션값 변경*/
    $("body").click(function( event ) {
        var table = $(event.target).parents("table");
        var tContent = $(event.target).parent().parent();
        if(table.hasClass("listBox") == false){
            return;
        }
        if(event.target.nodeName == "TD" && tContent.is("tbody") || ($(event.target).parents("td").hasClass("contsTitle") && !$(event.target).hasClass("btnSmallGray"))){
            var target = $(event.target).parents("tr").toggleClass("checked");
            var type = false;
            if(target.hasClass("checked")) type = true;
            $(target).find("input").attr("checked",type);
        }else{
            if(tContent.is("thead")){
                table.find("input.allcheck").trigger("click");
            }
        }
    });



});

svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
            svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);
        if (hashSvc != "" && hashSvc != null) {
            $("#svcList > option[value=" + hashSvc + "]").attr("selected","true");
            hashSvc = "";
        }
        storList();
    } else {
        popAlertLayer(getMessage("common.bad.request.msg"), makeBakMove());
    }
}

storList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"storListSuccess","NoneForm");
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        checkedName = "";
        var memberStoreListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(memberStoreListHtml);
        $("#storList").attr("disabled", false);

        if (hashStore != "" && hashStore != null) {
            $("#storList > option[value=" + hashStore + "]").attr("selected","true");
            hashStore = "";
        }

        getContentsInfo();

        $("input").unbind( "click" );
    }  else {
        $("#storList").html("");
        $("#storList").attr("disabled", true);
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("#selectContents").html("");
        $("#selectVRContents").html("");
        $("input:not(.btnCancel):not(.btnLogout)").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });

    }
}

releaseOverlap = function(){
    var url = "/api/mms/contents";
    var str = $.trim($("#frmtnNm").val());

    if (str == "") {
        popAlertLayer(getMessage("cms.release.msg.Insert.Nm"));
        return;
    }

    if (str == "basic") {
        $("#overlap").next().text(getMessage("msg.contents.frmtnNm.not.basic"));
        return;
    }

    if ($("#storList").val() != 0) {
        formData("NoneForm", "storSeq", $("#storList").val());
        formData("NoneForm", "frmtnNm", str);
    } else {
        formData("NoneForm", "frmtnNm", str);
    }

    if(oriName != str  || str == 'basic'){
        callByGet(url, "checkOverlapSuccess", "NoneForm");
    }else{
        checkedName = str;
        popAlertLayer(getMessage("cms.release.msg.use.success.Nm"));
    }
    formDataDeleteAll("NoneForm");
}

checkOverlapSuccess = function(data){
    var result = data.resultCode;
    if(result == "1000"){
        $("#overlap").next().text(getMessage("cms.release.msg.exist.Nm"));
    }else{
        $("#overlap").next().text("");
        popAlertLayer(getMessage("cms.release.msg.use.success.Nm"));
        checkedName = $.trim($("#frmtnNm").val());
    }
}

/*
 * 콘텐츠 목록 체크박스 또는 client 콘텐츠 목록이 변경 될 경우
 * parameter: type(구분자-content, client 콘텐츠)
 * return: x
 */
function checkboxChange(type){
    var allCheck = false;

    // 콘텐츠 목록 / client 콘텐츠 목록 체크 박스 바뀔 경우
    if(type == "contents" || type == "vrContents"){
        allCheck = true;
        $("input:checkbox[name="+type+"Seq]").each(function(){
            if(!this.checked){
                allCheck = false;
                return;
            }
        });
    }
    if(allCheck){
        $("#"+type+"AllCb").attr("checked", true);
    } else{
        $("#"+type+"AllCb").attr("checked", false);
    }
}



/*
* 콘텐츠 목록 / client 콘텐츠 전체 체크 변경될 경우
* parameter: type(구분자-content, client 콘텐츠)
* return: x
*/
function checkboxAll(type){
    if($("#"+type+"AllCb").is(":checked")){
        $("input:checkbox[name="+type+"Seq]").attr("checked", true).parents("tr").addClass("checked");
        $(".hide").removeClass("checked");
    } else {
        $("input:checkbox[name="+type+"Seq]").attr("checked", false).parents("tr").removeClass("checked");
    }
}

//컨텐츠 전체 정보 가져오기
function getContentsInfo(){
    arr = new Array();

    var searchSvc = $("#svcList option:selected").val();
    if(searchSvc != 0){
        searchSvc = "?svcSeq="+$("#svcList option:selected").val()+"&sttus=05&rows=10000&delYn=N&cntrctFnsDt=NOW";
    }else{
        searchSvc = "?rows=10000&sttus=05&delYn=N&cntrctFnsDt=NOW";
    }
    $("input[name=_method]").val("GET");

    callByGet("/api/mms/contents"+searchSvc, "getContentsInfoSuccess");
}

function getContentsInfoSuccess(data){
    if(data.resultCode == "1000"){
        var contentsHtml = "";

        totalCount = data.result.totalCount;

        $(data.result.contentsList).each(function(i,item){
            contentsHtml += "<tr id=\"contentDetail"+(i+1)+"\"><td class=\"chkBox width50\"><input type=\"checkbox\" value=\""+item.contsSeq+"\" name=\"contsSeq\" style='visibility:hidden;'></td>";
            contentsHtml += "<td class=\"contsTitle\"><div class='ellipsis'>"+item.contsTitle+"</div><button class=\"btnSmallGray btnSmallGrayCustom\" onclick=\"contsInfoPopup("+item.contsSeq+")\">상세보기</button></td>";
            contentsHtml += "<td class=\"itemW30 contsCtgNm align_l\">["+item.firstCtgNm+"] "+item.secondCtgNm+"</td>";
            contentsHtml += "<td class=\"cretDt itemW20\">"+item.cretDt+"</td></tr>";
            arr.push({contsTitle: item.contsTitle,contsSeq: item.contsSeq,firstCtgNm:item.firstCtgNm,secondCtgNm : item.secondCtgNm});
        });

        $("#selectContents").html(contentsHtml);

        if($("#frmtnSeq").val() != "0"){
            getReleaseContentsInfo();
        }
    }else{
        popAlertLayer(getMessage("cms.release.msg.noData.conts"),"/mms/contents#page=1&storSeq="+$("#storList").val()+"&svcSeq="+$("#svcList").val()+"&type=frmtn");
    }

    checkContent();
}

//해당 VR에 컨텐츠 정보 삭제
function deleteContentsInfo(){
    if ($("#storList option").index() == -1) {
        popAlertLayer(getMessage("common.bad.request.msg"));
        return;
    }
    $(".noData").remove();

    $("input:checkbox[name=groupContentsSeq]:checked").each(function(){
        $("input[type=checkbox][value="+this.value+"]").parents("tr").removeClass("hide").show();
        $("input[type=checkbox][value="+this.value+"]").prop("checked",false);
        this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
    });
    $("#groupContentsAllCb").attr("checked", false);
    if($("#selectVRContents").find("tr").text() == ""){
        $("#selectVRContents").html("<tr class='noData'><td colspan=3 style='text-align:center !important' height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
    }

    checkScroll();
}


//해당 VR에 컨텐츠 정보 등록
function insertContentsInfo(){
    if ($("#storList option").index() == -1) {
        popAlertLayer(getMessage("common.bad.request.msg"));
        return;
    }
    $("#selectVRContents").find("tr").each(function(){
        if ($(this).text() == "") {
            $(this).remove();
        }
    });

    var VRContentsHtml = "";
    var contsSeq = "";
    var contsTitle = "";
    var ctgNm ="";
    var cretDt = "";
    var insertFlag;
    for(var num=1; num<totalCount+1; num++){
        if($("#contentDetail"+num+" input:checkbox:checked").val()){
            insertFlag = true;
            contsSeq = $("#contentDetail"+num+" input:checkbox").val();
            contsTitle = $("#contentDetail"+num+" .contsTitle .ellipsis").html();
            ctgNm = $("#contentDetail"+num+" .contsCtgNm").html();
            cretDt = $("#contentDetail"+num+" .cretDt").html();

            $("input:checkbox[name=groupContentsSeq]").each(function(){
                if(contsSeq == $(this).val()){
                    insertFlag = false;
                    return;
                }
            });

            if(insertFlag){
                VRContentsHtml += "<tr class='rowConts'><td class=\"chkBox width50\"><input type=\"checkbox\" value=\""+contsSeq+"\" name=\"groupContentsSeq\" style='visibility:hidden;'></td>";
                VRContentsHtml += "<td class=\"contsTitle\">"+contsTitle+"</td>";
                VRContentsHtml += "<td class=\"itemW30 contsCtgNm align_l\">"+ctgNm+"</td>";
            }
        }
    }
    $("#selectContents > .checked").hide().removeClass("checked").addClass("hide");

    if (VRContentsHtml == "" && $("#selectVRContents").find(".noData").index() == 0) {
        return;
    } else {
        $(".noData").remove();
        $("#selectVRContents").find("tr").each(function(){
            if ($(this).text() == "") {
                $(this).remove();
            }
        });
        if($("#selectVRContents").find("tr").text() == getMessage("cms.release.msg.noData") && VRContentsHtml.length > 0){
            $("#selectVRContents").html("");
         }

        $("#contsAllCb").prop("checked",false);
        $("#selectVRContents").append(VRContentsHtml);
    }

    checkScroll();
}

//적용하기
function applyRelease(){
    var str = $.trim($("#frmtnNm").val());

    if (str == "") {
        popAlertLayer(getMessage("cms.release.msg.Insert.Nm"));
        return;
    }

    if ($("#selectVRContents tr.rowConts").length == $("#selectVRContents .hide").length) {
        popAlertLayer(getMessage("cms.release.msg.select.conts"));
        return;
    }

    if(checkedName == $.trim($("#frmtnNm").val())){
        popConfirmLayer(getMessage("common.regist.msg"), function() {
            formData("NoneForm" , "frmtnNm", $("#frmtnNm").val());
            formData("NoneForm" , "storSeq", $("#storList option:selected").val());

            var contsSeqs = "";
            $("#selectVRContents tr.rowConts").each(function(){
                if ($(this).css("display") != "none") {
                    contsSeqs += $(this).find("input").val() + ",";
                }
            });

            formData("NoneForm" , "contsSeqs", contsSeqs.slice(0,-1));
            callByPost("/api/mms/release" , "applyReleaseContsInfoSuccess", "NoneForm","InsertFail");
        }, null, getMessage("common.confirm"));
    }else{
        popAlertLayer(getMessage("cms.release.msg.check.frmtnNm"));
    }

}

InsertFail = function(data){
    popAlertLayer(getMessage("fail.common.insert"));
    formDataDeleteAll("NoneForm");
}

function applyReleaseContsInfoSuccess(data){
    formDataDelete("NoneForm", "contsSeqs");
    formDataDelete("NoneForm", "frmtnSeq");
    formDataDelete("NoneForm", "frmtnNm");
    formDataDelete("NoneForm", "svcSeq");
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.insert"), "/mms/contents#page=1&svcSeq="+$("#svcList").val()+"&type=frmtn&storSeq="+$("#storList").val());
    }
}


function checkContent(){
    $(".noData").remove();
    $("tbody tr").each(function(){
        if ($(this).html() == "") {
            $(this).remove();}
        }
    );
    $("#selectVRContents tr.rowConts").each(function(){
        var obj = $("#selectContents input[value='"+$(this).find(".chkBox input").val()+"']");
        if ($(obj).index() == -1) {
            $(this).addClass("hide").hide();
        } else {
            $(this).removeClass("hide").show();
        }
    });

    $("#selectVRContents tr.rowConts").each(function(){
        $("#selectContents input[value='"+$(this).find(".chkBox input").val()+"']").prop("checked",false).parents("tr").addClass("hide").hide();
    });

    if($("#selectVRContents tr.rowConts").length == $("#selectVRContents .hide").length){
        $("#selectVRContents").append("<tr class='noData'><td colspan=3 style='text-align:center !important' height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
    }

    checkScroll();
}

function checkScroll(){
    $(".scroll-table").each(function(i){
        if (i == 0) {
            console.log($(this).width());
            if ($(this).find(".tableList").height() > 311) {
                $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"212px"});
                $(this).find(".tableList").children("thead").find("th:eq(3)").css({"width":"158px","padding-right":"16px"});
            } else {
                $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"30%"});
                $(this).find(".tableList").children("thead").find("th:eq(3)").css({"width":"20%","padding-right":"0px"});
            }
        } else {
            if ($(this).find(".tableList").height() > 311) {
                $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"300px","padding-right":"16px"});
            } else {
                $(this).find(".tableList").children("thead").find("th:eq(2)").css({"width":"40%","padding-right":"0px"})
            }
        }

    })
}

searchOnConts = function(){
    var searchConts = $("#searchKeyword").val();
    var filter = searchConts.toUpperCase();

    $("#selectContents").find("tr").each(function(){
        if (!$(this).hasClass("hide")) {
            if ($(this).find(".ellipsis").text().toUpperCase().indexOf(filter) > -1) {
                $(this).css("display","inline-table");
            } else {
                $(this).css("display","none");
            }
        }
    })

    checkScroll();
}

function searchOnContsReset() {
    $("#searchKeyword").val("");
    searchOnConts();
}

function makeBakMove(){
    var str_hash = document.location.hash.replace("#","");
    var bakStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
    var bakSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
    pageMove("/mms/contents#type=frmtn&svcSeq="+bakSvc+"&storSeq="+bakStore);
}

