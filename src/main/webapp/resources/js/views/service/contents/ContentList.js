/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var searchField;
var searchString;
var readyPage = false;
var apiUrl = makeAPIUrl("/api/mms/contents");
var lastBind = false;
var gridState;
var hashSvc;
var hashStore;
var selTab =0;
var hashType;
var totalPage = 0;

$(window).resize(function(){
    $("#jqgridData").setGridWidth($("#contents").width());
})
$(window).ready(function(){
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
            hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));
            hashType = checkUndefined(findGetParameter(str_hash,"type"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            $("#svcList").val(hashSvc);
            $("#type").val(hashType);
            setSvcList();

        } else {
            gridState = "READY";
        }
    });
})
$(document).ready(function(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashType = checkUndefined(findGetParameter(str_hash,"type"));
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#type").val(hashType);
        $("#keyword").val(searchString);

        if (hashType == "frmtn") {
            selTab = 1;
        }

        gridState = "NONE";
    }

    svcList();

    $("#keyword").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });


});

function setStorList(){
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        hashStore = checkUndefined(findGetParameter(str_hash,"storSeq"));
        hashSvc = checkUndefined(findGetParameter(str_hash,"svcSeq"));

        $("#svcList").val(hashSvc);
        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }

        if (hashStore == "null") {
            hashStore = "";
            popAlertLayer(getMessage("info.store.nodata.msg"));
            $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox']").bind("click", function(e) {
                popAlertLayer(getMessage("common.bad.request.msg"));
            });
            $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
        } else {
            storList(hashStore);
            $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").unbind( "click" );
        }

        gridState = "NONE";
    } else {
        storList(-1);
    }
}

function keywordSearch(){
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

function searchFieldSet(){
    if ($("#target > option").length == 0) {

        var searchHtml = "";

        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');
        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name) {
            case "frmtnNm":break;
            case "contsTitle":break;
            case "cretrNm":break;
            default:continue;
            }
            searchHtml += "<option value=\""+colModel[i].index+"\">"+colNames[i]+"</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}


svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
//            if (item.storCount != 0) {
            svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
//            }
        });
        $("#svcList").html(svcListHtml);

        if ($("#memberSec").val() == "04" || $("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#svcList").attr('disabled', 'true');
        }
        setStorList();
    } else {
        storList();
    }
}

var stateVal;
storList = function(val){
    stateVal = val;
    var svcListVal = $("#svcList").val();
    if (svcListVal == null) {
        svcListVal = -1
    }
    callByGet("/api/store?searchType=list&svcSeq="+svcListVal,"storListSuccess","NoneForm");
}
var noStorData = false;
storListSuccess = function(data){
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(storeListHtml);
        $("#storList").attr("disabled", false);
        if ($("#memberSec").val() == "05" ) {
            // 04 : 서비스 관리자, 05 : 매장 관리자, 서비스 변경 불가
            $("#storList").attr('disabled', 'true');
        }

        if (stateVal != -1) {
            $("#storList").val(stateVal);
        }

        if (!readyPage) {
            readyPage = true;
            tabSet();
            contentList();
        } else {
            jQuery("#jqgridData").trigger("reloadGrid");
        }
        $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").unbind( "click" );
    } else {
        $("#storList").html("");
        $("#storList").attr("disabled", true);
        popAlertLayer(getMessage("info.store.nodata.msg"));
        $("input:not(.btnCancel):not(.btnLogout):not([type='checkbox'])").bind("click", function(e) {
            popAlertLayer(getMessage("common.bad.request.msg"));
        });
        jQuery('#jqgridData').jqGrid('clearGridData');
        noStorData = true;
        tabSet();
        $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    }
}

function tabVal(seq,e){
    gridState = "TABVAL";

    if ($(e).parent("div").hasClass("tabOn")) {
        return;
    }

    $(".conTitleGrp div").removeClass("tabOn");
    $(e).parent("div").addClass("tabOn").css({ 'pointer-events': 'none' });
    selTab = seq;

    $("#target").val("");
    $("#keyword").val("");

    totalPage = 0;

    jQuery("#jqgridData").jqGrid('GridUnload');
    tabSet();
    $("#target").html("");
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
}

modelSet = function(){
    var ccm = $("#jqgridData").getGridParam();

    tabSet();

    if (selTab == 0) {
        $("#jqgridData").jqGrid("hideCol", ["cb"]);
        $(".CenterGrp .cellTable").hide();
        $(".SyncBtn").hide();
    } else if (selTab == 1) {
        $("#jqgridData").jqGrid("showCol", ["cb"]);
        $(".CenterGrp .cellTable").show();
        $(".SyncBtn").show();
    }

    $("#jqgridData").setGridWidth($("#contents").width());
    $("#jqgridData").jqGrid('setGridParam',ccm);
}

var option_list =[];
var option_common = {
    url:apiUrl,
    datatype: "json", // 데이터 타입 지정
    jsonReader : {
        page: "result.currentPage",
        total: "result.totalPage",
        root: "result.releaseList",
        records: "result.totalCount",
        repeatitems: false
    },
    colNames:[getMessage("column.title.num"),getMessage("column.title.release"),getMessage("store.comn.name"),getMessage("column.title.updateDay"),"frmtnSeq"],
    colModel:[
        {name:"num", index: "num", align:"center", width:20},
        {name:"frmtnNm", index:"FRMTN_NM", align:"center",formatter:pointercursor},
        {name:"storNm", index:"STOR_NM", align:"center"},
        {name:"amdDt", index:"AMD_DT", align:"center"},
        {name:"frmtnSeq", index: "FRMTN_SEQ", align:"center", width:20,hidden:true},
    ],
    sortorder: "desc",
    height: "auto",
    autowidth: true, // width 자동 지정 여부
    rowNum: 10, // 페이지에 출력될 칼럼 개수
    pager: "#pageDiv", // 페이징 할 부분 지정
    viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
    sortname: "contsSeq",
    multiselect: true,
    beforeRequest:function(){
        tempState = gridState;
        var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

        if (gridState == "TABVAL") {
            searchField = "";
            searchString = "";
        } else if (gridState != "SEARCH") {
            $("#target").val(searchField);
            $("#keyword").val(searchString);
        }

        if (isNaN(page)) {
           page =1;
        }

        if (totalPage > 0 && totalPage < page) {
            page = 1;
        }

        searchField = checkUndefined($("#target").val());
        searchString = checkUndefined($("#keyword").val());
        hashSvc = checkUndefined($("#svcList").val());
        hashStore = checkUndefined($("#storList").val());
        hashType = checkUndefined($("#type").val());

        if (hashType == "" || hashType == "list" || hashType == null) {
            hashType = "list";
            apiUrl = makeAPIUrl("/api/mms/contents");
            selTab = 0;
        } else {
            apiUrl = makeAPIUrl("/api/mms/release");
            selTab = 1;
        }

        modelSet();

        $(".conTitleGrp div").removeClass("tabOn").addClass("tabOff").css({ 'pointer-events': '' });
        $(".conTitleGrp div:eq("+selTab+")").addClass("tabOn").removeClass("tabOff").css({ 'pointer-events': 'none' });


        if (gridState == "HASH"){
            myPostData.page = page;
            tempState = "READY";
        } else {
            if(tempState == "SEARCH"){
               myPostData.page = 1;
            } else {
                tempState = "";
            }
        }

        if(gridState == "NONE"){
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
            myPostData.page = page;

            if (searchField != null) {
                tempState = "SEARCH";
            } else {
                tempState = "READY";
            }
        }

        if(searchField != null && searchField != "" && searchString != ""){
            myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
        }else{
            delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
        }

        gridState = "GRIDBEGIN";

        if (hashSvc != null && hashSvc != "") {
            myPostData.svcSeq = hashSvc;
        } else {
            myPostData.svcSeq = "";
        }

        if (hashStore != null && hashStore != "") {
            myPostData.storSeq = hashStore;
        } else {
            myPostData.storSeq = "";
        }

        myPostData.type = hashType;
        if (noStorData || $("#storList option").index() == -1) {
            if ($("#svcList option").index() == -1) {
                popAlertLayer(getMessage("info.service.nodata.msg"));
            } else if ($("#storList option").index() == -1) {
                popAlertLayer(getMessage("info.store.nodata.msg"));
            }
            myPostData.page = 0;
            hashStore = "";
            noStorData = false;
        }

        $('#jqgridData').jqGrid('setGridParam', {url:apiUrl, postData: myPostData });
    },
    loadComplete: function (data) {
        var str_hash = document.location.hash.replace("#","");
        var page = parseInt(findGetParameter(str_hash,"page"));

        //session
        if(sessionStorage.getItem("last-url") != null){
            sessionStorage.removeItem('state');
            sessionStorage.removeItem('last-url');
        }

        //Search 필드 등록
        searchFieldSet();

        if (data.resultCode == 1000) {
            totalPage = data.result.totalPage;

            if (page != data.result.currentPage) {
                tempState = "";
            }
        } else {
            tempState = "";
        }

        if (hashStore == null){
            hashStore = "";
        }

        /*뒤로가기*/
        var hashlocation = "page="+$(this).context.p.page+"&storSeq="+hashStore+"&svcSeq="+$("#svcList option:selected").val()+"&type="+$("#type").val();

        if($(this).context.p.postData._search && $("#target").val() != null){
            hashlocation +="&searchField="+$("#target").val()+"&searchString="+$("#keyword").val();
        }

        gridState = "GRIDCOMPLETE";
        document.location.hash = hashlocation;

        if (tempState != "" || str_hash == hashlocation) {
            gridState = "READY";
        }

      //pageMove Max
        $('.ui-pg-input').on('keyup', function() {
            this.value = this.value.replace(/\D/g, '');
            if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
        });

        $(this).find(".pointer").parent("td").addClass("pointer");
    },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
    onCellSelect: function(rowId, columnId, cellValue, event){
        // 콘텐츠 ID 선택시
        if(columnId == 2){
          var list = $("#jqgridData").jqGrid('getRowData', rowId);
          sessionStorage.setItem("last-url", location);
          sessionStorage.setItem("state", "view");
          pageMove("/mms/release/"+list.frmtnSeq);
        }
    }
}

contentList = function(){
    $("#jqgridData").jqGrid(option_common).trigger('reloadGrid');
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

setSvcList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"setSvcListSuccess","NoneForm");
}


setSvcListSuccess = function(data){
    if (data.resultCode == "1000") {
        var storeListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            storeListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(storeListHtml);
        $("#storList").val(hashStore);
        jQuery("#jqgridData").trigger("reloadGrid");
    }

}


deleteRelease = function(){
    var frmtnSeq = "";
    var frmtnNms = "";
    for(var i=1; i<$("#limit").val()+1; i++){
        if($("#jqg_jqgridData_"+i).prop("checked")){
            var list = $("#jqgridData").jqGrid('getRowData', i);
            var frmtnNm = $("#jqgridData").jqGrid('getRowData', i).frmtnNm.replace(/(<([^>]+)>)/ig,"");
            if (frmtnNms != "") {
                frmtnNms += ", ";
            }

            frmtnSeq += list.frmtnSeq + ",";
            frmtnNms += frmtnNm;
        }
    }

    if(frmtnSeq == ""){
        popAlertLayer(getMessage("common.select.obj.delete.msg"));
        return;
    }

    popConfirmLayer(frmtnNms+" "+getMessage("common.release.delete.confirm"), function(){
        formData("NoneForm" , "frmtnSeqs", frmtnSeq.slice(0,-1));
        callByDelete("/api/mms/release", "deleteReleaseSuccess", "NoneForm","deleteFail");
    },null,getMessage("common.confirm"));
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
    formDataDeleteAll("NoneForm");
}

deleteReleaseSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"), function(){location.reload();});
    }
}


releaseSync = function(overlap){
    var searchSvc = $("#svcList option:selected").val();
    if(searchSvc != 0){
        searchSvc = "?svcSeq="+$("#svcList option:selected").val();
    }else{
        popAlertLayer(getMessage("msg.contents.select.store"));
        return;
    }

    formDataDeleteAll("NoneForm");
    if(overlap){
        formData("NoneForm", "overlap",overlap);
    }
    formData("NoneForm", "svcSeq", $("#svcList option:selected").val());
    formData("NoneForm", "storSeq", $("#storList option:selected").val());
    callByPost("/api/releaseSync", "releaseSyncSuccess","NoneForm","SyncFail");

}

SyncFail = function(data){
    popAlertLayer(getMessage("fail.request.msg"));
    formDataDeleteAll("NoneForm");
}

releaseSyncSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayerFnc(getMessage("msg.contents.success.sync"), reload);
    }else if(data.resultCode == "1011"){
        var str = "";
        var frmtnSeq = "";
        $(data.result.releaseList).each(function(i,item) {
            if(i != 0 && str != ""){
                str += ",&nbsp;";
                frmtnSeq += ",";
            }
            str += item.frmtnNm;
            frmtnSeq += item.frmtnSeq;
        })
        popConfirmLayer(getMessage("msg.exist.frmtn")+"<br>("+str+")<br>"+getMessage("msg.confirm.frmtnSync")+"<br>"+getMessage("msg.confirm.frmtnSync.cancel"), function(){
            //organizeGroupDelete(groupsSeq);
            releaseSync("Y");
        },function(){
            releaseSync("N");
        });
    }
}

tabSet = function(){
    switch(selTab){
    case 0:
        $("#type").val("list");
        option_common['url'] = makeAPIUrl("/api/contents");
        option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentsList",
            records: "result.totalCount",
            repeatitems: false
        };
        option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.contentNm"),getMessage("column.title.register"),getMessage("column.title.cretDt"),getMessage("column.title.contsSeq")];
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:20},
            {name:"contsTitle", index:"CONTS_TITLE", align:"center",formatter:pointercursor},
            {name:"cretrNm", index:"CRETR_ID", align:"center"},
            {name:"cretDt", index:"CRET_DT", align:"center"},
            {name:"contsSeq", index:"CONTS_SEQ", hidden:true}
        ];
        option_common['sortname'] = "contsSeq";
        option_common['onCellSelect'] = function(rowId, columnId, cellValue, event){
            // 콘텐츠 ID 선택시
            if(columnId == 2){
              var list = $("#jqgridData").jqGrid('getRowData', rowId);
              sessionStorage.setItem("last-url", location);
              sessionStorage.setItem("state", "view");
              pageMove("/mms/contents/"+list.contsSeq);
            }
        }
        break;
    case 1:
        $("#type").val("frmtn");
        option_common['url'] = makeAPIUrl("/api/mms/contents");
        option_common['jsonReader'] = {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.releaseList",
            records: "result.totalCount",
            repeatitems: false
        };
        option_common['colNames'] = [getMessage("column.title.num"),getMessage("column.title.release"),getMessage("table.storeName"),getMessage("column.title.updateDay"),"frmtnSeq"];
        option_common['colModel'] = [
            {name:"num", index: "num", align:"center", width:20},
            {name:"frmtnNm", index:"FRMTN_NM", align:"center",formatter:pointercursor},
            {name:"storNm", index:"STOR_NM", align:"center"},
            {name:"amdDt", index:"AMD_DT", align:"center"},
            {name:"frmtnSeq", index: "FRMTN_SEQ", align:"center", width:20,hidden:true},
        ];
        option_common['sortname'] = "amdDt";
        option_common['onCellSelect'] = function(rowId, columnId, cellValue, event){
            // 콘텐츠 ID 선택시
            if(columnId == 2){
              var list = $("#jqgridData").jqGrid('getRowData', rowId);
              sessionStorage.setItem("last-url", location);
              sessionStorage.setItem("state", "view");
              pageMove("/mms/release/"+list.frmtnSeq);
            }
        }
        break;
    }

    option_common['rowNum'] = $("#limit").val();

}

function makePageMove(e){
    if ($("#storList option").index() != -1) {
        pageMove("/mms/release/regist#svcSeq="+$("#svcList").val()+"&storSeq="+$("#storList").val());
    }

}

