/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
var totalCount;
var checkedName;
var oriName;
var arr = new Array();
$(document).ready(function(){
    setkeyup();
    svcList();
    checkboxChange();
    /* list 클릭 시, 옵션값 변경*/
    $("body").click(function( event ) {
        var table = $(event.target).parents("table");
        var tContent = $(event.target).parent().parent();
        if(table.hasClass("listBox") == false){
            return;
        }
        if(event.target.nodeName == "TD" && tContent.is("tbody")){
            var target = $(event.target).parents("tr").toggleClass("checked");
            var type = false;
            if(target.hasClass("checked")) type = true;
            $(target).find("input").attr("checked",type);
        }else{
            if(tContent.is("thead")){
                table.find("input.allcheck").trigger("click");
            }
        }
    });


});
var storSeq = 0;
svcList = function(){
    callByGet("/api/service?searchType=list","svcListSuccess","NoneForm");
}
svcListSuccess = function(data){
    if(data.resultCode == "1000"){
        var svcListHtml = "";
        $(data.result.serviceNmList).each(function(i,item) {
            svcListHtml += "<option value=\""+item.svcSeq+"\">"+item.svcNm+"</option>";
        });
        $("#svcList").html(svcListHtml);

        storList();
    }
}

storList = function(){
    callByGet("/api/store?searchType=list&svcSeq="+$("#svcList").val(),"storListSuccess","NoneForm");
}

storListSuccess = function(data){
    if (data.resultCode == "1000") {
        var memberStoreListHtml = "";
        $(data.result.storeNmList).each(function(i, item) {
            memberStoreListHtml += "<option value=\"" + item.storSeq + "\">" + item.storNm + "</option>";
        });
        $("#storList").html(memberStoreListHtml);
        if (storSeq == 0) {
            getReleaseInfo();
        } else {
            $("#storList").val(storSeq);
            getContentsInfo();
        }
    }
}

storInfo = function(storSeq){
    callByGet("/api/store?storSeq="+storSeq,"storInfoSuccess","NoneForm");
}

storInfoSuccess = function(data){
    if (data.resultCode == "1000") {
        var item = data.result.storeInfo;

        $("#svcList").val(item.svcSeq);
        storList();

    }
}

getReleaseInfo = function(){
    callByGet("/api/mms/release?frmtnSeq="+$("#frmtnSeq").val(), "getReleaseInfoSuccess");
}

getReleaseInfoSuccess = function(data){
    if (data.resultCode == "1000") {
        oriName = data.result.releaseInfo.frmtnNm;
        checkedName = oriName;
        $("#frmtnNm").val(oriName);

        if (oriName == 'basic') {
            $(".btnModify").remove();
        }

        storSeq = data.result.releaseInfo.storSeq;
        storInfo(data.result.releaseInfo.storSeq);

        var releaseContentsHtml = "";
        $(data.result.contsList).each(function(i,item){
            releaseContentsHtml += "<tr class='rowConts'><td class=\"chkBox width50\"><input type=\"checkbox\" value=\""+item.contsSeq+"\" name=\"groupContentsSeq\" hidden></td>";
            releaseContentsHtml += "<td class=\"width50 contsTitle\">"+item.contsTitle+"</td>";
            releaseContentsHtml += "<td class=\"itemW30 contsCtgNm align_l ellipsis\">["+item.firstCtgNm+"]"+item.secondCtgNm+"</td>";
        });
        $("#selectVRContents").html(releaseContentsHtml);
        listBack($(".btnCancel"));
    }

    getContentsInfo();
}


releaseOverlap = function(){
    var str = $.trim($("#frmtnNm").val());
    var url = "/api/mms/release";
    if (str == "") {
        popAlertLayer(getMessage("cms.release.msg.Insert.Nm"));
        return;
    }

    if (str == "basic") {
        $("#overlap").next().text(getMessage("msg.contents.frmtnNm.not.basic"));
        return;
    }

    if(oriName != str || str == 'basic'){
        if ($("#storList").val() != 0) {
            url += "?storSeq="+$("#storList").val()+"&frmtnNm="+str;
        } else {
            url += "?frmtnNm="+str;
        }

        callByGet(url, "checkOverlapSuccess", "NoneForm");
    }else{
        checkedName = str;
        popAlertLayer(getMessage("cms.release.msg.use.success.Nm"));
    }
}

checkOverlapSuccess = function(data){
    var result = data.resultCode;
    if(result == "1000"){
        $("#overlap").next().text(getMessage("cms.release.msg.exist.Nm"));
    }else{
        $("#overlap").next().text("");
        popAlertLayer(getMessage("cms.release.msg.use.success.Nm"));
        checkedName = $.trim($("#frmtnNm").val());
    }
}

/*
 * 콘텐츠 목록 체크박스 또는 client 콘텐츠 목록이 변경 될 경우
 * parameter: type(구분자-content, client 콘텐츠)
 * return: x
 */
function checkboxChange(type){
    var allCheck = false;

    // 콘텐츠 목록 / client 콘텐츠 목록 체크 박스 바뀔 경우
    if(type == "contents" || type == "vrContents"){
        allCheck = true;
        $("input:checkbox[name="+type+"Seq]").each(function(){
            if(!this.checked){
                allCheck = false;
                return;
            }
        });
    }
    if(allCheck){
        $("#"+type+"AllCb").attr("checked", true);
    } else{
        $("#"+type+"AllCb").attr("checked", false);
    }
}



/*
* 콘텐츠 목록 / client 콘텐츠 전체 체크 변경될 경우
* parameter: type(구분자-content, client 콘텐츠)
* return: x
*/
function checkboxAll(type){
    if($("#"+type+"AllCb").is(":checked")){
        $("input:checkbox[name="+type+"Seq]").attr("checked", true).parents("tr").addClass("checked");
        $(".hide").removeClass("checked");
    } else {
        $("input:checkbox[name="+type+"Seq]").attr("checked", false).parents("tr").removeClass("checked");
    }
}

//컨텐츠 전체 정보 가져오기
function getContentsInfo(){
    arr = new Array();


    var searchSvc = $("#svcList").val();
    if(searchSvc != 0){
        searchSvc = "?svcSeq="+$("#svcList").val()+"&sttus=05&rows=10000&delYn=N&cntrctFnsDt=NOW";
    }else{
        searchSvc = "?rows=10000&sttus=05&delYn=N&cntrctFnsDt=NOW";
    }
    $("input[name=_method]").val("GET");

    callByGet("/api/contents"+searchSvc, "getContentsInfoSuccess");
}

function getContentsInfoSuccess(data){
    if(data.resultCode == "1000"){
        var contentsHtml = "";

        totalCount = data.result.totalCount;

        $(data.result.contentsList).each(function(i,item){
            contentsHtml += "<tr id=\"contentDetail"+(i+1)+"\"><td class=\"chkBox width50\"><input type=\"checkbox\" value=\""+item.contsSeq+"\" name=\"contsSeq\" hidden></td>";
            contentsHtml += "<td class=\"width50 contsTitle\">"+item.contsTitle+"</td>";
            contentsHtml += "<td class=\"itemW30 contsCtgNm align_l ellipsis\">["+item.firstCtgNm+"]"+item.secondCtgNm+"</td>";
            contentsHtml += "<td class=\"cretDt\" style=\"display:none;\">"+item.cretDt+"</td></tr>";
            arr.push({contsTitle: item.contsTitle,contsSeq: item.contsSeq,firstCtgNm:item.firstCtgNm,secondCtgNm : item.secondCtgNm});
        });

        $("#selectContents").html(contentsHtml);

    }else{
        popAlertLayer(getMessage("cms.release.msg.noData.conts"),"/mms/contents");
    }

    checkContent();
}

//해당 VR에 컨텐츠 정보 삭제
function deleteContentsInfo(){
    $(".noData").remove();

    $("input:checkbox[name=groupContentsSeq]:checked").each(function(){
        $("input[type=checkbox][value="+this.value+"]").parents("tr").removeClass("hide").show();
        $("input[type=checkbox][value="+this.value+"]").prop("checked",false);
        this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
    });
    $("#groupContentsAllCb").attr("checked", false);
    if($("#selectVRContents").find("tr").text() == ""){
        $("#selectVRContents").html("<tr class='noData'><td colspan=3 align=center height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
    }
}


//해당 VR에 컨텐츠 정보 등록
function insertContentsInfo(){
    $(".noData").remove();

    var VRContentsHtml = "";
    var contsSeq = "";
    var contsTitle = "";
    var ctgNm ="";
    var cretDt = "";
    var insertFlag;
    for(var num=1; num<totalCount+1; num++){
        if($("#contentDetail"+num+" input:checkbox:checked").val()){
            insertFlag = true;
            contsSeq = $("#contentDetail"+num+" input:checkbox").val();
            contsTitle = $("#contentDetail"+num+" .contsTitle").html();
            ctgNm = $("#contentDetail"+num+" .contsCtgNm").html();
            cretDt = $("#contentDetail"+num+" .cretDt").html();

            $("input:checkbox[name=groupContentsSeq]").each(function(){
                if(contsSeq == $(this).val()){
                    insertFlag = false;
                    return;
                }
            });

            if(insertFlag){
                VRContentsHtml += "<tr class='rowConts'><td class=\"chkBox width50\"><input type=\"checkbox\" value=\""+contsSeq+"\" name=\"groupContentsSeq\" hidden></td>";
                VRContentsHtml += "<td class=\"contsTitle\">"+contsTitle+"</td>";
                VRContentsHtml += "<td class=\"itemW30 contsCtgNm align_l ellipsis\">"+ctgNm+"</td>";
                VRContentsHtml += "<td class=\"cretDt\" style='display:none'>"+cretDt+"</td></tr>";
            }
        }
    }
    $("#selectContents > .checked").hide().removeClass("checked").addClass("hide");

    if($("#selectVRContents").find("tr").text() == getMessage("cms.release.msg.noData") && VRContentsHtml.length > 0){
        $("#selectVRContents").html("");
     }
    $("#contsAllCb").prop("checked",false);
    $("#selectVRContents").append(VRContentsHtml);

    if (VRContentsHtml == "" && VRContentsHtml.length == 0) {
        $("#selectVRContents").append("<tr class='noData'><td colspan=3 style='text-align:center !important' height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
    }

}

//적용하기
function updateRelease(){
    if ($("#selectVRContents tr.rowConts").length == $("#selectVRContents .hide").length) {
        popAlertLayer(getMessage("cms.release.msg.select.conts"));
        return;
    }

    if(checkedName == $.trim($("#frmtnNm").val()) && checkedName != 'basic'){
        formData("NoneForm" , "frmtnNm", $("#frmtnNm").val());
        formData("NoneForm" , "storSeq", $("#storList").val());

        var contsSeqs = "";
        $("#selectVRContents tr.rowConts").each(function(){
            if ($(this).css("display") != "none") {
                contsSeqs += $(this).find("input").val() + ",";
            }
        });

        formData("NoneForm" , "frmtnSeq", $("#frmtnSeq").val());
        formData("NoneForm" , "contsSeqs", contsSeqs.slice(0,-1));
        callByPut("/api/mms/release" , "updateReleaseContsInfoSuccess", "NoneForm");
    }else{
        popAlertLayer(getMessage("cms.release.msg.check.frmtnNm"));
    }

}

function updateReleaseContsInfoSuccess(data){
    formDataDelete("NoneForm", "contsSeqs");
    formDataDelete("NoneForm", "frmtnSeq");
    formDataDelete("NoneForm", "frmtnNm");
    formDataDelete("NoneForm", "storSeq");
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.update"), "/mms/contents#type=frmtn&storSeq="+$("#storList").val()+"&svcSeq="+$("#svcList").val());
    }
}


function checkContent(){
    $(".noData").remove();
    $("tbody tr").each(function(){
        if ($(this).html() == "") {
            $(this).remove();}
        }
    );
    $("#selectVRContents tr.rowConts").each(function(){
        var obj = $("#selectContents input[value='"+$(this).find(".chkBox input").val()+"']");
        if ($(obj).index() == -1) {
            $(this).addClass("hide").hide();
        } else {
            $(this).removeClass("hide").show();
        }
    });

    $("#selectVRContents tr.rowConts").each(function(){
        $("#selectContents input[value='"+$(this).find(".chkBox input").val()+"']").prop("checked",false).parents("tr").addClass("hide").hide();
    });

    if($("#selectVRContents tr.rowConts").length == $("#selectVRContents .hide").length){
        $("#selectVRContents").append("<tr class='noData'><td colspan=3 align=center height=80>"+getMessage("cms.release.msg.noData")+"</td><tr>");
    }
}


deleteRelease = function(){
    var frmtnSeq = "";
    popConfirmLayer(getMessage("cms.delete.release.msg"), function(){
        formData("NoneForm" , "frmtnSeqs", $("#frmtnSeq").val());
        callByDelete("/api/mms/release", "deleteReleaseSuccess", "NoneForm","deleteFail");
    });
}

deleteFail = function(data){
    popAlertLayer(getMessage("msg.common.delete.fail"));
}

deleteReleaseSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.delete"),"/mms/contents");
    }
}
