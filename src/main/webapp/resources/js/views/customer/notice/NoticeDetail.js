/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    // Master or CMS 관리자일 경우에만 공지사항 관리 가능
    if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02') {
        $("#btnUpdate").hide();
        $("#btnDelete").hide();
    }

    noticeInfo();
});

function noticeInfo(){
    formData("NoneForm", "noticeSeq", $("#noticeSeq").val());
    callByGet("/api/notice", "noticeInfoSuccess", "NoneForm", "noticeInfoFail");
}

noticeInfoSuccess = function(data) {
    formDataDelete("NoneForm", "noticeSeq");
    if(data.resultCode == "1000"){
        var noticeDetail = data.result.noticeDetail;
        $("#noticeTitle").html(noticeDetail.noticeTitle + "<span class='noticeDate'>" + getMessage("common.section") + " : " + noticeDetail.noticeTypeNm + "<span class='div'>|</span>" + getMessage("common.reger") + " : " + maskName(noticeDetail.cretrNm) + "<span class='div'>|</span>" + getMessage("common.regdate") + " : " + noticeDetail.regDt + "</span>" );
        $("#noticeSbst").html(noticeDetail.noticeSbst);
        //$("#noticeSbst").html(xssChk(noticeDetail.noticeSbst));
        //$("#noticeSbst").html($("#noticeSbst").text());

        if (data.result.fileList.length == 0) {
            $("#fileGrp").hide();
        } else {
            var fileListHtml = "<table width='100%'><tr><td class='attatch'><div class='txtFileadd'>" + getMessage("common.attachfile") + " : </div></td><td class='attatchFile'>";
            $(data.result.fileList).each(function(i,item) {
                fileListHtml += "<p><span class='float_l'>" + item.fileName + "</span><a href='" + makeAPIUrl("/api/fileDownload/" + item.fileSeq) + "' class='fontblack btnDownloadGray'>[" + getMessage("common.download") + "]</a></p>";
            });
            fileListHtml += "</td></tr></table>";
            $("#fileGrp").html(fileListHtml);
        }

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/customer/notice");
    }
}

noticeInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/customer/notice");
}

function deleteNotice() {
    formDataDeleteAll("NoneForm");
    popConfirmLayer(getMessage("common.delete.msg"), function() {
        formData("NoneForm", "noticeSeqList", $("#noticeSeq").val());
        callByDelete("/api/notice", "deleteNoticeSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

deleteNoticeSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/customer/notice");
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}