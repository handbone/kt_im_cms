/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var noticeTypeCd;

var ATTCH_FILE;
var FILE_SETTINGS   = {
        // Backend Settings
//      upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
};

$(document).ready(function(){
    $(".checkPopupView").attr('disabled', true);

    var now=new Date();
    $("#startDt").val(dateString((new Date(now.getTime()))));
    $("#endDt").val(dateString((new Date(now.getTime()))));

    $("#startDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#endDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#endDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#startDt").datepicker("option","maxDate", selected);

            var nowDt = dateString(new Date(now.getTime()));
            if (nowDt > $("#endDt").val()) {
                $("input[name='delYn'][value=Y]").attr("checked", true);
                $("input[name='delYn']").attr("disabled", true);
            } else {
                $("input[name='delYn'][value=N]").attr("checked", true);
                $("input[name='delYn']").attr("disabled", false);
            }
        }
    });

    limitInputTitle("noticeTitle");

    setkeyup();

    noticeInfo();
    fileUploadBtn();

    listBack($(".btnCancel"));
});

//swfupload - attch_file
fileUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'notice'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.pdf;*.hwp;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.png;*.jpg;*.jpeg;*.bmp;*.gif",
    file_settings.file_queue_limit =  "0";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    ATTCH_FILE  = new SWFUpload(file_settings);
}

noticeInfo = function() {
    formData("NoneForm", "noticeSeq", $("#noticeSeq").val());
    formData("NoneForm", "searchType", "edit");
    callByGet("/api/notice", "noticeInfoSuccess", "NoneForm", "noticeInfoFail");
}

noticeInfoSuccess = function(data) {
    formDataDelete("NoneForm", "noticeSeq");
    if (data.resultCode == "1000") {
        var noticeDetail = data.result.noticeDetail;
        $("#noticeTitle").val(noticeDetail.noticeTitle);

        var strNoticeSbst = strConv(noticeDetail.noticeSbst);
        strNoticeSbst = strConv(strNoticeSbst);
        CKEDITOR.instances.noticeSbst.setData(strNoticeSbst);

        noticeTypeCd = noticeDetail.noticeType;
        $("#rankSelbox").val(noticeDetail.noticePrefRank);

        if(noticeDetail.popupViewYn == 'Y'){
            $(".checkPopupView").attr("checked","checked");
        }

        // YYYY_MM_DD
        $("#startDt").val(noticeDetail.stDt.substring(0, 10));
        $("#endDt").val(noticeDetail.fnsDt.substring(0, 10));

        var fileListHtml = "";
        $(data.result.fileList).each(function(i, item) {
            fileListHtml += "<table class=\"fileTable\"><tr><td><p class=\"fileName\" title=\"" + item.fileName
                        + "\">" + getMessage("common.filename") + " : "+ item.fileName + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\""
                        + " onclick=\"savedfileDelete(this, " + item.fileSeq + ")\" value=\"" + getMessage("button.delete") + "\"></td></tr></table>";
        });
        $("#fileArea").html(fileListHtml);

        $("input[name='delYn'][value=" + noticeDetail.delYn + "]").attr("checked", true);

        var now=new Date();
        var nowDt = dateString(new Date(now.getTime()));
        if (nowDt > $("#endDt").val()) {
            $("input[name='delYn']").attr("disabled", true);
        } else {
            $("input[name='delYn']").attr("disabled", false);
        }

        noticeTypeList(noticeDetail.cretrMbrSe);

        listBack($(".btnCancel"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/customer/notice");
    }
}

noticeInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/customer/notice");
}

noticeTypeList = function(cretrMbrSe) {
    callByGet("/api/noticeType?cretrMbrSe=" + cretrMbrSe, "noticeTypeListSuccess", "NoneForm", "noticeTypeListFail");
}

noticeTypeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var noticeTypeListHtml = "";
        $(data.result.typeList).each(function(i, item) {
            noticeTypeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#typeSelbox").html(noticeTypeListHtml);
        $("#typeSelbox").val(noticeTypeCd);

        if ($("#typeSelbox").val() == '04') {
            $(".checkPopupView").attr('disabled', false);
        }
    } else {
        $(".btnModify").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

noticeTypeListFail = function(data) {
    $(".btnModify").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

function changeNoticeType() {
    $(".checkPopupView").attr('checked', false);
    if ($("#typeSelbox").val() == '04') {
        $(".checkPopupView").attr('disabled', false);
    } else {
        $(".checkPopupView").attr('disabled', true);
    }
}

var delFileSeq="";
savedfileDelete = function(e, fileSeq){
    $(e).parents("table.fileTable").remove();
    delFileSeq += fileSeq + ",";
}

fileQueueDelete = function(e, fileId){
    $(e).parents("table.fileTable").remove();
    fileTemp.cancelUpload(fileId);
}

function updateNotice() {
    if ($("#noticeTitle").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if ($("#startDt").val() == "") {
        popAlertLayer(getMessage("notice.display.period.startdate.empty.msg"));
        return;
    }

    if ($("#endDt").val() == "") {
        popAlertLayer(getMessage("notice.display.period.enddate.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.noticeSbst.getData().length < 1) {
        popAlertLayer(getMessage("notice.detail.description.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        CKEDITOR.instances.noticeSbst.updateElement();
        $("#noticeSbst").val(CKEDITOR.instances.noticeSbst.getData());

        var popupYn = $(".checkPopupView").attr("checked");
        if (typeof popupYn == 'undefined') {
            popupYn = 'N';
        } else {
            popupYn = 'Y';
        }

        formData("NoneForm", "noticeSeq", $("#noticeSeq").val());
        formData("NoneForm", "noticeTitle", $("#noticeTitle").val());
        formData("NoneForm", "noticeType", $("#typeSelbox").val());

        // 공지사항을 비공개로 설정 시 중요도 설정을 일반으로 변경
        var isPrivate = $('input:radio[name=delYn]:checked').val();
        if (isPrivate == 'Y') {
            formData("NoneForm", "noticePrefRank", 0);
        } else {
            formData("NoneForm", "noticePrefRank", $("#rankSelbox").val());
        }

        formData("NoneForm", "popupYn", popupYn);
        formData("NoneForm", "startDt", $("#startDt").val() + " 00:00:00");
        formData("NoneForm", "endDt", $("#endDt").val() + " 23:59:59");
        formData("NoneForm", "noticeSbst", $("#noticeSbst").val());
        formData("NoneForm", "delFileSeqs", delFileSeq.slice(0,-1));
        formData("NoneForm", "delYn", $('input:radio[name=delYn]:checked').val());

        callByPut("/api/notice", "updateNoticeSuccess", "NoneForm", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data){
    formDataDeleteAll("NoneForm");
    if (fileTemp != undefined) {
        fileTemp.cancelUpload(e.id);
    }
    popAlertLayer(getMessage("fail.common.update"));
}

updateNoticeSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        $("#contsSeq").val($("#noticeSeq").val());
        uploadAttachFiles();
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

uploadAttachFiles = function() {
    if (fileTemp != undefined) {
        fileTemp.startUpload();
    } else {
        popAlertLayer(getMessage("success.common.update"), "/customer/notice");
    }
}

fileFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("msg.common.file.upload.fail"));
}

fileInsertSuccess = function(data) {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertFail = function() {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertSuccessPopup = function() {
    var msg = getMessage("success.common.update");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }

    popAlertLayer(msg, "/customer/notice");
}