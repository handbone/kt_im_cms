/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var ATTCH_FILE;
var FILE_SETTINGS   = {
        // Backend Settings
//      upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
};

$(document).ready(function(){
    $(".checkPopupView").attr('disabled', true);

    var now=new Date();
    $("#startDt").val(dateString((new Date(now.getTime()))));
    $("#endDt").val(dateString((new Date(now.getTime()))));

    $("#startDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#endDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#endDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#startDt").datepicker("option","maxDate", selected)
        }
    });

    limitInputTitle("noticeTitle");

    setkeyup();

    noticeTypeList();
    fileUploadBtn();

    listBack($(".btnCancel"));
});

//swfupload - attch_file
fileUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'notice'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.pdf;*.hwp;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.png;*.jpg;*.jpeg;*.bmp;*.gif",
    file_settings.file_queue_limit =  "0";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    ATTCH_FILE  = new SWFUpload(file_settings);
}

noticeTypeList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_SE", "noticeTypeListSuccess", "NoneForm", "noticeTypeListFail");
}

noticeTypeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var noticeTypeListHtml = "";
        $(data.result.codeList).each(function(i, item) {
            noticeTypeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#typeSelbox").html(noticeTypeListHtml);
    } else {
        $(".btnWrite").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

noticeTypeListFail = function(data) {
    $(".btnWrite").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

function changeNoticeType() {
    console.log(">> $(#typeSelbox).val() : " + $("#typeSelbox").val());
    $(".checkPopupView").attr('checked', false);
    if ($("#typeSelbox").val() == '04') {
        $(".checkPopupView").attr('disabled', false);
    } else {
        $(".checkPopupView").attr('disabled', true);
    }
}

function registNotice() {
    if ($("#noticeTitle").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if ($("#startDt").val() == "") {
        popAlertLayer(getMessage("notice.display.period.startdate.empty.msg"));
        return;
    }

    if ($("#endDt").val() == "") {
        popAlertLayer(getMessage("notice.display.period.enddate.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.noticeSbst.getData().length < 1) {
        popAlertLayer(getMessage("notice.detail.description.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        CKEDITOR.instances.noticeSbst.updateElement();
        $("#noticeSbst").val(CKEDITOR.instances.noticeSbst.getData());

        var popupYn = $(".checkPopupView").attr("checked");
        if (typeof popupYn == 'undefined') {
            popupYn = 'N';
        } else {
            popupYn = 'Y';
        }

        formData("NoneForm", "noticeTitle", $("#noticeTitle").val());
        formData("NoneForm", "noticeType", $("#typeSelbox").val());
        formData("NoneForm", "noticePrefRank", $("#rankSelbox").val());
        formData("NoneForm", "popupYn", popupYn);
        formData("NoneForm", "startDt", $("#startDt").val() + " 00:00:00");
        formData("NoneForm", "endDt", $("#endDt").val() + " 23:59:59");
        formData("NoneForm", "noticeSbst", $("#noticeSbst").val());

        // 게시물 공개 기간이 현재 시간 이전일 경우 비공개 게시물로 저장
        var now=new Date();
        var nowDt = dateString(new Date(now.getTime()));
        if (nowDt > $("#endDt").val()) {
            formData("NoneForm", "delYn", "Y");
        } else {
            formData("NoneForm", "delYn", "N");
        }

        callByPost("/api/notice", "registNoticeSuccess", "NoneForm", "insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

registNoticeSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        $("#contsSeq").val(data.result.noticeSeq);
        uploadAttachFiles();
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

uploadAttachFiles = function() {
    if(fileTemp != undefined){
        fileTemp.startUpload();
    } else {
        popAlertLayer(getMessage("success.common.insert"), "/customer/notice");
    }
}

fileFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("msg.common.file.upload.fail"));
}

fileInsertSuccess = function(data) {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertFail = function() {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertSuccessPopup = function() {
    var msg = getMessage("success.common.insert");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }

    popAlertLayer(msg, "/customer/notice");
}

fileQueueDelete = function(e, fileId){
    $(e).parents("table.fileTable").remove();
    fileTemp.cancelUpload(fileId);
}
