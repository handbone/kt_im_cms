/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var qnaTyCd;

var ATTCH_FILE;
var FILE_SETTINGS   = {
        // Backend Settings
//      upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
};

$(document).ready(function(){
    setkeyup();

    qnaInfo();
    fileUploadBtn();
});

//swfupload - attch_file
fileUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'qnaAns'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.pdf;*.hwp;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.png;*.jpg;*.jpeg;*.bmp;*.gif",
    file_settings.file_queue_limit =  "0";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    ATTCH_FILE  = new SWFUpload(file_settings);
}

function qnaInfo(){
    formData("NoneForm", "qnaSeq", $("#qnaSeq").val());
    formData("NoneForm", "searchType", "edit");
    callByGet("/api/qna", "qnaInfoSuccess", "NoneForm", "qnaInfoFail");
}

qnaInfoSuccess = function(data) {
    formDataDelete("NoneForm", "qnaSeq");
    if(data.resultCode == "1000"){
        var qnaDetail = data.result.qnaDetail;

        $("#qnaCretrNm").html(qnaDetail.cretrNm);
        $("#qnaRegDt").html(qnaDetail.regDt);
        $("#qnaCtg").html(qnaDetail.qnaCtg);
        $("#qnaTitle").html(qnaDetail.qnaTitle);
        $("#qnaSbst").html(xssChk(qnaDetail.qnaSbst));
        $("#qnaSbst").html($("#qnaSbst").text());

        listBack($(".btnCancel"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/customer/qna");
    }
}

qnaInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/customer/qna");
}

fileQueueDelete = function(e, fileId){
    $(e).parents("table.fileTable").remove();
    fileTemp.cancelUpload(fileId);
}

function qnaAnsRegist() {
    if (CKEDITOR.instances.ansSbst.getData().length < 1) {
        popAlertLayer(getMessage("qna.answer.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("qna.answer.regist.confirm.msg"), function() {
        CKEDITOR.instances.ansSbst.updateElement();
        $("#qnaSbst").val(CKEDITOR.instances.ansSbst.getData());

        formData("NoneForm", "qnaSeq", $("#qnaSeq").val());
        formData("NoneForm", "ansSbst", $("#ansSbst").val());

        callByPut("/api/qna", "qnaAnsRegistSuccess", "NoneForm", "insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data){
    formDataDeleteAll("NoneForm");
    if (fileTemp != undefined) {
        while (fileTemp.getStats().files_queued != 0) {
            fileTemp.cancelUpload();
        }
    }
    popAlertLayer(getMessage("fail.common.insert"));
}

qnaAnsRegistSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        $("#contsSeq").val($("#qnaSeq").val());
        if ($("#usesAutoSend").val() == "Y") {
            var options = { type : "single", section : "replyQna", seq : $("#qnaSeq").val() };
            sendSms(options);
        }

        uploadAttachFiles();
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}

uploadAttachFiles = function() {
    if (fileTemp != undefined) {
        fileTemp.startUpload();
    } else {
        popAlertLayer(getMessage("success.common.insert"), "/customer/qna");
    }
}

fileFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("msg.common.file.upload.fail"));
}

fileInsertSuccess = function(data) {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertFail = function() {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertSuccessPopup = function() {
    var msg = getMessage("success.common.insert");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }

    popAlertLayer(msg, "/customer/qna");
}