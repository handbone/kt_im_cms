/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var gridState;
var tempState;
var searchField;
var searchString;
var totalPage = 0;

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            jQuery("#jqgridData").trigger("reloadGrid");
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    qnaList();

    $("#keyword").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

qnaList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("table.title"),
        getMessage("table.name"),
        getMessage("table.section"),
        getMessage("table.category"),
        getMessage("table.retvnum"),
        getMessage("table.file"),
        getMessage("table.regdate"),
        "cretrId",
        "ansCretrId",
        "qnaSeq"
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/qna"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.qnaList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames:columnNames,
        colModel:[
            {name:"num", index: "num", align:"center", width:40},
            {name:"qnaTitle", index:"QNA_TITLE", align:"left",formatter:pointercursor, width:400},
            {name:"cretrNm", index:"CRETR_NM", align:"center", cellattr: regerTitleAttr},
            {name:"qnaType", index:"QNA_TYPE", align:"center", hidden:true},
            {name:"qnaCtg", index:"QNA_CTG", align:"center"},
            {name:"retvNum", index:"RETV_NUM", align:"center"},
            {name:"fileExist", index:"FILE", align:"center", cellattr: function(rowId, val, rowObject, cm, rdata) {return 'title=""';}},
            {name:"regDt", index:"REG_DT", align:"center"},
            {name:"cretrId", index:"CRETR_ID", hidden:true},
            {name:"ansCretrId", index:"ANS_CRETR_ID", hidden:true},
            {name:"qnaSeq", index:"QNA_SEQ", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "QNA_SEQ",
            sortorder: "desc",
            height: "auto",
            multiselect: false,
            beforeRequest:function() {
                tempState = gridState;
                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

                if (isNaN(page)) {
                    page = 1;
                }

                if (totalPage > 0 && totalPage < page) {
                    page = 1;
                }

                if (gridState != "SEARCH") {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                }
                searchField = checkUndefined($("#target").val());
                searchString = checkUndefined($("#keyword").val());

                if (gridState == "HASH") {
                    myPostData.page = page;
                    tempState = "READY";
                } else {
                    if (tempState == "SEARCH") {
                        myPostData.page = 1;
                    } else {
                        tempState = "";
                    }
                }

                if (gridState == "NONE") {
                    searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                    searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                    myPostData.page = page;

                    if (searchField != null) {
                        tempState = "SEARCH";
                    } else {
                        tempState = "READY";
                    }
                }

                if (searchField != null && searchField != "" && searchString != "") {
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                } else {
                    delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
                }

                gridState = "GRIDBEGIN";

                $('#jqgridData').jqGrid('setGridParam', { postData: myPostData, page: myPostData.page });
            },
            loadComplete: function (data) {
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                //session
                if(sessionStorage.getItem("last-url") != null){
                    sessionStorage.removeItem('state');
                    sessionStorage.removeItem('last-url');
                }

                //Search 필드 등록
                searchFieldSet();

                if (data.resultCode == 1000) {
                    totalPage = data.result.totalPage;

                    if (page != data.result.currentPage) {
                        tempState = "";
                    }
                } else {
                    tempState = "";
                }

                /*뒤로가기*/
                var hashlocation = "page=" + $(this).context.p.page;
                if($(this).context.p.postData._search && $("#target").val() != null){
                    hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
                }

                gridState = "GRIDCOMPLETE";
                document.location.hash = hashlocation;

                if (tempState != "" || str_hash == hashlocation) {
                    gridState = "READY";
                }

                $(this).find("td").each(function(){
                    if ($(this).index() == 1) {
                        if ($(this).parent("tr").find("td:eq(9)").text().replace(/\s/g,'') != "") {
                            console.log(">>> ansCretrId : " + $(this).parent("tr").find("td:eq(9)").text());
                            if ($(this).children("span.pointer").width() > ($(this).width() - 40)) {
                                // 답변이 있는 상태 : 컬럼 크기에서 reply 아이콘 값을 제외한 width 보다 내용이 클 경우
                                $(this).html("<span class='pointer' style='width: 90%; text-overflow: ellipsis; overflow: hidden; float: left;'>"
                                        + $(this).text() + "</span>" + "<img src='" + makeAPIUrl("/resources/image/icon_reply.png") + "' style='width: 50px; height: 18px; vertical-align: top;'>");
                            } else {
                                $(this).html("<span class='pointer replyIcon'>" + $(this).text() + "</span>");
                            }
                        }
                    } else if ($(this).index() == 2) {
                        var name = $(this).text();
                        $(this).text(maskName(name));
                    } else if ($(this).index() == 6) { // FILE
                        if ($(this).text() == "true") {
                            $(this).html("<img src='" + makeAPIUrl("/resources/image/icon_file.png") + "' style='width: 13px;vertical-align: middle;' />");
                        } else {
                            $(this).html("");
                        }
                    }
                });

                //pageMove Max
                $('.ui-pg-input').on('keyup', function() {
                    this.value = this.value.replace(/\D/g, '');
                    if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
                });

                //포인터
                $(this).find(".pointer").parent("td").addClass("pointer");

                resizeJqGridWidth("jqgridData", "gridArea");
            },
            loadError: function (jqXHR, textStatus, errorThrown) {
                $("#jqgridData").clearGridData();
                $("#sp_1_pageDiv").text(1);
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event){
                // 제목 셀 클릭시
                if (columnId == 1) {
                    var list = $("#jqgridData").jqGrid('getRowData', rowId);
                    sessionStorage.setItem("last-url", location);
                    sessionStorage.setItem("state", "view");

                    /*
                    if (list.cretrId != null && list.cretrId != "undefined" && list.cretrId != "") {
                        setCookie('cretrId', list.cretrId);
                    }

                    if (list.ansCretrId != null && list.ansCretrId != "undefined" && list.ansCretrId != "") {
                        setCookie('ansCretrId', list.ansCretrId);
                    }
                    */

                    pageMove("/customer/qna/"+ list.qnaSeq);
                }
            }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

function regerTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = maskName(rawObject.cretrNm);
    return 'title="' + retVal  + '"';
}

function keywordSearch() {
    /*
    $("#jqgridData").jqGrid('setGridParam',
            {search: true,
        postData:
        {searchField: $("#target").val(),searchString: $("#keyword").val()},page:1}
    );
    $("#jqgridData").trigger("reloadGrid");
    */
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

searchFieldSet = function() {
    if ($("#target > option").length == 0) {
        var searchHtml = "";
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name){
            case "qnaTitle":
                break;
            case "cretrNm":
                break;
            default:
                continue;
            }
            searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}
