/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var ansCretrId;

$(document).ready(function(){
    qnaInfo();
});

function qnaInfo(){
    formData("NoneForm", "qnaSeq", $("#qnaSeq").val());
    callByGet("/api/qna", "qnaInfoSuccess", "NoneForm", "qnaInfoFail");
}

qnaInfoSuccess = function(data) {
    formDataDelete("NoneForm", "qnaSeq");
    if(data.resultCode == "1000"){
        var qnaDetail = data.result.qnaDetail;
        $("#qnaTitle").html("<span class='txtRed'>" + qnaDetail.qnaTitle + "</span><span class='qnaDate'>"
                + getMessage("qna.reger") + " : " + maskName(qnaDetail.cretrNm) + "<span class='div'>|</span>"
                + getMessage("common.category") + " : " + qnaDetail.qnaCtg + "<span class='div'>|</span>"
                + getMessage("common.regdate") + " : " + qnaDetail.regDt + "</span>" );
        $("#qnaSbst").html(qnaDetail.qnaSbst);
        //$("#qnaSbst").html(xssChk(qnaDetail.qnaSbst));
        //$("#qnaSbst").html($("#qnaSbst").text());

        if (data.result.fileList.length == 0) {
            $("#qnaFileGrp").hide();
        } else {
            var fileListHtml = "<table width='100%'><tr><td class='attatch'><div class='txtFileadd'>" + getMessage("common.attachfile") + " : </div></td><td class='attatchFile'>";
            $(data.result.fileList).each(function(i,item) {
                fileListHtml += "<p><span class='float_l'>" + item.fileName + "</span><a href='" + makeAPIUrl("/api/fileDownload/" + item.fileSeq) + "' class='fontblack btnDownloadGray'>[" + getMessage("common.download") + "]</a></p>";
            });
            fileListHtml += "</td></tr></table>";
            $("#qnaFileGrp").html(fileListHtml);
        }

        // 수정 시 답변이 있는지 확인하기 위해 저장
        ansCretrId = qnaDetail.ansCretrId;

        if (qnaDetail.ansCretrId == "") {
            $("#ansTitle").hide();
            $("#ansSbst").hide();
            $("#ansFileGrp").hide();

            // Master, CMS 관리자인 경우에만 답변 등록 가능
            if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02') {
                $("#ansBtn").hide();
            }
        } else {
            $("#ansTitle").html("<span class='txtBlue'>" + getMessage("qna.answer") + "</span><span class='qnaDate'>" + getMessage("qna.reger") + " : " + maskName(qnaDetail.ansCretrNm) 
                    + "<span class='div'>|</span>" + getMessage("common.regdate") + " : " + qnaDetail.ansRegDt + "</span>" );

            $("#ansSbst").html(qnaDetail.ansConts);
            //$("#ansSbst").html(xssChk(qnaDetail.ansConts));
            //$("#ansSbst").html($("#ansSbst").text());
            if (data.result.ansFileList.length == 0) {
                $("#ansFileGrp").hide();
            } else {
                var ansFileListHtml = "<table width='100%'><tr><td class='attatch'><div class='txtFileadd'>" + getMessage("common.attachfile") + " : </div></td><td class='attatchFile'>";
                $(data.result.ansFileList).each(function(i,item) {
                    ansFileListHtml += "<p><span class='float_l'>" + item.fileName + "</span><a href='" + makeAPIUrl("/api/fileDownload/" + item.fileSeq) + "' class='fontblack btnDownloadGray'>[" + getMessage("common.download") + "]</a></p>";
                });
                ansFileListHtml += "</td></tr></table>";
                $("#ansFileGrp").html(ansFileListHtml);
            }

            // 답변 등록 버튼 비활성화
            $("#ansBtn").hide();
        }

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/customer/qna");
    }
}

qnaInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/customer/qna");
}

