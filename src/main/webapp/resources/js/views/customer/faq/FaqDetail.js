/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    faqInfo();
});

faqInfo = function() {
    formData("NoneForm", "faqSeq", $("#faqSeq").val());
    callByGet("/api/faq", "faqInfoSuccess", "NoneForm", "faqInfoFail");
}

faqInfoSuccess = function(data) {
    formDataDelete("NoneForm", "faqSeq");
    if (data.resultCode == "1000") {
        var faqDetail = data.result.faqDetail;
        $("#faqTitleQsn").html("<span class='txtRed'>" + getMessage("common.question") + "</span><span class='faqDate'>"
                    + getMessage("common.section") + " : " + faqDetail.faqType + "<span class='div'>|</span>"
                    + getMessage("common.category") + " : " + faqDetail.faqCtg + "<span class='div'> | </span>"
                    + getMessage("common.reger") + " : " + maskName(faqDetail.cretrNm) + "<span class='div'> | </span>"
                    + getMessage("common.regdate") + " : " + faqDetail.regDt + "</span>");
        $("#faqQsn").html(faqDetail.faqTitle);
        $("#faqAns").html(faqDetail.faqSbst);
        //$("#faqAns").html(xssChk(faqDetail.faqSbst));
        //$("#faqAns").html($("#faqAns").text());

        if ($("#memberSec").val() != '01' && $("#memberSec").val() != '02') {
            $("#btnUpdate").hide();
            $("#btnDelete").hide();
        }

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/customer/faq");
    }
}

faqInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/customer/faq");
}

function deleteFaq() {
    popConfirmLayer(getMessage("faq.delete.confirm.msg"), function() {
        formData("NoneForm" , "faqSeqList", $("#faqSeq").val());
        callByDelete("/api/faq", "deleteFaqSuccess", "NoneForm", "deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

deleteFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), "/customer/faq");
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}