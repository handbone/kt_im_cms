/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var gridState;
var tempState;
var searchField;
var searchString;
var totalPage = 0;

$(window).ready(function() {
    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        } else if (gridState != "GRIDCOMPLETE") {
            gridState = "HASH";
            var str_hash = document.location.hash.replace("#","");
            searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
            searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

            $("#target").val(searchField);
            $("#keyword").val(searchString);
            jQuery("#jqgridData").trigger("reloadGrid");
        } else {
            gridState = "READY";
        }
    });
});

$(document).ready(function() {
    if (document.location.hash != "") {
        var str_hash = document.location.hash.replace("#","");
        searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
        searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
        $("#target").val(searchField);
        $("#keyword").val(searchString);

        gridState = "NONE";
    }

    faqList();

    $("#keyword").keydown(function(e) {
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            keywordSearch();
        }
    });
});

faqList = function() {
    var multiSel = false;
    // Master or CMS 관리자일 경우에만 공지사항 관리 가능
    if ($("#memberSec").val() == '01' || $("#memberSec").val() == '02') {
        multiSel = true;
    } else {
        // 삭제, 등록 버튼 비활성화
        $("#btnDel").hide();
        $("#btnCreate").hide();
    }

    var columnNames = [
        getMessage("table.num"),
        getMessage("table.title"),
        getMessage("table.name"),
        getMessage("table.section"),
        getMessage("table.category"),
        getMessage("table.retvnum"),
        getMessage("table.regdate"),
        "delYn",
        "faqSeq"
    ];

    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/faq"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.faqList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames:columnNames,
        colModel:[
            {name:"num", index: "num", align:"center", width:40},
            {name:"faqTitle", index: "FAQ_TITLE", align:"left",formatter:pointercursor, width:400},
            {name:"cretrNm", index:"CRETR_NM", align:"center", cellattr: regerTitleAttr},
            {name:"faqType", index:"FAQ_TYPE", align:"center"},
            {name:"faqCtg", index:"FAQ_CTG", align:"center"},
            {name:"retvNum", index:"RETV_NUM", align:"center"},
            {name:"regDt", index:"REG_DT", align:"center"},
            {name:"delYn", index:"DEL_YN", hidden:true},
            {name:"faqSeq", index:"FAQ_SEQ", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "FAQ_SEQ",
            sortorder: "desc",
            height: "auto",
            multiselect: multiSel,
            beforeRequest:function(){
                tempState = gridState;
                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));
                searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                searchString = checkUndefined(findGetParameter(str_hash,"searchString"));

                if (isNaN(page)) {
                    page = 1;
                }

                if (totalPage > 0 && totalPage < page) {
                    page = 1;
                }

                if (gridState != "SEARCH") {
                    $("#target").val(searchField);
                    $("#keyword").val(searchString);
                }
                searchField = checkUndefined($("#target").val());
                searchString = checkUndefined($("#keyword").val());

                if (gridState == "HASH") {
                    myPostData.page = page;
                    tempState = "READY";
                } else {
                    if (tempState == "SEARCH") {
                        myPostData.page = 1;
                    } else {
                        tempState = "";
                    }
                }

                if (gridState == "NONE") {
                    searchField = checkUndefined(findGetParameter(str_hash,"searchField"));
                    searchString = checkUndefined(findGetParameter(str_hash,"searchString"));
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                    myPostData.page = page;

                    if (searchField != null) {
                        tempState = "SEARCH";
                    } else {
                        tempState = "READY";
                    }
                }

                if (searchField != null && searchField != "" && searchString != "") {
                    myPostData._search = true; myPostData.searchField = searchField; myPostData.searchString = searchString;
                } else {
                    delete myPostData.searchString; delete myPostData.searchField;  myPostData._search = false;
                }

                gridState = "GRIDBEGIN";

                $('#jqgridData').jqGrid('setGridParam', { postData: myPostData, page: myPostData.page });
            },
            loadComplete: function (data) {
                var str_hash = document.location.hash.replace("#","");
                var page = parseInt(findGetParameter(str_hash,"page"));

                //session
                if(sessionStorage.getItem("last-url") != null){
                    sessionStorage.removeItem('state');
                    sessionStorage.removeItem('last-url');
                }

                //Search 필드 등록
                searchFieldSet();

                if (data.resultCode == 1000) {
                    totalPage = data.result.totalPage;

                    if (page != data.result.currentPage) {
                        tempState = "";
                    }
                } else {
                    tempState = "";
                }

                /*뒤로가기*/
                var hashlocation = "page=" + $(this).context.p.page;
                if($(this).context.p.postData._search && $("#target").val() != null){
                    hashlocation += "&searchField=" + $("#target").val() + "&searchString=" + $("#keyword").val();
                }

                gridState = "GRIDCOMPLETE";
                document.location.hash = hashlocation;

                if (tempState != "" || str_hash == hashlocation) {
                    gridState = "READY";
                }

                $(this).find("td").each(function(){
                    if (multiSel) {
                        if ($(this).index() == 2) {
                            if ($(this).parent("tr").find("td:eq(8)").text() == 'Y') { // 비활성 여부 확인
                                if ($(this).children("span.pointer").width() > ($(this).width() - 40)) {
                                    // 비활성 상태 : 컬럼 크기에서 hidden 아이콘 값을 제외한 width 보다 내용이 클 경우
                                    $(this).html("<span class='pointer' style='width: 90%; text-overflow: ellipsis; overflow: hidden; float: left;'>"
                                            + $(this).text() + "</span>" + "<img src='" + makeAPIUrl("/resources/image/icon_hidden.png") + "' style='width: 40px; height: 15px; vertical-align: top;'>");
                                } else {
                                    $(this).html("<span class='pointer hiddenIcon'>" + $(this).text() + "</span>");
                                }
                            }
                        } else if ($(this).index() == 3) {
                            var name = $(this).text();
                            $(this).text(maskName(name));
                        }
                    } else {
                        if ($(this).index() == 1) {
                            if ($(this).parent("tr").find("td:eq(7)").text() == 'Y') { // 비활성 여부 확인
                                if ($(this).children("span.pointer").width() > ($(this).width() - 40)) {
                                    // 비활성 상태 : 컬럼 크기에서 hidden 아이콘 값을 제외한 width 보다 내용이 클 경우
                                    $(this).html("<span class='pointer' style='width: 90%; text-overflow: ellipsis; overflow: hidden; float: left;'>"
                                            + $(this).text() + "</span>" + "<img src='" + makeAPIUrl("/resources/image/icon_hidden.png") + "' style='width: 40px; height: 15px; vertical-align: top;'>");
                                } else {
                                    $(this).html("<span class='pointer hiddenIcon'>" + $(this).text() + "</span>");
                                }
                            }
                        } else if ($(this).index() == 2) {
                            var name = $(this).text();
                            $(this).text(maskName(name));
                        }
                    }
                });

                //pageMove Max
                $('.ui-pg-input').on('keyup', function() {
                    this.value = this.value.replace(/\D/g, '');
                    if (this.value > $("#jqgridData").getGridParam("lastpage")) this.value = $("#jqgridData").getGridParam("lastpage");
                });

                //포인터
                $(this).find(".pointer").parent("td").addClass("pointer");

                resizeJqGridWidth("jqgridData", "gridArea");
            },
            loadError: function (jqXHR, textStatus, errorThrown) {
                $("#jqgridData").clearGridData();
                $("#sp_1_pageDiv").text(1);
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event){
                // 제목 셀 클릭시
                if (multiSel) {
                    if (columnId == 2) {
                        var list = $("#jqgridData").jqGrid('getRowData', rowId);
                        sessionStorage.setItem("last-url", location);
                        sessionStorage.setItem("state", "view");
                        pageMove("/customer/faq/"+list.faqSeq);
                    }
                } else {
                    if (columnId == 1) {
                        var list = $("#jqgridData").jqGrid('getRowData', rowId);
                        sessionStorage.setItem("last-url", location);
                        sessionStorage.setItem("state", "view");
                        pageMove("/customer/faq/"+list.faqSeq);
                    }
                }
            }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
}

function regerTitleAttr(rowId, val, rawObject, cm, rdata) {
    var retVal = maskName(rawObject.cretrNm);
    return 'title="' + retVal  + '"';
}

function keywordSearch() {
    gridState = "SEARCH";
    jQuery("#jqgridData").trigger("reloadGrid");
}

searchFieldSet = function() {
    if ($("#target > option").length == 0) {
        var searchHtml = "";
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        for (var i=0; i<colModel.length; i++) {
            //검색제외추가
            switch(colModel[i].name){
            case "faqTitle":
                break;
            case "cretrNm":
                break;
            case "faqCtg":
                break;
            default:
                continue;
            }
            searchHtml += "<option value=\"" + colModel[i].index + "\">" + colNames[i] + "</option>";
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
    $("#target").val(searchField);
}

function deleteFaq() {
    var faqSeqList = "";
    var delCnt = 0;

    for (var i = 1; i<$("#limit").val()+1; i++) {
        if ($("#jqg_jqgridData_" + i).prop("checked")) {
            var selFaqSeq = $("#jqgridData").jqGrid('getRowData', i).faqSeq;
            faqSeqList += selFaqSeq + ",";
            delCnt++;
        }
    }

    if (delCnt == 0) {
        popAlertLayer(getMessage("faq.delete.not.select.msg"));
        return;
    }

    popConfirmLayer(getMessage("faq.delete.list.confirm.msg"), function() {
        formData("NoneForm" , "faqSeqList", faqSeqList.slice(0, -1));
        callByDelete("/api/faq", "deleteFaqSuccess", "NoneForm","deleteFail");
    }, null, getMessage("common.confirm"));
}

deleteFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.delete"));
}

deleteFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.delete"), function(){location.reload();});
    } else {
        popAlertLayer(getMessage("fail.common.delete"));
    }
}