/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    limitInputTitle("faqTitle");

    setkeyup();

    faqTypeList();

    listBack($(".btnCancel"));
});

faqTypeList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_SE", "faqTypeListSuccess", "NoneForm", "faqTypeListFail");
}

faqTypeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var faqTypeListHtml = "";
        $(data.result.codeList).each(function(i, item) {
            faqTypeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#typeSelbox").html(faqTypeListHtml);

        faqCategoryList();
    } else {
        $(".btnWrite").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

faqTypeListFail = function(data) {
    $(".btnWrite").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

faqCategoryList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_CTG", "faqCategoryListSuccess", "NoneForm", "faqCategoryListFail");
}

faqCategoryListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var faqCategoryListHtml = "";
        $(data.result.codeList).each(function(i, item) {
            faqCategoryListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#ctgSelbox").html(faqCategoryListHtml);
    } else {
        $(".btnWrite").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

faqCategoryListFail = function(data) {
    $(".btnWrite").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

function registFaq() {
    if ($("#faqTitle").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.faqSbst.getData().length < 1) {
        popAlertLayer(getMessage("faq.answer.sbst.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        CKEDITOR.instances.faqSbst.updateElement();
        $("#faqSbst").val(CKEDITOR.instances.faqSbst.getData());

        formData("NoneForm", "faqTitle", $("#faqTitle").val());
        formData("NoneForm", "faqType", $("#typeSelbox").val());
        formData("NoneForm", "faqCtg", $("#ctgSelbox").val());
        formData("NoneForm", "faqSbst", $("#faqSbst").val());

        callByPost("/api/faq", "registFaqSuccess", "NoneForm", "insertFail");
    }, null, getMessage("common.confirm"));
}

insertFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.insert"));
}

registFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.insert"), "/customer/faq");
    } else {
        popAlertLayer(getMessage("fail.common.insert"));
    }
}
