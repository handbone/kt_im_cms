/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var faqTypeCd;
var faqCtgCd;

$(document).ready(function(){
    limitInputTitle("faqTitle");

    setkeyup();

    faqInfo();
});

faqInfo = function() {
    formData("NoneForm", "faqSeq", $("#faqSeq").val());
    formData("NoneForm", "searchType", "edit");
    callByGet("/api/faq", "faqInfoSuccess", "NoneForm", "faqInfoFail");
}

faqInfoSuccess = function(data) {
    formDataDelete("NoneForm", "faqSeq");
    if (data.resultCode == "1000") {
        var faqDetail = data.result.faqDetail;
        faqTypeCd = faqDetail.faqTypeCode;
        faqCtgCd = faqDetail.faqCtgCode;

        $("#faqTitle").val(faqDetail.faqTitle);

        var strFaqSbst = strConv(faqDetail.faqSbst);
        strFaqSbst = strConv(strFaqSbst);
        CKEDITOR.instances.faqSbst.setData(strFaqSbst);

        $("input[name='delYn'][value=" + faqDetail.delYn + "]").attr("checked", true);

        listBack($(".btnCancel"));

        faqTypeList(faqDetail.cretrMbrSe);
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/customer/faq");
    }
}

faqInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/customer/faq");
}

faqTypeList = function(cretrMbrSe) {
    callByGet("/api/faqType?cretrMbrSe=" + cretrMbrSe, "faqTypeListSuccess", "NoneForm", "faqTypeListFail");
}

faqTypeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var faqTypeListHtml = "";
        $(data.result.typeList).each(function(i, item) {
            faqTypeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#typeSelbox").html(faqTypeListHtml);
        $("#typeSelbox").val(faqTypeCd);

        faqCategoryList();
    } else {
        $(".btnModify").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

faqTypeListFail = function(data) {
    $(".btnModify").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

faqCategoryList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_CTG", "faqCategoryListSuccess", "NoneForm", "faqCategoryListFail");
}

faqCategoryListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var faqCategoryListHtml = "";
        $(data.result.codeList).each(function(i, item) {
            faqCategoryListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNm + "</option>";
        });
        $("#ctgSelbox").html(faqCategoryListHtml);
        $("#ctgSelbox").val(faqCtgCd);
    } else {
        $(".btnModify").hide();
        popAlertLayer(getMessage("fail.common.select"));
    }
}

faqCategoryListFail = function(data) {
    $(".btnModify").hide();
    popAlertLayer(getMessage("fail.common.select"));
}

function updateFaq() {
    if ($("#faqTitle").val().trim() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.faqSbst.getData().length < 1) {
        popAlertLayer(getMessage("faq.answer.sbst.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        CKEDITOR.instances.faqSbst.updateElement();
        $("#faqSbst").val(CKEDITOR.instances.faqSbst.getData());

        formData("NoneForm", "faqSeq", $("#faqSeq").val());
        formData("NoneForm", "faqTitle", $("#faqTitle").val());
        formData("NoneForm", "faqType", $("#typeSelbox").val());
        formData("NoneForm", "faqCtg", $("#ctgSelbox").val());
        formData("NoneForm", "faqSbst", $("#faqSbst").val());
        formData("NoneForm", "delYn", $('input:radio[name=delYn]:checked').val());

        callByPut("/api/faq", "updateFaqSuccess", "NoneForm", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data) {
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

updateFaqSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/customer/faq");
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}