/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var _ajaxAsync = true;   // 비동기 여부
var consoleLog = false;  // console 출력 여부
jQuery.support.cors = true; // for IE cross domain ajax call

function makeAPIUrl(url,type) {
    var APIURL;
    if(type == "img"){
        APIURL = $("#contextPath").val()+"/api/imgUrl?filePath="+url;
    }else{
        APIURL = $("#contextPath").val() + url;
    }

    return APIURL;
}

function callByGetWithoutLoading(url, callbackFunc, formId, callbackFailFunc) {
    callByGetWithoutLoaing_org(_ajaxAsync, url, callbackFunc, formId, callbackFailFunc);
}
function callByGetWithoutLoaing_org(ajaxAsync, url, callbackFunc, formId, callbackFailFunc) {
    var formData = formId != null && formId != "" ? $("#" + formId).serialize() : "";

    url = makeAPIUrl(url);
    if(consoleLog){
        console.log(url);
    }
    $.ajax({
        type: "get",
        async: ajaxAsync,
        url: url,
        data: formData,
        dataType : "json",
        cache: false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        /*
        beforeSend:function(){
            $("#loading").show();
        },
        complete:function(){
            $("#loading").hide();
        },
        */
        success: function(data, textStatus, jqXHR) {
            if(consoleLog){
                console.log("Success=====S");
                logView(data);
                console.log("Success=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFunc](data, jqXHR);
            } else {
                if (isFunction(callbackFunc)) {
                    window[callbackFunc](data);
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if(consoleLog){
                console.log("error=====S");
                logView(jqXHR);
                console.log("error=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFailFunc](jqXHR);
            } else {
                handleErrorStatus(jqXHR);
            }
        }
    });
}

function callByGet(url, callbackFunc, formId, callbackFailFunc) {
    callByGet_org(_ajaxAsync, url, callbackFunc, formId, callbackFailFunc);
}
function callByGet_org(ajaxAsync, url, callbackFunc, formId, callbackFailFunc) {
    var formData = formId != null && formId != "" ? $("#" + formId).serialize() : "";

    url = makeAPIUrl(url);
    if(consoleLog){
        console.log(url);
    }
    $.ajax({
        type: "get",
        async: ajaxAsync,
        url: url,
        data: formData,
        dataType : "json",
        cache: false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend:function(){
            $("#loading").show();
        },
        complete:function(){
            $("#loading").hide();
        },
        success: function(data, textStatus, jqXHR) {
            if(consoleLog){
                console.log("Success=====S");
                logView(data);
                console.log("Success=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFunc](data, jqXHR);
            } else {
                if (isFunction(callbackFunc)) {
                    window[callbackFunc](data);
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if(consoleLog){
                console.log("error=====S");
                logView(jqXHR);
                console.log("error=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFailFunc](jqXHR);
            } else {
                handleErrorStatus(jqXHR);
            }
        }
    });
}

function callByPost(url, callbackFunc, formId, callbackFailFunc) {
    callByPost_org(_ajaxAsync, url, callbackFunc, formId, callbackFailFunc);
}

function callByPostSync(url, callbackFunc, formId, callbackFailFunc) {
    callByPost_org(false, url, callbackFunc, formId, callbackFailFunc);
}

function callByPost_org(ajaxAsync, url, callbackFunc, formId, callbackFailFunc) {
    var formData = formId != null ? $("#" + formId).serialize() : "";

    url = makeAPIUrl(url);
    if(consoleLog){
        console.log(url);
    }
    $.ajax({
        type: "post",
        async: ajaxAsync,
        url: url,
        data: formData,
        dataType : "json",
        cache: false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            if(consoleLog){
                console.log("Success=====S");
                logView(data);
                console.log("Success=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFunc](data, jqXHR);
            } else {
                if (isFunction(callbackFunc)) {
                    window[callbackFunc](data);
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if(consoleLog){
                console.log("error=====S");
                logView(jqXHR);
                console.log("error=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFailFunc](jqXHR);
            } else {
                handleErrorStatus(jqXHR);
            }
        }
    });
}

function callByPut(url, callbackFunc, formId, callbackFailFunc) {
    callByPut_org(_ajaxAsync, url, callbackFunc, formId, callbackFailFunc);
}
function callByPut_org(ajaxAsync, url, callbackFunc, formId, callbackFailFunc) {
    var objMethod = document.createElement("input");
    objMethod.type = "hidden";
    objMethod.name = "_method";
    objMethod.value = "PUT";
    document.getElementById(formId).insertBefore(objMethod, null);

    var formData = formId != null ? $("#" + formId).serialize() : "";
    url = makeAPIUrl(url);
    if(consoleLog){
        console.log(url);
    }
    $.ajax({
        type: "post",
        async: ajaxAsync,
        url: url,
        data: formData,
        dataType : "json",
        cache: false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            if(consoleLog){
                console.log("Success=====S");
                logView(data);
                console.log("Success=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFunc](data, jqXHR);
            } else {
                if (data.resultCode == "1000" || data.resultCode == "1010" || data.resultCode == "1011" || data.resultCode == "1012" || data.resultCode == "1013") {
                    if (isFunction(callbackFunc)) {
                        window[callbackFunc](data);
                    }
                } else {
                    checkStatus(data);
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if(consoleLog){
                console.log("error=====S");
                logView(jqXHR);
                console.log("error=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFailFunc](jqXHR);
            } else {
                handleErrorStatus(jqXHR);
            }
        }
    });
}

function callByDelete(url, callbackFunc, formId, callbackFailFunc) {
    callByDelete_org(_ajaxAsync, url, callbackFunc, formId, callbackFailFunc);
}
function callByDelete_org(ajaxAsync, url, callbackFunc, formId, callbackFailFunc) {
    var objMethod = document.createElement("input");
    objMethod.type = "hidden";
    objMethod.name = "_method";
    objMethod.value = "DELETE";
    document.getElementById(formId).insertBefore(objMethod, null);

    var formData = formId != null ? $("#" + formId).serialize() : "";
    url = makeAPIUrl(url);
    if(consoleLog){
        console.log(url);
    }
    $.ajax({
        type: "post",
        async: ajaxAsync,
        url: url,
        data: formData,
        dataType : "json",
        cache: false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            if(consoleLog){
                console.log("Success=====S");
                logView(data);
                console.log("Success=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFunc](data, jqXHR);
            } else {
                if (data.resultCode == "1000" || data.resultCode == "1010") {
                    if (isFunction(callbackFunc)) {
                        window[callbackFunc](data);
                    }
                } else {
                    checkStatus(data);
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if(consoleLog){
                console.log("error=====S");
                logView(jqXHR);
                console.log("error=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFailFunc](jqXHR);
            } else {
                handleErrorStatus(jqXHR);
            }
        }
    });
}
var fileAccss;
function callByMultipart(url, callbackFunc, formId, callbackFailFunc) {
    if(consoleLog){
        console.log(url);
    }
    fileAccss = true;
    url = makeAPIUrl(url);
    $("#"+formId).ajaxForm({
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        beforeSend: function() {
        },
        complete: function(jqXHR, textStatus) {
            if(consoleLog){
                console.log("complete=====S");
                logView(data);
                console.log("complete=====E");
            }
            var data = JSON.parse(jqXHR.responseText);
            fileAccss = true;
            if (isFunction(callbackFailFunc)) {
                window[callbackFunc](data, jqXHR);
            } else {
                if (data.resultCode == "1000" || data.resultCode == "1010") {
                    window[callbackFunc](data);
                } else {
                    checkStatus(data);
                }
            }
        },
        error: function(jqXHR, textStatus){
            fileAccss = false;
            if(consoleLog){
                console.log("error=====S");
                logView(jqXHR);
                console.log("error=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFailFunc](jqXHR);
            } else {
                handleErrorStatus(jqXHR);
            }
        }
    });

    $("#"+formId).attr("action", url);
    $("#"+formId).submit();
}

function outCallByGet(url, callbackFunc, formId, callbackFailFunc) {
    outCallByGet_org(_ajaxAsync, url, callbackFunc, formId, callbackFailFunc);
}
function outCallByGet_org(ajaxAsync, url, callbackFunc, formId, callbackFailFunc) {
    var formData = formId != null ? $("#" + formId).serialize() : "";
    url = makeAPIUrl(url, "youtube");
    if(consoleLog){
        console.log(url);
    }
    $.ajax({
        type: "get",
        async: ajaxAsync,
        url: url,
        data: formData,
        dataType : "jsonp",
        cache: false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            if(consoleLog){
                console.log("Success=====S");
                logView(data);
                console.log("Success=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFunc](data, jqXHR);
            } else {
                window[callbackFunc](data);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if(consoleLog){
                console.log("error=====S");
                logView(jqXHR);
                console.log("error=====E");
            }
            if (isFunction(callbackFailFunc)) {
                window[callbackFailFunc](jqXHR);
            } else {
                handleErrorStatus(jqXHR);
            }
        }
    });
}

function checkStatus(data) {
    //console.log(JSON.stringify(data,null,4));
}

function handleErrorStatus(jqXHR, forbidden) {
    if (jqXHR.status == 401) {
        document.location.href = makeAPIUrl("/");
    } else if (jqXHR.status == 403) {
        //console.log(JSON.stringify(jqXHR.responseText,null,4));

    } else if (jqXHR.status == 404) {
        //console.log(JSON.stringify(jqXHR.responseText,null,4));
    } else if (jqXHR.status == 400) {
        //console.log(JSON.stringify(jqXHR.responseText,null,4));
    } else if (jqXHR.status == 405) {

    } else if (jqXHR.status == 500) {
        //console.log(JSON.stringify(jqXHR.responseText,null,4));
    } else {
        //console.log(JSON.stringify(jqXHR.responseText,null,4));
    }
}


function logView(data){
    console.log(JSON.stringify(data,null,4));
}

/**
 * API 호출 시 데이터 전달 함수
 * @param {Object} id Form 아이디
 * @param {Object} name 파라메타 명
 * @param {Object} value 파라메타 값
 */
function formData(id ,name, value){
    var objMethod = document.createElement("input");
    objMethod.type = "hidden";
    objMethod.name = name;
    objMethod.value = value;
    document.getElementById(id).insertBefore(objMethod, null);
}

/**
 * API 호출 시 데이터 전달 Form 삭제 함수
 * @param {Object} id Form 아이디
 * @param {Object} name 파라메타 명
 */
function formDataDelete(id , name){
    $("#"+id+" input[name=\""+name+"\"]").remove();
}

/**
 * API 호출 시 데이터 전달 Form 전체 삭제 함수
 * @param {Object} id Form 아이디
 */
function formDataDeleteAll(id){
    $("#"+id).children().remove();
}

function isFunction(functionName) {
    return (typeof window[functionName] == "function");
}