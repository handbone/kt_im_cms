/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */


/* 현재 이용 브라우저 값*/
var useBrower = navigator.userAgent.toLowerCase();

/* 현재 이용 브라우저 비교 값 저장*/
Brower = {
        ie : useBrower.indexOf('msie') != -1,
        ie6 : useBrower.indexOf('msie 6') != -1,
        ie7 : useBrower.indexOf('msie 7') != -1,
        ie8 : useBrower.indexOf('msie 8') != -1,
        ie9 : useBrower.indexOf('msie 9') != -1,
        ie10 : useBrower.indexOf('msie 10') != -1,
        opera : !!window.opera,
        safari : useBrower.indexOf('safari') != -1,
        safari3 : useBrower.indexOf('applewebkir/5') != -1,
        mac : useBrower.indexOf('mac') != -1,
        chrome : useBrower.indexOf('chrome') != -1,
        firefox : useBrower.indexOf('firefox') != -1
}


function isIE () {
    var myNav = navigator.userAgent.toLowerCase();
    if((myNav.indexOf('msie') != -1)) {
        return parseInt(myNav.split('msie')[1]);
    }else {
        return (myNav.indexOf('MSIE'))? parseInt(myNav.split('MSIE')[1]): false;
    }
}
if(typeof swal != 'undefined'){
    var proxied = swal;
}
var swal = function() {
    if(Brower.ie7 || Brower.ie8) {
        if(typeof arguments[0]['showCancelButton'] == 'boolean') {
            var result = confirm(arguments[0]['title']);
            if(arguments[arguments.length-1] != 0 && typeof arguments[arguments.length-1] == "function") {
                arguments[arguments.length-1](result);
            } else {
                return result;
            }
        } else {
            if(typeof arguments[0] == 'string') {
                alert(arguments[0]);
            } else {

            }
        }
    } else {
        if(Brower.ie9 && (typeof arguments[0]!='Object')) {
            if(arguments[2]=="success") {
                var temp = new Array();
                temp['imageUrl']='/images/success.png';
                temp['title']=arguments[0];
                temp['text']=arguments[1];
                arguments[0] = temp;
                arguments[1] = null;
                arguments[2] = null;
            }
            if(arguments[2]=="error") {
                var temp = new Array();
                temp['imageUrl']='/images/error.png';
                temp['title']=arguments[0];
                temp['text']=arguments[1];
                arguments[0] = temp;
                arguments[1] = null;
                arguments[2] = null;
            }
        }
        return proxied.apply( this, arguments );
    }
};

var reloadFlag = true;
var fistPopup = true;
$(document).ready(function(){
    if (isFunction("getMessage")) {
        $(".searchInput > input").attr("placeholder", getMessage("input.placeholer.research"));
    }

    /* ie8 대비 방편 코드*/
    if (Brower.ie8) {
        if (typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }

        if (typeof Array.prototype.forEach != 'function') {
            Array.prototype.forEach = function(callback){
              for (var i = 0; i < this.length; i++){
                callback.apply(this, [this[i], i, this]);
              }
            };
        }
    }
});


var pageUrl;
var ContextPath;
var glo_url;
init = function(pageUrlTemp,CntxtPath,url){
    pageUrl = pageUrlTemp;
    ContextPath = CntxtPath;
    glo_url = url;
    var chkUrl = url.replace(ContextPath,"");

    if (chkUrl != "/mypage/edit" && !chkUrl.match("/member/edit") && !(chkUrl.match("/service/") && chkUrl.match("/edit")) && !(chkUrl.match("/store/") && chkUrl.match("/edit")) && !(chkUrl.match("/cp/") && chkUrl.match("/edit"))) {
        sessionStorage.removeItem('cofirm-Pwd');
        sessionStorage.setItem("cofirm-last-url", location.href);
    } else {
        formData("NoneForm" , "type", "comfirmEdit");
        formData("NoneForm" , "pwd", sessionStorage.getItem("cofirm-Pwd"));
        callByGet("/api/member/find" , "initFindUserInfo", "NoneForm", "initNotFindUserInfo");
        formDataDeleteAll("NoneForm");
    }
}

/**
 * 페이지 이동 함수
 * @param {Object} url URL 주소 값
 */
function pageMove(url){
    // full domain 주소가 아닌 경우에만 contextPath 추가
    if (url.indexOf("http") == -1) {
        url = makeAPIUrl(url);
    }
    //defer 가져오기
    location.href = url;
}
function pageMove_reload(url){
    // full domain 주소가 아닌 경우에만 contextPath 추가
    if (url.indexOf("http") == -1) {
        url = makeAPIUrl(url);
    }
    //defer 가져오기
    location.href = url;
    if(reloadFlag){
        location.reload();
        reloadFlag = false
    }
}

/**
 * Window Resize Event 발생 시에 Popup Layer 위치 변경
 */
function popupResize(layerId) {
    var wHeight = $(window).height();
    var wWidth = $(window).width();
    var sTop = document.body.scrollTop == 0 ? document.documentElement.scrollTop : document.body.scrollTop;
    var sLeft = document.body.scrollLeft == 0 ? document.documentElement.scrollLeft : document.body.scrollLeft;

    var offsetTop = (wHeight - $("#" + layerId).height()) /2 + sTop;
    var offsetLeft = (wWidth - $("#" + layerId).width()) /2 + sLeft;
    offsetTop = offsetTop > 0 ? offsetTop : 0;
    offsetLeft = offsetLeft > 0 ? offsetLeft : 0;

    $("#" + layerId).offset({
        top: offsetTop,
        left: offsetLeft
    });

}

/**
 * Vertical Middle, Horizontal Center Popup Layer Load
 * (페이지 내 DIV 영역을 popup으로 띄움)
 * @param layerId : DIV id
 * @param width : Popup Layer width
 * @param height : Popup Layer height
 * @param changePosition : Window Resize Event 시에 위치 변경 여부
 */
function popLayerDiv(layerId, width, height, changePosition)
{
    var offsetTop = ($(window).height() - height) /2;
    var offsetLeft = ($(window).width() - width) /2;
    offsetTop = (offsetTop > 0)? offsetTop:0;
    offsetLeft = (offsetLeft > 0)? offsetLeft:0;
    $.blockUI({
        message: $("#" + layerId),
        css: {
            border: 'none',
            top:  offsetTop +300+'px',
            left: offsetLeft+150+ 'px',
            width: '0px',
            height: '0px',
            textAlign: "",
            cursor:null
        }
    });
    $("body").css("overflow-y" , "hidden");
    $("body").children().filter(".blockUI").css("cursor", "default");
    if(changePosition)
    {
        popupResize(layerId);
        $(window).resize(function(){
            if($("#" + layerId).length == 1 && $("#" + layerId).css("display")=="block")
            {
                popupResize(layerId);
            }
        });
    }
}


/**
 * Popup Layer 닫기
 */
function popLayerClose()
{
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}


/**
 * Page Navigation 그리기
 *
 * @param totCnt    총 건수
 */
var _blockSize = 10;
function drawPaging(totCnt,offset,limit,searchFunc,pagingDiv) {
    pageNo = toInt(offset) / toInt(limit) + 1;
    pageSize = toInt(limit);
    var totPageCnt = toInt(totCnt / pageSize) + (totCnt % pageSize > 0 ? 1 : 0);
    var totBlockCnt = toInt(totPageCnt / _blockSize) + (totPageCnt % _blockSize > 0 ? 1 : 0);
    var blockNo = toInt(pageNo / _blockSize) + (pageNo % _blockSize > 0 ? 1 : 0);
    var startPageNo = (blockNo - 1) * _blockSize + 1;
    var endPageNo = blockNo * _blockSize;

    if (endPageNo > totPageCnt) {
        endPageNo = totPageCnt;
    }
    var prevBlockPageNo = (blockNo - 1) * _blockSize;
    var nextBlockPageNo = blockNo * _blockSize + 1;

    var strHTML = "<ul>";
    if (totPageCnt > 1 && pageNo != 1) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(1);\"><img src=\"/resources/image/btn_fast_rewind.gif\" width=\"15\" height=\"15\"></a></li>";

    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\"><img src=\"/resources/image/btn_fast_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    if (pageNo > 1) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + (pageNo-1) + ");\" ><img src=\"/resources/image/btn_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    var numberStyle = "", numberClass = "";
    for (var i = startPageNo; i <= endPageNo; i++) {
        numberStyle = (i == pageNo) ? "font-weight:bold;  letter-spacing:-1px;" : "";
        strHTML += "<li><a href=\"javascript:"+searchFunc+"(" + i + ");\" style='color:#3677b2;" + numberStyle + "'>" + i + "</a></li>";

    }
    if (totCnt == 0) {
        strHTML += "<li><a href=\"javascript:search(1);\">1</a></li>";

    }
    if (pageNo < totPageCnt) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + (pageNo+1) + ");\" ><img src=\"/resources/image/btn_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    if (totPageCnt > 1 && pageNo != totPageCnt) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + totPageCnt + ");\" ><img src=\"/resources/image/btn_fast_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_fast_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    }

    strHTML += "</ul>";
    $('#'+pagingDiv).html(strHTML);

}
/**
 * 검색 API offset parameter 값 계산
 *
 * @param pageNo    페이지번호
 * @return offset
*/
function getSearchOffset(pageNo) {
    return (pageNo - 1) * toInt($("#limit").val());
}


function toInt(str) {
    var n = null;
    try {
        n = parseInt(str, 10);
    } catch (e) {}
    return n;
}


var CALL_COUNT            = 0;
var CALL_SUCCESS_COUNT    = 0;
var CALL_TOT_COUNT        = 0;
var alertFlag = true;
function jsCallComplete() {
    if ( CALL_COUNT == CALL_TOT_COUNT ) {
        if ( CALL_SUCCESS_COUNT == CALL_COUNT ) {
            if(alertFlag){
                alertFlag = false;
                contentsXmlCreate();
            }
        }else{
            setTimeout("jsCallComplete()", 100);
        }
    }
    else {
        setTimeout("jsCallComplete()", 100);
    }
}
function jsCallComplete1() {
    if ( CALL_COUNT == CALL_TOT_COUNT ) {
        if ( CALL_SUCCESS_COUNT == CALL_COUNT ) {
            if(alertFlag){
                alertFlag = false;
                contentsXmlCreate();
            }
        }else{
            setTimeout("jsCallComplete1()", 100);
        }
    }
    else {
        setTimeout("jsCallComplete1()", 100);
    }
}


function strConv(str){
    str = str.replace(/&lt;/gi,"<");
    str = str.replace(/&gt;/gi,">");
    str = str.replace(/&quot;/gi,"\"");
    //str = str.replace(/&nbsp;/gi," ");
    str = str.replace(/&amp;/gi,"&");
    str = str.replace(/&amp;#034;/gi,"\"");
    str = str.replace(/&#034;/gi, "\"");
    return str;
}



//jqgrid 포인터
function pointercursor(cellvalue, options, rowObject)
{
    var new_formatted_cellvalue = '<span class="pointer">' + cellvalue + '</span>';

    return new_formatted_cellvalue;
}

function userInfoValidateCheck(type, memberType){
    // 등록일때
    if(type == "r"){
        if($("#memberId").val() == ""){
            popAlertLayer("아이디를 입력하세요");
            return false;
        } else if(!duplicationFlag){
            popAlertLayer("아이디를 중복체크해주세요.");
            return false;
        }

        if($("#memberPwd").val() == ""){
            popAlertLayer("비밀번호를 입력해주세요.");
            return false;
        } else if($("#memberPwd").val() !=$("#memberPwdRe").val()){
            popAlertLayer("비밀번호가 일치하지 않습니다.");
            return false;
        } else {
            var pwdRegular = /[a-z|0-9]{6,15}$/gi;
            if(!pwdRegular.test($("#memberPwd").val())){
                popAlertLayer("비밀번호 입력 양식을 확인하여 주십시오.");
                $("#memberPwd").val("");
                $("#pwdValueChk").css("display", "block");
                return false;
            } else if($("#memberPwd").val().search(/[0-9]/g) < 0 || $("#memberPwd").val().search(/[a-z]/g) < 0 ){
                $("#pwdValueChk").css("display", "block");
                return false;
            }
        }
    }
    // 수정일 때
    else if(type == "e"){
        if($("#memberPwd").val() != ""){
            if($("#memberPwd").val() !=$("#memberPwdRe").val()){
                popAlertLayer("비밀번호가 일치하지 않습니다.");
                return false;
            } else {
                var pwdRegular = /[a-z|0-9]{6,15}$/gi;
                if(!pwdRegular.test($("#memberPwd").val())){
                    popAlertLayer("비밀번호 입력 양식을 확인하여 주십시오.");
                    $("#pwdValueChk").css("display", "block");
                    $("#memberPwd").val("");
                    return false;
                } else if($("#memberPwd").val().search(/[0-9]/g) < 0 || $("#memberPwd").val().search(/[a-z]/g) < 0 ){
                    $("#pwdValueChk").css("display", "block");
                    return false;
                }
            }
        }
    }
    $("#pwdValueChk").css("display", "none");


    if($("#memberName").val() == ""){
        popAlertLayer("고객명을 입력해주세요.");
        return false;
    } else {
        var nameRegular = /^[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]{2,10}$/gi;
        if(!nameRegular.test($("#memberName").val())){
            popAlertLayer("고객명 입력 양식을 확인하여 주십시오.");
            $("#memberName").val("");
            return false;
        }
    }

    // 관리자일때
    if(memberType == "admin"){
        var phoneRegular1 = /[0-9]{2,3}/;
        var phoneRegular2 = /[0-9]{3,4}/;
        var phoneRegular3 = /[0-9]{4}/;
        if(!phoneRegular1.test($("#memberPhone1").val()) || !phoneRegular2.test($("#memberPhone2").val())
            || !phoneRegular3.test($("#memberPhone3").val())){
            popAlertLayer("전화번호를 정확히 입력해 주십시오.");
            return false;
        }
    }

    var mobilePhoneRegular1 = /[0-9]{3}/;
    var mobilePhoneRegular2 = /[0-9]{3,4}/;
    var mobilePhoneRegular3 = /[0-9]{4}/;
    if(!mobilePhoneRegular1.test($("#mobilePhone1").val()) || !mobilePhoneRegular2.test($("#mobilePhone2").val())
        || !mobilePhoneRegular3.test($("#mobilePhone3").val())){
        popAlertLayer("휴대폰 번호를 정확히 입력해 주십시오.");
        return false;
    }

    var email1Regular = /[a-z|0-9]$/gi;
    if(!email1Regular.test($("#memberEmail1").val())){
        popAlertLayer("이메일 입력 양식을 확인하여 주십시오.");
        $("#memberEmail1").val("");
        return false;
    }
    var email2Regular = /([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+)$/;
    if(!email2Regular.test($("#memberEmail2").val())){
        popAlertLayer("이메일 입력 양식을 확인하여 주십시오.");
        $("#memberEmail2").val("");
        return false;
    }
    return true;
}

function userPwdChange(){
    var pwdRegular = /[a-z|0-9]{6,15}$/gi;
    if(!pwdRegular.test($("#memberPwd").val())){
        $("#pwdValueComplete").text("사용 불가능한 비밀번호입니다.");
        $("#memberPwd").val("");
        return;
    } else if($("#memberPwd").val().search(/[0-9]/g) < 0 || $("#memberPwd").val().search(/[a-z]/g) < 0 ){
        $("#pwdValueComplete").text("사용 불가능한 비밀번호입니다.");
        $("#memberPwd").val("");
        return;
    } else {
        $("#pwdValueComplete").text("사용 가능한 비밀번호입니다.");
    }

}

function cutString(object, maxLength){
    var strLength = 0;
    var newStr = '';
    var str = "";
    if(typeof object == "object"){
        str = object.value;
    } else {
        str = object;
    }

    for (var i=0;i<str.length; i++) {
        var n = str.charCodeAt(i);
        var nv = str.charAt(i); // charAt : string 개체로부터 지정한 위치에 있는 문자를 꺼낸다.
        if ((n>= 0)&&(n<256)) {
            strLength ++; // ASCII 문자코드 set.
        } else {
            strLength += 2; // 한글이면 2byte로 계산한다.
        }

        if (strLength>maxLength) {
            break; // 제한 문자수를 넘길경우.
        } else {
            newStr = newStr + nv;
        }
    }

    if(typeof object == "object"){
        object.value = newStr;
    } else {
        return newStr;
    }
}

function valueFormatter(value, length){
    if(value.length <= length){
        return value;
    } else {
        return value.substring(0, length) + "...";
    }
}

/*str에서 해당 parameter 값 가져오기 ex) page=5&val=5 */
function findGetParameter(str,parameterName) {
    var result = null,
        tmp = [];
    var items = str.split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}
/*뒤로가기*/

/*목록(뒤로가기)*/
function listBack(obj,str){
    if(typeof str != "undefined"){
        location.href = str;
    }

    if(sessionStorage.getItem("state") == "view"){
        $(obj).attr("onclick","listBack(this,'"+sessionStorage.getItem("last-url")+"')");
    }
}

function listBackConfirmRegist(obj,str) {
    if(typeof str != "undefined"){
        location.href = str;
    }

    if(sessionStorage.getItem("state") == "view"){
        $(obj).attr("onclick","popConfirmLayer(getMessage('common.regist.cancel.msg'), function(){listBack(this,'"+sessionStorage.getItem("last-url")+"')});");
    }
}

function listBackConfirmEdit(obj,str) {
    if(typeof str != "undefined"){
        location.href = str;
    }

    if(sessionStorage.getItem("state") == "view"){
        $(obj).attr("onclick","popConfirmLayer(getMessage('common.update.cancel.msg'), function(){listBack(this,'"+sessionStorage.getItem("last-url")+"')});");
    }
}

/* 목록(뒤로가기) - 수정버전*/
function setListButton(options) {
    var obj = options.obj; // Jquery 오브젝트
    var defaultUri = options.defaultUri; // 기본 uri
    var extraAllowedUriList = options.extraAllowedUriList; // 추가적으로 세션을 허용하는 uri
    var useConfirmPopup = options.useConfirmPopup || false; // Confirm 팝업 사용 여부
    var confirmPopupMsg = options.useConfirmPopupMsg || "common.cancel.msg"; // Confirm 팝업 표시 메세지 아이디
    if (defaultUri == "" || defaultUri == null || defaultUri == undefined) {
        return;
    }

    var clickFunction;
    var isAllowedSessonUrl = true;
    var sessionUrl = sessionStorage.getItem("last-url");
    if (sessionStorage.getItem("state") != "view" || sessionUrl == null || sessionUrl == undefined || sessionUrl == "") {
        isAllowedSessonUrl = false;
    } else {
        var isAllowedSessonUrl = (sessionUrl.indexOf(defaultUri) != -1);
        if (!isAllowedSessonUrl && extraAllowedUriList) {
            $(extraAllowedUriList).each(function(i, item) {
                if (sessionUrl.indexOf(item) != -1) {
                    isAllowedSessonUrl = true;
                    return false;
                }
            });
        }
    }

    if (useConfirmPopup) {
        if (isAllowedSessonUrl) {
            clickFunction = function() {
                popConfirmLayer(getMessage(confirmPopupMsg), function() {
                    location.href = sessionUrl;
                }, null, getMessage("button.confirm"));
            };
        } else {
            clickFunction = function() {
                popConfirmLayer(getMessage(confirmPopupMsg), function() {
                    location.href = makeAPIUrl(defaultUri);
                }, null, getMessage("button.confirm"));
            };
        }
    } else {
        if (isAllowedSessonUrl) {
            clickFunction = function() { location.href = sessionUrl; };
        } else {
            clickFunction = function() { location.href = makeAPIUrl(defaultUri); };
        }
    }

    $(obj).removeAttr("onclick");

    $(obj).click(clickFunction);
}

/*글자 조회*/
function setkeyup(){
    $('.remaining').each(function() {
        var $maximumCount = $(this).attr("max") * 1;
        var $input = $(this);
        var update = function() {
            var now = $maximumCount - $input.val().length;
            // 사용자가 입력한 값이 제한 값을 초과하는지를 검사한다.
            if (now < 0) {
                var str = $input.val();
                popAlertLayer(getMessage("common.input.max.limit.first.msg") + ' ' + $maximumCount + getMessage("common.input.max.limit.second.msg"));
                $input.val(str.substr(0, $maximumCount));
                if ($input.hasClass("onlynum")) {
                    $input.val($input.val().replace(/[^\d]+/g, ''));
                } else {
                    $input.val($input.val().replace(/[\<>&\"']/gi, ''));
                }
                now = 0;
            }
        }
        $input.bind('input keyup paste', function() {
            setTimeout(update, 0)
        });
        update();
    })
}

// 특문 입력 제한 (&, <, >, ", ')
function limitInputTitle(ids) {
    var idList = ids.split(",");
    for (var i in idList) {
        var ida = idList[i];
        $("#" + ida).bind("keyup",function() {
            re = /[\<>&\"']/gi;
            var temp=$(this).val();
            if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
                $(this).val(temp.replace(re,""));
            }
        });
    }
}

function limitPkgName(ids) {
    var idList = ids.split(",");
    for (var i in idList) {
        var ida = idList[i];
        $("#" + ida).bind("keyup",function() {
            re = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣|~!@\#$%^&*\()\-=+_'"?<>`:;,]/gi;
            var temp=$(this).val();
            if (re.test(temp)) { // 영문과 특수문자 .만 사용 가능
                $(this).val(temp.replace(re,""));
            }
        });
    }
}

/* ul header*/
function setHeaderUse(){
    var firstName = $(".leftMenuOn").text().trim();
    var secondName = $(".leftSubMenuOn").text().trim();
    var thirdName = $("#tabGrp .tabOn").text().trim() || $(".conTitle.tabOn").text().trim() || $(".conTitle").text().trim();

    var firstUrl = $(".leftMenuOn").next(".leftSubMenu").children("a").attr("onclick");
    var secondUrl = $(".leftSubMenuOn a").attr("onclick");
    var thirdUrl = "window.location.reload()";

    var sVal = $(".leftSubMenuOn").length;

    $("div.locate > ul").html("");
    $("div.locate > ul").append("<li class='iconHome'>&nbsp;</li>");
    var hasPrevious = false;
    if (firstName) {
        hasPrevious = true;
        $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + firstUrl + "\">" + firstName + "</li>");
    }
    if (sVal) {
        if (hasPrevious) {
            $("div.locate > ul").append("<li><img src='" + makeAPIUrl("/resources/image/gts.gif") + "'>&nbsp;</li>");
        }
        hasPrevious = true;
        $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + secondUrl + "\">" + secondName + "</li>");
    }

    if (thirdName) {
        if (hasPrevious) {
            $("div.locate > ul").append("<li><img src='" + makeAPIUrl("/resources/image/gts.gif") +  "'>&nbsp;</li>");
        }
        $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + thirdUrl + "\">" + thirdName + "</li>");
    }
}

function resizeJqGridWidth(grid_id, div_id){
    $(window).bind("resize", function() { // 그리드의 width 초기화
        var resizeWidth = $("#gridArea").width();

        $("#" + grid_id).setGridWidth(resizeWidth, true);
    }).trigger("resize");
}


function keyup(){
    $(".onlynum").keyup(function(e) {
        $(this).val($(this).val().replace(/[^\d]+/g, ''));
    });
}


function dateString(dayVal){
    var str = dayVal.getFullYear()+"-";
    var month = dayVal.getMonth()+1;
    var day = dayVal.getDate();
    if(month <10){        str += "0";        }
    str += (dayVal.getMonth()+1)+"-";
    if(day <10){        str += "0";        }
    str += dayVal.getDate();

    return str;
}

function dateTimeString(dayVal){
    var str = dayVal.getFullYear()+"-";
    var month = dayVal.getMonth()+1;
    var day = dayVal.getDate();
    if(month <10){        str += "0";        }
    str += (dayVal.getMonth()+1)+"-";
    if(day <10){        str += "0";        }
    str += dayVal.getDate();

    var hour = dayVal.getHours();
    str += " ";
    if(hour <10){        str += "0";        }
    str += hour+":";

    var minute = dayVal.getMinutes();
    if(minute <10){        str += "0";        }
    str += minute;

    return str;
}


function is_number(x)

{

    var reg = /^\d+$/;

    return reg.test(x);

}

function validatePassword(value, id) {
    var pwdRegular = /^(?=.*[0-9])(?=.*[a-zA-Z])[0-9a-zA-Z]{10,16}$/g;
    if (!pwdRegular.test(value)) {
        return "invalidSyntax"
    }

    var continuousCharRegular = /(\w)\1\1\1/;
    if (continuousCharRegular.test(value)) {
        return "continuousChar";
    }

    if (id && id !== undefined && id.length >= 4) {
        for (var i = 0; i + 3 < id.length; i++) {
            var idPart = id.substring(i, i + 4);
            if (value.indexOf(idPart) != -1) {
                return "containsIdPart";
            }
        }
    }

    return "valid";
}

function validateId(str) {
    if (!str || str.length <=0) {
        return false;
    }
    var regStr = /^[a-z]+[a-z|0-9]{5,14}$/g;
    return regStr.test(str);
}

function validateName(str) {
    if (!str || str.length <= 0) {
        return false;
    }
    var regStr = /^[가-힣a-zA-Z]{2,10}$/gi;
    return regStr.test(str);
}

function validateTelNumber(str) {
    if (!str) {
        return false;
    }

    str = str.trim();
    var temp = str.split('-').join('');
    if (temp.length <= 0 || temp.length < 9 || temp.length > 11) {
        return false;
    }
    var regStr = /^(02)-?([1-9]{1}[0-9]{2,3})-?[0-9]{4}$|(0505|0[3-8]{1}[0-5]{1})-?([1-9]{1}[0-9]{2,3})-?[0-9]{4}$/;
    return regStr.test(str);
}

function validatePhoneNumber(str) {
    if (!str) {
        return false;
    }

    str = str.trim();
    var temp = str.split('-').join('');
    if (temp.length <= 0 || temp.length < 10 || temp.length > 11) {
        return false;
    }
    var regStr = /^(01[16789]{1})-?([1-9]{1}[0-9]{2,3})-?[0-9]{4}$|(010)-?([1-9]{1}[0-9]{3})-?[0-9]{4}$/;
    return regStr.test(str);
}

function phoneNumberFormatter(num) {
    if (!num) {
        return num;
    }
    num = num.trim();
    return num.replace(/-/g, "").replace(/(^02.{0}|^0505.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
}

function validateBizNo(bizID) {
    if (!bizID) {
        return false;
    }

    // bizID는 숫자만 10자리로 해서 문자열로 넘긴다.
    var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
    var tmpBizID;
    var i;
    var chkSum = 0;
    var c2;
    var remander;
    bizID = bizID.replace(/-/gi,'');

    for (i=0; i<=7; i++)
        chkSum += checkID[i] * bizID.charAt(i);

    c2 = "0" + (checkID[8] * bizID.charAt(8));
    c2 = c2.substring(c2.length - 2, c2.length);
    chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
    remander = (10 - (chkSum % 10)) % 10 ;

    if (Math.floor(bizID.charAt(9)) == remander)
        return true ; // OK!

    return false;
}

function bizNoFormatter(num) {
    if (!num) {
        return num;
    }
    return num.replace(/-/g, "").replace(/(\d{3})(\d{2})(\d{5})/g, "$1-$2-$3");
}

function validateEmail(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var regStr = /^([0-9a-zA-Z+_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;
    return regStr.test(str);
}

function validateEmailId(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var regStr = /^([0-9a-zA-Z+_\.-]+)$/;
    return regStr.test(str);
}

function validateEmailDomain(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var regStr = /^([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;
    return regStr.test(str);
}

function validateIpAddress(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var expUrl = /^(1|2)?\d?\d([.](1|2)?\d?\d){3}$/;
    return expUrl.test(str);
}

function validateLength(str, minLength, maxLength) {
    if (!str) {
        return false;
    }

    return validateMinLength(str, minLength) && validateMaxLength(str, maxLength);
}

function validateMinLength(str, minLength) {
    if (!str) {
        return false;
    }

    return str.length >=  minLength;
}

function validateMaxLength(str, maxLength) {
    if (!str) {
        return false;
    }

    return str.length <=  maxLength;
}

function validateOnlyNumber(num) {
    num += "" + num;
    if (!num) {
        return false;
    }

    var regStr = /^[0-9]*$/;
    return regStr.test(num);
}

function validateUrl(url) {
    if (!url || url.length <= 0) {
        return false;
    }

    var regStr = /^((http(s?))\:\/\/)([0-9a-zA-Z\-]+\.)+[a-zA-Z]{2,6}(\:[0-9]+)?(\/\S*)?$/;
    return regStr.test(url);
}

function checkUndefined(str){
    if (typeof str == "undefined") {
        return "";
    } else {
        return str;
    }

}

function checkNull(str) {
    if( str == "" || str == null || typeof str == "undefined" || !str || 0 === str.length){
        return "";
    } else {
        return str;
    }
}
var stus;
function contsInfoPopup(contsSeq,sttus){
    stus = typeof sttus !== 'undefined' ? sttus : "none";
    contsInfo(contsSeq);
}

contsInfo = function(contsSeq){
    callByGet("/api/contents?contsSeq="+contsSeq, "contsInfoSuccess");
}

contentsInfoMetaData = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.contentsInfo;
        metaData = item.metaData.replace(/&#034;/gi, "\"");
    }
}

contsInfoSuccess = function(data){
    contentsInfoMetaData(data);
    if(data.resultCode == "1000"){
        var item = data.result.contentsInfo;
        var coverImg = data.result.contentsInfo.coverImg;
        var metaBtnHtml = "<input type=\"button\" class=\"btnDefaultWhite btnMetadata\" style=\"width:180px;\" value=\"" + getMessage("submeta.confirm") + "\" onclick=\"contsMetaInfo()\">";

        $("#contsTitle, #headTitle").html(item.contsTitle);
        $("#contsSubTitle").html(item.contsSubTitle);
        $("#contsDesc").html(item.contsDesc);
        $("#contsId").html(item.contsID);
        $("#contCtgNm").html(item.contCtgNm);
        $("#cretrID").html(maskId(item.cretrID));
        $("#mbrNm").html(item.mbrNm);
        $("#contsVer").html(item.contsVer);

        if(metaData != ""){
            $("#contCtgNm").append("&nbsp;&nbsp;" + metaBtnHtml);
        }

        var rcessWhySbst = "";
        if (item.sttus == getMessage("contents.table.refuse")) {
            rcessWhySbst = 'popAlertLayer("'+item.rcessWhySbst+"\",'','"+getMessage("contents.table.refuse.space.reason").toString()+"',true)";
        } else {
            rcessWhySbst = "";
        }

        if (item.sttus == getMessage("contents.table.refuse")) {
            $("#sttus").html("<a href=\"javascript:;\"  onclick="+rcessWhySbst+"  >"+item.sttus+"</a>");
            $("#sttus a").css("color","red");
            $("#sttusBtn").attr("disabled",true);
        } else {
            if (stus == "none") {
                $("#sttus").text(item.sttus.replace(getMessage("contents.table.state.exhibition.success"), getMessage("contents.table.state.verify.success")));
            } else {
                $("#sttus").text(item.sttus);
            }
        }
        $("#cp").html(item.cp);
        $("#cntrctDt").html(item.cntrctStDt+" ~ "+item.cntrctFnsDt);
        $("#cretDt").html(item.cretDt);
        $("#amdDt").html(item.amdDt);
        $("#amdrID").html(maskId(item.amdrID));
        $("#maxAvlNop").html(item.maxAvlNop);

        var genreHtml = "";
        $(data.result.contentsInfo.genreList).each(function(i,item){
            genreHtml += item.genreNm+"/";
        });
        $("#genreList").html(genreHtml.slice(0,-1));

        var serviceHtml = "";
        $(data.result.contentsInfo.serviceList).each(function(i,item){
            serviceHtml += item.svcNm+"/";
        });
        $("#serviceList").html(serviceHtml.slice(0,-1));

        $("#fileType").html(item.fileType);
        if (item.fileType == "VIDEO") {
            if(item.firstCtgNm.match(getMessage("common.liveon")) != null){ // 첫번째 카테고리 명이 '영상_LiveOn'일때
                $("#contsDesc").parents("li").after("<li><label>"+getMessage("common.run.streaming.url")+"</label><div class='detilBox align_l' id='streamingUrl'>"+item.exeFilePath+"</div></li>");
            }
            $(".exeFilePath").hide();
            $(".contentInfo").show();
        }
        $("#exeFilePath").html(item.exeFilePath);
//        $("#videoName").html(item.videoName); 없음
//        if(item.contentsXMLName){
//            $("#contentsXmlName").html(""+item.contentsXMLName+"<a href='/api/fileDownload/"+item.contentsXMLSeq+"' class='fontblack'>다운로드</a>");
//        }
        $("#coverImg").html("<a href=\""+makeAPIUrl(coverImg.coverImgPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(coverImg.coverImgPath,"img")+"' title=\""+coverImg.fileSeq+"\"></a>");
        $("#coverImg").justifiedGallery({
            rowHeight : 220,
            lastRow : 'nojustify',
            margins : 3
        });
        var videoHtml = "";
        var metadataHtml = "";
        var contsHtml = "";
        var meataDataFileNm = "";
        $(data.result.contentsInfo.videoList).each(function(i,item){
            videoHtml += "<p><span class='float_l'>"+(i+1)+". "+item.fileNm+"("+item.fileSize+"Byte)</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            metadataHtml += "<p><span class='float_l'>"+item.metadataInfo.fileNm+"</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.metadataInfo.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            if(item.metadataInfo.fileNm == ""){
                metadataHtml = "";
            }
//            contsHtml += "<p><span class='float_l'>"+item.contsXmlInfo.fileNm+"</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.contsXmlInfo.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>";
            contsHtml = "";
            meataDataFileNm = item.metadataInfo.fileNm;
        });

        $(data.result.contentsInfo.prevList).each(function(i,item){
            $("#prevList").html("<p><span class='float_l'>"+(i+1)+". "+item.fileNm+"("+item.fileSize+"Byte)</span><a href='" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "' class='fontblack btnDownloadGray'>다운로드</a></p>");
        });

//        $("#pakageDown").attr("onclick","pageMove('/api/package/"+item.seq+"')");
        var thumbnailHtml = "";
        // 썸네일 사이즈 지정하여 호출, 지정된 썸네일 사이즈의 파일이 없을 경우 원본 이미지를 로드함.
        var thumbImgSizeStr = "&width=368&height=276";
        $(data.result.contentsInfo.thumbnailList).each(function(i,item){
            thumbnailHtml += "<a href=\""+makeAPIUrl(item.thumbnailPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(item.thumbnailPath + thumbImgSizeStr,"img")+"' title=\""+item.fileSeq+"\"></a>";
        });
        $("#thumbnailList").html(thumbnailHtml);
        $("#thumbnailList").justifiedGallery({
            rowWidth : 378,
            rowHeight : 276,
            lastRow : 'nojustify',
            margins : 3
        });
        $(".swipebox").swipebox();
        $("#widthSetting").css("width", "10%");

        var fileHtml = "";

        if (videoHtml == "") {
            $("#videoList").parents("li").hide();
            $("#metadataName").parents("li").hide();
            $(data.result.contentsInfo.fileList).each(function(i,item){
                fileHtml = "<p><span class='float_l'>"+item.fileNm+"&nbsp;</span><a href=\"" + makeAPIUrl("/api/fileDownload/"+item.fileSeq) + "\" class=\"fontblack btnDownloadGray\">[다운로드]</a></p>";
                $("#file").html(fileHtml);
            });
        } else {
            $("#videoList").html(videoHtml);
            $("#metadataName").html(metadataHtml);
            if(metadataHtml == "") {
                $("#metadataName").parents("tr").hide();
            }
            $("#contsInfo").html(contsHtml);
            $(".contsInfo").hide();
            $("#file").parents("li").hide();
        }
        openPopups();
    }
}

contsMetaInfo = function(){
    callByGet("/api/codeInfo?comnCdCtg=CONTS_SUB_META", "contsMetaInfoSuccess", "NoneForm");
}
contsMetaInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var tableHtml = "";
        var metaVal = "";
        $("#detailMeta").html("");
        $(data.result.codeList).each(function(i, item) {
            var metaObj = JSON.parse(metaData);
            for (key in metaObj) {
                if (key == item.comnCdValue) {
                    if (item.comnCdNm.indexOf(getMessage("submeta.time")) != -1 && metaObj[key] != "") {
                        metaVal = metaObj[key] + " " + getMessage("common.minute");
                    } else if(item.comnCdNm.indexOf(getMessage("submeta.rt")) != -1){
                        // #a8484는 작은 따옴표(') #q0808은 큰 따옴표(")로 DB 저장
                        if(metaObj[key].match("#a8484") != null || metaObj[key].match("#q0808") != null){
                            metaObj[key] = metaObj[key].replace("#a8484", "\'");
                            metaObj[key] = metaObj[key].replace("#q0808", "\"");
                        }
                        metaVal = metaObj[key];
                    } else {
                        metaVal = metaObj[key];
                    }
                }
            }
            if (metaVal != "") { //메타데이터가 없으면 조회 리스트에 추가하지 않음
                tableHtml += "<tr><th>" + item.comnCdNm + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">" + "</th><td>" + metaVal + "</td>";
                metaVal = "";
            }
            tableHtml += "</tr>";
        });
        $("#detailMeta").append(tableHtml);
        openMetaPopups();
    } else {
        $("#detailMeta").html("");
    }
}

function openPopups(){
    $.blockUI({
        message: $('#popupWinBig'),
//        onOverlayClick: $.unblockUI,
        css:{border:'0px',cursor:'default',top:'15%',left:'35%',width:'auto',height:'700px'}
    });

    if (fistPopup){
        $("#popupWinBig").data({
            'originalLeft': $("#popupWinBig").css('left'),
            'origionalTop': $("#popupWinBig").css('top')
        });
        fistPopup = false;
        $("#popupWinBig").draggable();
    } else {
        $("#popupWinBig").css({
            'left': $("#popupWinBig").data('originalLeft'),
            'top': $("#popupWinBig").data('origionalTop')
        });
    }
}

function openPopups2(){
    $.blockUI({
        message: $('#popupWinBig'),
        css:{border:'0px',cursor:'default',width:'auto',height:'620px',top:"calc(50% - 310px)",left:"calc(50% - 510px)"}
    });

    if (fistPopup){
        $("#popupWinBig").data({
            'originalLeft': $("#popupWinBig").css('left'),
            'origionalTop': $("#popupWinBig").css('top')
        });
        fistPopup = false;
        $("#popupWinBig").draggable();
    } else {
        $("#popupWinBig").css({
            'left': $("#popupWinBig").data('originalLeft'),
            'top': $("#popupWinBig").data('origionalTop')
        });
    }
}

function openMetaPopups(){
    $("#popupWinBig").block({
        message: $('#popupMetaWin'),
        css:{border:'0px',cursor:'default'}
    });
    $("#popupWinBig").css({
        border:'0px', 'box-shadow': 'none'
    })
    fistPopup = true;
    if (fistPopup){
        fistPopup = false;
        $("#popupMetaWin").draggable();
    }
}

function openPopups3(tableIndex){
    var imgPaths = $("#jqgridData").jqGrid('getRowData',tableIndex)["banrImg.imgPath"];
    var im = new Image();
    im.src = makeAPIUrl(imgPaths,"img");
    im.onload = function(){
        $("#popupImg").attr("src",makeAPIUrl($("#jqgridData").jqGrid('getRowData',tableIndex)["banrImg.imgPath"],"img"));
        $.blockUI({
            message: $('#popupPreView'),
            css:{border:'0px',cursor:'default',width:'auto',height:'620px',top:"calc(50% - 310px)",left:"calc(50% - 510px)"}
        });
    };

    im.onerror = function () {
        popAlertLayer(getMessage("common.imgWarn.notExist"));

    };

    if (fistPopup){
        $("#popupPreView").data({
            'originalLeft': $("#popupPreView").css('left'),
            'origionalTop': $("#popupPreView").css('top')
        });
        fistPopup = false;
        $("#popupPreView").draggable();
    } else {
        $("#popupPreView").css({
            'left': $("#popupPreView").data('originalLeft'),
            'top': $("#popupPreView").data('origionalTop')
        });
    }
}

function openPopups4(type,tableIndex){
    var imgPaths;
    if (type == 1) {
        imgPaths = $("#jqgridData").jqGrid('getRowData',tableIndex)["cprtImg"];
    } else {
        imgPaths = $("#jqgridData").jqGrid('getRowData',tableIndex)["cprtThmbImg"];
    }

    var im = new Image();
    im.src = makeAPIUrl(imgPaths,"img");
    im.onload = function(){
        $("#popupImg").attr("src",makeAPIUrl(imgPaths,"img"));
        $.blockUI({
            message: $('#popupPreView'),
            css:{border:'0px',cursor:'default',width:'auto',height:'620px',top:"calc(50% - 310px)",left:"calc(50% - 510px)"}
        });
    };

    im.onerror = function () {
        popAlertLayer(getMessage("common.imgWarn.notExist"));

    };

    if (fistPopup){
        $("#popupPreView").data({
            'originalLeft': $("#popupPreView").css('left'),
            'origionalTop': $("#popupPreView").css('top')
        });
        fistPopup = false;
        $("#popupPreView").draggable();
    } else {
        $("#popupPreView").css({
            'left': $("#popupPreView").data('originalLeft'),
            'top': $("#popupPreView").data('origionalTop')
        });
    }
}

function openPopups5(imgPaths){
    var im = new Image();
    im.src = makeAPIUrl(imgPaths,"img");
    im.onload = function(){
        $("#popupImg").attr("src",makeAPIUrl(imgPaths,"img"));
        $.blockUI({
            message: $('#popupPreView'),
            css:{border:'0px',cursor:'default',width:'auto',height:'620px',top:"calc(50% - 310px)",left:"calc(50% - 510px)"}
        });
    };

    im.onerror = function () {
        popAlertLayer(getMessage("common.imgWarn.notExist"));

    };

    if (fistPopup){
        $("#popupPreView").data({
            'originalLeft': $("#popupPreView").css('left'),
            'origionalTop': $("#popupPreView").css('top')
        });
        fistPopup = false;
        $("#popupPreView").draggable();
    } else {
        $("#popupPreView").css({
            'left': $("#popupPreView").data('originalLeft'),
            'top': $("#popupPreView").data('origionalTop')
        });
    }
}

function openPopups6(tableIndex){
    var imgPaths = $("#jqgridData").jqGrid('getRowData',tableIndex)["appImgPath"];
    var im = new Image();
    im.src = makeAPIUrl(imgPaths,"img");
    im.onload = function(){
        $("#popupImg").attr("src",makeAPIUrl($("#jqgridData").jqGrid('getRowData',tableIndex)["appImgPath"],"img"));
        $.blockUI({
            message: $('#popupPreView'),
            css:{border:'0px',cursor:'default',width:'auto',height:'620px',top:"calc(50% - 310px)",left:"calc(50% - 510px)"}
        });
    };

    im.onerror = function () {
        popAlertLayer(getMessage("common.imgWarn.notExist"));

    };

    if (fistPopup){
        $("#popupPreView").data({
            'originalLeft': $("#popupPreView").css('left'),
            'origionalTop': $("#popupPreView").css('top')
        });
        fistPopup = false;
        $("#popupPreView").draggable();
    } else {
        $("#popupPreView").css({
            'left': $("#popupPreView").data('originalLeft'),
            'top': $("#popupPreView").data('origionalTop')
        });
    }
}

closePopups = function(){
    $.unblockUI();
}

closeMetaPopups = function(){
    $("#popupWinBig").unblock();
    $("#popupWinBig").css({
        border:'solid 1px #999','box-shadow':'5px 5px 2px #ddd'
    })
}

function addEmailDomainList(obj) {
    if (obj == "undefined" || (obj.tagName != "SELECT")) {
        return;
    }

    var msgFuntion = window["getLoginMessage"] || window["getMessage"];
    var listHtml = "<option value=''>" + msgFuntion('login.select.direct') + "</option>";
    listHtml += "<option value='naver.com'>naver.com</option>";
    listHtml += "<option value='gmail.com'>gmail.com</option>";
    listHtml += "<option value='nate.com'>nate.com</option>";
    listHtml += "<option value='yahoo.co.kr'>yahoo.co.kr</option>";
    listHtml += "<option value='hanmail.net'>hanmail.net</option>";
    listHtml += "<option value='daum.net'>daum.net</option>";
    listHtml += "<option value='dreamwiz.com'>dreamwiz.com</option>";
    listHtml += "<option value='lycos.co.kr'>lycos.co.kr</option>";
    listHtml += "<option value='empal.com'>empal.com</option>";
    listHtml += "<option value='korea.com'>korea.com</option>";
    listHtml += "<option value='paran.com'>paran.com</option>";
    listHtml += "<option value='freechal.com'>freechal.com</option>";
    listHtml += "<option value='hitel.net'>hitel.net</option>";
    listHtml += "<option value='hanmir.com'>hanmir.com</option>";
    listHtml += "<option value='hotmail.com'>hotmail.com</option>";

    $(obj).html(listHtml);
}

downloadExcelFile = function(uri, params) {
    var gridId = params.gridId || "jqgridData";
    if ($("#" + gridId).length && parseInt($("#" + gridId).getGridParam("records")) <= 0) {
        popAlertLayer(getMessage("info.nodata.excel.msg"));
        return;
    }

    var form = document.forms[0];
    var prevTarget = form.target;
    var prevAction = form.action;

    $("#excelForm").remove();
    $("body").append("<iframe id='excelForm' src='' width='0' height='0' frameborder='0' scrolling='no'></iframe>");

    form.target = "excelForm";
    form.action = makeAPIUrl(uri);

    Object.keys(params).forEach(function(key) {
        formData("NoneForm" , key, params[key]);
    })

    form.submit();

    formDataDeleteAll("NoneForm");

    form.target = prevTarget;
    form.action = prevAction;
}

isAdministrator = function(codeValue) {
    return (codeValue == "01" || codeValue == "02");
}

/* XSS 값 replace*/
function xssChk(str){
    var text = str;
    var decoded = $('<div/>').html(str).text();

    return decoded;
}

function getTermInfo() {
    var termStr = getMessage("info.term.ready");
    /*
     * 2018.08.31 박소영 대리 요청으로 주석 처리 - 내용 변경 예정으로 현재는 준비 중으로만 표시하도록 변경
    var termStr = getMessage("info.term.1st") + getMessage("info.term.2nd") + getMessage("info.term.3th") + getMessage("info.term.4th")
                    + getMessage("info.term.5th") + getMessage("info.term.6th") + getMessage("info.term.7th") + getMessage("info.term.8th")
                    + getMessage("info.term.9th") + getMessage("info.term.10th") + getMessage("info.term.11th") + getMessage("info.term.12th")
                    + getMessage("info.term.13th") + getMessage("info.term.14th") + getMessage("info.term.15th") + getMessage("info.term.16th")
                    + getMessage("info.term.17th") + getMessage("info.term.18th") + getMessage("info.term.19th") + getMessage("info.term.20th")
                    + getMessage("info.term.sub");
    */
    return termStr;
}

function getPolicyInfo() {
    var policyStr = getMessage("info.policy.1st") + getMessage("info.policy.2nd") + getMessage("info.policy.3th") + getMessage("info.policy.4th")
                    + getMessage("info.policy.5th") + getMessage("info.policy.6th") + getMessage("info.policy.7th") + getMessage("info.policy.8th")
                    + getMessage("info.policy.9th") + getMessage("info.policy.10th") + getMessage("info.policy.11th");
    return policyStr;
}

function replaceAll(str, searchStr, replaceStr) {
    return str.split(searchStr).join(replaceStr);
}

// IE 10 버전 이하에서 SweetAlert Layer에 의해 이벤트 차단되는 문제 해결
function setVisibilitySweetAlert(visible, isMulti, multiReset) {

    if (!$(".swal-overlay").length) {
        return;
    }

    if (navigator.appName != 'Microsoft Internet Explorer') {
        return;
    }

    if (!Brower.ie || (!Brower.ie9 && !Brower.ie10)) {
        return;
    }

    if (isMulti && !multiReset) {
        if (!visible) {
            $(".swal-overlay").show();
        } else {
            $(".swal-overlay").hide();
        }
        return;
    }

    if (visible) {
        $(".swal-overlay").show();
    } else {
        $(".swal-overlay").hide();
    }
}

/**
 * Client에서 개인정보 마스킹 처리 코드
 * Server 단 처리로 현재 사용하지 않는 코드이지만 소스 내 호출 코드가 있어,
 * 원형만 유지하고 전달 받은 값을 그대로 return 하도록 함.
 * 또한 Client 단에서 처리 필요 시 사용토록 주석으로 코드 유지함.
 * @param name
 * @returns
 */
// 회원 이름 숨김 처리 - 뒤 1자리 (이름 길이의 1/3 영역 숨김)
function maskName(name) {
    /*
    if (name == undefined || name === '') {
        return '';
    }

    var nameLen = name.length;
    var maskLen = Math.round(nameLen / 3);
    var nonMaskStr = name.substr(0, nameLen - maskLen);
    var maskStr = name.substr(nameLen - maskLen, nameLen);

    for (var i = 0; i < maskStr.length; i++) {
        maskStr = maskStr.replace(maskStr.substring(i, i+ 1), "*");
    }

    return nonMaskStr + maskStr;
    */
    return name;
}

// 회원 아이디 숨김 처리 - 뒤 3자리
function maskId(id) {
    /*
    if (id == undefined || id === '') {
        return '';
    }

    var pattern = /.{3}$/; // 뒤에 3자리 마스킹
    return id.replace(pattern, "***");
    */
    return id;
}

// 회원 email 숨김 처리 - email id 뒤 3자리
function maskEmail(email) {
    /*
    if (email == undefined || email === '') {
        return '';
    }

    var emailArray = email.split("@");
    var pattern;

    if (emailArray[0].length < 4) {
        pattern = /.$/;
        emailArray[0] = emailArray[0].replace(pattern, "*");
    } else {
        pattern = /.{3}$/;
        emailArray[0] = emailArray[0].replace(pattern, "***");
    }

    var retEmail = "";
    for (var i = 0; i < emailArray.length; i++) {
        retEmail += emailArray[i];
        if (i == 0) {
            retEmail += "@";
        }
    }

    return retEmail;
    */
    return email;
}

// 회원 전화번호, 휴대전화번호 숨김 처리 - 국번 뒤 2자리, 번호 앞 1자리
function maskTelNum(telno) {
    /*
    if (telno == undefined || telno === '') {
        return '';
    }

    var telnoArray = telno.split("-");
    var pattern;

    var patten;
    pattern = /.{2}$/;
    telnoArray[1] = telnoArray[1].replace(pattern, "**");

    pattern = /.{1}/;
    telnoArray[2] = telnoArray[2].replace(pattern, "*");

    return telnoArray[0] + "-" + telnoArray[1] + "-" + telnoArray[2];
    */
    return telno;
}

// IP Address 숨김 처리 - ex) ***.123.***.123
function maskIpAddr(ipAddr) {
    /*
    if (ipAddr == undefined || ipAddr === '') {
        return '';
    }

    var ipAddrArray = ipAddr.split(".");
    ipAddrArray[0] = ipAddrArray[0].replace(ipAddrArray[0], "***");
    ipAddrArray[2] = ipAddrArray[2].replace(ipAddrArray[2], "***");

    return ipAddrArray[0] + "." + ipAddrArray[1] + "." + ipAddrArray[2] + "." + ipAddrArray[3];
    */
    return ipAddr;
}

// 이름(아이디) 숨김 처리
function maskNamePlusId(str) {
    /*
    if (str == undefined || str === '') {
        return '';
    }

    if ( str.indexOf("(") == -1) {
        return str;
    }

    var strArray = str.split("(");
    strArray[1] = strArray[1].slice(0, -1);

    return maskName(strArray[0]) + "(" + maskId(strArray[1]) + ")";
    */
    return str;
}

function maskBizno(bizno) {
    /*
    if (bizno == undefined || bizno === '') {
        return '';
    }

    // 사업자번호 뒤 5자리 숨김
    var biznoArray = bizno.split("-");
    biznoArray[2] = biznoArray[2].replace(biznoArray[2], "*****");

    return biznoArray[0] + "-" + biznoArray[1] + "-" + biznoArray[2];
    */
    return bizno;
}

function maskAddress(addr) {
    /*
    if (addr == undefined || addr === '') {
        return '';
    }

    var addrArray = addr.split(" ");
    var matchCnt = 0;
    var str = "";
    var regEx = /(([가-힣]+(\d{1,5}|\d{1,5}(,|.)\d{1,5}|)+(읍|면|동|가|리))(^구|)((\d{1,5}(~|-)\d{1,5}|\d{1,5})(가|리|)|))([ ](산(\d{1,5}(~|-)\d{1,5}|\d{1,5}))|)|(([가-힣]|(\d{1,5}(~|-)\d{1,5})|\d{1,5})+(로|길))/;
    var resultStr = "";
    for (var i = 0; i < addrArray.length; i++) {
        str += addrArray[i] + " ";

        if (regEx.test(str)) {
            if (matchCnt == 0
                || str.slice(0, -1).substr(str.length - 2) == getMessage("common.address.ro")
                || str.slice(0, -1).substr(str.length - 2) == getMessage("common.address.gil")
                || str.slice(0, -1).substr(str.length - 2) == getMessage("common.address.dong")
                || str.slice(0, -1).substr(str.length - 2) == getMessage("common.address.li")) {
                resultStr = str;
            }

            matchCnt++;
        }
    }

    if (resultStr != "" && matchCnt > 0) {
        resultStr += "***";
    } else {
        for (var i = 0; i < addrArray.length -1; i++) {
            resultStr += addrArray[i] + " ";
        }
        resultStr += "***";
    }

    return resultStr;
    */
    return addr;
}

sendSms = function(options) {
    var type = options.type || "";
    var section = options.section || "";
    var seq = options.seq || "";
    var pwd = options.pwd || "";

    formData("NoneForm", "type", type);
    formData("NoneForm", "section", section);
    formData("NoneForm", "seq", seq);
    formData("NoneForm", "pwd", pwd);

    callByPost("/api/sms", "", "NoneForm", "");
    formDataDeleteAll("NoneForm");
}

function imgToThumbImg() {
    // 현재 페이지의 모든 이미지에 대해 img 표시 사이즈로 썸네일 이미지를 불러와 교체
    //$('img[src*="/api/imgUrl"]:not([class^="' + str + '"])').each(function(){
    $('img[src*="/api/imgUrl"]').each(function(){
        $(this).attr('src',$(this).attr("src").replace("/imgUrl?", "/imgUrl?width=" + $(this).css("width").replace("px","") + "&height=" + $(this).css("height").replace("px","") + "&"));
    });
}

var fileSet = new Array();
var cretImg = true;
/* 파일 폼 세팅*/
fileFormInsert = function(fileId, fileSe, lmtWidth, lmtHeight, position,disPlayPosition, accessType){
    fileSet.push({
        fileId  : fileId,
        fileSe  : fileSe,
        lmtWidth : lmtWidth,
        lmtHeight : lmtHeight,
        position : position,
        disPlayPosition : disPlayPosition,
        accessType : accessType,
        contsSeq : 0,
        filePath : "",
        chgCnt : 0,
        lmtFileSize : 0
    });
    if(cretImg) {
        $('body').append('<img src="" id="temp_img" style="display:none;" />');
        cretImg = false;
    }
}
/* 파일 Input Box 화면상 표기 (전체) */
fileFormAllView = function(){
    for(var i =0; i < fileSet.length; i++){
        var setVar = fileSet[i];
        if (Brower.ie) {
            $("#"+setVar.position).append("<input name='file' class='inputFile' type='file' data-value="+i+" onchange=\"fileUploadPreview(this,\'"+setVar.disPlayPosition+"\')\" />");
        } else {
            document.getElementById(setVar.position).innerHTML += "<input name='file' class='inputFile' type='file' data-value="+i+" onchange=\"fileUploadPreview(this,\'"+setVar.disPlayPosition+"\')\" />";
        }
    }

}

/* 파일 Input Box 화면상 표기 (일부) */
fileFormView = function(i){
    var setVar = fileSet[i];
        if (Brower.ie) {
            $("#"+setVar.position).append("<input name='file' class='inputFile' type='file' data-value="+i+" onchange='fileUploadPreview(this,"+setVar.disPlayPosition+")' />");
        } else {
            document.getElementById(setVar.position).innerHTML += "<input name='file' class='inputFile' type='file' data-value="+i+" onchange='fileUploadPreview(this,"+setVar.disPlayPosition+")' />";
        }
}

function fileUploadPreview(thisObj, preViewer) {
    var chg = false;
    if (thisObj.value.length == 0) {
//      "Suspect Cancel was hit, no files selected."
        chg = false;

    } else {
//     "File selected: ", thisObj.value
        chg = true;
    }

    if (document.getElementById("waring"+thisObj.getAttribute('data-value')) != null ) {
        document.getElementById("waring"+thisObj.getAttribute('data-value')).remove();
    }

    var objFileInfo = fileSet[thisObj.getAttribute('data-value')];
    var fileName = thisObj.value.slice(thisObj.value.indexOf(".") + 1).toLowerCase();
    preViewer = (typeof(preViewer) == "object") ? preViewer : document.getElementById(preViewer);
    if(fileName != "jpg" && fileName != "png" &&  fileName != "gif" &&  fileName != "bmp" && fileName != "jpeg"){
        fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.type2"),chg);
//        popAlertLayer(getMessage("common.imgWarn.notAllow.type2"));

        return;
    }

    var str = objFileInfo.accessType;
    if(str.indexOf(fileName) == -1){
        fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.type"),chg);
//        popAlertLayer(getMessage("common.imgWarn.notAllow.type"));
        return;
    }

    // IE 7 , IE 8 이면
    /* 실제 서버에서 구동 시 오류 발생하여 주석 처리함. 아래 크롬, 사파리 조건문 내에 코드를 ie7, ie8로 변경
    if (Brower.ie7 || Brower.ie8) {
        preViewer.innerHTML = "";
        var elem = document.createElement("img");
        var im = new Image();
        im.src = "file://" + String(thisObj.value);
        elem.style.width = im.width;
        elem.style.height = im.height;
        if (objFileInfo.lmtWidth != -1) {
            if (im.width > objFileInfo.lmtWidth) {
                fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.scale"),chg);
//                popAlertLayer(getMessage("common.imgWarn.notAllow.scale"));
                return;
            }
        }
        if (objFileInfo.lmtHeight != -1) {
            if (im.height > objFileInfo.lmtHeight) {
                fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.scale"),chg);
//                popAlertLayer(getMessage("common.imgWarn.notAllow.scale"));
                return;
            }
        }

        elem.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='file://" + String(thisObj.value) +"', sizingMethod='scale')";
        preViewer.appendChild(elem);
        objFileInfo.filePath = im.src;

        objFileInfo.chgCnt++;
        return;
    }
    */

    if (Brower.chrome || Brower.safari || Brower.safari3) {
        var files = thisObj.files;
        var filesArr = Array.prototype.slice.call(files);

            preViewer.innerHTML = "";
            var W = preViewer.offsetWidth;
            var H = preViewer.offsetHeight;

        filesArr.forEach(function(f) {
            if(!f.type.match("image.*")) {
                fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.type3"),chg);
//                popAlertLayer(getMessage("common.imgWarn.notAllow.type3"));
                return;
            }

            sel_file = f;

            var reader = new FileReader();
            reader.onload = function(e) {
                $('#temp_img').attr('src', e.target.result);
                if (objFileInfo.lmtWidth != -1) {
                    if ($('#temp_img').width() > objFileInfo.lmtWidth) {
                        fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.scale"),chg);
//                        popAlertLayer(getMessage("common.imgWarn.notAllow.scale"));
                        return;
                    }
                }
                if (objFileInfo.lmtHeight != -1) {
                    if ($('#temp_img').height() > objFileInfo.lmtHeight) {
                        fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.scale"),chg);
//                        popAlertLayer(getMessage("common.imgWarn.notAllow.scale"));
                        return;
                    }
                }

                $(preViewer).append("<img />");
                $(preViewer).find("img").last().attr({"src": e.target.result});
                objFileInfo.filePath = "file://" + e.target.result;
            }
            reader.readAsDataURL(f);
            objFileInfo.chgCnt++;
        });

    } else if (Brower.ie7 || Brower.ie8) {
        var img_path = "";
        if (thisObj.value.indexOf("\\fakepath\\") < 0) {
            img_path = thisObj.value;
        } else {
            thisObj.select();
            //var selectionRange = document.selection.createRange();
            //img_path = selectionRange.text.toString();
            // ie 7, 8에서는 위 코드로는 에러가 발생함. 보안 상 이유로 막힌 것으로 보임.
            // 아래 함수를 사용하면 오류는 발생하지 않지만 미리보기는 안됨. 단순히 오류가 발생하지 않도록만 처리함.
            img_path = document.body.createTextRange();

            thisObj.blur();
        }

        $(preViewer).append("<img />");
        $(preViewer).find("img").last().attr({"src": img_path});
        objFileInfo.filePath = "file://" + img_path;
        preViewer.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='fi" + "le://" + img_path + "', sizingMethod='scale')";
        objFileInfo.chgCnt++;
    } else {
        preViewer.innerHTML = "";
        var W = preViewer.offsetWidth;
        var H = preViewer.offsetHeight;
        var elem = document.createElement("img");
        elem.onerror = function () {
            return preViewer.innerHTML = "";
        }

        elem.onload = function (e) {
        }
        var files = thisObj.files;

        // IE9 ~ IE11은 fileReader를 사용할 수 있으므로 모두 아래 구문을 수행하도록 함.
        /*if (Brower.ie)*/ {
            var files = thisObj.files;
            var filesArr = Array.prototype.slice.call(files);

            filesArr.forEach(function(f) {
                if(!f.type.match("image.*")) {
                    fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.type3"),chg);
//                    popAlertLayer(getMessage("common.imgWarn.notAllow.type3"));
                    return;
                }

                sel_file = f;

                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#temp_img').attr('src', e.target.result);
                    if (objFileInfo.lmtWidth != -1) {
                        if ($('#temp_img').width() > objFileInfo.lmtWidth) {
                            fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.scale"),chg);
//                            popAlertLayer(getMessage("common.imgWarn.notAllow.scale"));
                            $('#temp_img').remove(); //위에서 생성한 임시 img 태그 삭제
                            return;
                        }
                    }
                    if (objFileInfo.lmtHeight != -1) {
                        if ($('#temp_img').height() > objFileInfo.lmtHeight) {
                            fileReSetPath(thisObj.getAttribute('data-value'),preViewer,thisObj,getMessage("common.imgWarn.notAllow.scale"),chg);
//                            popAlertLayer(getMessage("common.imgWarn.notAllow.scale"));
                            $('#temp_img').remove(); //위에서 생성한 임시 img 태그 삭제
                            return;
                        }
                    }
                    $(preViewer).append("<img />");
                    $(preViewer).find("img").last().attr("src", e.target.result);
                    objFileInfo.filePath = "file://" + e.target.result;
                }
                reader.readAsDataURL(f);
                objFileInfo.chgCnt++;
            });
        }/* else {
            preViewer.appendChild(elem);
            elem.src = "file://" + thisObj.value;
            objFileInfo.filePath = "file://" + thisObj.value;
            objFileInfo.chgCnt++;
        }*/
    }
}
function fileUploadAllCheck() {
    for(var i =0; i < fileSet.length; i++){
        var im = new Image();
        if (fileSet[i].filePath.indexOf("file://") == -1) {
            im.src = "file://" + String(fileSet[i].filePath);
        } else {
            im.src = fileSet[i].filePath;
        }



        im.onerror = function () {
            alert("로딩실패!!!");
        };
    }
}

function empty(val) {

    // test results
    //---------------
    // []        true, empty array
    // {}        true, empty object
    // null      true
    // undefined true
    // ""        true, empty string
    // ''        true, empty string
    // 0         false, number
    // true      false, boolean
    // false     false, boolean
    // Date      false
    // function  false

    if (val === undefined)
        return true;

    if (typeof (val) == 'function' || typeof (val) == 'number' || typeof (val) == 'boolean' || Object.prototype.toString.call(val) === '[object Date]')
        return false;

    if (val == null || val.length === 0)        // null or 0 length array
        return true;

    if (typeof (val) == "object") {
        // empty object

        var r = true;

        for (var f in val)
            r = false;

        return r;
    }

    return false;
}

fileReSetPath = function(idx,preViewer,thisObj,msg,chged) {
    if (msg == getMessage("common.imgWarn.notAllow.type2") && chged){
        var elem = document.createElement("span");
        elem.id = "waring" + idx;
        elem.innerHTML = " *" +msg;
        elem.classList.add("alertCaption");
        document.getElementById(fileSet[idx].fileId).appendChild(elem);
    }  else if (chged) {
        var elem = document.createElement("span");
        elem.id = "waring" + idx;
        elem.innerHTML = " *" +msg;
        elem.classList.add("alertCaption");
        document.getElementById(fileSet[idx].fileId).appendChild(elem);
    }

    fileSet[idx].filePath ="";
    preViewer.innerHTML = "";
    fileSet[idx].chgCnt++;
    $(thisObj).val("");
}


