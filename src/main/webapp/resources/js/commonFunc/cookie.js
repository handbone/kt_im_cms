/*
 * IM Platform version 1.0
 *
 *  Copyright �� 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

function setCookie(cKey, cValue) {
    var date = new Date();
    var validity = 30;
    date.setDate(date.getDate() + validity);
    document.cookie = cKey + '=' + escape(cValue) + ';expires=' + date.toGMTString() + ';path=/';
}

function setTempCookie(cKey, cValue) {
    document.cookie = cKey + '=' + escape(cValue) + ';path=/';
}

function getCookie(cKey) {
    var allcookies = document.cookie;
    var cookies = allcookies.split("; ");
    for (var i = 0; i < cookies.length; i++) {
        var keyValues = cookies[i].split("=");
        if (keyValues[0] == cKey) {
            return unescape(keyValues[1]);
        }
    }
    return "";
}

function removeCookie(cKey) {
    var date = new Date(); 
    var validity = -1;
    date.setDate(date.getDate() + validity);
    document.cookie = cKey + "=;expires=" + date.toGMTString() + ';path=/';
}

function removeTempCookie(cKey) {
    var date = new Date(); 
    var validity = -1;
    date.setDate(date.getDate() + validity);
    document.cookie = cKey + '=;path=/';
}
