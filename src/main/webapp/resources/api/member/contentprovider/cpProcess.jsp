<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>
    <c:if test="${resultCode eq '1010' and cpNm != ''}">
        <json:object name="result">
            <json:property name="cpNm" value="${ cpNm }" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'cpList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="cpList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="cpSeq" value="${ i.cpSeq }"/>
                        <json:property name="cpNm" value="${ i.cpNm }" escapeXml="false"/>
                        <json:property name="cpTelNo" value="${ i.cpTelNo }" />
                        <json:property name="cpBasAddr" value="${ i.cpBasAddr }" />
                        <json:property name="useYn" value="${ i.useYn }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'cpBuyinList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="cpList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="cpSeq" value="${ i.cpSeq }"/>
                        <json:property name="cpNm" value="${ i.cpNm }" escapeXml="false"/>
                        <json:property name="cpTelNo" value="${ i.cpTelNo }" />
                        <json:property name="cpAddr" value="${ i.cpAddr }" />
                        <json:property name="useYn" value="${ i.useYn }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="count" value="${ i.count }" />
                        <json:property name="totPrice" value="${ i.totPrice }" />
                        <json:property name="buyinAmdDt" value="${ i.buyinAmdDt }" />
                        <json:property name="contsCount" value="${ i.contsCount }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'cpDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="cpInfo">
                    <json:property name="cpSeq" value="${ item.cpSeq }"/>
                    <json:property name="cpNm" value="${ item.cpNm }" escapeXml="false"/>
                    <json:property name="cpTelNo" value="${ item.cpTelNo }"/>
                    <json:property name="zipcd" value="${ item.zipcd }"/>
                    <json:property name="cpBasAddr" value="${ item.cpBasAddr }"/>
                    <json:property name="cpDtlAddr" value="${ item.cpDtlAddr }"/>
                    <json:property name="bizno" value="${ item.bizno }"/>
                    <json:property name="contDt" value="${ item.contDt }"/>
                    <json:property name="contStDt" value="${ item.contStDt }"/>
                    <json:property name="contFnsDt" value="${ item.contFnsDt }"/>
                    <json:property name="cretrId" value="${item.cretrId}"/>
                    <json:property name="cretDt" value="${item.cretDt}"/>
                    <json:property name="amdrId" value="${item.amdrId}"/>
                    <json:property name="amdDt" value="${item.amdDt}"/>
                    <json:property name="useYn" value="${ item.useYn }"/>
                    <c:if test="${ buyinContNo ne '' }">
                        <json:property name="buyinContCnt" value="${ item.buyinContCnt }"/>
                    </c:if>
                    <json:array name="cpServiceList" items="${serviceList}" var="i">
                        <json:object>
                            <json:property name="svcSeq" value="${ i.svcSeq }" />
                            <json:property name="svcNm" value="${ i.svcNm }" />
                        </json:object>
                    </json:array>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contractList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="cpNm" value="${ cpNm }" />
                <json:array name="contractList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="buyinSeq" value="${ i.buyinSeq }"/>
                        <json:property name="buyinContNo" value="${ i.buyinContNo }" />
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsCount" value="${ i.contsCount }" />
                        <json:property name="ratYn" value="${ i.ratYn }" />
                        <json:property name="buyinContType" value="${ i.buyinContType }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="totPrice" value="${ i.totPrice }" />
                        <json:property name="usePrice" value="${ i.usePrice }" />
                        <json:property name="useCount" value="${ i.useCount }" />
                        <json:property name="setVal" value="${ i.setVal }" />
                        <json:property name="amdDt" value="${ i.amdDt }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }(${ i.cretrId })" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contractInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
            <json:property name="cpNm" value="${ cpNm }" />
                <json:object name="contractInfo">
                    <json:property name="cpSeq" value="${ item.cpSeq }"/>
                    <json:property name="buyinSeq" value="${ item.buyinSeq }"/>
                    <json:property name="buyinContNo" value="${ item.buyinContNo }" />
                    <json:property name="contsTitle" value="${ item.contsTitle }" escapeXml="false"/>
                    <json:property name="ratYn" value="${ item.ratYn }" />
                    <json:property name="buyinContType" value="${ item.buyinContType }" />
                    <json:property name="buyinContStDt" value="${ item.buyinContStDt }" />
                    <json:property name="buyinContFnsDt" value="${ item.buyinContFnsDt }" />
                    <json:property name="runLmtCnt" value="${ item.runLmtCnt }" />
                    <json:property name="runPerPrc" value="${ item.runPerPrc }" />
                    <json:property name="ratTime" value="${ item.ratTime }" />
                    <json:property name="ratPrc" value="${ item.ratPrc }" />
                    <json:property name="lmsmpyPrc" value="${ item.lmsmpyPrc }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <json:property name="amdrId" value="${ item.amdrId }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="amdDt" value="${ item.amdDt }" />

                    <json:array name="contsList" items="${cntItem}" var="i">
                        <json:object>
                            <json:property name="contsSeq" value="${ i.contsSeq }"/>
                            <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                            <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                            <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                            <json:property name="amdDt" value="${ i.amdDt }"/>
                            <json:property name="cretDt" value="${ i.cretDt }"/>
                        </json:object>
                    </json:array>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contractHistoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contractHistoryList" items="${item}" var="i">
                    <json:object>
                        <json:property name="buyinContHstSeq" value="${ i.buyinContHstSeq }"/>
                        <json:property name="buyinSeq" value="${ i.buyinSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsCount" value="${ i.contsCount }" />
                        <json:property name="ratYn" value="${ i.ratYn }" />
                        <json:property name="buyinContType" value="${ i.buyinContType }" />
                        <json:property name="totPrice" value="${ i.totPrice }" />
                        <json:property name="setVal" value="${ i.setVal }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="amdrId" value="${ i.amdrId }" />
                        <json:property name="amdDt" value="${ i.amdDt }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>



</json:object>