<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'serviceList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="serviceList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }" escapeXml="false" />
                        <json:property name="telNo" value="${ i.telNo }" />
                        <json:property name="basAddr" value="${ i.basAddr }" />
                        <json:property name="cretDt" value="${ i.cretDt }" />
                        <json:property name="useYn" value="${ i.useYn }" />
                        <json:property name="appType" value="${ i.appType }" />
                        <json:property name="mirrYn" value="${ i.mirrYn }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'serviceInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="serviceInfo">
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }" escapeXml="false" />
                    <json:property name="telNo" value="${ item.telNo }" />
                    <json:property name="zipcd" value="${ item.zipcd }" />
                    <json:property name="basAddr" value="${ item.basAddr }" />
                    <json:property name="dtlAddr" value="${ item.dtlAddr }" />
                    <json:property name="bizno" value="${ item.bizno }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <json:property name="amdrId" value="${ item.amdrId }" />
                    <json:property name="amdDt" value="${ item.amdDt }" />
                    <json:property name="useYn" value="${ item.useYn }" />
                    <json:property name="svcType" value="${ item.svcType }" />
                    <json:property name="appType" value="${ item.appType }" />
                    <json:property name="mirrYn" value="${ item.mirrYn }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'serviceNmList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="serviceNmList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }" />
                        <json:property name="storCount" value="${ i.storCount }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

</json:object>