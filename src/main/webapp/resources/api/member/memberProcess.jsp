<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${ resultType eq 'insertMember' }">
        <c:if test="${ resultCode eq '1000' }">
            <c:if test="${mbrSeq != 0}">
                <json:object name="result">
                    <json:property name="mbrSeq" value="${ mbrSeq }" />
                </json:object>
            </c:if>
        </c:if>
    </c:if>
    <c:if test="${ resultCode eq '1000' }">
        <c:if test="${!empty result}">
            <json:object name="result">
                <json:property name="mbrSeq" value="${ result.mbrSeq }" />
                <json:property name="mbrId" value="${ result.mbrId }" />
                <json:property name="mbrNm" value="${ result.mbrNm }" />
                <json:property name="mbrSe" value="${ result.mbrSe }" />
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'memberTokenInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="id" value="${ id }" />
                <json:property name="token" value="${ token }" />
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'findInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="findInfo">
                    <json:property name="id" value="${ item }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'memberInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="memberInfo">
                    <json:property name="mbrSeq" value="${ item.mbrSeq }" />
                    <json:property name="mbrId" value="${ item.mbrId }" />
                    <json:property name="mbrNm" value="${ item.mbrNm }" escapeXml="false" />
                    <json:property name="mbrTelNo" value="${ item.mbrTelNo }" />
                    <c:set var="tel" value="${fn:split(item.mbrMphonNo,';')}" />
                    <json:array name="mbrMphonList">
                        <c:forEach var="telNum" items="${tel}" varStatus="g">
                            <json:object>
                                <json:property name="mbrMphonNo" value="${ telNum }" />
                            </json:object>
                        </c:forEach>
                    </json:array>
                    <json:property name="mbrEmail" value="${ item.mbrEmail }" />
                    <json:property name="mbrSe" value="${ item.mbrSe }" />
                    <json:property name="mbrSeNm" value="${ item.mbrSeNm }" />
                    <json:property name="mbrSttus" value="${ item.mbrSttus }" />
                    <json:property name="mbrSttusNm" value="${ item.mbrSttusNm }" />
                    <c:if test="${ item.svcSeq != 0 }">
                        <json:property name="svcSeq" value="${ item.svcSeq }" />
                        <json:property name="svcNm" value="${ item.svcNm }" escapeXml="false" />
                    </c:if>
                    <c:if test="${ item.storSeq != 0 }">
                        <json:property name="storSeq" value="${ item.storSeq }" />
                        <json:property name="storNm" value="${ item.storNm }" escapeXml="false" />
                    </c:if>
                    <c:if test="${ item.cpSeq != 0 }">
                        <json:property name="cpSeq" value="${ item.cpSeq }" />
                        <json:property name="cpNm" value="${ item.cpNm }" escapeXml="false" />
                    </c:if>
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="amdDt" value="${ item.amdDt }" />
                    <json:property name="amdrId" value="${ item.amdrId }" />
                    <json:property name="apvDt" value="${ item.apvDt }" />
                    <json:property name="apvrId" value="${ item.apvrId }" />
                    <json:property name="ipadr" value="${ item.ipadr }" />
                    <json:property name="ipadr2" value="${ item.ipadr2 }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'memberList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="memberList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * 10 + count) }" />
                        <json:property name="mbrSeq" value="${ i.mbrSeq }" />
                        <json:property name="mbrId" value="${ i.mbrId }" />
                        <json:property name="mbrNm" value="${ i.mbrNm }" escapeXml="false" />
                        <json:property name="mbrSeNm" value="${ i.mbrSeNm }" />
                        <json:property name="mbrSttusNm" value="${ i.mbrSttusNm }" />
                        <json:property name="cretDt" value="${ i.cretDt }" />
                        <c:if test="${ !empty i.cpNm }">
                            <json:property name="cpNm" value="${ i.cpNm }" escapeXml="false"/>
                        </c:if>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'memberHistoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="memberHistoryList" items="${item}" var="i">
                    <json:object>
                        <json:property name="mbrChghstSeq" value="${ i.mbrChghstSeq }" />
                        <json:property name="mbrSeq" value="${ i.mbrSeq }" />
                        <c:choose>
                            <c:when test="${ i.changeCol eq 'MBR_NM' }">
                                <json:property name="changeCol" value="사용자명" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'MBR_PWD' }">
                                <json:property name="changeCol" value="비밀번호" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'MBR_TEL_NO' }">
                                <json:property name="changeCol" value="전화번호" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'MBR_MPHON_NO' }">
                                <json:property name="changeCol" value="휴대폰" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'MBR_EMAIL' }">
                                <json:property name="changeCol" value="이메일" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'MBR_SE' }">
                                <json:property name="changeCol" value="계정구분" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'MBR_STTUS' }">
                                <json:property name="changeCol" value="상태" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'SVC_SEQ' }">
                                <json:property name="changeCol" value="서비스 명" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'STOR_SEQ' }">
                                <json:property name="changeCol" value="매장 명" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'CP_SEQ' }">
                                <json:property name="changeCol" value="CP사 명" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'IPADR' }">
                                <json:property name="changeCol" value="첫번째 IP Address" />
                            </c:when>
                            <c:when test="${ i.changeCol eq 'IPADR2' }">
                                <json:property name="changeCol" value="두번째 IP Address" />
                            </c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                        <json:property name="legacyValue" value="${ i.legacyValue }" />
                        <json:property name="changeValue" value="${ i.changeValue }" />
                        <json:property name="cretrId" value="${ i.cretrId }" />
                        <json:property name="cretDt" value="${ i.cretDt }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'storeMemberInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="memberInfo">
                    <json:property name="mbrSeq" value="${ item.mbrSeq }" />
                    <json:property name="mbrId" value="${ item.mbrId }" />
                    <json:property name="mbrNm" value="${ item.mbrNm }" escapeXml="false" />
                    <json:property name="mbrTelNo" value="${ item.mbrTelNo }" />
                    <c:set var="tel" value="${fn:split(item.mbrMphonNo,';')}" />
                    <c:forEach var="telNum" items="${tel}" varStatus="g">
                        <c:if test="${g.count == 1}">
	                            <json:property name="mbrMphonNo" value="${ telNum }" />
                        </c:if>
                    </c:forEach>
                    <json:property name="mbrEmail" value="${ item.mbrEmail }" />
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <c:if test="${ item.svcSeq != 0 }">
                        <json:property name="svcSeq" value="${ item.svcSeq }" />
                        <json:property name="svcNm" value="${ item.svcNm }" />
                    </c:if>
                    <c:if test="${ item.storSeq != 0 }">
                        <json:property name="storSeq" value="${ item.storSeq }" />
                        <json:property name="storNm" value="${ item.storNm }" />
                    </c:if>
                    <c:if test="${ devSeq != null }">
                        <json:property name="devSeq" value="${ devSeq }"/>
                    </c:if>
                    <c:if test="${ token != null }">
                        <json:property name="token" value="${ token }"/>
                    </c:if>
                    <json:property name="storCode" value="${ item.storCode }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'posMemberInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="memberInfo">
                    <json:property name="seq" value="${ item.seq }" />
                    <json:property name="memberId"
                        value="${ item.memberId }" />
                    <json:property name="memberName"
                        value="${ item.memberName }" />
                    <c:choose>
                        <c:when
                            test="${ item.memberPhone1 != '' && item.memberPhone2 != '' && item.memberPhone3 != '' }">
                            <json:property name="memberPhone"
                                value="${ item.memberPhone1 }-${ item.memberPhone2 }-${ item.memberPhone3 }" />
                        </c:when>
                        <c:otherwise>
                            <json:property name="memberPhone" value="" />
                        </c:otherwise>
                    </c:choose>
                    <json:property name="mobilePhone"
                        value="${ item.mobilePhone1 }-${ item.mobilePhone2 }-${ item.mobilePhone3 }" />
                    <json:property name="memberEmail"
                        value="${ item.memberEmail1 }@${item.memberEmail2}" />
                    <json:property name="memberSec"
                        value="${ item.memberSecName }" />
                    <json:property name="memberOffice"
                        value="${ item.memberOffice }" />
                    <json:property name="storeCode"
                        value="${ item.storeCode }" />
                    <c:if test="${ item.memberSec != null }">
                        <json:property name="memberSecSeq"
                            value="${ item.memberSec }" />
                    </c:if>
                    <c:if test="${ clientSeq != null }">
                        <json:property name="clientSeq"
                            value="${ clientSeq }" />
                    </c:if>
                    <c:if test="${ item.memberOffice != null }">
                        <json:property name="officeSeq"
                            value="${ item.memberOfficeSeq }" />
                    </c:if>
                    <c:if test="${ token != null }">
                        <json:property name="token" value="${ token }" />
                    </c:if>
                    <json:property name="regDate"
                        value="${ item.regDate }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'cofirmPasswd' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="memberInfo">
                    <json:property name="mbrId" value="${ item.mbrId }" />
                    <json:property name="mbrPwd" value="${ item.mbrPwd }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'integratedMemberInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="integratedMemberInfo">
                    <json:property name="intgrateAthnMbrSeq" value="${ item.intgrateAthnMbrSeq }" />
                    <json:property name="uid" value="${ item.uid }" />
                    <json:property name="svcSeq" value="${ item.svcSeq }" />
                    <json:property name="joinRoute" value="${ item.joinRoute }" />
                    <json:property name="mbrNm" value="${ item.mbrNm }" escapeXml="false" />
                    <json:property name="nickName" value="${ item.nickName }" />
                    <json:property name="accRoute" value="${ item.accRoute }" />
                    <json:property name="mbrTelNo" value="${ item.mbrTelNo }" />
                    <json:property name="mbrMphonNo" value="${ item.mbrMphonNo }" />
                    <json:property name="mbrEmail" value="${ item.mbrEmail }" />
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <json:property name="amdDt" value="${ item.amdDt }" />
                    <json:property name="useYn" value="${ item.useYn }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'integratedMemberList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="integratedMemberList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * 10 + count) }" />
                        <json:property name="svcSeq" value="${ i.svcSeq }" />
                        <json:property name="joinRoute" value="${ i.joinRoute }" />
                        <json:property name="uid" value="${ i.uid }" />
                        <json:property name="mbrNm" value="${ i.mbrNm }" escapeXml="false" />
                        <json:property name="accRoute" value="${ i.accRoute }" />
                        <json:property name="mbrEmail" value="${ i.mbrEmail }" />
                        <json:property name="useYn" value="${ i.useYn }" />
                        <json:property name="cretDt" value="${ i.cretDt }" />
                        <json:property name="intgrateAthnMbrSeq" value="${ i.intgrateAthnMbrSeq }" />
                        <json:property name="indv_info_agree_yn" value="${ i.indvInfoAgreeYn }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'memberTermsHstList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="memberTermsList" items="${item}" var="i">
                    <json:object>
                        <json:property name="stpltSeq" value="${ i.stpltSeq }" />
                        <json:property name="stpltType" value="${ i.stpltTypeNm }" />
                        <json:property name="stpltVer" value="${ i.stpltVer }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>
</json:object>