<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />

    <c:if test="${ resultType eq 'banrDisplayList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="banrDisplayList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="banrSeq" value=" ${ i.banrSeq }" />
                        <json:property name="banrTitle" value=" ${ i.banrTitle }" escapeXml="false"/>
                        <json:property name="banrSubTitle" value=" ${ i.banrSubTitle }" escapeXml="false"/>
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                        <json:property name="svcSeq" value=" ${ i.svcSeq }" />
                        <json:property name="banrType" value=" ${ i.banrType }"/>
                        <json:property name="banrLink" value=" ${ i.banrLink }"/>
                        <json:property name="fileSeq" value="${ i.fileSeq }"/>
                        <json:property name="orginlFileNm" value="${ i.orginlFileNm }"/>
                        <json:property name="filePath" value="${ i.filePath }"/>
                        <json:property name="fileSize" value="${ i.fileSize }"/>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'banrPlcyInsert' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:object name="banrPlcyInsert">
                        <json:property name="plcySeq" value="${ item.plcySeq }"/>
                    </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'banrInsert' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:object name="banrInsert">
                        <json:property name="banrSeq" value="${ item.banrSeq }"/>
                    </json:object>
            </json:object>
        </c:if>
    </c:if>





    <c:if test="${ resultType eq 'banrPlcyInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="plcyInfo">
	                <json:property name="plcySeq" value="${ item.plcySeq }"/>
	                <json:property name="terrNm" value="${ item.terrNm }" escapeXml="false"/>
	                <json:property name="terrUrl" value="${ item.terrUrl }" />
	                <json:property name="svcSeq" value="${ item.svcSeq }"/>
	                <json:property name="svcNm" value="${ item.svcNm }"/>
	                <json:property name="size" value="${ item.size}"/>
	                <json:property name="sizeHght" value="${ item.sizeHght}"/>
	                <json:property name="sizeWdth" value="${ item.sizeWdth}"/>
	                <json:property name="fileTypeALL" value="${ item.fileType}"/>
	                <c:if test="${item.fileType != null and item.fileType != ''}">
	                <c:forEach items="${fn:split(item.fileType, ',') }" var="sitem">
	                    <json:object name="fileType">
	                        <json:property name="typeNm" value="${ sitem }"/>
	                    </json:object>
	                </c:forEach>
	                </c:if>

	                <json:property name="lmtFileSize" value="${ item.lmtFileSize }"/>
	                <c:if test="${item.fileSeq != null and item.fileSeq != ''}">
		                <json:object name="banrImg">
	                        <json:property name="fileSeq" value="${ item.fileSeq }"/>
	                        <json:property name="coverImgNm" value="${ item.orginlFileNm }"/>
	                        <json:property name="coverImgPath" value="${ item.filePath }"/>
	                        <json:property name="coverImgSize" value="${ item.fileSize }"/>
	                    </json:object>
                    </c:if>
                    <json:property name="useYn" value="${ item.useYn }"/>
                    <json:property name="delYn" value="${ item.delYn }"/>
	                <json:property name="amdDt" value="${ item.amdDt }"/>
	                <json:property name="amdrId" value="${ item.amdrId }"/>
	                <json:property name="cretDt" value="${ item.cretDt }"/>
	                <json:property name="cretrId" value="${ item.cretrId }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'banrPlcyList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="plcyList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="plcySeq" value="${ i.plcySeq }"/>
                        <json:property name="terrNm" value="${ i.terrNm }" escapeXml="false"/>
                        <json:property name="terrUrl" value="${ i.terrUrl }" escapeXml="false"/>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="size" value="${ i.size}"/>
                        <json:property name="sizeHght" value="${ i.sizeHght}"/>
                        <json:property name="sizeWdth" value="${ i.sizeWdth}"/>
                        <json:property name="fileTypeALL" value="${ i.fileType}"/>
                        <c:if test="${i.fileType != null and i.fileType != ''}">
	                        <c:forEach items="${fn:split(i.fileType, ',') }" var="sitem">
	                        <json:object name="fileType">
	                            <json:property name="typeNm" value="${ sitem }"/>
	                        </json:object>
	                        <json:property name="lmtFileSize" value="${ i.lmtFileSize }"/>
	                        </c:forEach>
                        </c:if>
                        <c:if test="${i.fileSeq != null and i.fileSeq != ''}">
	                        <json:object name="banrImg">
	                        <json:property name="fileSeq" value="${ i.fileSeq }"/>
	                        <json:property name="coverImgNm" value="${ i.orginlFileNm }"/>
	                        <json:property name="coverImgPath" value="${ i.filePath }"/>
	                        <json:property name="coverImgSize" value="${ i.fileSize }"/>
	                        </json:object>
                        </c:if>
                        <json:property name="amdDt" value="${ i.amdDt }"/>
                        <json:property name="amdrId" value="${ i.amdrId }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <json:property name="cretrId" value="${ i.cretrId }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>



    <c:if test="${ resultType eq 'banrCprtInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="banrCprtInfo">
                    <json:property name="cprtSvcSeq" value="${ item.cprtSvcSeq }"/>
                    <json:property name="cprtSvcNm" value="${ item.cprtSvcNm }" escapeXml="false"/>
                    <json:property name="menuUrl" value="${ item.menuUrl }" escapeXml="false"/>
                    <json:property name="perdYn" value="${ item.perdYn }"/>
                    <json:property name="stDt" value="${ item.stDt }"/>
                    <json:property name="fnsDt" value="${ item.fnsDt }"/>
                    <json:array name="cprtImgList" items="${cprtImg}" var="j">
                        <json:object>
                            <json:property name="fileSeq" value="${ j.fileSeq }"/>
                            <json:property name="imgNm" value="${ j.orginlFileNm }"/>
                            <json:property name="imgPath" value="${ j.filePath }"/>
                        </json:object>
                    </json:array>

                    <json:array name="cprtThumbImg" items="${cprtThumbImg}" var="z">
                        <json:object>
                            <json:property name="fileSeq" value="${ z.fileSeq }"/>
                            <json:property name="imgNm" value="${ z.orginlFileNm }"/>
                            <json:property name="imgPath" value="${ z.filePath }"/>
                        </json:object>
                    </json:array>

                    <json:property name="perdSttus" value="${ item.perdSttus }"/>
                    <json:property name="useYn" value="${ item.useYn }"/>
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="cretrId" value="${ item.cretrId }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                    <json:property name="amdrId" value="${ item.amdrId }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'banrCprtList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="cprtList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="cprtSvcSeq" value="${ i.cprtSvcSeq }"/>
                        <json:property name="cprtSvcNm" value="${ i.cprtSvcNm }" escapeXml="false"/>
                        <json:property name="menuUrl" value="${ i.menuUrl }" escapeXml="false"/>
                        <json:property name="perdYn" value="${ i.perdYn}"/>
                        <json:property name="stDt" value="${ i.stDt }"/>
                        <json:property name="fnsDt" value="${ i.fnsDt }"/>
                        <json:array name="cprtImg" items="${i.cprtImgList}" var="j">
                            <json:object>
                                <json:property name="fileSeq" value="${ j.fileSeq }"/>
                                <json:property name="imgNm" value="${ j.orginlFileNm }"/>
                                <json:property name="imgPath" value="${ j.filePath }"/>
                            </json:object>
                        </json:array>

                        <json:array name="cprtThmbImg" items="${i.cprtThmbImgList}" var="z">
                            <json:object>
                                <json:property name="fileSeq" value="${ z.fileSeq }"/>
                                <json:property name="imgNm" value="${ z.orginlFileNm }"/>
                                <json:property name="imgPath" value="${ z.filePath }"/>
                            </json:object>
                        </json:array>

                        <json:property name="useYn" value="${ i.useYn}"/>
                        <json:property name="svcSeq" value="${ i.svcSeq}"/>
                        <json:property name="svcNm" value="${ i.svcNm}"/>
                        <json:property name="amdDt" value="${ i.amdDt }"/>
                        <json:property name="amdrId" value="${ i.amdrId }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <json:property name="cretrId" value="${ i.cretrId }"/>
                        <json:property name="cretrNm" value="${ i.cretrNm }"/>
                        <json:property name="perdSttus" value="${ i.perdSttus}"/>
                        <json:property name="isImgFile" value="${ i.isImgFile }"/>
                        <json:property name="isThumbImgFile" value="${ i.isThumbImgFile}"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'banrInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="banrInfo">
                    <json:property name="banrSeq" value="${ item.banrSeq }"/>
                    <json:property name="orderNum" value="${ item.orderNum }" />
                    <json:property name="banrCampNm" value="${ item.banrCampNm }" />
                    <json:property name="svcSeq" value="${ item.svcSeq}" />
                    <json:property name="svcNm" value="${ item.svcNm}" />
                    <json:property name="plcySeq" value="${ item.plcySeq}" />
                    <json:property name="terrNm" value="${ item.terrNm}" />
                    <json:property name="perdYn" value="${ item.perdYn}" />
                    <json:property name="stDt" value="${ item.stDt}" />
                    <json:property name="fnsDt" value="${ item.fnsDt}" />
                    <json:property name="linkUrl" value="${ item.linkUrl}" escapeXml="false"/>
                    <json:property name="linkUri" value="${ item.linkUri}" escapeXml="false"/>
                    <json:property name="useTitleType" value="${ item.useTitleType}" />
                    <json:property name="btnTitle" value="${ item.btnTitle}" />
                    <json:property name="banrTitle" value="${ item.banrTitle}" />
                    <json:property name="banrSubTitle" value="${ item.banrSubTitle}" />
                    <json:property name="useYn" value="${ item.useYn}" />
                    <json:property name="perdSttus" value="${ item.perdSttus}" />
                    <json:object name="banrImg">
                        <c:if test="${item.fileSeq != null and item.fileSeq != ''}">
                            <json:property name="fileSeq" value="${ item.fileSeq }"/>
                            <json:property name="imgNm" value="${ item.orginlFileNm }"/>
                            <json:property name="imgPath" value="${ item.filePath }"/>
                            <json:property name="fileSize" value="${ item.fileSize }"/>
                        </c:if>
                    </json:object>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="cretrNm" value="${ item.cretrNm }"/>
                    <json:property name="cretrId" value="${ item.cretrId }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                    <json:property name="amdrId" value="${ item.amdrId }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'banrList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="banrList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="banrSeq" value="${ i.banrSeq }"/>
                        <json:property name="orderNum" value="${ i.orderNum }"/>
                        <json:property name="banrCampNm" value="${ i.banrCampNm }" escapeXml="false"/>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="perdYn" value="${ i.perdYn }"/>
                        <json:property name="perdSttus" value="${ i.perdSttus }"/>
                        <json:property name="perdDt" value="${ i.perdDt }"/>
                        <json:property name="stDt" value="${ i.stDt }"/>
                        <json:property name="fnsDt" value="${ i.fnsDt }"/>
                        <json:object name="banrImg">
                            <c:if test="${i.fileSeq != null and i.fileSeq != ''}">
                                <json:property name="fileSeq" value="${ i.fileSeq }"/>
                                <json:property name="imgNm" value="${ i.orginlFileNm }"/>
                                <json:property name="imgPath" value="${ i.filePath }"/>
                                <json:property name="fileSize" value="${ i.fileSize }"/>
                            </c:if>
                        </json:object>
                        <json:property name="linkUrl" value="${ i.linkUrl }" escapeXml="false"/>
                        <json:property name="linkUri" value="${ i.linkUri }" escapeXml="false"/>
                        <json:property name="useTitleType" value="${ i.useTitleType }"/>
                        <json:property name="btnTitle" value="${ i.btnTitle }" escapeXml="false"/>
                        <json:property name="banrTitle" value="${ i.banrTitle }" escapeXml="false"/>
                        <json:property name="banrSubTitle" value="${ i.banrSubTitle }" escapeXml="false"/>
                        <json:property name="useYn" value="${ i.useYn }"/>
                        <json:property name="delYn" value="${ i.delYn }"/>
                        <json:property name="plcySeq" value="${ i.plcySeq }"/>
                        <json:property name="terrNm" value="${ i.terrNm }"/>
                        <json:property name="amdDt" value="${ i.amdDt }"/>
                        <json:property name="amdrId" value="${ i.amdrId }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <json:property name="cretrNm" value="${ i.cretrNm }"/>
                        <json:property name="cretrId" value="${ i.cretrId }"/>
                        <json:property name="isImgFile" value="${ i.isImgFile }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'banrAPIList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="banrList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="banrSeq" value="${ i.banrSeq }"/>
                        <json:property name="banrCampNm" value="${ i.banrCampNm }" escapeXml="false"/>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="terrNm" value="${ i.terrNm }"/>
                        <json:property name="perdYn" value="${ i.perdYn }"/>
                        <json:property name="stDt" value="${ i.stDt }"/>
                        <json:property name="fnsDt" value="${ i.fnsDt }"/>
                        <json:property name="linkUrl" value="${ i.linkUrl }" escapeXml="false"/>
                        <json:property name="linkUri" value="${ i.linkUri }" escapeXml="false"/>
                        <c:forEach items="${fn:split(i.useTitleType, ',') }" var="sitem">
                            <c:choose>
                                <c:when test="${sitem eq 'BT'}">
                                    <json:property name="btnTitle" value="${ i.btnTitle }" escapeXml="false"/>
                                </c:when>
                                <c:when test="${sitem eq 'BNT'}">
                                    <json:property name="banrTitle" value="${ i.banrTitle }" escapeXml="false"/>
                                </c:when>
                                <c:when test="${sitem eq 'BST'}">
                                    <json:property name="banrSubTitle" value="${ i.banrSubTitle }" escapeXml="false"/>
                                </c:when>
                            </c:choose>

                        </c:forEach>

                        <json:object name="banrImg">
                            <c:if test="${i.fileSeq != null and i.fileSeq != ''}">
                                <json:property name="fileSeq" value="${ i.fileSeq }"/>
                                <json:property name="imgNm" value="${ i.orginlFileNm }"/>
                                <json:property name="imgPath" value="${ i.filePath }"/>
                                <json:property name="fileSize" value="${ i.fileSize }"/>
                            </c:if>
                        </json:object>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'banrAPIInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="banrInfo">
                    <json:property name="banrCampNm" value="${ item.banrCampNm }" />
                    <json:property name="svcSeq" value="${ item.svcSeq}" />
                    <json:property name="svcNm" value="${ item.svcNm}" />
                    <json:property name="terrNm" value="${ item.terrNm}" />
                    <json:property name="perdYn" value="${ item.perdYn}" />
                    <json:property name="stDt" value="${ item.stDt}" />
                    <json:property name="fnsDt" value="${ item.fnsDt}" />
                    <json:property name="linkUrl" value="${ item.linkUrl}" escapeXml="false"/>
                    <json:property name="linkUri" value="${ item.linkUri}" escapeXml="false"/>
                    <c:forEach items="${fn:split(item.useTitleType, ',') }" var="sitem">
                        <c:choose>
                            <c:when test="${sitem eq 'BT'}">
                                <json:property name="btnTitle" value="${ item.btnTitle }" escapeXml="false"/>
                            </c:when>
                            <c:when test="${sitem eq 'BNT'}">
                                <json:property name="banrTitle" value="${ item.banrTitle }" escapeXml="false"/>
                            </c:when>
                            <c:when test="${sitem eq 'BST'}">
                                <json:property name="banrSubTitle" value="${ item.banrSubTitle }" escapeXml="false"/>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                    <json:object name="banrImg">
                        <c:if test="${item.fileSeq != null and item.fileSeq != ''}">
                            <json:property name="fileSeq" value="${ item.fileSeq }"/>
                            <json:property name="imgNm" value="${ item.orginlFileNm }"/>
                            <json:property name="imgPath" value="${ item.filePath }"/>
                            <json:property name="fileSize" value="${ item.fileSize }"/>
                        </c:if>
                    </json:object>
                </json:object>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'banrCprtAPIInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="cprtInfo">
                    <json:property name="cprtSvcSeq" value="${ item.cprtSvcSeq }"/>
                    <json:property name="cprtSvcNm" value="${ item.cprtSvcNm }" escapeXml="false"/>
                    <json:property name="menuUrl" value="${ item.menuUrl }" escapeXml="false"/>
                    <json:property name="perdYn" value="${ item.perdYn }"/>
                    <json:property name="stDt" value="${ item.stDt }"/>
                    <json:property name="fnsDt" value="${ item.fnsDt }"/>

                    <json:array name="cprtImgList" items="${cprtImg}" var="j">
                        <json:object>
                            <json:property name="fileSeq" value="${ j.fileSeq }"/>
                            <json:property name="imgNm" value="${ j.orginlFileNm }"/>
                            <json:property name="imgPath" value="${ j.filePath }"/>
                        </json:object>
                    </json:array>

                    <json:array name="cprtThmbImg" items="${cprtThumbImg}" var="z">
                        <json:object>
                            <json:property name="fileSeq" value="${ z.fileSeq }"/>
                            <json:property name="imgNm" value="${ z.orginlFileNm }"/>
                            <json:property name="imgPath" value="${ z.filePath }"/>
                        </json:object>
                    </json:array>
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'banrCprtAPIList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="cprtList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="cprtSvcSeq" value="${ i.cprtSvcSeq }"/>
                        <json:property name="cprtSvcNm" value="${ i.cprtSvcNm }" escapeXml="false"/>
                        <json:property name="menuUrl" value="${ i.menuUrl }" escapeXml="false"/>
                        <json:property name="perdYn" value="${ i.perdYn}"/>
                        <json:property name="stDt" value="${ i.stDt }"/>
                        <json:property name="fnsDt" value="${ i.fnsDt }"/>
                        <json:array name="cprtImgList" items="${i.cprtImgList}" var="j">
                            <json:object>
                                <json:property name="fileSeq" value="${ j.fileSeq }"/>
                                <json:property name="imgNm" value="${ j.orginlFileNm }"/>
                                <json:property name="imgPath" value="${ j.filePath }"/>
                            </json:object>
                        </json:array>

                        <json:array name="cprtThmbImg" items="${i.cprtThmbImgList}" var="z">
                            <json:object>
                                <json:property name="fileSeq" value="${ z.fileSeq }"/>
                                <json:property name="imgNm" value="${ z.orginlFileNm }"/>
                                <json:property name="imgPath" value="${ z.filePath }"/>
                            </json:object>
                        </json:array>
                        <json:property name="svcSeq" value="${ i.svcSeq}"/>
                        <json:property name="svcNm" value="${ i.svcNm}"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
             </json:object>
        </c:if>
    </c:if>


</json:object>