<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:set var="count" value="0"></c:set>

    <c:if test="${ resultType eq 'productInsert' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:property name="prodSeq" value="${ prodSeq }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'productList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="productList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="prodSeq" value="${ i.prodSeq }" />
                        <json:property name="prodNm" value="${ i.prodNm }" escapeXml="false" />
                        <json:property name="prodTimeLmt" value="${ i.prodTimeLmt }" />
                        <json:property name="prodLmtCnt" value="${ i.prodLmtCnt }" />
                        <json:property name="prodPrc" value="${ i.prodPrc }" />
                        <json:property name="useYn" value="${ i.useYn }"/>
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'productDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="productInfo">
                    <json:property name="prodSeq" value="${ item.prodSeq }"/>
                    <json:property name="storSeq" value="${ item.storSeq }"/>
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }"/>
                    <json:property name="storNm" value="${ item.storNm }"/>
                    <json:property name="prodNm" value="${ item.prodNm }" escapeXml="false"/>
                    <json:property name="prodLmtCnt" value="${ item.prodLmtCnt }"/>
                    <json:property name="prodTimeLmt" value="${ item.prodTimeLmt }"/>
                    <json:property name="prodPrc" value="${ item.prodPrc }"/>
                    <json:property name="prodDesc" value="${ item.prodDesc }" escapeXml="false"/>
                    <json:property name="gsrProdCode" value="${ item.gsrProdCode }"/>
                    <json:property name="useYn" value="${ item.useYn }"/>
                </json:object>
                <json:array name="productCategory" items="${CItem}" var="i" >
                    <json:object>
                        <json:property name="prodCtgSeq" value="${ i.prodCtgSeq }"/>
                        <json:property name="devCtgNm" value="${ i.devCtgNm }"/>
                        <json:property name="lmtCnt" value="${ i.lmtCnt }"/>
                        <json:property name="deduction" value="${ i.deduction }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'productContsCategoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="productContsCategoryList" items="${item}" var="i">
                    <json:object>
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'productsListAPI_POS' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="productList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${count+1 }" />
                        <json:property name="prodSeq" value="${ i.prodSeq }"/>
                        <json:property name="prodNm" value="${ i.prodNm }"/>
                        <json:property name="gsrProdCode" value="${ i.gsrProdCode }"/>
                        <json:property name="prodLmtCnt" value="${ i.prodLmtCnt }"/>
                        <json:property name="prodTimeLmt" value="${ i.prodTimeLmt }"/>
                        <json:property name="prodPrc" value="${ i.prodPrc }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="regDt" value="${ i.regDt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'productsListAPI' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="productList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${count+1 }" />
                        <json:property name="productSeq" value="${ i.prodSeq }"/>
                        <json:property name="productName" value="${ i.prodNm }"/>
                        <json:property name="gsrProdCode" value="${ i.gsrProdCode }"/>
                        <json:property name="prodLmtCnt" value="${ i.prodLmtCnt }"/>
                        <c:if test="${i.prodLmtCnt == 0}">
                            <json:property name="prodLmtCnt" value="${ 9999 }" />
                        </c:if>
                        <json:property name="prodTimeLmt" value="${ i.prodTimeLmt }"/>
                        <json:property name="prodPrc" value="${ i.prodPrc }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="regDate" value="${ i.regDt }"/>
                        <json:array name="prodCategoryList" items="${i.prodCategoryList}" var="j" >
                            <json:object>
                                <json:property name="devCtgNm" value="${ j.devCtgNm }"/>
                                <json:property name="lmtCnt" value="${ j.lmtCnt }"/>
                            </json:object>
                        </json:array>
                        <c:set var="count" value="${count + 1 }"></c:set>

                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

</json:object>