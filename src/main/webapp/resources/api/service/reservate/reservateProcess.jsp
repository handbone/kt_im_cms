<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0" />
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />

    <c:if test="${ resultType eq 'reservateList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="reservateList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*row+count) }" />
                        <json:property name="tagSeq" value="${ i.tagSeq }" />
                        <json:property name="tagId" value="${ i.tagId }" />
                        <json:property name="devSeq" value="${ i.devSeq }" />
                        <json:property name="useYn" value="${ i.useYn }" />
                        <json:property name="useSttus" value="${ i.useSttus }" />
                        <json:property name="useYnNm" value="${ i.useYnNm }" />
                        <json:property name="settlSeq" value="${ i.settlSeq }" />
                        <json:property name="prodLmtCnt" value="${ i.prodLmtCnt }" />
                        <json:property name="usageMinute" value="${i.usageMinute}" />
                        <json:property name="rmndCnt" value="${i.rmndCnt}" />
                        <json:property name="prodNm" value="${i.prodNm}" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage"
                    value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage"
                            value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage"
                            value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'reservateDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="reservateInfo">
                    <json:property name="tagSeq" value="${ item.tagSeq }"/>
                    <json:property name="storSeq" value="${ item.storSeq }"/>
                    <json:property name="tagId" value="${ item.tagId }"/>
                    <json:property name="useYnCode" value="${ item.useYnCode }"/>
                    <json:property name="useYnNm" value="${ item.useYnNm }"/>
                    <json:property name="prodSeq" value="${ item.prodSeq }"/>
                    <json:property name="prodNm" value="${ item.prodNm }"/>
                    <json:property name="prodLmtCnt" value="${ item.prodLmtCnt }"/>
                    <json:property name="prodTimeLmt" value="${ item.prodTimeLmt }"/>
                    <json:property name="rmndCnt" value="${ item.rmndCnt }"/>
                    <json:property name="devTypeNm" value="${ item.devTypeNm }"/>
                    <json:property name="usageMinute" value="${item.usageMinute}"/>
                    <json:property name="regDt" value="${ item.regDt }"/>
                    <json:array name="usageCategoryCount" items="${CItem}" var="i">
                        <json:object>
                            <json:property name="categorySeq" value="${ i.tagCtgSeq }" />
                            <json:property name="categoryName" value="${ i.devCtgNm }" />
                            <json:property name="categoryCount" value="${i.lmtCnt}"/>
                            <json:property name="categoryUsed" value="${i.ctgUseYn}"/>
                            <json:property name="deduction" value="${i.deduction}"/>
                        </json:object>
                    </json:array>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'reservateUseList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="reservateUseList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="storSeq" value="${i.storSeq}"/>
                        <json:property name="devSeq" value="${i.devSeq}"/>
                        <json:property name="categoryName" value="${i.devTypeNm}"/>
                        <json:property name="tagId" value="${ i.tagId }"/>
                        <json:property name="useSttus" value="${ i.useSttus }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'reservateUseContentList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="useContentList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*row+count) }" />
                        <json:property name="contsUseTimeSeq" value="${ i.contsUseTimeSeq }"/>
                        <json:property name="devSeq" value="${ i.devSeq }"/>
                        <json:property name="devId" value="${ i.devId }"/>
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="stTime" value="${ i.stTime }"/>
                        <json:property name="fnsTime" value="${ i.fnsTime }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'reservateSecondCategoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="reservateSecondCategoryList" items="${item}" var="i">
                    <json:object>
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

</json:object>
