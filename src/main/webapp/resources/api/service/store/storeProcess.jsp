<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and marketNm != ''}">
        <json:object name="result">
            <json:property name="marketNm" value="${ marketNm }" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'storeList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="storeList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="storNm" value="${ i.storNm }" escapeXml="false" />
                        <json:property name="storTelNo" value="${ i.storTelNo }" />
                        <json:property name="storBasAddr" value="${ i.storBasAddr }" />
                        <json:property name="useYn" value="${ i.useYn }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'storeNmList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="storeNmList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="storNm" value="${ i.storNm }" escapeXml="false" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'storeDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="storeInfo">
                    <json:property name="storSeq" value="${ item.storSeq }"/>
                    <json:property name="storNm" value="${ item.storNm }" escapeXml="false"/>
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }" escapeXml="false"/>
                    <json:property name="cretrId" value="${ item.cretrId }"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="amdrId" value="${ item.amdrId }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                    <json:property name="storTelNo" value="${ item.storTelNo }"/>
                    <json:property name="zipcd" value="${ item.zipcd }"/>
                    <json:property name="storBasAddr" value="${ item.storBasAddr }"/>
                    <json:property name="storDtlAddr" value="${ item.storDtlAddr }"/>
                    <json:property name="storCode" value="${ item.storCode }"/>
                    <json:property name="useYn" value="${ item.useYn }"/>
                    <c:if test="${ ratContNo ne '' }">
                        <json:property name="ratContCnt" value="${ item.ratContCnt }"/>
                    </c:if>
                </json:object>
                <json:object name="reservationSetting">
                    <json:property name="rsrvTimeSetCycle" value="${ item.rsrvTimeSetCycle }"/>
                    <json:property name="stTime" value="${ item.stTime }"/>
                    <json:property name="fnsTime" value="${ item.fnsTime }"/>
                </json:object>
                <json:array name="useDevList" items="${devCount}" var="j">
                    <json:object>
                        <json:property name="devTypeNm" value=" ${j.devTypeNm}" />
                        <json:property name="count" value="${ j.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'storeBillingList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="billingList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }" escapeXml="false"/>
                        <json:property name="storSeq" value="${ i.storSeq }" />
                        <json:property name="storNm" value="${ i.storNm }"  escapeXml="false"/>
                        <json:property name="count" value="${ i.count }" />
                        <json:property name="totPrice" value="${ i.totPrice }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'billingDetailList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="marketNm" value="${ marketNm }" />
                <json:array name="billingDetailList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="ratSeq" value="${ i.ratSeq }"/>
                        <json:property name="ratContNo" value="${ i.ratContNo }" />
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsCount" value="${ i.contsCount }" />
                        <json:property name="ratYn" value="${ i.ratYn }" />
                        <json:property name="ratContType" value="${ i.ratContType }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="totPrice" value="${ i.totPrice }" />
                        <json:property name="usePrice" value="${ i.usePrice }" />
                        <json:property name="useCount" value="${ i.useCount }" />
                        <json:property name="setVal" value="${ i.setVal }" />
                        <json:property name="amdDt" value="${ i.amdDt }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }(${ i.cretrId })" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'billingInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
            <json:property name="marketNm" value="${ marketNm }" />
            <json:property name="storNm" value="${ storNm }" />
                <json:property name="svcNm" value="${ svcNm }" />
                <json:object name="billingInfo">
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="storSeq" value="${ item.storSeq }"/>
                    <json:property name="ratSeq" value="${ item.ratSeq }"/>
                    <json:property name="ratContNo" value="${ item.ratContNo }" />
                    <json:property name="contsTitle" value="${ item.contsTitle }" escapeXml="false"/>
                    <json:property name="ratYn" value="${ item.ratYn }" />
                    <json:property name="ratContType" value="${ item.ratContType }" />
                    <json:property name="ratContStDt" value="${ item.ratContStDt }" />
                    <json:property name="ratContFnsDt" value="${ item.ratContFnsDt }" />
                    <json:property name="runLmtCnt" value="${ item.runLmtCnt }" />
                    <json:property name="runPerPrc" value="${ item.runPerPrc }" />
                    <json:property name="ratTime" value="${ item.ratTime }" />
                    <json:property name="ratPrc" value="${ item.ratPrc }" />
                    <json:property name="lmsmpyPrc" value="${ item.lmsmpyPrc }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <json:property name="amdrId" value="${ item.amdrId }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="amdDt" value="${ item.amdDt }" />

                    <json:array name="contsList" items="${cntItem}" var="i">
                        <json:object>
                            <json:property name="contsSeq" value="${ i.contsSeq }"/>
                            <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                            <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                            <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                            <json:property name="amdDt" value="${ i.amdDt }"/>
                            <json:property name="cretDt" value="${ i.cretDt }"/>
                        </json:object>
                    </json:array>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'billingHistoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="billingHistoryList" items="${item}" var="i">
                    <json:object>
                        <json:property name="ratContHstSeq" value="${ i.ratContHstSeq }"/>
                        <json:property name="ratSeq" value="${ i.ratSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsCount" value="${ i.contsCount }" />
                        <json:property name="ratYn" value="${ i.ratYn }" />
                        <json:property name="ratContType" value="${ i.ratContType }" />
                        <json:property name="totPrice" value="${ i.totPrice }" />
                        <json:property name="setVal" value="${ i.setVal }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="amdrId" value="${ i.amdrId }" />
                        <json:property name="amdDt" value="${ i.amdDt }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'cpContStat' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="cpContStat" items="${item}" var="i">
                    <json:object>
                        <json:property name="cpSeq" value="${ i.cpSeq }"/>
                        <json:property name="cpNm" value="${ i.cpNm }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="contract" value="${ i.contract }" escapeXml="false"/>
                        <json:property name="contractType" value="${ i.contractType }" />
                        <json:property name="playTime" value="${ i.playTime }" />
                        <json:property name="pricePerSec" value="${ i.pricePerSec }" />
                        <json:property name="price" value="${ i.price }" />
                        <json:property name="playCnts" value="${ i.playCnts }" />
                        <json:property name="totalPlayCnts" value="${ i.totalPlayCnts }" />
                        <json:property name="remainPlayCnts" value="${ i.remainPlayCnts }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcContStat' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="svcContStat" items="${item}" var="i">
                    <json:object>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="contract" value="${ i.contract }" escapeXml="false"/>
                        <json:property name="contractType" value="${ i.contractType }" />
                        <json:property name="playTime" value="${ i.playTime }" />
                        <json:property name="pricePerSec" value="${ i.pricePerSec }" />
                        <json:property name="price" value="${ i.price }" />
                        <json:property name="playCnts" value="${ i.playCnts }" />
                        <json:property name="totalPlayCnts" value="${ i.totalPlayCnts }" />
                        <json:property name="remainPlayCnts" value="${ i.remainPlayCnts }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>





</json:object>