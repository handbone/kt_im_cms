<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0" />
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'categoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="categoryList" items="${item}" var="i">
                    <json:object>
                        <json:property name="devTypeNm"
                            value="${ i.devTypeNm }" />
                        <json:property name="devCtgCnt"
                            value="${ i.devCtgCnt }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'deviceList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="deviceList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="devSeq" value="${ i.devSeq }"/>
                        <json:property name="devId" value="${ i.devId }" escapeXml="false"/>
                        <json:property name="devTypeNm" value="${ i.devTypeNm }"/>
                        <json:property name="devTypeNmFull" value="${ i.devTypeNmFull }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="frmtnSeq" value="${ i.frmtnSeq }"/>
                        <json:property name="frmtnNm" value="${ i.frmtnNm }"/>
                        <json:property name="macAdr" value="${ i.macAdr }"/>
                        <json:property name="devIpadr" value="${ i.devIpadr }"/>
                        <json:property name="playTime" value="${ i.playTime }"/>
                        <json:property name="cretrId" value="${ i.cretrId }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <json:property name="amdrId" value="${ i.amdrId }"/>
                        <json:property name="amdDt" value="${ i.amdDt }"/>
                        <json:property name="mbrNm" value="${ i.mbrNm }"/>
                        <json:property name="delYn" value="${ i.delYn }"/>
                        <json:array name="useTagList" items="${i.useTagList}" var="k">
                            <json:object>
                                <json:property name="tagId" value="${ k.tagId }"/>
                            </json:object>
                        </json:array>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'deviceInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="deviceInfo">
                        <json:property name="devSeq" value="${ item.devSeq }"/>
                        <json:property name="devId" value="${ item.devId }" escapeXml="false"/>
                        <json:property name="devTypeNm" value="${ item.devTypeNm }"/>
                        <json:property name="svcSeq" value="${ item.svcSeq }"/>
                        <json:property name="svcNm" value="${ item.svcNm }" escapeXml="false"/>
                        <json:property name="storSeq" value="${ item.storSeq }"/>
                        <json:property name="storNm" value="${ item.storNm }" escapeXml="false"/>
                        <json:property name="frmtnCnt" value="${ item.frmtnCnt }"/>
                        <json:property name="devContsCnt" value="${ item.devContsCnt }"/>
                        <json:property name="playTime" value="${ item.playTime }"/>
                        <json:property name="devIpadr" value="${ item.devIpadr }"/>
                        <json:property name="macAdr" value="${ item.macAdr }"/>
                        <json:property name="frmtnSeq" value="${ item.frmtnSeq }"/>
                        <json:property name="frmtnNm" value="${ item.frmtnNm }" escapeXml="false"/>
                        <json:property name="cretrId" value="${ item.cretrId }"/>
                        <json:property name="cretDt" value="${ item.cretDt }"/>
                        <json:property name="amdrId" value="${ item.amdrId }"/>
                        <json:property name="amdDt" value="${ item.amdDt }"/>
                        <json:property name="mbrNm" value="${ item.mbrNm }"/>
                        <json:property name="delYn" value="${ item.delYn }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'tagUsageCount' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="tagSeq" value="${ item.tagSeq }" />
                <json:property name="usageCount" value="${ item.usageCount }" />
                <json:property name="useYn" value="${item.useYn}" />
                <json:property name="usageDate" value="${item.timeLimit}" />
                <json:property name="usageMinute" value="${item.shutTime}" />

                <c:if test="${item.shutTime < 0}">
                    <json:property name="usageMinute" value="${0}" />
                </c:if>

                <c:if test="${item.prodLmtCnt == 0}">
                    <json:property name="usageCount" value="9999" />
                </c:if>
                <json:property name="playTime" value="${item.playTime}" />
                <json:array name="usageCategoryCount" items="${useCategoryList}"
                    var="i">
                    <json:object>
                        <json:property name="categoryName" value="${ i.devCtgNm }" />
                        <json:property name="categoryCount" value="${i.lmtCnt}" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'codeNameAPIList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="codeList" items="${item}" var="i">
                    <json:object>
                        <json:property name="codeName" value="${ i.genreNm }"/>
                        <json:property name="codeValue" value="${ i.genreSeq }"/>
                        <json:property name="count" value="${ i.usageCount }"/>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'deviceMacInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="macInfo">
                    <json:property name="macAdr" value="${ item.macAdr }" />
                    <json:property name="devIpadr"
                        value="${ item.devIpadr }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'deviceMacInfo' }">
        <c:if test="${ resultCode eq '1013' }">
            <json:object name="result">
                <json:object name="macInfo">
                    <json:property name="macAdr" value="${ item.macAdr }" />
                    <json:property name="devIpadr"
                        value="${ item.devIpadr }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>



    <c:if test="${ resultType eq 'deviceInfoAPI' or resultType eq 'deviceConnect'}">
        <c:if test="${ resultCode eq '1000' }">
                <json:object name="result">
                        <json:array name="deviceConnectList" items="${item}" var="i">
                            <json:object>
                                <json:property name="devSeq" value="${ i.devSeq }" />
                                <json:property name="devId" value="${ i.devId }" escapeXml="false"/>
                                <json:property name="storSeq" value="${ i.storSeq }" />
                                <json:property name="cretrId" value="${ i.cretrId }" />
                                <json:property name="frmtnCnt" value="${ i.frmtnCnt }" />
                                <json:property name="frmtnNm" value="${ i.frmtnNm}" />
                                <json:property name="devIpadr" value="${ i.devIpadr }" />
                                <json:property name="devUseCpuRate" value="${ i.devUseCpuRate }" />
                                <json:property name="devUsageMem" value="${ i.devUsageMem }" />
                                <json:property name="devStrgeSpace" value="${ i.devStrgeSpace }" />
                                <json:property name="devUseYn" value="${ i.devUseYn }" />
                                <json:property name="frmtnSeq" value="${ i.frmtnSeq }" />
                                <json:property name="devTypeNm" value="${ i.devTypeNm }" />
                                <json:property name="devTypeNmFull" value="${ i.devTypeNmFull }"/>
                                <json:property name="playTime" value="${i.playTime}" />
                                <json:property name="useFnsTime" value="${i.useFnsTime}" />
                                <json:property name="useStTime" value="${i.useStTime}" />
                                <json:property name="contsTitle" value="${i.contsTitle}" />
                                <json:property name="contsDesc" value="${i.contsDesc}" />
                                <json:property name="contsSubTitle" value="${i.contsSubTitle}" />
                                <json:property name="filePath" value="${i.filePath}" />
                                <json:array name="useTagList" items="${i.useTag}" var="j">
                                    <json:object>
                                        <json:property name="tagId" value="${ j.tagId }" />
                                        <json:property name="tagSeq" value="${ j.tagSeq }" />
                                        <json:property name="rmndCnt" value="${ j.rmndCnt }" />
                                        <c:if test="${j.prodLmtCnt == 0}">
                                            <json:property name="rmndCnt" value="9999" />
                                        </c:if>
                                        <json:property name="usageMinute" value="${ j.usageMinute }" />
                                    </json:object>
                                </json:array>
                            </json:object>
                        </json:array>
                </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'deviceUpdate' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="devSeq" value="${ devSeq }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'deviceDownLog' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="deviceDownLog" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="devSeq" value="${ i.devSeq }"/>
                        <json:property name="devId" value="${ i.devId }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'deviceUseDetailInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="deviceUseDetailInfo">
                    <json:property name="devSeq" value="${ item.devSeq }" />
                    <json:property name="devId" value="${ item.devId }" />
                    <json:property name="devUseCpuRate" value="${ item.devUseCpuRate }" />
                    <json:property name="devUsageMem" value="${ item.devUsageMem }" />
                    <json:property name="devStrgeSpace" value="${ item.devStrgeSpace }" />
                    <json:property name="devUseInfoUpdDt" value="${ item.devUseInfoUpdDt }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

</json:object>