<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:set var="count" value="0"></c:set>

    <c:if test="${ resultType eq 'svcNoticeList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="noticeList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value="${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="svcNoticeSeq" value="${ i.svcNoticeSeq }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }" />
                        <json:property name="cretrMbrSe" value="${ i.cretrMbrSe }" />
                        <json:property name="cretrMbrSvcSeq" value="${ i.cretrMbrSvcSeq }" />
                        <json:property name="cretrMbrStorSeq" value="${ i.cretrMbrStorSeq }" />
                        <json:property name="svcNoticeTitle" value="${ i.svcNoticeTitle }" escapeXml="false" />
                        <json:property name="svcNoticeType" value="${ i.svcNoticeType }" />
                        <json:property name="svcNoticeTypeNm" value="${ i.svcNoticeTypeNm }" />
                        <c:set var="fileExist" value="false" />
                        <c:if test="${i.fileCount > 0}">
                            <c:set var="fileExist" value="true" />
                        </c:if>
                        <json:property name="fileExist" value="${fileExist}" />
                        <json:property name="retvNum" value="${ i.retvNum }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="delYn" value="${ i.delYn }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcNoticeDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="noticeDetail">
                    <json:property name="svcNoticeSeq" value="${ item.svcNoticeSeq }" />
                    <json:property name="svcNoticeTitle" value="${ item.svcNoticeTitle }" escapeXml="false" />
                    <json:property name="svcNoticeType" value="${ item.svcNoticeType }" />
                    <json:property name="svcNoticeTypeNm" value="${ item.svcNoticeTypeNm }" />
                    <json:property name="svcNoticeSbst" value="${ item.svcNoticeSbst }" escapeXml="false" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="cretrMbrSe" value="${ item.cretrMbrSe }" />
                    <json:property name="cretrMbrSvcSeq" value="${ item.cretrMbrSvcSeq }" />
                    <json:property name="cretrMbrStorSeq" value="${ item.cretrMbrStorSeq }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                    <json:property name="popupViewYn" value="${ item.popupViewYn }" />
                    <json:property name="stDt" value="${ item.stDt }" />
                    <json:property name="fnsDt" value="${ item.fnsDt }" />
                    <json:property name="delYn" value="${ item.delYn }" />
                </json:object>
                <json:array name="fileList" items="${fileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcNoticeInsert' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:property name="svcNoticeSeq" value="${ svcNoticeSeq }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getNoticeList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="noticeList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value="${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="noticeSeq" value="${ i.svcNoticeSeq }" />
                        <json:property name="noticeTitle" value="${ i.svcNoticeTitle }" escapeXml="false" />
                        <json:property name="noticeType" value="${ i.svcNoticeTypeNm }" />
                        <c:set var="fileExist" value="false" />
                        <c:if test="${i.fileCount > 0}">
                            <c:set var="fileExist" value="true" />
                        </c:if>
                        <json:property name="fileExist" value="${fileExist}" />
                        <json:property name="retvNum" value="${ i.retvNum }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % row == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage + 1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getNoticeDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="noticeDetail">
                    <json:property name="noticeSeq" value="${ item.svcNoticeSeq }" />
                    <json:property name="noticeTitle" value="${ item.svcNoticeTitle }" escapeXml="false" />
                    <json:property name="noticeContent" value="${ item.svcNoticeSbst }" escapeXml="false" />
                    <json:property name="noticeType" value="${ item.svcNoticeTypeNm }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                </json:object>
                <json:array name="fileList" items="${fileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'latestNoticeDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="latestNoticeDetail">
                    <json:property name="noticeSeq" value="${ item.svcNoticeSeq }" />
                    <json:property name="noticeTitle" value="${ item.svcNoticeTitle }" escapeXml="false" />
                    <json:property name="noticeContent" value="${ item.svcNoticeSbst }" escapeXml="false" />
                    <json:property name="noticeType" value="${ item.svcNoticeTypeNm }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                </json:object>
                <json:array name="fileList" items="${fileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

</json:object>