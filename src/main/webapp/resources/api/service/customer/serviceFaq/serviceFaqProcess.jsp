<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:set var="count" value="0"></c:set>

    <c:if test="${ resultType eq 'svcFaqList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="faqList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*row+count) }" />
                        <json:property name="svcFaqSeq" value="${ i.svcFaqSeq }" />
                        <json:property name="svcFaqTitle" value="${ i.svcFaqTitle }" escapeXml="false" />
                        <json:property name="cretrNm" value="${ i.cretrNm }" />
                        <json:property name="cretrMbrSe" value="${ i.cretrMbrSe }" />
                        <json:property name="cretrMbrSvcSeq" value="${ i.cretrMbrSvcSeq }" />
                        <json:property name="cretrMbrStorSeq" value="${ i.cretrMbrStorSeq }" />
                        <json:property name="svcFaqType" value="${ i.svcFaqType }" />
                        <json:property name="svcFaqTypeNm" value="${ i.svcFaqTypeNm }" />
                        <json:property name="svcFaqCtg" value="${ i.svcFaqCtg }" />
                        <json:property name="svcFaqCtgNm" value="${ i.svcFaqCtgNm }" />
                        <json:property name="retvNum" value="${ i.retvNum }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="delYn" value="${ i.delYn }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcFaqDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="faqDetail">
                    <json:property name="svcFaqSeq" value="${ item.svcFaqSeq }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="cretrMbrSe" value="${ item.cretrMbrSe }" />
                    <json:property name="cretrMbrSvcSeq" value="${ item.cretrMbrSvcSeq }" />
                    <json:property name="cretrMbrStorSeq" value="${ item.cretrMbrStorSeq }" />
                    <json:property name="svcFaqTitle" value="${ item.svcFaqTitle }" escapeXml="false" />
                    <json:property name="svcFaqSbst" value="${ item.svcFaqSbst }" escapeXml="false"/>
                    <json:property name="svcFaqType" value="${ item.svcFaqType }" />
                    <json:property name="svcFaqTypeNm" value="${ item.svcFaqTypeNm }" />
                    <json:property name="svcFaqCtg" value="${ item.svcFaqCtg }" />
                    <json:property name="svcFaqCtgNm" value="${ item.svcFaqCtgNm }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                    <json:property name="delYn" value="${ item.delYn }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getFaqList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="faqList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*row+count) }" />
                        <json:property name="faqSeq" value="${ i.svcFaqSeq }" />
                        <json:property name="faqTitle" value="${ i.svcFaqTitle }" escapeXml="false" />
                        <json:property name="faqType" value="${ i.svcFaqTypeNm }" />
                        <json:property name="faqCtg" value="${ i.svcFaqCtgNm }" />
                        <json:property name="retvNum" value="${ i.retvNum }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % row == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getFaqDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="faqDetail">
                    <json:property name="faqSeq" value="${ item.svcFaqSeq }" />
                    <json:property name="faqTitle" value="${ item.svcFaqTitle }" escapeXml="false" />
                    <json:property name="faqContent" value="${ item.svcFaqSbst }" escapeXml="false"/>
                    <json:property name="faqType" value="${ item.svcFaqTypeNm }" />
                    <json:property name="faqCtg" value="${ item.svcFaqCtgNm }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getFaqDetailList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="faqDetailList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*row+count) }" />
                        <json:property name="svcFaqSeq" value="${ i.svcFaqSeq }" />
                        <json:property name="svcFaqQuestion" value="${ i.svcFaqTitle }" escapeXml="false" />
                        <json:property name="svcFaqAnswer" value="${ i.svcFaqSbst }" escapeXml="false" />
                        <json:property name="svcFaqType" value="${ i.svcFaqTypeNm }" />
                        <json:property name="svcFaqCtg" value="${ i.svcFaqCtgNm }" />
                        <json:property name="svcFaqTarget" value="${ i.svcFaqTargetNm }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>
</json:object>