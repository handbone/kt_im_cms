<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${ resultType eq 'keywordList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="keywordList" items="${item}" var="i">
                    <json:object>
                        <json:property name="keywordSeq" value="${ i.keywordSeq }" />
                        <json:property name="svcSeq" value="${ i.svcSeq }" />
                        <json:property name="keyword" value="${ i.keyword }" />
                        <json:property name="type" value="${ i.type }" />
                        <json:property name="rank" value="${ i.rank }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

</json:object>