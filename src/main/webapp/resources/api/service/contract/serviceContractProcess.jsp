<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0" />
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'serviceContractList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="serviceContractList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row + count)}" />
                        <json:property name="ratSeq" value="${ i.ratSeq }"/>
                        <json:property name="ratContNo" value="${ i.ratContNo }" />
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsCount" value="${ i.contsCount }" />
                        <json:property name="ratYn" value="${ i.ratYn }" />
                        <json:property name="ratContType" value="${ i.ratContType }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="totPrice" value="${ i.totPrice }" />
                        <json:property name="usePrice" value="${ i.usePrice }" />
                        <json:property name="useCount" value="${ i.useCount }" />
                        <json:property name="setVal" value="${ i.setVal }" />
                        <json:property name="amdDt" value="${ i.amdDt }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }(${ i.cretrId })"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'serviceContractDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="marketNm" value="${ marketNm }" />
                <json:property name="storNm" value="${ storNm }" />
                <json:property name="svcNm" value="${ svcNm }" />
                <json:object name="serviceContractDetail">
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="storSeq" value="${ item.storSeq }"/>
                    <json:property name="marketNm" value="${ item.marketNm }"/>
                    <json:property name="ratSeq" value="${ item.ratSeq }"/>
                    <json:property name="ratContNo" value="${ item.ratContNo }" />
                    <json:property name="contsTitle" value="${ item.contsTitle }" escapeXml="false"/>
                    <json:property name="ratYn" value="${ item.ratYn }" />
                    <json:property name="ratContType" value="${ item.ratContType }" />
                    <json:property name="ratContStDt" value="${ item.ratContStDt }" />
                    <json:property name="ratContFnsDt" value="${ item.ratContFnsDt }" />
                    <json:property name="runLmtCnt" value="${ item.runLmtCnt }" />
                    <json:property name="runPerPrc" value="${ item.runPerPrc }" />
                    <json:property name="ratTime" value="${ item.ratTime }" />
                    <json:property name="ratPrc" value="${ item.ratPrc }" />
                    <json:property name="lmsmpyPrc" value="${ item.lmsmpyPrc }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <json:property name="amdrId" value="${ item.amdrId }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="amdDt" value="${ item.amdDt }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

</json:object>