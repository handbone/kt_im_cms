<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />

    <c:if test="${ resultType eq 'resveInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="resveInfo">
                    <json:property name="resveSeq" value="${ item.resveSeq }"/>
                    <json:property name="devCtgNm" value="${ item.devCtgNm }"/>
                    <json:property name="cstmrNm" value="${ item.cstmrNm }"/>
                    <json:property name="cstmrTelNo" value="${ item.cstmrTelNo }"/>
                    <json:property name="resveDt" value="${ item.resveDt }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'resveList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="resveList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="resveSeq" value="${ i.resveSeq }"/>
                        <json:property name="resveDt" value="${ i.resveDt }"/>
                        <json:property name="cstmrNm" value="${ i.cstmrNm }"/>
                        <json:property name="cstmrTelNo" value="${ i.cstmrTelNo }"/>
                        <json:property name="devCtgNm" value="${ i.devCtgNm }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>
</json:object>