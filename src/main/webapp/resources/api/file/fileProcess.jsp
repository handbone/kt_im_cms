<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${ resultType eq 'fileUpload' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="fileInfo">
                    <json:property name="orginlFileNm" value="${ item.orginlFileNm }"/>
                    <json:property name="fileSize" value="${ item.fileSize }"/>
                    <json:property name="fileExt" value="${ item.fileExt }"/>
                    <json:property name="fileDir" value="${ item.fileDir }"/>
                    <json:property name="streFileNm" value="${ item.streFileNm }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'filePathInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="fileInfo">
                    <json:property name="fileSeq" value="${ item.fileSeq }"/>
                    <json:property name="filePath" value="${ item.filePath }"/>
                    <json:property name="orginlFileNm" value="${ item.orginlFileNm }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
</json:object>