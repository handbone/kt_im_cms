<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />

    <c:if test="${ resultType eq 'qnaInsert' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:property name="qnaSeq" value="${ qnaSeq }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'qnaList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="qnaList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*row+count) }" />
                        <json:property name="qnaSeq" value="${ i.qnaSeq }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }" />
                        <json:property name="qnaTitle" value="${ i.qnaTitle }" escapeXml="false" />
                        <json:property name="qnaType" value="${ i.qnaType }" />
                        <json:property name="qnaCtg" value="${ i.qnaCtg }" />
                        <c:set var="fileExist" value="false" />
                        <c:if test="${i.fileCount > 0}">
                            <c:set var="fileExist" value="true" />
                        </c:if>
                        <json:property name="fileExist" value="${fileExist}" />
                        <json:property name="retvNum" value="${ i.retvNum }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="cretrId" value="${ i.cretrId }" />
                        <json:property name="ansCretrId" value="${ i.ansCretrId }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'qnaDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="qnaDetail">
                    <json:property name="qnaSeq" value="${ item.qnaSeq }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="qnaTitle" value="${ item.qnaTitle }" escapeXml="false" />
                    <json:property name="qnaSbst" value="${ item.qnaSbst }" escapeXml="false" />
                    <json:property name="regDt" value="${ item.regDt }" />
                    <json:property name="qnaType" value="${ item.qnaType }" />
                    <json:property name="qnaTypeCode" value="${ item.qnaTypeCd }" />
                    <json:property name="qnaCtg" value="${ item.qnaCtg }" />
                    <json:property name="qnaCtgCode" value="${ item.qnaCtgCd }" />
                    <json:property name="ansConts" value="${item.ansConts}" escapeXml="false" />
                    <json:property name="ansCretrId" value="${item.ansCretrId}" />
                    <json:property name="ansAmdrId" value="${item.ansAmdrId}" />
                    <json:property name="ansCretrNm" value="${item.ansCretrNm}" />
                    <json:property name="ansRegDt" value="${item.ansRegDt}" />
                </json:object>
                <json:array name="fileList" items="${fileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
                <json:array name="ansFileList" items="${ansFileList}" var="j">
                    <json:object>
                        <json:property name="fileSeq" value="${ j.fileSeq }" />
                        <json:property name="filePath" value="${ j.filePath }" />
                        <json:property name="fileName" value="${ j.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

</json:object>