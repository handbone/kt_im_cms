<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0" />
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'versionList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="versionList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="verSeq" value="${ i.verSeq }"/>
                        <json:property name="verNm" value="${ i.verNm }" escapeXml="false"/>
                        <json:property name="comnCdNm" value="${ i.comnCdNm }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'versionInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="versionInfo">
                    <json:property name="verSeq" value="${ item.verSeq }"/>
                    <json:property name="verNm" value="${ item.verNm }" escapeXml="false"/>
                    <json:property name="comnCdNm" value="${ item.comnCdNm }"/>
                    <json:property name="verHst" value="${ item.verHst }" escapeXml="false"/>
                    <json:property name="storSeq" value="${ item.storSeq }"/>
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }" escapeXml="false"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:array name="fileList" items="${file}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="orginlFileNm" value="${ i.orginlFileNm }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                        </json:object>
                    </json:array>
                </json:object>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'versionFileInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:object name="versionFileInfo">
                        <json:property name="fileDir" value="${ item.fileDir }"/>
                        <json:property name="orginlFileNm" value="${ item.orginlFileNm }"/>
                        <json:property name="streFileNm" value="${ item.streFileNm }"/>
                        <json:property name="fileSize" value="${ item.fileSize }"/>
                    </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'versionInsert' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:object name="versionInsert">
                        <json:property name="verSeq" value="${ item.verSeq }"/>
                    </json:object>
            </json:object>
        </c:if>
    </c:if>



</json:object>