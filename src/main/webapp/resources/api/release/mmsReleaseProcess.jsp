<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'releaseList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="releaseList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="frmtnSeq" value="${ i.frmtnSeq }"/>
                        <json:property name="frmtnNm" value="${ i.frmtnNm }" escapeXml="false" />
                        <json:property name="storSeq" value="${ i.storSeq }" />
                        <json:property name="storNm" value="${ i.storNm }" escapeXml="false" />
                        <json:property name="amdDt" value="${ i.amdDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'releaseInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="releaseInfo">
                    <json:property name="frmtnSeq" value="${ item.frmtnSeq }"/>
                    <json:property name="frmtnNm" value="${ item.frmtnNm }" escapeXml="false" />
                    <json:property name="storSeq" value="${ item.storSeq }" />
                    <json:property name="svcSeq" value="${ item.svcSeq }" />
                    <json:property name="storNm" value="${ item.storNm }" escapeXml="false" />
                    <json:property name="svcNm" value="${ item.svcNm }" escapeXml="false" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <json:property name="amdrId" value="${ item.amdrId }" />
                    <json:property name="amdDt" value="${ item.amdDt }" />
                    <json:property name="delYn" value="${ item.delYn }" />
                </json:object>
                <json:array name="contsList" items="${contsList}" var="i" >
                    <json:object>
                        <json:property name="contsSeq" value="${ i.contsSeq }" />
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false" />
                        <json:property name="firstCtgNm" value="${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }" />
                        <json:property name="cretDt" value="${ i.cretDt }" />

                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'releaseInsert' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="frmtnSeq" value="${ frmtnSeq }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'releaseRecordList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="releaseRecord">
                    <json:property name="frmtnSeq" value="${ item.frmtnSeq }"/>
                    <json:property name="frmtnNm" value="${ item.frmtnNm }" escapeXml="false" />
                    <json:property name="svcSeq" value="${ item.svcSeq }" />
                    <json:property name="svcNm" value="${ item.svcNm }" escapeXml="false" />
                </json:object>
                <json:array name="recordList" items="${contsList}" var="i" >
                    <json:object>
                        <json:property name="frmtnContsSeq" value="${ i.frmtnContsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false" />
                        <json:property name="frmtnSttus" value="${ i.frmtnSttus }" />
                        <json:property name="cretDt" value="${ i.cretDt }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>
</json:object>