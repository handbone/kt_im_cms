<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />

    <c:if test="${ resultType eq 'dbList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="dbList" items="${item}" var="i">
                    <json:object>
                        <json:property name="dbSeq" value="${ i.dbSeq }" />
                        <json:property name="dbNm" value="${ i.dbNm }" escapeXml="false" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'dbInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="dbInfo">
                    <json:property name="dbSeq" value="${ item.dbSeq }"/>
                    <json:property name="dbIp" value="${ item.dbIp }"/>
                    <json:property name="dbNm" value="${ item.dbNm }" escapeXml="false" />
                    <json:property name="dbTotSize" value="${ item.dbTotSize }"/>
                    <json:property name="cretrId" value="${ item.cretrId }"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="amdrId" value="${ item.amdrId }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'fileInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="fileInfo">
                    <json:property name="size" value="${ item }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
</json:object>