<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />

    <c:if test="${ resultType eq 'productsListAPI_POS' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="productList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${count+1 }" />
                        <json:property name="productSeq" value="${ i.prodSeq }"/>
                        <json:property name="productName" value="${ i.prodNm }"/>
                        <json:property name="productId" value="${ i.gsrProdCode }"/>
                        <json:property name="expiryCount" value="${ i.prodLmtCnt }"/>
                        <json:property name="expiryMinute" value="${ i.prodTimeLmt }"/>
                        <json:property name="expiryPrice" value="${ i.prodPrc }"/>
                        <json:property name="regDate" value="${ i.regDt }"/>
                        <json:property name="officeSeq" value="${ i.storSeq }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'memberInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="memberInfo">
                    <json:property name="seq" value="${ item.mbrSeq }" />
                    <json:property name="memberId" value="${ item.mbrId }" />
                    <json:property name="memberName" value="${ item.mbrNm }" />
                    <json:property name="memberPhone" value="${ item.mbrTelNo }" />
                    <c:set var="tel" value="${fn:split(item.mbrMphonNo,';')}" />
					<c:forEach var="telNum" items="${tel}" varStatus="g">
						<c:if test="${g.count == 1}">
	                            <json:property name="memberMobilePhone" value="${ telNum }" />
                      	</c:if>
                    </c:forEach>
                    <json:property name="memberEmail" value="${ item.mbrEmail }" />
                    <json:property name="memberSec" value="${ item.mbrSeNm }" />
                    <json:property name="memberOffice" value="${ item.storNm }" />
                    <json:property name="storCode" value="${ item.storCode }" />
                    <json:property name="officeSeq" value="${ item.storSeq }" />
                    <json:property name="token" value="${ token }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'productDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="productInfo">
                    <json:property name="seq" value="${ item.prodSeq }"/>
                    <json:property name="productName" value="${ item.prodNm }"/>
                    <json:property name="productId" value="${ item.gsrProdCode }"/>
                    <json:property name="expiryMinute" value="${ item.prodTimeLmt }"/>
                    <json:property name="expiryCount" value="${ item.prodLmtCnt }"/>
                    <json:property name="expiryPrice" value="${ item.prodPrc }"/>
                    <json:property name="officeSeq" value="${ item.storSeq }"/>
                    <json:property name="officeName" value="${ item.storNm }"/>
                </json:object>
                <json:array name="productCategory" items="${CItem}" var="i" >
                    <json:object>
                        <json:property name="categorySeq" value="${ i.prodCtgSeq }"/>
                        <json:property name="categoryName" value="${ i.devCtgNm }"/>
                        <json:property name="categoryCount" value="${ i.lmtCnt }"/>
                        <json:property name="deduction" value="${ i.deduction }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'paymentList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="paymentList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="seq" value="${ i.settlSeq }"/>
                        <json:property name="storCode" value="${ i.storCode }"/>
                        <json:property name="customerGender" value="${ i.cstmrGrupCode}"/>
                        <json:property name="posDate" value="${ i.settlDt }"/>
                        <json:property name="type" value="${ i.settlType }"/>
                        <json:property name="netAmount" value="${ i.totSettlAmt }"/>
                        <json:property name="paymentType" value="${ i.settlMthd }"/>
                        <json:property name="creditCompany" value="${ i.crdtCardCmpny }"/>
                        <json:property name="casherId" value="${ i.selerId }"/>
                        <json:property name="posId" value="${ i.posId }"/>
                        <json:property name="receiptNo" value="${ i.reptNo }"/>
                        <json:property name="canclePosId" value="${ i.canclPosId }"/>
                        <json:property name="cancleReceiptNo" value="${ i.canclReptNo }"/>
                        <c:set var="count" value="${1 }"></c:set>
                        <json:array name="tagList" items="${i.tagInfo}" var="j">
                            <json:object>
                                <json:property name="seq" value="${ sleNo }" />
                                <json:property name="netAmount" value="${ j.settlAmt }" />
                                <json:property name="tagId" value="${ j.tagId }" />
                                <json:property name="productId" value="${ j.gsrProdCode }" />
                                <json:property name="productPrice" value="${ j.prodPrc }" />
                                <json:property name="discount" value="${ j.dscntPrc }" />
                                <c:set var="count" value="${count + 1 }"></c:set>
                            </json:object>
                        </json:array>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'reservateUseInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="seq" value="${ item.tagSeq }"/>
                <json:property name="tagId" value="${item.tagId}"/>
                <json:property name="used" value="${item.useYn}"/>
                <c:if test="${item.useYn eq 'B' or item.useYn eq 'E'}">
                    <json:property name="used" value="${'N'}"/>
                </c:if>
            </json:object>
        </c:if>
    </c:if>


</json:object>