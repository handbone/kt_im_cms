<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />


    <c:if test="${ resultType eq 'contentPlayStats' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentPlayList" items="${item}" var="i">
                    <json:object>
                        <json:property name="name" value="${ i.title }"/>
                        <json:property name="data" value="${ i.data }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentPlayFirstStats' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentPlayList" items="${item}" var="i">
                    <json:object>
                        <json:property name="name" value="${ i.title }"/>
                        <json:property name="data" value="${ i.data }"/>
                        <json:property name="count" value="${ i.count }"/>
                        <json:property name="firstCategoryName" value="${ i.firstCategoryName }"/>
                        <json:property name="secondCategoryName" value="${ i.secondCategoryName }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>




    <c:if test="${ resultType eq 'ticketInfoList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="ticketInfoList" items="${item}" var="i">
                    <json:object>
                        <json:property name="name" value="${ i.title }"/>
                        <json:property name="data" value="${ i.data }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'visitorInfoList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="visitorInfoList" items="${item}" var="i">
                    <json:object>
                        <json:property name="name" value="${ i.title }"/>
                        <json:property name="data" value="${ i.data }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'saleInfoStats' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="saleStats" items="${item}" var="i">
                    <json:object>
                        <json:property name="data" value="${ i.data }"/>
                        <json:property name="netAmount" value="${ i.netAmount }"/>
                        <json:property name="count" value="${ i.count }"/>
                        <json:property name="fbAmount" value="${ i.fbAmount }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>



    <c:if test="${ resultType eq 'visitantStats' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="visitantList" items="${item}" var="i">
                    <json:object>
                        <json:property name="name" value="${ i.memberId }"/>
                        <json:property name="data" value="${ i.data }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>



    <c:if test="${ resultType eq 'downStatsList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="downStatsList" items="${item}" var="i">
                    <json:object>
                        <json:property name="seq" value="${ i.seq }"/>
                        <json:property name="title" value="${ i.title }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'ContentsDownStats' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="ContentsDownStatsLine" items="${item}" var="i">
                    <json:object>
                        <json:property name="name" value="${ i.title }"/>
                        <json:array name="data"  items="${i.data}" var="j">
                        <json:array>
                            <json:property name="regDate" value="${ j.regDate*1000 }"/>
                            <json:property name="downCnt" value="${ j.downCnt }"/>
                        </json:array>
                        </json:array>
                    </json:object>
                </json:array>
                <json:array name="ContentsDownStatsPie" items="${pie}" var="i">
                    <json:array>
                            <json:property name="title" value="${ i.title }"/>
                            <json:property name="percent" value="${ i.data }"/>
                    </json:array>
                </json:array>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'ConnectStats' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="ConnectStats" items="${item}" var="i">
                    <json:object>
                        <json:property name="name" value="${ i.title }"/>
                        <json:array name="data"  items="${i.data}" var="j">
                        <json:array>
                            <json:property name="regDate" value="${ j.regDate*1000 }"/>
                            <json:property name="downCnt" value="${ j.downCnt }"/>
                        </json:array>
                        </json:array>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'CreateStats' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="CreateStats" items="${item}" var="i">
                    <json:object>
                        <json:property name="name" value="${ i.title }"/>
                        <json:array name="data"  items="${i.data}" var="j">
                        <json:array>
                            <%-- %><json:property name="regDate" value="${ j.regDate * 1000 }"/><% --%>
                            <json:property name="regDate" value="${ j.regDate*1000 }"/>
                            <json:property name="downCnt" value="${ j.downCnt }"/>
                        </json:array>
                        </json:array>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'payList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="payList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1) * row +count) }" />
                        <json:property name="seq" value="${ i.seq }"/>
                        <json:property name="tagId" value="${ i.tagId }"/>
                        <json:property name="typeSeq" value="${ i.typeSeq }"/>
                        <json:property name="netAmount" value="${ i.netAmount }"/>
                        <json:property name="discount" value="${ i.discount }"/>
                        <json:property name="productId" value="${ i.productId }"/>
                        <json:property name="productId" value="${ i.productId }"/>
                        <json:property name="productSeqs" value="${ i.productSeqs }"/>
                        <json:property name="productName" value="${ i.productName }"/>
                        <json:property name="paymentType" value="${ i.paymentType }"/>
                        <json:property name="customerGender" value="${ i.customerGender }"/>
                        <json:property name="creditCompany" value="${ i.creditCompany }"/>
                        <json:property name="posDate" value="${ i.posDate }"/>
                        <json:property name="casherId" value="${ i.casherId }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>
    <%--
    <c:if test="${ resultType eq 'payInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="payInfo">
                    <json:property name="netAmount" value="${ item.netAmount }"/>
                    <json:property name="netAmountYesterDay" value="${ item.netAmountYesterDay }"/>
                    <json:property name="netAmountLastWeek" value="${ item.netAmountLastWeek }"/>
                    <json:property name="payCard" value="${ item.payCard }"/>
                    <json:property name="payCardYesterDay" value="${ item.payCardYesterDay }"/>
                    <json:property name="payCardLastWeek" value="${ item.payCardLastWeek }"/>
                    <json:property name="payMoney" value="${ item.payMoney }"/>
                    <json:property name="payMoneyYesterDay" value="${ item.payMoneyYesterDay }"/>
                    <json:property name="payMoneyLastWeek" value="${ item.payMoneyLastWeek }"/>
                    <json:property name="payEtc" value="${ item.payEtc }"/>
                    <json:property name="payEtcYesterDay" value="${ item.payEtcYesterDay }"/>
                    <json:property name="payEtcLastWeek" value="${ item.payEtcLastWeek }"/>
                    <json:property name="payDiscount" value="${ item.payDiscount }"/>
                    <json:property name="payDiscountYesterDay" value="${ item.payDiscountYesterDay }"/>
                    <json:property name="payDiscountLastWeek" value="${ item.payDiscountLastWeek }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    --%>

    <c:if test="${ resultType eq 'todayInfoList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="todayInfoList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="title" value="${ i.title }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'resetvateUseInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="resetvateUseInfo" items="${item}" var="i" >
                    <json:object>
                        <json:property name="productName" value="${ i.productName }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>






    <c:if test="${ resultType eq 'paymentList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="paymentList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="seq" value="${ i.seq }"/>
                        <json:property name="storeCode" value="${ i.storeCode }"/>
                        <json:property name="customerGender" value="${ i.customerGender }"/>
                        <json:property name="posDate" value="${ i.posDate }"/>
                        <json:property name="type" value="${ i.typeSeq }"/>
                        <json:property name="netAmount" value="${ i.netAmount }"/>
                        <json:property name="paymentType" value="${ i.paymentType }"/>
                        <json:property name="creditCompany" value="${ i.creditCompany }"/>
                        <json:property name="casherId" value="${ i.casherId }"/>
                        <json:property name="posId" value="${ i.posId }"/>
                        <json:property name="receiptNo" value="${ i.receiptNo }"/>
                        <json:property name="cancelPosId" value="${ i.cancelPosId }"/>
                        <json:property name="cancelReceiptNo" value="${ i.cancelReceiptNo }"/>
                        <c:set var="count" value="${1 }"></c:set>
                        <json:array name="tagList" items="${i.tagInfo}" var="j">
                            <json:object>
                                <json:property name="seq" value="${ count }" />
                                <json:property name="netAmount" value="${ j.netAmount }" />
                                <json:property name="tagId" value="${ j.tagId }" />
                                <json:property name="productId" value="${ j.productId }" />
                                <json:property name="productPrice" value="${ j.payPrice }" />
                                <json:property name="discount" value="${ j.discount }" />
                                <c:set var="count" value="${count + 1 }"></c:set>
                            </json:object>
                        </json:array>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'smsSendList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="smsSendList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*row+count) }" />
                        <json:property name="seq" value="${ i.seq }"/>
                        <json:property name="phoneNumSend" value="${ i.phoneNumSend }"/>
                        <json:property name="phoneNumReceive" value="${ i.phoneNumReceive }"/>
                        <json:property name="state" value="${ i.state }"/>
                        <json:property name="officeSeq" value="${ i.officeSeq }"/>
                        <json:property name="regDate" value="${ i.regDates }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>

                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'displayWebTrafficOverviewList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="webPvTotalCnt" value="${ webPvTotalCnt }" />
                <json:property name="mobPvTotalCnt" value="${ mobPvTotalCnt }" />
                <json:property name="webUvTotalCnt" value="${ webUvTotalCnt }" />
                <json:property name="mobUvTotalCnt" value="${ mobUvTotalCnt }" />
                <json:array name="displayWebTrafficOverviewList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="connectDt" value="${ i.connectDt }"/>
                        <json:property name="webUvCnt" value="${ i.webUvCnt }"/>
                        <json:property name="webPvCnt" value="${ i.webPvCnt }"/>
                        <json:property name="mobUvCnt" value="${ i.mobUvCnt }"/>
                        <json:property name="mobPvCnt" value="${ i.mobPvCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'usedContentsList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="usedContentsList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="cpNm" value="${i.cpNm}"/>
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="ctgNm" value="${ i.ctgNm }"/>
                        <json:property name="contsTotalPlayCount" value="${ i.contsTotalPlayCount }"/>
                        <json:property name="contsTotalPlayTime" value="${ i.contsTotalPlayTime }"/>
                        <json:property name="contsAvgPlayTime" value="${ i.contsAvgPlayTime }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'launcherUsedApp' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="usedAppList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="svcSeq" value="${i.svcSeq}"/>
                        <json:property name="appSeq" value="${ i.appSeq }"/>
                        <json:property name="pkgNm" value="${ i.pkgNm }"/>
                        <json:property name="totCount" value="${ i.totCount }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'usedContentsDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="usedContentsDetailList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="useDt" value="${ i.useDt }"/>
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="ctgNm" value="${ i.ctgNm }"/>
                        <json:property name="contsTotalPlayCount" value="${ i.contsTotalPlayCount }"/>
                        <json:property name="contsTotalPlayTime" value="${ i.contsTotalPlayTime }"/>
                        <json:property name="contsAvgPlayTime" value="${ i.contsAvgPlayTime }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
                <json:property name="totalTime" value="${ totalTime }" />
                <json:property name="totPlayCount" value="${ totPlayCount }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'trafficList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="trafficList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="useDt" value="${ i.useDt }"/>
                        <json:property name="svcAppTotCount" value="${ i.svcAppTotCount }"/>
                        <json:property name="mirAppTotCount" value="${ i.mirAppTotCount }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'launcherUsedAppDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="usedAppDetailList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="useDt" value="${ i.useDt }"/>
                        <json:property name="totCount" value="${ i.totCount }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'registeredContentsList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="registeredContentsList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="name" value="${ i.name }"/>
                        <json:property name="totalCnt" value="${ i.totalCnt }"/>
                        <json:property name="saveCnt" value="${ i.saveCnt }"/>
                        <json:property name="requestCnt" value="${ i.requestCnt }"/>
                        <json:property name="verifyCnt" value="${ i.verifyCnt }"/>
                        <json:property name="rejectCnt" value="${ i.rejectCnt }"/>
                        <json:property name="completeCnt" value="${ i.completeCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsByCpList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsByCpList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="cpNm" value="${ i.cpNm }"/>
                        <json:property name="totalCnt" value="${ i.totalCnt }"/>
                        <json:property name="verifyCnt" value="${ i.verifyCnt }"/>
                        <json:property name="completeCnt" value="${ i.completeCnt }"/>
                        <json:property name="rejectCnt" value="${ i.rejectCnt }"/>
                        <json:property name="displayCnt" value="${ i.displayCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'verifiedContentsList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="verifiedContentsList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="mbrNm" value="${ i.mbrNm }"/>
                        <json:property name="completeCnt" value="${ i.completeCnt }"/>
                        <json:property name="rejectCnt" value="${ i.rejectCnt }"/>
                        <json:property name="verifyTotalCnt" value="${ i.verifyTotalCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'visitorList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="visitorList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="visitedTotalCnt" value="${ i.visitedTotalCnt }"/>
                        <json:property name="leftCnt" value="${ i.leftCnt }"/>
                        <json:property name="enteringCnt" value="${ i.enteringCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'visitorDetailList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="visitorDetailList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="visitedTotalCnt" value="${ i.visitedTotalCnt }"/>
                        <json:property name="leftCnt" value="${ i.leftCnt }"/>
                        <json:property name="enteringCnt" value="${ i.enteringCnt }"/>
                        <json:property name="useDt" value="${ i.useDt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>





    <c:if test="${ resultType eq 'tagList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="tagList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="prodNm" value="${ i.prodNm }"/>
                        <json:property name="tagCnt" value="${ i.tagCnt }"/>
                        <json:property name="prodSeq" value="${ i.prodSeq }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'tagStoreList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="prodList" items="${prodList}" var="j" >
                    <json:object>
                        <json:property name="prodSeq" value="${ j[0] }"/>
                        <json:property name="prodNm" value="${ j[1] }"/>
                        <json:property name="totCnt" value="${ j[2] }"/>
                    </json:object>
                </json:array>
                <json:array name="dateList" items="${dateList}" var="j" >
                    <json:object>
                        <json:property name="date" value="${ j }"/>
                    </json:object>
                </json:array>
                <json:array name="tagList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="prodNm" value="${ i.prodNm }"/>
                        <json:property name="tagCnt" value="${ i.putCnt }"/>
                        <json:property name="prodSeq" value="${ i.prodSeq }"/>
                        <json:property name="useDt" value="${ i.date }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'tagProdList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="prodList" items="${prodList}" var="j" >
                    <json:object>
                        <json:property name="prodSeq" value="${ j.prodSeq }"/>
                        <json:property name="prodNm" value="${ j.prodNm }"/>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'tagDetailList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="svcNm" value="${ svcNm }" />
                <json:property name="storNm" value="${ storNm }" />
                <json:property name="prodNm" value="${ prodNm }" />
                <json:array name="tagDetailList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * 10 + count) }" />
                        <json:property name="rowNum" value="${ i.rowNum }"/>
                        <json:property name="tagSeq" value="${ i.tagSeq }"/>
                        <json:property name="tagId" value="${ i.tagId }"/>
                        <json:property name="useSttus" value="${ i.useSttus }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'tagHistoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="tagHistoryList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="devId" value="${ i.devId }"/>
                        <json:property name="stTime" value="${ i.stTime }"/>
                        <json:property name="fnsTime" value="${ i.fnsTime }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsByTagList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsByTagList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="prodNm" value="${ i.prodNm }"/>
                        <json:property name="usedContsCnt" value="${ i.usedContsCnt }"/>
                        <json:property name="prodSeq" value="${ i.prodSeq }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsDetailByTagList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsDetailByTagList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="prodNm" value="${ i.prodNm }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="contsPlayCnt" value="${ i.contsPlayCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'settlementManageList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="settlementManageList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="settlDt" value="${ i.settlDt }"/>
                        <json:property name="totalAmount" value="${ i.totalAmount }"/>
                        <json:property name="cardAmount" value="${ i.cardAmount }"/>
                        <json:property name="cashAmount" value="${ i.cashAmount }"/>
                        <json:property name="etcAmount" value="${ i.etcAmount }"/>
                        <json:property name="dscntAmount" value="${ i.dscntAmount }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'usedOlsvcContentsList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="usedOlsvcContentsList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="cpNm" value="${ i.cpNm }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="contsSubTitle" value="${ i.contsSubTitle }"/>
                        <json:property name="totClickCnt" value="${ i.contsClickCnt }"/>
                        <json:property name="totStmCnt" value="${ i.contsStmCnt }"/>
                        <json:property name="totStmTime" value="${ i.contsPlayTime }"/>
                        <json:property name="totDlCnt" value="${ i.contsDlCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'olSvcLauncherUsedContsList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="usedContsList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="cpNm" value="${ i.cpNm }"/>
                        <json:property name="appType" value="${ i.appType }"/>
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsId" value="${ i.contsId }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="ctgNm" value="${ i.firstCtgNm } > ${i.genreNm }"/>
                        <json:property name="playCnt" value="${ i.contsPlayCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'olSvcLauncherUsedContsDetailList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="usedContsDetailList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="useDt" value="${ i.useDt }"/>
                        <json:property name="playCnt" value="${ i.playCnt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>
</json:object>