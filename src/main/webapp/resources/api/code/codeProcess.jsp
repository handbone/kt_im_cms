<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
  
    <c:if test="${ resultType eq 'codeList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="codeList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="comnCdSeq" value="${ i.comnCdSeq }"/>
                        <json:property name="comnCdCtg" value="${ i.comnCdCtg }"/>
                        <json:property name="comnCdNm" value="${ i.comnCdNm }" escapeXml="false" />
                        <json:property name="comnCdValue" value="${ i.comnCdValue }"/>
                        <c:if test="${not empty i.appType }">
                            <json:property name="appType" value="${ i.appType }"/>
                        </c:if>
                        <json:property name="delYn" value="${ i.delYn }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'codeInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="codeInfo">
                    <json:property name="comnCdSeq" value="${ item.comnCdSeq }"/>
                    <json:property name="comnCdCtg" value="${ item.comnCdCtg }"/>
                    <json:property name="comnCdNm" value="${ item.comnCdNm }" escapeXml="false" />
                    <json:property name="comnCdValue" value="${ item.comnCdValue }"/>
                    <json:property name="delYn" value="${ item.delYn }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
</json:object>