<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0" />
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'svcAppVersionInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="svcAppVersionInfo">
                    <json:property name="svcAppVerSeq" value="${ item.svcAppVerSeq }"/>
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }"/> escapeXml="false"/>
                    <json:property name="appType" value="${ item.appType }"/>
                    <json:property name="svcAppVer" value="${ item.svcAppVer }" escapeXml="false"/>
                    <json:property name="dlStoreType" value="${ item.dlStoreType }" escapeXml="false"/>
                    <json:property name="dlStorePath" value="${ item.dlStorePath }" escapeXml="false"/>
                    <json:property name="uptSbst" value="${ item.uptSbst }" escapeXml="false"/>
                    <json:property name="useYn" value="${ item.useYn }"/>
                    <json:property name="regrId" value="${ item.regrId }"/>
                    <c:if test="${not empty item.mbrNm }">
                        <json:property name="regrNm" value="${ item.mbrNm }"/>
                    </c:if>
                    <json:property name="regDt" value="${ item.regDt }"/>
                    <c:if test="${not empty item.amdrId }">
                        <json:property name="amdrId" value="${ item.amdDt }"/>
                    </c:if><c:if test="${not empty item.amdDt }">
                        <json:property name="amdDt" value="${ item.amdDt }"/>
                    </c:if>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcAppVersionList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="svcAppVersionList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="svcAppVerSeq" value="${ i.svcAppVerSeq }"/>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }"/> escapeXml="false"/>
                        <json:property name="svcAppVer" value="${ i.svcAppVer }" escapeXml="false"/>
                        <json:property name="appType" value="${ i.appType }"/>
                        <json:property name="dlStoreType" value="${ i.dlStoreType }"/>
                        <json:property name="dlStorePath" value="${ i.dlStorePath }" escapeXml="false"/>
                        <json:property name="regrId" value="${ i.regrId }"/>
                        <json:property name="regrNm" value="${ i.mbrNm }"/>
                        <json:property name="regDt" value="${ i.regDt }"/>
                        <json:property name="useYn" value="${ i.useYn }"/>
                        <json:property name="svcType" value="${ i.svcType }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'versionInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="versionInfo">
                    <json:property name="svcAppVerSeq" value="${ item.svcAppVerSeq }"/>
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }"/> escapeXml="false"/>
                    <json:property name="appType" value="${ item.appType }"/>
                    <json:property name="svcAppVer" value="${ item.svcAppVer }" escapeXml="false"/>
                    <json:property name="dlStoreType" value="${ item.dlStoreType }" escapeXml="false"/>
                    <json:property name="dlStorePath" value="${ item.dlStorePath }" escapeXml="false"/>
                    <json:property name="uptSbst" value="${ item.uptSbst }" escapeXml="false"/>
                    <json:property name="useYn" value="${ item.useYn }"/>
                    <json:property name="regrId" value="${ item.regrId }"/>
                    <c:if test="${not empty item.mbrNm }">
                        <json:property name="regrNm" value="${ item.mbrNm }"/>
                    </c:if>
                    <json:property name="regDt" value="${ item.regDt }"/>
                    <c:if test="${not empty item.amdrId }">
                        <json:property name="amdrId" value="${ item.amdDt }"/>
                    </c:if>
                    <c:if test="${not empty item.amdDt }">
                        <json:property name="amdDt" value="${ item.amdDt }"/>
                    </c:if>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

</json:object>