<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:set var="count" value="0"></c:set>

    <c:if test="${ resultType eq 'insertSvcApp' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:property name="appSeq" value="${ appSeq }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcAppList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="appList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * row + count) }" />
                        <json:property name="appSeq" value="${ i.appSeq }" />
                        <json:property name="svcSeq" value="${ i.svcSeq }" />
                        <json:property name="svcNm" value="${ i.svcNm }" />
                        <json:property name="orderNum" value="${ i.orderNum }" />
                        <json:property name="type" value="${ i.type }" />
                        <json:property name="appNm" value="${ i.appNm }" />
                        <json:property name="contsTitle" value="${ i.contsTitle }" />
                        <json:property name="pkgNm" value="${ i.pkgNm }" />
                        <json:property name="appImgPath" value="${ i.appImgPath }" />
                        <json:property name="storUrl" value="${ i.storUrl }" escapeXml="false" />
                        <json:property name="uri" value="${ i.uri }" escapeXml="false" />
                        <json:property name="isFile" value="${ i.isFile }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcAppInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="appInfo">
                    <json:property name="appSeq" value="${ item.appSeq }" />
                    <json:property name="svcSeq" value="${ item.svcSeq }" />
                    <json:property name="svcNm" value="${ item.svcNm }" />
                    <json:property name="type" value="${ item.type }" />
                    <json:property name="appNm" value="${ item.appNm }" />
                    <json:property name="contsTitle" value="${ item.contsTitle }" />
                    <json:property name="pkgNm" value="${ item.pkgNm }" />
                    <json:property name="storUrl" value="${ item.storUrl }" escapeXml="false" />
                    <json:property name="uri" value="${ item.uri }" escapeXml="false" />
                </json:object>
                <json:array name="appImgFileList" items="${appImgFileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getSvcAppList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="appList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${ totalCount - count }" />
                        <json:property name="appSeq" value="${ i.appSeq }" />
                        <json:property name="svcSeq" value="${ i.svcSeq }" />
                        <json:property name="svcNm" value="${ i.svcNm }" />
                        <json:property name="orderNum" value="${ i.orderNum }" />
                        <json:property name="type" value="${ i.type }" />
                        <json:property name="appNm" value="${ i.appNm }" />
                        <json:property name="contsTitle" value="${ i.contsTitle }" />
                        <json:property name="pkgNm" value="${ i.pkgNm }" />
                        <json:property name="appImgPath" value="${ i.appImgPath }" />
                        <json:property name="storUrl" value="${ i.storUrl }" escapeXml="false" />
                        <json:property name="uri" value="${ i.uri }" escapeXml="false" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
            </json:object>
        </c:if>
    </c:if>
</json:object>