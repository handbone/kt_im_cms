<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:set var="count" value="0"></c:set>

    <c:if test="${ resultType eq 'latestSvcTermsInfoList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="termsInfoList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="stpltSeq" value="${ i.stpltSeq }"/>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="stpltType" value="${ i.stpltType }"/>
                        <json:property name="stpltTypeNm" value="${ i.stpltTypeNm }"/>
                        <json:property name="stpltSbst" value="${ i.stpltSbst }" escapeXml="false"/>
                        <json:property name="stpltVer" value="${ i.stpltVer}"/>
                        <json:property name="mandYn" value="${ i.mandYn}"/>
                        <json:property name="regDt" value="${ i.regDt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcTermsInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="svcTermsInfo">
                    <json:property name="stpltSeq" value="${ item.stpltSeq }"/>
                    <json:property name="svcNm" value="${ item.svcNm }"/>
                    <json:property name="svcSeq" value="${ item.svcSeq }"/>
                    <json:property name="stpltTypeNm" value="${ item.stpltTypeNm }"/>
                    <json:property name="stpltType" value="${ item.stpltType }"/>
                    <json:property name="stpltSbst" value="${ item.stpltSbst }" escapeXml="false"/>
                    <json:property name="stpltVer" value="${ item.stpltVer }"/>
                    <json:property name="cretrNm" value="${ item.cretrNm }"/>
                    <json:property name="useYn" value="${ item.useYn }"/>
                    <json:property name="mandYn" value="${ item.mandYn }"/>
                    <json:property name="stDt" value="${ item.stDt }"/>
                    <json:property name="fnsDt" value="${ item.fnsDt }"/>
                    <json:property name="perdYn" value="${ item.perdYn }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcTermsInfoList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="svcTermsInfoList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="stpltSeq" value="${ i.stpltSeq }"/>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="stpltVer" value="${ i.stpltVer }"/>
                        <json:property name="stpltType" value="${ i.stpltType }"/>
                        <json:property name="cretrNm" value="${ i.cretrNm }"/>
                        <json:property name="mandYn" value="${ i.mandYn }"/>
                        <json:property name="useYn" value="${ i.useYn}"/>
                        <json:property name="perdYn" value="${ i.perdYn }"/>
                        <json:property name="stDt" value="${ i.stDt }"/>
                        <json:property name="fnsDt" value="${ i.fnsDt }"/>
                        <json:property name="regDt" value="${ i.regDt }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'insertSvcTerms' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="stpltSeq" value="${ stpltSeq }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'svcTermsExistInfoList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="svcTermsExistInfoList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="stpltSeq" value="${ i.stpltSeq }"/>
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="stpltVer" value="${ i.stpltVer }"/>
                        <json:property name="useYn" value="${ i.useYn}"/>
                        <json:property name="perdYn" value="${ i.perdYn}"/>
                        <json:property name="stDt" value="${ i.stDt}"/>
                        <json:property name="fnsDt" value="${ i.fnsDt }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>
</json:object>