<%--
   IM Platform version 1.0

   Copyright �� 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:set var="count" value="0"></c:set>
    <c:set var="imptCount" value="0"></c:set>
    <c:set var="imptTxt"><spring:message code="notice.important"/></c:set>
    <c:set var="appTypeAll"><spring:message code="common.app.type.all"/></c:set>
    <c:set var="appTypeSA"><spring:message code="common.app.type.service"/></c:set>
    <c:set var="appTypeMA"><spring:message code="common.app.type.mirror"/></c:set>

    <c:if test="${ resultType eq 'olSvcNoticeList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="noticeList" items="${item}" var="i">
                    <json:object>
                        <c:choose>
                            <c:when test="${i.prefRank ne '0'}">
                                <json:property name="num" value="" />
                            </c:when>
                            <c:otherwise>
                                <json:property name="num" value="${noticeTotCnt - ((currentPage-1) * row + count)}" />
                            </c:otherwise>
                        </c:choose>
                        <json:property name="prefRank" value="${ i.prefRank }" />
                        <json:property name="svcNoticeSeq" value="${ i.svcNoticeSeq }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }" />
                        <json:property name="cretrMbrSe" value="${ i.cretrMbrSe }" />
                        <c:choose>
                            <c:when test="${i.prefRank ne '0'}">
                                <json:property name="svcNoticeTitle" value="[${ imptTxt }] ${ i.svcNoticeTitle }" escapeXml="false" />
                            </c:when>
                            <c:otherwise>
                                <json:property name="svcNoticeTitle" value="${ i.svcNoticeTitle }" escapeXml="false" />
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${ i.dispType eq 'ALL'}">
                                <json:property name="dispType" value="${ appTypeAll }" />
                            </c:when>
                            <c:when test="${ i.dispType eq 'SA'}">
                                <json:property name="dispType" value="${ appTypeSA }" />
                            </c:when>
                            <c:when test="${ i.dispType eq 'MA'}">
                                <json:property name="dispType" value="${ appTypeMA }" />
                            </c:when>
                            <c:otherwise>
                                <json:property name="dispType" value="" />
                            </c:otherwise>
                        </c:choose>
                        <json:property name="retvNum" value="${ i.retvNum }" />
                        <c:set var="fileExist" value="false" />
                        <c:if test="${i.fileCount > 0}">
                            <c:set var="fileExist" value="true" />
                        </c:if>
                        <json:property name="fileExist" value="${fileExist}" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <json:property name="delYn" value="${ i.delYn }" />
                        <c:choose>
                            <c:when test="${i.prefRank ne '0'}">
                                <c:set var="imptCount" value="${imptCount + 1 }"></c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="count" value="${count + 1 }"></c:set>
                            </c:otherwise>
                        </c:choose>
                    </json:object>
                </json:array>
                <json:property name="imptNoticeTotCnt" value="${ imptNoticeTotCnt }" />
                <json:property name="normalNoticeTotCnt" value="${ noticeTotCnt }" />
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'olSvcNoticeDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="noticeDetail">
                    <json:property name="noticeSeq" value="${ item.svcNoticeSeq }" />
                    <json:property name="svcSeq" value="${ item.svcNoticeType }" />
                    <json:property name="svcNm" value="${ item.svcNoticeTypeNm }" />
                    <json:property name="dispType" value="${ item.dispType }" />
                    <c:choose>
                        <c:when test="${ item.dispType eq 'ALL'}">
                            <json:property name="dispTypeNm" value="${ appTypeAll }" />
                        </c:when>
                        <c:when test="${ item.dispType eq 'SA'}">
                            <json:property name="dispTypeNm" value="${ appTypeSA }" />
                        </c:when>
                        <c:when test="${ item.dispType eq 'MA'}">
                            <json:property name="dispTypeNm" value="${ appTypeMA }" />
                        </c:when>
                        <c:otherwise>
                            <json:property name="dispType" value="" />
                        </c:otherwise>
                    </c:choose>
                    <json:property name="targetType" value="${ item.targetType }" />
                    <json:property name="targetTypeNm" value="${ item.targetTypeNm }" />
                    <json:property name="prefRank" value="${ item.prefRank }" />
                    <json:property name="orignlNoticeTitle" value="${ item.svcNoticeTitle }" />
                    <c:choose>
                        <c:when test="${item.prefRank ne '0'}">
                            <json:property name="noticeTitle" value="[${ imptTxt }] ${ item.svcNoticeTitle }" escapeXml="false" />
                        </c:when>
                        <c:otherwise>
                            <json:property name="noticeTitle" value="${ item.svcNoticeTitle }" escapeXml="false" />
                        </c:otherwise>
                    </c:choose>
                    <json:property name="noticeSbst" value="${ item.svcNoticeSbst }" escapeXml="false" />
                    <json:property name="retvNum" value="${ item.retvNum }" />
                    <json:property name="postViewYn" value="${ item.postViewYn }" />
                    <json:property name="popupViewYn" value="${ item.popupViewYn }" />
                    <json:property name="popupUrl" value="${ item.popupUrl }" escapeXml="false" />
                    <json:property name="popupPerdYn" value="${ item.popupPerdYn }" />
                    <json:property name="popupViewStDt" value="${ item.popupViewStDt }" />
                    <json:property name="popupViewFnsDt" value="${ item.popupViewFnsDt }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="cretrMbrSe" value="${ item.cretrMbrSe }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                    <json:property name="delYn" value="${ item.delYn }" />
                </json:object>
                <json:array name="popupImgFileList" items="${popupImgFileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
                <json:array name="attachFileList" items="${attachFileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'insertOlSvcNotice' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                    <json:property name="svcNoticeSeq" value="${ svcNoticeSeq }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getOlSvcNoticeList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="noticeList" items="${item}" var="i">
                    <json:object>
                        <c:choose>
                            <c:when test="${i.prefRank ne '0'}">
                                <json:property name="num" value="" />
                            </c:when>
                            <c:otherwise>
                                <json:property name="num" value="${noticeTotCnt - ((currentPage-1) * row + count)}" />
                            </c:otherwise>
                        </c:choose>
                        <json:property name="noticeSeq" value="${ i.svcNoticeSeq }" />
                        <c:choose>
                            <c:when test="${i.prefRank ne '0'}">
                                <json:property name="noticeTitle" value="[${ imptTxt }] ${ i.svcNoticeTitle }" escapeXml="false" />
                            </c:when>
                            <c:otherwise>
                                <json:property name="noticeTitle" value="${ i.svcNoticeTitle }" escapeXml="false" />
                            </c:otherwise>
                        </c:choose>
                        <json:property name="noticeSbst" value="${ i.svcNoticeSbst }" escapeXml="false" />
                        <json:property name="svcNm" value="${ i.svcNoticeTypeNm }" />
                        <json:property name="prefRank" value="${ i.prefRank }" />
                        <c:choose>
                            <c:when test="${ i.dispType eq 'ALL'}">
                                <json:property name="dispType" value="${ appTypeAll }" />
                            </c:when>
                            <c:when test="${ i.dispType eq 'SA'}">
                                <json:property name="dispType" value="${ appTypeSA }" />
                            </c:when>
                            <c:when test="${ i.dispType eq 'MA'}">
                                <json:property name="dispType" value="${ appTypeMA }" />
                            </c:when>
                            <c:otherwise>
                                <json:property name="dispType" value="" />
                            </c:otherwise>
                        </c:choose>
                        <json:property name="targetType" value="${ i.targetType }" />
                        <json:property name="postViewYn" value="${ i.postViewYn }" />
                        <json:property name="popupViewYn" value="${ i.popupViewYn }" />
                        <json:property name="popupUrl" value="${ i.popupUrl }" escapeXml="false" />
                        <json:property name="popupPerdYn" value="${ i.popupPerdYn }" />
                        <json:property name="popupViewStDt" value="${ i.popupViewStDt }" />
                        <json:property name="popupViewFnsDt" value="${ i.popupViewFnsDt }" />
                        <json:property name="popupImgFilePath" value=" ${ i.filePath }" />
                        <json:property name="retvNum" value="${ i.retvNum }" />
                        <json:property name="cretrNm" value="${ i.cretrNm }" />
                        <json:property name="regDt" value="${ i.regDt }" />
                        <c:set var="attachFileExist" value="false" />
                        <c:if test="${i.fileCount > 0}">
                            <c:set var="attachFileExist" value="true" />
                        </c:if>
                        <c:choose>
                            <c:when test="${i.prefRank ne '0'}">
                                <c:set var="imptCount" value="${imptCount + 1 }"></c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="count" value="${count + 1 }"></c:set>
                            </c:otherwise>
                        </c:choose>
                    </json:object>
                </json:array>
                <json:property name="imptNoticeTotCnt" value="${ imptNoticeTotCnt }" />
                <json:property name="normalNoticeTotCnt" value="${ noticeTotCnt }" />
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getOlSvcNoticeDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="noticeDetail">
                    <json:property name="noticeSeq" value="${ item.svcNoticeSeq }" />
                    <json:property name="svcNm" value="${ item.svcNoticeTypeNm }" />
                    <c:choose>
                        <c:when test="${ item.dispType eq 'ALL'}">
                            <json:property name="dispType" value="${ appTypeAll }" />
                        </c:when>
                        <c:when test="${ item.dispType eq 'SA'}">
                            <json:property name="dispType" value="${ appTypeSA }" />
                        </c:when>
                        <c:when test="${ item.dispType eq 'MA'}">
                            <json:property name="dispType" value="${ appTypeMA }" />
                        </c:when>
                        <c:otherwise>
                            <json:property name="dispType" value="" />
                        </c:otherwise>
                    </c:choose>
                    <json:property name="targetType" value="${ item.targetTypeNm }" />
                    <json:property name="prefRank" value="${ item.prefRank }" />
                    <c:choose>
                        <c:when test="${i.prefRank ne '0'}">
                            <json:property name="noticeTitle" value="[${ imptTxt }] ${ item.svcNoticeTitle }" escapeXml="false" />
                        </c:when>
                        <c:otherwise>
                            <json:property name="noticeTitle" value="${ item.svcNoticeTitle }" escapeXml="false" />
                        </c:otherwise>
                    </c:choose>
                    <json:property name="svcNoticeSbst" value="${ item.svcNoticeSbst }" escapeXml="false" />
                    <json:property name="retvNum" value="${ item.retvNum }" />
                    <json:property name="postViewYn" value="${ item.postViewYn }" />
                    <json:property name="popupViewYn" value="${ item.popupViewYn }" />
                    <json:property name="popupUrl" value="${ item.popupUrl }" escapeXml="false" />
                    <json:property name="popupPerdYn" value="${ item.popupPerdYn }" />
                    <json:property name="popupViewStDt" value="${ item.popupViewStDt }" />
                    <json:property name="popupViewFnsDt" value="${ item.popupViewFnsDt }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                </json:object>
                <json:array name="popupImgFileList" items="${popupImgFileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
                <json:array name="attachFileList" items="${attachFileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'getLatestOlSvcNoticeDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="latestNoticeDetail">
                    <json:property name="noticeSeq" value="${ item.svcNoticeSeq }" />
                    <json:property name="svcNm" value="${ item.svcNoticeTypeNm }" />
                    <c:choose>
                        <c:when test="${ item.dispType eq 'ALL'}">
                            <json:property name="dispType" value="${ appTypeAll }" />
                        </c:when>
                        <c:when test="${ item.dispType eq 'SA'}">
                            <json:property name="dispType" value="${ appTypeSA }" />
                        </c:when>
                        <c:when test="${ item.dispType eq 'MA'}">
                            <json:property name="dispType" value="${ appTypeMA }" />
                        </c:when>
                        <c:otherwise>
                            <json:property name="dispType" value="" />
                        </c:otherwise>
                    </c:choose>
                    <json:property name="targetType" value="${ item.targetTypeNm }" />
                    <json:property name="prefRank" value="${ item.prefRank }" />
                    <c:choose>
                        <c:when test="${i.prefRank ne '0'}">
                            <json:property name="noticeTitle" value="[${ imptTxt }] ${ item.svcNoticeTitle }" escapeXml="false" />
                        </c:when>
                        <c:otherwise>
                            <json:property name="noticeTitle" value="${ item.svcNoticeTitle }" escapeXml="false" />
                        </c:otherwise>
                    </c:choose>
                    <json:property name="svcNoticeSbst" value="${ item.svcNoticeSbst }" escapeXml="false" />
                    <json:property name="retvNum" value="${ item.retvNum }" />
                    <json:property name="postViewYn" value="${ item.postViewYn }" />
                    <json:property name="popupViewYn" value="${ item.popupViewYn }" />
                    <json:property name="popupUrl" value="${ item.popupUrl }" escapeXml="false" />
                    <json:property name="popupPerdYn" value="${ item.popupPerdYn }" />
                    <json:property name="popupViewStDt" value="${ item.popupViewStDt }" />
                    <json:property name="popupViewFnsDt" value="${ item.popupViewFnsDt }" />
                    <json:property name="cretrNm" value="${ item.cretrNm }" />
                    <json:property name="regDt" value="${ item.regDt }" />
                </json:object>
                <json:array name="popupImgFileList" items="${popupImgFileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
                <json:array name="attachFileList" items="${attachFileList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }" />
                        <json:property name="filePath" value="${ i.filePath }" />
                        <json:property name="fileName" value="${ i.orginlFileNm }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

</json:object>