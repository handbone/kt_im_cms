<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />

    <c:if test="${ resultType eq 'genreList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="genreList" items="${item}" var="i">
                    <json:object>
                        <json:property name="genreSeq" value="${ i.genreSeq }" />
                        <json:property name="genreNm" value="${ i.genreNm }" escapeXml="false" />
                        <json:property name="genreId" value="${ i.genreId }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'genreInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="genreInfo">
                    <json:property name="genreSeq" value="${ item.genreSeq }"/>
                    <json:property name="firstCtgId" value="${ item.firstCtgId }"/>
                    <json:property name="genreNm" value="${ item.genreNm }" escapeXml="false"/>
                    <json:property name="genreId" value="${ item.genreId }"/>
                    <json:property name="cretrId" value="${ item.cretrId }"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="amdrId" value="${ item.amdrId }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'contentsGenreList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsGenreList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="genreSeq" value="${ i.genreSeq }"/>
                        <json:property name="genreNm" value="${ i.genreNm }" escapeXml="false"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'genreOrderList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="genreOrderList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="dispOrderSeq" value="${ i.dispOrderSeq }"/>
                        <json:property name="firstCtgId" value="${ i.firstCtgId }"/>
                        <json:property name="firstCtgNm" value="${ i.firstCtgNm }" escapeXml="false"/>
                        <json:property name="genreSeq" value="${ i.genreSeq }"/>
                        <json:property name="genreNm" value="${ i.genreNm }" escapeXml="false"/>
                        <json:property name="orderNum" value="${ i.orderNum }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>
</json:object>