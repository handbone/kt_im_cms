<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
        <c:set var="count" value="0"></c:set>
        <json:property name="resultCode" value="${ resultCode }" />
        <json:property name="resultMsg" value="${ resultMsg }" />
        <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'contentsInsert' }">
            <c:if test="${ resultCode eq '1000' }">
                    <json:object name="result">
                                    <json:property name="contsSeq" value="${ contsSeq }"/>
                    </json:object>
            </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="filePath" value="${ i.filePath }"/>
                        <json:property name="cretrID" value="${ i.mbrID }"/>
                        <json:property name="cpNm" value="${ i.cpNm }"/>
                        <json:property name="cretrNm" value="${ i.mbrNm }(${ i.mbrID })"/>
                        <json:property name="contCtgNm" value="${ i.contCtgNm}"/>
                        <json:property name="cntrctFnsDt" value="${ i.cntrctFnsDt }"/>
                        <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                        <json:property name="sttus" value="${ i.sttusNm }"/>
                        <json:property name="sttusVal" value="${ i.sttusVal }"/>
                        <json:property name="contsID" value="${ i.contsID }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <json:property name="amdDt" value="${ i.amdDt }"/>
                        <json:property name="vrfRqtDt" value="${ i.vrfRqtDt }"/>
                        <c:if test="${ i.genreNm != null and i.genreNm != ''}">
                                <json:property name="genreNm" value="${ i.genreNm }"/>
                        </c:if>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="contentsInfo">
                    <json:property name="contsSeq" value="${ item.contsSeq }"/>
                    <json:property name="contsTitle" value="${ item.contsTitle }" escapeXml="false"/>
                    <json:property name="contsSubTitle" value="${ item.contsSubTitle }" escapeXml="false"/>
                    <json:property name="contsDesc" value="${ item.contsDesc }" escapeXml="false"/>
                    <json:property name="contCtgNm" value="${ item.contCtgNm }"/>
                    <json:property name="contsID" value="${ item.contsID }"/>
                    <json:property name="firstCtgNm" value="${ item.firstCtgNm }"/>
                    <json:property name="secondCtgNm" value="${ item.secondCtgNm }"/>
                    <json:property name="rcessWhySbst" value="${ item.rcessWhySbst }"/>
                    <json:property name="cretrID" value="${ item.cretrId }"/>
                    <json:property name="fileType" value="${ item.fileType }"/>
                    <json:property name="maxAvlNop" value="${ item.maxAvlNop }"/>
                    <json:property name="exeFilePath" value="${ item.exeFilePath }"/>
                    <json:property name="mbrNm" value="${ item.mbrNm }"/>
                    <json:property name="contsVer" value="${ item.contsVer }"/>
                    <json:property name="contsCtgSeq" value="${ item.contsCtgSeq }"/>
                    <json:property name="sttus" value="${ item.sttusNm }"/>
                    <json:property name="sttusVal" value="${ item.sttusVal }"/>
                    <json:property name="cp" value="${ item.cpNm }"/>
                    <json:property name="cntrctStDt" value="${ item.cntrctStDt }"/>
                    <json:property name="cntrctFnsDt" value="${ item.cntrctFnsDt }"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                    <json:property name="amdrID" value="${ item.amdrID }"/>
                    <json:property name="contsSubMetadataSeq" value="${ item.contsSubMetadataSeq }"/>
                    <json:property name="metaData" value="${ item.metaData }"/>
                    <json:object name="coverImg">
                        <json:property name="fileSeq" value="${ coverImg.fileSeq }"/>
                        <json:property name="coverImgNm" value="${ coverImg.orginlFileNm }"/>
                        <json:property name="coverImgPath" value="${ coverImg.filePath }"/>
                        <json:property name="coverImgSize" value="${ coverImg.fileSize }"/>
                    </json:object>
                 <json:array name="thumbnailList" items="${thumbnailList}" var="i">
                     <json:object>
                         <json:property name="fileSeq" value="${ i.fileSeq }"/>
                         <json:property name="thumbnailNm" value="${ i.orginlFileNm }"/>
                         <json:property name="thumbnailPath" value="${ i.filePath }"/>
                         <json:property name="thumbnailSize" value="${ i.fileSize }"/>
                     </json:object>
                 </json:array>
                    <c:if test="${ item.fileType eq 'VIDEO'}">
                  <json:array name="videoList" items="${videoList}" var="i">
                      <json:object>
                          <json:property name="fileSeq" value="${ i.fileSeq }"/>
                          <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                          <json:property name="filePath" value="${ i.filePath }"/>
                          <json:property name="fileSize" value="${ i.fileSize }"/>
                          <json:object name="metadataInfo">
                              <json:property name="fileSeq" value="${ i.metadataXmlInfo.fileSeq}"/>
                              <json:property name="fileNm" value="${ i.metadataXmlInfo.orginlFileNm }"/>
                              <json:property name="filePath" value="${ i.metadataXmlInfo.filePath }"/>
                          </json:object>
                          <json:object name="contsXmlInfo">
                              <json:property name="fileSeq" value="${ i.contsXmlInfo.fileSeq }"/>
                              <json:property name="fileNm" value="${ i.contsXmlInfo.orginlFileNm }"/>
                              <json:property name="filePath" value="${ i.contsXmlInfo.filePath }"/>
                          </json:object>
                      </json:object>
                  </json:array>
                    </c:if>
                 <json:array name="prevList" items="${prevList}" var="i">
                     <json:object>
                         <json:property name="fileSeq" value="${ i.fileSeq }"/>
                         <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                         <json:property name="filePath" value="${ i.filePath }"/>
                         <json:property name="fileSize" value="${ i.fileSize }"/>
                     </json:object>
                 </json:array>
                    <c:if test="${ item.fileType ne 'VIDEO'}">
                  <json:array name="fileList" items="${fileList}" var="i">
                      <json:object>
                          <json:property name="fileSeq" value="${ i.fileSeq }"/>
                          <json:property name="filePath" value="${ i.filePath }"/>
                          <json:property name="fileSize" value="${ i.fileSize }"/>
                          <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                      </json:object>
                  </json:array>
                    </c:if>
                    <json:array name="genreList" items="${genreList}" var="i">
                        <json:object>
                            <json:property name="genreSeq" value="${ i.genreSeq }"/>
                            <json:property name="genreNm" value="${ i.genreNm }"/>
                        </json:object>
                    </json:array>
                 <json:array name="serviceList" items="${serviceList}" var="i">
                     <json:object>
                         <json:property name="svcSeq" value="${ i.svcSeq }"/>
                         <json:property name="svcNm" value="${ i.svcNm }"/>
                     </json:object>
                 </json:array>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'categoryInsert' }">
        <c:if test="${insertType != null and insertType != ''}">
            <json:property name="insertType" value="${ insertType }" />
        </c:if>
        <c:if test="${secondCtgId != null and secondCtgId != ''}">
            <json:property name="secondCtgId" value="${ secondCtgId }" />
        </c:if>
        <c:if test="${contsCtgSeq != null and contsCtgSeq != ''}">
            <json:property name="contsCtgSeq" value="${ contsCtgSeq }" />
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsCtgList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <c:if test="${ !empty firstCtgList}">
                    <json:array name="firstCtgList" items="${firstCtgList}" var="i">
                        <json:object>
                            <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                            <json:property name="firstCtgID" value="${ i.firstCtgID }"/>
                        </json:object>
                    </json:array>
                </c:if>
                <json:array name="contsCtgList" items="${item}" var="i">
                    <json:object>
                        <json:property name="contsCtgSeq" value="${ i.contsCtgSeq }"/>
                        <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                        <json:property name="firstCtgID" value="${ i.firstCtgID }"/>
                        <json:property name="secondCtgID" value="${ i.secondCtgID }"/>
                        <json:property name="ctgCnt" value="${ i.ctgCnt+1 }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsCtgListNm' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contsCtgList" items="${item}" var="i">
                    <json:object>
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                        <json:property name="secondCtgID" value="${ i.secondCtgID }"/>
                        <json:property name="storDevCnt" value="${ i.storDevCnt }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>



    <c:if test="${ resultType eq 'contentsUpdateHistoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsUpdateHistoryList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="seq" value="${ i.fileSeq }"/>
                        <json:property name="contentsSeq" value="${ i.contentsSeq }"/>
                        <c:choose>
                            <c:when test="${ i.columnName eq 'TITLE' }"><json:property name="columnName" value="콘텐츠명"/></c:when>
                            <c:when test="${ i.columnName eq 'SUBTITLE' }"><json:property name="columnName" value="설명"/></c:when>
                            <c:when test="${ i.columnName eq 'CONTENTS_CATEGORY_SEQ' }"><json:property name="columnName" value="카테고리"/></c:when>
                            <c:when test="${ i.columnName eq 'CONTENTS_ID' }"><json:property name="columnName" value="콘텐츠 ID"/></c:when>
                        </c:choose>
                        <json:property name="oldValue" value="${ i.oldValue }"/>
                        <json:property name="newValue" value="${ i.newValue }"/>
                        <json:property name="updateId" value="${ i.updaterId }"/>
                        <json:property name="regDate" value="${ i.regDate }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentDisplayList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentDisplayList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="contsSeq" value=" ${ i.contsSeq }" />
                        <json:property name="fileSeq" value=" ${ i.fileSeq }" />
                        <json:property name="filePath" value=" ${ i.filePath }" />
                        <json:property name="orginlFileNm" value=" ${ i.orginlFileNm }" />
                        <json:property name="contsID" value=" ${ i.contsID }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false" />
                        <json:property name="contsDesc" value=" ${ i.contsDesc }" escapeXml="false" />
                        <json:property name="firstCtgNm" value=" ${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value=" ${ i.secondCtgNm }" />
                        <json:property name="cretrID" value=" ${ i.cretrId }" />
                        <json:property name="cretrNm" value=" ${ i.cretrNm }" />
                        <json:property name="sttusVal" value=" ${ i.sttusVal }" />
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentDisplayTotCnt' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="totalCount" value="${ totalCount }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentDisplyState' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="dispOn" value="${ dispOn }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentDisplayHistoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentDisplayHistoryList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="actcSeq" value=" ${ i.actcSeq }" />
                        <json:property name="contsID" value=" ${ i.contsID }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false" />
                        <json:property name="cretrID" value=" ${ i.cretrId }" />
                        <json:property name="cretrNm" value=" ${ i.cretrNm }" />
                        <json:property name="actcSttus" value=" ${ i.actcSttus }" />
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 10 == 0}">
                        <json:property name="totalPage" value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage" value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentDisplayOnList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentDisplayOnList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="contsSeq" value=" ${ i.contsSeq }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false" />
                        <json:property name="firstCtgNm" value=" ${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value=" ${ i.secondCtgNm }" />
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'recommendContentList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="recommendContentList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="rcmdContsSeq" value=" ${ i.rcmdContsSeq }" />
                        <json:property name="contsSeq" value=" ${ i.contsSeq }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false" />
                        <json:property name="firstCtgNm" value=" ${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value=" ${ i.secondCtgNm }" />
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'devContsDetailList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsList" items="${item}" var="i">
                    <json:object>
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsSubTitle" value="${ i.contsSubTitle }" escapeXml="false"/>
                        <json:property name="contsDesc" value="${ i.contsDesc }" escapeXml="false"/>
                        <json:property name="contCtgNm" value="${ i.contCtgNm }"/>
                        <json:property name="contsID" value="${ i.contsID }"/>
                        <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                        <json:property name="rcessWhySbst" value="${ i.rcessWhySbst }"/>
                        <json:property name="cretrID" value="${ i.cretrId }"/>
                        <json:property name="fileType" value="${ i.fileType }"/>
                        <json:property name="maxAvlNop" value="${ i.maxAvlNop }"/>
                        <json:property name="exeFilePath" value="${ i.exeFilePath }"/>
                        <json:property name="mbrNm" value="${ i.mbrNm }"/>
                        <json:property name="contsVer" value="${ i.contsVer }"/>
                        <json:property name="contsCtgSeq" value="${ i.contsCtgSeq }"/>
                        <json:property name="sttus" value="${ i.sttusNm }"/>
                        <json:property name="sttusVal" value="${ i.sttusVal }"/>
                        <json:property name="cp" value="${ i.cpNm }"/>
                        <json:property name="cntrctStDt" value="${ i.cntrctStDt }"/>
                        <json:property name="cntrctFnsDt" value="${ i.cntrctFnsDt }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <json:property name="amdDt" value="${ i.amdDt }"/>
                        <json:property name="amdrID" value="${ i.amdrID }"/>
                        <json:array name="contentsGenreList" items="${i.genreList}" var="genre">
                            <json:object>
                                <c:if test="${genre.genreNm ne 'N/A'}">
                                    <json:property name="genreSeq" value="${ genre.genreSeq }"/>
                                    <json:property name="genreNm" value="${ genre.genreNm }"/>
                                </c:if>
                            </json:object>
                        </json:array>
                        <json:object name="coverImg">
                            <json:property name="fileSeq" value="${ i.coverImg.fileSeq }"/>
                            <json:property name="coverImgNm" value="${ i.coverImg.orginlFileNm }"/>
                            <json:property name="coverImgPath" value="${ i.coverImg.filePath }"/>
                            <json:property name="coverImgSize" value="${ i.coverImg.fileSize }"/>
                        </json:object>
                        <json:array name="thumbnailList" items="${i.thumbnailList}" var="k">
                            <json:object>
                                <json:property name="fileSeq" value="${ k.fileSeq }"/>
                                <json:property name="thumbnailNm" value="${ k.orginlFileNm }"/>
                                <json:property name="thumbnailPath" value="${ k.filePath }"/>
                                <json:property name="thumbnailSize" value="${ k.fileSize }"/>
                            </json:object>
                        </json:array>
                        <c:if test="${ i.fileType eq 'VIDEO'}">
                            <json:array name="videoList" items="${i.videoList}" var="e">
                                <json:object>
                                    <json:property name="fileSeq" value="${ e.fileSeq }"/>
                                    <json:property name="fileNm" value="${ e.orginlFileNm }"/>
                                    <json:property name="filePath" value="${ e.filePath }"/>
                                    <json:property name="fileSize" value="${ e.fileSize }"/>
                                    <json:object name="metadataInfo">
                                        <json:property name="fileSeq" value="${ e.metadataXmlInfo.fileSeq}"/>
                                        <json:property name="fileNm" value="${ e.metadataXmlInfo.orginlFileNm }"/>
                                        <json:property name="filePath" value="${ e.metadataXmlInfo.filePath }"/>
                                        <json:property name="fileSize" value="${ e.metadataXmlInfo.fileSize }"/>
                                    </json:object>
                                    <json:object name="contsXmlInfo">
                                        <json:property name="fileSeq" value="${ e.contsXmlInfo.fileSeq }"/>
                                        <json:property name="fileNm" value="${ e.contsXmlInfo.orginlFileNm }"/>
                                        <json:property name="filePath" value="${ e.contsXmlInfo.filePath }"/>
                                        <json:property name="fileSize" value="${ e.contsXmlInfo.fileSize }"/>
                                    </json:object>
                                </json:object>
                            </json:array>
                        </c:if>
                        <json:array name="prevList" items="${i.prevList}" var="f">
                            <json:object>
                                <json:property name="fileSeq" value="${ f.fileSeq }"/>
                                <json:property name="fileNm" value="${ f.orginlFileNm }"/>
                                <json:property name="filePath" value="${ f.filePath }"/>
                                <json:property name="fileSize" value="${ f.fileSize }"/>
                            </json:object>
                        </json:array>
                        <c:if test="${ i.fileType ne 'VIDEO'}">
                            <json:array name="fileList" items="${i.fileDataList}" var="o">
                                <json:object>
                                    <json:property name="fileSeq" value="${ o.fileSeq }"/>
                                    <json:property name="filePath" value="${ o.filePath }"/>
                                    <json:property name="fileSize" value="${ o.fileSize }"/>
                                    <json:property name="fileNm" value="${ o.orginlFileNm }"/>
                                </json:object>
                            </json:array>
                        </c:if>
                        <json:array name="serviceList" items="${i.serviceList}" var="p">
                            <json:object>
                                <json:property name="svcSeq" value="${ p.svcSeq }"/>
                                <json:property name="svcNm" value="${ p.svcNm }"/>
                            </json:object>
                        </json:array>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'contentsDetail' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="contentsInfo">
                    <json:property name="contsSeq" value="${ item.contsSeq }"/>
                    <json:property name="contsTitle" value="${ item.contsTitle }" escapeXml="false"/>
                    <json:property name="contsSubTitle" value="${ item.contsSubTitle }" escapeXml="false"/>
                    <json:property name="contsDesc" value="${ item.contsDesc }" escapeXml="false"/>
                    <json:property name="contsID" value="${ item.contsID }"/>
                    <json:property name="firstCtgNm" value="${ item.firstCtgNm }"/>
                    <json:property name="secondCtgNm" value="${ item.secondCtgNm }"/>
                    <json:property name="rcessWhySbst" value="${ item.rcessWhySbst }"/>
                    <json:property name="cretrID" value="${ item.cretrId }"/>
                    <json:property name="fileType" value="${ item.fileType }"/>
                    <json:property name="fileSize" value="${ item.fileSize }"/>
                    <json:property name="maxAvlNop" value="${ item.maxAvlNop }"/>
                    <json:property name="exeFilePath" value="${ item.exeFilePath }"/>
                    <json:property name="mbrNm" value="${ item.mbrNm }"/>
                    <json:property name="contsVer" value="${ item.contsVer }"/>
                    <json:property name="contsCtgSeq" value="${ item.contsCtgSeq }"/>
                    <json:property name="sttus" value="${ item.sttusNm }"/>
                    <json:property name="sttusVal" value="${ item.sttusVal }"/>
                    <json:property name="cpNm" value="${ item.cpNm }"/>
                    <json:property name="cntrctStDt" value="${ item.cntrctStDt }"/>
                    <json:property name="cntrctFnsDt" value="${ item.cntrctFnsDt }"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                    <json:property name="amdrID" value="${ item.amdrID }"/>
                    <json:property name="coverImgfileSeq" value="${ coverImg.fileSeq }"/>
                    <json:property name="coverImgNm" value="${ coverImg.orginlFileNm }"/>
                    <json:property name="coverImgPath" value="${ coverImg.filePath }"/>
                    <json:property name="contsSubMetadataSeq" value="${ item.contsSubMetadataSeq }"/>
                    <json:object name="contsSubMetadata">
                        <c:forEach var="list" items="${subMetadataVal}" varStatus="num">
                            <c:set var="key" value="${list.key }"></c:set>
                            <json:property name="${ key }" value="${ list.value }"/>
                        </c:forEach>
                    </json:object>
                    <json:array name="thumbnailList" items="${thumbnailList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="thumbnailNm" value="${ i.orginlFileNm }"/>
                            <json:property name="thumbnailPath" value="${ i.filePath }"/>
                        </json:object>
                    </json:array>
                    <json:array name="videoList" items="${videoList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                            <json:object name="metadataInfo">
                                <json:property name="fileSeq" value="${ i.metadataXmlInfo.fileSeq}"/>
                                <json:property name="fileNm" value="${ i.metadataXmlInfo.orginlFileNm }"/>
                                <json:property name="filePath" value="${ i.metadataXmlInfo.filePath }"/>
                            </json:object>
                            <json:object name="contsXmlInfo">
                                <json:property name="fileSeq" value="${ i.contsXmlInfo.fileSeq }"/>
                                <json:property name="fileNm" value="${ i.contsXmlInfo.orginlFileNm }"/>
                                <json:property name="filePath" value="${ i.contsXmlInfo.filePath }"/>
                            </json:object>
                        </json:object>
                    </json:array>
                    <json:array name="prevList" items="${prevList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                        </json:object>
                    </json:array>
                    <json:array name="fileList" items="${fileList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                        </json:object>
                    </json:array>
                    <json:array name="genreList" items="${genreList}" var="i">
                        <json:object>
                            <json:property name="genreSeq" value="${ i.genreSeq }"/>
                            <json:property name="genreNm" value="${ i.genreNm }" escapeXml="false"/>
                        </json:object>
                    </json:array>
                    <json:array name="serviceList" items="${serviceList}" var="i">
                        <json:object>
                            <json:property name="svcSeq" value="${ i.svcSeq }"/>
                            <json:property name="svcNm" value="${ i.svcNm }" escapeXml="false"/>
                        </json:object>
                    </json:array>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsDispList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentDisplayList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="contsSeq" value=" ${ i.contsSeq }" />
                        <json:property name="fileSeq" value=" ${ i.fileSeq }" />
                        <json:property name="filePath" value=" ${ i.filePath }" />
                        <json:property name="orginlFileNm" value=" ${ i.orginlFileNm }" />
                        <json:property name="contsID" value=" ${ i.contsID }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsDesc" value=" ${ i.contsDesc }" escapeXml="false"/>
                        <json:property name="contsSubTitle" value="${ i.contsSubTitle }" escapeXml="false"/>
                        <json:property name="firstCtgNm" value=" ${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value=" ${ i.secondCtgNm }" />
                        <json:property name="genreNm" value=" ${ i.genreNm }" escapeXml="false" />
                        <json:property name="cretrID" value=" ${ i.cretrID }" />
                        <json:property name="cretrNm" value=" ${ i.cretrNm }" />
                        <json:property name="sttusVal" value=" ${ i.sttusVal }" />
                        <json:property name="maxAvlNop" value=" ${ i.maxAvlNop }" />
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'multiGenreDispContList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="genreContentList" items="${item}" var="i" >
                    <c:set var="data" value="${i }"></c:set>
                    <json:object>
                        <json:property name="reqGenre" value=" ${ data['genreName'] }" />
                        <json:array name="contentList" items="${data['contList']}" var="j" >
                            <json:object>
                                <json:property name="contsSeq" value=" ${ j.contsSeq }" />
                                <json:property name="filePath" value=" ${ j.filePath }" />
                                <json:property name="contsTitle" value=" ${ j.contsTitle }" escapeXml="false"/>
                                <json:property name="contsDesc" value=" ${ j.contsDesc }" escapeXml="false"/>
                                <json:property name="contsSubTitle" value="${ j.contsSubTitle }" escapeXml="false"/>
                                <json:property name="firstCtgNm" value=" ${ j.firstCtgNm }" />
                                <json:property name="secondCtgNm" value=" ${ j.secondCtgNm }" />
                                <json:property name="genreNm" value=" ${ j.genreNm }" escapeXml="false" />
                                <json:property name="maxAvlNop" value=" ${ j.maxAvlNop }" />
                                <json:property name="amdDt" value=" ${ j.amdDt }" />
                            </json:object>
                        </json:array>
                        <json:property name="totalCount" value=" ${ data['totalCount'] }" />
                        <json:property name="currentPage" value=" ${ data['currentPage'] }" />
                        <json:property name="totalPage" value=" ${ data['totalPage'] }" />
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'searchResultContentsDispList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="searchResultContentList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="contsSeq" value=" ${ i.contsSeq }" />
                        <json:property name="filePath" value=" ${ i.filePath }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsDesc" value=" ${ i.contsDesc }" escapeXml="false"/>
                        <json:property name="contsSubTitle" value="${ i.contsSubTitle }" escapeXml="false"/>
                        <json:property name="firstCtgNm" value=" ${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value=" ${ i.secondCtgNm }" />
                        <json:property name="genreNm" value=" ${ i.genreNm }" escapeXml="false" />
                        <json:property name="cretrID" value=" ${ i.cretrID }" />
                        <json:property name="cretrNm" value=" ${ i.cretrNm }" />
                        <json:property name="maxAvlNop" value=" ${ i.maxAvlNop }" />
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'myPlayContList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="myPlayContList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="uid" value=" ${ i.uid }" />
                        <json:property name="accRoute" value=" ${ i.accRoute }" />
                        <json:property name="contsSeq" value=" ${ i.contsSeq }" />
                        <json:property name="filePath" value=" ${ i.filePath }" />
                        <json:property name="contsTitle" value=" ${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="contsDesc" value=" ${ i.contsDesc }" escapeXml="false"/>
                        <json:property name="contsSubTitle" value="${ i.contsSubTitle }" escapeXml="false"/>
                        <json:property name="firstCtgNm" value=" ${ i.firstCtgNm }" />
                        <json:property name="secondCtgNm" value=" ${ i.secondCtgNm }" />
                        <json:property name="genreNm" value=" ${ i.genreNm }" escapeXml="false" />
                        <json:property name="maxAvlNop" value=" ${ i.maxAvlNop }" />
                        <json:property name="amdDt" value=" ${ i.amdDt }" />
                        <json:property name="canPlayResn" value=" ${ i.canPlayResn }" />
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'resultCanPlayContents' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="canPlay" value="${ canPlay }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'useTimeInfo'}">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="useTimeInfo">
                    <json:property name="contsUseTimeSeq" value="${ item.contsUseTimeSeq }" />
                    <json:property name="devSeq" value="${ item.devSeq }" />
                    <json:property name="tagSeq" value="${ item.tagSeq }" />
                    <json:property name="contsSeq" value="${ item.contsSeq }" />
                    <json:property name="stTime" value="${ item.stTime }" />
                    <json:property name="fnsTime" value="${ item.fnsTime }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>
</json:object>