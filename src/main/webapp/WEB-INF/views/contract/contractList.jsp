<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contract/contractList.js"></script>

<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" name="mbrSe" id="mbrSe" value="${memberSec}" />
<input type="hidden" name="cpSeq" id="cpSeq" value="${cpSeq}" />
<input type="hidden" name="pageType" id="pageType" value="contract" />


<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
                <div class="conTitle tabOn"><a href="javascript:;" onclick="tabVal(0,this);"><spring:message code='menu.contractMng.list'/></a></div>
                <div class="conTitle tabEnd tabOff"><a href="javascript:;" onclick="tabVal(1,this)"><spring:message code='menu.distribute.list'/></a></div>
        </div>
        <!-- tabGrp E -->

        <div class="rsvGrp rsvGrpPos" style="width:auto;">
            <!-- Select box S -->
            <div class="rsvSelect" style="float:left;" id="cp">
                <span style='font-size:14px; vertical-align: middle;'><spring:message code="title.cp"/> &nbsp;</span>
                <span>
                    <select id="cpList" onchange="contractList();" style="min-width:136px;"></select>
                </span>
            </div>
            <!-- Select box E -->

            <div class="rsvSelect" style="float:left; margin-right: 10px;">
                <span style='font-size:14px; vertical-align: middle;'><spring:message code='content.service.name'/></span>
                <span>
                    <select id="svcList" onchange="storList();">
                    </select>
                </span>
            </div>

            <div class="rsvSelect">
                <span style='font-size:14px; vertical-align: middle;'><spring:message code="store.comn.name"/></span>
                <span>
                    <select id="storList" onchange="contractList();">
                    </select>
                </span>
            </div>
        </div>



        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea" class="">
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
            </div>
            <!-- Table Area E -->

            <!-- btnGrp S -->
            <div class="CenterGrp">
                <!-- Search Group S -->
                <div class="searchBox" style="display:none" >
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code='button.search'/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->
            </div>
            <!-- btnGrp E -->
        </div>
        <!-- content E -->
    </div>

    <%@include file="/WEB-INF/views/frame/footer.jsp"%>
</div>
<!-- Contents E -->

