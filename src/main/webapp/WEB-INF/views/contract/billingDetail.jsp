<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contract/billingDetail.js"></script>
<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="ratSeq" value="${ratSeq}" />
<input type="hidden" id="storSeq" value="0" />
<input type="hidden" id="svcSeq" value="0" />
<input type="hidden" id="limit" value="5">

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
       <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="billing.supply.detail"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="billing.title.marketNm"/></th>
                            <td id="marketNm"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.contract.no"/></th>
                            <td id="ratContNo"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.contractCnt"/></th>
                            <td id="contsTitle"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="billing.useYn"/></th>
                            <td>
                                <input type="radio" name="ratYn" id="ratYn_Y" value="Y">
                                <label for="ratYn_Y"><spring:message code="billing.type.not.free"/></label>
                                &nbsp;
                                <input type="radio" name="ratYn" id="ratYn_N" value="N">
                                <label for="ratYn_N"><spring:message code="billing.type.free"/></label>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.contract.date"/></th>
                            <td>
                                <span>
                                    <input type="text" class="inputBox lengthS txtCenter" id="ratContStDt" readonly>
                                    ~
                                    <input type="text" class="inputBox txtCenter lengthS" id="ratContFnsDt" readonly>
                                    <input type="button" class=" btnSmallWhite lengthSM txtCenter btnContract" value="<spring:message code="common.contract.maturity"/>" onclick="contractEditPost()">
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="excute.count.limit"/></th>
                            <td>
                                <ul>
                                    <li style="height:33px;">
                                        <input type="radio" name="ratCntYn" id="ratCntYn_Y" value="Y">
                                        <label for="ratCntYn_Y"><spring:message code="billing.type.not.free"/></label>
                                        &nbsp;
                                        <input type="radio" name="ratCntYn" id="ratCntYn_N" value="N">
                                        <label for="ratCntYn_N"><spring:message code="billing.type.free"/></label>
                                    </li>
                                    <li style="height:33px;">
                                        <input type="text" class="inputBox lengthM txtCenter" placeholder="<spring:message code="billing.type.count.insert"/>" id="runLmtCnt">&nbsp;건&nbsp;
                                        <input type="text" class="inputBox txtCenter lengthM remainingPrc" placeholder="<spring:message code="billing.type.price.insert"/>" id="runPerPrc">&nbsp;원
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="billing.Hourly"/></th>
                            <td>
                                <ul>
                                    <li style="height:33px;">
                                        <input type="radio" name="ratTimeYn" id="ratTimeYn_Y" value="Y">
                                        <label for="ratTimeYn_Y"><spring:message code="billing.type.not.free"/></label>
                                        &nbsp;
                                        <input type="radio" name="ratTimeYn" id="ratTimeYn_N" value="N">
                                        <label for="ratTimeYn_N"><spring:message code="billing.type.free"/></label>
                                    </li>
                                    <li style="height:33px;">
                                        <select class="txtCenter lengthM" style="margin-right:17px;" id="ratTime">
                                            <option value="0"><spring:message code="contents.contract.selTime"/></option>
                                            <option value="10">10<spring:message code="common.minute"/></option>
                                            <option value="20">20<spring:message code="common.minute"/></option>
                                            <option value="30">30<spring:message code="common.minute"/></option>
                                            <option value="40">40<spring:message code="common.minute"/></option>
                                            <option value="50">50<spring:message code="common.minute"/></option>
                                            <option value="60">60<spring:message code="common.minute"/></option>
                                        </select>&nbsp;
                                        <input type="text" class="inputBox txtCenter lengthM remainingPrc" placeholder="<spring:message code="billing.type.price.insert"/>" id="ratPrc">&nbsp;원
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="billing.type.lump"/></th>
                            <td>
                                <ul>
                                    <li style="height:33px;">
                                        <input type="radio" name="ratLumpYn" id="ratLumpYn_Y" value="Y">
                                        <label for="ratLumpYn_Y"><spring:message code="billing.type.not.free"/></label>
                                        &nbsp;
                                        <input type="radio" name="ratLumpYn" id="ratLumpYn_N" value="N">
                                        <label for="ratLumpYn_N"><spring:message code="billing.type.free"/></label>
                                    </li>
                                    <li style="height:33px;">
                                        <input type="text" class="inputBox lengthM txtCenter remainingPrc" placeholder="<spring:message code="billing.type.lumpPrice.insert"/>" id="lmsmpyPrc">&nbsp;원
                                    </li>
                                </ul>
                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnModify" value="<spring:message code='button.update'/>" onclick="pageMove('/billingDetail/${ratSeq}/edit')">
                <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.updateHistory'/>" onclick="showHistory()">
                <input type="button" class="btnNormal btnList btnRight" value="<spring:message code='button.list'/>" onclick="pageMoveStorList();">
            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>
<div id="openHistoryModify" style="display: none; top:0px; left:0px">
    <!-- content S -->
    <div class="content">
        <div class="popupTitle"><spring:message code='billing.supply.log'/></div>
        <div class="popupNumber align_l"><spring:message code='common.contract.no'/> : <span id="popRatContNo"></span></div>
        <!-- Table Area S -->
        <div class="tableArea" style="height:304px">
           <table id="jqgridHstData"></table>
           <div id="pageHstDiv"></div>
        </div>
        <!-- Table Area E -->

        <!-- Button Group S -->
        <div class="btnGrp txtCenter">
            <input type="button" class="btnNormal btnCancel" value="닫기" onclick="closePopups() ">
        </div>
        <!-- Button Group E -->
</div>
<!-- content E -->


</div>
