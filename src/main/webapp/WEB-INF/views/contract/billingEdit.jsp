<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contract/billingEdit.js"></script>
<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="ratSeq" value="${ratSeq}" />
<input type="hidden" id="storSeq" value="0" />
<input type="hidden" id="svcSeq" value="0" />

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
       <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="billing.supply.edit"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="billing.title.marketNm"/></th>
                            <td>
                                <input type="text" class="inputBox lengthM" id="svcNm" readonly>
                                <input type="text" class="inputBox lengthM" id="storNm" readonly>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.contract.no"/></th>
                            <td>
                                <input type="text" class="inputBox lengthXXL onlynum remaining" id="ratContNo" max="30" readonly>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.contractCnt"/></th>
                            <td>
                                <input type="text" class="inputBox lengthXXL" id="contsTitle" style="background:white;" onclick="setList(); searchOnContsReset(); openPopups2()" readonly>&nbsp;
                                <input type="button" class="btnSearchLongWhite btnSmallWhite lengthSS" value="<spring:message code="button.search"/>" onclick="setList(); searchOnContsReset(); openPopups2()" id="cntSeachBtn">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="billing.useYn"/></th>
                            <td>
                                <input type="radio" name="ratYn" id="ratYn_Y" value="Y">
                                <label for="ratYn_Y"><spring:message code="billing.type.not.free"/></label>
                                &nbsp;
                                <input type="radio" name="ratYn" id="ratYn_N" value="N">
                                <label for="ratYn_N"><spring:message code="billing.type.free"/></label>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.contract.date"/></th>
                            <td>
                                <span>
                                    <input type="text" class="inputBox lengthS txtCenter datepicker" id="ratContStDt" max="9999-12-31T23:59" readonly>
                                    ~
                                    <input type="text" class="inputBox txtCenter lengthS datepicker" id="ratContFnsDt" max="9999-12-31T23:59" readonly>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="excute.count.limit"/></th>
                            <td>
                                <ul>
                                    <li style="height:33px;">
                                        <input type="radio" name="ratCntYn" id="ratCntYn_Y" value="Y">
                                        <label for="ratCntYn_Y"><spring:message code="billing.type.not.free"/></label>
                                        &nbsp;
                                        <input type="radio" name="ratCntYn" id="ratCntYn_N" value="N">
                                        <label for="ratCntYn_N"><spring:message code="billing.type.free"/></label>
                                    </li>
                                    <li style="height:33px;">
                                        <input type="text" class="inputBox lengthM txtCenter onlynum" placeholder="<spring:message code="billing.type.count.insert"/>" id="runLmtCnt">&nbsp;건&nbsp;
                                        <input type="text" class="inputBox txtCenter lengthM remainingPrc" placeholder="<spring:message code="billing.type.price.insert"/>" id="runPerPrc">&nbsp;원
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="billing.Hourly"/></th>
                            <td>
                                <ul>
                                    <li style="height:33px;">
                                        <input type="radio" name="ratTimeYn" id="ratTimeYn_Y" value="Y">
                                        <label for="ratTimeYn_Y"><spring:message code="billing.type.not.free"/></label>
                                        &nbsp;
                                        <input type="radio" name="ratTimeYn" id="ratTimeYn_N" value="N">
                                        <label for="ratTimeYn_N"><spring:message code="billing.type.free"/></label>
                                    </li>
                                    <li style="height:33px;">
                                        <select class="txtCenter lengthM" style="margin-right:17px;" id="ratTime">
                                            <option value="0"><spring:message code="contents.contract.selTime"/></option>
                                            <option value="10">10<spring:message code="common.minute"/></option>
                                            <option value="20">20<spring:message code="common.minute"/></option>
                                            <option value="30">30<spring:message code="common.minute"/></option>
                                            <option value="40">40<spring:message code="common.minute"/></option>
                                            <option value="50">50<spring:message code="common.minute"/></option>
                                            <option value="60">60<spring:message code="common.minute"/></option>
                                        </select>&nbsp;
                                        <input type="text" class="inputBox txtCenter lengthM remainingPrc" placeholder="<spring:message code="billing.type.price.insert"/>" id="ratPrc">&nbsp;원
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="billing.type.lump"/></th>
                            <td>
                                <ul>
                                    <li style="height:33px;">
                                        <input type="radio" name="ratLumpYn" id="ratLumpYn_Y" value="Y">
                                        <label for="ratLumpYn_Y"><spring:message code="billing.type.not.free"/></label>
                                        &nbsp;
                                        <input type="radio" name="ratLumpYn" id="ratLumpYn_N" value="N">
                                        <label for="ratLumpYn_N"><spring:message code="billing.type.free"/></label>
                                    </li>
                                    <li style="height:33px;">
                                        <input type="text" class="inputBox lengthM txtCenter remainingPrc" placeholder="<spring:message code="billing.type.lumpPrice.insert"/>" id="lmsmpyPrc">&nbsp;원
                                    </li>
                                </ul>
                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                    <input type="button" class="btnNormal btnModify" value="<spring:message code='button.update.confirm'/>" onclick="billingEdit()">
                    <input type="button" class="btnNormal btnCancel btnCancelView" value="<spring:message code='button.reset'/>" onclick="popConfirmLayer(getMessage('common.update.cancel.msg'), function(){pageMoveStorList();});">
            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>

<div id="popupWinBig" style="width: 1020px; height:620px; top:0px; left:0px">
    <div class="popupTitle"><spring:message code="contents.contractCnt.select" /></div>
    <!-- harf div S -->
    <div id="harfGrp" style="margin-top:10px; background-color:#fff; border:0; padding:0;">
        <!-- harf left table S -->
        <div class="divHarf harfLeft" style="height:470px;">
            <h1 class="harfTitle align_l"><spring:message code="common.allList" /></h1>

            <!-- Search Group S -->
            <div class="searchBox2">
                        <div class="searchGrpWhite">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td width="105">
                                            <select style="height:30px;" id="searchSel">
                                                <option><spring:message code="table.contentsTitle" /></option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="inputBox4" style="width:100%;" id="searchKeyword" onkeypress="if(event.keyCode==13) {searchOnConts(); return false;}">
                                        </td>
                                        <td width="85" style="text-align:right; padding-right:5px;">
                                            <input type="button" class="btnSearchLongWhite btnSmallWhite lengthSS" value="<spring:message code="button.search" />" style="height:30px;" onclick="searchOnConts()">
                                        </td>
                                        <td width="100">
                                            <input type="button" class="btnResetWhite btnSmallWhite lengthS" value="<spring:message code="button.allView" />" onclick="searchOnContsReset()" style="height:30px;">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            <!-- Search Group E -->

            <!-- table S -->
            <div class="scroll-table">
                <div class="scrollField">
                    <table class="titleTable fix_table listBox tableList">
                        <thead>
                            <tr style="left:0;">
                                <th class="width50 chkBox"><input type="checkbox" id="contsAllCb" onchange="checkboxAll('conts')"></th>
                                <th class=""><spring:message code="common.contents.nm"/></th>
                                <th class="itemW20"><spring:message code="cms.release.table.ctgNm"/></th>
                                <th class="itemW20"><spring:message code="table.regdate"/></th>
                            </tr>
                        </thead>
                        <tbody id="selectContents">

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- table E -->
        </div>
        <!-- harf left table E -->

        <!-- button S -->
        <div class="addBtns">
            <div class="btnLeftArrow cursorPointer" onclick="deleteContentsInfo()"><img src="<%=request.getContextPath()%>/resources/image/btn_left.gif"></div>
            <div class="btnRightArrow cursorPointer" onclick="insertContentsInfo()"><img src="<%=request.getContextPath()%>/resources/image/btn_right.gif"></div>
        </div>
        <!-- button E -->

    <!-- harf right table S -->
        <div class="divHarf harfRight" style="height:470px;">
            <h1 class="harfTitle align_l"><spring:message code="common.selList" /></h1><br><br>

            <!-- table S -->
            <div class="scroll-table">
                <div class="scrollField">
                    <table class="titleTable fix_table listBox tableList">
                        <thead>
                            <tr style="left:0;">
                                <th class="width50 chkBox"><input type="checkbox" id="groupContentsAllCb" onchange="checkboxAll('groupContents')"></th>
                                <th class=""><spring:message code="common.contents.nm"/></th>
                                <th class="itemW40"><spring:message code="cms.release.table.ctgNm"/></th>
                            </tr>
                        </thead>
                        <tbody id="selectVRContents">

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- table E -->


            </div>
            <!-- harf right table E -->
        </div>
        <!-- harf div E -->
        <div class="txtCenter">
            <span><input type="button" class="btnNormal btnOk" value="확인" onclick="popConfirmLayer(getMessage('common.content.update.msg'), function(){closePopups();setList();});"></span>
            <span><input type="button" class="btnNormal btnCancel" value="취소" onclick="popConfirmLayer(getMessage('common.content.cancel.msg'), function(){closePopups(); resetList();});"></span>
        </div>

    </div>
 <!---------  popup End   ----------->