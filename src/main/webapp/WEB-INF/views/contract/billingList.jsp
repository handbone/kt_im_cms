<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contract/billingList.js"></script>

<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" name="mbrSe" id="mbrSe" value="${memberSec}" />
<input type="hidden" name="storSeq" id="storSeq" value="${storSeq}" />

<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
                <div class="conTitle tabOn tabEnd"><spring:message code='menu.distribute.store.list'/></div>
        </div>
        <!-- tabGrp E -->

        <div class="titleAreaGrp" style="top:55px;">
            <div class="areaLeft h4 txtBold" style="padding:5px 0 0 0; letter-spacing:-1px;"><spring:message code='billing.title.storeNm'/> : <span id="marketNm"></span></div>
            <div class="areaRight"><input type="button" class="btnSmall btnAdd btnRight" value="추가" onclick="pageMove('/billingDetail/${storSeq}/regist')"></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea" class="">
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
            </div>
            <!-- Table Area E -->

            <!-- btnGrp S -->
            <div class="CenterGrp absol_btn">
                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox" style="min-width: 120px; width: auto;">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code='button.search'/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->

                <div class="cellTable">
                    <input type="button" class="btnSmall btnBack btnRight" value="BACK" onclick="pageMove('/contract#page=1&pageType=billing&svcSeq=&storSeq=')">
                </div>
            </div>
            <!-- btnGrp E -->
        </div>
        <!-- content E -->
    </div>

    <%@include file="/WEB-INF/views/frame/footer.jsp"%>
</div>
<!-- Contents E -->

