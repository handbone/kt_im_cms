<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>

<%@include file="/WEB-INF/views/frame/header.jsp"%>

<script>
var loginMbrId = "";
$(document).ready(function(){
    var name = "<c:out value='${name}' /> " + getMessage("common.man");
    $("#loginUserName").html(name);
});
</script>

        <!-- left group S -->
        <div id="leftGrp">
            <ul>
                <li class="logo">
                    <a href="javascript:;" onclick="pageMove('/')">
                        <img src="<c:url value='/resources/image/logo_cms_w.png' />" alt="<spring:message code='menu.title'/>"/>
                    </a>
                </li>
                <li>
                    <div class="loginUserGrp">
                        <ul>
                            <li class="loginUser">
                                <span class="loginUserName">
                                    <a href="javascript:;" onclick="pageMove('/mypage')" id="loginUserName">
                                    </a>
                                </span>
                            <li class="loginUserLogout">
                                <input type="button" class="btnLogout cursorPointer" value="<spring:message code='button.logout'/>" onclick="logout()">
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="leftMenuGrp">
                    <ul>
                        <c:forEach var="item" items="${menuList}" varStatus="status">
                            <c:choose>
                                <c:when test="${ item.upperIndex eq 0 }">
                                    <li class="leftMenu cursorPointer <c:out value='${item.addClass}' /> <c:if test='${ item.isFocused }'>leftMenuOn</c:if>">
                                        <a href="javascript:;"><c:out value="${item.name}" /></a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="leftSubMenu <c:if test='${ item.isFocused }'>leftSubMenuOn</c:if>" style="display:none;">
                                        <a href="javascript:;" onclick="pageMove('<c:out value='${item.uri}' />')"><c:out value="${item.name}" /></a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- left group E -->

        <!-- right group S -->
        <div id="rightGrp">
