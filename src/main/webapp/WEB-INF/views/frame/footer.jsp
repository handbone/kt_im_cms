<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/frame/footer.js"></script>

    <div id="bottom">
        <div class="terms">
            <span><a href='javascript:void(0);' onclick="showPolicyPopup('popupWin')"><spring:message code='common.terms'/></a></span>
            <span><a href='javascript:void(0);' onclick="showPolicyPopup('popupWin2')"><spring:message code='common.privacy.policy'/></a></span>
        </div>
        <div class="copyright">Copyright © <spring:message code='menu.title'/> Co.,Ltd.  All Rights Reserved.</div>
    </div>
    <form id="NoneForm"></form>
</div>
<!-- right group E -->

</div>
<!-- wrapper E -->

<div id="center" style="display: none;">
    <!-- Popup S -->
    <div class="popupTerms" id="popupWin">
        <div class="conTitleGrp">
            <div class="conTitle"><center><spring:message code='common.terms.title'/></center></div>
        </div>

        <!-- content S -->
        <div class="content">
            <div class="contentsText" id="termsContent"></div>

            <!-- Button Group S -->
            <div class="btnGrp2">
                <input type="button" class="btnNormal btnClose btnCenter" value="<spring:message code='button.close'/>" onclick="showPolicyPopupClose()">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
    <!-- Popup E -->

    <!-- Popup S -->
    <div class="popupPrivacy" id="popupWin2">
        <div class="conTitleGrp">
            <div class="conTitle"><center><spring:message code='common.privacy.policy.title'/></center></div>
        </div>

        <!-- content S -->
        <div class="content">
            <div class="contentsText" id="policyContent"></div>

            <!-- Button Group S -->
            <div class="btnGrp2">
                <input type="button" class="btnNormal btnClose btnCenter" value="<spring:message code='button.close'/>" onclick="showPolicyPopupClose()">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
    <!-- Popup E -->
</div>

<!--  Comfirm Password Popup S -->
<div class="popupPrivacy" id="popupComfirm">
    <div class="modifyPassword">
        <h1><center><spring:message code='common.cofirm.passwd' /></center></h1>
        <ul>
            <li style="display: table;">
                <span style="display: table-cell; white-space: nowrap;">비밀번호 &nbsp;</span>
                <span style="display: table-cell;width: 100%;">
                    <input id="cofirmPwd" type="password" class="inputbox lengthW100 textInput" autocomplete="new-password">
                </span>
            </li>
            <li style="margin: 12px 0px;">
                <div style="position: relative;text-align: center;">
                    <input type="button" class="btnNormal btnOk" value="<spring:message code='common.confirm' />" style="padding: 2.5px 50px 2.5px 50px;">
                    <input type="button" class="btnNormal btnCancel" value="<spring:message code='login.button.cancel' />" onclick="popLayerClose()">
                </div>
            </li>
        </ul>
    </div>
</div>
<!--  Comfirm Password Popup E -->

</body>
</html>