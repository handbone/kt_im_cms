<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code='menu.title'/></title>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/jquery/jquery-ui.css">
<link rel="stylesheet"
    href="<%=request.getContextPath()%>/resources/css/jquery/jquery-ui-timepicker-addon.css?version=201805101011">
<link href="<%=request.getContextPath()%>/resources/css/jqGrid/ui.jqgrid.css?version=12313" rel="stylesheet"
    type="text/css">
<script
    src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-1.7.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-ui-1.10.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js//libs/jquery/jquery.form.js"></script>
<script src="<%=request.getContextPath()%>/resources/js//libs/jquery/jquery.cookie.min.js"></script>
<!-- chartJS S -->
<script src="<%=request.getContextPath()%>/resources/js/libs/chartJS/moment.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/chartJS/Chart.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/chartJS/utils.js"></script>
<!-- chartJS E -->
<!-- Highchart
<script type="text/javascript" src="<%=request.getContextPath()%>/resources//js/libs/highchart/highstock.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources//js/libs/highchart/highcharts-more.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources//js/libs/highchart/solid-gauge.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources//js/libs/highchart/exporting.js"></script>
 -->
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-ui-timepicker-addon.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jqGrid/grid.locale-en.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jqGrid/jquery.jqGrid.src.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/apiFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/baseFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/commonFunc.js?version=201805251958"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/cookie.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/views/frame/header.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/sweetalert/sweetalert.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/message.js?time=<%=session.getLastAccessedTime()%>"></script>

<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${fn:replace(req.requestURL, req.requestURI, '')}" />
<c:set var="params" value="${requestScope['javax.servlet.forward.query_string']}"/>
<c:set var="requestPath" value="${requestScope['javax.servlet.forward.request_uri']}"/>
<c:set var="pageUrl" value="${ baseURL }${ requestPath }${ not empty params?'?'+=params:'' }"/>


<c:choose>
    <c:when test="${fn:contains(pageUrl, 'qna') || fn:contains(pageUrl, 'faq') || fn:contains(pageUrl, 'notice')}">
        <link href="<%=request.getContextPath()%>/resources/css/common_editor.css?version=201805081746" rel="stylesheet" type="text/css">
    </c:when>
    <c:otherwise>
        <link href="<%=request.getContextPath()%>/resources/css/common.css?version=201805081746" rel="stylesheet" type="text/css">
    </c:otherwise>
</c:choose>


<link href="<%=request.getContextPath()%>/resources/css/implatformcms.css?version=3" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/resources/css/custom.css?version=201805081746" rel="stylesheet" type="text/css">



<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

.swal-title {
    margin: 0px;
    font-size: 18px;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
    margin-bottom: 28px;
    font-weight: 700;
}

.swal-button {
    background-color: #000000;
}

.swal-button[not :disabled]:hover {
    background-color: #A6A6A6;
}

.swal-button:active {
    background-color: #A6A6A6;
}

.swal-button:focus {
    box-shadow: 0 0 0 0;
}
</style>
</head>

<body onload="init('${pageUrl}','<%=request.getContextPath()%>','${requestPath }')">
    <input type="hidden" id="contextPath" value="<%=request.getContextPath()%>">
    <input type="hidden" id="usesAutoSend" value="${usesAutoSend}">
    <script type="text/javascript">
        $(window).load(function() {
            $("#loading").hide();
            setHeaderUse();
            keyup();
        });
    </script>
    <div id="loading">
        <img id="loading-image" src="<c:url value='/resources/image/img/loading.gif' />" alt="Loading..." />
    </div>

    <!-- wrapper S -->
    <div id="wrapper">
