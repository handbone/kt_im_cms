<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/system/db/DbList.js"></script>

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

                <!-- locate S -->
                <div id="locateGrp">
                    <div class="locate">
                        <ul>
                        </ul>
                    </div>
                </div>
                <!-- locate E -->
                
                <!-- page title S -->
                <div class="conTitleGrp">
                    <div class="conTitle"><spring:message code='db.manage.title'/></div>
                </div>
                <!-- page title E -->

                    <div class="noContent" style="display:none;">
                        <div class="txtCenter">
                            <spring:message code="db.table.nodata"/>
                        </div>
                        <!-- Button Group S -->
                        <div class="btnGrp">
                            <input type="button" class="btnNormal btnWrite btnRight" value="<spring:message code='button.create'/>">
                        </div>
                        <!-- Button Group E -->
                    </div>

                    <!-- Table Area S -->
                    <div class="tableArea" style="display:none;">
                        <table class="tableList">
                            <tr>
                                <th><spring:message code='common.item'/></th>
                                <th><spring:message code='common.detail'/></th>
                            </tr>
                            <tr>
                                <td><spring:message code="db.table.name"/></td>
                                <td>
                                    <select id="dbList">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><spring:message code="login.input.ipAddress"/></td>
                                <td id="dbIp"></td>
                            </tr>
                            <tr>
                                <td><spring:message code="db.table.totalSize" /></td>
                                <td id="dbTotSize"></td>
                            </tr>
                        </table>

                        <!-- Button Group S -->
                        <div class="btnGrp">
                            <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code='button.update' />">
                            <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='button.delete'/>">
                            <input type="button" class="btnNormal btnWrite btnRight" value="<spring:message code='button.create'/>">
                        </div>
                        <!-- Button Group E -->
                    </div>
                    <!-- Table Area E -->
                    <br><br>

                    <p class="h2 textBold"><spring:message code="db.manage.totalSize" /></p>
                    <div class="databaseGraphGrp">
                        <canvas id="container" style="margin-top:5px"></canvas>
                    </div>

            </div>
            <!-- content E -->
            
        </div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>