<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/system/db/DbEdit.js"></script>

<input type="hidden" id="dbSeq" value="${seq}">

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

                <!-- locate S -->
                <div id="locateGrp">
                    <div class="locate">
                        <ul>
                        </ul>
                    </div>
                </div>
                <!-- locate E -->
                
                <!-- page title S -->
                <div class="conTitleGrp">
                    <div class="conTitle"><spring:message code="db.manage.modify.title"/></div>
                </div>
                <!-- page title E -->

                    <!-- Table Area S -->
                    <div class="tableArea">

                        <table class="tableList">
                            <tr>    
                                <th class="itemW10"><spring:message code="db.table.name"/></th>
                                <td class="txtLeft"><input id="dbNm" type="text" class="inputBox lengthL remaining" max="20"></td>
                            </tr>
                            <tr>
                                <th><spring:message code="login.input.ipAddress"/></th>
                                <td class="txtLeft"><input id="dbIp" type="text" class="inputBox lengthL remaining" max="15"></td>
                            </tr>
                            <tr>
                                <th><spring:message code="db.table.totalSize" /></th>
                                <td class="txtLeft"><input id="dbTotSize" type="text" class="inputBox lengthL onlynum"><span>&nbsp;GB</span></td>
                            </tr>
                        </table>

                    
                    </div>
                    <!-- Table Area E -->

                    <!-- Button Group S -->
                    <div class="btnGrp txtCenter">
                        <input type="button" class="btnNormal btnModify" value="<spring:message code='button.update.confirm'/>">
                        <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>">
                    </div>
                    <!-- Button Group E -->
            </div>
            <!-- content E -->
        </div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>