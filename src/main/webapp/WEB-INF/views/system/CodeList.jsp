<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript"
    src="<%=request.getContextPath()%>/resources/js/views/system/CodeList.js?version=201805311750"></script>

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

                <!-- locate S -->
                <div id="locateGrp">
                    <div class="locate">
                        <ul>
                        </ul>
                    </div>
                </div>
                <!-- locate E -->
                
                <!-- page title S -->
                <div class="conTitleGrp">
                    <div class="conTitle"><spring:message code='code.manage.title'/></div>
                </div>
                <!-- page title E -->

                    <!-- Table Area S -->
                    <div class="tableArea">

                        <table id="codeInfoDiv" class="tableList">
                            <tr>
                                <th class="itemW10"><spring:message code='code.manage.section'/></th>
                                <td colspan="3" class="txtLeft ">
                                    <select id="selectCodeSec" class="inputBox lengthM">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th id="detailCodeCount" rowspan="2" ><spring:message code='code.manage.detail'/></th>
                                <td class="bgBrWhite"><spring:message code='code.manage.name'/></td>
                                <td class="bgBrWhite" style="width:120px;"><spring:message code='code.manage.value'/></td>
                                <td class="bgBrWhite"  style="width:110px;"><spring:message code='title.update'/> / <spring:message code='title.delete'/></td>
                            </tr>
                        </table>
                        <br>

                        <table class="tableList">
                            <tr>
                                <th class="itemW10"><spring:message code='code.manage.new'/></th>
                                <td class="bgBrWhite"><input id="codeName" type="text" class="inputBox itemW100 remaining" max="50" placeholder="<spring:message code='code.manage.name'/>"></td>
                                <td class="bgBrWhite" style="width:120px;"><input id="codeValue" type="text" class="inputBox width100 txtCenter" placeholder="<spring:message code='code.manage.value'/>"></td>
                                <td id="btnWrite" class="bgBrWhite" style="width:110px;"><input type="button" class="btnSmallGray" value="<spring:message code='button.create'/>"></td>
                            </tr>
                        </table>
                    </div>
                    <!-- Table Area E -->
            </div>
            <!-- content E -->
        </div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>