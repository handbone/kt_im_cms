<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/swfupload/video_handlers.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/customer/ServiceCustomerNoticeEdit.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="8">
<input type="hidden" id="noticeSeq" value="${svcNoticeSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">
<input type="hidden" id="memberStorSeq" value="${storSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.notice.edit.title"/></div>
        </div>
        <!-- Tab E -->

        <form id="fileForm">
            <input type="hidden" id="contsSeq" name="contsSeq"> <input
                type="hidden" id="fileSe" name="fileSe"> <input
                type="hidden" id="orginlFileNm" name="orginlFileNm"> <input
                type="hidden" id="fileDir" name="fileDir"> <input
                type="hidden" id="fileSize" name="fileSize"> <input
                type="hidden" id="fileExt" name="fileExt"> <input
                type="hidden" id="streFileNm" name="streFileNm">
        </form>

        <!-- content S --> 
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code="common.title"/></th>
                        <td><input type="text" class="inputBox itemW100 remaining" id="noticeTitle" max="200"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.section"/></th>
                        <td>
                            <select class="inputBox width200" id="typeSelbox">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.display.period"/></th>
                        <td>
                            <input type="text" class="lengthS inputBox2 txtCenter datepicker" id="startDt" name="startDt" readonly> - 
                            <input type="text" value="2018.9.1" class="lengthS inputBox2 txtCenter datepicker" id="endDt" name="endDt" readonly>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.detail.description"/></th>
                        <td><textarea class="inputBox itemW100 ckeditor" rows="10" id="noticeSbst"></textarea></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.attachfile"/></th>
                        <td>
                            <div id="file_btn_placeholder"></div>
                            <div id="fileArea"></div>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="customer.post.display.yn"/></th>
                        <td>
                            <input type="radio" name='delYn' value='N' /><label><spring:message code="customer.post.display.on"/></label>&nbsp; 
                            <input type="radio" name='delYn' value='Y' /><label><spring:message code="customer.post.display.off"/></label>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->


            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnModify cursorPointer" value="<spring:message code="button.update.confirm"/>" onclick="updateNotice()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="pageMove('/servicecustomer')">
            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display: none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>" width="0%" height="24"
                id="barWidth"><br> <span id="uploadPercent">0</span> <span
                id="uploadText"></span>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>