<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/customer/ServiceCustomerFaqEdit.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="8">
<input type="hidden" id="svcFaqSeq" value="${svcFaqSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">
<input type="hidden" id="memberStorSeq" value="${storSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.faq.edit.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- content S --> 
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="width150"><spring:message code="common.section"/></th>
                        <td>
                            <select class="inputBox width200" id="typeSelbox">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.category"/></th>
                        <td>
                            <select class="inputBox width200" id="ctgSelbox">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.faq.question.plus.title"/></th>
                        <td><input type="text" class="itemW100 remaining" id="faqTitle" max="200"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.faq.answer.plus.sbst"/></th>
                        <td><textarea class="inputBox itemW100 ckeditor" rows="10" id="faqSbst"></textarea></td>
                    </tr>
                    <tr>
                        <th><spring:message code="customer.post.display.yn"/></th>
                        <td>
                            <input type="radio" name='delYn' value='N' /><label><spring:message code="customer.post.display.on"/></label>&nbsp; 
                            <input type="radio" name='delYn' value='Y' /><label><spring:message code="customer.post.display.off"/></label>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnModify cursorPointer" value="<spring:message code="button.update.confirm"/>" onclick="updateFaq()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="pageMove('/servicecustomer')">
            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>