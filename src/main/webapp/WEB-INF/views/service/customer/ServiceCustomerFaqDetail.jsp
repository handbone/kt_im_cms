<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/customer/ServiceCustomerFaqDetail.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="8">
<input type="hidden" id="svcFaqSeq" value="${svcFaqSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">
<input type="hidden" id="memberStorSeq" value="${storSeq}">

<style>
#faqAns img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.faq.detail.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- content S -->
        <div class="content faq">
            <div class="faqTitleQsn faqQuestion" id="faqTitleQsn"></div>
            <!-- Detail Area S -->
            <div class="faqDetail" id="faqQsn" class="noBasic"></div>
            <!-- Detail Area E -->
            <div class="faqTitleAns faqAnswer"><span class="txtBlue"><spring:message code="common.answer"/></span></div>
            <!-- Detail Area S -->
            <div class="faqDetail" id="faqAns"></div>
            <!-- Detail Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" id="btnModify" class="btnNormal btnModify btnLeft cursorPointer" value="<spring:message code="button.update"/>" onclick="pageMove('/servicecustomer/faq/${svcFaqSeq}/edit')">
                <input type="button" id="btnDelete" class="btnNormal btnDelete btnLeft cursorPointer" value="<spring:message code="button.delete"/>" onclick="deleteFaq()">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" value="<spring:message code="button.list"/>" onclick="pageMove('/servicecustomer')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>