<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/customer/ServiceCustomerNoticeDetail.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="8">
<input type="hidden" id="svcNoticeSeq" value="${svcNoticeSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">
<input type="hidden" id="memberStorSeq" value="${storSeq}">

<style>
#noticeSbst img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.notice.detail.title"/></div>
        </div>
        <!-- page title E -->

        <!-- content S -->
        <div class="content notice">
            <div class="noticeTitle" id="noticeTitle"></div>
            <div class="viewDetail" id="noticeSbst" class="noBasic"></div>
            <div class="attatchGrp" id="fileGrp"></div>

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" id="btnModify" class="btnNormal btnModify btnLeft cursorPointer" value="<spring:message code="button.update"/>" onclick="pageMove('/servicecustomer/notice/${svcNoticeSeq}/edit')">
                <input type="button" id="btnDelete" class="btnNormal btnDelete btnLeft cursorPointer" value="<spring:message code="button.delete"/>" onclick="deleteNotice()">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" value="<spring:message code="button.list"/>" onclick="pageMove('/servicecustomer')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>