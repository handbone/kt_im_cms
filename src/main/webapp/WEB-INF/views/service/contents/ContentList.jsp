<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/contents/ContentList.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="4">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" id="type" value="list">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
            <div class="conTitle tabOn"><a href="javascript:;" onclick="tabVal(0,this)"><spring:message code='contents.table.list'/></a></div>
            <div class="conTitle tabOff tabEnd"><a href="javascript:;" onclick="tabVal(1,this)"><spring:message code='service.conts.frmtnlist'/></a></div>
        </div>
        <!-- tabGrp E -->

        <input type="button" class="btnXLarge SyncBtn" value="<spring:message code="button.getRelease"/>" onclick="releaseSync()" style="display:none;">


        <div class="rsvGrp rsvGrpPos" style="width:323px">
            <div class="rsvSelect" style="float:left;">
                <span><spring:message code="service.table.name"/></span>
                <span>
                    <select id="svcList" onchange="storList(-1);">
                    </select>
                </span>
            </div>
            <div class="rsvSelect">
                <span><spring:message code="store.comn.name"/></span>
                <span>
                    <select id="storList" onchange="keywordSearch()">
                    </select>
                </span>
            </div>
        </div>



        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea" class="">
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="CenterGrp">
                <div class="cellTable" style="display:none;">
                    <input type="button" class="btnNormal btnDelete" value="<spring:message code='button.delete'/>" onclick="deleteRelease()">
                </div>

                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code='button.search'/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->

                <div class="cellTable" style="display:none;">
                    <input type="button" class="btnNormal btnWrite" value="<spring:message code='button.create'/>" onclick="makePageMove(this)">
                </div>


            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>