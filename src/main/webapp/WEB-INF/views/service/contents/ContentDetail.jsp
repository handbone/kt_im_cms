<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/ContentDetail.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/libs/swipebox/swipebox.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swipebox/jquery.swipebox.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/justifiedGallery/justifiedGallery.min.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<link href="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="4">
<input type="hidden" id="contsSeq" value="${contsSeq}">

<!-- Contents S -->
<div id="contents">

    <div class="boxRound">

        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="contents.table.details"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tbody>
                    <tr>
                        <th class="w150"><spring:message code="contents.table.name"/></th>
                        <td>
                            <span class="article float_l" id="contsTitle"></span>
                                <span class="boxStateBlack" id="sttusView">
                        </td>
                    </tr>
                    <!-- 콘텐츠 ID -->
                    <tr>
                        <th class="w150"><spring:message code="table.contentsId"/></th>
                        <td><span class="article" id="contsId"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="table.category"/></th>
                        <td><span class="article" id="contCtgNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="table.cp"/></th>
                        <td><span class="article" id="cp"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.genre"/></th>
                        <td><span class="article" id="genreList"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.service"/></th>
                        <td><span class="article" id="serviceList"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.maxPeople"/></th>
                        <td><span class="article" id="maxAvlNop"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.subTitle"/></th>
                        <td><span class="article" id="contsSubTitle"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.detail.explane"/></th>
                        <td><span class="article" id="contsDesc"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.fileType"/></th>
                        <td><span class="article" id="fileType"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.exefile"/></th>
                        <td><span class="article" id="file"></span></td>
                    </tr>
                    <tr class="exeFilePath">
                        <th><spring:message code="contents.table.exeFilePath"/></th>
                        <td><span class="article" id="exeFilePath"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.videoFile"/></th>
                        <td><span class="article" id="videoList"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.metadata"/></th>
                        <td><span class="article" id="metadataName"></span></td>
                    </tr>
                    <tr class="contsInfo d_none">
                        <th><spring:message code="contents.table.information"/></th>
                        <td><span class="article" id="contsInfo"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.cover"/></th>
                        <td><span class="article" id="coverImg" style="display:block"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.thumbnail"/></th>
                        <td><span class="article" id="thumbnailList" style="display:block"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.prev"/></th>
                        <td><span class="article" id="prevList"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.exhibition.Dt"/></th>
                        <td><span class="article" id="cntrctDt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.register.id"/></th>
                        <td><span class="article" id="cretrID"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.registDate"/></th>
                        <td><span class="article" id="cretDt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.update.user.id"/></th>
                        <td><span class="article" id="amdrID"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="contents.table.update.dt"/></th>
                        <td><span class="article" id="amdDt"></span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" onclick="pageMove('/mms/contents')" value="<spring:message code="button.list"/>">
            </div>
            <!-- Button Group E -->

            <!-- popupDetailSubmetadata S -->
            <div id="popupDetailSubmetadata" style="display: none;">
                <div class="popupTitle" id="popupDetailSubmetadataTitle"><spring:message code='submeta.confirm.title' /></div>
                <!-- Table Area S -->
                <div id="submetadataTableArea" class="metadataGrpSubmetadata">
                    <table id="detailSubmetadata">
                    </table>
                </div>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp txtCenter">
                    <input type="button" class="btnNormal btnCancel" onclick="closeMetaLayer()" value="<spring:message code='submeta.button.close'/>" />
                </div>
                <!-- Button Group E -->
            </div>
            <!-- popupDetailSubmetadata E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>