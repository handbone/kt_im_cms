<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<input type="hidden" id="tagSeq" value="${tagSeq}">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js//libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/contents/ContentRegist.js"></script>
<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="4">
<input type="hidden" id="frmtnSeq" value="0">

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
       <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="cms.release.table.regist"/></div>
        </div>

        <!-- formTitleGrp  S -->
        <div id="formTitleGrp">
            <div class="ItemOne">
                <span class="sname"><spring:message code="cms.release.table.svcNm"/></span>
                <span class="name">
                    <select id="svcList" class="inputBox lengthL" onchange="checkedName = ''; storList();">
                    </select>
                </span>
            </div>
            <div class="ItemOne">
                <span class="sname"><spring:message code="table.storeName"/></span>
                <span class="name">
                    <select id="storList" class="inputBox lengthL" onchange="checkedName = ''; getContentsInfo();">
                    </select>
                </span>
            </div>
            <div class="itemTwo">
                <label class="float_l"><spring:message code="cms.release.table.releaseNm"/></label>
                <span class="float_l">
                    <input type="text" class="inputBox lengthL" id="frmtnNm">
                </span>
                <span>
                    <input type="button" value="<spring:message code="cms.release.table.checkId"/>" class="btnNormal btnDouble" onclick="releaseOverlap()" id="overlap"/>
                    <span></span>
                </span>
            </div>
        </div>
        <!-- formTitleGrp E -->

        <!-- content S -->
        <div class="content">

            <!-- harfGrp S -->
            <div id="harfGrp">
                <!-- harf left table S -->
                <div class="divHarf harfLeft">
                    <h1 class="harfTitle"><spring:message code="contents.table.list"/></h1>

                    <!-- table S -->
                    <div class="harfTable">
                        <div class="scrollTable">
                            <table class="titleTable fix_table listBox">
                                <thead>
                                    <tr>
                                       <th class="width50 chkBox"><input type="checkbox" id="contsAllCb" onchange="checkboxAll('conts')"></th>
                                       <th><spring:message code="common.contents.nm"/></th>
                                       <th class="itemW30"><spring:message code="cms.release.table.ctgNm"/></th>
                                    </tr>
                                </thead>
                                <tbody id="selectContents">
                                </tbody>
                            </table>
                        </div>
                        <!-- table E -->
                    </div>
                    </div>
                    <!-- harf left table E -->

                    <!-- button S -->
                    <div class="addBtns">
                        <div class="btnLeftArrow cursorPointer" onclick="deleteContentsInfo()"><img src="<%=request.getContextPath()%>/resources/image/btn_left.gif"></div>
                        <div class="btnRightArrow cursorPointer" onclick="insertContentsInfo()"><img src="<%=request.getContextPath()%>/resources/image/btn_right.gif"></div>
                    </div>
                    <!-- button E -->

                    <!-- harf right table S -->
                    <div class="divHarf harfRight">
                        <h1 class="harfTitle"><spring:message code="contents.table.list"/></h1>
                        <!-- table S -->
                        <div class="harfTable">
                            <div class="scrollTable">
                                <table class="titleTable fix_table listBox">
                                    <thead>
                                        <tr>
                                           <th class="width50 chkBox"><input type="checkbox" id="groupContentsAllCb" onchange="checkboxAll('groupContents')"></th>
                                           <th><spring:message code="common.contents.nm"/></th>
                                           <th class="itemW30"><spring:message code="cms.release.table.ctgNm"/></th>
                                        </tr>
                                    </thead>
                                    <tbody id="selectVRContents">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- table E -->
                    </div>
                    <!-- harf right table E -->
                </div>
                <!-- harfGrp E -->

                <!-- Button Group S -->
                <div class="btnGrp">
                    <input type="button" class="btnNormal btnWrite btnLeft" onclick="applyRelease()" value="<spring:message code="button.create.confirm"/>">
                    <input type="button" class="btnNormal btnCancel btnRight" onclick="pageMove('/cms/release')" value="<spring:message code="button.reset"/>">
                </div>
                <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>