<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<input type="hidden" id="tagSeq" value="${tagSeq}">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js//libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/reservate/ReservateDetail.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="offset" value="1"> 
<input type="hidden" id="limit" value="10">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="reservate.detail.title"/></div>
        </div>
        <!-- page title E --> 

        <!-- content S --> 
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tr>
                        <th class="w150"><spring:message code="reservate.tagNm"/></th>
                        <td><span class="article" id="tagId"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.useYn"/></th>
                        <td><span class="article" id="useYn"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.prodNm"/></th>
                        <td><span class="article" id="prodNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.usageMinute"/></th>
                        <td><span class="article" id="minute"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.rmndCnt"/></th>
                        <td><span class="article" id="rmndCnt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.detail.useCategory"/></th>
                        <td>
                            <ul class="useCategoryCount">
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->
            <br>
            <p class="title"><spring:message code="reservate.detail.uselist"/></p><br>
            <input type="hidden" id="offset" value="0">
            <input type="hidden" id="limit" value="10">

            <!-- Table Area S -->
            <div class="tableArea">
                <div id="gridArea">
                    <table id="jqgridData">
                    </table>
                    <div id="pageDiv"></div>
                </div>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnModify btnLeft cursorPointer" onclick="pageMove('/reservate/${tagSeq}/edit')" value="<spring:message code="button.update"/>">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" onclick="pageMove('/reservate')" value="<spring:message code="button.list"/>">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>