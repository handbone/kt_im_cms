<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<input type="hidden" id="officeSeq" value="${storSeq}">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/reservate/ReservateList.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="offset" value="1">
<input type="hidden" id="limit" value="10">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!--style>
.datepicker{font-size: 13px;    padding: 8px 5px;}
</style-->

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="reservate.list.title"/></div>
        </div>
        <!-- page title E -->

        <!-- Select Box S -->
        <div class="rsvGrpLeft" style="right:0;">
            <div class="rsvSelect">
                <span><spring:message code="reservate.list.seviceNm"/></span>
                <span>
                    <select id="svcSelbox" onchange="storeReload();"></select>
                </span>
                &nbsp;
                <span><spring:message code="reservate.list.storeNm"/></span>
                <span>
                    <select id="storSelbox" onchange="jqGridReload();"></select>
                </span>
            </div>
        </div>
        <!-- Select Box E -->

        <div class="rsvGrp2">
            <div class="rsvSelect">
                <span><spring:message code="reservate.list.searchPeriod"/></span>
                <span>
                    <input type="text" class="lengthXM inputBox2 txtCenter datepicker" id="startDt" name="startDt" readonly> ~
                    <input type="text" value="2018.9.1" class="lengthXM inputBox2 txtCenter datepicker" id="endDt" name="endDt" readonly>
                </span>
                <span>
                    <input type="button" class="btnWhite btnSearchWhite cursorPointer" onclick="keywordSearch();" value="<spring:message code="button.search"/>">
                </span>
            </div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea">
                <!-- jQGrid S -->
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
                <!-- jQGrid E -->
            </div>
            <!-- Table Area E -->

            <div class="CenterGrp absol_btn">
                <div class="cellTable">
                <!--
                    <input type="button" class="btnNormal btnDelete cursorPointer" onclick="reservateInfoDelete()" value="<spring:message code="button.delete"/>">
                 -->
                </div>

                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput"><input type="text" id="keyword"></div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code="button.search"/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->
            </div>
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>