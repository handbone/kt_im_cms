<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<input type="hidden" id="tagSeq" value="${tagSeq}">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js//libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/reservate/ReservateEdit.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="storSeq">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="reservate.edit.title"/></div>
        </div>
        <!-- page title E -->

        <!-- content S --> 
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tr>
                        <th class="w150"><spring:message code="reservate.tagId"/></th>
                        <td><span class="article" id="tagNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.useYn"/></th>
                        <td>
                            <span><input type="radio" name="tagUseSttus" id="useSttusY" value="04"/><label><spring:message code="reservate.useSttus.y"/></label></span>
                            <span><input type="radio" name="tagUseSttus" id="useSttusN" value="05"/><label><spring:message code="reservate.useSttus.n"/></label></span>
                            <span><input type="radio" name="tagUseSttus" id="useSttusE" value="03"/><label><spring:message code="reservate.useSttus.e"/></label></span>
                            <span><input type="radio" name="tagUseSttus" id="useSttusNo" value="01"/><label><spring:message code="reservate.useYn.n"/></label></span>
                            <span><input type="radio" name="tagUseSttus" id="useSttusB" value="02"/><label><spring:message code="reservate.useSttus.b"/></label></span>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.prodNm"/></th>
                        <td><span class="article" id="prodNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.usageMinute"/></th>
                        <td><input type="text" class="inputBox lengthXS txtCenter onlynum" id="m_time_time"><input type="hidden" class="m_time_time" /></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.rmndCnt"/></th>
                        <td><input type="text" class="inputBox lengthXS txtCenter" id="rmndCnt"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="reservate.edit.useCategory"/></th>
                        <td>
                            <ul class="useCategoryCount">
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnModify cursorPointer" value="<spring:message code="button.update.confirm"/>" onclick="reservateInfoUpdate()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="pageMove('/reservate')">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>