<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/reservation/ReservationList.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="3">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" id="type" value="list">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
                <div class="conTitle tabOn"><a href="javascript:;" onclick="tabVal(0,this)"><spring:message code='reservation.table.list'/></a></div>
                <div class="conTitle tabOff tabEnd"><a href="javascript:;" onclick="tabVal(1,this)"><spring:message code='reservation.table.setting'/></a></div>
        </div>
        <!-- tabGrp E -->

        <!-- rsvGrp S -->
        <div class="rsvGrp periodGrp tab1" style="top:56px; border:0; background-color:#fff;" hidden>
            <div class="rsvSelect">
                <span><spring:message code='reservation.table.service'/></span>
                <span>
                    <select class="svcList" onchange="storList()">
                    </select>
                </span>
                &nbsp;
                <span><spring:message code='reservation.table.market'/></span>
                <span>
                    <select class="storList" onchange="storInfo()">
                    </select>
                </span>
            </div>
            <div class="rsvGrp rsvSelect" style="position: relative; top: 0;">
                <input type="hidden" class="year" />
                <span class="itemName"><spring:message code='common.period'/></span>

                <!-- 이전날 버튼 -->
                <span class="buttons">
                    <!--  <img src="<c:url value='/resources/image/btn_cal_monthago.png'/>" style="cursor:pointer" class="prev-month"> -->
                    <img src="<c:url value='/resources/image/icon_left.png'/>" style="cursor:pointer" class="prev-day">
                </span>

                <!--  예약 View 기간 -->
                <span class="inputArea">
                    <input type="text" class="inputBox2 align_c lengthM datepicker" id="startDate" name="startDate" max="9999-12-31T23:59" readonly>
                </span>

                <!--  다음날 버튼 -->
                <span class="buttons" style="padding-right:20px;">
                    <img src="<c:url value='/resources/image/icon_right.png'/>" style="cursor:pointer" class="next-day">
                    <!-- <img src="<c:url value='/resources/image/btn_cal_monthlater.png'/>" style="cursor:pointer" class="next-month">  -->
                 </span>
            </div>
        </div>
        <!-- rsvGrp E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea tab1" hidden>
                <table class="tableList tableReserve">
                    <thead>
                        <tr>
                            <th class="tableTime itemW30"><spring:message code='reservation.table.time'/></th>
                            <th class="tableName imgBasketball itemW35"><spring:message code='reservation.category.basketball'/></th>
                            <th class="tableName imgSoccer itemW35"><spring:message code='reservation.category.soccer'/></th>

                        </tr>
                    </thead>
                    <tbody>
                     </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <div class="tableArea tab2" hidden>
                <table class="tableForm">
                    <tbody>
                        <tr>
                            <th class="width150"><spring:message code='reservation.table.service'/></th>
                            <td>
                                <select class="width150 svcList" onchange="storList()">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='reservation.table.market'/></th>
                            <td>
                                <select class="width150 storList" onchange="storInfo()">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='reservation.table.stTime'/></th>
                            <td>
                                <select class="width50" id="startTimeType" onchange="timeReSet('start')">
                                    <option value="morning"><spring:message code='reservation.table.moring'/></option>
                                    <option value="afternoon"><spring:message code='reservation.table.afterNoon'/></option>
                                </select>
                                <select class="width50" id="startHour">
                                </select> <spring:message code='reservation.table.hour'/>
                                <select class="width50" id="startMinute">
                                </select> <spring:message code='reservation.table.minute'/>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='reservation.table.endTime'/></th>
                            <td>
                                <select class="width50" id="endTimeType" onchange="timeReSet('end')">
                                    <option value="morning"><spring:message code='reservation.table.moring'/></option>
                                    <option value="afternoon"><spring:message code='reservation.table.afterNoon'/></option>
                                </select>
                                <select class="width50" id="endHour">
                                </select> <spring:message code='reservation.table.hour'/>
                                <select class="width50" id="endMinute">
                                </select> <spring:message code='reservation.table.minute'/>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='reservation.table.timeCycle'/></th>
                            <td>
                                <select class="width100" id="cycleBox">
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <!-- Button Group S -->
                <div class="btnGrp txtCenter tab2" hidden>
                    <input type="button" class="btnNormal btnSave" value="<spring:message code='button.save'/>" onclick="settingSave();">
                    <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>" onclick="$('.tabOff a').trigger('click')">
                </div>
                <!-- Button Group E -->

            </div>
        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->

    <!-- Popup S -->
    <div id="popupWinidsm" style="height:100%;top: -webkit-calc(50% - 250px); top: -moz-calc(50% - 250px); top: calc(50% - 250px);">
        <form id="reservation">
            <input type="hidden" name="devCtgNm" id="devCtgNm" />
            <input type="hidden" name="usageDate" id="usageDate" />
            <input type="hidden" name="resveSeq" id="resveSeq" />
            <input type="hidden" name="storSeq" id="storSeq">

            <div class="popupTitle"><spring:message code='table.reservation'/></div>
            <!-- Table Area S -->
            <div class="popupContent">
                <ul>
                    <li class="align_l lineBottom">
                        <span class="nm">Device : </span>
                        <span class="co"><span class="article devCtgNm"></span></span>
                    </li>
                    <li class="align_l lineBottom">
                        <span class="nm" hidden><spring:message code='reservation.table.custom'/> : </span>
                        <span class="co">
                            <input type="text" class="inputBox itemW100 remaining" id="cstmrNm" name="cstmrNm" placeholder="<spring:message code='reservation.table.custom'/>" max="10">
                            <span></span>
                        </span>

                    </li>
                    <li class="align_l lineBottom">
                        <span class="nm" hidden><spring:message code='reservation.table.telNo'/> : </span>
                        <span class="co">
                            <input type="text" class="inputBox itemW100 only_num remaining" id="cstmrTelNo" name="cstmrTelNo" max="13" placeholder="<spring:message code='reservation.table.telNo'/>">
                            <span></span>
                            <span id= "cstmrTelNo"></span>
                        </span>
                    </li>
                    <li class="align_l lineBottom" id="reserveView" hidden>
                        <span class="nm" hidden><spring:message code='reservation.table.reservation.time'/> : </span>
                        <span class="co">
                            <span><span class="month"></span><spring:message code='reservation.table.month'/> <span class="day"></span><spring:message code='reservation.table.day'/> <span class="timeText"></span></span>
                        </span>
                    </li>
                    <li class="align_l lineBottom">
                        <div class="txtPrivacy align_l" id="policy"></div>
                    </li>
                    <li class="agree align_l lineBottom">
                        <span>
                            <label for="agree" style="font-weight:100">
                                <input type="checkbox" id="agree" name="agree">
                                                <spring:message code='reservation.table.private.agree' />
                            </label>
                        </span>
                    </li>
                    <li class="msg txtReserveQuest lineBottom" style="border:0;">
                        <span><span class="month"></span><spring:message code='reservation.table.month' /> <span class="day"></span><spring:message code='reservation.table.day' /> <span class="timeText"></span><span class="stateMsg"><spring:message code='reservation.confirm.insert' /></span></span>
                    </li>
                </ul>
            </div>
                <!-- Table Area E -->
            <div class="btnGrp btnCenter2">
                <input type="button" class="btnNormal btn btnOk" value="<spring:message code='common.confirm' />" onclick="insertReservate()">
                <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset' />" onclick="closePopup()">
            </div>
        </form>
    </div>
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>