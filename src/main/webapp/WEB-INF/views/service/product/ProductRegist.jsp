<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/product/ProductRegist.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="1">
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="storSeq" value="${storSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="product.regist.title"/></div>
        </div>
        <!-- page title E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tr>
                        <th class="w150"><spring:message code="product.comn.use.prodNm"/></th>
                        <td><input type="text" class="inputBox lengthXXL remaining" id="prodNm" max="30"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.seviceNm"/></th>
                        <td>
                            <select id="svcSelbox" class="inputBox lengthL" onchange="storeReload();"></select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.storeNm"/></th>
                        <td>
                            <select id="storSelbox" class="inputBox lengthL" onchange="categoryReload();"></select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.tot.limit.count"/></th>
                        <td>
                            <select class="inputBox lengthL" id="prodLmtCnt" onchange="chkProdLmtCnt()">
                                <option value="-1"><spring:message code="common.cannot.used"/></option>
                                <option value="0"><spring:message code="common.unlimit.uncount.msg"/></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="userSet"><spring:message code="common.userSet"/></option>
                            </select>
                            <span id="userSetProdLmtCntBox">
                                <input type="text" class="inputBox lengthM onlynum remaining bizno" id="userProdLmtCnt" max="4" style="width: 185px !important; text-align: right;">
                                <span>
                                    <spring:message code="common.cnt"/>
                                </span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.use.time.limit"/></th>
                        <td>
                            <select class="inputBox lengthL" id="prodTimeLmt" onchange="uncountSel()">
                                <option value="0"><spring:message code="common.unlimit.uncount.msg"/></option>
                                <option value="30">30 <spring:message code="common.minute"/></option>
                                <option value="60">60 <spring:message code="common.minute"/></option>
                                <option value="100">100 <spring:message code="common.minute"/></option>
                                <option value="120">120 <spring:message code="common.minute"/></option>
                                <option value="180">180 <spring:message code="common.minute"/></option>
                                <option value='userSet'><spring:message code="common.userSet"/></option>
                            </select>
                            <span id="userSetBox">
                                &nbsp;
                                <input type="text" class="inputBox lengthM onlynum remaining bizno" id="userSet" max="4" style="width: 185px !important; text-align: right;">
                                &nbsp;
                                <span>
                                    <spring:message code="common.minute"/>
                                </span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.price"/></th>
                        <td><input type="text" class="inputBox lengthL remainingPrc" max="9" id="prodPrc"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.desc"/></th>
                        <td><input type="text" class="inputBox lengthXXL remaining" max="100" id="prodDesc"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.use.device"/></th>
                        <td>
                            <ul class="prodCategory">
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.prod.gsr.code"/></th>
                        <td><input type="text" class="inputBox lengthXXL remaining" max="13" id="gsrProdCode"></td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnWrite cursorPointer" onclick="productRegist();" value="<spring:message code="button.create.confirm"/>">
                <input type="button" class="btnNormal btnCancel cursorPointer" onclick="pageMove('/product')" value="<spring:message code="button.reset"/>">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>