<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/product/ProductDetail.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="1">
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="storSeq" value="${storSeq}">
<input type="hidden" id="prodSeq" value="${prodSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="product.detail.title"/></div>
        </div>
        <!-- page title E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code="product.comn.seviceNm"/></th>
                        <td><span class="article" id="svcNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.storeNm"/></th>
                        <td><span class="article" id="storNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.use.prodNm"/></th>
                        <td><span class="article" id="prodNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.tot.limit.count"/></th>
                        <td><span class="article" id="prodLmtCnt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.use.time.limit"/></th>
                        <td><span class="article" id="prodTimeLmt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.price"/></th>
                        <td><span class="article" id="prodPrc"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.desc"/></th>
                        <td><span class="article" id="prodDesc"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.use.device"/></th>
                        <td>
                            <ul class="prodCtgSet">
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.prod.gsr.code"/></th>
                        <td><span class="article" id="gsrProdCode"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="product.comn.useyn"/></th>
                        <td><span class="article" id="useYn"></span></td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnModify btnLeft cursorPointer" onclick="pageMove('/product/${prodSeq}/edit')" value="<spring:message code="button.update"/>">
                <input type="button" class="btnNormal btnDelete btnLeft cursorPointer" onclick="productDelete()" value="<spring:message code="button.delete"/>">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" onclick="pageMove('/product')" value="<spring:message code="button.list"/>">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>