<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/store/StoreList.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" id="offset" value="1"> 
<input type="hidden" id="limit" value="10">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="store.list.title"/></div>
        </div>
        <!-- page title E -->

        <!-- Select Box S -->
        <div class="rsvGrp">
            <div class="rsvSelect">
                <span><spring:message code="store.comn.serviceNm"/></span>
                <span>
                    <select id="svcSelbox" onchange="jqGridReload()"></select>
                </span>
            </div>
        </div> 
        <!-- Select Box E -->

        <!-- content S --> 
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea">
                <!-- jQGrid S -->
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
                <!-- jQGrid E -->
            </div>
            <!-- Table Area E -->

            <div class="CenterGrp absol_btn">
                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code="button.search"/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->

                <div class="cellTable">
                    <input type="button" class="btnNormal btnWrite cursorPointer" id="btn_regist" value="<spring:message code="button.create"/>" onclick="moveToRegistView()">
                </div>
            </div>
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>