<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/store/StoreDetail.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" id="storSeq" value="${storSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="store.detail.title"/></div>
        </div>
        <!-- page title E -->

        <span class="title" id="storNm"></span>
        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code="store.comn.serviceNm"/></th>
                        <td><span class="article" id="svcNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.tel.no"/></th>
                        <td><span class="article" id="storTelNo"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.address"/></th>
                        <td><span class="article" id="storAddr"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.code"/></th>
                        <td><span class="article" id="storCode"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.register.id"/></th>
                        <td><span class="article" id="cretrId"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.regist.date"/></th>
                        <td><span class="article" id="cretDt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.updater.id"/></th>
                        <td><span class="article" id="amdrId"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.update.date"/></th>
                        <td><span class="article" id="amdDt"></span></td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code="button.update"/>" onclick="cofirmMove('/store/${storSeq}/edit')">
                <input type="button" class="btnNormal btnList btnRight" value="<spring:message code="button.list"/>" onclick="pageMove('/store')">
            </div>
            <!-- Button Group E -->
        </div>

    </div>
    <!-- content E -->

</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>