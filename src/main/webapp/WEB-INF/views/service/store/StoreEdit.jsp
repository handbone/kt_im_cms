<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/store/StoreEdit.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/daum/postcode.v2.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/daum/180326.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" id="storSeq" value="${storSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="store.update.title"/></div>
        </div>
        <!-- page title E -->

        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tr>
                        <th class="w150"><spring:message code="store.comn.serviceNm"/></th>
                        <td><span class="article" id="svcNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.name"/></th>
                        <td><input type="text" class="inputBox lengthXL remaining" id="storeName" max="25" min="0"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.tel.no"/></th>
                        <td>
                            <input type="text" class="inputBox lengthXS telNo onlynum remaining" max="4" min="2"> -
                            <input type="text" class="inputBox lengthS telNo onlynum remaining" max="4" min="3"> -
                            <input type="text" class="inputBox lengthS telNo onlynum remaining" max="4" min="4">
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.address.store"/></th>
                        <td>
                            <span>
                                <input type="text" id="zipcode" name="zipcode" class="inputBox lengthS remaining" max="7">
                                <input type="button" class="btnSearchLongWhite btnWhite" value="<spring:message code="button.zipcode"/>" onclick="javascript:openDaumPostcode('zipcode', 'basAddr', 'dtlAddr');">
                            </span>
                            <br /><input type="text" class="inputBox lengthXXL marginTB4 remaining" max="100" id="basAddr" name="basAddr">
                            <br /><input type="text" class="inputBox lengthXXL marginTB4 remaining" max="100" id="dtlAddr">
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.code"/></th>
                        <td><input type="text" class="inputBox lengthL remaining" id="storeCode" max="5" min="5"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="store.comn.useyn"/></th>
                        <td>
                            <input type="radio" name='useYn' value='Y' /><label><spring:message code="common.enable"/></label>&nbsp; 
                            <input type="radio" name='useYn' value='N' /><label><spring:message code="common.disable"/></label>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnModify cursorPointer" value="<spring:message code="button.update.confirm"/>" onclick="storeUpdate()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="pageMove('/store')">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>