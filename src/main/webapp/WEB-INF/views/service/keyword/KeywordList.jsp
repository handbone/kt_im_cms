<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/service/keyword/KeywordList.js"></script>
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />
<style>
    table{background:white; border:0px solid black;}
    table th{background:white; border:0px solid black;}
    table td{background:white; border:0px solid black;}
</style>

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

                <!-- locate S -->
                <div id="locateGrp">
                    <div class="locate">
                        <ul>
                        </ul>
                    </div>
                </div>
                <!-- locate E -->

                <!-- page title S -->
                <div class="conTitleGrp">
                    <div class="conTitle"><spring:message code='menu.service.searchKeyword'/></div>
                </div>
                <!-- page title E -->

                    <!-- Table Area S -->
                    <div class="tableArea">

                        <table class="tableList">
                            <tr>
                                <th class="itemW10"><spring:message code='common.service'/></th>
                                <td class="txtLeft">
                                    <select id="svcSelbox" class="inputBox lengthM" onchange="keywordList()">
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <th><spring:message code='common.Classification'/></th>
                                <td class="txtLeft"><spring:message code='common.keyword.recommand'/></td>
                                <td></td>
                            </tr>
                            <tr style="font-weight:bold">
                                <th></th>
                                <td style="background: #f9f9f9;"><spring:message code='common.keyword.name'/></td>
                                <td style="background: #f9f9f9;"><spring:message code='common.order'/></td>
                            </tr>
                            <tr>
                                <th style="vertical-align:top;" rowspan=8><spring:message code='common.keyword'/></th>
                                <td style="width:100%;">
                                    <div class="clearInput_box">
									    <input class="inputBox itemW100 txtCenter remaining clearInput" type="text" max="10" onkeyup="this.setAttribute('value', this.value);" value="">
									    <span class="clearBtn">X</span>
									</div>
                                </td>
                                <td>
                                    <span class="cursorPointer" onclick=""><img src="<%=request.getContextPath()%>/resources/image/btn_tup_disable.png"></span>
                                    <span class="cursorPointer" onclick="rankDown(this)"><img src="<%=request.getContextPath()%>/resources/image/btn_tdown.png"></span>
                                </td>
                            </tr>
                            <c:forEach var="i" begin="1" end="7" step="1">
                            <c:set var="contextPath" value="<%=request.getContextPath()%>" />
                            <tr>
                                <td>
                                    <div class="clearInput_box">
                                        <input class="inputBox itemW100 txtCenter remaining clearInput" type="text" max="10" onkeyup="this.setAttribute('value', this.value);" value="">
                                        <div class="clearBtn"><span>X</span></div>
                                    </div>
                                </td>
                                <td style="white-space: nowrap;">
                                    <c:choose>
                                    <c:when test="${i == 7}">
                                        <span class="cursorPointer" onclick="rankUp(this)"><img src="${contextPath }/resources/image/btn_tup.png"></span>
                                        <span class="cursorPointer"><img src="${contextPath }/resources/image/btn_tdown_disable.png"></span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="cursorPointer" onclick="rankUp(this)"><img src="${contextPath }/resources/image/btn_tup.png"></span>
                                        <span class="cursorPointer" onclick="rankDown(this)"><img src="${contextPath }/resources/image/btn_tdown.png"></span>
                                    </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            </c:forEach>

                        </table>
                        <br>
                        <div class="btnGroup txtCenter" id="rcmdBtnGrp" style="display: block;">
			                <input type="button" class="btnNormal btnWrite" value="등록완료" onclick="registKeywordList()">
			                <input type="button" class="btnNormal btnCancel" value="취소" onclick="keywordList()">
			            </div>

                    </div>
                    <!-- Table Area E -->
            </div>
            <!-- content E -->
        </div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>