<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/left_menu.jsp" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/banr/banrMng.js"></script>

<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="memberSec" value="${memberSec}">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

            <!-- locate S -->
            <div id="locateGrp">
                <div class="locate">
                    <ul>
                    </ul>
                </div>
            </div>
            <!-- locate E -->

            <!-- page title S -->
            <div class="conTitleGrp">
                <div class="conTitle tabOn"><a href="javascript:;"><spring:message code="menu.online.banr.mng"/></a></div>
                <div class="conTitle tabOff tabEnd"><a href="javascript:pageMove('/onlineService/cprtSvc');"><spring:message code="menu.online.banr.cprtSvcMng"/></a></div>
                <!--
                <div class="conTitle tabOff"><a href="javascript:;"><spring:message code="menu.online.banr.cprtSvcMng"/></a></div>
                <div class="conTitle tabEnd"><spring:message code="menu.online.banr.plcyMng"/></div>
                 -->
            </div>
            <!-- page title E -->
            <div class="rsvGrp">
                <div class="rsvSelect">
                    <span><spring:message code="service.table.name"/></span>
                    <span>
                        <select id="svcList" onchange="pageReset(); banrInPlcyList();" class="auto"></select>
                    </span>
                    &nbsp;
                    <span><spring:message code="banr.position.noSpace"/></span>
                    <span>
                        <select id="banrInPlcyList" onchange="keywordSearch()" class="auto"></select>
                    </span>
                    &nbsp;
                    <span><spring:message code="table.status"/></span>
                    <span>
                        <select id="statusList" onchange="keywordSearch()" class="auto">
                            <option value=""><spring:message code="select.all"/></option>
                            <option value="WAIT"><spring:message code="common.sttus.wait"/></option>
                           <option value="DISPLAY"><spring:message code="common.sttus.display"/></option>
                          <option value="OVER_OUT"><spring:message code="common.sttus.overOut"/></option>
                            <option value="STOP"><spring:message code="common.sttus.stop"/></option>
                        </select>
                    </span>
                </div>
            </div>

	        <!-- content S -->
	        <div class="content">
	            <!-- Table Area S -->
                <div id="gridArea" class="">
                    <table id="jqgridData"></table>
                    <div id="pageDiv"></div>
                </div>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp">
                    <!-- Search Group S -->
                    <div class="searchBox">
                        <div class="searchGrp">
                            <div class="selectBox">
                                <select id="target"></select>
                            </div>
                            <div class="searchInput"><input type="text" id="keyword"></div>
                            <div class="searchBtn"><input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code='button.search'/>"></div>
                        </div>
                    </div>
                    <!-- Search Group E -->

                    <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='button.delete'/>" onclick="deleteBanr()">
                    <input type="button" class="btnNormal btnWrite btnRight2" value="<spring:message code='button.create'/>" onclick="makePageMove(this)">
                </div>
                <!-- Button Group E -->
	        </div>
        <!-- content E -->

        </div>
        <!-- right contents E -->
        <br>
        <!-- Popup Window S 미리보기 팝업 -->
	    <div id="popupPreView" style="display:none;" class="popupWrap preView"><div class="popupSection">
            <input type="button" class="topClose" onclick="closePopups()">
	        <div class="popupContent">
	            <img id="popupImg">
	        </div>
	        <!-- Button Group S -->
	        <div class="btnGrp btnCenter">
	        <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset' />" onclick="closePopups()">
	        </div>
	        <!-- Button Group E -->
	    </div></div>
        <!-- Popup Window E 미리보기 팝업 -->
    </div>

<%@include file="/WEB-INF/views/frame/footer.jsp" %>