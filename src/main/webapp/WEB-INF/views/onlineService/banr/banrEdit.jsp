<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/left_menu.jsp" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/banr/banrEdit.js"></script>
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="banrSeq" value="${banrSeq}">
<input type="hidden" id="memberSec" value="${memberSec}">

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">
                <div class="conTitleGrp">
                    <div class="conTitle"><spring:message code="banr.campain.edit"/></div>
                </div>
                <form id="comnty_write_from" name="comnty_write_from" enctype="multipart/form-data" method="post">
                <!-- content S -->
                <div class="content">
                    <!-- Table Area S -->
                    <div class="tableArea">
                        <table class="tableForm">
                            <tr>
                                <th class="w150"><spring:message code="common.service"/></th>
                                <td id="svcNm"></td>
                            </tr>
                            <tr>
                                <th><spring:message code="banr.campainNm"/></th>
                                <td id="banrCampNm"></td>
                            </tr>
                            <tr>
                                <th class="w150"><spring:message code="banr.position"/></th>
                                <td id="terrNm"></td>
                            </tr>
                            <tr>
                                <th><spring:message code="common.image.regist"/></th>
                                <td id="banrImg">
                                </td>
                            </tr>
                            <tr>
                                <th><spring:message code="common.preview"/></th>
                                <td>
                                    <div id="imgPreview" class="imgPreview"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>URL</th>
                                <td>
                                    <input type="text" class="inputBox lengthXL remaining" placeholder="주소를 입력해 주세요" id="linkUrl" max="300">
                                 </td>
                            </tr>
                            <tr>
                                <th>URI</th>
                                <td>
                                    <input type="text" class="inputBox lengthXL remaining" placeholder="주소를 입력해 주세요" id="linkUri" max="300">
                                 </td>
                            </tr>
                            <tr>
                                <th><spring:message code="common.btnNm"/></th>
                                <td>
                                    <input type="text" class="inputBox lengthXL remaining" placeholder="버튼명을 입력하세요." max="100" id="btnTitle">&nbsp;
                                    <input type="checkbox" checked name="btnTitleChk" id="btnTitleChk"><label for="btnTitleChk"><spring:message code="common.use"/></label>
                                </td>
                            </tr>
                            <tr>
                                <th><spring:message code="common.ek.title"/></th>
                                <td>
                                    <input type="text" class="inputBox lengthXL remaining" placeholder="타이틀을 입력하세요." max="100" id="banrTitle">&nbsp;
                                    <input type="checkbox" checked name="banrTitleChk" id="banrTitleChk"><label for="banrTitleChk"><spring:message code="common.use"/></label>
                                </td>
                            </tr>
                            <tr>
                                <th><spring:message code="common.contents.sub.title.space"/></th>
                                <td>
                                    <input type="text" class="inputBox lengthXL remaining" placeholder="서브 타이틀을 입력하세요." max="300" id="banrSubTitle">&nbsp;
                                    <input type="checkbox" checked name="banrSubTitleChk" id="banrSubTitleChk"><label for="banrSubTitleChk"><spring:message code="common.use"/></label>
                                </td>
                            </tr>
                            <tr>
                                <th><spring:message code="common.display.period"/></th>
                                <td>
                                    <input type="radio" checked name="perdYn" value="N"><label><spring:message code="common.unlimit.word"/></label>&nbsp;
                                    <input type="radio" name="perdYn" value="Y"><label><spring:message code="common.period.set"/></label>&nbsp;

                                    <input type="text" class="inputBox width150 txtCenter datepicker" id="stDt" name="perdDt" readonly="" disabled>
                                    ~
                                    <input type="text" class="inputBox width150 txtCenter datepicker" id="fnsDt" name="perdDt" readonly="" disabled>
                                </td>
                            </tr>
                            <tr>
                                <th><spring:message code="common.display.useYn.noSpace"/></th>
                                <td>
                                    <input type="radio" checked name="useYn" value="Y"><label><spring:message code="common.sttus.display"/></label>&nbsp;
                                    <input type="radio" name="useYn" value="N"><label><spring:message code="common.sttus.stop"/></label>
                                </td>
                            </tr>
                        </table>
                        <!-- Table Area E -->
                        <!-- Button Group S -->
                        <div class="btnGrp txtCenter">
                            <input type="button" class="btnNormal btnModify" value="<spring:message code='button.update.confirm'/>" onclick="banrEdit()">
                            <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>" onclick="makePageMove(this);">
                        <div>
                        <!-- Button Group E -->


                    </div>
                    </div>

                </div>
            <!-- Button Group E -->
            </div>
            </form>
            <!-- content E -->

        </div>
        <!-- right contents E -->
    </div>
        <!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp" %>