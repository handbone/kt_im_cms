<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/banr/banrDetail.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/libs/swipebox/swipebox.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swipebox/jquery.swipebox.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/justifiedGallery/justifiedGallery.min.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<link href="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.js"></script>


<input type="hidden" id="banrSeq" value="${banrSeq}">
<input type="hidden" id="plcySeq">
<input type="hidden" id="svcSeq">


    <!-- Contents S -->
    <div id="contents">
        <!-- boxRound S -->
        <div class="boxRound">
            <!-- locate S -->
            <div id="locateGrp">
                <div class="locate">
                    <ul>
                    </ul>
                </div>
            </div>
            <!-- locate E -->

	        <div class="conTitleGrp">
	            <div class="conTitle"><spring:message code="banr.campain.detail"/></div>
	        </div>

	        <!-- content S -->
	        <div class="content">
	            <!-- Table Area S -->
	            <div class="tableArea">
	                <table class="tableDetail">
	                    <tbody>
	                        <tr>
	                            <th class="w150"><spring:message code="common.service"/></th>
	                            <td id="svcNm"></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="banr.campainNm"/></th>
	                            <td id="banrCampNm"></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="banr.position"/></th>
	                            <td id="terrNm"></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.image.regist"/></th>
	                            <td id="fileNm"></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.preview"/></th>
	                            <td class="imgPreview">
	                               <div id="imgPreview"></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <th class="w150">URL</th>
	                            <td>
	                                <span id="linkUrl"></span>
	                            </td>
	                        </tr>
	                        <tr>
                                <th class="w150">URI</th>
                                <td>
                                    <span id="linkUri"></span>
                                </td>
                            </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.btnNm"/></th>
	                            <td><div class="setInpChk"> <span id="btnTitle"></span><span class="chk"><input type="checkbox" name="btnTitleChk" disabled><label><spring:message code="button.use"/></label></span></div></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.ek.title"/></th>
	                            <td><div class="setInpChk"><span id="banrTitle"></span><span class="chk"><input type="checkbox" name="banrTitleChk" disabled><label><spring:message code="button.use"/></label></span></div></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.contents.sub.title.space"/></th>
	                            <td><div class="setInpChk"><span id="banrSubTitle" ></span><span class="chk"><input type="checkbox" name="banrSubTitleChk" disabled><label><spring:message code="button.use"/></label></span></div></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.display.period"/></th>
	                            <td><input type="radio" name="perdYn" value="N" disabled><label><spring:message code="common.unlimit.word"/></label>&nbsp; <input type="radio" name="perdYn" value="Y" disabled><label><spring:message code="common.period.set"/></label>&nbsp; <input type="text" class="inputBox width150 txtCenter" id="stDt" disabled> ~ <input type="text" class="txtCenter inputBox width150" id="fnsDt" disabled></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.display.useYn.noSpace"/></th>
	                            <td><input type="radio" name="useYn" value="Y" disabled><label><spring:message code="common.sttus.display"/></label>&nbsp; <input type="radio" name="useYn" value="N" disabled><label><spring:message code="common.sttus.stop"/></label></td>
	                        </tr>
	                    </tbody>
	                </table>
	            </div>
	            <!-- Table Area E -->

	            <!-- Button Group S -->
	            <div class="btnGrp">
	                    <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='button.delete'/>" onclick="deleteBanr()">
	                    <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code='button.update'/>" onclick="pageMove('/onlineService/banr/edit/${banrSeq}')">
	                    <input type="button" class="btnNormal btnList btnRight" value="<spring:message code='button.list'/>" onclick="pageMove('/onlineService/banr')">
	            </div>
	            <!-- Button Group E -->
	        </div>
	        <!-- content E -->
	    </div>
	    <!-- boxRound E -->
	</div>
	<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>