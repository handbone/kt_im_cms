<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/left_menu.jsp" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/cprt/cprtEdit.js"></script>
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="cprtSvcSeq" value="${cprtSvcSeq}">
<input type="hidden" id="memberSec" value="${memberSec}">

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">
                <div class="conTitleGrp">
                    <div class="conTitle"><spring:message code="banr.cprtSvc.edit"/></div>
                </div>
                <form id="comnty_write_from" name="comnty_write_from" enctype="multipart/form-data" method="post">
                <!-- content S -->
                <div class="content">
                    <!-- Table Area S -->
                    <div class="tableArea">
	                    <table class="tableForm">
	                        <tr>
                                <th class="w150"><spring:message code="common.service"/></th>
                                <td id="svcNm"></td>
                            </tr>
	                        <tr>
                                <th class="w150"><spring:message code="banr.cprtSvcNm.space"/></th>
                                <td id="cprtSvcNm"></td>
                            </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="banr.menu.URL"/></th>
	                            <td><input type="text" class="inputBox lengthXL remaining" id="menuUrl" max="300"></td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.image.regist"/></th>
	                            <td>
	                                <div id="cprtImg">

	                                </div>
	                                <!-- <span>* 900 X 600, 3M 용량 제한, JPG, png</span>
	                                <p>해당 영역 사이즈, 크기 규격에 맞지 않음</p> -->
	                            </td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.image.thumbnail"/></th>
	                            <td>
	                               <div id="cprtThumImg">

                                    </div>
	                               <!-- <input type="file" class="width400">&nbsp;<span>* 300 X 300, 3M 용량 제한, JPG, png</span>-->
                                </td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.preview"/></th>
	                            <td class="imgPreview double">
	                                <div id="imgPreview"></div>
	                                <div id="imgThumPreview"></div>
	                            </td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.display.period"/></th>
	                            <td>
                                    <input type="radio" checked name="perdYn" value="N"><label><spring:message code="common.unlimit.word"/></label>&nbsp;
                                    <input type="radio" name="perdYn" value="Y"><label><spring:message code="common.period.set"/></label>&nbsp;
                                    <input type="text" class="inputBox width150 txtCenter datepicker" id="stDt" name="perdDt" readonly="" disabled>
                                    <input type="text" class="inputBox width150 txtCenter datepicker" id="fnsDt" name="perdDt" readonly="" disabled>
                                </td>
	                        </tr>
	                        <tr>
	                            <th class="w150"><spring:message code="common.useYn"/></th>
	                            <td>
                                    <input type="radio" checked name="useYn" value="Y"><label><spring:message code="common.sttus.display"/></label>&nbsp;
                                    <input type="radio" name="useYn" value="N"><label><spring:message code="common.sttus.stop"/></label>
                                </td>
	                        </tr>
	                    </table>
                    </div>
                    <!-- Table Area E -->

                    <!-- Button Group S -->
	                <div class="btnGrp txtCenter">
	                    <input type="button" class="btnNormal btnModify" value="<spring:message code='button.save'/>" onclick="cprtSvcEdit()">
                        <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>" onclick="makePageMove(this);">
	                </div>
                    <!-- Button Group E -->
                </div>
                </form>
            <!-- content E -->

            </div>
            <!-- right contents E -->
        </div>
        <!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp" %>