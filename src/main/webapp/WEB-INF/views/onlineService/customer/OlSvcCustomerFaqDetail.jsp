<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/customer/OlSvcCustomerFaqDetail.js"></script>
<input type="hidden" id="svcFaqSeq" value="${svcFaqSeq}" />
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="memberSvcSeq" value="${svcSeq}" />
<input type="hidden" id="currSvcSeq" value="${currSvcSeq}" />

<style>
#sbst img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.online.faq.detail.title"/></div>
        </div>
        <!-- Tab E -->
        
        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <colgroup>
                        <col style="width:150px">
                        <col>
                        <col style="width:150px">
                        <col>
                    </colgroup>
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="service.common.register.name"/></th>
                            <td id="regNm" colspan="3"></td>
                        </tr>
                        <tr>
                            <th class="w150"><spring:message code="service.common.service.name"/></th>
                            <td id="svcNm"></td>
                            <th class="w150"><spring:message code="common.regdate"/></th>
                            <td id="regDt"></td>
                        </tr>
                        <tr>
                            <th class="w150"><spring:message code="service.common.target.type"/></th>
                            <td id="targetType" colspan="3"></td>
                        </tr>
                        <tr>
                            <th class="w150"><spring:message code="common.category"/></th>
                            <td id="faqCtg" colspan="3"></td>
                        </tr>
                        <tr>
                            <th class="w150"><spring:message code="service.faq.title.and.question"/></th>
                            <td id="title" colspan="3"></td>
                        </tr>
                        <tr>
                            <th class="w150"><spring:message code="service.faq.content.and.answer"/></th>
                            <td id="sbst" colspan="3" style="line-height:initial"><div></div></td>
                        </tr>
                        <tr>
                            <th class="w150"><spring:message code="common.useYn.noSpace"/></th>
                            <td colspan="3">
                                <input type="radio" value='Y' name='useYn' disabled>
                                <label>
                                    <spring:message code="common.sttus.display"/>
                                </label>
                                &nbsp;
                                <input type="radio" value='N' name='useYn' disabled>
                                <label>
                                    <spring:message code="common.sttus.stop"/>
                                </label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" style="display:none" id="btnModify" class="btnNormal btnModify btnLeft cursorPointer" value="<spring:message code="button.update"/>" onclick="moveToEdit()">
                <input type="button" style="display:none"id="btnDelete" class="btnNormal btnDelete btnLeft cursorPointer" value="<spring:message code="button.delete"/>" onclick="deleteFaq()">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" value="<spring:message code="button.list"/>" onclick="pageMove('/onlineSvc/customer#page=1&display=faq&svcSeq=${currSvcSeq}')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>