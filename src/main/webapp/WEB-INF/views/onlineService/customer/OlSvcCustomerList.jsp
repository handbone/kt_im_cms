<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/customer/OlSvcCustomerList.js"></script>

<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="13">
<input type="hidden" id="display" value="notice">
<input type="hidden" id="memberSec" value="${memberSec}">
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle tabOn"><a href="javascript:;" onclick="tabVal(0,this)"><spring:message code="common.notice.manage.title"/></a></div>
            <div class="conTitle tabOff tabEnd"><a href="javascript:;" onclick="tabVal(1,this)"><spring:message code="common.faq.manage.title"/></a></div>
        </div>
        <!-- page title E -->

        <!-- Select Box S -->
        <div class="rsvGrp">
            <div class="rsvSelect">
                <span><spring:message code="common.service.name"/></span>
                <span>
                    <select id="serviceList" onchange="updateList()" class="auto"></select>
                </span>
            </div>
        </div> 
        <!-- Select Box E -->

        <!-- content S --> 
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea" class="">
                <table id="jqgridOlSvcNoticeData"></table>
                <div id="noticePageDiv"></div>
                <table id="jqgridFaqData"></table>
                <div id="faqPageDiv"></div>
            </div>
            <!-- Table Area E -->

            <div class="CenterGrp">
                <div class="cellTable">
                    <input type="button" id="btnDelete" class="btnNormal btnDelete cursorPointer" onclick="deletePost()" value="<spring:message code="button.delete"/>">
                </div>

                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select class="selectBox" id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="검색">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->

                <div class="cellTable">
                    <input type="button" id="btnCreate" class="btnNormal btnWrite cursorPointer" onclick="createPost()" value="<spring:message code="button.create"/>">
                </div>
            </div>
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>