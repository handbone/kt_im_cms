<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/swfupload/video_handlers.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/customer/OlSvcCustomerNoticeEdit.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/ckeditor/ckeditor.js"></script>

<input type="hidden" id="currSvcSeq" value="${currSvcSeq}">
<input type="hidden" id="svcNoticeSeq" value="${svcNoticeSeq}">
<input type="hidden" id="memberNm" value="${name}" />
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="id" value="${id}" />
<input type="hidden" id="otpCreateTime" value="${otpCreateTime}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.online.notice.edit.title"/></div>
        </div>
        <!-- Tab E -->
        <form id="fileForm">
            <input type="hidden" id="contsSeq" name="contsSeq"> <input
                type="hidden" id="fileSe" name="fileSe"> <input
                type="hidden" id="orginlFileNm" name="orginlFileNm"> <input
                type="hidden" id="fileDir" name="fileDir"> <input
                type="hidden" id="fileSize" name="fileSize"> <input
                type="hidden" id="fileExt" name="fileExt"> <input
                type="hidden" id="streFileNm" name="streFileNm">
        </form>

        <!-- content S -->
        <form id="comnty_write_from" name="comnty_write_from" enctype="multipart/form-data" method="post">
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code="service.common.register.name"/></th>
                        <td id="regNm"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.service.name"/></th>
                        <td>
                            <select class="inputBox width200" id="serviceList" onchange="chgService()">
                            </select>
                        </td>
                    </tr>
                    <tr id="dispType" style="display:none;">
                        <th><spring:message code="service.common.display.type"/></th>
                        <td>
                            <select class="inputBox width200" id="dispTypeList">
                                <option value='ALL'><spring:message code="common.app.type.all"/></option> <!-- 전체 -->
                                <option value='SA'><spring:message code="common.app.type.service"/></option> <!-- 서비스앱 -->
                                <option value='MA'><spring:message code="common.app.type.mirror"/></option> <!-- 미러링앱 -->
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.target.type"/></th>
                        <td>
                            <select class="inputBox width200" id="targetTypeList">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.important"/></th>
                        <td>
                            <select class="inputBox width200" id="rankList">
                                <option value='0'>0</option> <!-- 없음 -->
                                <option value='1'>1</option> <!-- 가장 중요 -->
                                <option value='2'>2</option> <!-- 중요 -->
                                <option value='3'>3</option> <!-- 보통 -->
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.title"/></th>
                        <td><input type="text" class="inputBox itemW100 remaining" id="title" max="200"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.content"/></th>
                        <td><textarea class="inputBox itemW100 ckeditor" rows="10" id="sbst"></textarea></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.notice.popup"/></th>
                        <td id="popupNoti">
                            <input type="checkbox" name="popupNoti" id="popup" value="popup">
                            <label>
                                <spring:message code="common.popup"/>
                            </label>
                            &nbsp;
                            <input type="checkbox" name="popupNoti" id="post" value="post">
                            <label>
                                <spring:message code="common.post"/>
                            </label>
                        </td>
                    </tr>
                    <tr id="popupImgFile">
                        <th><spring:message code="service.common.popup.image"/></th>
                        <td id="popupImg"></td>
                    </tr>
                    <tr id="popupImgPreview">
                        <th><spring:message code="common.preview"/></th>
                        <td>
                            <div id="imgPreview" class="imgPreview"></div>
                        </td>
                    </tr>
                    <tr id="popupUrlInput">
                        <th><spring:message code="service.common.popup.url"/></th>
                        <td><input type="text" class="inputBox itemW100 remaining" id="popupUrl" max="300" disabled readonly></td>
                    </tr>
                    <tr id="popupPerdDt">
                        <th><spring:message code="service.common.popup.disp.date"/></th>
                        <td>
                            <input type="radio" value='N' name='popupPerd' checked disabled>
                            <label>
                                <spring:message code="common.not.set"/>
                            </label>
                            &nbsp;
                            <input type="radio" value='Y' name='popupPerd' disabled>
                            <label>
                                <spring:message code="common.set"/>
                            </label>
                            &nbsp;
                            <input type="text" class="lengthS inputBox2 txtCenter datepicker" id="startDt" name="startDt" disabled readonly> - 
                            <input type="text" class="lengthS inputBox2 txtCenter datepicker" id="endDt" name="endDt" disabled readonly>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.useYn.noSpace"/></th>
                        <td>
                            <input type="radio" value='Y' name='useYn' checked>
                            <label>
                                <spring:message code="common.sttus.display"/>
                            </label>
                            &nbsp;
                            <input type="radio" value='N' name='useYn'>
                            <label>
                                <spring:message code="common.sttus.stop"/>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.attachfile"/></th>
                        <td>
                            <div id="file_btn_placeholder"></div>
                            <div id="fileArea"></div>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnModify cursorPointer" value="<spring:message code="button.update"/>" onclick="updateNotice()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="returnToPage()">
            </div>
            <!-- Button Group E -->

        </div>
        </form>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display: none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>" width="0%" height="24"
                id="barWidth"><br> <span id="uploadPercent">0</span> <span
                id="uploadText"></span>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>