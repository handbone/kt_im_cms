<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/customer/OlSvcCustomerNoticeDetail.js"></script>
<input type="hidden" id="svcNoticeSeq" value="${svcNoticeSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">
<input type="hidden" id="currSvcSeq" value="${currSvcSeq}">

<style>
#sbst img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.online.notice.detail.title"/></div>
        </div>
        <!-- page title E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="service.common.register.name"/></th>
                            <td id="regNm"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.service.name"/></th>
                            <td id="svcNm"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.display.type"/></th>
                            <td id="dispType"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.target.type"/></th>
                            <td id="targetType"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.regdate"/></th>
                            <td id="regDt"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.important"/></th>
                            <td id="prefRank"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.title"/></th>
                            <td id="title"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.content"/></th>
                            <td id="sbst" style="line-height:initial"><div></div></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.notice.popup"/></th>
                            <td id="popupNoti">
                                <input type="checkbox" name="popupNoti" value="popup" disabled>
                                <label>
                                    <spring:message code="common.popup"/>
                                </label>
                                &nbsp;
                                <input type="checkbox" name="popupNoti" value="post" disabled>
                                <label>
                                    <spring:message code="common.post"/>
                                </label>
                            </td>
                        </tr>
                        <tr id="popupImgRegn">
                            <th><spring:message code="service.common.popup.image"/></th>
                            <td id="popupImgNm"></td>
                        </tr>
                        <tr id="popupUrlRegn">
                            <th><spring:message code="service.common.popup.url"/></th>
                            <td id="popupUrl"></td>
                        </tr>
                        <tr id="popupPerdRegn">
                            <th><spring:message code="service.common.popup.disp.date"/></th>
                            <td id="popupDispDt"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.useYn.noSpace"/></th>
                            <td id="useYn"></td>
                        </tr>
                        <tr id="attachFile" hidden>
                            <th><spring:message code="common.attachfile"/></th>
                            <td id="files"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input hidden type="button" id="btnModify" class="btnNormal btnModify btnLeft cursorPointer" value="<spring:message code="button.update"/>" onclick="moveToEdit()">
                <input hidden type="button" id="btnDelete" class="btnNormal btnDelete btnLeft cursorPointer" value="<spring:message code="button.delete"/>" onclick="deleteNotice()">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" value="<spring:message code="button.list"/>" onclick="pageMove('/servicecustomer')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<!-- Popup Window S 미리보기 팝업 -->
<div id="popupPreView" style="display:none;" class="popupWrap preView"><div class="popupSection">
    <input type="button" class="topClose" onclick="closePopups()">
    <div class="popupContent">
        <img id="popupImg">
    </div>
    <!-- Button Group S -->
    <div class="btnGrp btnCenter">
    <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset' />" onclick="closePopups()">
    </div>
    <!-- Button Group E -->
</div></div>
<!-- Popup Window E 미리보기 팝업 -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>