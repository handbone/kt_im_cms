<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/customer/OlSvcCustomerFaqEdit.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/ckeditor/ckeditor.js"></script>

<input type="hidden" id="currSvcSeq" value="${currSvcSeq}" />
<input type="hidden" id="svcFaqSeq" value="${svcFaqSeq}">
<input type="hidden" id="memberNm" value="${name}" />
<input type="hidden" id="memberSec" value="${memberSec}" />
<input type="hidden" id="memberSvcSeq" value="${svcSeq}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.online.faq.edit.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- content S --> 
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code="service.common.register.name"/></th>
                        <td id="regNm"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.service.name"/></th>
                        <td>
                            <select class="inputBox width200" id="serviceList" onchange="chgService()">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.target.type"/></th>
                        <td>
                            <select class="inputBox width200" id="targetTypeList">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.category"/></th>
                        <td>
                            <select class="inputBox width200" id="faqCtgList">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.faq.title.and.question"/></th>
                        <td><input type="text" class="inputBox itemW100 remaining" id="title" max="200"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.faq.content.and.answer"/></th>
                        <td><textarea class="inputBox itemW100 ckeditor" rows="10" id="sbst"></textarea></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.useYn.noSpace"/></th>
                        <td>
                            <input type="radio" value='Y' name='useYn' checked>
                            <label>
                                <spring:message code="common.sttus.display"/>
                            </label>
                            &nbsp;
                            <input type="radio" value='N' name='useYn'>
                            <label>
                                <spring:message code="common.sttus.stop"/>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->


            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnModify cursorPointer" value="<spring:message code="button.update"/>" onclick="updateFaq()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="returnToPage()">
            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>