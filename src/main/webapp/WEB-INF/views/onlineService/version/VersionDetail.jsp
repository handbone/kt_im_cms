<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/version/VersionDetail.js"></script>
<input type="hidden" id="leftNum" value="4">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" id="svcSeq">
<input type="hidden" id="svcType" value="${svcType}">
<input type="hidden" id="svcAppVerSeq" value="${svcAppVerSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul></ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="version.title.detail"/></div>
        </div>
        <!-- page title E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code='version.regrId'/></th>
                        <td id ="regrNm"><span class="article" id="regrNm"></span></td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="common.service"/></th>
                        <td><span class="article" id="svcNm"></span></td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="version.appType"/></th>
                        <td id="appTypeMA" style="display:none">
                            <input type="radio" value='SA' name='appType' checked>
                            <label><spring:message code="button.service.app"/></label>
                            &nbsp;
                            <input type="radio" value='MA' name='appType'>
                            <label><spring:message code="button.mirroring.app"/></label>
                        </td>
                        <td id="appTypeSA" style="display:none">
                            <input type="radio" value='SA' name='saAppType' checked>
                            <label><spring:message code="button.service.app"/></label>
                        </td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="version.table.name"/></th>
                        <td><span class="article" id="verNm"></span></td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="version.dl.store.type"/></th>
                        <td><span class="article" id="dlStoreType"></span></td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="version.dl.store.path"/></th>
                        <td><span class="article" id="dlStorePath"></span></td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="version.table.updateHst"/></th>
                        <td><span class="article" id="uptSbst"></span></td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="version.useYn"/></th>
                        <td>
                            <input type="radio" value='Y' name='useYn'>
                            <label><spring:message code="version.useYn.Y"/></label>
                            &nbsp;
                            <input type="radio" value='N' name='useYn'>
                            <label><spring:message code="version.useYn.N"/></label>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='button.delete'/>" onclick="deleteVersion()">
                <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code="button.update"/>" onclick="pageMove('/onlineSvc/version/${svcAppVerSeq}/edit')">
                <input type="button" class="btnNormal btnList btnRight" value="<spring:message code="button.list"/>" onclick="pageMove('/onlineSvc/version)">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>