<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/version/VersionList.js"></script>

<input type="hidden" id="memberSec" value="${memberSec}">
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">
<input type="hidden" id="leftNum" value="4">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" id="type" value="SA">
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="svcType" value="${svcType}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul></ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
            <div class="conTitle tabOn"><a href="javascript:;" onclick="tabVal(0,this)"><spring:message code='button.service.app'/></a></div>
            <div class="conTitle tabOff tabEnd"><a href="javascript:;" onclick="tabVal(1,this)"><spring:message code='button.mirroring.app'/></a></div>
        </div>
        <!-- tabGrp E -->

        <!-- Select Box S -->
        <div class="rsvGrp">
            <div class="rsvSelect">
                <span><spring:message code='version.serviceNm'/></span>
                <span class="selSpan">
                    <select id="svcList" onchange="pageReset(); jqGridReload();"></select>
                </span>
            </div>
        </div>
        <!-- Select Box E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="tableArea">
                <table id="jqgridSaData"></table>
                <div id="saPageDiv"></div>
                <table id="jqgridMaData"></table>
                <div id="maPageDiv"></div>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select class="selectBox" id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code='button.search'/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->

                <div class="cellTable">
                    <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='button.delete'/>" onclick="deleteVersion()">
                    <input type="button" class="btnNormal btnWrite btnRight" value="<spring:message code='button.create'/>" onclick="makePageMove(this)">
                </div>
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- content E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>