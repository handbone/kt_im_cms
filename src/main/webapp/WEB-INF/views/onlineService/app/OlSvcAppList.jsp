<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/app/OlSvcAppList.js"></script>

<input type="hidden" id="memberSec" value="${memberSec}">
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="menu.online.service.main.app"/></div>
        </div>
        <!-- Tab E -->

        <!-- Select Box S -->
        <div class="rsvGrp">
            <div class="rsvSelect">
                <span><spring:message code="common.service.name"/></span>
                <span>
                    <select id="serviceList" onchange="updateList()" class="auto"></select>
                </span>
            </div>
        </div> 
        <!-- Select Box E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea" class="">
                <table id="jqgridData"></table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code="button.delete"/>" onclick="deleteApps()">
                <input type="button" class="btnXLarge btnWrite btnLeft" value="<spring:message code="button.app.or.content.regist"/>" onclick="moveToRegist()">
                <input type="button" class="btnNormal btnSave btnRight2" value="<spring:message code="button.save"/>" onclick="storageOrderNums()">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
    <br>
    <!-- Popup Window S 미리보기 팝업 -->
    <div id="popupPreView" style="display:none;" class="popupWrap preView">
        <div class="popupSection">
            <input type="button" class="topClose" onclick="closePopups()">
            <div class="popupContent">
                <img id="popupImg">
            </div>
            <!-- Button Group S -->
            <div class="btnGrp btnCenter">
                <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset' />" onclick="closePopups()">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- Popup Window E 미리보기 팝업 -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>