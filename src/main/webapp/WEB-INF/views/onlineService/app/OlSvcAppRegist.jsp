<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/left_menu.jsp" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/app/OlSvcAppRegist.js"></script>

<input type="hidden" id="currSvcSeq" value="${currSvcSeq}">
<input type="hidden" id="memberSec" value="${memberSec}">
<input type="hidden" id="memberSvcSeq" value="${svcSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.common.app.or.content.regist.title"/></div>
        </div>
        <form id="comnty_write_from" name="comnty_write_from" enctype="multipart/form-data" method="post">
        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tr>
                        <th class="w150"><spring:message code="common.service"/></th>
                        <td>
                            <select id="serviceList" class="inputBox lengthXL"></select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.type"/></th>
                        <td>
                            <input type="radio" name="type" value="APP" checked><label>APP</label>&nbsp;
                            <input type="radio" name="type" value="CONTS"><label><spring:message code="common.contents"/></label>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.app.nm.space"/></th>
                        <td><input type="text" id="appNm" class="inputBox lengthXL remaining" max="100"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.contents.title"/></th>
                        <td><input type="text" id="contsTitle" class="inputBox lengthXL remaining" max="100" disabled readonly></td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.package.nm.space"/></th>
                        <td><input type="text" id="pkgNm" class="inputBox lengthXL remaining" max="200"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.store.url"/></th>
                        <td><input type="text" id="storUrl" class="inputBox lengthXL remaining" max="300"></td>
                    </tr>
                    <tr>
                        <th>URI</th>
                        <td><input type="text" id="uri" class="inputBox lengthXL remaining" max="300"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="service.common.main.image"/></th>
                        <td id="mainImg"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.preview"/></th>
                        <td>
                            <div id="imgPreview" class="imgPreview"></div>
                        </td>
                    </tr>
                </table>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp txtCenter">
                    <input type="button" class="btnNormal btnWrite" value="<spring:message code="button.create"/>" onclick="registApp()">
                    <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>" onclick="returnToPage()">
                </div>
                <!-- Button Group E -->
            </div>
        </div>
        <!-- content E -->
        </form>
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp" %>