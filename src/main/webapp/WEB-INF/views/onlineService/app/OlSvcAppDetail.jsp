<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/app/OlSvcAppDetail.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/libs/swipebox/swipebox.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swipebox/jquery.swipebox.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/justifiedGallery/justifiedGallery.min.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<link href="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.js"></script>

<input type="hidden" id="appSeq" value="${appSeq}">
<input type="hidden" id="currSvcSeq" value="${currSvcSeq}">

   <!-- Contents S -->
   <div id="contents">
       <!-- boxRound S -->
       <div class="boxRound">
           <!-- locate S -->
           <div id="locateGrp">
               <div class="locate">
                   <ul>
                   </ul>
               </div>
           </div>
           <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.common.app.or.content.detail.title"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="common.service"/></th>
                            <td id="svcNm"></td>
                        </tr>
                        <tr>
                        <th><spring:message code="common.type"/></th>
                            <td>
                                <input type="radio" name="type" value="APP" disabled><label>APP</label>&nbsp;
                                <input type="radio" name="type" value="CONTS" disabled><label><spring:message code="common.contents"/></label>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.app.nm.space"/></th>
                            <td id="appNm"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.contents.title"/></th>
                            <td id="contsTitle"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.package.nm.space"/></th>
                            <td id="pkgNm"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.store.url"/></th>
                            <td id="storUrl"></td>
                        </tr>
                        <tr>
                            <th>URI</th>
                            <td id="uri"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.common.main.image"/></th>
                            <td id="fileNm"></td>
                        </tr>
                        <tr>
                               <th><spring:message code="common.preview"/></th>
                               <td class="imgPreview">
                                  <div id="imgPreview"></div>
                               </td>
                           </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
               <div class="btnGrp">
                   <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code="button.delete"/>" onclick="deleteApp()">
                   <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code="button.update"/>" onclick="moveToEdit()">
                   <input type="button" class="btnNormal btnList btnRight" value="<spring:message code="button.list"/>" onclick="returnToList()">
               </div>
               <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>