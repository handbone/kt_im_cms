<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/terms/ServiceTermsDetail.js"></script>
<input type="hidden" id="leftNum" value="4">
<input type="hidden" id="leftSubMenuNum" value="3">
<input type="hidden" id="stpltSeq" value="${stpltSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="svcterms.detail.title"/></div>
        </div>
        <!-- page title E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code="svcterms.reger"/></th>
                        <td><span class="article" id="stpltReger"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="svcterms.serviceNm"/></th>
                        <td><span class="article" id="svcNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="svcterms.stplt.section"/></th>
                        <td><span class="article" id="stpltSection"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="svcterms.table.mandYn"/></th>
                        <td><span class="article" id="stpltMandYn"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="svcterms.table.version"/></th>
                        <td><span class="article" id="stpltVer"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.content"/></th>
                        <td><span class="article" id="stpltContent"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.period"/></th>
                        <td><span class="article" id="stpltPerd"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.useYn"/></th>
                        <td><span class="article" id="stpltUseYn"></span></td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnModify btnLeft cursorPointer" onclick="pageMove('/onlineService/terms/${stpltSeq}/edit')" value="<spring:message code="button.update"/>">
                <input type="button" class="btnNormal btnDelete btnLeft cursorPointer" onclick="svcTermsDeleteCheck()" value="<spring:message code="button.delete"/>">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" onclick="pageMove('/onlineService/terms')" value="<spring:message code="button.list"/>">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>