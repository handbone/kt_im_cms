<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/onlineService/terms/ServiceTermsEdit.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/ckeditor/ckeditor.js"></script>

<input type="hidden" id="leftNum" value="4">
<input type="hidden" id="leftSubMenuNum" value="3">
<input type="hidden" id="name" value="${name}" />
<input type="hidden" id="stpltSeq" value="${stpltSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="svcterms.edit.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- content S --> 
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code="svcterms.reger"/></th>
                        <td id="regNm"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="svcterms.serviceNm"/></th>
                        <td>
                            <select class="inputBox width200" id="svcSelbox"></select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="svcterms.stplt.section"/></th>
                        <td id="stpltType"></td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="svcterms.table.mandYn"/></th>
                        <td>
                            <input type="radio" value='Y' name='mandYn' id="mandY">
                            <label><spring:message code="svcterms.mandYn.yes"/></label>
                            &nbsp;
                            <input type="radio" value='N' name='mandYn' id="mandN">
                            <label><spring:message code="svcterms.mandYn.no"/></label>
                        </td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="svcterms.table.version"/></th>
                        <td><input type="text" class="inputBox itemW100 remaining" id="svcTermsVer" max="20"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.content"/></th>
                        <td><textarea class="inputBox itemW100 ckeditor" rows="10" id="svcTermsSbst"></textarea></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.period"/></th>
                        <td>
                            <input type="radio" value='N' name='perdYn' id="perdN">
                            <label><spring:message code="svcterms.perdYn.no"/></label>
                            &nbsp;
                            <input type="radio" value='Y' name='perdYn' id="perdY">
                            <label><spring:message code="svcterms.perdYn.yes"/></label>
                            &nbsp;
                            <input type="text" class="lengthS inputBox2 txtCenter datepicker" id="startDt" name="startDt" disabled readonly>
                            ~
                            <input type="text" value="2018.9.1" class="lengthS inputBox2 txtCenter datepicker" id="endDt" name="endDt" disabled readonly>
                        </td>
                    </tr>
                    <tr>
                        <th class="w150"><spring:message code="common.useYn"/></th>
                        <td>
                            <input type="radio" value='Y' name='useYn' id="useY">
                            <label><spring:message code="common.sttus.display"/></label>
                            &nbsp;
                            <input type="radio" value='N' name='useYn' id="useN">
                            <label><spring:message code="common.sttus.stop"/></label>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnModify cursorPointer" value="<spring:message code="button.update.confirm"/>" onclick="editSvcTerms()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="cancelEditSvcTerms()">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display: none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>" width="0%" height="24" id="barWidth"><br> 
            <span id="uploadPercent">0</span>
            <span id="uploadText"></span>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>