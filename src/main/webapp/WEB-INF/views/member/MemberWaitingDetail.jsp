<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->
<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/member/MemberWaitingDetail.js?version=201806041516"></script>

<input type="hidden" id="seq" value="${seq}">

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

            <!-- locate S -->
            <div id="locateGrp">
                <div class="locate">
                    <ul>
                    </ul>
                </div>
            </div>
            <!-- locate E -->

            <div class="conTitleGrp">
                <div class="conTitle"><spring:message code="member.manage.waiting.detail"/></div>
            </div>

            <!-- content S -->
            <div class="content">
                    
                <!-- Table Area S -->
                <div class="tableArea">

                    <table class="tableDetail">
                        <tr>
                            <th class="w150"><spring:message code="login.input.name"/></th>
                            <td id="memberName"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.id"/></th>
                            <td id="memberId"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.section"/></th>
                            <td id="memberSection"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.cretDt"/></th>
                            <td id="cretDt"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.telNo"/></th>
                            <td id="memberTelNo"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.phoneNo"/></th>
                            <td id="memberPhoneNo"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.email"/></th>
                            <td id="memberEmail"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="table.useYn"/></th>
                            <td id="memberStatusName"></td>
                        </tr>
                    </table>

                
                </div>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp">
                    <input type="button" class="btnNormal btnOk btnLeft" value="<spring:message code='button.acknowledgement' />">
                    <input type="button" class="btnNormal btnDeleteLong btnLeft" value="<spring:message code='button.member.manage.delete'/>">
                    <input type="button" class="btnNormal btnList btnRight" value="<spring:message code='button.list'/>">
                </div>
                <!-- Button Group E -->

            </div>
            <!-- content E -->
            
        </div>
    </div>
    <!-- right contents E -->

<!-- Popup Window S -->
<div id="popupWindow" style="display:none;">
    <div class="popupTitle"><span><spring:message code="member.manage.waiting.popup.title" /></span><div class="btnClose"><a href="javascript:;"> </a></div></div>
    <div class="popupTxt">
        <ul>
            <li class="popupTxtQ"><spring:message code="member.manage.waiting.accept.msg" /></li>
            <li class="popupTxtQ1"><spring:message code="member.manage.waiting.check.document.msg" /></li>
            <li class="popupTxtQ2">
                <span class="popupTxtQ3"><input name="proofDocumentYn" value="Y" type="radio"></span>
                <label><spring:message code="button.member.manage.submit" /></label>
                <span><input name="proofDocumentYn" value="N" type="radio"></span>
                <label><spring:message code="button.member.manage.noSubmit" /></label>
            </li>
        </ul>
    </div>

    <!-- Button Group S -->
    <div class="btnGrp btnCenter2">
        <input type="button" class="btnNormal btnOk" value="<spring:message code='button.confirm' />">
        <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset' />">
    </div>
    <!-- Button Group E -->

</div>
<!-- Popup Window E -->
<%@include file="/WEB-INF/views/frame/footer.jsp"%>