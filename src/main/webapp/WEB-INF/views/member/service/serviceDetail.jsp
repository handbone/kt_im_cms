<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/member/service/ServiceDetail.js"></script>
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="leftNum" value="1">
<input type="hidden" id="leftSubMenuNum" value="2">
<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.table.detail" /></div>
        </div>
        <!-- tabGrp E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="service.table.name"/></th>
                            <td id="svcNm"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.telNo"/></th>
                            <td id="telNo"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.addr"/></th>
                            <td id="addr"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.bizno"/></th>
                            <td id="bizno"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.svcType"/></th>
                            <td id="svcType"></td>
                        </tr>
                        <tr id="mirrYn" hidden>
                            <th><spring:message code="service.table.mirrYn"/></th>
                            <td id="mirrYnVal"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.useYn"/></th>
                            <td id="useYn"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.register.id"/></th>
                            <td id="cretrId"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.registDate"/></th>
                            <td id="cretDt"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.update.user.id"/></th>
                            <td id="amdrId"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.update.dt"/></th>
                            <td id="amdDt"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                    <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code='title.update'/>" onclick="cofirmMove('/service/${svcSeq}/edit')">
                    <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='title.delete'/>" onclick="svcDelete();">
                    <input type="button" class="btnNormal btnList btnRight" value="<spring:message code='title.list'/>" onclick="pageMove('/service')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- Contents E -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display:none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>"  width="50%" height="24" id="barWidth"><br>
            <span id="uploadPercent">0</span>
            <span id="uploadText"></span>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>