<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/member/service/ServiceEdit.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/daum/postcode.v2.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/daum/180326.js"></script>
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="leftNum" value="1">
<input type="hidden" id="leftSubMenuNum" value="2">
<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="service.table.edit" /></div>
        </div>
        <!-- tabGrp E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="service.table.name"/></th>
                            <td><input type="text" class="inputBox lengthM remaining" max="15" id="svcNm" name="svcNm"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.tel"/></th>
                            <td>
                                <input type="text" class="inputBox lengthXS telNo onlynum remaining" max="4" min="2"> -
                                <input type="text" class="inputBox lengthS telNo onlynum remaining" max="4" min="3"> -
                                <input type="text" class="inputBox lengthS telNo onlynum remaining" max="4" min="4">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.addr"/></th>
                            <td>
                                <span>
                                    <input type="text" id="zipcd" name="zipcd" class="inputBox lengthS remaining" max="7">
                                    <input type="button" class="btnSearchLongWhite btnWhite" value="<spring:message code="service.table.find.zipcd"/>" onclick="javascript:openDaumPostcode('zipcd', 'basAddr', 'dtlAddr');">
                                </span>
                                <br /><input type="text" class="inputBox lengthXXL marginTB4 remaining" max="100" id="basAddr" name="basAddr">
                                <br /><input type="text" class="inputBox lengthXXL marginTB4 remaining" max="100" id="dtlAddr">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.bizno"/></th>
                            <td>
                                <input type="text" class="inputBox lengthXS onlynum remaining bizno" max="3" min="3"> -
                                <input type="text" class="inputBox lengthXS onlynum remaining bizno" max="2" min="2"> -
                                <input type="text" class="inputBox lengthS onlynum remaining bizno" max="5" min="5">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.svcType"/></th>
                            <td id="svcType">
                                <input type="radio" value='OF' name='svcType'>
                                <label>
                                    <spring:message code="service.table.svcType.off"/>
                                </label>
                                &nbsp;
                                <input type="radio" value='ON' name='svcType'>
                                <label>
                                    <spring:message code="service.table.svcType.on"/>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="service.table.useYn"/></th>
                            <td>
                                <input type="radio" value='Y' name='useYn'>
                                <label>
                                    <spring:message code="common.enable"/>
                                </label>
                                &nbsp;
                                <input type="radio" value='N' name='useYn'>
                                <label>
                                    <spring:message code="common.disable"/>
                                </label>
                            </td>
                        </tr>
                        <tr id="mirrYn" style="display:none;">
                            <th><spring:message code="service.table.mirrYn"/></th>
                            <td id="mirrYnVal">
                                <input type="radio" value='MA' name='appType'>
                                <label>Y</label>
                                &nbsp;
                                <input type="radio" value='SA' name='appType' checked>
                                <label>N</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnModify" value="<spring:message code="button.update.confirm"/>" onclick="svcEdit()">
                <input type="button" class="btnNormal btnCancel" value="<spring:message code="button.reset"/>" onclick="pageMove('/service')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- Contents E -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display:none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>"  width="50%" height="24" id="barWidth"><br>
            <span id="uploadPercent">0</span>
            <span id="uploadText"></span>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>