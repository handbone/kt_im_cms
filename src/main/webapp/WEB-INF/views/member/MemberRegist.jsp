<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->
<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/member/MemberRegist.js?version=201806011519"></script>

<input type="hidden" id="memberSec" value="${memberSec}" />

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

            <!-- locate S -->
            <div id="locateGrp">
                <div class="locate">
                    <ul>
                    </ul>
                </div>
            </div>
            <!-- locate E -->

            <div class="conTitleGrp">
                <div class="conTitle"><spring:message code="member.manage.regist"/></div>
            </div>

            <!-- content S -->
            <div class="content">

                <!-- Table Area S -->
                <div class="tableArea">

                    <table class="tableForm">
                        <tr>
                            <th class="w150"><spring:message code="login.input.id"/></th>
                            <td><span><input id="memberId" type="text" class="inputBox lengthM"></span>&nbsp;<span><input id="btnCheckId" type="button" class="btnSmallWhite btnDoubleGray" value="<spring:message code='login.button.checkId'/>"></span>
                                <br><span id="memberIdMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.password"/></th>
                            <td>
                                <input id="memberPwd" type="password" class="inputBox lengthL">
                                <br><span id="memberPwdMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.passwordConfirm"/></th>
                            <td>
                                <input id="memberPwdRe" type="password" class="inputBox lengthL">
                                <br><span id="memberPwdReMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.section"/></th>
                            <td>
                                <select id="memberSection" class="selectBox">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.service"/></th>
                            <td>
                                <select id="memberService" class="selectBox">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.select.store"/></th>
                            <td>
                                <select id="memberStore" class="selectBox">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.name"/></th>
                            <td>
                                <input id="memberName" type="text" class="inputBox lengthL">
                                <br><span id="memberNameMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.telNo"/></th>
                            <td>
                                <input id="memberTelNo" type="text" class="inputBox lengthL">
                                <br><span id="memberTelNoMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.phoneNo"/></th>
                            <td id="memberPhoneNoCol">
                                <div class="margin_2">
                                    <p class="memberPhoneBox">
                                        <input type="text" class="inputBox lengthL memberPhoneNo" value="">
                                        <input type="button" class="rightIconPlus rightBtn" id="mPlus"/>
                                        <br><span class="cautionTxt"></span>
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.email"/></th>
                            <td>
                                <input id="memberEmailId" type="text" class="lengthS inputBox"> @ <input id="memberEmailDomain" type="text" class="lengthS inputBox"> &nbsp;
                                <select id="memberDomainList" class="lengthS inputBox">
                                </select>
                                <br><span id="memberEmailIdMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.ipAddress"/></th>
                            <td>
                                <input id="memberIpadr" type="text" class="inputBox lengthM">
                                <input id="memberIpadr2" type="text" class="inputBox lengthM">
                                <br><span id="memberIpadrMsg" class="cautionTxt"></span><span id="memberIpadr2Msg" class="cautionTxt"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp txtCenter">
                    <input type="button" class="btnNormal btnWrite" value="<spring:message code='button.create.confirm'/>">
                    <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>">
                </div>
                <!-- Button Group E -->

            </div>
            <!-- content E -->



        </div>
        <!-- right contents E -->

    </div>
<!-- Wrapper E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>