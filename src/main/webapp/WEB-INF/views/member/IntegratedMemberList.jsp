<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->
<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/member/IntegratedMemberList.js?version=201806011515"></script>

    <!-- right contents S -->
    <div id="contents">
        <div class="boxRound">
            <!-- locate S -->
            <div id="locateGrp">
                <div class="locate">
                    <ul>
                    </ul>
                </div>
            </div>
            <!-- locate E -->

            <div class="conTitleGrp">
                <div class="conTitle"><spring:message code="member.integrated.manage.list"/></div>
            </div>

            <div class="rsvGrp rsvGrpNoTab">
                <div class="rsvSelect">
                    <span><spring:message code="table.useYn"/></span>
                    <span>
                        <select id="useYn" onchange="searchKeyword()">
                            <option value=""><spring:message code="select.all"/></option>
                            <option value="Y"><spring:message code="common.enable"/></option>
                            <option value="N"><spring:message code="common.disable"/></option>
                        </select>
                    </span>
                </div>
            </div> 

            <!-- content S -->
            <div class="content">

                <!-- Table Area S -->
                <div id="gridArea">
                    <table id="jqgridData"></table>
                    <div id="pageDiv"></div>
                </div>
                <!-- Table Area E -->

                <div class="CenterGrp absol_btn">
                    <!-- Search Group S -->
                    <div class="searchBox">
                        <div class="searchGrp">
                            <div class="selectBox">
                                <select id="target">
                                </select>
                            </div>
                            <div class="searchInput"><input id="keyword" type="text"></div>
                            <div class="searchBtn"><input type="button" class="btnNormal btnSearch" value="<spring:message code='button.search'/>"></div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- content E -->
        </div>
    </div>
    <!-- right contents E -->
<%@include file="/WEB-INF/views/frame/footer.jsp"%>