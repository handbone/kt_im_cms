<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/member/contentprovider/CpRegist.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/daum/postcode.v2.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/daum/180326.js"></script>
<input type="hidden" id="leftNum" value="1">
<input type="hidden" id="leftSubMenuNum" value="3">

<%
    String mbrSe = session.getAttribute("memberSec").toString();
%>
<input type="hidden" name="mbrSe" id="mbrSe" value="<%=mbrSe%>" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="cp.regist.title"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableForm">
                    <tr>
                        <th class="w150"><spring:message code="cp.comn.name"/></th>
                        <td><input type="text" class="inputBox lengthXL remaining" id="cpName" max="25" min="0"></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.tel.no"/></th>
                        <td>
                            <input type="text" class="inputBox lengthXS telNo onlynum remaining" max="4" min="2"> -
                            <input type="text" class="inputBox lengthS telNo onlynum remaining" max="4" min="3"> -
                            <input type="text" class="inputBox lengthS telNo onlynum remaining" max="4" min="4">
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.address"/></th>
                        <td>
                            <span>
                                <input type="text" id="zipcode" name="zipcode" class="inputBox lengthS remaining" max="7">
                                <input type="button" class="btnSearchLongWhite btnWhite" value="<spring:message code="button.zipcode"/>" onclick="javascript:openDaumPostcode('zipcode', 'basAddr', 'dtlAddr');">
                            </span>
                            <br /><input type="text" class="inputBox lengthXXL marginTB4 remaining" max="100" id="basAddr" name="basAddr">
                            <br /><input type="text" class="inputBox lengthXXL marginTB4 remaining" max="100" id="dtlAddr">
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.business.license"/></th>
                        <td>
                            <input type="text" class="inputBox lengthXS onlynum remaining bizno" max="3" min="3"> - 
                            <input type="text" class="inputBox lengthXS onlynum remaining bizno" max="2" min="2"> - 
                            <input type="text" class="inputBox lengthS onlynum remaining bizno" max="5" min="5">
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.alliance.services"/></th>
                        <td id="serviceList">
                        </td>
                        <!--
                        <td>
                            <input type="button" value="IMEX" class="inputBox inputBoxChecked lengthS">&nbsp;
                            <input type="button" value="K-Live" class="inputBox inputBoxChecked lengthS">&nbsp;
                            <input type="button" value="서비스1" class="inputBox  lengthS">&nbsp;
                            <input type="button" value="서비스2" class="inputBox  lengthS">&nbsp;
                            <input type="button" value="서비스3" class="inputBox  lengthS">&nbsp;
                            <input type="button" value="서비스4" class="inputBox  lengthS">
                        </td>
                        -->
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.contract.period"/></th>
                        <td>
                            <input type="text" class="lengthS inputBox2 txtCenter datepicker" id="contStDt" name="contStDt" readonly> - 
                            <input type="text" value="2018.9.1" class="lengthS inputBox2 txtCenter datepicker" id="contFnsDt" name="contFnsDt" readonly>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnWrite cursorPointer" value="<spring:message code="button.create.confirm"/>" onclick="cpRegist()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="pageMove('/cp')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>