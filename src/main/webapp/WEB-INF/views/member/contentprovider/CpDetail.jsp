<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/member/contentprovider/CpDetail.js"></script>
<input type="hidden" id="leftNum" value="1">
<input type="hidden" id="leftSubMenuNum" value="3">
<input type="hidden" id="cpSeq" value="${cpSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="cp.detail.title"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tr>
                        <th class="w150"><spring:message code="cp.comn.name"/></th>
                        <td><span class="article" id="cpNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.tel.no"/></th>
                        <td><span class="article" id="cpTelNo"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.address"/></th>
                        <td><span class="article" id="cpAddr"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.business.license"/></th>
                        <td><span class="article" id="bizNo"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.alliance.services"/></th>
                        <td><span class="article" id="cprtSvc"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.contract.period"/></th>
                        <td><span class="article" id="contDt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.useyn"/></th>
                        <td><span class="article" id="useYnNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.register.id"/></th>
                        <td><span class="article" id="cretrId"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.regist.date"/></th>
                        <td><span class="article" id="cretDt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.updater.id"/></th>
                        <td><span class="article" id="amdrId"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="cp.comn.update.date"/></th>
                        <td><span class="article" id="amdDt"></span></td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code="button.update"/>" onclick="cofirmMove('/cp/${cpSeq}/edit')">
                <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code="button.delete"/>" onclick="cpDelete()">
                <input type="button" class="btnNormal btnList btnRight" value="<spring:message code="button.list"/>" onclick="pageMove('/cp')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>