<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/statistics/CommonStatistics.js?version=201807171750"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/statistics/traffic/Traffic.js?version=201807171750"></script>
<input type="hidden" id="startDate" />
<input type="hidden" id="endDate" />

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

                <!-- locate S -->
                <div id="locateGrp">
                    <div class="locate">
                        <ul>
                        </ul>
                    </div>
                </div>
                <!-- locate E -->
                
                <!-- page title S -->
                <div class="conTitleGrp">
                    <div class="conTitle"><spring:message code='statistics.traffic.title'/></div>
                </div>
                <!-- page title E -->

                <!-- Select Option S -->
                <div class="periodGrp" style="text-align:right;"> 
                    <!-- 사용자지정 기간 선택 S -->
                    <span class="itemPeriod"><spring:message code='common.period'/></span>
                    <span><input id="inputDatePicker1" type="text" class="inputBox2 lengthS txtCenter" readonly></span> ~ <span><input id="inputDatePicker2" type="text" class="inputBox2 lengthS txtCenter" readonly></span>
                    <span><input type="button" value="<spring:message code='button.inquire'/>" class="btnSearchWhite"></span>
                </div>
                <!-- Select Option E -->

                <!-- Button down S  -->
                <div class="btnGrp2">
                    <div class="btnNormal btnXls btnRight" style="cursor:pointer"><spring:message code='button.download.excel'/></div>
                </div>
                <!-- Button down E  -->

                <!-- Table Area S -->
                <div id="gridArea">
                    <table id="jqgridData"></table>
                    <div id="pageDiv"></div>
                </div>
                <!-- Table Area E -->
            </div>
            <!-- content E -->
        </div>
<%@include file="/WEB-INF/views/frame/footer.jsp"%>