<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/statistics/CommonStatistics.js?version=201807171750"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/statistics/contents/RegisteredContents.js?version=201807171750"></script>
<input type="hidden" id="svcSeq" value="${svcSeq}">

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

                <!-- locate S -->
                <div id="locateGrp">
                    <div class="locate">
                        <ul>
                        </ul>
                    </div>
                </div>
                <!-- locate E -->
                
                <!-- page title S -->
                <div class="conTitleGrp">
                    <div id="btnRegistered" class="conTitle tabOn"><spring:message code='statistics.contents.registered.title'/></div>
                    <div id="btnUsed" class="conTitle tabOff tabEnd"><a href="javascript:;"><spring:message code='statistics.contents.used.title'/></a></div>
                </div>
                <!-- page title E --> 

                <!-- Select Box S -->
                <div class="rsvGrp rsvGrpNoTab">
                    <div class="rsvSelect">
                        <span><spring:message code='table.section'/></span>
                        <span>
                            <select id="sectionList" class="bgWhite">
                                <option value="service"><spring:message code='common.service'/></option>
                                <option value="cp"><spring:message code='common.cp.office'/></option>
                            </select>
                        </span>
                    </div>
                </div> 
                <!-- Select Box E -->
                <div style="border-top:solid 1px #ddd; height:44px;"></div>

                <!-- Button down S  -->
                <div class="btnGrp2">
                    <div class="btnNormal btnXls btnRight" style="cursor:pointer"><spring:message code='button.download.excel'/></div>
                </div>
                <!-- Button down E  -->

                <!-- Table Area S -->
                <div id="gridArea">
                    <table id="jqgridData"></table>
                    <div id="pageDiv"></div>
                </div>
                <!-- Table Area E -->
            </div>
            <!-- content E -->
        </div>
<%@include file="/WEB-INF/views/frame/footer.jsp"%>