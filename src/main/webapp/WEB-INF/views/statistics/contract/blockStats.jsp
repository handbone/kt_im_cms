<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="sysYear"><fmt:formatDate value="${now}" pattern="yyyy" /></c:set>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/statistics/contract/blockStats.js"></script>

<input type="hidden" id="leftNum" value="5">
<input type="hidden" id="leftSubMenuNum" value="6">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" name="mbrSe" id="mbrSe" value="${memberSec}" />
<input type="hidden" name="cpSeq" id="cpSeq" value="${cpSeq}" />
<input type="hidden" name="pageType" id="pageType" value="contract" />


<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
                <div class="conTitle tabOn"><a href="javascript:;" onclick="tabVal(0,this);"><spring:message code='menu.stat.CpConts'/></a></div>
                <div class="conTitle tabEnd tabOff"><a href="javascript:;" onclick="tabVal(1,this)"><spring:message code='menu.stat.SvcConts'/></a></div>
        </div>
        <!-- tabGrp E -->

        <div class="rsvGrp rsvGrpPos" style="width:auto;">
            <!-- Select box S -->
            <div class="rsvSelect" style="float:left;" id="cp">
                <span style='font-size:14px; vertical-align: middle;'><spring:message code="title.cp"/> &nbsp;</span>
                <span>
                    <select id="cpList" onchange="contractList();" style="min-width:136px;"></select>
                </span>
            </div>
            <!-- Select box E -->

            <div class="rsvSelect" style="float:left; margin-right: 10px;">
                <span style='font-size:14px; vertical-align: middle;'><spring:message code='content.service.name'/></span>
                <span>
                    <select id="svcList" onchange="storList();">
                    </select>
                </span>
            </div>

            <div class="rsvSelect">
                <span style='font-size:14px; vertical-align: middle;'><spring:message code="store.comn.name"/></span>
                <span>
                    <select id="storList" onchange="contractList();">
                    </select>
                </span>
            </div>
        </div>

        <div class="block-chain-tap-group">
            <div class="block-chain-caution"><spring:message code="contract.stat.warning"/></div>
                <div class="select-per-grp">
                    <span class="itemPeriod"><spring:message code="common.period"/></span>
                    <span class="itemPeriod">
                        <select id="year" onchange="changedYear(this);">
                            <c:forEach var="i" begin="0" end="${sysYear-2018}">
                                <c:set var="yearOption" value="${sysYear + (2018 - sysYear) + i}" />
                                <option value="${yearOption}">${yearOption}</option>
                            </c:forEach>
                        </select> <spring:message code="common.year"/>
                    </span>
                    <span class="itemPeriod">
                    <select id="month" onchange="contractList();">
                         <c:forEach begin="1" end="12" step="1" var="i">
                            <option value="${i}">${i}</option>
                        </c:forEach>
                    </select> <spring:message code="common.month"/>
                </span>
            </div>
        </div>

        <div class="select-radio-grp">
            <input type="radio" name="type" value="C" id="type_C" checked>
            <label for="type_C"><spring:message code="contract.type.count"/></label>
            <input type="radio" name="type" value="T" id="type_T">
            <label for="type_T"><spring:message code="contract.type.time"/></label>
            <input type="radio" name="type" value="L" id="type_L">
            <label for="type_L"><spring:message code="billing.type.lump"/></label>
            <input type="radio" name="type" value="N" id="type_N">
            <label for="type_N"><spring:message code="billing.type.free"/></label>
        </div>



        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea" class="">
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
            </div>
            <!-- Table Area E -->

            <!-- btnGrp S -->
            <div class="CenterGrp">
                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code='button.search'/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->
            </div>
            <!-- btnGrp E -->
        </div>
        <!-- content E -->
    </div>

    <%@include file="/WEB-INF/views/frame/footer.jsp"%>
</div>
<!-- Contents E -->
