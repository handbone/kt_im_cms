<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/statistics/CommonStatistics.js?version=201807171750"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/statistics/store/VisitorList.js?version=201807171750"></script>
<input type="hidden" id="memberSec" value="${memberSec}">
<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="storSeq" value="${storSeq}">
<input type="hidden" id="startDate" />
<input type="hidden" id="endDate" />

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

                <!-- locate S -->
                <div id="locateGrp">
                    <div class="locate">
                        <ul>
                        </ul>
                    </div>
                </div>
                <!-- locate E -->
                
                <!-- page title S -->
                <div class="conTitleGrp">
                    <div id="btnVisitor" class="conTitle tabOn"><spring:message code='statistics.store.visitor.title'/></div>
                    <div id="btnTag" class="conTitle tabOff"><a href="javascript:;"><spring:message code='statistics.store.tag.title'/></a></div>
                    <div id="btnContentsByTag" class="conTitle tabOff tabEnd"><a href="javascript:;"><spring:message code='statistics.store.contentsByTag.title'/></a></div>
                </div>
                <!-- page title E --> 

                <!-- Select Box S -->
                <div class="rsvGrp rsvGrpNoTab">
                    <div class="rsvSelect">
                        <span><spring:message code="product.comn.seviceNm"/></span>
                        <span>
                            <select id="serviceList" class="bgWhite">
                            </select>
                        </span>&nbsp;
                        <span><spring:message code="product.comn.storeNm"/></span>
                        <span>
                            <select id="storeList" class="bgWhite">
                            </select>
                        </span>
                    </div>
                </div> 
                <!-- Select Box E -->

                <!-- Select Option S -->
                <div class="periodGrp">
                    <div class="tabPeriodGrp">
                        <span class="itemPeriod"><spring:message code='common.period'/></span>
                        <span class="btnPeriod"><a id="btnPrveDate" href="javascript:;"><img src="<c:url value='/resources/image/icon_left.png'/>"></a></span>
                        <span id="datePicker">
                            <input id="inputDatePicker1" type="text" class="inputBox2 lengthS txtCenter datepicker" readonly>
                            <span id="periodTxt">~&nbsp;</span><input id="inputDatePicker2" type="text" class="inputBox2 lengthS txtCenter datepicker" readonly>
                        </span>
                        <span class="btnPeriod"><a id="btnNextDate"  href="javascript:;"><img src="<c:url value='/resources/image/icon_right.png'/>"></a></span>
                        <span id="dateSelector" style="display:none">
                            <span class="itemPeriod">
                                <select id="yearSelector">
                                </select> <spring:message code='common.year'/>
                            </span>
                            <span class="itemPeriod">
                                <select id="monthSelector">
                                </select> <spring:message code='common.month'/>
                            </span>
                        </span>
                        <div class="tabPeriod" style="margin-left:20px;">
                            <div id="btnCustom" class="tabBtn" style="cursor:pointer"><spring:message code='button.custom'/></div>
                            <div id="btnDay" class="tabBtn" style="cursor:pointer"><spring:message code='button.day'/></div>
                            <div id="btnWeek" class="tabBtn tabBtnOn" style="cursor:pointer"><spring:message code='button.week'/></div>
                            <div id="btnMonth" class="tabBtn tabBtnEnd" style="cursor:pointer"><spring:message code='button.month'/></div>
                        </div>
                    </div>
                </div>
                <!-- Select Option E -->

                <!-- Button down S  -->
                <div class="btnGrp2">
                    <div class="btnNormal btnXls btnRight" style="cursor:pointer"><spring:message code='button.download.excel'/></div>
                </div>
                <!-- Button down E  -->

                <!-- Table Area S -->
                <div id="gridArea" class="">
                    <table id="jqgridData"></table>
                    <div id="pageDiv"></div>
                </div>
                <!-- Table Area E -->

            </div>
            <!-- content E -->
        </div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>