<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/DisplayList.js"></script>

<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="3">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" id="display" value="display">
<input type="hidden" id="sttus" value="${sttus}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle tabOn"><a href="javascript:;" onclick="tabVal(0,this)"><spring:message code="display.main.title"/></a></div>
            <div class="conTitle tabOff"><a href="javascript:;" onclick="tabVal(1,this)"><spring:message code="display.recommend.title"/></a></div>
            <div class="conTitle tabOff tabEnd"><a href="javascript:;" onclick="tabVal(2,this)"><spring:message code="display.history.title"/></a></div>
        </div>
        <!-- page title E -->

        <!-- Select Box S -->
        <div class="rsvGrp">
            <div class="rsvSelect" id="rsvSelect">
               &nbsp;
                <span><spring:message code="display.progress.status"/></span>
                <span>
                    <select id="sttusSelbox" onchange="contentStateChange()">
                    </select>
                </span>
            </div>
        </div>
        <!-- Select Box E -->

        <div id="rcmdTitleBar">
            <div class="h2 textBold" style="position:absolute; top:60px; left:5px;"><spring:message code="display.list.recommend"/></div>
            <div style="position:absolute; top:65px; right:5px;">
                <span class="cursorPointer" onclick="recommendContentsRankUp()"><img src="<%=request.getContextPath()%>/resources/image/icon_up.png"></span>
                <span class="cursorPointer" onclick="recommendContentsRankDown()"><img src="<%=request.getContextPath()%>/resources/image/icon_down.png"></span>
            </div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea" class="tableArea">
                <table id="jqgridDispData"></table>
                <div id="dispPageDiv"></div>
                <table id="jqgridDispHstData"></table>
                <div id="dispHstPageDiv"></div>
            </div>

            <div id="tableArea" class="tableArea">
                <table class="titleTable fix_table listBox tableList">
                    <colgroup>
                        <col width="50%">
                        <col width="20%">
                        <col width="13%">
                        <col width="17%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th><spring:message code="common.contents.nm"/></th>
                            <th><spring:message code="common.categoryName"/></th>
                            <th><spring:message code="common.regdate"/></th>
                            <th></th>
                            <th class="d_none"></th>
                            <!--
                            <th class="width500"><spring:message code="common.title"/></th>
                            <th class="width200"><spring:message code="common.categoryName"/></th>
                            <th class="width100"><spring:message code="common.regdate"/></th>
                            <th></th>
                            -->
                        </tr>
                    </thead>
                    <tbody id="rcmdContents">
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <div class="CenterGrp absol_btn" id="dispBtnGrp">
                <div class="cellTable">
                    <input type="button" id="btnDspOn" class="btnNormal btnDisplay" onclick="changeDisplayState('displayOn')" value="<spring:message code="button.display.on"/>">
                </div>

                <div class="cellTable">
                    <input type="button" id="btnDspOff" class="btnNormal btnDisplay" onclick="changeDisplayState('displayOff')" value="<spring:message code="button.display.off"/>">
                </div>

                <div class="searchBox" id="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="검색">
                        </div>
                    </div>
                </div>
            </div>

            <div class="btnGroup txtCenter" id="rcmdBtnGrp">
                <input type="button" class="btnNormal btnWrite" value="<spring:message code="button.create.confirm"/>" onclick="registRecommendContents()">
                <input type="button" class="btnNormal btnCancel" value="<spring:message code="button.reset"/>" onclick="cancelRegist()">
            </div>
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<!-- Popup Window S 노출 콘텐츠 목록 팝업 -->
<div class="popupWinLarge" style="display: none;">
    <div class="popupTitle"><span><spring:message code="display.main.list.title"/></span></div>
    <div class="popupContent">
        <div class="PopSearchGrp">
            <div class="cellTable">
                <select>
                    <option><spring:message code="common.contents.nm"/></option>
                </select>
            </div>
            <div class="cellTable"><input type="text" class="boxSearch" id="searchKeyword" onkeypress="if(event.keyCode==13) {searchDispOnConts(); return false;}" ></div>
            <div class="cellTable"><input type="button" value="<spring:message code="button.search"/>" class="btnNormalWhite" onClick="searchDispOnConts()"></div>
            <div class="cellTable"><input type="button" value="<spring:message code="button.refresh"/>" class="btnNormalWhite" onClick="searchDispOnContsReset()"></div>
        </div>

        <!-- Table Area S -->
        <div class="tableArea disp-scroll-table" style="min-height:315px;">
            <div style="border:solid 1px #ddd; height:276px; overflow-y:auto;">
                <table class="titleTable fix_table listBox tableList">
                    <thead>
                        <tr>
                            <th><spring:message code="common.contents.nm"/></th>
                            <th><spring:message code="common.categoryName"/></th>
                            <th><spring:message code="common.regdate"/></th>
                            <th class="d_none"></th>
                            <th class="d_none"></th>
                        </tr>
                    </thead>
                    <tbody id="dispOnContents">
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Table Area E -->
    </div>

    <!-- Button Group S -->
    <div class="btnGrp btnCenter">
        <input type="button" class="btnNormal btnOk" value="<spring:message code="button.confirm" />" onclick="closeDispOnContsListPopup()">
    </div>
    <!-- Button Group E -->
</div>
<!-- Popup Window E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>