<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/DisplayRecommend.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/libs/swipebox/swipebox.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swipebox/jquery.swipebox.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/justifiedGallery/justifiedGallery.min.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>

<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="3">
<style>
.listBox tr.checked {
    background-color: #ddd
}
</style>

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="display.recommend.title"/></div>
        </div>
        <!-- page title E -->

        <!-- content S -->
        <div class="content">
            <!-- harfGrp S -->
            <div id="harfGrp" style="height:300px;">
                <!-- harf left table S -->
                <div class="divHarf harfLeft height230 tableAreaSm">
                    <h1 class="harfTitle"><spring:message code="display.list.display.on"/></h1>
                    <!-- harfTable S -->
                    <div class="scroll-table">
                        <div class="scrollField">
                            <table class="titleTable fix_table listBox tableList">
                                <thead>
                                    <tr>
                                        <th class="width50 chkBox"><input type="checkbox" id="contsAllCb" onchange="checkboxAll('conts')"></th>
                                        <th><spring:message code="common.title"/></th>
                                        <th class="itemW30"><spring:message code="common.categoryName"/></th>
                                        <th class="itemW30"><spring:message code="common.regdate"/></th>
                                    </tr>
                                </thead>
                                <tbody id="selectDisplayContents">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- harfTable E -->
                </div>
                <!-- harf left table E -->

                <!-- button S -->
                <div class="addBtns">
                    <div class="btnLeftArrow cursorPointer" onclick="deleteDisplayRecommentContents()"><img src="<%=request.getContextPath()%>/resources/image/btn_left.gif"></div>
                    <div class="btnRightArrow cursorPointer" onclick="insertDisplayRecommendContents()"><img src="<%=request.getContextPath()%>/resources/image/btn_right.gif"></div>
                </div>
                <!-- button E -->

                <!-- harf right table S -->
                <div class="divHarf harfRight  height230 tableAreaSm">
                    <h1 class="harfTitle"><spring:message code="display.list.recommend"/></h1>
                    <div class="updownGrp">
                        <span class="padding2 cursorPointer" onclick="recommendContentsRankUp()"><img src="<%=request.getContextPath()%>/resources/image/btn_up.png"></span><span class="cursorPointer" onclick="recommendContentsRankDown()"><img src="<%=request.getContextPath()%>/resources/image/btn_down.png"></span>
                    </div>
                    <!-- table S -->
                    <div class="scroll-table">
                        <div class="scrollField">
                            <table class="titleTable fix_table listBox tableList">
                                <thead>
                                    <tr>
                                        <th class="chkBox width50"><input type="checkbox" id="rcmdContsAllCb" onchange="checkboxAll('rcmdConts')"></th>
                                        <th><spring:message code="common.title"/></th>
                                        <th class="width130"><spring:message code="common.categoryName"/></th>
                                        <th class="width130"><spring:message code="common.regdate"/></th>
                                    </tr>
                                </thead>
                                <tbody id="selectRecommendContents">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- table E -->
                </div>
                <!-- harf right table E -->
            </div>
            <!-- harf div E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnWrite btnLeft cursorPointer" value="<spring:message code="button.create.confirm"/>" onclick="registRecommendContents()">
                <input type="button" class="btnNormal btnCancel btnRight cursorPointer" value="<spring:message code="button.reset"/>" onclick="pageMove('/display#page=1&sttus=06&display=display')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->


<div id="popupWinBig">
    <div class="popupTitle"><span><spring:message code="contents.view.detail"/></span></div>
    <div class="popupContent" style="max-height:550px; overflow-y:auto; border:solid 1px #ddd; margin-bottom:10px;">
        <ul>
            <li>
                <label><spring:message code="contents.table.name" /></label>
                <div class='detilBox align_l' id="contsTitle"></div>
            </li>
            <li>
                <label><spring:message code="cp.comn.name" /></label>
                <div class='detilBox align_l' id="cp"></div>
            </li>
            <li>
                <label><spring:message code="cms.release.table.service" /></label>
                <div class='detilBox align_l' id="serviceList"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.register.id" /></label>
                 <div class='detilBox align_l' id="cretrID"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.registDate" /></label>
                <div class='detilBox align_l' id="cretDt"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.update.user.id" /></label>
                <div class='detilBox align_l' id="amdrID"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.update.dt" /></label>
                <div class='detilBox align_l' id="amdDt"></div>
            </li>
            <li>
                <label><spring:message code="column.title.category" /></label>
                <div class='detilBox align_l' id="contCtgNm"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.genre" /></label>
                <div class='detilBox align_l' id="genreList"></div>
            </li>
            <li>
                <label><spring:message code="table.progressStatus" /></label>
                <div class='detilBox align_l' id="sttus"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.exhibition.Dt" /></label>
                <div class='detilBox align_l' id="cntrctDt"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.subTitle" /></label>
                <div class='detilBox align_l' id="contsSubTitle"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.detail.explane" /></label>
                <div class='detilBox align_l' id="contsDesc"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.thumbnail" /></label>
                <div class='detilBox align_l' id="thumbnailList"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.videoFile" /></label>
                <div class='detilBox align_l' id="videoList"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.exefile"/></label>
                <div class='detilBox align_l'><span class="article" id="file"></span></div>
            </li>
        </ul>
    </div>

    <!-- Button Group S -->
    <div class="btnGrp btnCenter">
        <input type="button" class="btnNormal btnCancel" value="<spring:message code="button.close" />" onclick="closePopups()">
    </div>
    <!-- Button Group E -->

</div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>