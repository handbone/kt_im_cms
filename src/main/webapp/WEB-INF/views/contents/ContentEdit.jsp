<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/swfupload/video_handlers.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/ContentEdit.js"></script>



<input type="hidden" id="leftNum" value="0">
<input type="hidden" id="leftSubMenuNum" value="1">
<input type="hidden" id="contsSeq" value="${contsSeq}">

<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="contents.table.edit"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <form id="fileForm">
                <input type="hidden" id="contsSeq" name="contsSeq" value="${contsSeq}">
                <input type="hidden" id="fileSe" name="fileSe">
                <input type="hidden" id="orginlFileNm" name="orginlFileNm">
                <input type="hidden" id="fileDir" name="fileDir">
                <input type="hidden" id="fileSize" name="fileSize">
                <input type="hidden" id="fileExt" name="fileExt">
                <input type="hidden" id="streFileNm" name="streFileNm">
            </form>


            <!-- Table Area S -->
            <div class="content">
                <table class="tableForm">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="table.contentsTitle"/></th>
                            <td><input type="text" class="inputBox lengthXXL" id="contsTitle" name="contsTitle"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="table.progressStatus"/></th>
                            <td id="sttus"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="table.category"/></th>
                            <td>
                                <select class="selectBox" id="firstCtg" onchange="contsCtgList()"></select> &gt;
                                <select class="selectBox" id="secondCtg"></select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.space.genre"/>
                            </th>
                            <td id="genreList">
                               <!-- 장르 리스트 -->
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.service"/></th>
                            <td id="serviceList">
                                <!-- 서비스 리스트 -->
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.space.id"/></th>
                            <td id="contsID"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.maxPeople"/></th>
                            <td>
                                <select class="selectBox" name="maxAvlNop" id="maxAvlNop">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th><spring:message code="contents.table.subTitle"/></th>
                            <td><textarea rows="10" class="textareaBox" id="contsSubTitle" name="contsSubTitle"></textarea></td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.detail "/></th>
                            <td><textarea rows="10" class="textareaBox" id="contsDesc" name="contsDesc"></textarea></td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.space.fileType"/></th>
                            <td>
                                <select class="selectBox" id="exefileType">
                                    <option value="ZIP">ZIP</option>
                                    <option value="EXE">EXE</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.exeFilePaths"/></th>
                            <td>
                                <input type="text" class="inputBox lengthXXL" name="exeFilePath" id="exeFilePath">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.company"/></th>
                            <td>
                                <select class="selectBox" id="cpList">
                                    <!-- 콘텐츠 제공사 리스트 -->
                                </select>
                            </td>
                        </tr>
                        <tr class="prevPart">
                            <th><spring:message code="contents.table.prev.regist"/></th>
                            <td>
                                <div id="prev_btn_placeholder"></div>
                                <div id="prevArea" style="display: none;"></div>
                            </td>
                        </tr>
                        <tr style="display:none;" class="videoPart">
                            <th><spring:message code="contents.table.prev.regist"/></th>
                            <td>
                                <div id="video_btn_placeholder"></div>
                                <div id="videoArea"></div>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.cover.img.regist"/></th>
                            <td>
                                <div id="coverImg_btn_placeholder"></div>
                                <div id="coverImgArea"></div>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.thumbnail.regist"/></th>
                            <td>
                                <div id="thumbnail_btn_placeholder"></div>
                                <div id="thumbnailArea"></div>
                            </td>
                        </tr>
                        <tr style="display:none;" class="gamePart">
                            <th><spring:message code="contents.table.exefile"/></th>
                            <td>
                                <div id="file_btn_placeholder"></div>
                                <div id="fileArea"></div>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="contents.table.exhibition.Dt"/></th>
                            <td>
                                <input type="text" class="lengthS borderBox datepicker" id="cntrctStDt" name="cntrctStDt"> -
                                <input type="text" value="2018.9.1" class="lengthS borderBox datepicker" id="cntrctFnsDt" name="cntrctFnsDt">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="menu.service.device.version"/></th>
                            <td>
                                <span id="version"></span>
                                <input type="checkbox" id="versionModify"><spring:message code="contents.table.version.msg"/>
                            </td>
                        </tr>
                    </tbody></table>

            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" onclick="pageMove('/contents')" value="<spring:message code='title.list'/>">
                <input type="button" class="btnNormal btnWrite btnLeft cursorPointer" onclick="contentsEdit();" value="<spring:message code='button.update.confirm'/>">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- Contents E -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display:none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>"  width="50%" height="24" id="barWidth"><br>
            <span id="uploadPercent">0</span>
            <span id="uploadText"></span>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>