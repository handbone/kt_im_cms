<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/GenreList.js?version=201805311709"></script>
<style>
.popupWinSmall {width:520px; height:340px; border:solid 1px #999; position:absolute; z-index:5000; background-color:#fff; padding:20px; box-shadow:5px 5px 2px #ddd;  display:none}
.popupWinSmall .popupTitle {padding:10px 5px; font-size:18px; font-weight:600; width:100%; position:relative; text-align:center; border-bottom:solid 2px #333;}
.popupWinSmall .popupContent {font-size:14px; padding:10px 0;}
.popupWinSmall .popupContent li {border-bottom:solid 1px #ddd; padding:5px;}
.popupWinSmall .popupContent label {width:160px; display:inline-block;}
.popupWinSmall .btnGrp {border:solid 0px #f30; display:inline-block;}
.popupWinSmall .alignCenter {margin:0 auto; position:absolute; left:140px; bottom:30px;}
</style>

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

            <!-- locate S -->
            <div id="locateGrp">
                <div class="locate">
                    <ul>
                    </ul>
                </div>
            </div>
            <!-- locate E -->

            <!-- page title S -->
            <div class="conTitleGrp">
                <div class="conTitle tabOn tabEnd"><spring:message code="contents.genre.list.title"/></div>
            </div>
            <!-- page title E --> 

            <!-- content S --> 
            <div class="content">
                <!-- harf div S -->
                <div id="harfGrp">
                    <!-- harf left category S -->
                    <div class="divHarf harfLeft height230">
                        <h1 class="harfTitle"><spring:message code="contents.category.first.msg"/></h1>
                        <!-- category S -->
                        <div id="firstCategoryList" class="harfTable scrollBox">
                            <ul>
                            </ul> 
                        </div>
                        <!-- category E --> 

                    </div>
                    <!-- harf left category E -->

                    <!-- harf right category S -->
                    <div class="divHarf harfRight height230">
                      <h1 class="harfTitle"><spring:message code="contents.genre.genreList.msg"/></h1>
                      <div class="catBtnGrp">
                            <span><input type="button" class="btnNormalWhite" value="<spring:message code='button.addition'/>" onclick="showGenrePopup('insert')"></span>
                            <span><input type="button" class="btnNormalWhite" value="<spring:message code='button.update'/>" onclick="showGenrePopup('update')"></span>
                            <span><input type="button" class="btnNormalWhite" value="<spring:message code='button.delete'/>" onclick="confirmDeleteGenre()"></span>
                        </div>

                        <!-- category S -->
                        <div id="genreList" class="harfTable scrollBox">
                            <ul>
                            </ul> 
                        </div>
                        <!-- category E --> 
                    </div>
                    <!-- harf right category E -->
                </div>                  
                <!-- harf div E -->
                <!-- Table Area S -->
                <div id="gridArea" class="">
                    <table id="jqgridData"></table>
                    <div id="pageDiv"></div>
                </div>
                <!-- Table Area E -->
             </div>
            <!-- content E -->
        </div>
    <!-- Middle E -->
    </div>
    <!-- Right Contents E -->

<!-- New Genre S -->
<div id="genreListForm" class="popupWinSmall" style="display:none;"> 
    <div class="popupTitle"><spring:message code="contents.genre.regist.msg"/></div>
    <div class="popupContent">
        <ul>
            <li><label><spring:message code="contents.category.firstCategory.name.msg"/></label><span id="firstCategoryName"></span></li>
            <li><label><spring:message code="contents.category.firstCategory.id.msg"/></label><span id="firstCategoryId"></span></li>
            <li><label><spring:message code="contents.genre.name.msg"/></label><span><input id="genreName" type="text" class="inputBox lengthL remaining" max="50"></span></li>
            <li><label><spring:message code="contents.genre.id.msg"/></label><span><input id="genreId" type="text" class="inputBox lengthL" maxlength="1" onkeyup="categoryValueValidationCheck(this)"></span></li>
        </ul>
    </div>
    <div class="btnGrp alignCenter">
            <span><input id="btnSave" type="button" class="btnNormal btnWrite" value="<spring:message code='button.create.confirm'/>"></span>
            <span><input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>" onclick="closeGenrePopup();"></span>
        </div>
</div>
<!-- New Genre E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>