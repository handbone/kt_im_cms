<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->
<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/mypage/MyPageDetail.js?version=201806051515"></script>

<input type="hidden" id="seq" value="${seq}">

        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

            <div class="conTitleGrp">
                <div class="conTitle"><spring:message code="member.mypage.detail"/></div>
            </div>

            <!-- content S -->
            <div class="content">
                    
                <!-- Table Area S -->
                <div class="tableArea">

                    <table class="tableDetail">
                        <tr>
                            <th class="w150"><spring:message code="login.input.name"/></th>
                            <td id="memberName"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.id"/></th>
                            <td id="memberId"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.section"/></th>
                            <td id="memberSection"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.telNo"/></th>
                            <td id="memberTelNo"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.phoneNo"/></th>
                            <td id="memberPhoneNo"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.email"/></th>
                            <td id="memberEmail"></td>
                        </tr>
                        <tr style="display:none;">
                            <th><spring:message code="login.input.ipAddress"/></th>
                            <td id="memberIpadr"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="table.useYn"/></th>
                            <td id="memberStatusName"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.cretrId"/></th>
                            <td id="cretrId"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.cretDt"/></th>
                            <td id="cretDt"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.amdrId"/></th>
                            <td id="amdrId"></td>
                        </tr>
                        <tr>
                            <th><spring:message code="common.amdDt"/></th>
                            <td id="amdDt"></td>
                        </tr>
                    </table>

                
                </div>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp">
                    <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code='button.update' />" style="display:none;">
                    <input type="button" class="btnNormal btnHisModify btnLeft" value="<spring:message code='button.updateHistory'/>">
                    <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='button.delete'/>" style="display:none;">
                </div>
                <!-- Button Group E -->

            </div>
            <!-- content E -->
            
        </div>
    </div>
    <!-- right contents E -->

    <!-- popup S -->
    
    <div id="popupHistory" style="display:none;">
        <div class="historyGrp txtCenter">
            <h2 class="title" style="border-bottom:solid 3px #333; margin-bottom:10px; font-size:22px;"><spring:message code="button.updateHistory"/></h2>
            <div class="historyList">
            <ul>
            </ul>
            </div>
            <div class="btnGroup"><input type="button" class="btnNormal btnClose btnCenter" value="<spring:message code='button.close'/>"></div>
        </div>
    </div>
    
    <!-- popup E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>