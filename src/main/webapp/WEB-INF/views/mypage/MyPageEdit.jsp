<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->
<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/mypage/MyPageEdit.js?version=201806121515"></script>

<input type="hidden" id="seq" value="${seq}">
<input type="hidden" id="memberSec" value="${memberSec}" />

        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

            <div class="conTitleGrp">
                <div class="conTitle"><spring:message code="member.mypage.modify"/></div>
            </div>

            <!-- content S -->
            <div class="content">

                <!-- Table Area S -->
                <div class="tableArea">

                    <table class="tableForm">
                        <tr>
                            <th class="w150"><spring:message code="login.input.id"/></th>
                            <td><span id="memberId"></span></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.password"/></th>
                            <td><span><input id="btnPwd" type="button" class="btnSmallWhite btnModifyWhite btnModify" value="<spring:message code='button.change'/>"></span>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.section"/></th>
                            <td><span id="memberSection"></span></td>
                        </tr>
                        <tr style="display: none;">
                            <th><spring:message code="login.input.service"/></th>
                            <td><span id="memberService"></span></td>
                        </tr>
                        <tr style="display: none;">
                            <th><spring:message code="login.select.store"/></th>
                            <td><span id="memberStore"></span></td>
                        </tr>
                        <tr style="display: none;">
                            <th><spring:message code="login.input.cp"/></th>
                            <td><span id="memberCp"></span></td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.name"/></th>
                            <td>
                                <input id="memberName" type="text" class="inputBox lengthS" value="">
                                <br><span id="memberNameMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.telNo"/></th>
                            <td>
                                <input id="memberTelNo" type="text" class="inputBox lengthL" value="">
                                <br><span id="memberTelNoMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.phoneNo"/></th>
                            <td id="memberPhoneNoCol">
                                <div class="margin_2">
                                    <p class="memberPhoneBox">
                                        <input type="text" class="inputBox lengthL memberPhoneNo" id="memberPhoneNo0">
                                        <input type="hidden" class="oriTelList" value="1" />
                                        <input type="button" class="rightIconPlus rightBtn" id="mPlus"/>
                                        <br><span class="cautionTxt"></span>
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="login.input.email"/></th>
                            <td>
                                <input id="memberEmailId" type="text" class="lengthS inputBox"> @ <input id="memberEmailDomain" type="text" class="lengthS inputBox"> &nbsp;
                                <select id="memberDomainList" class="lengthS inputBox">
                                </select>
                                <br><span id="memberEmailIdMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <th><spring:message code="login.input.ipAddress"/></th>
                            <td>
                                <input id="memberIpadr" type="text" class="inputBox lengthM" value="">
                                <input id="memberIpadr2" type="text" class="inputBox lengthM" value="">
                                <br><span id="memberIpadrMsg" class="cautionTxt"></span><span id="memberIpadr2Msg" class="cautionTxt"></span>
                            </td>
                        </tr>
                    </table>


                </div>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp txtCenter">
                    <input id="btnUpdate" type="button" class="btnNormal btnModify" value="<spring:message code='button.update.confirm'/>">
                    <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>">
                </div>
                <!-- Button Group E -->

            </div>
            <!-- content E -->

        </div>
    </div>
    <!-- right contents E -->

<!-- Popup Window S -->
<div id="popupWinidpw" style="display:none;">
    <div class="popupTitle"><span><spring:message code="login.passwordChange.msg"/></span></div>
    <div class="popupContent">
        <ul>
            <li><input id="oldPassword" type="password" class="inputBox itemW100" placeholder="<spring:message code='login.passwordChange.old.msg'/>"></li>
            <li><input id="updatePwd1" type="password" class="inputBox itemW100" placeholder="<spring:message code='login.passwordChange.new1.msg'/>"></li>
            <li><input id="updatePwd2" type="password" class="inputBox itemW100" placeholder="<spring:message code='login.passwordChange.new2.msg'/>"></li>
            <li>* <spring:message code='login.alert.password.msg'/></li>
        </ul>
    </div>


    <!-- Button Group S -->
    <div class="btnGrp btnCenter2">
        <input type="button" class="btnNormal btnModify" value="<spring:message code='button.update'/>">
        <input type="button" class="btnNormal btnCancel" value="<spring:message code='button.reset'/>">
    </div>
    <!-- Button Group E -->

</div>
<!-- Popup Window E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>