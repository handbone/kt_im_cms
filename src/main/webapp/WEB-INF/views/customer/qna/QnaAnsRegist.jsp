<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/swfupload/video_handlers.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/customer/qna/QnaAnsRegist.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/ckeditor/ckeditor.js"></script>

<style>
#qnaSbst img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<input type="hidden" id="leftNum" value="4">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="qnaSeq" value="${qnaSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="qna.answer.regist.title"/></div>
        </div>
        <!-- Tab E -->

        <form id="fileForm">
            <input type="hidden" id="contsSeq" name="contsSeq"> <input
                type="hidden" id="fileSe" name="fileSe"> <input
                type="hidden" id="orginlFileNm" name="orginlFileNm"> <input
                type="hidden" id="fileDir" name="fileDir"> <input
                type="hidden" id="fileSize" name="fileSize"> <input
                type="hidden" id="fileExt" name="fileExt"> <input
                type="hidden" id="streFileNm" name="streFileNm">
        </form>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class=tableDetail>
                    <tr>
                        <th class="width150"><spring:message code="qna.cretr.name"/></th>
                        <td><span class="article" id="qnaCretrNm"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.regdate"/></th>
                        <td><span class="article" id="qnaRegDt"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.category"/></th>
                        <td><span class="article" id="qnaCtg"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="qna.title"/></th>
                        <td><span class="article" id="qnaTitle"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="qna.content"/></th>
                        <td><span class="article" id="qnaSbst"></span></td>
                    </tr>
                    <tr>
                        <th><spring:message code="qna.answer.plus.sbst"/></th>
                        <td><textarea class="inputBox itemW100 ckeditor" rows="10" id="ansSbst"></textarea></td>
                    </tr>
                    <tr>
                        <th><spring:message code="common.attachfile"/></th> 
                        <td>
                            <div id="file_btn_placeholder"></div>
                            <div id="fileArea" style="display: none;"></div>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp txtCenter">
                <input type="button" class="btnNormal btnWrite cursorPointer" value="<spring:message code="button.create.confirm"/>" onclick="qnaAnsRegist()">
                <input type="button" class="btnNormal btnCancel cursorPointer" value="<spring:message code="button.reset"/>" onclick="pageMove('/customer/qna')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->

    </div>
</div>
<!-- right contents E -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display: none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>" width="0%" height="24"
                id="barWidth"><br> <span id="uploadPercent">0</span> <span
                id="uploadText"></span>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/views/frame/footer.jsp"%>