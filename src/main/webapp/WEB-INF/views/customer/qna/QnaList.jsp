<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/customer/qna/QnaList.js"></script>
<!-- Left Menu E -->
<input type="hidden" id="leftNum" value="4">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="qna.list.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- content S -->
        <div class="content">
                
            <!-- Table Area S -->
            <div id="gridArea">
                <!-- jQGrid S -->
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
                <!-- jQGrid E -->
            </div>
            <!-- Table Area E -->

            <div class="CenterGrp">
                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code="button.search"/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->
            </div>
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>