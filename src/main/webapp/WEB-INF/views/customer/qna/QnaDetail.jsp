<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/libs/ckeditor/skins/moono-lisa/editor.css">
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/customer/qna/QnaDetail.js"></script>
<style>
#qnaSbst img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}

#ansSbst img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<input type="hidden" id="leftNum" value="4">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="qnaSeq" value="${qnaSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="qna.detail.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- content S -->
        <div class="content qna">
            <div class="qnaTitleQsn qnaQuestion" id="qnaTitle"></div>
            <!-- Detail Area S -->
            <div class="qnaDetail noBasic" id="qnaSbst"></div>
            <!-- Attatch S -->
            <div class="attatchGrp" id="qnaFileGrp">
            </div>
            <!-- Attatch S -->
            <!-- Detail Area E -->

            <div class="qnaTitleAns qnaAnswer" id="ansTitle"></div>
            <!-- Detail Area S -->
            <div class="qnaDetail noBasic" id="ansSbst"></div>
            <!-- Attatch S -->
            <div class="attatchGrp" id="ansFileGrp">
            </div>
            <!-- Attatch S -->
            <!-- Detail Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnWrite btnLeft cursorPointer" id="ansBtn" value="<spring:message code="button.answer.create"/>" onclick="pageMove('/customer/qna/${qnaSeq}/registAns')">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" value="<spring:message code="button.list"/>" onclick="pageMove('/customer/qna')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>