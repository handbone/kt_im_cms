<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/customer/notice/NoticeDetail.js"></script>
<input type="hidden" id="leftNum" value="4">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" id="noticeSeq" value="${noticeSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />

<style>
#noticeSbst img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="notice.detail.title"/></div>
        </div>

        <!-- content S -->
        <div class="content notice">
            <div class="noticeTitle" id="noticeTitle"></div>
            <!-- Detail Area S -->
            <div class="viewDetail noBasic" id="noticeSbst"></div>
            <!-- Detail Area E -->

            <!-- Attatch S -->
            <div class="attatchGrp" id="fileGrp">
            </div>
            <!-- Attatch S -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnModify btnLeft cursorPointer" id="btnUpdate" value="<spring:message code="button.update"/>" onclick="pageMove('/customer/notice/${noticeSeq}/edit')">
                <input type="button" class="btnNormal btnDelete btnLeft cursorPointer" id="btnDelete" value="<spring:message code="button.delete"/>" onclick="deleteNotice()">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" value="<spring:message code="button.list"/>" onclick="pageMove('/customer/notice')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->

    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>