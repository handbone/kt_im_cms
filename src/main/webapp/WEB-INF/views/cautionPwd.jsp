<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="login.title"/></title>
<script src="<%=request.getContextPath()%>/resources/js/message.js?category=login&time=<%=session.getLastAccessedTime()%>"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-1.10.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/apiFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/baseFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/commonFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/cookie.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/views/cautionPwd.js?version=201806291748"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/sweetalert/sweetalert.min.js"></script>

<link href="<%=request.getContextPath()%>/resources/css/common.css?version=201805081745" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/resources/css/custom.css?version=201805081745" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/resources/css/implatformcms.css?version=1" rel="stylesheet" type="text/css">

<style>
.swal-title {
    margin: 0px;
    font-size: 16px;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
    margin-bottom: 28px;
}

.swal-button {
    background-color: #000000;
}

.swal-button[not :disabled]:hover {
    background-color: #A6A6A6;
}

.swal-button:active {
    background-color: #A6A6A6;
}

.swal-button:focus {
    box-shadow: 0 0 0 0;
}
</style>
</head>

<body>


<div id="wrapper">
    <input type="hidden" id="contextPath" value="<%=request.getContextPath()%>">
    <input type="hidden" id="mbrId" value="${tempLoginId}">
    <!-- Top S -->
    <div id="top">
        <div class="topGrp loginTop">
            <h1 class="logo"><img src="<c:url value='/resources/image/logo_cms.png'/>" alt="<spring:message code='login.title'/>"></h1>
        </div>
    </div>
    <!-- Top E -->

    <!-- Middle S -->
    <div id="pwdloginMiddle">
        <div class="pwdTitle"><span class="h1 textBold"><spring:message code='login.change.pwd.title1'/></span><span class="h1 textBold"><spring:message code='login.change.pwd.title2'/></span><span class="h1 textBold"><spring:message code='login.change.pwd.title3'/></span></div>
        <div class="pwdLoginGrp">
            <div class="pwdGrp">
                <div class="pwdDescription">
                    <div><span class="dotted">●</span><span><spring:message code='login.change.pwd.description1'/></span></div>
                    <div><span class="dotted">●</span><span><spring:message code='login.change.pwd.description2'/></span></div>
                    <div><span class="dotted">●</span><span><spring:message code='login.change.pwd.description3'/></span></div>
                </div>
                <ul>
                    <li><input id="oldPassword" type="password" class="inputBox itemW100" placeholder="<spring:message code='login.passwordChange.old.msg'/>"></li>
                    <li><input id="updatePwd1" type="password" class="inputBox itemW100" placeholder="<spring:message code='login.passwordChange.new1.msg'/>"></li>
                    <li><input id="updatePwd2" type="password" class="inputBox itemW100" placeholder="<spring:message code='login.passwordChange.new2.msg'/>"></li>
                </ul>

                <div><input id="btnChangePwd" type="button" class="btnGrp" value="<spring:message code='login.passwordChange.msg'/>"></div>
            </div>
        </div>
    </div>
    <!-- Middle E -->

    <div id="bottom" class="bottomLine">Copyright © <spring:message code='menu.title'/> Co.,Ltd.  All Rights Reserved.</div>
    <form id="NoneForm"></form>
</div>

</body>
</html>