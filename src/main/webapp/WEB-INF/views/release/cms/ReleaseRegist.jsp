<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<input type="hidden" id="tagSeq" value="${tagSeq}">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js//libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/release/cms/ReleaseRegist.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/libs/swipebox/swipebox.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swipebox/jquery.swipebox.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/justifiedGallery/justifiedGallery.min.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>

<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="4">
<input type="hidden" id="frmtnSeq" value="0">
<input type="hidden" id="svcSeq" value="${svcSeq}">

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
       <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp" style="margin-bottom:30px;">
            <div class="conTitle"><spring:message code="cms.release.table.regist"/></div>
        </div>

        <!-- formTitleGrp  S -->
        <div id="formTitleGrp" class="boxRoundSolid">
            <div class="ItemOne">
                <span class="sname textBold"><spring:message code="reservation.table.service"/></span>
                <span>
                    <select id="svcList" class="inputBox3 lengthL" onchange="getContentsInfo();">
                    </select>
                </span>
            </div>
            <div class="ItemOne">
                <span class="sname textBold"><spring:message code="cms.release.table.releaseNm"/></span>
                <span>
                    <input type="text" class="inputBox3 lengthL remaining" max="30" id="frmtnNm">
                </span>
                <span class="name" style="padding-top:3px; padding-left: 5px;">
                    <input type="button" value="<spring:message code="cms.release.table.checkId"/>" class="btnDoubleGray" onclick="releaseOverlap()" id="overlap"/>
                    <span></span>
                </span>
            </div>
        </div>
        <!-- formTitleGrp E -->

        <!-- content S -->
        <div class="content">

            <!-- harfGrp S -->
            <div id="harfGrp">
                <!-- harf left table S -->
                <div class="divHarf harfLeft">
                    <h1 class="harfTitle"><spring:message code="cms.release.conts.allList"/></h1>

                    <!--  searchBox S-->
                    <div class="searchBox2">
                        <div class="searchGrpWhite">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td width="105">
                                            <select style="height:30px;" id="searchSel">
                                                <option><spring:message code="table.contentsTitle" /></option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="inputBox4" style="width:100%;" id="searchKeyword" onkeypress="if(event.keyCode==13) {searchOnConts(); return false;}">
                                        </td>
                                        <td width="85" style="text-align:right; padding-right:5px;">
                                            <input type="button" class="btnSearchLongWhite btnSmallWhite lengthSS" value="<spring:message code="button.search" />" style="height:30px;" onclick="searchOnConts()">
                                        </td>
                                        <td width="100">
                                            <input type="button" class="btnResetWhite btnSmallWhite lengthS" value="<spring:message code="button.allView" />" onclick="searchOnContsReset()" style="height:30px;">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--  searchBox E-->

                    <div class="scroll-table">
                        <!-- table S -->
                        <div class="scrollField">
                            <table class="titleTable fix_table listBox tableList">
                                <thead>
                                    <tr>
                                        <th class="width50 chkBox"><input type="checkbox" id="contsAllCb" onchange="checkboxAll('conts')"></th>
                                        <th><spring:message code="common.contents.nm"/></th>
                                        <th class="itemW30"><spring:message code="cms.release.table.ctgNm"/></th>
                                        <th class="itemW20">등록일</th>
                                    </tr>
                                </thead>
                                <tbody id="selectContents">
                                </tbody>
                            </table>
                        </div>
                        <!-- table E -->
                    </div>
                </div>
                <!-- harf left table E -->

                <!-- button S -->
                <div class="addBtns">
                    <div class="btnLeftArrow cursorPointer" onclick="deleteContentsInfo()"><img src="<%=request.getContextPath()%>/resources/image/btn_left.gif"></div>
                    <div class="btnRightArrow cursorPointer" onclick="insertContentsInfo()"><img src="<%=request.getContextPath()%>/resources/image/btn_right.gif"></div>
                </div>
                <!-- button E -->

                <!-- harf right table S -->
                <div class="divHarf harfRight">
                    <h1 class="harfTitle" style="margin-bottom: 40px;"><spring:message code="cms.release.conts.frmtnlist"/></h1>

                    <div class="scroll-table">
                        <!-- table S -->
                        <div class="scrollField">
                            <table class="titleTable fix_table listBox tableList">
                                <thead>
                                    <tr>
                                        <th class="width50 chkBox"><input type="checkbox" id="groupContentsAllCb" onchange="checkboxAll('groupContents')"></th>
                                        <th><spring:message code="common.contents.nm"/></th>
                                        <th class="itemW30"><spring:message code="cms.release.table.ctgNm"/></th>
                                    </tr>
                                </thead>
                                <tbody id="selectVRContents">
                                </tbody>
                            </table>
                        </div>
                        <!-- table E -->
                    </div>

                </div>
                    <!-- harf right table E -->
            </div>
            <!-- harfGrp E -->

                <!-- Button Group S -->
                <div class="btnGrp txtCenter">
                    <input type="button" class="btnNormal btnWrite" onclick="applyRelease()" value="<spring:message code="button.create.confirm"/>">
                    <input type="button" class="btnNormal btnCancel" onclick="moveToReturnPage()" value="<spring:message code="button.reset"/>">
                </div>
                <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->


<div id="popupWinBig">
    <div class="popupTitle"><span><spring:message code="contents.view.detail"/></span></div>
    <div class="popupContent" style="max-height:550px; overflow-y:auto; border:solid 1px #ddd; margin-bottom:10px;">
        <ul>
            <li>
                <label><spring:message code="contents.table.name" /></label>
                <div class='detilBox align_l' id="contsTitle"></div>
            </li>
            <li>
                <label><spring:message code="cp.comn.name" /></label>
                <div class='detilBox align_l' id="cp"></div>
            </li>
            <li>
                <label><spring:message code="cms.release.table.service" /></label>
                <div class='detilBox align_l' id="serviceList"></div>
            </li>
            <li>
                <label><spring:message code="column.title.category" /></label>
                <div class='detilBox align_l' id="contCtgNm"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.genre" /></label>
                <div class='detilBox align_l' id="genreList"></div>
            </li>
            <li>
                <label><spring:message code="table.progressStatus" /></label>
                <div class='detilBox align_l' id="sttus"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.exhibition.Dt" /></label>
                <div class='detilBox align_l' id="cntrctDt"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.subTitle" /></label>
                <div class='detilBox align_l' id="contsSubTitle"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.detail.explane" /></label>
                <div class='detilBox align_l' id="contsDesc"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.thumbnail" /></label>
                <div class='detilBox align_l' id="thumbnailList"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.videoFile" /></label>
                <div class='detilBox align_l' id="videoList"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.exefile"/></label>
                <div class='detilBox align_l'><span class="article" id="file"></span></div>
            </li>
            <li>
                <label><spring:message code="contents.table.register.id" /></label>
                 <div class='detilBox align_l' id="cretrID"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.registDate" /></label>
                <div class='detilBox align_l' id="cretDt"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.update.user.id" /></label>
                <div class='detilBox align_l' id="amdrID"></div>
            </li>
            <li>
                <label><spring:message code="contents.table.update.dt" /></label>
                <div class='detilBox align_l' id="amdDt"></div>
            </li>
        </ul>
    </div>

    <!-- Button Group S -->
    <div class="btnGrp btnCenter">
        <input type="button" class="btnNormal btnCancel" value="<spring:message code="button.close" />" onclick="closePopups()">
    </div>
    <!-- Button Group E -->
</div>

<!-- popupDetailSubmetadata S -->
<div id="popupMetaWin" style="display: none;">
    <div class="popupTitle" id="popupMetaTitle"><spring:message code='submeta.confirm.title' /></div>
    <!-- Table Area S -->
    <div id="metaTableArea" class="metaGrp">
        <table id="detailMeta">
        </table>
    </div>
    <!-- Table Area E -->
    <!-- Button Group S -->
    <div class="btnGrp txtCenter">
        <input type="button" class="btnNormal btnCancel" value="<spring:message code='submeta.button.close'/>" onclick="closeMetaPopups()">
    </div>
    <!-- Button Group E -->
</div>
<!-- popupDetailSubmetadata E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>