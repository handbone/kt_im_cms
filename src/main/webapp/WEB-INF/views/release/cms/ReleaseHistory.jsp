<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<input type="hidden" id="tagSeq" value="${tagSeq}">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js//libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/release/cms/ReleaseHistory.js"></script>
<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="4">
<input type="hidden" id="frmtnSeq" value="${frmtnSeq}">


<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- page title S -->
        <div class="conTitleGrp" style="margin-bottom:30px;">
            <div class="conTitle"><spring:message code="cms.release.table.history.detail"/></div>
        </div>
        <!-- page title E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="formTitleGrp" class="boxRoundSolid">
                <div class="ItemOne">
                    <span class="sname textBold"><spring:message code="cms.release.table.releaseNm"/> :</span>
                    <span class="name" id="frmtnNm" style="padding-top: 4px;"></span>
                </div>
                <div class="ItemOne">
                    <span class="sname textBold"><spring:message code="cms.release.noSpace.table.service"/> :</span>
                    <span class="name" id="svcNm" style="padding-top: 4px;"></span>
                </div>
            </div>
            <!-- Table Area E -->

            <input type="hidden" id="offset" value="0">
            <input type="hidden" id="limit" value="10">

            <!-- Table Area S -->
            <div class="tableArea">
                <span class="title line-height30"><spring:message code="cms.release.table.registCnt"/></span>
                <div class='releaseScrollBoxs'>
                    <table class="tableList box">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <span class="title line-height30"><spring:message code="cms.release.table.delete"/></span>
                <div class='releaseScrollBoxs'>
                    <table class="tableList box">
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" onclick="pageMove('/cms/release')" value="<spring:message code="button.list"/>">
            </div>
            <!-- Button Group E -->
        </div>
    </div>
    <!-- content E -->
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>