<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/release/cms/ReleaseList.js"></script>

<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="4">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" id="type" value="list">

<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp S -->
        <div class="conTitleGrp">
                <div class="conTitle tabOn"><a href="javascript:;" onclick="tabVal(0,this)"><spring:message code='cms.release.table.list'/></a></div>
                <div class="conTitle tabOff tabEnd"><a href="javascript:;" onclick="tabVal(1,this)"><spring:message code='cms.release.table.history'/></a></div>
        </div>
        <!-- tabGrp E -->

        <div class="rsvGrp rsvGrpPos">
            <div class="rsvSelect" id="svc">
                <span><spring:message code='content.service.name'/></span>
                <span>
                    <select id="svcList" onchange="keywordSearch()">
                    </select>
                </span>
            </div>
        </div>



        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea" class="">
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
            </div>
            <!-- Table Area E -->

            <!-- btnGrp S -->
            <div class="CenterGrp">
                <div class="cellTable">
                    <input type="button" class="btnNormal btnDelete" value="<spring:message code="button.delete"/>" onclick="deleteRelease()">
                </div>
                <!-- Search Group S -->
                <div class="searchBox">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code='button.search'/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->
                <div class="cellTable">
                    <input type="button" class="btnNormal btnWrite" value="<spring:message code="button.create"/>" onclick="moveToRegistView()">
                </div>
            </div>
            <!-- btnGrp E -->
        </div>
        <!-- content E -->


    </div>
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>