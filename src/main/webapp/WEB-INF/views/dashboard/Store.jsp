<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%> 
<%@include file="/WEB-INF/views/frame/left_menu.jsp" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/dashboard/Store.js"></script>
<link href="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.js"></script>

<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="storSeq" value="${storSeq}">
<input type="hidden" id="memberSec" value="${memberSec}">

        <!-- right contents S -->
        <div id="contents">
            <div class="boxRound">

            <!-- page title S -->
            <div class="conTitleGrp">
                <div class="conTitle"><spring:message code="menu.dashboard.store"/></div>
            </div>
            <!-- page title E -->

            <!-- Select Box S -->
            <div class="rsvGrp rsvGrpPos" style="width:360px">
                <div class="rsvSelect" style="float:left;">
                    <span><spring:message code="product.comn.seviceNm"/></span>
                    <span>
                        <select id="serviceList" disabled>
                        </select>
                    </span>
                </div>
                <div class="rsvSelect">
                    <span><spring:message code="product.comn.storeNm"/></span>
                    <span>
                        <select id="storeList" disabled>
                        </select>
                    </span>
                </div>
            </div>
            <!-- Select Box E -->

            <!-- content S -->
 
            <div class="content">
                    
                <!-- Today Stats Table S -->
                <div class="statsTodayGrp">
                    <div class="statsToday">
                        <div class="statsTitle"><h3><spring:message code="dashboard.store.today.statistics"/></h3></div>
                        <table class="statsTicketTable">
                        </table>
                    </div>
                    <div class="todayGraphGrp" style="height:360px">
                        <div class="todayGraph">
                            <div class="statsTitle"><h3><spring:message code="dashboard.store.status"/></h3></div>
                            <div class="statsTodayGraph"><canvas id="container" style="display:block;position:relative"></canvas></div>
                        </div>
                    </div>

                </div>
                <!-- Today Stats Table E -->

                <!-- Ticket Stats Table S -->
                <div class="statsTicketGrp">
                    <div class="statsTicket">
                        <div class="statsTitle"><h3><spring:message code="dashboard.store.tag.statistics"/></h3></div>
                        <table class="statsTicketTable">
                        </table>
                    </div>
                    <div class="ticketGraphGrp" style="height:360px">
                        <div class="ticketGraph">
                            <div class="statsTitle"><h3><spring:message code="dashboard.store.tag.status"/></h3></div>
                            <div class="statsTicketGraph"><canvas id="container2" style="display:block;position:relative"></canvas></div>
                        </div>
                    </div>

                </div>
                <!-- Ticket Stats Table E -->
            </div>
            <!-- content E -->
        </div>
    </div>
    <!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp" %>