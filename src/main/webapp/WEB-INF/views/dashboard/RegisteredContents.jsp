<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%> 
<%@include file="/WEB-INF/views/frame/left_menu.jsp" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/dashboard/RegisteredContents.js"></script>

<input type="hidden" id="svcSeq" value="${svcSeq}">
<input type="hidden" id="storSeq" value="${storSeq}">
<input type="hidden" id="memberSec" value="${memberSec}">

        <!-- right contents S -->
        <div id="contents">

            <div class="boxRound">

            <!-- locate S -->
            <div id="locateGrp">
                <div class="locate">
                    <ul>
                    </ul>
                </div>
            </div>
            <!-- locate E -->

            <!-- page title S -->
            <div class="conTitleGrp">
                <c:if test="${!memberSec.equals('05')}">
                <div class="conTitle tabOff"><a href="javascript:;"><spring:message code="dashboard.contents.verify.status"/></a></div>
                </c:if>
                <div class="conTitle tabOn tabEnd"><spring:message code="dashboard.contents.regist.status"/></div>
            </div>
            <!-- page title E -->

            <!-- Select Box S -->
            <div class="rsvGrp rsvGrpPos">
                <div class="rsvSelect">
                    <span><spring:message code="product.comn.seviceNm"/></span>
                    <span>
                        <select id="serviceList">
                        </select>
                    </span>  &nbsp;
                    <span><spring:message code="product.comn.storeNm"/></span>
                    <span>
                        <select id="storeList">
                        </select>
                    </span>
                </div>
            </div>
            <!-- Select Box E -->

            <!-- content S -->
 
            <div class="content">
                <div class="graphTitle"><a id="btnUsedContents" href="javascript:;"><spring:message code="dashboard.contents.regist.most.used"/></a></div>
                <div id="mostContentsContainer" class="graph"></div>
                <div class="graphTitle"><spring:message code="dashboard.contents.regist.category.status"/></div>
                <div id="categoryContainer" class="graph" style="height:400px"></div>
            </div>
            <!-- content E -->
            
        </div>
        <!-- right contents E -->
        <br>
    </div>

<%@include file="/WEB-INF/views/frame/footer.jsp" %>