<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/swfupload/video_handlers.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/version/VersionRegist.js"></script>

<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="7">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
       <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="version.table.regist"/></div>
        </div>

        <form id="fileForm">
            <input type="hidden" id="contsSeq" name="contsSeq">
            <input type="hidden" id="fileSe" name="fileSe">
            <input type="hidden" id="orginlFileNm" name="orginlFileNm">
            <input type="hidden" id="fileDir" name="fileDir">
            <input type="hidden" id="fileSize" name="fileSize">
            <input type="hidden" id="fileExt" name="fileExt">
            <input type="hidden" id="streFileNm" name="streFileNm">
        </form>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code='store.comn.serviceNm'/></th>
                            <td>
                                <select class="inputBox width200" id="svcList" onchange="storList();">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='store.comn.name'/></th>
                            <td>
                                <select class="inputBox width200" id="storList">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='version.table.name'/></th>
                            <td><input type="text" class="width400 remaining" id="verNm" max="30"></td>
                        </tr>
                        <tr>
                            <th><spring:message code='version.table.type'/></th>
                            <td>
                                <select class="inputBox width500" id="verType">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='common.attachfile'/></th>
                            <td>
                                <div id="file_btn_placeholder"></div>
                                <div id="fileArea" style="display: none;"></div>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='version.table.updateHst'/></th>
                            <td>
                                <textarea class="wi dth500" id="verHst" cols="120" rows="5" placeholder="<spring:message code='version.table.updateHst'/>"></textarea></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                    <input type="button" class="btnNormal btnWrite btnLeft" value="<spring:message code='button.create.confirm'/>" onclick="verRegister()">
                    <input type="button" class="btnNormal btnCancel btnLeft" value="<spring:message code='button.reset'/>" onclick="pageMove('/version')">
            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display:none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>"  width="0%" height="24" id="barWidth"><br>
            <span id="uploadPercent">0</span>
            <span id="uploadText"></span>
        </div>
    </div>
</div>


<%@include file="/WEB-INF/views/frame/footer.jsp"%>