<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/version/VersionDetail.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="7">
<input type="hidden" id="verSeq" value="${verSeq}">
<input type="hidden" id="storSeq">
<input type="hidden" id="svcSeq">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
       <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="version.table.detail" /></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- contentGrp Area S -->
            <div class="contentGrp">
                <div class="title">
                    <span id="verNm"></span>
                    <span class="timeWrite"><spring:message code="table.regdate" /> : <span id="cretDt"></span></span>
                </div>
                <div class="contents" id="verHst">

                </div>
            </div>
            <!-- contentGrp Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                    <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code='button.update'/>" onclick="pageMove('/version/${verSeq}/edit')">
                    <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='button.delete'/>" onclick="deleteVersion()">
                    <input type="button" class="btnNormal btnList btnRight" value="<spring:message code='button.list'/>" onclick="pageMove('/version')">
            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>