<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="login.title"/></title>
<script src="<%=request.getContextPath()%>/resources/js/message.js?category=login&time=<%=session.getLastAccessedTime()%>"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-1.10.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/apiFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/baseFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/commonFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/cookie.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/views/login.js?version=2201806071748"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/sweetalert/sweetalert.min.js"></script>

<link href="<%=request.getContextPath()%>/resources/css/common.css?version=201805081745" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/resources/css/implatformcms.css?version=1" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/resources/css/custom.css?version=201805081746" rel="stylesheet" type="text/css">

<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

.swal-title {
    margin: 0px;
    font-size: 16px;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
    margin-bottom: 28px;
}

.swal-button {
    background-color: #000000;
}

.swal-button[not :disabled]:hover {
    background-color: #A6A6A6;
}

.swal-button:active {
    background-color: #A6A6A6;
}

.swal-button:focus {
    box-shadow: 0 0 0 0;
}
</style>
</head>

<body style="background-color:#fff;">
    <input type="hidden" id="contextPath" value="<%=request.getContextPath()%>">
    <input type="hidden" id="requestUri" value="${requestUri}">
    <div id="wrapper">

    <!-- Top S -->
    <div id="top">
        <div class="topGrp loginTop">
            <h1 class="logo"><img src="<c:url value='/resources/image/logo_cms.png'/>" alt="<spring:message code='login.title'/>"></h1>
        </div>
    </div>
    <!-- Top E -->

    <!-- Middle S -->
    <div id="loginMiddle">
        <div class="loginGrp">
            <span class="big textBold"> <spring:message code="login.oneStep.msg"/>  <spring:message code="login.title.msg"/></span>
                <div class="loginBox">
                    <div class="inputID"><input id="mId" type="text" placeholder="<spring:message code='login.id.msg'/>"></div>
                    <div class="inputPW"><input id="mPwd" type="password" placeholder="<spring:message code='login.password.msg'/>"></div>
                    <div class="btnLogin"><input type="button" value="<spring:message code='login.msg'/>" onclick="Login()"></div>
                </div>
            <ul>
                <li class="loginBtnGrp">
                    <input id="idSaved" type="checkbox" class="chkbox"><label><spring:message code="login.checkbox.saveId"/></label>
                </li>
                <li class="findPW">
                    <a href="javascript:;" onclick="openFindMemberPopup('id')"><spring:message code='login.findIdTitle.msg'/></a>
                    <span class="div"> | </span>
                    <a href="javascript:;" onclick="openFindMemberPopup('password')"><spring:message code='login.findPasswordTitle.msg'/></a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Middle E -->

        <div id="bottom" class="bottomLine" style="margin-top:0px;">Copyright © <spring:message code="login.title"/> Co.,Ltd.  All Rights Reserved.</div>
        <form id="NoneForm"></form>
    </div>

<!--  Popup S -->
<div id="findMember" style="display:none;">
    <div id="loading" style="display:none;">
        <img id="loading-image" src="<c:url value='/resources/image/img/loading.gif' />" alt="Loading..." />
    </div>
    <div class="findTitle"><span id="findTitle"><spring:message code='login.findPasswordTitle.msg'/></span><div class="btnClose"><a href="javascript:;" onclick="popLayerClose()"><img src="<c:url value='/resources/image/icon_close.png'/>"></a></div></div>
    
    <div class="findForm">
        <ul>
            <li>
                <span id="findType" class="item"><spring:message code='login.input.id'/></span><input id="findId" type="text" class="joinInput lengthL">
            </li>
            <li>
                <span class="item"><spring:message code='login.input.email'/></span><input id="findEmailId" type="text" class="joinInput lengthS"> @ <input id="findEmailDomain" type="text" class="joinInput lengthS">&nbsp;
                <select id="domainList" style="height:33px;">
                </select>
            </li>
        </ul>
        <div class="btnGrp btnGrpJoin">
            <span><input type="button" class="btnNormal btnSearch btnJoin" value="<spring:message code='login.button.find'/>"></span>
            <span><input type="button" class="btnNormal btnCancel btnJoin" value="<spring:message code='button.reset'/>" onclick="popLayerClose()"></span>
        </div>
    </div>
</div>
<!--  Popup E -->

</body>
</html>