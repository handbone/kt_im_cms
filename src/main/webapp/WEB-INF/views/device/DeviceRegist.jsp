<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/device/DeviceRegist.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="6">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
       <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="device.table.regist"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tbody>
                        <tr>
                            <th class="w150"><spring:message code="service.table.name"/></th>
                            <td>
                                <select id="svcList" class="inputBox" onchange="storList();">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.storNm"/></th>
                            <td>
                                <select id="storList" class="inputBox" onchange="releaseList();">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.release"/></th>
                            <td>
                                <select id="frmtnList" class="inputBox">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.kind"/></th>
                            <td>
                                <select id="devType" class="inputBox">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.playTime"/></th>
                            <td>
                                <select id="playTime" class="inputBox" onchange="userTimeSet()">
                                </select>
                                <span id="userSetBox">
                                        &nbsp;
                                        <input type="text" class="inputBox lengthXS onlynum remaining bizno" id="userSet" max="4">
                                        &nbsp;
                                        <span>
                                            <spring:message code="common.minute"/>
                                        </span>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th>Device ID</th>
                            <td>
                                <input type="text" class="inputBox remaining" max="30" id="devId">&nbsp;
                                <span>
                                    <input type="button" class="btnDoubleGray" value="<spring:message code="device.table.checkId"/>" onclick="devIdDuplication()">
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.ipadr"/></th>
                            <td>
                                <input type="text" class="inputBox" id="devIpadr">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                    <input type="button" class="btnNormal btnWrite btnLeft" value="<spring:message code='button.create.confirm'/>" onclick="devRegister()">
                    <input type="button" class="btnNormal btnCancel btnLeft" value="<spring:message code='button.reset'/>" onclick="makeBakMove()">
            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>