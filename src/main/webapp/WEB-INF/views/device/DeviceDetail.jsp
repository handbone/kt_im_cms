<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<!-- Left Menu E -->
<input type="hidden" id="tagSeq" value="${tagSeq}">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/device/DeviceDetail.js"></script>
<input type="hidden" id="leftNum" value="3">
<input type="hidden" id="leftSubMenuNum" value="6">
<input type="hidden" id="devSeq" value="${devSeq}">
<input type="hidden" id="storSeq">
<input type="hidden" id="svcSeq">

<!-- Contents S -->
<div id="contents">
    <!-- boxRound S -->
    <div class="boxRound">
       <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <div class="conTitleGrp">
            <div class="conTitle"><spring:message code="device.table.detail"/></div>
        </div>

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div class="tableArea">
                <table class="tableDetail">
                    <tbody>
                        <tr>
                            <th>Device ID</th>
                            <td id="devId">
                            </td>
                        </tr>
                        <tr>
                            <th class="w150"><spring:message code="table.reger"/></th>
                            <td id="cretrId">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.storNm"/></th>
                            <td id="storNm">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.release"/></th>
                            <td id="frmtnNm">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.release.count"/></th>
                            <td id="frmtnCnt">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.kind"/></th>
                            <td id="devType">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.playTime"/></th>
                            <td id="playTime">
                            </td>
                        </tr>

                        <tr>
                            <th><spring:message code="device.table.ipadr"/></th>
                            <td id="devIpadr">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.macAdr"/></th>
                            <td id="macAdr">
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code="device.table.down.conts.cnt" /></th>
                            <td id="devContsCnt">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- Table Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                    <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code='button.update'/>" onclick="pageMove('/device/${devSeq}/edit')">
                    <input type="button" class="btnNormal btnDelete btnLeft" value="<spring:message code='button.delete'/>" onclick="deleteDevice()">
                    <input type="button" class="btnNormal btnList btnRight" value="<spring:message code='button.list'/>" onclick="pageMove('/device')">

            </div>
            <!-- Button Group E -->

        </div>
        <!-- content E -->
    </div>
    <!-- boxRound E -->
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>