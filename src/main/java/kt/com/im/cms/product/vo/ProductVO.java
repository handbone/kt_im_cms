/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.product.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 상품(이용권)에 대한 데이터 객체
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 16.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class ProductVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7130791756451740094L;

    /** 상품(이용권) 번호 */
    private int prodSeq;

    /** 매장 번호 */
    private int storSeq;

    /** 서비스 번호 */
    private int svcSeq;

    /** 서비스 명 */
    private String svcNm;

    /** 매장 명 */
    private String storNm;

    /** 상품(이용권) 명 */
    private String prodNm;

    /** 상품(이용권) 설명 */
    private String prodDesc;

    /** 상품(이용권) 제한 시간 */
    private int prodTimeLmt;

    /** 상품(이용권) 제한 횟수 */
    private int prodLmtCnt;

    /** 상품 가격 */
    private int prodPrc;

    /** GSR 상품 코드 */
    private String gsrProdCode;

    /** 등록자 아이디 */
    private String cretrId;

    /** 등록 일시 */
    private String cretDt;

    /** 수정자 아이디 */
    private String amdrId;

    /** 등록자(또는 수정자) 이름 */
    private String cretrNm;

    /** 등록(또는 수정) 일시 */
    private String regDt;

    /** 상품 종류 - 통계를 위해 사용 */
    private int prodKind;

    /** 사용 여부 */
    private String useYn;

    /** 정렬 컬럼명 */
    private String sidx;

    /** 정렬 방법 (DESC, ASC) */
    private String sord;

    /** 검색 영역 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 상품 카테고리 관리 번호 */
    private int prodCtgSeq;

    /** 상품 카테고리 장비 명 */
    private String devCtgNm;

    /** 상품 카테고리 장비 사용 제한 횟수 */
    private int lmtCnt;

    /** 상품 카테고리 - 사용시 전체횟수 차감 여부 */
    private String deduction;

    /** 이전 GSR 상품 코드 */
    private String beforeGsrProdCode;

    /** 매장(점포) 코드 */
    private String storCode;

    /** 삭제할 상품(이용권) 목록 */
    private int[] delProdSeqList;

    /** 콘텐츠 1차 카테고리 명 */
    private String firstCtgNm;

    /** 콘텐츠 2차 카테고리 명 */
    private String secondCtgNm;

    /** 상품 카테고리 목록 */
    private List<ProductVO> prodCategoryList;

    /** 로그인 멤버 권한 */
    private String mbrSe;

    /** 삭제 여부 */
    private String delYn;

    public int getProdSeq() {
        return prodSeq;
    }

    public void setProdSeq(int prodSeq) {
        this.prodSeq = prodSeq;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public String getProdNm() {
        return prodNm;
    }

    public void setProdNm(String prodNm) {
        this.prodNm = prodNm;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public int getProdTimeLmt() {
        return prodTimeLmt;
    }

    public void setProdTimeLmt(int prodTimeLmt) {
        this.prodTimeLmt = prodTimeLmt;
    }

    public int getProdLmtCnt() {
        return prodLmtCnt;
    }

    public void setProdLmtCnt(int prodLmtCnt) {
        this.prodLmtCnt = prodLmtCnt;
    }

    public int getProdPrc() {
        return prodPrc;
    }

    public void setProdPrc(int prodPrc) {
        this.prodPrc = prodPrc;
    }

    public String getGsrProdCode() {
        return gsrProdCode;
    }

    public void setGsrProdCode(String gsrProdCode) {
        this.gsrProdCode = gsrProdCode;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public int getProdKind() {
        return prodKind;
    }

    public void setProdKind(int prodKind) {
        this.prodKind = prodKind;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getProdCtgSeq() {
        return prodCtgSeq;
    }

    public void setProdCtgSeq(int prodCtgSeq) {
        this.prodCtgSeq = prodCtgSeq;
    }

    public String getDevCtgNm() {
        return devCtgNm;
    }

    public void setDevCtgNm(String devCtgNm) {
        this.devCtgNm = devCtgNm;
    }

    public int getLmtCnt() {
        return lmtCnt;
    }

    public void setLmtCnt(int lmtCnt) {
        this.lmtCnt = lmtCnt;
    }

    public String getBeforeGsrProdCode() {
        return beforeGsrProdCode;
    }

    public void setBeforeGsrProdCode(String beforeGsrProdCode) {
        this.beforeGsrProdCode = beforeGsrProdCode;
    }

    public String getStorCode() {
        return storCode;
    }

    public void setStorCode(String storCode) {
        this.storCode = storCode;
    }

    public int[] getDelProdSeqList() {
        return delProdSeqList;
    }

    public void setDelProdSeqList(String[] delProdSeqList) {
        this.delProdSeqList = new int[delProdSeqList.length];
        for (int i = 0; i < delProdSeqList.length; i++) {
            this.delProdSeqList[i] = Integer.parseInt(delProdSeqList[i]);
        }
    }

    public String getSecondCtgNm() {
        return secondCtgNm;
    }

    public void setSecondCtgNm(String secondCtgNm) {
        this.secondCtgNm = secondCtgNm;
    }

    public List<ProductVO> getProdCategoryList() {
        return prodCategoryList;
    }

    public void setProdCategoryList(List<ProductVO> prodCategoryList) {
        this.prodCategoryList = prodCategoryList;
    }

    public String getCretDt() {
        return cretDt;
    }

    public String getMbrSe() {
        return mbrSe;
    }

    public void setMbrSe(String mbrSe) {
        this.mbrSe = mbrSe;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getDeduction() {
        return deduction;
    }

    public void setDeduction(String deduction) {
        this.deduction = deduction;
    }

}
