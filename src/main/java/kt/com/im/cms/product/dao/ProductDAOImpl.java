/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.product.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.product.vo.ProductVO;

/**
*
* 상품(이용권) 관련 데이터 접근  클래스를 정의한다
*
* @author A2TEC
* @since 2018
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
*
*
* </pre>
*/

@Repository("ProductDAO")
@Transactional
public class ProductDAOImpl extends mysqlAbstractMapper  implements ProductDAO {

    /**
     * 검색조건에 해당되는 상품(이용권) 리스트 정보
     * 
     * @param ProductVO
     * @return 조회 목록 결과
    */
    @Override
    public List<ProductVO> productList(ProductVO vo) throws Exception {
        return selectList("ProductDAO.productList", vo);
    }

    /**
     * 검색조건에 해당되는 상품(이용권) 리스트 합계 정보
     * 
     * @param ProductVO
     * @return 조회 목록 합계
    */
    @Override
    public int productListTotalCount(ProductVO vo) throws Exception {
        return selectOne("ProductDAO.productListTotalCount", vo);
    }

    /**
     * 검색조건에 해당되는 상품(이용권) 정보
     * 
     * @param ProductVO
     * @return 조회 상품 정보
    */
    @Override
    public ProductVO productDetail(ProductVO vo) throws Exception {
        return selectOne("ProductDAO.productDetail", vo);
    }

    /**
     * 검색조건에 해당되는 상품(이용권) 카테고리 장비 목록
     * 
     * @param ProductVO
     * @return 조회 상품 카테고리 목록
    */
    @Override
    public List<ProductVO> productCategoryList(ProductVO vo) throws Exception {
        return selectList("ProductDAO.productCategoryList", vo);
    }

    /**
     * 조건에 해당되는 상품(이용권)이 이미 등록되어 있는지 여부 확인
     * 
     * @param ProductVO
     * @return 조회 상품 카테고리 목록
    */
    @Override
    public boolean productExistYn(ProductVO vo) throws Exception {
        boolean result = false;
        int cnt = selectOne("ProductDAO.productExistYn", vo);

        if (cnt > 0) {
            result = true;
        }

        return result;
    }

    /**
     * 상품(이용권) 등록
     * 
     * @param ProductVO
     * @return
    */
    @Override
    public void productInsert(ProductVO vo) throws Exception {
        insert("ProductDAO.productInsert", vo);
    }

    /**
     * 상품(이용권) 등록 시 사용카테고리 등록
     * 
     * @param ProductVO
     * @return
    */
    @Override
    public void productCategoryInsert(ProductVO vo) throws Exception {
        insert("ProductDAO.productCategoryInsert", vo);
    }

    /**
     * 상품(이용권) 정보 수정
     * 
     * @param ProductVO
     * @return int(result)
    */
    @Override
    public int productUpdate(ProductVO vo) throws Exception {
        return update("ProductDAO.productUpdate", vo);
    }

    /**
     * 상품(이용권) 사용 카테고리 정보 삭제
     * 
     * @param ProductVO
     * @return
    */
    @Override
    public void productCategoryDelete(ProductVO vo) throws Exception {
        delete("ProductDAO.productCategoryDelete", vo);
    }

    /**
     * 상품(이용권) 사용 카테고리 정보 수정
     * 
     * @param ProductVO
     * @return
    */
    @Override
    public void productCategoryUpdate(ProductVO vo) throws Exception {
        update("ProductDAO.productCategoryUpdate", vo);
    }

    /**
     * 상품(이용권)에 대한 결재 정보 수정 (상품 정보 수정 시 결재 정보의 상품 코드 변경)
     * 
     * @param ProductVO
     * @return
    */
    @Override
    public void payInfoUpdate(ProductVO vo) throws Exception {
        update("ProductDAO.payInfoUpdate", vo);
    }

    /**
     * 상품(이용권) 삭제
     * 
     * @param ProductVO
     * @return int(result)
    */
    @Override
    public int productDelete(ProductVO vo) throws Exception {
        return update("ProductDAO.productDelete", vo);
    }

    /**
     * 상품(이용권) 삭제에 따른 카테고리 정보 삭제
     * 
     * @param ProductVO
     * @return
    */
    @Override
    public void productDeleteCategory(ProductVO vo) throws Exception {
        update("ProductDAO.productDeleteCategory", vo);
    }

    /**
     * 상품(이용권) 매장별 카테고리 리스트
     * 
     * @param ProductVO
     * @return 카테고리 목록
    */
    @Override
    public List<ProductVO> productContsCategoryList(ProductVO vo) {
        return selectList("ProductDAO.productContsCategoryList", vo);
    }

}
