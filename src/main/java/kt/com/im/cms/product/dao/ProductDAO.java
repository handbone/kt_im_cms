/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.product.dao;

import java.util.List;

import kt.com.im.cms.product.vo.ProductVO;

/**
*
* 상품(이용권) 관련 데이터 접근  인터페이스 클래스를 정의한다
*
* @author A2TEC
* @since 2018
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
*
*
* </pre>
*/

public interface ProductDAO {

    public List<ProductVO> productList(ProductVO vo) throws Exception;

    public int productListTotalCount(ProductVO vo) throws Exception;

    public ProductVO productDetail(ProductVO vo) throws Exception;

    public List<ProductVO> productCategoryList(ProductVO vo) throws Exception;

    public boolean productExistYn(ProductVO vo) throws Exception;

    public void productInsert(ProductVO vo) throws Exception;

    public void productCategoryInsert(ProductVO vo) throws Exception;

    public int productUpdate(ProductVO vo) throws Exception;

    public void productCategoryDelete(ProductVO vo) throws Exception;

    public void productCategoryUpdate(ProductVO vo) throws Exception;

    public void payInfoUpdate(ProductVO vo) throws Exception;

    public int productDelete(ProductVO vo) throws Exception;

    public void productDeleteCategory(ProductVO vo) throws Exception;

    public List<ProductVO> productContsCategoryList(ProductVO vo);

}
