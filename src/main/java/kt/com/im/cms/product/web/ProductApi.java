/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */


package kt.com.im.cms.product.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.product.service.ProductService;
import kt.com.im.cms.product.vo.ProductVO;

/**
 *
 * 상품(이용권)에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 15.   A2TEC      최초생성
 *
 * </pre>
 */

@Controller
public class ProductApi {
    @Resource(name = "ProductService")
    private ProductService productService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    /**
     * 상품(이용권) 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/product")
    public ModelAndView productList(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/product/productProcess");

        // 로그인 되지 않았거나 권한이 없을경우 접근불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ProductVO item = new ProductVO();

        String loginId = userVO.getMbrId();
        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "prodNm,svcSeq,storSeq,prodLmtCnt,prodTimeLmt,prodPrc,gsrProdCode");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            item.setCretrId(loginId);

            String prodNm = CommonFnc.emptyCheckString("prodNm", request);
            item.setProdNm(prodNm);

            String prodDesc = CommonFnc.emptyCheckString("prodDesc", request);
            item.setProdDesc(prodDesc);

            int prodLmtCnt = CommonFnc.emptyCheckInt("prodLmtCnt", request);
            item.setProdLmtCnt(prodLmtCnt);

            int prodTimeLmt = CommonFnc.emptyCheckInt("prodTimeLmt", request);
            item.setProdTimeLmt(prodTimeLmt);

            int prodPrc = CommonFnc.emptyCheckInt("prodPrc", request);
            item.setProdPrc(prodPrc);

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            item.setSvcSeq(svcSeq);

            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            item.setStorSeq(storSeq);

            String gsrProdCode = CommonFnc.emptyCheckString("gsrProdCode", request);
            item.setGsrProdCode(gsrProdCode);

            boolean isExist = productService.productExistYn(item);
            if (isExist) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("Exist ProductCode(GSR)");
                return rsModel.getModelAndView();
            }

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                productService.productInsert(item);

                String selCategorys = CommonFnc.emptyCheckString("selCategorys", request);
                String selCategoryCounts = CommonFnc.emptyCheckString("selCategoryCounts", request);
                String selDeductions = CommonFnc.emptyCheckString("selDeduction", request);

                if (selCategorys != "" && selCategoryCounts != "" && selDeductions != "") {
                    String[] selCategory = selCategorys.split(",");
                    String[] selCategoryCount = selCategoryCounts.split(",");
                    String[] selDeduction = selDeductions.split(",");

                    for (int i = 0; i < selCategory.length; i++) {
                        item.setDevCtgNm(selCategory[i]);
                        item.setLmtCnt(Integer.parseInt(selCategoryCount[i]));
                        item.setDeduction(selDeduction[i]);
                        productService.productCategoryInsert(item);
                    }
                }
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("prodSeq", item.getProdSeq());
                rsModel.setData("resultType", "productInsert");
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setData("resultType", "productInsert");
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request,"prodSeq,prodNm,storSeq,prodLmtCnt,prodTimeLmt,prodPrc");
            if(!checkString.equals("")){
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            item.setAmdrId(loginId);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int prodSeq = CommonFnc.emptyCheckInt("prodSeq", request);
            String gsrProdCode = CommonFnc.emptyCheckString("gsrProdCode", request);
            String useYn = request.getParameter("useYn") == null? "Y" : request.getParameter("useYn");
            item.setStorSeq(storSeq);
            item.setGsrProdCode(gsrProdCode);
            item.setUseYn(useYn);
            item.setStorCode("");

            if (useYn.equals("Y")) {
                item.setDelYn("N");
            } else {
                item.setDelYn("Y");
            }

            if (gsrProdCode != "") {
                boolean isExist = productService.productExistYn(item);
                if (isExist) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage("Exist ProductCode(GSR)");
                    return rsModel.getModelAndView();
                }
            }

            item.setStorSeq(storSeq);
            item.setGsrProdCode(gsrProdCode);
            item.setProdSeq(prodSeq);
            item.setProdDesc(request.getParameter("prodDesc"));
            item.setProdNm(request.getParameter("prodNm"));

            int prodLmtCnt = CommonFnc.emptyCheckInt("prodLmtCnt", request);
            item.setProdLmtCnt(prodLmtCnt);

            item.setProdTimeLmt(Integer.parseInt(request.getParameter("prodTimeLmt")));
            item.setProdPrc(Integer.parseInt(request.getParameter("prodPrc")));

            // 수정 정보에 따른 업데이트 전 이전 정보 보관
            ProductVO prevItem = new ProductVO();
            // prevItem.setProdSeq(prodSeq);
            prevItem = productService.productDetail(item);
            String beforeGsrProdCode = prevItem.getGsrProdCode();

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = productService.productUpdate(item);
                item.setBeforeGsrProdCode(beforeGsrProdCode);
                if (result == 1) {
                    String selCategorys = CommonFnc.emptyCheckString("selCategorys", request);
                    String selCategoryCounts = CommonFnc.emptyCheckString("selCategoryCounts", request);
                    String selCategorySeqs = CommonFnc.emptyCheckString("selCategorySeq", request);
                    String selDeductions = CommonFnc.emptyCheckString("selDeduction", request);
                    String[] selCategorySeq = {};
                    if (selCategorySeqs != "")
                        selCategorySeq = selCategorySeqs.split(",");

                    List<ProductVO> resultCategoryItem = productService.productCategoryList(item);

                    for (int j = 0; j < resultCategoryItem.size(); j++) {
                        int categorySeq = resultCategoryItem.get(j).getProdCtgSeq();
                        boolean delCategory = true;
                        if (selCategorySeqs == "") {
                            item.setProdCtgSeq(0);
                            item.setProdSeq(Integer.parseInt(request.getParameter("prodSeq")));
                            productService.productCategoryDelete(item);
                            break;
                        } else {
                            for (int i = 0; i < selCategorySeq.length; i++) {
                                if (Integer.parseInt(selCategorySeq[i]) == categorySeq) {
                                    delCategory = false;
                                    break;
                                }
                            }
                        }
                        if (delCategory) {
                            item.setProdCtgSeq(categorySeq);
                            productService.productCategoryDelete(item);
                        }
                    }

                    if (selCategorys != "" && selCategoryCounts != "" && selCategorySeqs != "" && selDeductions != "") {
                        String[] selCategory = selCategorys.split(",");
                        String[] selCategoryCount = selCategoryCounts.split(",");
                        String[] selDeduction = selDeductions.split(",");

                        for (int i = 0; i < selCategory.length; i++) {
                            int categorySeq = Integer.parseInt(selCategorySeq[i]);

                            item.setProdSeq(Integer.parseInt(request.getParameter("prodSeq")));
                            item.setProdCtgSeq(categorySeq);
                            item.setDevCtgNm(selCategory[i]);
                            item.setDeduction(selDeduction[i]);
                            item.setLmtCnt(Integer.parseInt(selCategoryCount[i]));

                            if (categorySeq != 0) {
                                productService.productCategoryUpdate(item);
                            } else {
                                productService.productCategoryInsert(item);
                            }
                        }
                    }

                    if (!gsrProdCode.equals("")) {
                        productService.payInfoUpdate(item);
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            String prodSeqList = CommonFnc.emptyCheckString("prodSeqList", request);
            String[] delProdSeqList = prodSeqList.split(",");

            if (delProdSeqList.length == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }

            item.setAmdrId(loginId);
            item.setDelProdSeqList(delProdSeqList);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = productService.productDelete(item);
                if (result > 0) {
                    productService.productDeleteCategory(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            int prodSeq = CommonFnc.emptyCheckInt("prodSeq", request);
            if (prodSeq != 0) {
                item.setProdSeq(prodSeq);
                ProductVO resultItem = productService.productDetail(item);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != resultItem.getSvcSeq())
                        || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE) && userVO.getStorSeq() != resultItem.getStorSeq())) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                List<ProductVO> resultCategoryItem = productService.productCategoryList(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("CItem", resultCategoryItem);
                rsModel.setData("resultType", "productDetail");
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (prodSeq == 0 && CommonFnc.checkReqParameter(request, "prodSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if (searchConfirm.equals("true")) {
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }
                /*
                 * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                 */
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);

                int page = (currentPage - 1) * limit + 1;
                int pageEnd = page + limit - 1;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("num")) {
                    sidx = "PROD_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                item.setStorSeq(storSeq);

                List<ProductVO> resultItem = productService.productList(item);
                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    int totalCount = productService.productListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setData("resultType", "productList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 상품(이용권) 사용 카테고리 정보 API (2차 카테고리 리스트 조회)
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/productContsCategory")
    public ModelAndView productContsCategoryList(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/product/productProcess");

        // 로그인 되지 않았거나 권한이 없을경우 접근불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ProductVO item = new ProductVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            item.setStorSeq(storSeq);
            List<ProductVO> resultItem = productService.productContsCategoryList(item);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("resultType", "productContsCategoryList");
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
    }

}
