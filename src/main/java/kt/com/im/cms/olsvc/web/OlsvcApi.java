/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.olsvc.web;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.olsvc.service.OlsvcService;
import kt.com.im.cms.olsvc.vo.OlsvcVO;

/**
 *
 *온라인 서비스 관련 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 7. 02.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class OlsvcApi {

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "OlsvcService")
    private OlsvcService olsvcService;

    @Autowired
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * 온라인 서비스 사용 콘텐츠 정보 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     *
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/olsvcDevicePlayContsDetailInfo")
    public ModelAndView olsvcDevicePlayContsDetailInfoProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        System.out.println(">>>>> olsvcDevicePlayContsDetailInfo");
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/olsvc/olsvcProcess");

        request.setCharacterEncoding("UTF-8");
        OlsvcVO vo = new OlsvcVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String checkString = CommonFnc.requiredChecking(request,
                    "mbrTokn,storSeq,devSerial,svcSeq,contsSeq,contsTitle,playType,contsUrl,contsGenre,contsPlayStDt,contsPlayFnsDt");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            if (svcSeq == 0) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
                return rsModel.getModelAndView();
            }

            String uid = CommonFnc.emptyCheckString("uid", request);
            String devSerial = CommonFnc.emptyCheckString("devSerial", request);
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String contsTitle = CommonFnc.emptyCheckString("contsTitle", request);
            String playType = CommonFnc.emptyCheckString("playType", request);
            String contsUrl = CommonFnc.emptyCheckString("contsUrl", request);
            String contsGenre = CommonFnc.emptyCheckString("contsGenre", request);
            String contsPlayStDt = CommonFnc.emptyCheckString("contsPlayStDt", request);
            String contsPlayFnsDt = CommonFnc.emptyCheckString("contsPlayFnsDt", request);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date stDate = sdf.parse(contsPlayStDt);
            Date fnsDate = sdf.parse(contsPlayFnsDt);

            long playTimeSec = (fnsDate.getTime() - stDate.getTime()) / 1000;


            vo.setUid(uid);
            vo.setDevSerial(devSerial);
            vo.setSvcSeq(svcSeq);
            vo.setContsSeq(contsSeq);
            vo.setContsTitle(contsTitle);
            vo.setPlayType(playType);
            vo.setContsUrl(contsUrl);
            vo.setContsGenre(contsGenre);
            vo.setContsPlayStDt(contsPlayStDt);
            vo.setContsPlayFnsDt(contsPlayFnsDt);
            vo.setContsPlayTime((int)playTimeSec);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = olsvcService.insertPlayContsDetailInfo(vo);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);

            /*
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date stDate = sdf.parse("2019-01-16 17:02:14");
            Date fnsDate = sdf.parse("2019-01-17 17:03:17");

            long diffSec = (fnsDate.getTime() - stDate.getTime()) / 1000;
            System.out.println(">> diffSec : " + diffSec);
            long sec = diffSec % 60;
            long min = diffSec / 60;
            System.out.println(">> [1] min : " + min);
            long hour = 0;
            if (min >= 60) {
                hour = min / 60;
                min = min % 60;
                System.out.println(">> [2] min : " + min + ", hour : " + hour);
            }
            
            String playTime = String.format("%02d", hour) + ":" + String.format("%02d", min) + ":" + String.format("%02d", sec);

            System.out.println(">> playTime - " + playTime);
            */

        }

        return rsModel.getModelAndView();
    }

}
