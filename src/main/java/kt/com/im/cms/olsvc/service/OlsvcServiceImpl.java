/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.olsvc.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.olsvc.dao.OlsvcDAO;
import kt.com.im.cms.olsvc.vo.OlsvcVO;

/**
 *
 * 온라인 서비스 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 01. 17.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("OlsvcService")
public class OlsvcServiceImpl implements OlsvcService {

    @Resource(name = "OlsvcDAO")
    private OlsvcDAO olsvcDAO;

    /**
     * 온라인 서비스 콘텐츠 사용 정보 등록
     * @param OlsvcVO
     * @return 등록 결과
     */
    @Override
    public int insertPlayContsDetailInfo(OlsvcVO vo) {
        return olsvcDAO.insertPlayContsDetailInfo(vo);
    }

    /**
     * 온라인 서비스 콘텐츠 상세 정보 조회 이력 추가 (click 횟수)
     * @param OlsvcVO
     * @return 등록 결과
     */
    @Override
    public int insertContsDetailClickHst(OlsvcVO vo) {
        return olsvcDAO.insertContsDetailClickHst(vo);
    }

}
