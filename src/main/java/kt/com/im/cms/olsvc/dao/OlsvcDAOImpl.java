/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.olsvc.dao;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.StateAbstractMapper;
import kt.com.im.cms.olsvc.vo.OlsvcVO;

/**
 *
 * 온라인 서비스 관련 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 01. 17.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("OlsvcDAO")
public class OlsvcDAOImpl extends StateAbstractMapper implements OlsvcDAO {

    /**
     * 온라인 서비스 콘텐츠 사용 정보 등록
     * @param OlsvcVO
     * @return 등록 결과
     */
    @Override
    public int insertPlayContsDetailInfo(OlsvcVO vo) {
        return insert("OlsvcDAO.insertPlayContsDetailInfo", vo);
    }

    /**
     * 온라인 서비스 콘텐츠 상세 정보 조회 이력 추가 (click 횟수)
     * @param OlsvcVO
     * @return 등록 결과
     */
    @Override
    public int insertContsDetailClickHst(OlsvcVO vo) {
        return insert("OlsvcDAO.insertContsDetailClickHst", vo);
    }

}
