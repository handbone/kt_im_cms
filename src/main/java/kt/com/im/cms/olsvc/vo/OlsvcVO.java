/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.olsvc.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * OlsvcVO
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class OlsvcVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7716725797970426261L;

    /** 온라인 서비스 이력 번호 */
    private int olsvcHstSeq;

    /** 인증 회원 아이디 */
    private String uid;

    /** 장치 시리얼번호 */
    private String devSerial;

    /** 서비스 번호 */
    private int svcSeq;

    /** 콘텐츠 번호 */
    private int contsSeq;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 재생 유형 ('S': 스트리밍, 'D': 다운로드) */
    private String playType;

    /** 콘텐츠 URL (스트리밍 URL) */
    private String contsUrl;

    /** 콘텐츠 장르 */
    private String contsGenre;

    /** 콘텐츠 재생 시작 일시 */
    private String contsPlayStDt;

    /** 콘텐츠 재생 종료 일시 */
    private String contsPlayFnsDt;

    /** 콘텐츠 재생 시간 */
    private int contsPlayTime;

    /** 등록 일시 */
    private String regDt;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** 콘텐츠 상세 조회 클릭 이력 번호 */
    private int contsClickHstSeq;

    public int getOlsvcHstSeq() {
        return olsvcHstSeq;
    }

    public void setOlsvcHstSeq(int olsvcHstSeq) {
        this.olsvcHstSeq = olsvcHstSeq;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDevSerial() {
        return devSerial;
    }

    public void setDevSerial(String devSerial) {
        this.devSerial = devSerial;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public String getPlayType() {
        return playType;
    }

    public void setPlayType(String playType) {
        this.playType = playType;
    }

    public String getContsUrl() {
        return contsUrl;
    }

    public void setContsUrl(String contsUrl) {
        this.contsUrl = contsUrl;
    }

    public String getContsGenre() {
        return contsGenre;
    }

    public void setContsGenre(String contsGenre) {
        this.contsGenre = contsGenre;
    }

    public String getContsPlayStDt() {
        return contsPlayStDt;
    }

    public void setContsPlayStDt(String contsPlayStDt) {
        this.contsPlayStDt = contsPlayStDt;
    }

    public String getContsPlayFnsDt() {
        return contsPlayFnsDt;
    }

    public void setContsPlayFnsDt(String contsPlayFnsDt) {
        this.contsPlayFnsDt = contsPlayFnsDt;
    }

    public int getContsPlayTime() {
        return contsPlayTime;
    }

    public void setContsPlayTime(int contsPlayTime) {
        this.contsPlayTime = contsPlayTime;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getContsClickHstSeq() {
        return contsClickHstSeq;
    }

    public void setContsClickHstSeq(int contsClickHstSeq) {
        this.contsClickHstSeq = contsClickHstSeq;
    }

}
