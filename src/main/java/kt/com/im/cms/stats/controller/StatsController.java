/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.stats.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * 통계에 관한 컨트롤러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.07.17
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 7. 17.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Component
@Controller
public class StatsController {

    /**
     * 유입 통계 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/traffic", method = RequestMethod.GET)
    public ModelAndView traffic(ModelAndView mv) {
        mv.setViewName("/views/statistics/traffic/Traffic");
        return mv;
    }

    /**
     * 콘텐츠 현황 통계 > 콘텐츠 등록 현황
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/contents/registered", method = RequestMethod.GET)
    public ModelAndView registeredContents(ModelAndView mv) {
        mv.setViewName("/views/statistics/contents/RegisteredContents");
        return mv;
    }

    /**
     * 콘텐츠 현황 통계 > 콘텐츠 사용 현황
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/contents/used", method = RequestMethod.GET)
    public ModelAndView usedContents(ModelAndView mv) {
        mv.setViewName("/views/statistics/contents/UsedContents");
        return mv;
    }

    /**
     * 콘텐츠 현황 통계 > 콘텐츠 사용 현황 > 콘텐츠 상세 사용 현황
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/contents/used/{contsSeq}", method = RequestMethod.GET)
    public ModelAndView usedContentsDetail(ModelAndView mv, HttpServletRequest request,
            @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.addObject("dateType", request.getParameter("dateType"));
        mv.addObject("startDate", request.getParameter("startDate"));
        mv.addObject("endDate", request.getParameter("endDate"));
        mv.addObject("section", request.getParameter("section"));
        mv.addObject("svcSeq", request.getParameter("svcSeq"));
        mv.addObject("storSeq", request.getParameter("storSeq"));
        mv.setViewName("/views/statistics/contents/UsedContentsDetail");
        return mv;
    }

    /**
     * CP사 통계 > CP사 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/cp", method = RequestMethod.GET)
    public ModelAndView contentsProvider(ModelAndView mv) {
        mv.setViewName("/views/statistics/cp/ContentProvider");
        return mv;
    }

    /**
     * 매장 현황 통계 > 이용객 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/store/visitor", method = RequestMethod.GET)
    public ModelAndView visitor(ModelAndView mv) {
        mv.setViewName("/views/statistics/store/VisitorList");
        return mv;
    }

    /**
     * 매장 현황 통계 > 이용객 통계 > 이용객
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/store/visitor/{storSeq}", method = RequestMethod.GET)
    public ModelAndView visitorDetail(ModelAndView mv, HttpServletRequest request,
            @PathVariable(value = "storSeq") String storSeq) {
        mv.addObject("storSeq", storSeq);
        mv.addObject("dateType", request.getParameter("dateType"));
        mv.addObject("startDate", request.getParameter("startDate"));
        mv.addObject("endDate", request.getParameter("endDate"));
        mv.setViewName("/views/statistics/store/VisitorDetail");
        return mv;
    }

    /**
     * 매장 현황 통계 > 이용권 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/store/tag", method = RequestMethod.GET)
    public ModelAndView usedTag(ModelAndView mv) {
        mv.setViewName("/views/statistics/store/UsedTagList");
        return mv;
    }

    /**
     * 매장 현황 통계 > 이용권 통계 > 이용권 발급 목록
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/store/tag/{seq}", method = RequestMethod.GET)
    public ModelAndView usedTagDetail(ModelAndView mv, HttpServletRequest request,
            @PathVariable(value = "seq") String seq) {
        mv.addObject("prodSeq", seq);
        mv.addObject("dateType", request.getParameter("dateType"));
        mv.addObject("startDate", request.getParameter("startDate"));
        mv.addObject("endDate", request.getParameter("endDate"));
        mv.setViewName("/views/statistics/store/UsedTagDetail");
        return mv;
    }

    /**
     * 매장 현황 통계 > 이용권 통계 > 이용권 발급 목록 <매장선택>
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/store/tagStore/{storSeq}", method = RequestMethod.GET)
    public ModelAndView usedStoreTagDetail(ModelAndView mv, HttpServletRequest request,
            @PathVariable(value = "storSeq") String storSeq) {
        mv.addObject("storSeq", storSeq);
        mv.addObject("dateType", request.getParameter("dateType"));
        mv.addObject("startDate", request.getParameter("startDate"));
        mv.addObject("endDate", request.getParameter("endDate"));
        mv.setViewName("/views/statistics/store/UsedStoreTagDetail");
        return mv;
    }

    /**
     * 매장 현황 통계 > 이용권 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/store/contentsByTag", method = RequestMethod.GET)
    public ModelAndView contentsByTag(ModelAndView mv) {
        mv.setViewName("/views/statistics/store/ContentsByTag");
        return mv;
    }

    /**
     * 온라인 현황 통계 > 콘텐츠 사용 현황
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/online/usedOlsvcContents", method = RequestMethod.GET)
    public ModelAndView usedOlsvcContents(ModelAndView mv) {
        mv.setViewName("/views/statistics/online/UsedOlsvcContents");
        return mv;
    }

    /**
     * 매장 현황 통계 > 검수 현황 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/verifier", method = RequestMethod.GET)
    public ModelAndView verifier(ModelAndView mv) {
        mv.setViewName("/views/statistics/verifier/Verifier");
        return mv;
    }

    /**
     * 매출 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/payInfo", method = RequestMethod.GET)
    public ModelAndView payInfo(ModelAndView mv) {
        mv.setViewName("/views/statistics/sales/PayInfo");
        return mv;
    }

    /**
     * 블록체인 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/blockStats", method = RequestMethod.GET)
    public ModelAndView blockStats(ModelAndView mv) {
        mv.setViewName("/views/statistics/contract/blockStats");
        return mv;
    }

    /* 런쳐 통계 S */

    /**
     * 통계 > 런쳐 통계 > 유입 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/olSvcLauncher/traffic", method = RequestMethod.GET)
    public ModelAndView olSvcTraffic(ModelAndView mv) {
        mv.setViewName("/views/statistics/launcher/Traffic");
        return mv;
    }

    /**
     * 통계 > 런쳐 통계 > 이용 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/olSvcLauncher/usedAppDetail", method = RequestMethod.GET)
    public ModelAndView olSvcUsedApp(ModelAndView mv) {
        mv.setViewName("/views/statistics/launcher/UsedApp");
        return mv;
    }

    /**
     * 통계 > 런쳐 통계 > 이용 통계 상세
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/olSvcLauncher/usedAppDetail/{appSeq}", method = RequestMethod.GET)
    public ModelAndView olSvcUsedAppDetail(ModelAndView mv, HttpServletRequest request,
            @PathVariable(value = "appSeq") String appSeq) {
        mv.addObject("appSeq", appSeq);
        mv.setViewName("/views/statistics/launcher/UsedAppDetail");
        return mv;
    }

    /**
     * 온라인 현황 통계 > 콘텐츠 재생 통계
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/olSvcLauncher/usedConts", method = RequestMethod.GET)
    public ModelAndView olSvcUsedConts(ModelAndView mv) {
        System.out.println(">> [controller] /statistics/olSvcLauncher/usedConts");
        mv.setViewName("/views/statistics/launcher/UsedConts");
        return mv;
    }

    /**
     * 온라인 현황 통계 > 콘텐츠 재생 통계 > 콘텐츠 상세 현황
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/statistics/olSvcLauncher/usedConts/{contsSeq}", method = RequestMethod.GET)
    public ModelAndView olSvcUsedContsDetail(ModelAndView mv, HttpServletRequest request,
            @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/statistics/launcher/UsedContsDetail");
        return mv;
    }

    /* 런쳐 통계 E */
}
