/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.stats.dao;

import java.util.List;
import java.util.Map;

import kt.com.im.cms.stats.vo.StatsVO;

/**
 *
 * 결제 관련 및 통계 관련 데이터 접근 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 28.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public interface StatsDAO {

    int searchSettlList(StatsVO vo) throws Exception;

    int insertSettlInfo(StatsVO vo) throws Exception;

    void insertSettlDetail(StatsVO vo) throws Exception;

    List<StatsVO> searchResetTicket(StatsVO vo) throws Exception;

    List<StatsVO> getTodayVisitor(StatsVO vo) throws Exception;

    List<StatsVO> getVisitor(StatsVO vo) throws Exception;

    List<StatsVO> getSoldTicket(StatsVO vo) throws Exception;

    List<Map> getTotalContentsStatus(StatsVO vo) throws Exception;

    List<Map> getContentsByCategory(StatsVO vo) throws Exception;

    List<Map> getMostDownloadContents(StatsVO vo) throws Exception;

    List<Map> getMostUseTimeContents(StatsVO vo) throws Exception;

    List<Map> getMostPopularContents(StatsVO vo) throws Exception;

    List<Map> getDisplayWebTrafficOverviewList(StatsVO vo) throws Exception;

    int getDisplayWebTrafficOverviewListTotalCount(StatsVO vo) throws Exception;

    List<Map> getRegisteredContentsList(StatsVO vo) throws Exception;

    int getRegisteredContentsListTotalCount(StatsVO vo) throws Exception;

    List<Map> getUsedContentsList(StatsVO vo) throws Exception;

    int getUsedContentsListTotalCount(StatsVO vo) throws Exception;

    List<Map> getContentsByCpList(StatsVO vo) throws Exception;

    int getContentsByCpListTotalCount(StatsVO vo) throws Exception;

    List<Map> getVerifiedContentsList(StatsVO vo) throws Exception;

    int getVerifiedContentsListTotalCount(StatsVO vo) throws Exception;

    List<Map> getVisitorList(StatsVO vo) throws Exception;

    int getVisitorListTotalCount(StatsVO vo) throws Exception;

    List<Map> getTagList(StatsVO vo) throws Exception;

    int getTagListTotalCount(StatsVO vo) throws Exception;

    List<Map> getTagDetailList(StatsVO vo) throws Exception;

    int getTagDetailListTotalCount(StatsVO vo) throws Exception;

    List<Map> getTagHistoryList(StatsVO vo) throws Exception;

    int getTagHistoryListTotalCount(StatsVO vo) throws Exception;

    List<Map> getContentsByTagList(StatsVO vo) throws Exception;

    int getContentsByTagListTotalCount(StatsVO vo) throws Exception;

    List<Map> getContentsDetailByTagList(StatsVO vo) throws Exception;

    int getContentsDetailByTagListTotalCount(StatsVO vo) throws Exception;

    StatsVO getStoreFromTag(StatsVO vo) throws Exception;

    List<StatsVO> payDetailLst(StatsVO vo) throws Exception;

    List<StatsVO> payTagList(StatsVO vo) throws Exception;

    StatsVO searchSettlInfo(StatsVO vo) throws Exception;

    int subtractApdTime(StatsVO vo) throws Exception;

    List<Map> selectSettlementManage(StatsVO vo) throws Exception;

    int selectSettlementManageTotalCount(StatsVO vo) throws Exception;

    int getUsedContentsDetailListTotalCount(StatsVO vo) throws Exception;

    List<Map> getUsedContentsDetailList(StatsVO vo) throws Exception;

    List<StatsVO> getUsedOlsvcContentsStatsList(StatsVO vo) throws Exception;

    int getUsedOlsvcContentsStatsListTotalCount(StatsVO vo) throws Exception;

    List<Map<Object, Object>> getUsedOlsvcContentsList(StatsVO vo) throws Exception;

    int getUsedOlsvcContentsListTotalCount(StatsVO vo) throws Exception;

    int getVisitorDetailListTotalCount(StatsVO vo) throws Exception;

    List<Map> getVisitorDetailList(StatsVO vo) throws Exception;

    int getTagStoreListTotalCount(StatsVO vo) throws Exception;

    List<Map> getTagStoreList(StatsVO vo) throws Exception;

    List<Map> getTagStoreProdList(StatsVO vo) throws Exception;

    int getLauncherTrafficListTotalCount(StatsVO vo) throws Exception;

    List<Map> getLauncherTrafficList(StatsVO vo) throws Exception;

    int getLauncherUsedAppListTotalCount(StatsVO vo) throws Exception;

    List<Map> getLauncherUsedAppList(StatsVO vo) throws Exception;

    List<Map> getLauncherUsedAppDetailList(StatsVO vo) throws Exception;

    int getLauncherUsedAppDetailListTotalCount(StatsVO vo) throws Exception;

    List<StatsVO> getOlSvcLauncherUsedContsStatsList(StatsVO vo) throws Exception;

    int getOlSvcLauncherUsedContsStatsListTotalCount(StatsVO vo) throws Exception;

    List<Map> getOlSvcLauncherUsedContsDetailStatsList(StatsVO vo) throws Exception;

    int getOlSvcLauncherUsedContsDetailStatsListTotalCount(StatsVO vo) throws Exception;

}
