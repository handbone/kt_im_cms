/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.stats.web;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.FormatCheckUtil;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.stats.service.StatsService;
import kt.com.im.cms.stats.vo.StatsVO;
import kt.com.im.cms.store.service.StoreService;
import kt.com.im.cms.store.vo.StoreVO;

@Controller
public class StatsApi {

    private String viewName = "../resources/api/stats/statsProcess";

    @Resource(name = "StatsService")
    private StatsService statsService;

    @Resource(name = "StoreService")
    private StoreService storeService;

    @Autowired
    MessageSource messageSource;

    /**
     * 통계 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/statistics")
    public ModelAndView genreProcess(Locale locale, HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");

        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String validateParams = "type";
            String invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                        "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            String type = request.getParameter("type");
            if ("today".equals(type)) {
                return this.getTodayVisitor(request, rsModel, userVO);
            }

            if ("visitor".equals(type)) {
                return this.getVisitor(request, rsModel, userVO);
            }

            if ("ticket".equals(type)) {
                return this.getSoldTicket(request, rsModel, userVO);
            }

            if ("totalContents".equals(type)) {
                return this.getTotalContents(request, rsModel, userVO);
            }

            if ("contentsByCategory".equals(type)) {
                return this.getContentsByCategory(request, rsModel, userVO);
            }

            if ("mostDownloadContents".equals(type)) {
                return this.getMostDownloadContents(request, rsModel, userVO);
            }

            if ("mostUseTimeContents".equals(type)) {
                return this.getMostUseTimeContents(request, rsModel, userVO);
            }

            if ("mostPopularContents".equals(type)) {
                return this.getMostPopularContents(request, rsModel, userVO);
            }

            if ("displayWebTrafficOverview".equals(type)) {
                return this.getDisplayWebTrafficOverview(request, rsModel, userVO);
            }

            if ("registeredContentsList".equals(type)) {
                return this.getRegisteredContentsList(request, rsModel, userVO);
            }

            if ("usedContentsList".equals(type)) {
                return this.getUsedContentsList(request, rsModel, userVO);
            }

            if ("usedContentsDetail".equals(type)) {
                return this.getUsedContentsDetail(request, rsModel, userVO);
            }

            if ("contentsByCpList".equals(type)) {
                return this.getContentsByCpList(request, rsModel, userVO);
            }

            if ("verifiedContentsList".equals(type)) {
                return this.getVerifiedContentsList(request, rsModel, userVO);
            }

            if ("visitorList".equals(type)) {
                return this.getVisitorList(request, rsModel, userVO);
            }

            if ("visitorDetail".equals(type)) {
                return this.getVisitorDetail(request, rsModel, userVO);
            }

            if ("tagList".equals(type)) {
                return this.getTagList(request, rsModel, userVO);
            }

            if ("tagStroeList".equals(type)) {
                return this.getTagStroeList(request, rsModel, userVO);
            }

            if ("tagProdList".equals(type)) {
                return this.getTagStroeProdList(request, rsModel, userVO);
            }

            if ("tagDetailList".equals(type)) {
                return this.getTagDetailList(request, rsModel, userVO);
            }

            if ("tagHistoryList".equals(type)) {
                return this.getTagHistoryList(request, rsModel, userVO);
            }

            if ("contentsByTagList".equals(type)) {
                return this.getContentsByTagList(request, rsModel, userVO);
            }

            if ("contentsDetailByTagList".equals(type)) {
                return this.getContentsDetailByTagList(request, rsModel, userVO);
            }

            if ("settlementManageList".equals(type)) {
                return this.getSettlementManageList(request, rsModel, userVO);
            }

            if ("usedOlsvcContentsList".equals(type)) {
                return this.getUsedOlsvcContentsList(request, rsModel, userVO);
            }

            if ("launcherTraffic".equals(type)) {
                return this.getLauncherTrafficList(request, rsModel, userVO);
            }

            if ("launcherUsedApp".equals(type)) {
                return this.getLauncherUsedAppList(request, rsModel, userVO);
            }

            if ("launcherUsedAppDetail".equals(type)) {
                return this.getLauncherUsedAppDetailList(request, rsModel, userVO);
            }

            if ("launcherUsedConts".equals(type)) {
                return this.getLauncerUsedContsList(request, rsModel, userVO);
            }

            if ("launcherUsedContsDetail".equals(type)) {
                return this.getLauncherUsedContsDetailList(locale, request, rsModel, userVO);
            }
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권 통계 매장별 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getTagStroeList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String dateType = CommonFnc.emptyCheckString("dateType", request);
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setDateType(dateType);
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        // 날짜 API

        // 시작 , 끝 날짜 임의 세팅

        String s1 = startDate;
        String s2 = endDate;
        String dateFormat = "yyyy-MM-dd";

        if (dateType.equals("day")) {
            dateFormat = "yyyy-MM-dd HH";
            s1 += " 00";
            s2 += " 23";
        }

        int DateTot = 0;

        DateFormat df = new SimpleDateFormat(dateFormat);
        SimpleDateFormat format1 = new SimpleDateFormat(dateFormat);

        // Date타입으로 변경

        Date d1 = df.parse(s1);
        Date d2 = df.parse(s2);

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        Calendar startT = Calendar.getInstance();
        Calendar endT = Calendar.getInstance();

        // Calendar 타입으로 변경 add()메소드로 1일씩 추가해 주기위해 변경
        c1.setTime(d1);
        c2.setTime(d2);

        List<String> dateList = new ArrayList<String>();

        while (c1.compareTo(c2) != 1) {
            String date = format1.format(c1.getTime());
            dateList.add(date);
            DateTot++;
            if (dateType.equals("day")) {
                c1.add(Calendar.HOUR, 1);
            } else {
                c1.add(Calendar.DATE, 1);
            }

        }

        if (request.getParameter("sord").equals("asc")) {
            Collections.sort(dateList);
        } else {
            Collections.sort(dateList, new AscendingString());
        }

        if (currentPage == 1 || currentPage == 0) {
            DateTot += 1;
        }

        int totalPage = (rows > 0) ? (int) Math.ceil((double) DateTot / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        if (currentPage == 1 || currentPage == 0) {
            rows = 9;
        }

        item.setOffset(-1);
        item.setLimit(-1);

        int DateStart = (currentPage - 1) * rows;
        if (currentPage != 1 && currentPage != 0) {
            DateStart--;
        }
        int DateEnd = DateStart + (rows - 1);

        /* dateList || startDate EndDate */

        if (request.getParameter("sord").equals("desc")) {
            c2.setTime(df.parse(dateList.get(DateStart)));
            if (DateEnd < dateList.size()) {
                c1.setTime(df.parse(dateList.get(DateEnd)));
            } else {
                c1.setTime(df.parse(dateList.get(dateList.size() - 1)));
            }
        } else {
            c1.setTime(df.parse(dateList.get(DateStart)));
            if (DateEnd < dateList.size()) {
                c2.setTime(df.parse(dateList.get(DateEnd)));
            } else {
                c2.setTime(df.parse(dateList.get(dateList.size() - 1)));
            }
        }

        dateList = new ArrayList<String>();
        while (c1.compareTo(c2) != 1) {
            if (dateType.equals("day")) {
                String date = format1.format(c1.getTime());
                c1.add(Calendar.HOUR, 1);
                dateList.add(date);
            } else {
                String date = format1.format(c1.getTime());
                dateList.add(date);
                c1.add(Calendar.DATE, 1);
            }
        }

        // if (c1.compareTo(c2) == 1 && DateCurrent <= DateEnd) {
        // c1.add(Calendar.DATE, -1);
        // String date = format1.format(c1.getTime());
        // item.setEndDate(date);
        // endT.setTime(c1.getTime());
        // }

        if (!request.getParameter("sord").equals("desc")) {
            Collections.sort(dateList);
            item.setStartDate(dateList.get(0));
            item.setEndDate(dateList.get(dateList.size() - 1));

            startT.setTime(df.parse(dateList.get(0)));
            endT.setTime(df.parse(dateList.get(dateList.size() - 1)));

        } else {
            Collections.sort(dateList, new AscendingString());

            item.setStartDate(dateList.get(dateList.size() - 1));
            item.setEndDate(dateList.get(0));

            startT.setTime(df.parse(dateList.get(dateList.size() - 1)));
            endT.setTime(df.parse(dateList.get(0)));
        }

        if (dateType.equals("day")) {
            item.setStartDate(startDate);
            item.setEndDate(endDate);
        }

        List<Map> resultItem = statsService.getTagStoreList(item);

        item.setStartDate(startDate);
        item.setEndDate(endDate);
        List<Map> resultPItem = statsService.getTagStoreProdList(item);
        List<Map> resultTotItem = statsService.getTagList(item);

        if (resultPItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        String[][] prodList_dist = new String[resultPItem.size()][3];

        /* 상품 목록 */
        for (int i = 0; i < resultPItem.size(); i++) {

            prodList_dist[i][0] = Integer.toString((int) resultPItem.get(i).get("prodSeq"));
            prodList_dist[i][1] = (String) resultPItem.get(i).get("prodNm");
            prodList_dist[i][2] = "0";
            for (int j = 0; j < resultTotItem.size(); j++) {
                int lprodSeq = (int) resultTotItem.get(j).get("prodSeq");
                int rprodSeq = Integer.parseInt(prodList_dist[i][0]);

                if (lprodSeq == rprodSeq && resultTotItem.get(j).get("prodNm").equals(prodList_dist[i][1])) {
                    prodList_dist[i][2] = resultTotItem.get(j).get("tagCnt").toString();
                    break;
                }
            }

        }

        /* 상품 목록 중복제거 */
        List<List<String>> prodList_result = Arrays.stream(prodList_dist).map(Arrays::asList).distinct()
                .collect(Collectors.toList());

        List<reservateInfo> reserVateList = new ArrayList<reservateInfo>();
        // 시작날짜와 끝 날짜를 비교해, 시작날짜가 작거나 같은 경우 출력

        c1.setTime(startT.getTime());

        // dateList
        if (dateType.equals("day")) {
            dateList = new ArrayList<String>();
        }

        while (c1.compareTo(endT) != 1) {
            String date = format1.format(c1.getTime());
            for (int i = 0; i < prodList_result.size(); i++) {
                SimpleDateFormat format2 = new SimpleDateFormat("HH");
                if (dateType.equals("day")) {
                    c2.setTime(c1.getTime());
                    c2.add(Calendar.HOUR, 1);

                    date = format2.format(c1.getTime());
                    String date2 = format2.format(c2.getTime());

                    date = date + ":00" + " ~ " + date2 + ":00";
                    if (i == 0) {
                        dateList.add(date);
                    }

                }

                reservateInfo s = new reservateInfo(date, Integer.parseInt(prodList_result.get(i).get(0)),
                        prodList_result.get(i).get(1), 0);
                reserVateList.add(s);
            }

            if (dateType.equals("day")) {
                c1.add(Calendar.HOUR, 1);
            } else {
                c1.add(Calendar.DATE, 1);
            }

        }

        if (dateType.equals("day")) {
            if (!request.getParameter("sord").equals("desc")) {
                Collections.sort(dateList);
            } else {
                Collections.sort(dateList, new AscendingString());
            }
        }

        for (reservateInfo x : reserVateList) {
            for (int i = 0; i < resultItem.size(); i++) {
                if (resultItem.get(i).get("prodSeq").equals(x.getProdSeq())
                        && resultItem.get(i).get("prodNm").equals(x.getProdNm())
                        && resultItem.get(i).get("useDt").equals(x.getDate())) {
                    Long lcount = (Long) resultItem.get(i).get("tagCnt");
                    int count = lcount.intValue();
                    x.setPutCnt(count);
                }
            }

        }

        // getTagStoreList - PRODCUE LIST - 날짜 계산 배열 생성
        // 합계

        rsModel.setData("prodList", prodList_result);
        rsModel.setData("dateList", dateList);
        rsModel.setData("totalCount", DateTot);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("tagStoreList");
        rsModel.setData("item", reserVateList);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권 통계 매장별 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getTagStroeProdList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String dateType = CommonFnc.emptyCheckString("dateType", request);
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setDateType(dateType);
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        List<Map> resultPItem = statsService.getTagStoreProdList(item);

        if (resultPItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        // 합계

        rsModel.setData("prodList", resultPItem);
        rsModel.setData("currentPage", currentPage);
        rsModel.setResultType("tagProdList");
        return rsModel.getModelAndView();
    }

    /**
     * 금일 매장 방문 정보 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getTodayVisitor(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "storSeq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));

        List<StatsVO> resultItem = statsService.getTodayVisitor(item);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("todayInfoList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권 구입 고객층 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getVisitor(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "storSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};" + "dateType:{" + Validator.DATE_TYPE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        String dateType = request.getParameter("dateType");
        item.setDateType(this.getDateQueryFrom(dateType));

        List<StatsVO> resultItem = statsService.getVisitor(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("visitorInfoList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용객 통계 상세 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getVisitorDetail(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String dateType = CommonFnc.emptyCheckString("dateType", request);
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        StatsVO Allitem = new StatsVO();

        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);

        Allitem.setSvcSeq(svcSeq);
        Allitem.setStorSeq(storSeq);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setDateType(dateType);

        Allitem.setSidx(request.getParameter("sidx"));
        Allitem.setSord(request.getParameter("sord"));
        Allitem.setStartDate(startDate);
        Allitem.setEndDate(endDate);

        int totalCount = statsService.getVisitorDetailListTotalCount(item);

        if (currentPage == 1 || currentPage == 0) {
            totalCount += 1;
        }

        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);

        if (currentPage == 1 || currentPage == 0) {
            rows = 9;
        } else {
            offset -= 1;
        }

        item.setOffset(offset);
        item.setLimit(rows);

        Allitem.setOffset(-1);
        Allitem.setLimit(-1);

        List<Map> resultItem = statsService.getVisitorDetailList(item);

        List<Map> resultAllItem = statsService.getVisitorDetailList(Allitem);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        int visitedTotalCnt = 0;
        int leftTotalCnt = 0;
        int enteringTotalCnt = 0;

        for (int i = 0; i < resultAllItem.size(); i++) {
            Map vItem = resultAllItem.get(i);

            visitedTotalCnt += Integer.parseInt(vItem.get("visitedTotalCnt").toString());
            leftTotalCnt += Integer.parseInt(vItem.get("leftCnt").toString());
            enteringTotalCnt += Integer.parseInt(vItem.get("enteringCnt").toString());

        }

        if (currentPage == 1 || currentPage == 0) {
            Map totInfo = new HashMap<>();
            /* 합계 */
            totInfo.put("useDt", "합계");
            totInfo.put("svcNm", "");
            totInfo.put("storSeq", "");
            totInfo.put("storNm", "");
            totInfo.put("visitedTotalCnt", visitedTotalCnt);
            totInfo.put("leftCnt", leftTotalCnt);
            totInfo.put("enteringCnt", enteringTotalCnt);

            resultItem.add(0, totInfo);
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("visitorDetailList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권 별 구입 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getSoldTicket(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "storSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};" + "dateType:{" + Validator.DATE_TYPE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        String dateType = request.getParameter("dateType");
        item.setDateType(this.getDateQueryFrom(dateType));

        List<StatsVO> resultItem = statsService.getSoldTicket(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("ticketInfoList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 전체 콘텐츠 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getTotalContents(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName("jsonView");
        boolean prevented = MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setSvcSeq(Integer.parseInt(request.getParameter("svcSeq")));

        List<Map> resultItem = statsService.getTotalContentsStatus(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultData("totalContentsStatusList", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 카테고리별 콘텐츠 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getContentsByCategory(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName("jsonView");
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setSvcSeq(Integer.parseInt(request.getParameter("svcSeq")));
        List<Map> resultItem = statsService.getContentsByCategory(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultData("contentsByCategoryList", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 인기 콘텐츠 다운로드 현황
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getMostDownloadContents(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName("jsonView");
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setSvcSeq(Integer.parseInt(request.getParameter("svcSeq")));
        item.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));
        item.setStartDate(startDate + " 00:00");
        item.setEndDate(endDate + " 23:59");
        item.setLimit(10);

        List<Map> resultItem = statsService.getMostDownloadContents(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultData("mostDownloadContentsList", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 인기 콘텐츠 사용시간 현황
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getMostUseTimeContents(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName("jsonView");
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setSvcSeq(Integer.parseInt(request.getParameter("svcSeq")));
        item.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));
        item.setStartDate(startDate + " 00:00");
        item.setEndDate(endDate + " 23:59");
        item.setLimit(10);

        List<Map> resultItem = statsService.getMostUseTimeContents(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultData("mostUseTimeContentsList", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 인기 콘텐츠 현황
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getMostPopularContents(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName("jsonView");
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setSvcSeq(Integer.parseInt(request.getParameter("svcSeq")));
        item.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));
        item.setLimit(10);

        List<Map> resultItem = statsService.getMostPopularContents(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultData("mostPopularContentsList", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 전시 페이지 유입 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getDisplayWebTrafficOverview(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        // 웹/모바일 PV, UV 별 전체 총 갯수 조회
        int limit = statsService.getDisplayWebTrafficOverviewListTotalCount(item);
        item.setLimit(limit);
        List<Map> resultTotalItem = statsService.getDisplayWebTrafficOverviewList(item);
        int webUvTotalCnt = 0;
        int webPvTotalCnt = 0;
        int mobUvTotalCnt = 0;
        int mobPvTotalCnt = 0;
        for (int i = 0; i < resultTotalItem.size(); i++) {
            Map result = resultTotalItem.get(i);
            webPvTotalCnt += Integer.parseInt(String.valueOf(result.get("webPvCnt")));
            mobPvTotalCnt += Integer.parseInt(String.valueOf(result.get("mobPvCnt")));
            webUvTotalCnt += Integer.parseInt(String.valueOf(result.get("webUvCnt")));
            mobUvTotalCnt += Integer.parseInt(String.valueOf(result.get("mobUvCnt")));
        }
        rsModel.setData("webPvTotalCnt", webPvTotalCnt);
        rsModel.setData("mobPvTotalCnt", mobPvTotalCnt);
        rsModel.setData("webUvTotalCnt", webUvTotalCnt);
        rsModel.setData("mobUvTotalCnt", mobUvTotalCnt);

        int totalCount = statsService.getDisplayWebTrafficOverviewListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getDisplayWebTrafficOverviewList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("displayWebTrafficOverviewList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 등록 현황 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getRegisteredContentsList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "section";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            item.setSvcSeq(userVO.getSvcSeq());
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setTarget(request.getParameter("section"));

        int totalCount = statsService.getRegisteredContentsListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getRegisteredContentsList(item);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("registeredContentsList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 사용 현황 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getUsedContentsList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "section;svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "cpSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};" + "endDate:{"
                + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        String section = request.getParameter("section");

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        int cpSeq = Integer.parseInt(request.getParameter("cpSeq"));

        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        item.setCpSeq(cpSeq);
        if ("service".equals(section) || "cp".equals(section)) {
            if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                    || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }
            }
        } else {
            if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }
            } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }
            }
        }
        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setKeyword(request.getParameter("keyword"));
        item.setTarget(request.getParameter("target"));

        int totalCount = statsService.getUsedContentsListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getUsedContentsList(item);

        // Stream<Map> mapStream = resultItem.stream().filter(alpha -> "미니풋볼".equals(alpha.get("contsTitle")));
        // resultItem = mapStream.collect(Collectors.toList());

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("usedContentsList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 상세 사용 현황 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getUsedContentsDetail(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "section;svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "contsSeq:{" + Validator.NUMBER + "};" + "cpSeq:{" + Validator.NUMBER + "};" + "startDate:{"
                + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        String section = request.getParameter("section");

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        int cpSeq = Integer.parseInt(request.getParameter("cpSeq"));
        int contsSeq = Integer.parseInt(request.getParameter("contsSeq"));

        StatsVO item = new StatsVO();
        StatsVO Allitem = new StatsVO();
        item.setDateType(dateType);
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        item.setContsSeq(contsSeq);
        item.setCpSeq(cpSeq);

        Allitem.setSvcSeq(svcSeq);
        Allitem.setContsSeq(contsSeq);
        Allitem.setStorSeq(storSeq);
        Allitem.setCpSeq(cpSeq);

        if ("service".equals(section) || "cp".equals(section)) {
            if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                    || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }
            }
        } else {
            if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }
            } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }
            }
        }
        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        Allitem.setSidx(request.getParameter("sidx"));
        Allitem.setSord(request.getParameter("sord"));
        Allitem.setStartDate(startDate);
        Allitem.setEndDate(endDate);

        int totalCount = statsService.getUsedContentsDetailListTotalCount(item);

        if (currentPage == 1 || currentPage == 0) {
            totalCount += 1;
        }

        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);

        if (currentPage == 1 || currentPage == 0) {
            rows = 9;
        } else {
            offset -= 1;
        }

        item.setOffset(offset);
        item.setLimit(rows);

        Allitem.setOffset(-1);
        Allitem.setLimit(-1);

        List<Map> resultItem = statsService.getUsedContentsDetailList(item);

        List<Map> resultAllItem = statsService.getUsedContentsDetailList(Allitem);

        // Stream<Map> mapStream = resultItem.stream().filter(alpha -> "미니풋볼".equals(alpha.get("contsTitle")));
        // resultItem = mapStream.collect(Collectors.toList());

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }
        int totPlayCount = 0;
        int totPlyTime = 0;

        for (int i = 0; i < resultAllItem.size(); i++) {
            Map vItem = resultAllItem.get(i);

            String tempTimes = vItem.get("contsTotalPlayTime").toString();
            String saveTimes = vItem.get("contsTotalPlayTime").toString();
            totPlayCount += Integer.parseInt(vItem.get("contsTotalPlayCount").toString());
            if (tempTimes.contains("h")) {// h:m:s, h:m, h:s, h
                if (tempTimes.contains("m") && tempTimes.contains("s")) {// h:m:s
                    saveTimes = saveTimes.replaceAll("h", ":");
                    saveTimes = saveTimes.replaceAll("m", ":");
                    saveTimes = saveTimes.replaceAll("s", "");

                    String[] timeSet = saveTimes.split(":");

                    totPlyTime += Integer.parseInt(timeSet[0]) * 3600;
                    totPlyTime += Integer.parseInt(timeSet[1]) * 60;
                    totPlyTime += Integer.parseInt(timeSet[2]);
                } else if (tempTimes.contains("m")) {// h:m
                    saveTimes = saveTimes.replaceAll("h", ":");
                    saveTimes = saveTimes.replaceAll("m", "");

                    String[] timeSet = saveTimes.split(":");

                    totPlyTime += Integer.parseInt(timeSet[0]) * 3600;
                    totPlyTime += Integer.parseInt(timeSet[1]) * 60;

                } else if (tempTimes.contains("s")) {// h:s
                    saveTimes = saveTimes.replaceAll("h", ":");
                    saveTimes = saveTimes.replaceAll("s", "");

                    String[] timeSet = saveTimes.split(":");

                    totPlyTime += Integer.parseInt(timeSet[0]) * 3600;
                    totPlyTime += Integer.parseInt(timeSet[1]);

                } else {// h
                    totPlyTime += Integer.parseInt(saveTimes.replaceAll("h", "")) * 3600;
                }
            } else if (tempTimes.contains("m")) {// m, m:s
                if (tempTimes.contains("s")) {// m:s
                    saveTimes = saveTimes.replaceAll("m", ":");
                    saveTimes = saveTimes.replaceAll("s", "");

                    String[] timeSet = saveTimes.split(":");

                    totPlyTime += Integer.parseInt(timeSet[0]) * 60;
                    totPlyTime += Integer.parseInt(timeSet[1]);

                } else {// m
                    totPlyTime += Integer.parseInt(saveTimes.replaceAll("m", "")) * 60;
                }
            } else {// s
                totPlyTime += Integer.parseInt(saveTimes.replaceAll("s", ""));
            }

        }

        String resultTotPlayTime = "";
        if (totPlyTime != 0 && (totPlyTime / 3600) >= 1) {
            resultTotPlayTime += (totPlyTime / 3600) + "h";
            resultTotPlayTime += ((totPlyTime % 3600) / 60) + "m";
            resultTotPlayTime += ((totPlyTime % 60)) + "s";
        } else if (totPlyTime != 0 && (totPlyTime / 60) >= 1) {
            resultTotPlayTime += ((totPlyTime % 3600) / 60) + "m";
            resultTotPlayTime += ((totPlyTime % 60)) + "s";
        } else {
            resultTotPlayTime = totPlyTime + "s";
        }

        if (currentPage == 1 || currentPage == 0) {
            Map totInfo = new HashMap<>();
            /* 합계 */
            totInfo.put("useDt", "합계");
            totInfo.put("contsTitle", "");
            totInfo.put("contsTotalPlayTime", resultTotPlayTime);
            totInfo.put("contsTotalPlayCount", totPlayCount);
            totInfo.put("contsSeq", 0);
            totInfo.put("ctgNm", "");
            totInfo.put("contsTitle", "");
            if (totPlyTime != 0) {
                String resultTotAvgPlayTime = "";
                int avgSecond = totPlyTime / totPlayCount;
                if (avgSecond != 0 && (avgSecond / 3600) >= 1) {
                    resultTotAvgPlayTime += (avgSecond / 3600) + "h";
                    resultTotAvgPlayTime += ((avgSecond % 3600) / 60) + "m";
                    resultTotAvgPlayTime += ((avgSecond % 60)) + "s";
                } else if (avgSecond != 0 && (avgSecond / 60) >= 1) {
                    resultTotAvgPlayTime += ((avgSecond % 3600) / 60) + "m";
                    resultTotAvgPlayTime += ((avgSecond % 60)) + "s";
                } else {
                    resultTotAvgPlayTime = avgSecond + "s";
                }
                totInfo.put("contsAvgPlayTime", resultTotAvgPlayTime);
            } else {
                totInfo.put("contsAvgPlayTime", "0s");
            }
            resultItem.add(0, totInfo);
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("usedContentsDetail");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * CP사 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getContentsByCpList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = !this.isAdministrator(userVO);
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int totalCount = statsService.getContentsByCpListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getContentsByCpList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("contentsByCpList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 검수 현황 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getVerifiedContentsList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = !(this.isAdministrator(userVO)
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        if (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())) {
            item.setTarget(userVO.getMbrId());
        }

        int totalCount = statsService.getVerifiedContentsListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getVerifiedContentsList(item);
        /* 개인정보 마스킹 (검수자 이름, 아이디) */
        for (int i = 0; i < resultItem.size(); i++) {
            String name = CommonFnc.getNameMask(String.valueOf(resultItem.get(i).get("mbrNm")));
            String id = CommonFnc.getIdMask(String.valueOf(resultItem.get(i).get("mbrId")));
            resultItem.get(i).put("mbrNm", name + " (" + id + ")");
        }

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("verifiedContentsList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용객 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getVisitorList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int totalCount = statsService.getVisitorListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getVisitorList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("visitorList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getTagList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int totalCount = statsService.getTagListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getTagList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("tagList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권 발급 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getTagDetailList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "prodSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setProdSeq(Integer.parseInt(request.getParameter("prodSeq")));
        StatsVO storeVO = statsService.getStoreFromTag(item);
        if (storeVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(prodSeq)");
            return rsModel.getModelAndView();
        }
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != storeVO.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != storeVO.getSvcSeq() || userVO.getStorSeq() != storeVO.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        rsModel.setData("svcNm", storeVO.getSvcNm());
        rsModel.setData("storNm", storeVO.getStorNm());
        rsModel.setData("prodNm", storeVO.getProdNm());

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int totalCount = statsService.getTagDetailListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getTagDetailList(item);

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("tagDetailList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권 사용내역 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getTagHistoryList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "tagSeq:{" + Validator.NUMBER + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        StatsVO item = new StatsVO();
        item.setTagSeq(Integer.parseInt(request.getParameter("tagSeq")));

        StatsVO storeVO = statsService.getStoreFromTag(item);
        if (storeVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(tagSeq)");
            return rsModel.getModelAndView();
        }
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != storeVO.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != storeVO.getSvcSeq() || userVO.getStorSeq() != storeVO.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));

        int totalCount = statsService.getTagHistoryListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getTagHistoryList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("tagHistoryList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권별 이용 콘텐츠 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getContentsByTagList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int totalCount = statsService.getContentsByTagListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getContentsByTagList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("contentsByTagList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 런쳐통계 > 이용 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    private ModelAndView getLauncherUsedAppList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));

        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setKeyword(request.getParameter("keyword"));
        item.setTarget(request.getParameter("target"));

        int totalCount = statsService.getLauncherUsedAppListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getLauncherUsedAppList(item);

        // Stream<Map> mapStream = resultItem.stream().filter(alpha -> "미니풋볼".equals(alpha.get("contsTitle")));
        // resultItem = mapStream.collect(Collectors.toList());

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("launcherUsedApp");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 런쳐통계 > 이용 통계 상세 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    private ModelAndView getLauncherUsedAppDetailList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "appSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int appSeq = Integer.parseInt(request.getParameter("appSeq"));

        StatsVO item = new StatsVO();
        StatsVO Allitem = new StatsVO();
        item.setDateType(dateType);
        item.setSvcSeq(svcSeq);
        item.setAppSeq(appSeq);

        Allitem.setDateType(dateType);
        Allitem.setSvcSeq(svcSeq);
        Allitem.setAppSeq(appSeq);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }
        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        Allitem.setSidx(request.getParameter("sidx"));
        Allitem.setSord(request.getParameter("sord"));
        Allitem.setStartDate(startDate);
        Allitem.setEndDate(endDate);

        int totalCount = statsService.getLauncherUsedAppDetailListTotalCount(item) + 1;

        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);

        if (currentPage == 1 || currentPage == 0) {
            rows = 9;
        } else {
            offset -= 1;
        }

        item.setOffset(offset);
        item.setLimit(rows);

        Allitem.setOffset(-1);
        Allitem.setLimit(-1);

        List<Map> resultItem = statsService.getLauncherUsedAppDetailList(item);

        List<Map> resultAllItem = statsService.getLauncherUsedAppDetailList(Allitem);

        // Stream<Map> mapStream = resultItem.stream().filter(alpha -> "미니풋볼".equals(alpha.get("contsTitle")));
        // resultItem = mapStream.collect(Collectors.toList());

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }
        int totCount = 0;

        for (int i = 0; i < resultAllItem.size(); i++) {
            Map vItem = resultAllItem.get(i);
            totCount += Integer.parseInt(vItem.get("totCount").toString());
        }
        if (currentPage == 1 || currentPage == 0) {
            Map totInfo = new HashMap<>();
            /* 합계 */
            totInfo.put("useDt", "합계");
            totInfo.put("totCount", totCount);
            resultItem.add(0, totInfo);
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("launcherUsedAppDetail");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 런쳐통계 > 유입 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     * @throws Exception
     */
    private ModelAndView getLauncherTrafficList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));

        StatsVO item = new StatsVO();
        StatsVO Allitem = new StatsVO();
        item.setDateType(dateType);
        item.setSvcSeq(svcSeq);

        Allitem.setSvcSeq(svcSeq);
        Allitem.setDateType(dateType);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }
        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        Allitem.setSidx(request.getParameter("sidx"));
        Allitem.setSord(request.getParameter("sord"));
        Allitem.setStartDate(startDate);
        Allitem.setEndDate(endDate);

        int totalCount = statsService.getLauncherTrafficListTotalCount(item) + 1;

        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);

        if (currentPage == 1 || currentPage == 0) {
            rows = 9;
        } else {
            offset -= 1;
        }

        item.setOffset(offset);
        item.setLimit(rows);

        Allitem.setOffset(-1);
        Allitem.setLimit(-1);

        List<Map> resultItem = statsService.getLauncherTrafficList(item);

        List<Map> resultAllItem = statsService.getLauncherTrafficList(Allitem);

        // Stream<Map> mapStream = resultItem.stream().filter(alpha -> "미니풋볼".equals(alpha.get("contsTitle")));
        // resultItem = mapStream.collect(Collectors.toList());

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }
        int svcAppTotCount = 0;
        int mirAppTotCount = 0;

        for (int i = 0; i < resultAllItem.size(); i++) {
            Map vItem = resultAllItem.get(i);
            svcAppTotCount += Integer.parseInt(vItem.get("svcAppTotCount").toString());
            mirAppTotCount += Integer.parseInt(vItem.get("mirAppTotCount").toString());
        }

        if (currentPage == 1 || currentPage == 0) {
            Map totInfo = new HashMap<>();
            /* 합계 */
            totInfo.put("useDt", "합계");
            totInfo.put("svcAppTotCount", svcAppTotCount);
            totInfo.put("mirAppTotCount", mirAppTotCount);
            resultItem.add(0, totInfo);
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("trafficList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권별 이용 콘텐츠 상세 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getContentsDetailByTagList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        int prodSeq = CommonFnc.emptyCheckInt("prodSeq", request);
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        item.setProdSeq(prodSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int totalCount = statsService.getContentsDetailByTagListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getContentsDetailByTagList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("contentsDetailByTagList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 결제 관리 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getSettlementManageList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};" + "target";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));

        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setTarget(request.getParameter("target"));

        int totalCount = statsService.selectSettlementManageTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.selectSettlementManage(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("settlementManageList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 온라인 현황 통계 > 콘텐츠 사용 현황 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getUsedOlsvcContentsList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int totalCount = statsService.getUsedOlsvcContentsStatsListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<StatsVO> resultItem = statsService.getUsedOlsvcContentsStatsList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setData("item", resultItem);
        rsModel.setResultType("usedOlsvcContentsList");

        return rsModel.getModelAndView();
    }

    /**
     * 런처 통계 > 콘텐츠 재생 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getLauncerUsedContsList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != svcSeq) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int totalCount = statsService.getOlSvcLauncherUsedContsStatsListTotalCount(item);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        item.setOffset(offset);
        item.setLimit(rows);

        List<StatsVO> resultItem = statsService.getOlSvcLauncherUsedContsStatsList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setData("item", resultItem);
        rsModel.setResultType("olSvcLauncherUsedContsList");

        return rsModel.getModelAndView();
    }

    /**
     * 런처 통계 > 콘텐츠 재생 통계 > 상세 현황 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getLauncherUsedContsDetailList(Locale locale, HttpServletRequest request, ResultModel rsModel,
            MemberVO userVO) throws Exception {
        rsModel.setViewName(viewName);
        boolean prevented = (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "contsSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != svcSeq) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        if (dateType.equals("")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(dateType)");
            return rsModel.getModelAndView();
        }

        int contsSeq = Integer.parseInt(request.getParameter("contsSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setContsSeq(contsSeq);

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        String sidx = CommonFnc.emptyCheckString("sidx", request);
        String sord = CommonFnc.emptyCheckString("sord", request);

        item.setSidx(sidx);
        item.setSord(sord);
        item.setDateType(dateType);
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        // 1 page 첫 항목에 합계 항목이 표시되어야 하므로 1을 더함. 합계 항목은 따로 값을 계산하여 추가하고 있음.
        int totalCount = statsService.getOlSvcLauncherUsedContsDetailStatsListTotalCount(item) + 1;
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);

        if (currentPage == 1 || currentPage == 0) {
            rows = 9;
        } else {
            offset -= 1;
        }

        item.setOffset(offset);
        item.setLimit(rows);

        List<Map> resultItem = statsService.getOlSvcLauncherUsedContsDetailStatsList(item);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        StatsVO allItem = new StatsVO();
        allItem.setSvcSeq(svcSeq);
        allItem.setContsSeq(contsSeq);
        allItem.setDateType(dateType);
        allItem.setSidx(sidx);
        allItem.setSord(sord);
        allItem.setDateType(dateType);
        allItem.setStartDate(startDate);
        allItem.setEndDate(endDate);
        allItem.setOffset(-1);
        allItem.setLimit(-1);

        List<Map> resultAllItem = statsService.getOlSvcLauncherUsedContsDetailStatsList(allItem);

        int playTotCount = 0;
        for (int i = 0; i < resultAllItem.size(); i++) {
            Map mData = resultAllItem.get(i);
            playTotCount += Integer.parseInt(mData.get("playCnt").toString());
        }

        if (currentPage == 1 || currentPage == 0) {
            Map totInfo = new HashMap<>();
            /* 합계 */
            totInfo.put("useDt", messageSource.getMessage("common.totCnt", null, locale));
            totInfo.put("playCnt", playTotCount);
            resultItem.add(0, totInfo);
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setData("item", resultItem);
        rsModel.setResultType("olSvcLauncherUsedContsDetailList");

        return rsModel.getModelAndView();
    }

    /**
     * 통계 엑셀 다운로드 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/statistics/download")
    public void downloadStatistics(Locale locale, HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");

        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String validateParams = "type";
            String invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                        "invalid parameter(" + invalidParams + ")");
                return;
            }

            String type = request.getParameter("type");

            if ("displayWebTrafficOverview".equals(type)) {
                this.getDisplayWebTrafficOverviewExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("registeredContentsList".equals(type)) {
                this.getRegisteredContentsExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("usedContentsList".equals(type)) {
                this.getUsedContentsExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("usedContentsDetail".equals(type)) {
                this.getUsedDetailContentsExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("contentsByCpList".equals(type)) {
                this.getContentsByCpExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("verifiedContentsList".equals(type)) {
                this.getVerifiedContentsExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("visitorList".equals(type)) {
                this.getVisitorExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("visitorDetail".equals(type)) {
                this.getVisitorDetailExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("tagList".equals(type)) {
                this.getTagExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("tagStroeList".equals(type)) {
                this.getTagStroeListExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("tagDetailList".equals(type)) {
                this.getTagDetailExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("contentsByTagList".equals(type)) {
                this.getContentsByTagExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("settlementManageList".equals(type)) {
                this.getSettlementManageExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("usedOlsvcContentsList".equals(type)) {
                this.getUsedOlsvcContentsExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("launcherTraffic".equals(type)) {
                this.getLauncherTrafficExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("launcherUsedApp".equals(type)) {
                this.getLauncherUsedAppExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("launcherUsedAppDetail".equals(type)) {
                this.getLauncherUsedAppDetailExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("launcherUsedConts".equals(type)) {
                this.getLauncerUsedContsListExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("launcherUsedContsDetail".equals(type)) {
                this.getLauncherUsedContsDetailListExcelFile(locale, request, response, rsModel, userVO);
                return;
            }
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
    }

    /**
     * 런쳐통계 > 유입 통계 엑셀 다운로드
     *
     * @param response
     * @param locale
     *
     * @param StatsVO
     * @return 조회 목록 결과
     * @throws Exception
     */
    private void getLauncherTrafficExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));

        StatsVO item = new StatsVO();
        StatsVO Allitem = new StatsVO();
        item.setDateType(dateType);
        item.setSvcSeq(svcSeq);

        Allitem.setDateType(dateType);
        Allitem.setSvcSeq(svcSeq);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx("useDt");
        item.setSord("asc");
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setOffset(-1);
        item.setLimit(-1);
        int limit = statsService.getLauncherTrafficListTotalCount(item);
        item.setLimit(limit);

        Allitem.setSidx("useDt");
        Allitem.setSord("asc");
        Allitem.setStartDate(startDate);
        Allitem.setEndDate(endDate);

        int totalCount = statsService.getLauncherTrafficListTotalCount(item);

        Allitem.setOffset(-1);
        Allitem.setLimit(-1);
        List<Map> resultItem = statsService.getLauncherTrafficList(item);
        List<Map> resultAllItem = statsService.getLauncherTrafficList(Allitem);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return;
        }
        int svcAppTotCount = 0;
        int mirAppTotCount = 0;

        for (int i = 0; i < resultAllItem.size(); i++) {
            Map vItem = resultAllItem.get(i);
            svcAppTotCount += Integer.parseInt(vItem.get("svcAppTotCount").toString());
            mirAppTotCount += Integer.parseInt(vItem.get("mirAppTotCount").toString());
        }

        Map totInfo = new HashMap<>();

        /* 합계 */
        totInfo.put("svcAppTotCount", svcAppTotCount);
        totInfo.put("mirAppTotCount", mirAppTotCount);
        totInfo.put("useDt", "합계");
        resultItem.add(resultItem.size(), totInfo);

        String fileName = messageSource.getMessage("menu.statistics.traffic.noSpace", null, locale);

        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String dateFont = messageSource.getMessage("table.date", null, locale);
        String sheetName = messageSource.getMessage("menu.statistics.traffic.noSpace", null, locale);

        if (dateType.equals("day")) {
            dateFont = messageSource.getMessage("common.time", null, locale);
        }

        String[][] content = new String[resultItem.size()][3];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);

            int rowNum = i + 3;
            content[i][0] = String.valueOf(result.get("useDt"));
            content[i][1] = String.valueOf(result.get("svcAppTotCount"));
            content[i][2] = String.valueOf(result.get("mirAppTotCount"));
        }
        String titleHead = CommonFnc.emptyCheckString("titleHead", request);
        String[] titHed = titleHead.split(":");

        String[][] groupTitle = { { titHed[1], "0,0,1,2,type1" }, { dateFont, "1,2,0,0,type1" },
                { messageSource.getMessage("statistics.connect.column.user", null, locale), "1,1,1,2,type1" } };
        String[][] groupVal = { { titHed[0].replace(messageSource.getMessage("common.nm", null, locale), ""), "0,0", "type3" }, { "HMD", "2,1", "type1" },
                { "Mobile", "2,2", "type1" } };

        downloadExcelCustom(request, response, fileName, sheetName, null, content, locale, groupTitle, groupVal,
                "4500,4500,4500");
    }

    /**
     * 런쳐통계 > 이용 통계 상세 엑셀 다운로드
     *
     * @param response
     * @param locale
     *
     * @param StatsVO
     * @return 조회 목록 결과
     * @throws Exception
     */
    private void getLauncherUsedAppDetailExcelFile(Locale locale, HttpServletRequest request,
            HttpServletResponse response, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        String pkgNm = CommonFnc.emptyCheckString("pkgNm", request);
        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int appSeq = Integer.parseInt(request.getParameter("appSeq"));

        StatsVO Allitem = new StatsVO();
        Allitem.setSvcSeq(svcSeq);
        Allitem.setDateType(dateType);
        Allitem.setAppSeq(appSeq);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != svcSeq) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        Allitem.setSidx("useDt");
        Allitem.setSord("asc");
        Allitem.setStartDate(startDate);
        Allitem.setEndDate(endDate);
        Allitem.setOffset(-1);
        Allitem.setLimit(-1);
        List<Map> resultAllItem = statsService.getLauncherUsedAppDetailList(Allitem);

        if (resultAllItem.isEmpty()) {
            rsModel.setNoData();
            return;
        }

        int totCount = 0;

        for (int i = 0; i < resultAllItem.size(); i++) {
            Map vItem = resultAllItem.get(i);
            totCount += Integer.parseInt(vItem.get("totCount").toString());
        }

        Map totInfo = new HashMap<>();

        /* 합계 */
        totInfo.put("useDt", "합계");
        totInfo.put("totCount", totCount);
        resultAllItem.add(resultAllItem.size(), totInfo);

        String fileName = messageSource.getMessage("menu.statistics.used.detail", null, locale);
        String sheetName = messageSource.getMessage("menu.statistics.used.detail", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String dateFont = messageSource.getMessage("table.date", null, locale);

        if (dateType.equals("day")) {
            dateFont = messageSource.getMessage("common.time", null, locale);
        }

        String[][] content = new String[resultAllItem.size()][2];
        for (int i = 0; i < resultAllItem.size(); i++) {
            Map result = resultAllItem.get(i);

            int rowNum = i + 3;
            content[i][0] = String.valueOf(result.get("useDt"));
            content[i][1] = String.valueOf(result.get("totCount"));
        }
        String titleHead = CommonFnc.emptyCheckString("titleHead", request);
        String[] titHed = titleHead.split(":");

        String[][] groupTitle = { { titHed[1], "0,0,1,1,type1" },
                { messageSource.getMessage("statistics.column.AppNm", null, locale), "1,1,0,0,type2" },
                { dateFont, "2,2,0,0,type1" },

        };
        String[][] groupVal = {
                { titHed[0].replace(messageSource.getMessage("common.nm", null, locale), ""), "0,0", "type3" },
                { pkgNm, "1,1", "type4" },
                { messageSource.getMessage("statistics.column.exeCnt", null, locale), "2,1", "type1" } };

        downloadExcelCustom(request, response, fileName, sheetName, null, content, locale, groupTitle, groupVal,
                "4500,4500");
    }

    /**
     * 런쳐통계 > 이용 통계 엑셀 다운로드
     *
     * @param response
     * @param locale
     *
     * @param StatsVO
     * @return 조회 목록 결과
     * @throws Exception
     */
    private void getLauncherUsedAppExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));

        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setDateType(dateType);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setKeyword(request.getParameter("keyword"));
        item.setTarget(request.getParameter("target"));

        int totalCount = statsService.getLauncherUsedAppListTotalCount(item);

        item.setOffset(0);
        item.setLimit(totalCount);

        List<Map> resultItem = statsService.getLauncherUsedAppList(item);

        String fileName = messageSource.getMessage("menu.statistics.used", null, locale);
        String svcNm = CommonFnc.emptyCheckString("svcNm", request);
        fileName = svcNm + " " + fileName;

        String sheetName = messageSource.getMessage("menu.statistics.used", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[][] content = new String[resultItem.size()][2];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);

            int rowNum = i + 3;
            content[i][0] = String.valueOf(result.get("pkgNm"));
            content[i][1] = String.valueOf(result.get("totCount"));
        }

        String[] columnTitle = { messageSource.getMessage("statistics.column.AppNm", null, locale),
                messageSource.getMessage("statistics.column.exeCnt", null, locale) };

        downloadExcelCustom(request, response, fileName, sheetName, columnTitle, content, locale, null, null, "auto");
    }

    /**
     * 콘텐츠 다운로드 정보 목록 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getDisplayWebTrafficOverviewExcelFile(Locale locale, HttpServletRequest request,
            HttpServletResponse response, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "sidx;sord;startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        StatsVO item = new StatsVO();
        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        int limit = statsService.getDisplayWebTrafficOverviewListTotalCount(item);
        item.setLimit(limit);

        List<Map> resultItem = statsService.getDisplayWebTrafficOverviewList(item);
        String fileName = messageSource.getMessage("statistics.traffic.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        int columnTitleLength = 6;
        String[] columnTitle = new String[columnTitleLength];
        String[][] content = new String[resultItem.size()][columnTitle.length];

        int webUvTotalCnt = 0;
        int webPvTotalCnt = 0;
        int mobUvTotalCnt = 0;
        int mobPvTotalCnt = 0;
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("connectDt"));

            webPvTotalCnt += Integer.parseInt(String.valueOf(result.get("webPvCnt")));
            content[i][2] = String.valueOf(result.get("webPvCnt"));

            mobPvTotalCnt += Integer.parseInt(String.valueOf(result.get("mobPvCnt")));
            content[i][3] = String.valueOf(result.get("mobPvCnt"));

            webUvTotalCnt += Integer.parseInt(String.valueOf(result.get("webUvCnt")));
            content[i][4] = String.valueOf(result.get("webUvCnt"));

            mobUvTotalCnt += Integer.parseInt(String.valueOf(result.get("mobUvCnt")));
            content[i][5] = String.valueOf(result.get("mobUvCnt"));
        }

        columnTitle[0] = messageSource.getMessage("table.num", null, locale);
        columnTitle[1] = messageSource.getMessage("table.date", null, locale);
        columnTitle[2] = messageSource.getMessage("common.web", null, locale) + "(" + webPvTotalCnt + ")";
        columnTitle[3] = messageSource.getMessage("common.mobile", null, locale) + "(" + mobPvTotalCnt + ")";
        columnTitle[4] = messageSource.getMessage("common.web", null, locale) + "(" + webUvTotalCnt + ")";
        columnTitle[5] = messageSource.getMessage("common.mobile", null, locale) + "(" + mobUvTotalCnt + ")";

        String[][] groupTitle = { { messageSource.getMessage("table.num", null, locale), "0,1,0,0" },
                { messageSource.getMessage("table.date", null, locale), "0,1,1,1" },
                { messageSource.getMessage("common.pv", null, locale) + "(" + (webPvTotalCnt + mobPvTotalCnt) + ")",
                        "0,0,2,3" },
                { messageSource.getMessage("common.uv", null, locale) + "(" + (webUvTotalCnt + mobUvTotalCnt) + ")",
                        "0,0,4,5" }, };
        downloadExcel(request, response, fileName, columnTitle, content, groupTitle, locale);
    }

    /**
     * 콘텐츠 등록 현황 통계 목록 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getRegisteredContentsExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "section";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        StatsVO item = new StatsVO();
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            item.setSvcSeq(userVO.getSvcSeq());
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setTarget(request.getParameter("section"));
        int limit = statsService.getRegisteredContentsListTotalCount(item);
        item.setLimit(limit);

        List<Map> resultItem = statsService.getRegisteredContentsList(item);
        String fileName = messageSource.getMessage("statistics.contents.registered.title", null, locale);
        if ("service".equals(item.getTarget())) {
            fileName += "(" + messageSource.getMessage("common.service", null, locale) + ")";
        } else if ("cp".equals(item.getTarget())) {
            fileName += "(" + messageSource.getMessage("common.cp.office", null, locale) + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("common.service", null, locale),
                messageSource.getMessage("select.all", null, locale),
                messageSource.getMessage("contents.table.state.regist", null, locale),
                messageSource.getMessage("contents.table.state.verify.Waiting", null, locale),
                messageSource.getMessage("contents.table.state.verify.Companion", null, locale),
                messageSource.getMessage("contents.table.state.verify.ing", null, locale),
                messageSource.getMessage("contents.table.state.verify.success", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("name"));
            content[i][2] = String.valueOf(result.get("totalCnt"));
            content[i][3] = String.valueOf(result.get("saveCnt"));
            content[i][4] = String.valueOf(result.get("requestCnt"));
            content[i][5] = String.valueOf(result.get("verifyCnt"));
            content[i][6] = String.valueOf(result.get("rejectCnt"));
            content[i][7] = String.valueOf(result.get("completeCnt"));
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 콘텐츠 사용현황 목록 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getUsedContentsExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "section;svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "cpSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};" + "endDate:{"
                + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        String section = request.getParameter("section");

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        int cpSeq = Integer.parseInt(request.getParameter("cpSeq"));

        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        item.setCpSeq(cpSeq);

        if ("service".equals("section") || "cp".equals(section)) {
            if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                    || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return;
                }
            }
        } else {
            if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return;
                }
            } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return;
                }
            }
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setKeyword(request.getParameter("keyword"));
        item.setTarget(request.getParameter("target"));
        int limit = statsService.getUsedContentsListTotalCount(item);
        item.setLimit(limit);

        List<Map> resultItem = statsService.getUsedContentsList(item);
        String fileName = messageSource.getMessage("statistics.contents.used.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("table.category", null, locale),
                messageSource.getMessage("contents.cpNm", null, locale),
                messageSource.getMessage("contents.table.name", null, locale),
                messageSource.getMessage("contents.table.playCount", null, locale),
                messageSource.getMessage("contents.table.playTime", null, locale),
                messageSource.getMessage("contents.table.avgPlayTime", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("ctgNm"));
            content[i][2] = String.valueOf(result.get("cpNm"));
            content[i][3] = String.valueOf(result.get("contsTitle"));
            content[i][4] = String.valueOf(result.get("contsTotalPlayCount"));
            content[i][5] = String.valueOf(result.get("contsTotalPlayTime"));
            content[i][6] = String.valueOf(result.get("contsAvgPlayTime"));
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 콘텐츠 상세 사용현황 목록 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getUsedDetailContentsExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "section;svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "contsSeq:{" + Validator.NUMBER + "};" + "cpSeq:{" + Validator.NUMBER + "};" + "startDate:{"
                + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        String section = request.getParameter("section");

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        int cpSeq = Integer.parseInt(request.getParameter("cpSeq"));
        int contsSeq = Integer.parseInt(request.getParameter("contsSeq"));

        StatsVO item = new StatsVO();
        StatsVO Allitem = new StatsVO();
        item.setDateType(dateType);
        item.setDateType(dateType);
        item.setSvcSeq(svcSeq);

        item.setStorSeq(storSeq);
        item.setContsSeq(contsSeq);
        item.setCpSeq(cpSeq);

        Allitem.setSvcSeq(svcSeq);
        Allitem.setContsSeq(contsSeq);
        Allitem.setStorSeq(storSeq);
        Allitem.setCpSeq(cpSeq);

        if ("service".equals(section) || "cp".equals(section)) {
            if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                    || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return;
                }
            }
        } else {
            if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return;
                }
            } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
                if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return;
                }
            }
        }

        item.setSidx("useDt");
        item.setSord("asc");
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setOffset(-1);
        item.setLimit(-1);
        int limit = statsService.getUsedContentsDetailListTotalCount(item);
        item.setLimit(limit);

        Allitem.setSidx(request.getParameter("sidx"));
        Allitem.setSord(request.getParameter("sord"));
        Allitem.setStartDate(startDate);
        Allitem.setEndDate(endDate);

        int totalCount = statsService.getUsedContentsDetailListTotalCount(item);

        Allitem.setOffset(-1);
        Allitem.setLimit(-1);
        List<Map> resultItem = statsService.getUsedContentsDetailList(item);
        List<Map> resultAllItem = statsService.getUsedContentsDetailList(Allitem);

        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return;
        }
        int totPlayCount = 0;
        int totPlyTime = 0;

        for (int i = 0; i < resultAllItem.size(); i++) {
            Map vItem = resultAllItem.get(i);
            String tempTimes = vItem.get("contsTotalPlayTime").toString();
            String saveTimes = vItem.get("contsTotalPlayTime").toString();
            totPlayCount += Integer.parseInt(vItem.get("contsTotalPlayCount").toString());
            if (tempTimes.contains("h")) {// h:m:s, h:m, h:s, h
                if (tempTimes.contains("m") && tempTimes.contains("s")) {// h:m:s
                    saveTimes = saveTimes.replaceAll("h", ":");
                    saveTimes = saveTimes.replaceAll("m", ":");
                    saveTimes = saveTimes.replaceAll("s", "");
                    String[] timeSet = saveTimes.split(":");

                    totPlyTime += Integer.parseInt(timeSet[0]) * 3600;
                    totPlyTime += Integer.parseInt(timeSet[1]) * 60;
                    totPlyTime += Integer.parseInt(timeSet[2]);
                } else if (tempTimes.contains("m")) {// h:m
                    saveTimes = saveTimes.replaceAll("h", ":");
                    saveTimes = saveTimes.replaceAll("m", "");
                    String[] timeSet = saveTimes.split(":");

                    totPlyTime += Integer.parseInt(timeSet[0]) * 3600;
                    totPlyTime += Integer.parseInt(timeSet[1]) * 60;
                } else if (tempTimes.contains("s")) {// h:s
                    saveTimes = saveTimes.replaceAll("h", ":");
                    saveTimes = saveTimes.replaceAll("s", "");
                    String[] timeSet = saveTimes.split(":");

                    totPlyTime += Integer.parseInt(timeSet[0]) * 3600;
                    totPlyTime += Integer.parseInt(timeSet[1]);
                } else {// h
                    totPlyTime += Integer.parseInt(saveTimes.replaceAll("h", "")) * 3600;
                }
            } else if (tempTimes.contains("m")) {// m, m:s
                if (tempTimes.contains("s")) {// m:s
                    saveTimes = saveTimes.replaceAll("m", ":");
                    saveTimes = saveTimes.replaceAll("s", "");
                    String[] timeSet = saveTimes.split(":");

                    totPlyTime += Integer.parseInt(timeSet[0]) * 60;
                    totPlyTime += Integer.parseInt(timeSet[1]);
                } else {// m
                    totPlyTime += Integer.parseInt(saveTimes.replaceAll("m", "")) * 60;
                }
            } else {// s
                totPlyTime += Integer.parseInt(saveTimes.replaceAll("s", ""));
            }
        }

        String resultTotPlayTime = "";
        if (totPlyTime != 0 && (totPlyTime / 3600) >= 1) {
            resultTotPlayTime += (totPlyTime / 3600) + "h";
            resultTotPlayTime += ((totPlyTime % 3600) / 60) + "m";
            resultTotPlayTime += ((totPlyTime % 60)) + "s";
        } else if (totPlyTime != 0 && (totPlyTime / 60) >= 1) {
            resultTotPlayTime += ((totPlyTime % 3600) / 60) + "m";
            resultTotPlayTime += ((totPlyTime % 60)) + "s";
        } else {
            resultTotPlayTime = totPlyTime + "s";
        }

        Map totInfo = new HashMap<>();

        /* 합계 */
        totInfo.put("contsTitle", "");
        totInfo.put("contsTotalPlayTime", resultTotPlayTime);
        totInfo.put("contsTotalPlayCount", totPlayCount);
        totInfo.put("contsSeq", 0);
        totInfo.put("ctgNm", "");
        totInfo.put("contsTitle", "");
        totInfo.put("useDt", "합계");
        if (totPlyTime != 0) {
            String resultTotAvgPlayTime = "";
            int avgSecond = totPlyTime / totPlayCount;
            if (avgSecond != 0 && (avgSecond / 3600) >= 1) {
                resultTotAvgPlayTime += (avgSecond / 3600) + "h";
                resultTotAvgPlayTime += ((avgSecond % 3600) / 60) + "m";
                resultTotAvgPlayTime += ((avgSecond % 60)) + "s";
            } else if (avgSecond != 0 && (avgSecond / 60) >= 1) {
                resultTotAvgPlayTime += ((avgSecond % 3600) / 60) + "m";
                resultTotAvgPlayTime += ((avgSecond % 60)) + "s";
            } else {
                resultTotAvgPlayTime = avgSecond + "s";
            }
            totInfo.put("contsAvgPlayTime", resultTotAvgPlayTime);
        } else {
            totInfo.put("contsAvgPlayTime", "0s");
        }
        resultItem.add(0, totInfo);

        String fileName = messageSource.getMessage("statistics.contents.used.title.detail", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.date", null, locale),
                messageSource.getMessage("contents.table.playCount", null, locale),
                messageSource.getMessage("contents.table.playTime", null, locale),
                messageSource.getMessage("contents.table.avgPlayTime", null, locale) };

        if (dateType.equals("day")) {
            columnTitle[0] = messageSource.getMessage("common.time", null, locale);
        }

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);

            int rowNum = i + 3;
            content[i][0] = String.valueOf(result.get("useDt"));
            content[i][1] = String.valueOf(result.get("contsTotalPlayCount"));
            content[i][2] = String.valueOf(result.get("contsTotalPlayTime"));
            content[i][3] = String.valueOf(result.get("contsAvgPlayTime"));
        }

        String[][] groupTitle = { { messageSource.getMessage("common.service", null, locale), "0,0,0,0" },
                { "값", "0,0,1,3" }, { messageSource.getMessage("stats.table.market", null, locale), "1,1,0,0" },
                { "값", "1,1,1,3" }, { messageSource.getMessage("common.contents.nm", null, locale), "2,2,0,0" },
                { "값", "2,2,1,3" }, };

        downloadExcelDetail(request, response, fileName, columnTitle, content, locale, null);
    }

    /**
     * CP사 통계 목록 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getContentsByCpExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = !this.isAdministrator(userVO);
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "sidx;sord;startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        StatsVO item = new StatsVO();
        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        int limit = statsService.getContentsByCpListTotalCount(item);
        item.setLimit(limit);

        List<Map> resultItem = statsService.getContentsByCpList(item);
        String fileName = messageSource.getMessage("statistics.cp.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("common.cp.office", null, locale),
                messageSource.getMessage("statistics.table.total.cnt", null, locale),
                messageSource.getMessage("statistics.table.verify.cnt", null, locale),
                messageSource.getMessage("statistics.table.complete.cnt", null, locale),
                messageSource.getMessage("statistics.table.reject.cnt", null, locale),
                messageSource.getMessage("statistics.table.display.cnt", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("cpNm"));
            content[i][2] = String.valueOf(result.get("totalCnt"));
            content[i][3] = String.valueOf(result.get("verifyCnt"));
            content[i][4] = String.valueOf(result.get("completeCnt"));
            content[i][5] = String.valueOf(result.get("rejectCnt"));
            content[i][6] = String.valueOf(result.get("displayCnt"));
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 검수 현황 통계 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getVerifiedContentsExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = !(this.isAdministrator(userVO)
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        StatsVO item = new StatsVO();
        if (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())) {
            item.setTarget(userVO.getMbrId());
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int limit = statsService.getVerifiedContentsListTotalCount(item);
        item.setLimit(limit);
        List<Map> resultItem = statsService.getVerifiedContentsList(item);
        // 개인정보 마스킹 (검수자 이름, 아이디) */
        for (int i = 0; i < resultItem.size(); i++) {
            String name = CommonFnc.getNameMask(String.valueOf(resultItem.get(i).get("mbrNm")));
            String id = CommonFnc.getIdMask(String.valueOf(resultItem.get(i).get("mbrId")));
            resultItem.get(i).put("mbrNm", name + " (" + id + ")");
        }
        String fileName = messageSource.getMessage("statistics.verifier.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("column.title.verify.confirmer", null, locale),
                messageSource.getMessage("statistics.table.complete.cnt", null, locale),
                messageSource.getMessage("statistics.table.reject.cnt", null, locale),
                messageSource.getMessage("statistics.table.verify.sum", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("mbrNm"));
            content[i][2] = String.valueOf(result.get("completeCnt"));
            content[i][3] = String.valueOf(result.get("rejectCnt"));
            content[i][4] = String.valueOf(result.get("verifyTotalCnt"));
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 이용객 통계 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getVisitorExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int limit = statsService.getVisitorListTotalCount(item);
        item.setLimit(limit);
        List<Map> resultItem = statsService.getVisitorList(item);
        String fileName = messageSource.getMessage("statistics.store.visitor.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("common.service", null, locale),
                messageSource.getMessage("common.store", null, locale),
                messageSource.getMessage("dashboard.store.totalVisit", null, locale),
                messageSource.getMessage("dashboard.store.leave", null, locale),
                messageSource.getMessage("dashboard.store.entering", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("svcNm"));
            content[i][2] = String.valueOf(result.get("storNm"));
            content[i][3] = String.valueOf(result.get("visitedTotalCnt"));
            content[i][4] = String.valueOf(result.get("leftCnt"));
            content[i][5] = String.valueOf(result.get("enteringCnt"));
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 이용객 통계 상세 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getVisitorDetailExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "storSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        String dateType = CommonFnc.emptyCheckString("dateType", request);
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx("useDt");
        item.setSord("asc");
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setDateType(dateType);

        int limit = statsService.getVisitorDetailListTotalCount(item);
        item.setLimit(limit);
        List<Map> resultItem = statsService.getVisitorDetailList(item);
        String fileName = messageSource.getMessage("statistics.store.visitorDetail.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.date", null, locale),
                messageSource.getMessage("dashboard.store.space.totalVisit", null, locale),
                messageSource.getMessage("dashboard.store.space.leave", null, locale),
                messageSource.getMessage("dashboard.store.space.entering", null, locale) };

        if (dateType.equals("day")) {
            columnTitle[0] = messageSource.getMessage("common.time", null, locale);
        }

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);

            content[i][0] = String.valueOf(result.get("useDt"));
            content[i][1] = String.valueOf(result.get("visitedTotalCnt"));
            content[i][2] = String.valueOf(result.get("leftCnt"));
            content[i][3] = String.valueOf(result.get("enteringCnt"));
        }
        Map result;
        if (resultItem.size() > 1) {
            result = resultItem.get(1);
        } else {
            result = resultItem.get(0);
        }

        String[][] groupTitle = {
                { messageSource.getMessage("common.service", null, locale), String.valueOf(result.get("svcNm")) },
                { messageSource.getMessage("common.store", null, locale), String.valueOf(result.get("storNm")) } };

        downloadExcel(request, response, fileName, columnTitle, content, groupTitle, locale);
    }

    /**
     * 결제 관련 정보API (GET-서비스 목록)
     *
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return modelAndView & API 메세지값 & WebStatus
     *         + GET - 검색조건에 따른 코드 리스트
     */
    // @RequestMapping(value = "/api/payInfo")
    // public ModelAndView launcherTagAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session,
    // @RequestBody String jsonString) throws UnsupportedEncodingException, ParseException {
    // ResultModel rsModel = new ResultModel(response);
    // rsModel.setViewName("../resources/api/service/reservate/reservateProcess");
    //
    // request.setCharacterEncoding("UTF-8");
    // ReservateVO item = new ReservateVO();
    // ProductVO pItem = new ProductVO();
    // MemberVO mItem = new MemberVO();
    //
    // // Checking Mendatory S
    // String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storCode,tagId,gsrProdCode");
    // if (!checkString.equals("")) {
    // rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
    // rsModel.setResultMessage("invalid parameter(" + checkString + ")");
    // rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
    // return rsModel.getModelAndView();
    // }
    // // Checking Mendatory E
    //
    // String mbrTokn = request.getParameter("mbrTokn") == null ? "" : request.getParameter("mbrTokn");
    // String storCode = request.getParameter("storCode") == null ? "" : request.getParameter("storCode");
    // String encodeToken = "";
    // encodeToken = mbrTokn.replaceAll(" ", "+");
    //
    // mItem.setStorCode(storCode);
    // mItem.setMbrTokn(encodeToken);
    //
    // int tokenSearchResult = 0;
    // try {
    // tokenSearchResult = memberService.searchMemberToken(mItem);
    // } catch (Exception e) {
    // rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
    // rsModel.setResultMessage("Not Token Info");
    // rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
    // return rsModel.getModelAndView();
    // }
    //
    // if (tokenSearchResult == 0) {
    // rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
    // rsModel.setResultMessage("Not Token Info");
    // rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
    // } else {
    // String tagId = request.getParameter("tagId") == null ? "" : request.getParameter("tagId");
    // String gsrProdCode = request.getParameter("gsrProdCode") == null ? "" : request.getParameter("gsrProdCode");
    //
    // if (tagId.length() == 46) {
    // DefaultTransactionDefinition def = new DefaultTransactionDefinition();
    // def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
    // TransactionStatus status = transactionManager.getTransaction(def);
    //
    // try {
    // item.setStorCode(storCode);
    // item.setGsrProdCode(gsrProdCode);
    // item.setTagId(tagId);
    // item.setRegDt("");
    //
    // // int year = Integer.parseInt(tagId.substring(19, 4));
    // int month = Integer.parseInt(tagId.substring(22, 24));
    // int day = Integer.parseInt(tagId.substring(24, 26));
    // int hour = Integer.parseInt(tagId.substring(26, 28));
    // int time = Integer.parseInt(tagId.substring(28, 30));
    //
    // if (month < 1 || month > 12 || day < 1 || day > 31 || hour < 0 || hour < 24 || time > 59
    // || time < 0) {
    // item.setRegDt("sysdate");
    // }
    //
    // item.setSettlSeq(0);
    // int result = reservateService.reservateInsert(item);
    // if (result == 1) {
    // // 상품 카테고리 조회
    // // pItem = new ProductsVo();
    // pItem.setGsrProdCode(gsrProdCode);
    // pItem.setStorCode(storCode);
    // List<ProductVO> resultCategoryItem = productService.productCategoryList(pItem);
    // for (int j = 0; j < resultCategoryItem.size(); j++) {
    // item.setProdSeq(resultCategoryItem.get(j).getProdSeq());
    // item.setDevCtgNm(resultCategoryItem.get(j).getDevCtgNm());
    // item.setLmtCnt(resultCategoryItem.get(j).getLmtCnt());
    // item.setTagSeq(0);
    // reservateService.reservateCategoryInsert(item);
    // }
    // rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
    // rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
    // } else {
    // rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
    // rsModel.setResultMessage("Insert Fail Pelease Check Product State");
    // rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
    // }
    // transactionManager.commit(status);
    // } catch (Exception e) {
    // rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
    // rsModel.setResultMessage("Insert Fail Pelease Check Product State");
    // rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
    // transactionManager.rollback(status);
    // } finally {
    // if (!status.isCompleted()) {
    // transactionManager.rollback(status);
    // }
    // }
    // } else {
    // rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
    // rsModel.setResultMessage("Not validate tokenID Info");
    // rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
    // }
    // }
    //
    // return rsModel.getModelAndView();
    // }

    /**
     * 이용권 통계 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getTagExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        int limit = statsService.getTagListTotalCount(item);
        item.setLimit(limit);

        List<Map> resultItem = statsService.getTagList(item);
        String fileName = messageSource.getMessage("statistics.store.tag.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("common.service", null, locale),
                messageSource.getMessage("common.store", null, locale),
                messageSource.getMessage("statistics.table.prod.section", null, locale),
                messageSource.getMessage("statistics.table.tag.sum", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("svcNm"));
            content[i][2] = String.valueOf(result.get("storNm"));
            content[i][3] = String.valueOf(result.get("prodNm"));
            content[i][4] = String.valueOf(result.get("tagCnt"));
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 이용권 매장별 통계 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getTagStroeListExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "storSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        String dateType = CommonFnc.emptyCheckString("dateType", request);
        StatsVO item = new StatsVO();
        StoreVO sItem = new StoreVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        sItem.setSvcSeq(svcSeq);
        sItem.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx("useDt");
        item.setSord("asc");
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setDateType(dateType);

        StoreVO resultStoreInfo = storeService.storeDetail(sItem);

        if (resultStoreInfo == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        /* 수정중 ㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏ */
        String s1 = startDate;
        String s2 = endDate;
        String dateFormat = "yyyy-MM-dd";

        if (dateType.equals("day")) {
            dateFormat = "yyyy-MM-dd HH";
            s1 += " 00";
            s2 += " 23";
        }

        DateFormat df = new SimpleDateFormat(dateFormat);
        SimpleDateFormat format1 = new SimpleDateFormat(dateFormat);

        // Date타입으로 변경

        Date d1 = df.parse(s1);
        Date d2 = df.parse(s2);

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        Calendar startT = Calendar.getInstance();
        Calendar endT = Calendar.getInstance();

        // Calendar 타입으로 변경 add()메소드로 1일씩 추가해 주기위해 변경
        c1.setTime(d1);
        c2.setTime(d2);

        List<String> dateList = new ArrayList<String>();

        while (c1.compareTo(c2) != 1) {
            String date = format1.format(c1.getTime());
            dateList.add(date);
            if (dateType.equals("day")) {
                c1.add(Calendar.HOUR, 1);
            } else {
                c1.add(Calendar.DATE, 1);
            }

        }

        Collections.sort(dateList);

        item.setOffset(-1);
        item.setLimit(-1);

        /* dateList || startDate EndDate */

        c1.setTime(df.parse(dateList.get(0)));
        c2.setTime(df.parse(dateList.get(dateList.size() - 1)));

        dateList = new ArrayList<String>();
        while (c1.compareTo(c2) != 1) {
            if (dateType.equals("day")) {
                String date = format1.format(c1.getTime());
                c1.add(Calendar.HOUR, 1);
                dateList.add(date);
            } else {
                String date = format1.format(c1.getTime());
                dateList.add(date);
                c1.add(Calendar.DATE, 1);
            }
        }

        Collections.sort(dateList);
        item.setStartDate(dateList.get(0));
        item.setEndDate(dateList.get(dateList.size() - 1));

        startT.setTime(df.parse(dateList.get(0)));
        endT.setTime(df.parse(dateList.get(dateList.size() - 1)));

        if (dateType.equals("day")) {
            item.setStartDate(startDate);
            item.setEndDate(endDate);
        }

        List<Map> resultItem = statsService.getTagStoreList(item);

        item.setStartDate(startDate);
        item.setEndDate(endDate);
        List<Map> resultPItem = statsService.getTagStoreProdList(item);
        List<Map> resultTotItem = statsService.getTagList(item);

        if (resultPItem.isEmpty()) {
            rsModel.setNoData();
            return;
        }

        String[][] prodList_dist = new String[resultPItem.size()][3];

        /* 상품 목록 */
        for (int i = 0; i < resultPItem.size(); i++) {

            prodList_dist[i][0] = Integer.toString((int) resultPItem.get(i).get("prodSeq"));
            prodList_dist[i][1] = (String) resultPItem.get(i).get("prodNm");
            prodList_dist[i][2] = "0";
            for (int j = 0; j < resultTotItem.size(); j++) {
                int lprodSeq = (int) resultTotItem.get(j).get("prodSeq");
                int rprodSeq = Integer.parseInt(prodList_dist[i][0]);

                if (lprodSeq == rprodSeq && resultTotItem.get(j).get("prodNm").equals(prodList_dist[i][1])) {
                    prodList_dist[i][2] = resultTotItem.get(j).get("tagCnt").toString();
                    break;
                }
            }

        }

        /* 상품 목록 중복제거 */
        List<List<String>> prodList_result = Arrays.stream(prodList_dist).map(Arrays::asList).distinct()
                .collect(Collectors.toList());

        List<reservateInfo> reserVateList = new ArrayList<reservateInfo>();
        // 시작날짜와 끝 날짜를 비교해, 시작날짜가 작거나 같은 경우 출력

        c1.setTime(startT.getTime());

        // dateList
        if (dateType.equals("day")) {
            dateList = new ArrayList<String>();
        }

        while (c1.compareTo(endT) != 1) {
            String date = format1.format(c1.getTime());
            for (int i = 0; i < prodList_result.size(); i++) {
                SimpleDateFormat format2 = new SimpleDateFormat("HH");
                if (dateType.equals("day")) {
                    c2.setTime(c1.getTime());
                    c2.add(Calendar.HOUR, 1);

                    date = format2.format(c1.getTime());
                    String date2 = format2.format(c2.getTime());

                    date = date + ":00" + " ~ " + date2 + ":00";
                    if (i == 0) {
                        dateList.add(date);
                    }

                }

                reservateInfo s = new reservateInfo(date, Integer.parseInt(prodList_result.get(i).get(0)),
                        prodList_result.get(i).get(1), 0);
                reserVateList.add(s);
            }

            if (dateType.equals("day")) {
                c1.add(Calendar.HOUR, 1);
            } else {
                c1.add(Calendar.DATE, 1);
            }

        }

        if (dateType.equals("day")) {
            Collections.sort(dateList);
        }

        for (reservateInfo x : reserVateList) {
            for (int i = 0; i < resultItem.size(); i++) {
                if (resultItem.get(i).get("prodSeq").equals(x.getProdSeq())
                        && resultItem.get(i).get("prodNm").equals(x.getProdNm())
                        && resultItem.get(i).get("useDt").equals(x.getDate())) {
                    Long lcount = (Long) resultItem.get(i).get("tagCnt");
                    int count = lcount.intValue();
                    x.setPutCnt(count);
                }
            }

        }

        String fileName = messageSource.getMessage("menu.statistics.tagStore", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[][] groupTitle = {
                { messageSource.getMessage("common.service", null, locale), resultStoreInfo.getSvcNm() },
                { messageSource.getMessage("common.store", null, locale), resultStoreInfo.getStorNm() } };

        downloadExcelReserVate(request, response, locale, fileName, prodList_result, dateList, groupTitle,
                reserVateList);

        /* 수정중 ㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏㅏ */
    }

    /**
     * 매장별 이용권 발급 상세 목록 엑셀
     *
     * @param
     * @return 엑셀 파일
     */
    private void downloadExcelReserVate(HttpServletRequest request, HttpServletResponse response, Locale locale,
            String fileName, List<List<String>> prodList_result, List<String> dateList, String[][] groupTitle,
            List<reservateInfo> reserVateList) throws Exception {
        XSSFWorkbook workBook = new XSSFWorkbook();

        XSSFFont defaultFontStyle = workBook.createFont();
        defaultFontStyle.setFontHeight(10);
        defaultFontStyle.setColor(HSSFColor.BLACK.index);
        defaultFontStyle.setFontName("맑은 고딕");

        // 기본 스타일
        CellStyle setCellStyle;

        CellStyle defaultCellStyle = workBook.createCellStyle();
        defaultCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        defaultCellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        defaultCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setWrapText(true);// 자동줄바꿈
        defaultCellStyle.setFont(defaultFontStyle);

        // Cell 스타일 생성
        XSSFFont titelFontStyle = workBook.createFont();
        titelFontStyle.setFontHeight(12);
        titelFontStyle.setColor(HSSFColor.BLACK.index);
        titelFontStyle.setFontName("맑은 고딕");
        titelFontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 폰트굵게

        /*
         * Detail 엑셀 스타일 정의 S
         */

        // Cell 스타일 생성
        XSSFFont detailFontStyle = workBook.createFont();
        detailFontStyle.setFontHeight(12);
        detailFontStyle.setColor(HSSFColor.BLACK.index);
        detailFontStyle.setFontName("맑은 고딕");
        detailFontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL); // 폰트굵게

        // 왼쪽상단타이틀 스타일
        CellStyle leftTopTitle = workBook.createCellStyle();
        leftTopTitle.cloneStyleFrom(defaultCellStyle);
        leftTopTitle.setFont(detailFontStyle);
        leftTopTitle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        leftTopTitle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        // 상품명 스타일
        CellStyle prodTitleStyle = workBook.createCellStyle();
        prodTitleStyle.cloneStyleFrom(defaultCellStyle);
        prodTitleStyle.setFont(detailFontStyle);
        XSSFColor prodTitleColor = new XSSFColor(new java.awt.Color(198, 224, 180));
        ((XSSFCellStyle) prodTitleStyle).setFillForegroundColor(prodTitleColor);
        prodTitleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        // 콘텐츠 상단타이틀 스타일
        CellStyle contentsTitle = workBook.createCellStyle();
        contentsTitle.cloneStyleFrom(defaultCellStyle);
        contentsTitle.setFont(detailFontStyle);
        contentsTitle.setFillForegroundColor(IndexedColors.PALE_BLUE.index);
        contentsTitle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        /*
         * Detail 엑셀 스타일 정의 E
         */

        Sheet sheet;
        String type = CommonFnc.emptyCheckString("type", request);

        setCellStyle = defaultCellStyle;

        // 타이틀 스타일
        CellStyle titleCellStyle = workBook.createCellStyle();
        titleCellStyle.cloneStyleFrom(setCellStyle);
        titleCellStyle.setFont(titelFontStyle);
        titleCellStyle.setFillForegroundColor(IndexedColors.TAN.index);
        titleCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        boolean hasGroupTitle = (groupTitle != null);
        int baseIndex = hasGroupTitle ? 3 : 0;

        sheet = workBook.createSheet(fileName);

        /* HEADER SET S */
        if (hasGroupTitle) {
            for (int i = 0; i < groupTitle.length; i++) {
                Row groupRow = sheet.createRow(i);
                String title = groupTitle[i][0];
                String titleVal = groupTitle[i][1];

                Cell leftTitle = groupRow.createCell(0);
                leftTitle.setCellStyle(leftTopTitle);
                leftTitle.setCellValue(title);

                for (int j = 0; j < prodList_result.size(); j++) {
                    Cell rightVal = groupRow.createCell(1 + j);
                    rightVal.setCellStyle(setCellStyle);
                    rightVal.setCellValue(titleVal);
                }
            }
            /* 이용권 종류 */
            Row productListRow = sheet.createRow(groupTitle.length);
            Cell leftTitle = productListRow.createCell(0);
            leftTitle.setCellStyle(leftTopTitle);
            leftTitle.setCellValue(messageSource.getMessage("statistics.table.prod.section", null, locale));

            for (int j = 0; j < prodList_result.size(); j++) {
                Cell rightVal = productListRow.createCell(1 + j);
                rightVal.setCellStyle(prodTitleStyle);
                rightVal.setCellValue(prodList_result.get(j).get(1));
            }

        }

        /* HEADER SET E */

        /* DATE SET S */
        for (int i = 0; i < dateList.size(); i++) {
            String date = dateList.get(i);
            Row datwRow = sheet.createRow(i + baseIndex);

            Cell leftTitle = datwRow.createCell(0);
            leftTitle.setCellStyle(setCellStyle);
            leftTitle.setCellValue(dateList.get(i));
            for (int k = 0; k < prodList_result.size(); k++) {
                int insetCount = 0;
                for (int j = 0; j < reserVateList.size(); j++) {
                    if (date.equals(reserVateList.get(j).getDate())
                            && Integer.parseInt(prodList_result.get(k).get(0)) == reserVateList.get(j).getProdSeq()) {
                        Cell dateSet = datwRow.createCell(1 + k);
                        dateSet.setCellStyle(setCellStyle);
                        dateSet.setCellValue(reserVateList.get(j).getPutCnt());
                        insetCount++;
                    }
                    if (insetCount == prodList_result.size()) {
                        j = reserVateList.size();
                    }
                }
            }

        }

        /* DATE SET E */

        /* 합계 S */
        Row totRow = sheet.createRow(baseIndex + dateList.size());
        Cell cell_totTal = totRow.createCell(0);
        cell_totTal.setCellStyle(setCellStyle);
        cell_totTal.setCellValue(messageSource.getMessage("common.reservate.totCnt", null, locale));
        sheet.setColumnWidth((short) 0, (short) 4000);

        for (int k = 1; k <= prodList_result.size(); k++) {
            Cell cell_totTal1 = totRow.createCell(k);
            cell_totTal1.setCellStyle(setCellStyle);
            cell_totTal1.setCellValue(prodList_result.get(k - 1).get(2));
            sheet.setColumnWidth((short) k, (short) 5000);
        }
        /* 합계 E */

        // 엑셀 파일 다운로드
        response.setContentType("Application/octet-stream");
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
            response.setHeader("Content-Disposition",
                    "filename=" + (URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("MSIE") > -1) { // MS IE (보통은 6.x 이상 가정)
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("Trident") > -1 || userAgent.indexOf("Edge") > -1) { // MS IE 11, Edge
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else { // 모질라나 오페라
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (new String(fileName.getBytes("UTF-8"), "latin1") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        }

        OutputStream os = response.getOutputStream();
        workBook.write(os);
        os.flush();
        os.close();

    }

    /**
     * 이용권 발급 목록 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getTagDetailExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "prodSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        StatsVO item = new StatsVO();
        item.setProdSeq(Integer.parseInt(request.getParameter("prodSeq")));
        StatsVO storeVO = statsService.getStoreFromTag(item);
        if (storeVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(tagSeq)");
            return;
        }
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != storeVO.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != storeVO.getSvcSeq() || userVO.getStorSeq() != storeVO.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        int limit = statsService.getTagDetailListTotalCount(item);
        item.setLimit(limit);

        List<Map> resultItem = statsService.getTagDetailList(item);
        String fileName = messageSource.getMessage("reservate.list.title", null, locale);
        String productName = storeVO.getProdNm();

        if (productName != null && !productName.isEmpty()) {
            fileName += "_" + productName;
        }
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("reservate.table.tagId", null, locale),
                messageSource.getMessage("reservate.table.useYn", null, locale),
                messageSource.getMessage("reservate.table.regDate", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("tagId"));
            content[i][2] = String.valueOf(result.get("useSttus"));
            content[i][3] = String.valueOf(result.get("cretDt"));
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 이용권 별 이용 콘텐츠 목록 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getContentsByTagExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        int limit = statsService.getContentsDetailByTagListTotalCount(item);
        item.setLimit(limit);

        List<Map> resultItem = statsService.getContentsDetailByTagList(item);
        String fileName = messageSource.getMessage("statistics.store.contentsByTag.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("common.service", null, locale),
                messageSource.getMessage("common.store", null, locale),
                messageSource.getMessage("statistics.table.prod.section", null, locale),
                messageSource.getMessage("statistics.table.used.contents", null, locale),
                messageSource.getMessage("common.cntNum", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.get("svcNm"));
            content[i][2] = String.valueOf(result.get("storNm"));
            content[i][3] = String.valueOf(result.get("prodNm"));
            content[i][4] = String.valueOf(result.get("contsTitle"));
            content[i][5] = String.valueOf(result.get("contsPlayCnt"));
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 결제 관리 목록 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getSettlementManageExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};" + "target";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setStorSeq(storSeq);
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq() || userVO.getStorSeq() != item.getStorSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);
        item.setTarget(request.getParameter("target"));

        int limit = statsService.selectSettlementManageTotalCount(item);
        item.setLimit(limit);
        List<Map> resultItem = statsService.selectSettlementManage(item);
        String fileName = messageSource.getMessage("statistics.payInfo.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage(
                        "day".equals(item.getTarget()) ? "statistics.payInfo.table.top" : "common.period", null,
                        locale),
                messageSource.getMessage("statistics.payInfo.table.netAmount", null, locale),
                messageSource.getMessage("statistics.payInfo.table.card", null, locale),
                messageSource.getMessage("statistics.payInfo.table.money", null, locale),
                messageSource.getMessage("statistics.payInfo.table.etc", null, locale),
                messageSource.getMessage("statistics.payInfo.table.discount", null, locale) };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            if ("day".equals(item.getTarget())) {
                String description = messageSource.getMessage("statistics.payInfo.table.today", null, locale);
                if (i == 1) {
                    description = messageSource.getMessage("statistics.payInfo.table.yesterday", null, locale);
                } else if (i == 2) {
                    description = messageSource.getMessage("statistics.payInfo.table.prevWeek", null, locale);
                }

                String settleDt = description + "\r\n" + result.get("settlDt") + " ("
                        + CommonFnc.getDayOfWeek(String.valueOf(result.get("settlDt"))) + ")";
                content[i][1] = String.valueOf(settleDt);
                content[i][2] = this.settleAmountFormatter(i, resultItem.get(0).get("totalAmount"),
                        result.get("totalAmount"));
                content[i][3] = this.settleAmountFormatter(i, resultItem.get(0).get("cardAmount"),
                        result.get("cardAmount"));
                content[i][4] = this.settleAmountFormatter(i, resultItem.get(0).get("cashAmount"),
                        result.get("cashAmount"));
                content[i][5] = this.settleAmountFormatter(i, resultItem.get(0).get("etcAmount"),
                        result.get("etcAmount"));
                content[i][6] = this.settleAmountFormatter(i, resultItem.get(0).get("dscntAmount"),
                        result.get("dscntAmount"));
            } else {
                content[i][1] = String.valueOf(result.get("settlDt"));
                content[i][2] = String.format("\\%,d", result.get("totalAmount"));
                content[i][3] = String.format("\\%,d", result.get("cardAmount"));
                content[i][4] = String.format("\\%,d", result.get("cashAmount"));
                content[i][5] = String.format("\\%,d", result.get("etcAmount"));
                content[i][6] = String.format("\\%,d", result.get("dscntAmount"));
            }
        }

        downloadExcel(request, response, fileName, columnTitle, content, null, locale);
    }

    /**
     * 온라인 현황 통계 > 콘텐츠 사용 현황 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getUsedOlsvcContentsExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response,
            ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);

        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != item.getSvcSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        String fileName = messageSource.getMessage("statistics.online.title", null, locale);
        String svcNm = CommonFnc.emptyCheckString("svcNm", request);
        if (!svcNm.equals("")) {
            fileName += " " + svcNm + " ";
        } else {
            fileName += " ";
        }

        fileName += messageSource.getMessage("statistics.online.used.contents.title", null, locale);

        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        List<Map<Object, Object>> lData = new ArrayList<Map<Object, Object>>();

        // 첫번째 시트 - 콘텐츠 사용 현황 통계 목록 조회
        Map<Object, Object> mFirstSheet = new HashMap<Object, Object>();
        int limit = statsService.getUsedOlsvcContentsStatsListTotalCount(item);
        item.setLimit(limit);
        List<StatsVO> resultStatsList = statsService.getUsedOlsvcContentsStatsList(item);

        String sheetName = messageSource.getMessage("statistics.online.used.contents.title", null, locale);

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("common.contents.cp.nm", null, locale),
                messageSource.getMessage("common.contents.nm", null, locale),
                messageSource.getMessage("common.contents.sub.title", null, locale),
                messageSource.getMessage("common.contents.total.click.count", null, locale),
                messageSource.getMessage("common.contents.total.streaming.count", null, locale),
                messageSource.getMessage("common.contents.total.streaming.time", null, locale),
                messageSource.getMessage("common.contents.total.download.count", null, locale) };

        String[][] content = new String[resultStatsList.size()][columnTitle.length];
        for (int i = 0; i < resultStatsList.size(); i++) {
            StatsVO result = resultStatsList.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.getCpNm());
            content[i][2] = String.valueOf(result.getContsTitle());
            content[i][3] = String.valueOf(result.getContsSubTitle());
            content[i][4] = String.valueOf(result.getContsClickCnt());
            content[i][5] = String.valueOf(result.getContsStmCnt());
            content[i][6] = String.valueOf(result.getContsPlayTime());
            content[i][7] = String.valueOf(result.getContsDlCnt());
        }

        mFirstSheet.put("sheetNm", sheetName);
        mFirstSheet.put("columnTitle", columnTitle);
        mFirstSheet.put("content", content);
        mFirstSheet.put("groupTitle", null);
        lData.add(mFirstSheet);

        // 두번째 시트 - 콘텐츠 사용 현황 목록 조회
        Map<Object, Object> mSecondSheet = new HashMap<Object, Object>();
        limit = statsService.getUsedOlsvcContentsListTotalCount(item);
        item.setLimit(limit);
        List<Map<Object, Object>> resultContsList = statsService.getUsedOlsvcContentsList(item);

        sheetName = messageSource.getMessage("statistics.online.used.contents.detail.title", null, locale);

        columnTitle = new String[] { messageSource.getMessage("table.num", null, locale), "UID",
                messageSource.getMessage("statistics.online.table.serial.num", null, locale),
                messageSource.getMessage("statistics.online.table.contents.seq", null, locale),
                messageSource.getMessage("statistics.online.table.cp.nm", null, locale),
                messageSource.getMessage("statistics.online.table.contents.title", null, locale),
                messageSource.getMessage("statistics.online.table.contents.sub.title", null, locale),
                messageSource.getMessage("statistics.online.table.play.type", null, locale),
                messageSource.getMessage("statistics.online.table.contents.url", null, locale),
                messageSource.getMessage("statistics.online.table.genre", null, locale),
                messageSource.getMessage("statistics.online.table.start.date", null, locale),
                messageSource.getMessage("statistics.online.table.end.date", null, locale),
                messageSource.getMessage("statistics.online.table.play.time", null, locale) };

        content = new String[resultContsList.size()][columnTitle.length];
        for (int i = 0; i < resultContsList.size(); i++) {
            Map<Object, Object> result = resultContsList.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = CommonFnc.getIdMask(String.valueOf(result.get("uid")));
            content[i][2] = String.valueOf(result.get("devSerial"));
            content[i][3] = String.valueOf(result.get("contsSeq"));
            content[i][4] = String.valueOf(result.get("cpNm"));
            content[i][5] = String.valueOf(result.get("contsTitle"));
            content[i][6] = String.valueOf(result.get("contsSubTitle"));
            content[i][7] = String.valueOf(result.get("playType"));
            content[i][8] = String.valueOf(result.get("exeFilePath"));
            content[i][9] = String.valueOf(result.get("genreNm"));
            content[i][10] = String.valueOf(result.get("contsPlayStDt"));
            content[i][11] = String.valueOf(result.get("contsPlayFnsDt"));
            content[i][12] = String.valueOf(result.get("contsPlayTime"));
        }

        mSecondSheet.put("sheetNm", sheetName);
        mSecondSheet.put("columnTitle", columnTitle);
        mSecondSheet.put("content", content);
        mSecondSheet.put("groupTitle", null);
        lData.add(mSecondSheet);

        downloadExcelMultiSheet(request, response, fileName, lData, locale);
    }

    /**
     * 런처통계 > 콘텐츠 재생 통계 엑셀 다운로드
     *
     * @param response
     * @param locale
     *
     * @param StatsVO
     * @return 조회 목록 결과
     * @throws Exception
     */
    private void getLauncerUsedContsListExcelFile(Locale locale, HttpServletRequest request,
            HttpServletResponse response, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != svcSeq) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setSidx(request.getParameter("sidx"));
        item.setSord(request.getParameter("sord"));
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int limit = statsService.getOlSvcLauncherUsedContsStatsListTotalCount(item);
        item.setLimit(limit);
        List<StatsVO> resultStatsList = statsService.getOlSvcLauncherUsedContsStatsList(item);

        String sheetName = messageSource.getMessage("menu.statistics.plcyConts", null, locale);

        String[] columnTitle = { messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("common.contents.cp.nm", null, locale),
                messageSource.getMessage("common.category", null, locale),
                messageSource.getMessage("common.contents.nm", null, locale),
                messageSource.getMessage("statistics.column.plcyCnt", null, locale) };

        String[][] content = new String[resultStatsList.size()][columnTitle.length];
        for (int i = 0; i < resultStatsList.size(); i++) {
            StatsVO result = resultStatsList.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = String.valueOf(result.getCpNm());
            content[i][2] = String.valueOf(result.getFirstCtgNm()) + " > " + String.valueOf(result.getGenreNm());
            content[i][3] = String.valueOf(result.getContsTitle());
            content[i][4] = String.valueOf(result.getContsPlayCnt());
        }

        //String fileName = messageSource.getMessage("menu.statistics.launcher", null, locale);
        String fileName = "";

        String svcNm = CommonFnc.emptyCheckString("svcNm", request);
        if (!svcNm.equals("")) {
            fileName += " " + svcNm + " ";
        } else {
            fileName += " ";
        }

        fileName += messageSource.getMessage("menu.statistics.plcyConts", null, locale);

        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        /*
        String titleHead = CommonFnc.emptyCheckString("titleHead", request);
        String[] titHed = titleHead.split(":");

        String[][] groupTitle = { { titHed[1], "0,0,1,4,type1" } };
        String[][] groupVal = { { titHed[0].replace(messageSource.getMessage("common.nm", null, locale), ""), "0,0", "type3" } };
        */

        downloadExcelCustom(request, response, fileName, sheetName, columnTitle, content, locale, null/* groupTitle */,
                null/* groupVal */, "auto");
    }

    /**
     * 런처통계 > 콘텐츠 재생 상세 통계 엑셀 다운로드
     *
     * @param response
     * @param locale
     *
     * @param StatsVO
     * @return 조회 목록 결과
     * @throws Exception
     */
    private void getLauncherUsedContsDetailListExcelFile(Locale locale, HttpServletRequest request,
            HttpServletResponse response, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = (MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "contsSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};" + "endDate:{" + Validator.DATE + "};";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        if (MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            if (userVO.getSvcSeq() != svcSeq) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        String dateType = CommonFnc.emptyCheckString("dateType", request);
        if (dateType.equals("")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(dateType)");
            return;
        }

        int contsSeq = Integer.parseInt(request.getParameter("contsSeq"));
        String sidx = CommonFnc.emptyCheckString("sidx", request);
        String sord = CommonFnc.emptyCheckString("sord", request);

        StatsVO item = new StatsVO();
        item.setSvcSeq(svcSeq);
        item.setContsSeq(contsSeq);
        item.setSidx(sidx);
        item.setSord(sord);
        item.setDateType(dateType);
        item.setStartDate(startDate);
        item.setEndDate(endDate);

        int limit = statsService.getOlSvcLauncherUsedContsDetailStatsListTotalCount(item);
        item.setLimit(limit);
        List<Map> resultStatsList = statsService.getOlSvcLauncherUsedContsDetailStatsList(item);

        StatsVO allItem = new StatsVO();
        allItem.setSvcSeq(svcSeq);
        allItem.setContsSeq(contsSeq);
        allItem.setDateType(dateType);
        allItem.setSidx(sidx);
        allItem.setSord(sord);
        allItem.setDateType(dateType);
        allItem.setStartDate(startDate);
        allItem.setEndDate(endDate);
        allItem.setOffset(-1);
        allItem.setLimit(-1);

        List<Map> resultAllItem = statsService.getOlSvcLauncherUsedContsDetailStatsList(allItem);

        int playTotCount = 0;
        for (int i = 0; i < resultAllItem.size(); i++) {
            Map mData = resultAllItem.get(i);
            playTotCount += Integer.parseInt(mData.get("playCnt").toString());
        }

        Map totInfo = new HashMap<>();
        /* 합계 */
        totInfo.put("useDt", messageSource.getMessage("common.totCnt", null, locale));
        totInfo.put("playCnt", playTotCount);
        resultStatsList.add(totInfo);

        String sheetName = messageSource.getMessage("statistics.plcyConts.detail.title", null, locale);

        String[] columnTitle = { messageSource.getMessage("table.date", null, locale),
                messageSource.getMessage("statistics.column.plcyCnt", null, locale) };

        if (dateType.equals("day")) {
            columnTitle[0] = messageSource.getMessage("common.time", null, locale);
        }

        String[][] content = new String[resultStatsList.size()][columnTitle.length];
        for (int i = 0; i < resultStatsList.size(); i++) {
            Map result = resultStatsList.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(result.get("useDt"));
            content[i][1] = String.valueOf(result.get("playCnt"));
        }

        //String fileName = messageSource.getMessage("menu.statistics.launcher", null, locale);
        String fileName = "";

        String svcNm = CommonFnc.emptyCheckString("svcNm", request);
        if (!svcNm.equals("")) {
            fileName += " " + svcNm + " ";
        } else {
            fileName += " ";
        }

        /* 콘텐츠 명에는 특수문자가 포함되어 있어 파일 생성 시 오류 발생하므로 콘텐츠 명은 파일 명에 포함하지 않음.
        String contsTitle = CommonFnc.emptyCheckString("contsTitle", request);
        if (!contsTitle.equals("")) {
            fileName += " " + contsTitle + " ";
        } else {
            fileName += " ";
        }
        */

        fileName += messageSource.getMessage("statistics.plcyConts.detail.title", null, locale);

        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String contsTitle = CommonFnc.emptyCheckString("contsTitle", request);
        String titleHead = CommonFnc.emptyCheckString("titleHead", request);
        String[] titHed = titleHead.split(":");

        String[][] groupTitle = { { titHed[1], "0,0,1,1,type1" }, { contsTitle, "1,1,1,1,type4" } };
        String[][] groupVal = { { titHed[0].replace(messageSource.getMessage("common.nm", null, locale), ""), "0,0", "type3" }, { messageSource.getMessage("common.contents", null, locale), "1,0", "type2" } };

        downloadExcelCustom(request, response, fileName, sheetName, columnTitle, content, locale, groupTitle, groupVal,
                "4500,7550");
    }

    private void downloadExcel(HttpServletRequest request, HttpServletResponse response, String fileName,
            String[] columnTitle, String[][] content, String[][] groupTitle, Locale locale) throws Exception {
        XSSFWorkbook workBook = new XSSFWorkbook();

        XSSFFont defaultFontStyle = workBook.createFont();
        defaultFontStyle.setFontHeight(10);
        defaultFontStyle.setColor(HSSFColor.BLACK.index);
        defaultFontStyle.setFontName("맑은 고딕");

        // 기본 스타일
        CellStyle setCellStyle;

        CellStyle defaultCellStyle = workBook.createCellStyle();
        defaultCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        defaultCellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        defaultCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setWrapText(true);// 자동줄바꿈
        defaultCellStyle.setFont(defaultFontStyle);

        // Cell 스타일 생성
        XSSFFont titelFontStyle = workBook.createFont();
        titelFontStyle.setFontHeight(12);
        titelFontStyle.setColor(HSSFColor.BLACK.index);
        titelFontStyle.setFontName("맑은 고딕");
        titelFontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 폰트굵게

        /*
         * Detail 엑셀 스타일 정의 S
         */

        // Cell 스타일 생성
        XSSFFont detailFontStyle = workBook.createFont();
        detailFontStyle.setFontHeight(12);
        detailFontStyle.setColor(HSSFColor.BLACK.index);
        detailFontStyle.setFontName("맑은 고딕");
        detailFontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL); // 폰트굵게

        // 왼쪽상단타이틀 스타일
        CellStyle leftTopTitle = workBook.createCellStyle();
        leftTopTitle.cloneStyleFrom(defaultCellStyle);
        leftTopTitle.setFont(detailFontStyle);
        leftTopTitle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        leftTopTitle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        // 콘텐츠 상단타이틀 스타일
        CellStyle contentsTitle = workBook.createCellStyle();
        contentsTitle.cloneStyleFrom(defaultCellStyle);
        contentsTitle.setFont(detailFontStyle);
        contentsTitle.setFillForegroundColor(IndexedColors.PALE_BLUE.index);
        contentsTitle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        // 누적시간 컬럼 설정
        CellStyle playTimeCellStyle = workBook.createCellStyle();
        playTimeCellStyle.cloneStyleFrom(defaultCellStyle);
        CreationHelper creatHelper = workBook.getCreationHelper();
        playTimeCellStyle.setDataFormat(creatHelper.createDataFormat().getFormat("[h]:mm:ss"));

        /*
         * Detail 엑셀 스타일 정의 E
         */

        Sheet sheet;
        String type = CommonFnc.emptyCheckString("type", request);

        setCellStyle = defaultCellStyle;

        // 타이틀 스타일
        CellStyle titleCellStyle = workBook.createCellStyle();
        titleCellStyle.cloneStyleFrom(setCellStyle);
        titleCellStyle.setFont(titelFontStyle);
        titleCellStyle.setFillForegroundColor(IndexedColors.TAN.index);
        titleCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        boolean hasGroupTitle = (groupTitle != null);
        int baseIndex = hasGroupTitle ? 1 : 0;

        if (type.equals("visitorDetail")) {
            sheet = workBook.createSheet(fileName);

            baseIndex = groupTitle.length;

        } else {
            sheet = workBook.createSheet();

        }

        int visitedTotalCnt = 0;
        int leftTotalCnt = 0;
        int enteringTotalCnt = 0;

        for (int i = 0; i < content.length; i++) {
            int rowNum = i + (baseIndex + 1);

            Row row = sheet.createRow(rowNum);
            /* 합계 */
            for (int j = 0; j < content[i].length; j++) {
                if (type.equals("visitorDetail")) {
                    switch (j) {
                        case 1:
                            visitedTotalCnt += Integer.parseInt(content[i][j]);
                            break;
                        case 2:
                            leftTotalCnt += Integer.parseInt(content[i][j]);
                            break;
                        case 3:
                            enteringTotalCnt += Integer.parseInt(content[i][j]);
                            break;
                    }
                }
                Cell cell = row.createCell(j);

                // 컨텐츠 사용현황 일때
                if (type.equals("usedContentsList")) {

                    // 컬럼 제목이 Play 누적시간 일때
                    if (columnTitle[j].equals(messageSource.getMessage("contents.table.playTime", null, locale)) ||
                        columnTitle[j].equals(messageSource.getMessage("contents.table.avgPlayTime", null, locale))) {

                        // hh:mm:ss 문자열 분리
                        String[] s = content[i][j].split(":");

                        double hour = Integer.parseInt(s[0]) * 3600;
                        double minute = Integer.parseInt(s[1]) * 60;
                        double second = Integer.parseInt(s[2]);

                        // 해당 시간을 백분율로 변환
                        double time = ((hour + minute + second) / 86400);

                        cell.setCellStyle(playTimeCellStyle);
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(time);

                    } else {
                        cell.setCellStyle(setCellStyle);
                        cell.setCellValue(content[i][j]);
                    }

                } else {

                    cell.setCellStyle(setCellStyle);
                    cell.setCellValue(content[i][j]);

                }

            }
        }
        if (type.equals("visitorDetail")) {
            Row row = sheet.createRow(content.length + (baseIndex + 1));
            Cell cell_totTal = row.createCell(0);
            cell_totTal.setCellStyle(setCellStyle);
            cell_totTal.setCellValue("합계");
            sheet.setColumnWidth((short) 0, (short) 6000);

            Cell cell_totTal1 = row.createCell(1);
            cell_totTal1.setCellStyle(setCellStyle);
            cell_totTal1.setCellValue(visitedTotalCnt);
            sheet.setColumnWidth((short) 1, (short) 6000);

            Cell cell_totTal2 = row.createCell(2);
            cell_totTal2.setCellStyle(setCellStyle);
            cell_totTal2.setCellValue(leftTotalCnt);
            sheet.setColumnWidth((short) 2, (short) 6000);

            Cell cell_totTal3 = row.createCell(3);
            cell_totTal3.setCellStyle(setCellStyle);
            cell_totTal3.setCellValue(enteringTotalCnt);
            sheet.setColumnWidth((short) 3, (short) 6000);
        }

        Row headerRow = sheet.createRow(baseIndex);

        for (int i = 0; i < columnTitle.length; i++) {
            Cell cell = headerRow.createCell(i);
            if (type.equals("visitorDetail")) {
                cell.setCellStyle(setCellStyle);
            } else {
                cell.setCellStyle(titleCellStyle);
            }

            cell.setCellValue(columnTitle[i]);
            sheet.autoSizeColumn(i);
        }

        if (hasGroupTitle) {
            if (type.equals("visitorDetail")) {
                for (int i = 0; i < groupTitle.length; i++) {
                    Row groupRow = sheet.createRow(i);
                    String title = groupTitle[i][0];
                    String titleVal = groupTitle[i][1];

                    Cell leftTitle = groupRow.createCell(0);
                    leftTitle.setCellStyle(leftTopTitle);
                    leftTitle.setCellValue(title);

                    Cell rightVal = groupRow.createCell(1);
                    rightVal.setCellStyle(setCellStyle);
                    rightVal.setCellValue(titleVal);

                    Cell rightVal1 = groupRow.createCell(2);
                    rightVal1.setCellStyle(setCellStyle);

                    Cell rightVal2 = groupRow.createCell(3);
                    rightVal2.setCellStyle(setCellStyle);

                    sheet.addMergedRegion(new CellRangeAddress(i, i, 1, 3));
                }
            } else {
                Row groupRow = sheet.createRow(0);
                for (int i = 0; i < groupTitle.length; i++) {
                    String title = groupTitle[i][0];
                    String[] range = groupTitle[i][1].split(",");
                    if (range.length < 4) {
                        continue;
                    }

                    int startRowMergeIndex = Integer.parseInt(range[0]);
                    int endRowMergeIndex = Integer.parseInt(range[1]);
                    int startColumnMergeIndex = Integer.parseInt(range[2]);
                    int endColumnMergeIndex = Integer.parseInt(range[3]);

                    Cell startCell = groupRow.createCell(startColumnMergeIndex);
                    startCell.setCellStyle(titleCellStyle);
                    startCell.setCellValue(title);

                    Cell endCell = groupRow.createCell(endColumnMergeIndex);
                    endCell.setCellStyle(titleCellStyle);
                    endCell.setCellValue(title);
                    sheet.addMergedRegion(new CellRangeAddress(startRowMergeIndex, endRowMergeIndex,
                            startColumnMergeIndex, endColumnMergeIndex));
                }
            }
        }

        // 엑셀 파일 다운로드
        response.setContentType("Application/octet-stream");
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
            response.setHeader("Content-Disposition",
                    "filename=" + (URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("MSIE") > -1) { // MS IE (보통은 6.x 이상 가정)
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("Trident") > -1 || userAgent.indexOf("Edge") > -1) { // MS IE 11, Edge
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else { // 모질라나 오페라
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (new String(fileName.getBytes("UTF-8"), "latin1") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        }

        OutputStream os = response.getOutputStream();
        workBook.write(os);
        os.flush();
        os.close();
    }

    private void downloadExcelDetail(HttpServletRequest request, HttpServletResponse response, String fileName,
            String[] columnTitle, String[][] content, Locale locale, String[][] groupTitle)
            throws Exception {
        String titleHead = "";
        int titleHeadCnt = 0;

        titleHead = CommonFnc.emptyCheckString("titleHead", request);
        String contsTitle = CommonFnc.emptyCheckString("contsTitle", request);
        titleHeadCnt = titleHead.split(",").length;

        XSSFWorkbook workBook = new XSSFWorkbook();

        // Cell 스타일 생성
        XSSFFont titelFontStyle = workBook.createFont();
        titelFontStyle.setFontHeight(11);
        titelFontStyle.setColor(HSSFColor.BLACK.index);
        titelFontStyle.setFontName("맑은 고딕");

        // 기본 스타일
        CellStyle defaultCellStyle = workBook.createCellStyle();
        defaultCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        defaultCellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        defaultCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setWrapText(true);// 자동줄바꿈
        defaultCellStyle.setFont(titelFontStyle);

        XSSFFont contsFontStyle = workBook.createFont();
        contsFontStyle.setFontHeight(11);
        contsFontStyle.setColor(HSSFColor.BLACK.index);
        contsFontStyle.setFontName("맑은 고딕");
        contsFontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 폰트굵게

        // 왼쪽상단타이틀 스타일
        CellStyle leftTopTitle = workBook.createCellStyle();
        leftTopTitle.cloneStyleFrom(defaultCellStyle);
        leftTopTitle.setFont(titelFontStyle);
        leftTopTitle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        leftTopTitle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        // 콘텐츠 상단타이틀 스타일
        CellStyle contentsTitle = workBook.createCellStyle();
        contentsTitle.cloneStyleFrom(defaultCellStyle);
        contentsTitle.setFont(titelFontStyle);
        contentsTitle.setFillForegroundColor(IndexedColors.PALE_BLUE.index);
        contentsTitle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle titleCellStyle = workBook.createCellStyle();
        titleCellStyle.cloneStyleFrom(defaultCellStyle);
        titleCellStyle.setFont(contsFontStyle);
        titleCellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.index);
        titleCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        int baseIndex = 0;
        Sheet sheet = workBook.createSheet(fileName);
        sheet.setDefaultRowHeight((short) 400);
        for (int i = 0; i < content.length; i++) {
            int rowNum = i + (baseIndex + 1) + titleHeadCnt;

            if (i == 0) {
                rowNum = content.length + 1 + titleHeadCnt;
            }
            Row row = sheet.createRow(rowNum);
            row.setHeight((short) 330);
            for (int j = 0; j < content[i].length; j++) {
                Cell cell = row.createCell(j);
                cell.setCellStyle(defaultCellStyle);
                if (!(content[i][3].contains("m"))) {
                    content[i][3] = "0m " + content[i][3];
                }
                if (!(content[i][3].contains("h"))) {
                    content[i][3] = "0h " + content[i][3];
                }
                if (!(content[i][2].contains("m"))) {
                    content[i][2] = "0m " + content[i][2];
                }
                if (!(content[i][2].contains("h"))) {
                    content[i][2] = "0h " + content[i][2];
                }
                row.setHeight((short) 400);
                cell.setCellValue(content[i][j]);
            }
        }

        Row headerRow = sheet.createRow(baseIndex + titleHeadCnt + 1);
        headerRow.setHeight((short) 400);
        for (int i = 0; i < columnTitle.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellStyle(defaultCellStyle);
            cell.setCellValue(columnTitle[i]);
            sheet.autoSizeColumn(i);
        }

        for (int j = 0; j < titleHeadCnt; j++) {
            String[] titHed = titleHead.split(",");
            sheet.addMergedRegion(new CellRangeAddress(j, j, 1, 3));
            Row groupRow = sheet.createRow(j);
            groupRow.setHeight((short) 330);
            Cell Cell = groupRow.createCell(0);
            Cell.setCellStyle(leftTopTitle);
            Cell.setCellValue(titHed[j].replace(messageSource.getMessage("common.nm", null, locale), "").split(":")[0]);

            Cell Cell2 = groupRow.createCell(2);
            Cell2.setCellStyle(defaultCellStyle);
            Cell2 = groupRow.createCell(3);
            Cell2.setCellStyle(defaultCellStyle);
            Cell2 = groupRow.createCell(1);
            Cell2.setCellValue(titHed[j].split(":")[1]);
            Cell2.setCellStyle(defaultCellStyle);
        }
        Row contsTitles = sheet.createRow(titleHeadCnt);
        contsTitles.setHeight((short) 460);
        sheet.addMergedRegion(new CellRangeAddress(titleHeadCnt, titleHeadCnt, 1, 3));

        Cell Cell = contsTitles.createCell(0);
        Cell.setCellValue(messageSource.getMessage("common.contents", null, locale));
        Cell.setCellStyle(contentsTitle);
        Cell Cell2 = contsTitles.createCell(1);
        Cell2.setCellValue(contsTitle);
        Cell2.setCellStyle(titleCellStyle);
        Cell2 = contsTitles.createCell(2);
        Cell2.setCellStyle(defaultCellStyle);
        Cell2 = contsTitles.createCell(3);
        Cell2.setCellStyle(defaultCellStyle);

        // 엑셀 파일 다운로드
        response.setContentType("Application/octet-stream");
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
            response.setHeader("Content-Disposition",
                    "filename=" + (URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("MSIE") > -1) { // MS IE (보통은 6.x 이상 가정)
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("Trident") > -1 || userAgent.indexOf("Edge") > -1) { // MS IE 11, Edge
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else { // 모질라나 오페라
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (new String(fileName.getBytes("UTF-8"), "latin1") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        }

        OutputStream os = response.getOutputStream();
        workBook.write(os);
        os.flush();
        os.close();
    }

    private void downloadExcelMultiSheet(HttpServletRequest request, HttpServletResponse response, String fileName,
            List<Map<Object, Object>> data, Locale locale) throws Exception {
        XSSFWorkbook workBook = new XSSFWorkbook();

        XSSFFont defaultFontStyle = workBook.createFont();
        defaultFontStyle.setFontHeight(10);
        defaultFontStyle.setColor(HSSFColor.BLACK.index);
        defaultFontStyle.setFontName("맑은 고딕");

        // 기본 스타일
        CellStyle defaultCellStyle = workBook.createCellStyle();
        defaultCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        defaultCellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        defaultCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setWrapText(true);// 자동줄바꿈
        defaultCellStyle.setFont(defaultFontStyle);

        // Cell 스타일 생성
        XSSFFont titelFontStyle = workBook.createFont();
        titelFontStyle.setFontHeight(12);
        titelFontStyle.setColor(HSSFColor.BLACK.index);
        titelFontStyle.setFontName("맑은 고딕");
        titelFontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 폰트굵게

        // 타이틀 스타일
        CellStyle titleCellStyle = workBook.createCellStyle();
        titleCellStyle.cloneStyleFrom(defaultCellStyle);
        titleCellStyle.setFont(titelFontStyle);
        titleCellStyle.setFillForegroundColor(IndexedColors.TAN.index);
        titleCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        // 누적시간 컬럼 설정
        CellStyle playTimeCellStyle = workBook.createCellStyle();
        playTimeCellStyle.cloneStyleFrom(defaultCellStyle);
        CreationHelper creatHelper = workBook.getCreationHelper();
        playTimeCellStyle.setDataFormat(creatHelper.createDataFormat().getFormat("[h]:mm:ss"));

        for (int i = 0; i < data.size(); i++) {
            Map<Object, Object> mData = data.get(i);
            String sheetNm = mData.get("sheetNm").toString();
            String[] columnTitle = (String[]) mData.get("columnTitle");
            String[][] content = (String[][]) mData.get("content");
            String[][] groupTitle = (String[][]) mData.get("groupTitle");

            boolean hasGroupTitle = (groupTitle != null);
            int baseIndex = hasGroupTitle ? 1 : 0;
            Sheet sheet = workBook.createSheet(sheetNm);
            for (int j = 0; j < content.length; j++) {
                int rowNum = j + (baseIndex + 1);
                Row row = sheet.createRow(rowNum);
                for (int k = 0; k < content[j].length; k++) {
                    Cell cell = row.createCell(k);

                    // 컬럼 제목이 Play 누적시간 일때
                    if (columnTitle[k].equals(messageSource.getMessage("common.contents.total.streaming.time", null, locale))) {

                        // hh:mm:ss 문자열 분리
                        String[] s = content[j][k].split(":");

                        double hour = Integer.parseInt(s[0]) * 3600;
                        double minute = Integer.parseInt(s[1]) * 60;
                        double second = Integer.parseInt(s[2]);

                        // 해당 시간을 백분율로 변환
                        double time = ((hour + minute + second) / 86400);

                        cell.setCellStyle(playTimeCellStyle);
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(time);

                    } else {
                        cell.setCellStyle(defaultCellStyle);
                        cell.setCellValue(content[j][k]);
                    }
                }
            }

            Row headerRow = sheet.createRow(baseIndex);
            for (int j = 0; j < columnTitle.length; j++) {
                Cell cell = headerRow.createCell(j);
                cell.setCellStyle(titleCellStyle);
                cell.setCellValue(columnTitle[j]);
                sheet.autoSizeColumn(j);
            }

            if (hasGroupTitle) {
                Row groupRow = sheet.createRow(0);
                for (int j = 0; j < groupTitle.length; j++) {
                    String title = groupTitle[j][0];
                    String[] range = groupTitle[j][1].split(",");
                    if (range.length < 4) {
                        continue;
                    }

                    int startRowMergeIndex = Integer.parseInt(range[0]);
                    int endRowMergeIndex = Integer.parseInt(range[1]);
                    int startColumnMergeIndex = Integer.parseInt(range[2]);
                    int endColumnMergeIndex = Integer.parseInt(range[3]);

                    Cell startCell = groupRow.createCell(startColumnMergeIndex);
                    startCell.setCellStyle(titleCellStyle);
                    startCell.setCellValue(title);

                    Cell endCell = groupRow.createCell(endColumnMergeIndex);
                    endCell.setCellStyle(titleCellStyle);
                    endCell.setCellValue(title);
                    sheet.addMergedRegion(new CellRangeAddress(startRowMergeIndex, endRowMergeIndex,
                            startColumnMergeIndex, endColumnMergeIndex));
                }
            }
        }

        // 엑셀 파일 다운로드
        response.setContentType("Application/octet-stream");
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
            response.setHeader("Content-Disposition",
                    "filename=" + (URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("MSIE") > -1) { // MS IE (보통은 6.x 이상 가정)
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("Trident") > -1 || userAgent.indexOf("Edge") > -1) { // MS IE 11, Edge
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else { // 모질라나 오페라
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (new String(fileName.getBytes("UTF-8"), "latin1") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        }

        OutputStream os = response.getOutputStream();
        workBook.write(os);
        os.flush();
        os.close();
    }

    private String getDateQueryFrom(String dateType) {
        if (dateType.equals("year")) {
            dateType = "yyyy";
        } else if (dateType.equals("month")) {
            dateType = "yyyymm";
        } else if (dateType.equals("day")) {
            dateType = "yyyymmdd";
        } else if (dateType.equals("hour")) {
            dateType = "yyyymmddHH24";
        }
        return dateType;
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    private int getCurrentPage(HttpServletRequest request) {
        boolean validPageNumber = FormatCheckUtil.checkNumber(request.getParameter("page"));
        int currentPage = (validPageNumber) ? Integer.parseInt(request.getParameter("page")) : 1;
        if (currentPage <= 0) {
            currentPage = 1;
        }
        return currentPage;
    }

    private int getRowNumber(HttpServletRequest request) {
        boolean validRowNumber = FormatCheckUtil.checkNumber(request.getParameter("rows"));
        int rowNumber = (validRowNumber) ? Integer.parseInt(request.getParameter("rows")) : 10;
        return rowNumber;
    }

    private String settleAmountFormatter(int rowIdx, Object firstValue, Object secondValue) throws Exception {
        String value = "";
        if (rowIdx == 0) {
            value = this.toKRW(Integer.parseInt(String.valueOf(secondValue)));
        } else {
            int diffValue = this.getDiffValue(Integer.parseInt(String.valueOf(firstValue)),
                    Integer.parseInt(String.valueOf(secondValue)));
            String percentage = this.getPercentage(diffValue, Integer.parseInt(String.valueOf(secondValue)));
            value = this.toKRW(Integer.parseInt(String.valueOf(secondValue))) + "\r\n" + this.toKRW(diffValue) + " ("
                    + percentage + ")";
        }
        return value;
    }

    private int getDiffValue(int firstValue, int secondValue) {
        int diffValue = Integer.parseInt(String.valueOf(firstValue)) - Integer.parseInt(String.valueOf(secondValue));
        return diffValue;
    }

    private String getPercentage(int firstValue, int secondValue) {
        if (secondValue == 0) {
            return 0 + "%";
        }
        int percentage = Math.round((firstValue / secondValue) * 100);
        return percentage + "%";
    }

    private String toKRW(int amount) {
        return String.format("\\%,d", amount);
    }

    public class reservateInfo {
        private String date;

        private int prodSeq;

        private String prodNm;

        private int putCnt;

        public reservateInfo(String date, int prodSeq, String prodNm, int putCnt) {
            this.date = date;
            this.prodSeq = prodSeq;
            this.prodNm = prodNm;
            this.putCnt = 0;
        }

        public String getDate() {
            return date;
        }

        public int getProdSeq() {
            return prodSeq;
        }

        public String getProdNm() {
            return prodNm;
        }

        public int getPutCnt() {
            return putCnt;
        }

        public void setPutCnt(int putCnt) {
            this.putCnt = putCnt;
        }

    }

    private void downloadExcelCustom(HttpServletRequest request, HttpServletResponse response, String fileName,
            String sheetName, String[] columnTitle, String[][] content, Locale locale, String[][] groupTitle,
            String[][] groupVal, String sellWidth) throws Exception {
        XSSFWorkbook workBook = new XSSFWorkbook();

        // Cell 스타일 생성
        XSSFFont titelFontStyle = workBook.createFont();
        titelFontStyle.setFontHeight(11);
        titelFontStyle.setColor(HSSFColor.BLACK.index);
        titelFontStyle.setFontName("맑은 고딕");

        // 기본 스타일
        CellStyle defaultCellStyle = workBook.createCellStyle();
        defaultCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        defaultCellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        defaultCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setWrapText(true);// 자동줄바꿈
        defaultCellStyle.setFont(titelFontStyle);

        XSSFFont contsFontStyle = workBook.createFont();
        contsFontStyle.setFontHeight(11);
        contsFontStyle.setColor(HSSFColor.BLACK.index);
        contsFontStyle.setFontName("맑은 고딕");
        contsFontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 폰트굵게

        // 왼쪽상단타이틀 스타일
        CellStyle leftTopTitle = workBook.createCellStyle();
        leftTopTitle.cloneStyleFrom(defaultCellStyle);
        leftTopTitle.setFont(titelFontStyle);
        leftTopTitle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        leftTopTitle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        // 콘텐츠 상단타이틀 스타일
        CellStyle contentsTitle = workBook.createCellStyle();
        contentsTitle.cloneStyleFrom(defaultCellStyle);
        contentsTitle.setFont(titelFontStyle);
        contentsTitle.setFillForegroundColor(IndexedColors.PALE_BLUE.index);
        contentsTitle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        CellStyle titleCellStyle = workBook.createCellStyle();
        titleCellStyle.cloneStyleFrom(defaultCellStyle);
        titleCellStyle.setFont(contsFontStyle);
        titleCellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.index);
        titleCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        XSSFDataFormat format = workBook.createDataFormat();
        CellStyle numberic = workBook.createCellStyle();
        numberic.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        numberic.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        numberic.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        numberic.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        numberic.setBorderRight(HSSFCellStyle.BORDER_THIN);
        numberic.setBorderTop(HSSFCellStyle.BORDER_THIN);
        numberic.setWrapText(true);// 자동줄바꿈
        numberic.setFont(titelFontStyle);
        numberic.setDataFormat(format.getFormat("0"));

        // 타이틀 스타일
        CellStyle basTitleCellStyle = workBook.createCellStyle();
        basTitleCellStyle.cloneStyleFrom(defaultCellStyle);
        basTitleCellStyle.setFont(titelFontStyle);
        basTitleCellStyle.setFillForegroundColor(IndexedColors.TAN.index);
        basTitleCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        String type = request.getParameter("type");

        Sheet sheet = null;
        if (sheetName != null) {
            sheet = workBook.createSheet(sheetName);
        } else {
            sheet = workBook.createSheet(fileName);
        }

        boolean hasGroupTitle = (groupTitle != null);
        int baseIndex = hasGroupTitle ? groupTitle.length : 0;
        sheet.setDefaultRowHeight((short) 400);

        // 공통
        if (hasGroupTitle) {
            for (int i = 0; i < groupTitle.length; i++) {
                String title = groupTitle[i][0];
                String[] range = groupTitle[i][1].split(",");
                int startRowMergeIndex = Integer.parseInt(range[0]);
                int endRowMergeIndex = Integer.parseInt(range[1]);
                int startColumnMergeIndex = Integer.parseInt(range[2]);
                int endColumnMergeIndex = Integer.parseInt(range[3]);

                sheet.addMergedRegion(new CellRangeAddress(startRowMergeIndex, endRowMergeIndex, startColumnMergeIndex,
                        endColumnMergeIndex));

                Row groupRow = sheet.getRow(startRowMergeIndex);
                Row groupRow2 = sheet.getRow(endRowMergeIndex);
                if (groupRow == null) {
                    groupRow = sheet.createRow(startRowMergeIndex);
                }
                if (groupRow2 == null && startRowMergeIndex != endRowMergeIndex) {
                    sheet.createRow(endRowMergeIndex);
                }

                if (baseIndex <= endRowMergeIndex) {
                    baseIndex = endRowMergeIndex + 1;
                }

                CellStyle setStyle = null;
                if (range.length > 4) {
                    switch (range[4]) {
                        case "type1":
                            setStyle = defaultCellStyle;
                            break;
                        case "type2":
                            setStyle = contentsTitle;
                            break;
                        case "type3":
                            setStyle = leftTopTitle;
                            break;
                        case "type4":
                            setStyle = titleCellStyle;
                            break;
                    }
                } else {
                    setStyle = basTitleCellStyle;
                }

                Cell startCell = groupRow.createCell(startColumnMergeIndex);
                startCell.setCellStyle(setStyle);
                startCell.setCellValue(title);

                Cell endCell = groupRow.createCell(endColumnMergeIndex);
                endCell.setCellStyle(setStyle);
                endCell.setCellValue(title);
            }
        }

        if (groupVal != null) {
            for (int i = 0; i < groupVal.length; i++) {
                Row groupRow = sheet.getRow(Integer.parseInt(groupVal[i][1].split(",")[0]));

                if (groupRow == null) {
                    groupRow = sheet.createRow(Integer.parseInt(groupVal[i][1].split(",")[0]));
                }

                Cell cell = groupRow.getCell(Integer.parseInt(groupVal[i][1].split(",")[1]));

                if (cell == null) {
                    cell = groupRow.createCell(Integer.parseInt(groupVal[i][1].split(",")[1]));
                }

                cell.setCellValue(groupVal[i][0]);

                switch (groupVal[i][2]) {
                    case "type1":
                        cell.setCellStyle(defaultCellStyle);
                        break;
                    case "type2":
                        cell.setCellStyle(contentsTitle);
                        break;
                    case "type3":
                        cell.setCellStyle(leftTopTitle);
                        break;
                    case "type4":
                        cell.setCellStyle(titleCellStyle);
                        break;
                }

            }
        }

        Row headerRow = sheet.createRow(baseIndex);
        int baseCntIdx = 0;
        if (columnTitle != null) {
            for (int i = 0; i < columnTitle.length; i++) {
                Cell cell = headerRow.createCell(i);

                if (groupVal != null) {
                    cell.setCellStyle(defaultCellStyle);
                } else {
                    cell.setCellStyle(basTitleCellStyle);
                }

                cell.setCellValue(columnTitle[i]);
            }
            baseCntIdx = 1;
        } else {
            baseCntIdx = 0;
        }

        for (int i = 0; i < content.length; i++) {
            int rowNum = i + baseIndex + baseCntIdx;

            Row row = sheet.createRow(rowNum);
            row.setHeight((short) 330);
            for (int j = 0; j < content[i].length; j++) {
                Cell cell = row.createCell(j);

                if (i > 0 && "launcherTraffic".equals(type)) {
                    cell.setCellStyle(numberic);
                } else {
                    cell.setCellStyle(defaultCellStyle);
                }
                row.setHeight((short) 400);
                cell.setCellValue(content[i][j]);
            }
        }

        for (int i = 0; i < content[0].length; i++) {
            sheet.autoSizeColumn(i);
            int cellWidth = sheet.getColumnWidth(i);
            cellWidth += 1200;
            if (sellWidth.equals("auto")) {
                sheet.setColumnWidth(i, (short) cellWidth);
            } else {
                String setType = sellWidth.split(",")[i];
                if (setType.equals("auto")) {
                    sheet.setColumnWidth(i, (short) cellWidth);
                } else {

                    if (Integer.parseInt(setType) > cellWidth) {
                        sheet.setColumnWidth(i, (short) Integer.parseInt(setType));
                    } else {
                        sheet.setColumnWidth(i, (short) cellWidth);
                    }
                }
            }

        }

        // 엑셀 파일 다운로드
        response.setContentType("Application/octet-stream");
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
            response.setHeader("Content-Disposition",
                    "filename=" + (URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("MSIE") > -1) { // MS IE (보통은 6.x 이상 가정)
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("Trident") > -1 || userAgent.indexOf("Edge") > -1) { // MS IE 11, Edge
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (java.net.URLEncoder.encode(fileName, "UTF-8") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else { // 모질라나 오페라
            response.setHeader("Content-Disposition", "attachment; filename="
                    + (new String(fileName.getBytes("UTF-8"), "latin1") + ".xlsx").replaceAll("\\+", "\\ ") + ";");
        }

        OutputStream os = response.getOutputStream();
        workBook.write(os);
        os.flush();
        os.close();

    }

    class AscendingString implements Comparator<String> {
        @Override
        public int compare(String a, String b) {
            return b.compareTo(a);
        }
    }

}
