/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.stats.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * 통계 관리 기본 VO 클래스 
 *
 * @author A2TEC
 * @since 2018.07.17
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 7. 17.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class DefaultStatsVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = 169667865440292171L;

    /** 검색 대상 */
    private String target;

    /** 검색어 */
    private String keyword;

    /** 시작 일시 */
    private String startDate;

    /** 종료 일시 */
    private String endDate;

    /** 게시물 조회 페이지 */
    private int offset;

    /** 게시물 조회 개수 */
    private int limit;

    /** 정렬 필드 */
    private String sidx;

    /** 정렬 값(desc, asc) */
    private String sord;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
