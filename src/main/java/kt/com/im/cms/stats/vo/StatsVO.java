/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.stats.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 상품(이용권)에 대한 데이터 객체
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 16.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class StatsVO extends DefaultStatsVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4946448538967006574L;

    /** 결제 번호 */
    private int settlSeq;

    /** 점포 코드 */
    private String storCode;

    /* 결제 유형별 가격 */
    private int payAmount;

    /* 서비스 앱 번호 */
    private int appSeq;

    /** POS 아이디 */
    private String posId;

    /** 영수증 no */
    private String reptNo;

    /** 콘텐츠 번호 */
    private int contsSeq;

    /** 결제 일시 */
    private String settlDt;

    /** 할인 금액 */
    private int dscntPrc;

    /** 추가 시간 */
    private int apdTime;

    /** 결제사 (카드) */
    private String crdtCardCmpny;

    /** 결제 유형 */
    private int settlType;

    /* 태그 발급 순서 번호 */
    private int sleNo;

    /** 결제 취소 POS 아이디 */
    private String canclPosId;

    /** 결제 취소 영수증 no */
    private String canclReptNo;

    /** 객층 그룹 코드 (성별연령대) */
    private String cstmrGrupCode;

    /** 판매자 아이디 */
    private String selerId;

    /** 총 결제 금액 */
    private int totSettlAmt;

    /** 결제 방법 */
    private String settlMthd;

    /** 신용카드 번호 */
    private String crdtCardNo;

    /** 신용카드 사 */
    private String crdtCardCompny;

    /** 태그 아이디 */
    private String tagId;

    /** GSR 상품 코드 */
    private String gsrProdCode;

    /** 할인 가격 */
    private int discount;

    /** 상품 고유 번호 */
    private int prodSeq;

    /** 상품 명 */
    private String prodNm;

    /** 상품 가격 */
    private int prodPrc;

    /** 태그 번호 */
    private int tagSeq;

    /* 결제 태그 정보 리스트 */
    List<StatsVO> tagInfo;

    /** 결제 가격 */
    private int settlAmt;

    /** 결제 상세 번호 */
    private int settlDtlSeq;

    /** 통계 조회 타입 */
    private String dateType;

    /** 매장 고유 번호 */
    private int storSeq;

    /** 매장 명 */
    private String storNm;

    /** 통계 정보 제목 */
    private String title;

    /** 통계 정보 일시 */
    private String data;

    /** 통계 정보 개수 */
    private int count;

    /** 서비스 고유 번호 */
    private int svcSeq;

    /** 서비스 명 */
    private String svcNm;

    /** CP 고유 번호 */
    private int cpSeq;

    /** CP사 명 */
    private String cpNm;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 콘텐츠 서브타이틀 */
    private String contsSubTitle;

    /** 콘텐츠 클릭 횟수 */
    private int contsClickCnt;

    /** 콘텐츠 스트리밍 횟수 */
    private int contsStmCnt;

    /** 콘텐츠 재생 누적 시간 */
    private String contsPlayTime;

    /** 콘텐츠 다운로드 횟수 */
    private int contsDlCnt;

    /** App 구분 */
    private String appType;

    /** 실행 구분 */
    private String excuteType;

    /** 콘텐츠 아이디 */
    private String contsId;

    /** 콘텐츠 카테고리 */
    private String firstCtgNm;

    /** 콘텐츠 장르 */
    private String genreNm;

    /** 콘텐트 재생 횟수 */
    private int contsPlayCnt;

    public int getSettlSeq() {
        return settlSeq;
    }

    public void setSettlSeq(int settlSeq) {
        this.settlSeq = settlSeq;
    }

    public String getStorCode() {
        return storCode;
    }

    public void setStorCode(String storCode) {
        this.storCode = storCode;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getReptNo() {
        return reptNo;
    }

    public void setReptNo(String reptNo) {
        this.reptNo = reptNo;
    }

    public String getSettlDt() {
        return settlDt;
    }

    public void setSettlDt(String settlDt) {
        this.settlDt = settlDt;
    }

    public int getSettlType() {
        return settlType;
    }

    public void setSettlType(int settlType) {
        this.settlType = settlType;
    }

    public String getCanclPosId() {
        return canclPosId;
    }

    public void setCanclPosId(String canclPosId) {
        this.canclPosId = canclPosId;
    }

    public String getCanclReptNo() {
        return canclReptNo;
    }

    public void setCanclReptNo(String canclReptNo) {
        this.canclReptNo = canclReptNo;
    }

    public String getCstmrGrupCode() {
        return cstmrGrupCode;
    }

    public void setCstmrGrupCode(String cstmrGrupCode) {
        this.cstmrGrupCode = cstmrGrupCode;
    }

    public String getSelerId() {
        return selerId;
    }

    public void setSelerId(String selerId) {
        this.selerId = selerId;
    }

    public int getTotSettlAmt() {
        return totSettlAmt;
    }

    public void setTotSettlAmt(int totSettlAmt) {
        this.totSettlAmt = totSettlAmt;
    }

    public String getSettlMthd() {
        return settlMthd;
    }

    public void setSettlMthd(String settlMthd) {
        this.settlMthd = settlMthd;
    }

    public String getCrdtCardNo() {
        return crdtCardNo;
    }

    public void setCrdtCardNo(String crdtCardNo) {
        this.crdtCardNo = crdtCardNo;
    }

    public String getCrdtCardCompny() {
        return crdtCardCompny;
    }

    public void setCrdtCardCompny(String crdtCardCompny) {
        this.crdtCardCompny = crdtCardCompny;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getGsrProdCode() {
        return gsrProdCode;
    }

    public void setGsrProdCode(String gsrProdCode) {
        this.gsrProdCode = gsrProdCode;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getProdSeq() {
        return prodSeq;
    }

    public void setProdSeq(int prodSeq) {
        this.prodSeq = prodSeq;
    }

    public String getProdNm() {
        return prodNm;
    }

    public void setProdNm(String prodNm) {
        this.prodNm = prodNm;
    }

    public int getProdPrc() {
        return prodPrc;
    }

    public void setProdPrc(int prodPrc) {
        this.prodPrc = prodPrc;
    }

    public int getTagSeq() {
        return tagSeq;
    }

    public void setTagSeq(int tagSeq) {
        this.tagSeq = tagSeq;
    }

    public int getSettlAmt() {
        return settlAmt;
    }

    public void setSettlAmt(int settlAmt) {
        this.settlAmt = settlAmt;
    }

    public int getSettlDtlSeq() {
        return settlDtlSeq;
    }

    public void setSettlDtlSeq(int settlDtlSeq) {
        this.settlDtlSeq = settlDtlSeq;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public int getCpSeq() {
        return cpSeq;
    }

    public void setCpSeq(int cpSeq) {
        this.cpSeq = cpSeq;
    }

    public String getCpNm() {
        return cpNm;
    }

    public void setCpNm(String cpNm) {
        this.cpNm = cpNm;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public List<StatsVO> getTagInfo() {
        return tagInfo;
    }

    public void setTagInfo(List<StatsVO> tagInfo) {
        this.tagInfo = tagInfo;
    }

    public String getCrdtCardCmpny() {
        return crdtCardCmpny;
    }

    public void setCrdtCardCmpny(String crdtCardCmpny) {
        this.crdtCardCmpny = crdtCardCmpny;
    }

    public int getDscntPrc() {
        return dscntPrc;
    }

    public void setDscntPrc(int dscntPrc) {
        this.dscntPrc = dscntPrc;
    }

    public int getSleNo() {
        return sleNo;
    }

    public void setSleNo(int sleNo) {
        this.sleNo = sleNo;
    }

    public int getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(int payAmount) {
        this.payAmount = payAmount;
    }

    public int getApdTime() {
        return apdTime;
    }

    public void setApdTime(int apdTime) {
        this.apdTime = apdTime;
    }

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public String getContsSubTitle() {
        return contsSubTitle;
    }

    public void setContsSubTitle(String contsSubTitle) {
        this.contsSubTitle = contsSubTitle;
    }

    public int getContsClickCnt() {
        return contsClickCnt;
    }

    public void setContsClickCnt(int contsClickCnt) {
        this.contsClickCnt = contsClickCnt;
    }

    public int getContsStmCnt() {
        return contsStmCnt;
    }

    public void setContsStmCnt(int contsStmCnt) {
        this.contsStmCnt = contsStmCnt;
    }

    public String getContsPlayTime() {
        return contsPlayTime;
    }

    public void setContsPlayTime(String contsPlayTime) {
        this.contsPlayTime = contsPlayTime;
    }

    public int getContsDlCnt() {
        return contsDlCnt;
    }

    public void setContsDlCnt(int contsDlCnt) {
        this.contsDlCnt = contsDlCnt;
    }

    public int getAppSeq() {
        return appSeq;
    }

    public void setAppSeq(int appSeq) {
        this.appSeq = appSeq;
    }

	public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getExcuteType() {
        return excuteType;
    }

    public void setExcuteType(String excuteType) {
        this.excuteType = excuteType;
    }

    public String getContsId() {
        return contsId;
    }

    public void setContsId(String contsId) {
        this.contsId = contsId;
    }

    public String getFirstCtgNm() {
        return firstCtgNm;
    }

    public void setFirstCtgNm(String firstCtgNm) {
        this.firstCtgNm = firstCtgNm;
    }

    public String getGenreNm() {
        return genreNm;
    }

    public void setGenreNm(String genreNm) {
        this.genreNm = genreNm;
    }

    public int getContsPlayCnt() {
        return contsPlayCnt;
    }

    public void setContsPlayCnt(int contsPlayCnt) {
        this.contsPlayCnt = contsPlayCnt;
    }

}
