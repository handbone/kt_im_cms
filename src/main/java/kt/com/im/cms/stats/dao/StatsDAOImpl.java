/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.stats.dao;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.stats.vo.StatsVO;

/**
 *
 * 결제 관련 및 통계 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 28.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("StatsDAO")
@Transactional
public class StatsDAOImpl extends mysqlAbstractMapper implements StatsDAO {

    /**
     * 결제 리스트 정보 조회
     *
     * @param StatsVO
     * @return 검색조건에 해당되는 결제 정보 리스트 갯수(이전 동일 결제 정보가 있는지 판단하기 위함)
     */
    @Override
    public int searchSettlList(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.searchSettlList", vo);
    }

    /**
     * 결제 정보 생성
     *
     * @param StatsVO
     * @return result(int)
     */
    @Override
    public int insertSettlInfo(StatsVO vo) throws Exception {
        return insert("StatsDAO.insertSettlInfo", vo);
    }

    /**
     * 결제 상세 정보 생성
     *
     * @param StatsVO
     * @return
     */
    @Override
    public void insertSettlDetail(StatsVO vo) throws Exception {
        insert("StatsDAO.insertSettlDetail", vo);
    }

    /**
     * 이용권 반납을 위한 미사용 이용권 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> searchResetTicket(StatsVO vo) throws Exception {
        return selectList("StatsDAO.searchResetTicket", vo);
    }

    /**
     * 금일 매장 방문 정보 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getTodayVisitor(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectTodayVisitor", vo);
    }

    /**
     * 이용권 구입 고객층 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getVisitor(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectVisitor", vo);
    }

    /**
     * 이용권 별 구입 통계
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getSoldTicket(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectSoldTicket", vo);
    }

    /**
     * 전체 콘텐츠 현황
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getTotalContentsStatus(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectTotalContentsStatus", vo);
    }

    /**
     * 등록 콘텐츠 카테고리별 현황
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getContentsByCategory(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectContentsByCategory", vo);
    }

    /**
     * 인기 콘텐츠 다운로드 현황
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getMostDownloadContents(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectMostDowoloadContents", vo);
    }

    /**
     * 인기 콘텐츠 사용시간 현황
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getMostUseTimeContents(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectMostUseTimeContents", vo);
    }

    /**
     * 인기 콘텐츠 현황
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getMostPopularContents(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectMostPopularContents", vo);
    }

    /**
     * 전시 페이지 유입 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getDisplayWebTrafficOverviewList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectDisplayWebTrafficOverviewList", vo);
    }

    /**
     * 전시 페이지 유입 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getDisplayWebTrafficOverviewListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectDisplayWebTrafficOverviewListTotalCount", vo);
    }

    /**
     * 콘텐츠 등록 현황 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getRegisteredContentsList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectRegisteredContentsList", vo);
    }

    /**
     * 콘텐츠 등록 현황 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getRegisteredContentsListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectRegisteredContentsListTotalCount", vo);
    }

    /**
     * 콘텐츠 사용 현황 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getUsedContentsList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectUsedContentsList", vo);
    }

    /**
     * 콘텐츠 사용 현황 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getUsedContentsListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectUsedContentsListTotalCount", vo);
    }

    /**
     * 콘텐츠 상세 사용 현황 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getUsedContentsDetailListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectUsedContentsDetailListTotalCount", vo);
    }

    /**
     * 콘텐츠 상세 사용 현황 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getUsedContentsDetailList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectUsedContentsDetailList", vo);
    }

    /**
     * CP사 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getContentsByCpList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectContentsByCpList", vo);
    }

    /**
     * CP사 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getContentsByCpListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectContentsByCpListTotalCount", vo);
    }

    /**
     * 검수 현황 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getVerifiedContentsList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectVerifiedContentsList", vo);
    }

    /**
     * 검수 현황 통계 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getVerifiedContentsListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectVerifiedContentsListTotalCount", vo);
    }

    /**
     * 이용객 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getVisitorList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectVisitorList", vo);
    }

    /**
     * 이용객 통계 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getVisitorListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectVisitorListTotalCount", vo);
    }

    /**
     * 이용객 통계 상세 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getVisitorDetailList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectVisitorDetailList", vo);
    }

    /**
     * 이용객 통계 상세 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getVisitorDetailListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectVisitorDetailListTotalCount", vo);
    }

    /**
     * 이용권 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getTagList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectTagList", vo);
    }

    /**
     * 이용권 통계 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getTagListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectTagListTotalCount", vo);
    }

    /**
     * 매장별 기간내 발매된 이용권 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getTagStoreList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectTagStoreList", vo);
    }

    /**
     * 매장별 기간내 발매된 이용권 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getTagStoreProdList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectTagStoreProdList", vo);
    }

    /**
     * 매장별 기간내 발매된 이용권 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getTagStoreListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectTagStoreListTotalCount", vo);
    }

    /**
     * 이용권 발급 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getTagDetailList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectTagDetailList", vo);
    }

    /**
     * 이용권 발급 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getTagDetailListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectTagDetailListTotalCount", vo);
    }

    /**
     * 이용권 사용내역 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getTagHistoryList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectTagHistoryList", vo);
    }

    /**
     * 이용권 사용내역 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getTagHistoryListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectTagHistoryListTotalCount", vo);
    }

    /**
     * 이용권별 이용 콘텐츠 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getContentsByTagList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectContentsByTagList", vo);
    }

    /**
     * 이용권별 이용 콘텐츠 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getContentsByTagListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectContentsByTagListTotalCount", vo);
    }

    /**
     * 이용권별 이용 콘텐츠 상세 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getContentsDetailByTagList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectContentsDetailByTagList", vo);
    }

    /**
     * 이용권별 이용 콘텐츠 상세 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getContentsDetailByTagListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectContentsDetailByTagListTotalCount", vo);
    }

    /**
     * 이용권 또는 상품을 가진 매장 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public StatsVO getStoreFromTag(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectStoreFromTag", vo);
    }

    /**
     * 매장 결제 상세 정보 리스트 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> payDetailLst(StatsVO vo) throws Exception {
        return selectList("StatsDAO.payDetailLst", vo);
    }

    /**
     * 매장 결제 상세 정보 (태그 리스트) 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> payTagList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.payTagList", vo);
    }

    /**
     * 매장 결제 정보 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public StatsVO searchSettlInfo(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.searchSettlInfo", vo);
    }

    /**
     * 이용권 추가 시간 차감/ 상태 업데이트
     *
     * @param StatsVO
     * @return 업데이트 성공 여부
     */
    @Override
    public int subtractApdTime(StatsVO vo) throws Exception {
        int res = update("StatsDAO.subtractApdTime", vo);
        return res;
    }

    /**
     * 기간별 결제 내역 목록 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> selectSettlementManage(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectSettlementManage", vo);
    }

    /**
     * 기간별 결제 내역 목록 수 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int selectSettlementManageTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectSettlementManageTotalCount", vo);
    }

    /**
     * 온라인 서비스 콘텐츠 사용 현황 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getUsedOlsvcContentsStatsList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectUsedOlsvcContentsStatsList", vo);
    }

    /**
     * 온라인 서비스 콘텐츠 사용 현황 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getUsedOlsvcContentsStatsListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectUsedOlsvcContentsStatsListTotalCount", vo);
    }

    /**
     * 온라인 서비스 콘텐츠 사용 현황 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map<Object, Object>> getUsedOlsvcContentsList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectUsedOlsvcContentsList", vo);
    }

    /**
     * 온라인 서비스 콘텐츠 사용 현황 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getUsedOlsvcContentsListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectUsedOlsvcContentsListTotalCount", vo);
    }

    /**
     * 런쳐 유입 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getLauncherTrafficListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectLauncherTrafficListTotalCount", vo);
    }

    /**
     * 런쳐 유입 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getLauncherTrafficList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectLauncherTrafficList", vo);
    }

    /**
     * 런쳐 통계 > 유입 통계 현황 목록 총 개수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getLauncherUsedAppListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectLauncherUsedAppListTotalCount", vo);
    }

    /**
     * 런쳐 통계 > 유입 통계 현황 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getLauncherUsedAppList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectLauncherUsedAppList", vo);
    }

    /**
     * 런쳐 이용 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getLauncherUsedAppDetailList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectLauncherUsedAppDetailList", vo);
    }

    /**
     * 런쳐 이용 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getLauncherUsedAppDetailListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectLauncherUsedAppDetailListTotalCount", vo);
    }

	/**
     * 런처 콘텐츠 재생 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getOlSvcLauncherUsedContsStatsList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectOlSvcLauncherUsedContsStatsList", vo);
    }

    /**
     * 런처 콘텐츠 재생 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getOlSvcLauncherUsedContsStatsListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectOlSvcLauncherUsedContsStatsListTotalCount", vo);
    }

    /**
     * 런처 콘텐츠 재생 상세 통계 목록
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getOlSvcLauncherUsedContsDetailStatsList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectOlSvcLauncherUsedContsDetailStatsList", vo);
    }

    /**
     * 런처 콘텐츠 재생 상세 통계 목록 총 갯수
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public int getOlSvcLauncherUsedContsDetailStatsListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectOlSvcLauncherUsedContsDetailStatsListTotalCount", vo);
    }

}
