/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servics.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * 콘텐츠 관련 페이지 컨트롤러
 *
 * @author 정규현
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 8.   정규현      최초생성
* 2018. 6. 11.  정규현      servieRegist, servieDetail, servieEdit 생성
 *
 *
 *      </pre>
 */
@Component
@Controller
public class ServicsController {
    /**
     * 서비스 리스트 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/service", method = RequestMethod.GET)
    public ModelAndView getServiceList(ModelAndView mv) {
        mv.setViewName("/views/member/service/serviceList");
        return mv;
    }

    /**
     * 서비스 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/service/regist", method = RequestMethod.GET)
    public ModelAndView servieRegist(ModelAndView mv) {
        mv.setViewName("/views/member/service/serviceRegist");
        return mv;
    }

    /**
     * 서비스 상세 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/service/{svcSeq}/edit", method = RequestMethod.GET)
    public ModelAndView servieEdit(ModelAndView mv, @PathVariable(value = "svcSeq") String svcSeq) {
        mv.addObject("svcSeq", svcSeq);
        mv.setViewName("/views/member/service/serviceEdit");
        return mv;
    }

    /**
     * 서비스 수정 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/service/{svcSeq}", method = RequestMethod.GET)
    public ModelAndView servieDetail(ModelAndView mv, @PathVariable(value = "svcSeq") String svcSeq) {
        mv.addObject("svcSeq", svcSeq);
        mv.setViewName("/views/member/service/serviceDetail");
        return mv;
    }
}
