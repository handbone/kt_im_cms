/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servics.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.MemberAbstractMapper;
import kt.com.im.cms.servics.vo.ServicsVO;

/**
 *
 * 서비스 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
* 2018. 6. 11    정규현           serviceInsert, serviceInfo, serviceEdit, serviceDelete 추가
 *
 *      </pre>
 */

@Repository("ServicsDAO")
@Transactional
public class ServicsDAOImpl extends MemberAbstractMapper implements ServicsDAO {

    /**
     * 서비스 리스트 정보
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 서비스 리스트 정보
     */
    @Override
    public List<ServicsVO> serviceList(ServicsVO vo) {
        return selectList("ServicsDAO.serviceList", vo);
    }

    /**
     * Master, CMS 관리자 조건의 서비스 리스트 정보
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 서비스 리스트 정보
     */
    @Override
    public List<ServicsVO> serviceListForManager(ServicsVO vo) {
        return selectList("ServicsDAO.serviceListForManager", vo);
    }

    /**
     * 서비스 관리자 조건의 서비스 리스트 정보
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 서비스 리스트 정보
     */
    @Override
    public List<ServicsVO> serviceListForServiceManager(ServicsVO vo) {
        return selectList("ServicsDAO.serviceListForServiceManager", vo);
    }

    /**
     * 매장 관리자 조건의 서비스 리스트 정보
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 서비스 리스트 정보
     */
    @Override
    public List<ServicsVO> serviceListForStoreManager(ServicsVO vo) {
        return selectList("ServicsDAO.serviceListForStoreManager", vo);
    }

    /**
     * 검색조건에 해당되는 서비스 리스트의 총 개수
     *
     * @param ServicsVO
     * @return 총 서비스 수
     */
    @Override
    public int serviceListTotalCount(ServicsVO seq) {
        int res = (Integer) selectOne("ServicsDAO.serviceListTotalCount", seq);
        return res;
    }

    /**
     * 서비스 등록
     *
     * @param ServicsVO
     * @return 서비스 등록 성공 여부 /
     */
    @Override
    public int serviceInsert(ServicsVO data) {
        int res = insert("ServicsDAO.serviceInsert", data);
        return res;
    }

    /**
     * 서비스 상세 정보
     *
     * @param ServicsVO
     * @return 서비스 상세정보 /
     */
    @Override
    public ServicsVO serviceInfo(ServicsVO data) {
        return selectOne("ServicsDAO.serviceInfo", data);
    }

    /**
     * 서비스 상세 정보 리스트
     *
     * @param ServicsVO
     * @return 서비스 상세정보 리스트/
     */
    @Override
    public List<ServicsVO> serviceInfoList(ServicsVO data) {
        return selectList("ServicsDAO.serviceInfoList", data);
    }

    /**
     * 서비스 수정
     *
     * @param ServicsVO
     * @return 서비스 정보 수정 성공 여부 /
     */
    @Override
    public int serviceEdit(ServicsVO data) {
        int res = update("ServicsDAO.serviceEdit", data);
        return res;
    }

    /**
     * 서비스 삭제
     *
     * @param ServicsVO
     * @return 서비스 정보 삭제 성공 여부 /
     */
    @Override
    public int serviceDelete(ServicsVO data) {
        int res = update("ServicsDAO.serviceDelete", data);
        return res;
    }

    /**
     * 서비스 수정
     *
     * @param ServicsVO
     * @return 서비스 정보 수정 성공 여부 /
     */
    @Override
    public int serviceUpdate(ServicsVO data) {
        int res = update("ServicsDAO.serviceUpdate", data);
        return res;
    }

    /**
     * 서비스 사업자 등록번호 존재 유무
     *
     * @param ServicsVO
     * @return 조회 결과
     */
    @Override
    public int searchServiceInfo(ServicsVO data) {
        return selectOne("ServicsDAO.searchServiceInfo", data);
    }

}
