/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servics.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.servics.service.ServicsService;
import kt.com.im.cms.servics.vo.ServicsVO;

/**
 *
 * 서비스 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
* 2018. 6. 11    정규현           serviceInsert, serviceInfo, serviceUpdate, serviceDelete 추가
 *      </pre>
 */

@Controller
public class ServicsApi {

    @Resource(name = "ServicsService")
    private ServicsService servicsService;

    @Resource(name = "memberTransactionManager")
    private DataSourceTransactionManager transactionManager;

    /**
     * 서비스 관련 처리 API (GET-서비스 목록)
     *
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return modelAndView & API 메세지값 & WebStatus
     *         + GET - 검색조건에 따른 코드 리스트
     */
    @RequestMapping(value = "/api/service")
    public ModelAndView serviceInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        rsModel.setViewName("../resources/api/member/service/serviceProcess");
        ServicsVO item = new ServicsVO();

        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                // Master, CMS 관리자가 아닌 경우 접근 불가
                boolean isAdministrator = this.isAdministrator(userVO);
                if (!isAdministrator) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                String checkString = CommonFnc.requiredChecking(request,
                        "svcNm,telNo,bizno,zipcd,basAddr,useYn");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                item.setSvcSeq(CommonFnc.emptyCheckInt("svcSeq", request));
                item.setSvcNm(request.getParameter("svcNm"));

                boolean isExistSvcNm = (servicsService.searchServiceInfo(item) > 0) ? true : false;
                if (isExistSvcNm) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage("exist svcNm");
                    return rsModel.getModelAndView();
                }

                item.setBizno(request.getParameter("bizno"));

                boolean isExistBizno = (servicsService.searchServiceInfo(item) > 0) ? true : false;
                if (isExistBizno) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage("exist bizno");
                    return rsModel.getModelAndView();
                }

                item.setTelNo(request.getParameter("telNo"));
                item.setZipcd(request.getParameter("zipcd"));
                item.setBasAddr(request.getParameter("basAddr"));
                item.setDtlAddr(request.getParameter("dtlAddr"));
                item.setAmdrId(userVO.getMbrId());
                item.setUseYn(request.getParameter("useYn"));

                String appType = CommonFnc.emptyCheckString("appType", request);
                if (appType.equals("")) {
                    item.setAppType("SA"); // 기본값으로 설정
                } else {
                    item.setAppType(appType);
                }

                if (item.getUseYn().equals("Y")) {
                    item.setDelYn("N");
                } else if (item.getUseYn().equals("N")) {
                    item.setDelYn("Y");
                }

                item.setSvcType(request.getParameter("svcType"));

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    int result = servicsService.serviceUpdate(item);
                    if (result == 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        transactionManager.commit(status);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setResultType("serviceUpdate");
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            } else if (Method.equals("DELETE")) { // delete
                // Master, CMS 관리자가 아닌 경우 접근 불가
                boolean isAdministrator = this.isAdministrator(userVO);
                if (!isAdministrator) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                String checkString = CommonFnc.requiredChecking(request, "svcSeq,delYn");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                String delYn = CommonFnc.emptyCheckString("delYn", request);
                String useYn = CommonFnc.emptyCheckString("useYn", request);

                item.setSvcSeq(svcSeq);
                item.setDelYn(delYn);
                item.setUseYn(useYn);

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    int result = servicsService.serviceDelete(item);
                    if (result == 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        transactionManager.commit(status);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            } else { // post
                // Master, CMS 관리자가 아닌 경우 접근 불가
                boolean isAdministrator = this.isAdministrator(userVO);
                if (!isAdministrator) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                String checkString = CommonFnc.requiredChecking(request, "svcNm,telNo,bizno,zipcd,basAddr,svcType");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                item.setSvcNm(request.getParameter("svcNm"));

                boolean isExistSvcNm = (servicsService.searchServiceInfo(item) > 0) ? true : false;
                if (isExistSvcNm) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage("exist svcNm");
                    return rsModel.getModelAndView();
                }

                item.setBizno(request.getParameter("bizno"));

                boolean isExistBizno = (servicsService.searchServiceInfo(item) > 0) ? true : false;
                if (isExistBizno) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage("exist bizno");
                    return rsModel.getModelAndView();
                }

                item.setTelNo(request.getParameter("telNo"));
                item.setZipcd(request.getParameter("zipcd"));
                item.setBasAddr(request.getParameter("basAddr"));
                item.setDtlAddr(request.getParameter("dtlAddr"));
                item.setSvcType(request.getParameter("svcType"));
                item.setCretrId(userVO.getMbrId());

                String appType = CommonFnc.emptyCheckString("appType", request);
                if (appType.equals("")) {
                    item.setAppType("SA");; // 기본값으로 설정
                } else {
                    item.setAppType(appType);
                }

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    int result = servicsService.serviceInsert(item);
                    if (result == 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setResultType("serviceInsert");
                        transactionManager.commit(status);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        transactionManager.rollback(status);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            }
        } else { // get
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            if (svcSeq != 0) {
                // Master, CMS 관리자가 아닌 경우 접근 불가
                boolean isAdministrator = this.isAdministrator(userVO);
                if (!isAdministrator) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                item.setSvcSeq(svcSeq);
                ServicsVO resultItem = servicsService.serviceInfo(item);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 상세정보 요청일 경우 개인 정보 마스킹 처리 (전화번호, 주소, 사업자등록번호, 등록자 아이디, 수정자 아이디) */
                    String getType = CommonFnc.emptyCheckString("getType", request);
                    if (getType.equals("")) {
                        resultItem.setTelNo(CommonFnc.getTelnoMask(resultItem.getTelNo()));
                        resultItem.setBasAddr(CommonFnc.getAddrMask(resultItem.getBasAddr()));
                        resultItem.setDtlAddr("");
                        resultItem.setBizno(CommonFnc.getBiznoMask(resultItem.getBizno()));
                        resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                        resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }
                rsModel.setResultType("serviceInfo");
                return rsModel.getModelAndView();
            } else {
                // CP사 관계자는 서비스 목록 조회 불가
                boolean isNotAllowMember = this.isNotAllowMember(userVO);
                if (isNotAllowMember) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (svcSeq == 0 && CommonFnc.checkReqParameter(request, "svcSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                String searchType = request.getParameter("searchType") == null ? ""
                        : request.getParameter("searchType");

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    if (searchField == "CRETR_ID" || searchField.equals("CRETR_ID")) {
                        searchField = "A." + searchField;
                    }

                    item.setTarget(searchField);
                    item.setKeyword(searchString);

                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (!sidx.equals("SVC_NM") && !sidx.equals("TEL_NO") && !sidx.equals("CTG_NM")
                        && !sidx.equals("BAS_ADDR") && !sidx.equals("USE_YN") && !sidx.equals("CRET_DT")
                        && !sidx.equals("SVC_SEQ") && !sidx.equals("MIRR_YN")) {
                    sidx = "CRET_DT";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setoffset(page);
                item.setlimit(pageEnd);

                if (searchType.equals("list")) {
                    List<ServicsVO> resultItem;
                    String mbrSe = userVO.getMbrSe();
                    if (mbrSe.equals("01") || mbrSe.equals("02") || mbrSe.equals("03")) {
                        // 01 : Master 관리자, 02 : CMS 관리자, 03: 검수자
                        resultItem = servicsService.serviceListForManager(item);
                    } else if (mbrSe.equals("04")) {
                        // 04 : 서비스 관리자
                        item.setLoginId(session.getAttribute("id").toString());
                        resultItem = servicsService.serviceListForServiceManager(item);
                    } else {
                        // mbrSe 05인 경우 - 매장 관리자
                        item.setLoginId(session.getAttribute("id").toString());
                        resultItem = servicsService.serviceListForStoreManager(item);
                    }

                    if (resultItem.isEmpty()) {
                        rsModel.setData("resultCode", "1010");
                        rsModel.setData("resultMsg", "No data");
                    } else {
                        rsModel.setData("resultCode", "1000");
                        rsModel.setData("resultMsg", "Success");
                        rsModel.setData("item", resultItem);
                    }
                    rsModel.setResultType("serviceNmList");
                } else {
                    List<ServicsVO> resultItem = servicsService.serviceList(item);

                    if (resultItem.isEmpty()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        /* 개인 정보 마스킹 (전화번호, 주소) */
                        for (int i = 0; i < resultItem.size(); i++) {
                            resultItem.get(i).setTelNo(CommonFnc.getTelnoMask(resultItem.get(i).getTelNo()));
                            resultItem.get(i).setBasAddr(CommonFnc.getAddrMask(resultItem.get(i).getBasAddr()));
                        }

                        int totalCount = servicsService.serviceListTotalCount(item);
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("totalCount", totalCount);
                        rsModel.setData("currentPage", currentPage);
                        rsModel.setData("totalPage", (totalCount - 1) / limit);
                        rsModel.setResultType("serviceList");
                    }
                }
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    private boolean isAdminacceptor(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe);
    }

    private boolean isNotAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

}
