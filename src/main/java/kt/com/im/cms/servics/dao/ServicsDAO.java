/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.servics.dao;

import java.util.List;

import kt.com.im.cms.servics.vo.ServicsVO;

/**
 *
 * 서비스 관련 데이터 접근 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
* 2018. 6. 11    정규현           serviceInsert, serviceInfo, serviceEdit, serviceDelete 추가
 *
 *      </pre>
 */

public interface ServicsDAO {

    List<ServicsVO> serviceList(ServicsVO vo);

    List<ServicsVO> serviceListForManager(ServicsVO vo);

    List<ServicsVO> serviceListForServiceManager(ServicsVO vo);

    List<ServicsVO> serviceListForStoreManager(ServicsVO vo);

    int serviceListTotalCount(ServicsVO data);

    int serviceInsert(ServicsVO data);

    ServicsVO serviceInfo(ServicsVO data);

    int serviceEdit(ServicsVO data);

    int serviceDelete(ServicsVO data);

    int serviceUpdate(ServicsVO data);

    int searchServiceInfo(ServicsVO data);

    List<ServicsVO> serviceInfoList(ServicsVO data);

}
