/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servics.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.servics.dao.ServicsDAO;
import kt.com.im.cms.servics.vo.ServicsVO;

/**
 *
 * 서비스 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
* 2018. 6. 11    정규현           serviceInsert, serviceInfo, serviceUpdate, serviceDelete 추가
 *
 *      </pre>
 */

@Service("ServicsService")
public class ServicsServiceImpl implements ServicsService {
    @Resource(name = "ServicsDAO")
    private ServicsDAO servicsDAO;

    /**
     * 검색조건에 해당되는 서비스 리스트 정보
     *
     * @param StoreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ServicsVO> serviceList(ServicsVO vo) {
        return servicsDAO.serviceList(vo);
    }

    /**
     * Master 관리자, CMS 관리자 조건에 해당되는 서비스 리스트 정보
     *
     * @param StoreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ServicsVO> serviceListForManager(ServicsVO vo) {
        return servicsDAO.serviceListForManager(vo);
    }

    /**
     * 서비스 관리자 조건에 해당되는 서비스 리스트 정보
     *
     * @param StoreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ServicsVO> serviceListForServiceManager(ServicsVO vo) {
        return servicsDAO.serviceListForServiceManager(vo);
    }

    /**
     * 매장 관리자 조건에 해당되는 서비스 리스트 정보
     *
     * @param StoreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ServicsVO> serviceListForStoreManager(ServicsVO vo) {
        return servicsDAO.serviceListForStoreManager(vo);
    }

    /**
     * 검색조건에 해당되는 서비스 리스트의 총 개수
     *
     * @param ServicsVO
     * @return 총 콘텐츠 수 /
     */
    @Override
    public int serviceListTotalCount(ServicsVO data) {
        int res = servicsDAO.serviceListTotalCount(data);
        return res;
    }

    /**
     * 서비스 등록
     *
     * @param ServicsVO
     * @return 서비스 등록 성공 여부 /
     */
    @Override
    public int serviceInsert(ServicsVO data) {
        int res = servicsDAO.serviceInsert(data);
        return res;
    }

    /**
     * 서비스 상세 정보
     *
     * @param ServicsVO
     * @return 서비스 상세정보 /
     */
    @Override
    public ServicsVO serviceInfo(ServicsVO data) {
        return servicsDAO.serviceInfo(data);
    }

    /**
     * 서비스 상세 정보 리스트
     *
     * @param ServicsVO
     * @return 서비스 상세정보 리스트
     */
    @Override
    public List<ServicsVO> serviceInfoList(ServicsVO data) {
        return servicsDAO.serviceInfoList(data);
    }

    /**
     * 서비스 수정
     *
     * @param ServicsVO
     * @return 서비스 정보 수정 성공 여부 /
     */
    @Override
    public int serviceEdit(ServicsVO data) {
        return servicsDAO.serviceEdit(data);
    }

    /**
     * 서비스 삭제
     *
     * @param ServicsVO
     * @return 서비스 정보 삭제 성공 여부 /
     */
    @Override
    public int serviceDelete(ServicsVO data) {
        int res = servicsDAO.serviceDelete(data);
        return res;
    }

    /**
     * 서비스 수정
     *
     * @param ServicsVO
     * @return 서비스 정보 수정 성공 여부 /
     */
    @Override
    public int serviceUpdate(ServicsVO data) {
        int res = servicsDAO.serviceUpdate(data);
        return res;
    }

    /**
     * 서비스 사업자 등록번호 존재 유무
     * 
     * @param ServicsVO
     * @return 조회 결과
     */
    @Override
    public int searchServiceInfo(ServicsVO data) {
        int res = servicsDAO.searchServiceInfo(data);
        return res;
    }

}
