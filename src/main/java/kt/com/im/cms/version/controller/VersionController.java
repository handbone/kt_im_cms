/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright Version above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.version.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * Device 버전 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 7. 5.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Component
@Controller
public class VersionController {

    /**
     * Device 버전 리스트 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/version", method = RequestMethod.GET)
    public ModelAndView versionList(ModelAndView mv) {
        mv.setViewName("/views/version/VersionList");
        return mv;
    }

    /**
     * Device 버전 상세 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/version/{verSeq}", method = RequestMethod.GET)
    public ModelAndView versionDetail(ModelAndView mv, @PathVariable(value = "verSeq") String verSeq) {
        mv.addObject("verSeq", verSeq);
        mv.setViewName("/views/version/VersionDetail");
        return mv;
    }

    /**
     * Device 버전 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/version/regist", method = RequestMethod.GET)
    public ModelAndView versionRegist(ModelAndView mv) {
        mv.setViewName("/views/version/VersionRegist");
        return mv;
    }

    /**
     * Device 버전 수정 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/version/{verSeq}/edit", method = RequestMethod.GET)
    public ModelAndView deviceEdit(ModelAndView mv, @PathVariable(value = "verSeq") String verSeq) {
        mv.addObject("verSeq", verSeq);
        mv.setViewName("/views/version/VersionEdit");
        return mv;
    }

}
