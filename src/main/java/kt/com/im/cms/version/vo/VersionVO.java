/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright Version above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.version.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * VersionVO
 *
 * @author A2TEC
 * @since 2018.06.20
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 20.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
public class VersionVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5046948878362537770L;

    /** 생성 일시 */
    private String cretDt;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** 삭제 여부 */
    private String delYn;

    /** 로그인 아이디 */
    private String loginId;

    /** 활성화 여부 */
    private String useYn;

    /** 검색 유무 */
    private String searchConfirm;

    /** 검색 키워드 */
    private String keyword;

    /** 페이지 값 */
    private int offset;

    /** 리스트 개수 */
    private int limit;

    /** 파일 번호 */
    private int fileSeq;

    /** 파일 명 */
    private String orginlFileNm;

    /** 서비스 번호 */
    private int svcSeq;

    /** 서비스 명 */
    private String svcNm;

    /** 리스트 */
    private List list;

    /** 버전 번호 */
    private int verSeq;

    /** 버전 명 */
    private String verNm;

    /** 버전 유형(타입) */
    private String comnCdNm;

    /** 버전 유형 코드(타입) */
    private String comnCdValue;

    /** 버전 타입 */
    private String verType;

    /** 매장 번호 */
    private int storSeq;

    /** 등록자 아이디 */
    private String cretrId;

    /** 업데이트 내역 */
    private String verHst;

    /** 정렬 필드 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 조건(카테고리) */
    private String target;

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAamdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getoffset() {
        return offset;
    }

    public void setoffset(int offset) {
        this.offset = offset;
    }

    public int getlimit() {
        return limit;
    }

    public void setlimit(int limit) {
        this.limit = limit;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public int getVerSeq() {
        return verSeq;
    }

    public void setVerSeq(int verSeq) {
        this.verSeq = verSeq;
    }

    public String getVerNm() {
        return verNm;
    }

    public void setVerNm(String verNm) {
        this.verNm = verNm;
    }

    public String getVerType() {
        return verType;
    }

    public void setVerType(String verType) {
        this.verType = verType;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getVerHst() {
        return verHst;
    }

    public void setVerHst(String verHst) {
        this.verHst = verHst;
    }

    public String getComnCdNm() {
        return comnCdNm;
    }

    public void setComnCdNm(String comnCdNm) {
        this.comnCdNm = comnCdNm;
    }

    public String getComnCdValue() {
        return comnCdValue;
    }

    public void setComnCdValue(String comnCdValue) {
        this.comnCdValue = comnCdValue;
    }

    public int getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(int fileSeq) {
        this.fileSeq = fileSeq;
    }

    public String getOrginlFileNm() {
        return orginlFileNm;
    }

    public void setOrginlFileNm(String orginlFileNm) {
        this.orginlFileNm = orginlFileNm;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

}
