/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright Version above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.version.web;

import java.io.File;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFileUtil;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.file.web.FileApi;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.version.service.VersionService;
import kt.com.im.cms.version.vo.VersionVO;

/**
 *
 * Device 버전 관련 처리 API
 *
 * @author A2TEC
 * @since 2018.06.20
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 20.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class VersionApi {

    @Resource(name = "VersionService")
    private VersionService versionService;

    @Resource(name = "FileService")
    private FileService fileService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    /**
     * Build Facility Process
     */
    @Inject
    private FileSystemResource fsResource;

    // @Resource(name="ClientService")
    // private ClientService clientService;

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.KOREAN);

    Date date = new Date();

    @Autowired
    private DataSourceTransactionManager transactionManager;

    /**
     * Device 버전 등록 및 조회 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/version")
    public ModelAndView VersionProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/version/versionProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        FileVO fileVO = new FileVO();
        VersionVO item = new VersionVO();

        int mbrSvcSeq = 0;
        request.setCharacterEncoding("UTF-8");

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            boolean isManageAllowMember = this.isManageAllowMember(userVO);
            if (!isManageAllowMember) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,svcSeq,verNm,verType,verHst");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            item.setStorSeq(CommonFnc.emptyCheckInt("storSeq", request));
            item.setSvcSeq(CommonFnc.emptyCheckInt("svcSeq", request));
            item.setVerNm(CommonFnc.emptyCheckString("verNm", request));
            item.setVerType(CommonFnc.emptyCheckString("verType", request));
            item.setVerHst(CommonFnc.emptyCheckString("verHst", request));
            item.setCretrId((String) session.getAttribute("id"));

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = versionService.versionInsert(item);

                if (result == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", item);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("versionInsert");
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            boolean isManageAllowMember = this.isManageAllowMember(userVO);
            if (!isManageAllowMember) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,svcSeq,verNm,verType,verHst,verSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            item.setStorSeq(CommonFnc.emptyCheckInt("storSeq", request));
            item.setSvcSeq(CommonFnc.emptyCheckInt("svcSeq", request));
            item.setVerNm(CommonFnc.emptyCheckString("verNm", request));
            item.setVerType(CommonFnc.emptyCheckString("verType", request));
            item.setVerHst(CommonFnc.emptyCheckString("verHst", request));
            //item.setCretrId((String) session.getAttribute("id"));
            item.setAmdrId(userVO.getMbrId());
            item.setVerSeq(CommonFnc.emptyCheckInt("verSeq", request));

            try {
                int result = versionService.versionUpdate(item);
                if (result == 1) {

                    result = 0;
                    List<String> deleteFilePath = null;

                    String dfileSeqs = CommonFnc.emptyCheckString("dfileSeq", request);
                    deleteFilePath = new ArrayList<String>();

                    if (!dfileSeqs.equals("")) {
                        String[] dfileSeq = dfileSeqs.split(",");

                        for (int i = 0; i < dfileSeq.length; i++) {
                            FileVO dfile = new FileVO();
                            dfile.setFileSeq(Integer.parseInt(dfileSeq[i]));
                            fileService.fileSeqDelete(dfile);
                        }
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", item);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            }
        } else if (method.equals("DELETE")) {
            boolean isManageAllowMember = this.isManageAllowMember(userVO);
            if (!isManageAllowMember) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "verSeqs");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                String verSeqs = request.getParameter("verSeqs");
                String[] verSeq = verSeqs.split(",");

                List<Integer> list = new ArrayList<Integer>();
                for (int i = 0; i < verSeq.length; i++) {
                    list.add(Integer.parseInt(verSeq[i]));
                }
                item.setList(list);
                item.setDelYn("Y");
                item.setAmdrId(userVO.getMbrId());
                int result = versionService.versionDelete(item);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }

            rsModel.setResultType("releaseDelete");
            return rsModel.getModelAndView();
        } else { // GET
            String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType");
            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");

            item.setSearchConfirm(searchConfirm);
            // 검색 여부가 true일 경우
            if (searchConfirm.equals("true")) {
                /** 검색 카테고리 */
                String searchField = request.getParameter("searchField") == null ? ""
                        : request.getParameter("searchField");

                /** 검색어 */
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.setTarget(searchField);
                item.setKeyword(searchString);
            }

            /** 현재 페이지 */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));

            /** 검색에 출력될 개수 */
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

            /** 정렬할 필드 */
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

            /** 정렬 방법 */
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
            int page = (currentPage - 1) * limit + 1;

            /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
            int pageEnd = (currentPage - 1) * limit + limit;

            item.setSidx(sidx);
            item.setSord(sord);
            item.setoffset(page);
            item.setlimit(pageEnd);

            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int verSeq = CommonFnc.emptyCheckInt("verSeq", request);

            if (verSeq != 0) {
                item.setVerSeq(verSeq);
                VersionVO resultItem = versionService.versionInfo(item);
                FileVO SelFile = new FileVO();
                SelFile.setFileContsSeq(verSeq);
                SelFile.setFileSe("VERSION");
                List<FileVO> fileResult = fileService.fileList(SelFile);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                            && userVO.getSvcSeq() != resultItem.getSvcSeq())
                            || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)
                                    && userVO.getStorSeq() != resultItem.getStorSeq())) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("file", fileResult);
                }
                rsModel.setResultType("versionInfo");
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (verSeq == 0 && CommonFnc.checkReqParameter(request, "verSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                item.setStorSeq(storSeq);

                List<VersionVO> resultItem = versionService.versionList(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    int totalCount = versionService.versionListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", (totalCount - 1) / limit);
                    rsModel.setResultType("versionList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/versionfile")
    public ModelAndView asd(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        String pathBasic = fsResource.getPath();

        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/version/versionProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isManageAllowMember(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        FileVO file = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
            MultipartFile multipartFile = null;
            List resultList = new ArrayList();

            // 디레토리가 없다면 생성
            File dir = new File(pathBasic);
            if (!dir.isDirectory()) {
                dir.mkdirs();
            }

            while (iterator.hasNext()) {
                int verSeq = CommonFnc.emptyCheckInt("verSeq", request);
                multipartFile = multipartHttpServletRequest.getFile(iterator.next());
                if (multipartFile.isEmpty() == false) {
                    String flieName = multipartFile.getName();
                    String orginlFileNm = new String(multipartFile.getOriginalFilename().getBytes("8859_1"), "UTF-8"); // 한글꺠짐
                    if (!FileApi.isValidFileName(orginlFileNm)) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        return rsModel.getModelAndView();
                    }
                    long filesize = multipartFile.getSize();
                    // System.out.println("------------- file start -------------");
                    // System.out.println("name : "+flieName);
                    // System.out.println("filename : "+origName);
                    // System.out.println("size : "+filesize);
                    // System.out.println("-------------- file end --------------\n");

                    String ext = orginlFileNm.substring(orginlFileNm.lastIndexOf('.')); //
                    String now = new SimpleDateFormat("yyMMddHmsS").format(new Date()); // 현재시간
                    String path = "";
                    if (CommonFileUtil.isAllowUploadImgFileType(ext)) {
                        path += "/photo";
                    } else if (ext.equals("zip")) {
                        path += "/zip";
                    } else {
                        path += "/other";
                    }
                    String saveFileName = now + ext;
                    File serverFile = new File(pathBasic + path + File.separator + saveFileName);

                    multipartFile.transferTo(serverFile);
                    file.setOrginlFileNm(orginlFileNm);
                    file.setFileContsSeq(verSeq);
                    file.setStreFileNm(saveFileName);
                    file.setFileSize(BigInteger.valueOf(filesize));
                    file.setFileDir(path + "/" + saveFileName);
                    file.setFileSe("VERSION");

                    resultList.add(file);

                    DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                    def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                    TransactionStatus status = transactionManager.getTransaction(def);

                    try {
                        int result = fileService.fileInsert(file);
                        if (result == 1) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                            rsModel.setData("item", file);
                            rsModel.setResultType("versionFileInfo");
                        } else {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        }
                        transactionManager.commit(status);
                    } catch (Exception e) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        transactionManager.rollback(status);
                    } finally {
                        if (!status.isCompleted()) {
                            transactionManager.rollback(status);
                        }
                    }
                }
            }
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            int verSeq = CommonFnc.emptyCheckInt("verSeq", request);

            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "verSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            try {
                file.setFileContsSeq(verSeq);
                file.setFileSe("VERSION");

                FileVO resData = fileService.fileInfo(file);
                String filePath = "";
                if (resData != null) {
                    filePath = resData.getFilePath();
                }
                if (fileService.fileDelete(file) == 1) {
                    File delfile = new File(fsResource.getPath() + filePath);
                    delfile.delete();
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            }
        } else { // GET
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();

    }

    private boolean isManageAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe);
    }

    private boolean isNotAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe) || MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

}
