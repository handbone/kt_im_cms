/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright Version above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.version.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.version.vo.VersionVO;

/**
 *
 * Device 버전 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.20
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 20.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("VersionDAO")
public class VersionDAOImpl extends mysqlAbstractMapper implements VersionDAO {

    /**
     * 검색 조건에 맞는 버전 리스트 목록
     *
     * @param VersionVO
     * @return 버전 리스트 목록
     */
    @Override
    public List<VersionVO> VersionList(VersionVO vo) {
        return selectList("VersionDAO.versionList", vo);
    }

    /**
     * 검색 조건에 맞는 버전 리스트 페이징 정보
     *
     * @param VersionVO
     * @return 버전 리스트 페이징 정보
     */
    @Override
    public int versionListTotalCount(VersionVO vo) {
        int res = (Integer) selectOne("VersionDAO.versionListTotalCount", vo);
        return res;
    }

    /**
     * 버전 상세 정보
     *
     * @param VersionVO
     * @return 버전 상세 정보
     */
    @Override
    public VersionVO versionInfo(VersionVO vo) {
        return selectOne("VersionDAO.versionInfo", vo);
    }

    /**
     * 버전 정보 등록
     *
     * @param VersionVO
     * @return 버전 정보 등록 성공 여부
     */
    @Override
    public int versionInsert(VersionVO vo) {
        int res = Integer.valueOf(insert("VersionDAO.versionInsert", vo));
        return res;
    }

    /**
     * 버전 정보 삭제
     *
     * @param VersionVO
     * @return 버전 정보 삭제 성공 여부
     */
    @Override
    public int versionDelete(VersionVO vo) {
        int res = Integer.valueOf(update("VersionDAO.versionDelete", vo));
        return res;
    }

    /**
     * 버전 정보 수정
     *
     * @param VersionVO
     *
     * @return 버전 정보 수정 성공여부
     */
    @Override
    public int versionUpdate(VersionVO vo) {
        int res = Integer.valueOf(update("VersionDAO.versionUpdate", vo));
        return res;
    }

}
