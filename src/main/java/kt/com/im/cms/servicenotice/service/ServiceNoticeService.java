/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicenotice.service;

import java.util.List;

import kt.com.im.cms.servicenotice.vo.ServiceNoticeVO;

/**
 *
 * 서비스 공지사항 관련 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.11.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public interface ServiceNoticeService {

    List<ServiceNoticeVO> svcNoticeList(ServiceNoticeVO vo) throws Exception;

    int svcNoticeListTotalCount(ServiceNoticeVO vo) throws Exception;

    ServiceNoticeVO svcNoticeDetail(ServiceNoticeVO vo) throws Exception;

    void updateSvcNoticeRetvNum(ServiceNoticeVO vo) throws Exception;

    int insertSvcNotice(ServiceNoticeVO vo) throws Exception;

    int updateSvcNotice(ServiceNoticeVO vo) throws Exception;

    int deleteSvcNotice(ServiceNoticeVO vo) throws Exception;

    ServiceNoticeVO fileInfo(ServiceNoticeVO vo) throws Exception;

    int attachFileDelete(ServiceNoticeVO vo) throws Exception;

    int updateSvcNoticeDisplayFinish(ServiceNoticeVO vo) throws Exception;

    ServiceNoticeVO getLatestSvcNoticeDetail(ServiceNoticeVO vo) throws Exception;

    List<ServiceNoticeVO> olSvcNoticeImptList(ServiceNoticeVO vo) throws Exception;

    int olSvcNoticeImptListTotalCount(ServiceNoticeVO vo) throws Exception;

    List<ServiceNoticeVO> olSvcNoticeList(ServiceNoticeVO vo) throws Exception;

    int olSvcNoticeListTotalCount(ServiceNoticeVO vo) throws Exception;

    ServiceNoticeVO olSvcNoticeDetail(ServiceNoticeVO vo) throws Exception;

    ServiceNoticeVO getImptSvcNotice(ServiceNoticeVO vo) throws Exception;

    int updateImptSvcNotice(ServiceNoticeVO vo) throws Exception;

    int insertOlSvcNotice(ServiceNoticeVO vo) throws Exception;

    int updateOlSvcNotice(ServiceNoticeVO vo) throws Exception;

    ServiceNoticeVO latestOlSvcNoticeDetail(ServiceNoticeVO vo) throws Exception;

    int deleteOlSvcNotice(ServiceNoticeVO vo) throws Exception;

}
