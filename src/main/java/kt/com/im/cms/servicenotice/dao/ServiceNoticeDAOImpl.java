/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicenotice.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.servicenotice.vo.ServiceNoticeVO;

/**
 *
 * 서비스 공지사항 관련 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.11.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("ServiceNoticeDAO")
public class ServiceNoticeDAOImpl extends mysqlAbstractMapper implements ServiceNoticeDAO {

    /**
     * 서비스 공지사항 리스트 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 서비스 공지사항 리스트
     */
    @Override
    public List<ServiceNoticeVO> svcNoticeList(ServiceNoticeVO vo) throws Exception {
        return selectList("ServiceNoticeDAO.svcNoticeList", vo);
    }

    /**
     * 서비스 공지사항 리스트 합계 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 서비스 공지사항 리스트 합계
     */
    @Override
    public int svcNoticeListTotalCount(ServiceNoticeVO vo) throws Exception {
         int res = (Integer) selectOne("ServiceNoticeDAO.svcNoticeListTotalCount", vo);
         return res;
    }

    /**
     * 서비스 공지사항 상세 정보 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 서비스 공지사항 상세 정보
     */
    @Override
    public ServiceNoticeVO svcNoticeDetail(ServiceNoticeVO vo) throws Exception {
        return selectOne("ServiceNoticeDAO.svcNoticeDetail", vo);
    }

    /**
     * 서비스 공지사항 조회수 업데이트
     * @param ServiceNoticeVO
     * @return
     */
    @Override
    public void updateSvcNoticeRetvNum(ServiceNoticeVO vo) throws Exception {
        update("ServiceNoticeDAO.updateSvcNoticeRetvNum", vo);
    }

    /**
     * 서비스 공지사항 등록
     * @param ServiceNoticeVO
     * @return 등록 결과
     */
    @Override
    public int insertSvcNotice(ServiceNoticeVO vo) throws Exception {
        return insert("ServiceNoticeDAO.insertSvcNotice", vo);
    }

    /**
     * 서비스 공지사항 수정
     * @param ServiceNoticeVO
     * @return 수정 결과
     */
    @Override
    public int updateSvcNotice(ServiceNoticeVO vo) throws Exception {
        return update("ServiceNoticeDAO.updateSvcNotice", vo);
    }

    /**
     * 서비스 공지사항 삭제
     * @param ServiceNoticeVO
     * @return 삭제 결과
     */
    @Override
    public int deleteSvcNotice(ServiceNoticeVO vo) throws Exception {
        return update("ServiceNoticeDAO.deleteSvcNotice", vo);
    }

    /**
     * 첨부파일 정보
     * @param ServiceNoticeVO
     * @return 첨부파일 정보
     */
    @Override
    public ServiceNoticeVO fileInfo(ServiceNoticeVO vo) throws Exception {
        return selectOne("ServiceNoticeDAO.fileInfo", vo);
    }

    /**
     * 첨부파일 삭제
     * @param ServiceNoticeVO
     * @return 삭제결과
     */
    @Override
    public int attachFileDelete(ServiceNoticeVO vo) throws Exception {
        return delete("ServiceNoticeDAO.attachFileDelete", vo);
    }

    /**
     * 서비스 공지사항 게시 일자 종료에 따른 비활성화
     * @param ServiceNoticeVO
     * @return 업데이트 결과
     */
    @Override
    public int updateSvcNoticeDisplayFinish(ServiceNoticeVO vo) throws Exception {
        return update("ServiceNoticeDAO.updateSvcNoticeDisplayFinish", vo);
    }

    /**
     * 가장 최근의 서비스 공지사항 정보 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 서비스 공지사항 정보
     */
    @Override
    public ServiceNoticeVO getLatestSvcNoticeDetail(ServiceNoticeVO vo) throws Exception {
        return selectOne("ServiceNoticeDAO.getLatestSvcNoticeDetail", vo);
    }

    /**
     * 온라인 서비스 중요 공지사항 리스트 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 중요 공지사항 리스트
     */
    @Override
    public List<ServiceNoticeVO> olSvcNoticeImptList(ServiceNoticeVO vo) throws Exception {
        return selectList("ServiceNoticeDAO.selectOlSvcNoticeImptList", vo);
    }

    /**
     * 온라인 서비스 중요 공지사항 리스트 합계 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 중요 공지사항 리스트 합계
     */
    @Override
    public int olSvcNoticeImptListTotalCount(ServiceNoticeVO vo) throws Exception {
         return selectOne("ServiceNoticeDAO.selectOlSvcNoticeImptListTotalCount", vo);
    }

    /**
     * 온라인 서비스 공지사항 리스트 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 리스트
     */
    @Override
    public List<ServiceNoticeVO> olSvcNoticeList(ServiceNoticeVO vo) throws Exception {
        return selectList("ServiceNoticeDAO.selectOlSvcNoticeList", vo);
    }

    /**
     * 온라인 서비스 공지사항 리스트 합계 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 리스트 합계
     */
    @Override
    public int olSvcNoticeListTotalCount(ServiceNoticeVO vo) throws Exception {
         return selectOne("ServiceNoticeDAO.selectOlSvcNoticeListTotalCount", vo);
    }

    /**
     * 온라인 서비스 공지사항 상세 정보 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 상세 정보
     */
    @Override
    public ServiceNoticeVO olSvcNoticeDetail(ServiceNoticeVO vo) throws Exception {
        return selectOne("ServiceNoticeDAO.selectOlSvcNoticeDetail", vo);
    }

    /**
     * 중요 서비스 공지사항 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 정보
     */
    @Override
    public ServiceNoticeVO getImptSvcNotice(ServiceNoticeVO vo) throws Exception {
        return selectOne("ServiceNoticeDAO.selectImptSvcNotice", vo);
    }

    /**
     * 온라인 서비스 공지사항 중요도 수정
     * @param ServiceNoticeVO
     * @return 수정 결과
     */
    @Override
    public int updateImptSvcNotice(ServiceNoticeVO vo) throws Exception {
        return update("ServiceNoticeDAO.updateImptSvcNotice", vo);
    }

    /**
     * 온라인 서비스 공지사항 등록
     * @param ServiceNoticeVO
     * @return 등록 결과
     */
    @Override
    public int insertOlSvcNotice(ServiceNoticeVO vo) throws Exception {
        return insert("ServiceNoticeDAO.insertOlSvcNotice", vo);
    }

    /**
     * 온라인 서비스 공지사항 수정
     * @param ServiceNoticeVO
     * @return 수정 결과
     */
    @Override
    public int updateOlSvcNotice(ServiceNoticeVO vo) throws Exception {
        return update("ServiceNoticeDAO.updateOlSvcNotice", vo);
    }

    /**
     * 가장 최근의 온라인 서비스 공지사항 정보 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 정보
     */
    @Override
    public ServiceNoticeVO latestOlSvcNoticeDetail(ServiceNoticeVO vo) throws Exception {
        return selectOne("ServiceNoticeDAO.selectLatestOlSvcNoticeDetail", vo);
    }

    /**
     * 온라인 서비스 공지사항 삭제 (DB에서 삭제)
     * @param ServiceNoticeVO
     * @return 삭제 결과
     */
    @Override
    public int deleteOlSvcNotice(ServiceNoticeVO vo) throws Exception {
        return delete("ServiceNoticeDAO.deleteOlSvcNotice", vo);
    }

}
