/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicenotice.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.servicenotice.dao.ServiceNoticeDAO;
import kt.com.im.cms.servicenotice.vo.ServiceNoticeVO;

/**
 *
 * 서비스 공지사항 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.11.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("ServiceNoticeService")
public class ServiceNoticeServiceImpl implements ServiceNoticeService {

    @Resource(name = "ServiceNoticeDAO")
    private ServiceNoticeDAO svcNoticeDAO;

    /**
     * 서비스 공지사항 리스트 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 서비스 공지사항 리스트
     */
    @Override
    public List<ServiceNoticeVO> svcNoticeList(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.svcNoticeList(vo);
    }

    /**
     * 서비스 공지사항 리스트 합계 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 서비스 공지사항 리스트 합계
     */
    @Override
    public int svcNoticeListTotalCount(ServiceNoticeVO vo) throws Exception {
        int res = svcNoticeDAO.svcNoticeListTotalCount(vo);
        return res;
    }

    /**
     * 서비스 공지사항 상세 정보 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 서비스 공지사항 상세 정보
     */
    @Override
    public ServiceNoticeVO svcNoticeDetail(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.svcNoticeDetail(vo);
    }

    /**
     * 서비스 공지사항 조회수 업데이트
     * @param ServiceNoticeVO
     * @return
     */
    @Override
    public void updateSvcNoticeRetvNum(ServiceNoticeVO vo) throws Exception {
        svcNoticeDAO.updateSvcNoticeRetvNum(vo);
    }

    /**
     * 서비스 공지사항 등록
     * @param ServiceNoticeVO
     * @return 등록 결과
     */
    @Override
    public int insertSvcNotice(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.insertSvcNotice(vo);
    }

    /**
     * 서비스 공지사항 수정
     * @param ServiceNoticeVO
     * @return 수정 결과
     */
    @Override
    public int updateSvcNotice(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.updateSvcNotice(vo);
    }

    /**
     * 서비스 공지사항 삭제
     * @param ServiceNoticeVO
     * @return 삭제 결과
     */
    @Override
    public int deleteSvcNotice(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.deleteSvcNotice(vo);
    }

    /**
     * 첨부파일 정보
     * @param ServiceNoticeVO
     * @return 첨부파일 정보
     */
    @Override
    public ServiceNoticeVO fileInfo(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.fileInfo(vo);
    }

    /**
     * 첨부파일 삭제
     * @param ServiceNoticeVO
     * @return 삭제결과
     */
    @Override
    public int attachFileDelete(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.attachFileDelete(vo);
    }

    /**
     * 서비스 공지사항 게시 일자 종료에 따른 비활성화
     * @param ServiceNoticeVO
     * @return 업데이트 결과
     */
    @Override
    public int updateSvcNoticeDisplayFinish(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.updateSvcNoticeDisplayFinish(vo);
    }

    /**
     * 가장 최근의 서비스 공지사항 정보 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 서비스 공지사항 정보
     */
    @Override
    public ServiceNoticeVO getLatestSvcNoticeDetail(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.getLatestSvcNoticeDetail(vo);
    }

    /**
     * 온라인 서비스 중요 공지사항 리스트 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 중요 공지사항 리스트
     */
    @Override
    public List<ServiceNoticeVO> olSvcNoticeImptList(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.olSvcNoticeImptList(vo);
    }

    /**
     * 온라인 서비스 중요 공지사항 리스트 합계 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 중요 공지사항 리스트 합계
     */
    @Override
    public int olSvcNoticeImptListTotalCount(ServiceNoticeVO vo) throws Exception {
         return svcNoticeDAO.olSvcNoticeImptListTotalCount(vo);
    }

    /**
     * 온라인 서비스 공지사항 리스트 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 리스트
     */
    @Override
    public List<ServiceNoticeVO> olSvcNoticeList(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.olSvcNoticeList(vo);
    }

    /**
     * 온라인 서비스 공지사항 리스트 합계 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 리스트 합계
     */
    @Override
    public int olSvcNoticeListTotalCount(ServiceNoticeVO vo) throws Exception {
         return svcNoticeDAO.olSvcNoticeListTotalCount(vo);
    }

    /**
     * 온라인 서비스 공지사항 상세 정보 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 상세 정보
     */
    @Override
    public ServiceNoticeVO olSvcNoticeDetail(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.olSvcNoticeDetail(vo);
    }

    /**
     * 중요 서비스 공지사항 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 정보
     */
    @Override
    public ServiceNoticeVO getImptSvcNotice(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.getImptSvcNotice(vo);
    }

    /**
     * 온라인 서비스 공지사항 중요도 수정
     * @param ServiceNoticeVO
     * @return 수정 결과
     */
    @Override
    public int updateImptSvcNotice(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.updateImptSvcNotice(vo);
    }

    /**
     * 온라인 서비스 공지사항 등록
     * @param ServiceNoticeVO
     * @return 등록 결과
     */
    @Override
    public int insertOlSvcNotice(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.insertOlSvcNotice(vo);
    }

    /**
     * 온라인 서비스 공지사항 수정
     * @param ServiceNoticeVO
     * @return 수정 결과
     */
    @Override
    public int updateOlSvcNotice(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.updateOlSvcNotice(vo);
    }

    /**
     * 가장 최근의 온라인 서비스 공지사항 정보 조회
     * @param ServiceNoticeVO
     * @return 검색 조건에 부합하는 온라인 서비스 공지사항 정보
     */
    @Override
    public ServiceNoticeVO latestOlSvcNoticeDetail(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.latestOlSvcNoticeDetail(vo);
    }

    /**
     * 온라인 서비스 공지사항 삭제 (DB에서 삭제)
     * @param ServiceNoticeVO
     * @return 삭제 결과
     */
    @Override
    public int deleteOlSvcNotice(ServiceNoticeVO vo) throws Exception {
        return svcNoticeDAO.deleteOlSvcNotice(vo);
    }

}
