/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicenotice.web;

import java.io.File;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFileUtil;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.file.web.FileApi;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.servicenotice.service.ServiceNoticeService;
import kt.com.im.cms.servicenotice.vo.ServiceNoticeVO;

/**
 *
 * 서비스 공지사항 관련 처리 API
 *
 * @author A2TEC
 * @since 2018.11.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class ServiceNoticeApi {

    private final static String viewName = "../resources/api/service/customer/serviceNotice/serviceNoticeProcess";
    private final static String olSvcViewName = "../resources/api/onlineService/customer/serviceNotice/serviceNoticeProcess";

    @Resource(name="ServiceNoticeService")
    private ServiceNoticeService svcNoticeService;

    @Resource(name="FileService")
    private FileService fileService;

    @Resource(name="MemberService")
    private MemberService memberService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Inject
    private FileSystemResource fsResource;

    @Autowired
    private CommonFileUtil fileUtil;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * 서비스 공지사항 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/svcNotice")
    public ModelAndView svcNoticeProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        String svcType = CommonFnc.emptyCheckString("svcType", request);
        if (userVO == null || !isAllowMember(userVO, svcType)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        if (svcType.equals("")) {
            rsModel.setViewName(viewName);
            return offlineSvcNoticeProcess(request, rsModel, userVO);
        }

        if (svcType.equals("ON")) {
            rsModel.setViewName(olSvcViewName);
            return onlineSvcNoticeProcess(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * (Offline) 서비스 공지사항 - 현재는 서비스 매장관리 > 고객센터 관리 > 공지사항
     */
    private ModelAndView offlineSvcNoticeProcess(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        request.setCharacterEncoding("UTF-8");

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return addOfflineSvcNotice(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return editOfflineSvcNotice(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return delOfflineSvcNotice(request, rsModel, userVO);
        }

        if (method.equals("GET")) {
            return getOfflineSvcNotice(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addOfflineSvcNotice(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"svcNoticeTitle,svcNoticeType,startDt,endDt,svcNoticeSbst");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String svcNoticeTitle = CommonFnc.emptyCheckString("svcNoticeTitle", request);
        int svcNoticeType = CommonFnc.emptyCheckInt("svcNoticeType", request);
        String startDt = CommonFnc.emptyCheckString("startDt", request);
        String endDt = CommonFnc.emptyCheckString("endDt", request);
        String svcNoticeSbst = CommonFnc.emptyCheckString("svcNoticeSbst", request);
        String delYn = CommonFnc.emptyCheckString("delYn", request);

        ServiceNoticeVO item = new ServiceNoticeVO();

        item.setSvcNoticeTitle(svcNoticeTitle);
        item.setSvcNoticeType(svcNoticeType);
        item.setStDt(startDt);
        item.setFnsDt(endDt);
        item.setSvcNoticeSbst(svcNoticeSbst);
        item.setDelYn(delYn);
        item.setLoginId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcNoticeService.insertSvcNotice(item);
            if (result > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("svcNoticeSeq", item.getSvcNoticeSeq());
                rsModel.setResultType("svcNoticeInsert");
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch(Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView editOfflineSvcNotice(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"svcNoticeSeq,svcNoticeTitle,svcNoticeType,startDt,endDt,svcNoticeSbst");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        int svcNoticeSeq = CommonFnc.emptyCheckInt("svcNoticeSeq", request);
        String svcNoticeTitle = CommonFnc.emptyCheckString("svcNoticeTitle", request);
        int svcNoticeType = CommonFnc.emptyCheckInt("svcNoticeType", request);
        String startDt = CommonFnc.emptyCheckString("startDt", request);
        String endDt = CommonFnc.emptyCheckString("endDt", request);
        String svcNoticeSbst = CommonFnc.emptyCheckString("svcNoticeSbst", request);
        String delYn = CommonFnc.emptyCheckString("delYn", request);

        ServiceNoticeVO item = new ServiceNoticeVO();

        item.setSvcNoticeSeq(svcNoticeSeq);
        item.setSvcNoticeTitle(svcNoticeTitle);
        item.setSvcNoticeType(svcNoticeType);
        item.setStDt(startDt);
        item.setFnsDt(endDt);
        item.setSvcNoticeSbst(svcNoticeSbst);
        item.setDelYn(delYn);
        item.setLoginId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcNoticeService.updateSvcNotice(item);
            if (result == 1) {
                String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);
                if (!delFileSeqs.equals("")) {
                    fileUtil.deleteFiles(delFileSeqs);
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.rollback(status);
            }
        } catch (Exception e){
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if(!status.isCompleted()){
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView delOfflineSvcNotice(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String noticeSeqList = CommonFnc.emptyCheckString("delSvcNoticeSeqList", request);
        String[] delNoticeSeqList = noticeSeqList.split(",");

        if (delNoticeSeqList.length == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No notice list to delete.)");
            return rsModel.getModelAndView();
        }

        List<Integer> deleteList = new ArrayList<Integer>();
        for(int i = 0; i < delNoticeSeqList.length; i++) {
            deleteList.add(Integer.parseInt(delNoticeSeqList[i]));
        }

        ServiceNoticeVO item = new ServiceNoticeVO();

        item.setLoginId(userVO.getMbrId());
        item.setDeleteSvcNoticeSeqList(deleteList);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            svcNoticeService.deleteSvcNotice(item);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            transactionManager.commit(status);
        } catch(Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOfflineSvcNotice(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        ServiceNoticeVO item = new ServiceNoticeVO();
        FileVO fileVO = new FileVO();

        int svcNoticeSeq = CommonFnc.emptyCheckInt("svcNoticeSeq", request);

        if (svcNoticeSeq != 0) {
            item.setSvcNoticeSeq(svcNoticeSeq);
            fileVO.setFileContsSeq(svcNoticeSeq);
            fileVO.setFileSe("SVC_NOTI");
            List<FileVO> flist = fileService.fileList(fileVO);

            ServiceNoticeVO resultItem = svcNoticeService.svcNoticeDetail(item);

            if (resultItem == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                String searchType = CommonFnc.emptyCheckString("searchType", request);
                boolean isAllowNotice = this.isAllowNotice(userVO, resultItem, searchType);
                if (!isAllowNotice) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                // 서비스 공지사항 수정을 위해 상세 정보 요청 시에는 조회 수 업데이트 하지 않음.
                if (!searchType.equals("edit")) {
                    svcNoticeService.updateSvcNoticeRetvNum(item);
                    resultItem.setRetvNum(resultItem.getRetvNum() + 1);
                }

                // 개인정보 마스킹 (등록자 이름)
                resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                // 내용 복원
                resultItem.setSvcNoticeSbst(CommonFnc.unescapeStr(resultItem.getSvcNoticeSbst()));

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("fileList",flist);
                rsModel.setResultType("svcNoticeDetail");
            }
        } else {
            // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
            if (svcNoticeSeq == 0 && CommonFnc.checkReqParameter(request, "svcNoticeSeq")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                return rsModel.getModelAndView();
            }

            item.setMbrSe(userVO.getMbrSe());
            item.setMbrSvcSeq(userVO.getSvcSeq());
            item.setMbrStorSeq(userVO.getStorSeq());

            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            item.setSearchConfirm(searchConfirm);
            // 검색 여부가 true일 경우
            if(searchConfirm.equals("true")){
                String searchField = CommonFnc.emptyCheckString("searchField", request);
                String searchString = CommonFnc.emptyCheckString("searchString", request);

                if (searchField =="svcNoticeTitle") {
                    searchField = "SVC_NOTICE_TITLE";
                } else if (searchField =="cretrNm") {
                    searchField = "CRETR_NM";
                }

                item.setSearchTarget(searchField);
                item.setSearchKeyword(searchString);
            }
            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
            String sidx = CommonFnc.emptyCheckString("sidx", request);
            String sord = CommonFnc.emptyCheckString("sord", request);

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
            if (sidx.equals("num")) {
                sidx="SVC_NOTICE_SEQ";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setOffset(page);
            item.setLimit(pageEnd);

            List<ServiceNoticeVO> resultList = svcNoticeService.svcNoticeList(item);

            if (resultList.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (등록자 이름) */
                for (int i = 0; i < resultList.size(); i++) {
                    resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
                }

                int totalCount = svcNoticeService.svcNoticeListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultList);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;
                rsModel.setData("totalPage", totalPage);
                rsModel.setResultType("svcNoticeList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * (Online) 서비스 공지사항
     */
    private ModelAndView onlineSvcNoticeProcess(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        request.setCharacterEncoding("UTF-8");

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return addOnlineSvcNotice(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return editOnlineSvcNotice(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return delOnlineSvcNotice(request, rsModel, userVO);
        }

        if (method.equals("GET")) {
            boolean isList = (request.getParameter("svcNoticeSeq") == null);
            if (isList) {
                return getOnlineSvcNoticeList(request, rsModel, userVO);
            }

            return getOnlineSvcNotice(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addOnlineSvcNotice(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"svcNoticeType,dispType,targetType,prefRank,svcNoticeTitle,isPost");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        int svcNoticeType = CommonFnc.emptyCheckInt("svcNoticeType", request);
        if (!isAllowOlSvcNotice(userVO, svcNoticeType)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcNoticeType(svcSeq))");
            return rsModel.getModelAndView();
        }

        String dispType = CommonFnc.strFilter(CommonFnc.emptyCheckString("dispType", request));
        String targetType = CommonFnc.strFilter(CommonFnc.emptyCheckString("targetType", request));
        int prefRank = CommonFnc.emptyCheckInt("prefRank", request);
        String svcNoticeTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("svcNoticeTitle", request));
        String isPost = CommonFnc.strFilter(CommonFnc.emptyCheckString("isPost", request)); // 사용 여부 (게시/중지) -> delYn으로 치환
        String svcNoticeSbst = CommonFnc.strFilter(CommonFnc.emptyCheckString("svcNoticeSbst", request));
        String postViewYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("postViewYn", request));
        String popupViewYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupViewYn", request));
        String popupUrl = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupUrl", request));
        String popupPerdYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupPerdYn", request)); // 팝업 기간 설정 여부 (N: 미설정, Y: 기간 설정)
        String popupViewStDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupViewStDt", request));
        String popupViewFnsDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupViewFnsDt", request));
        String hasFile = CommonFnc.strFilter(CommonFnc.emptyCheckString("hasFile", request));

        ServiceNoticeVO item = new ServiceNoticeVO();
        item.setSvcNoticeType(svcNoticeType);
        item.setDispType(dispType);
        item.setTargetType(targetType);
        item.setSvcNoticeTitle(svcNoticeTitle);
        item.setSvcNoticeSbst(svcNoticeSbst);
        item.setPostViewYn(postViewYn);
        item.setPopupViewYn(popupViewYn);

        // escape 문자들 원복하여 저장
        popupUrl = CommonFnc.unescapeStr(popupUrl);
        item.setPopupUrl(popupUrl);
        item.setPopupPerdYn(popupPerdYn);
        item.setPopupViewStDt(popupViewStDt);
        item.setPopupViewFnsDt(popupViewFnsDt);

        if (isPost.equals("Y")) {
            item.setDelYn("N");
        } else {
            // 게시 중지일 경우 중요도 설정은 무시하고 이를 0으로 변경하여 일반 공지사항으로 변경함.
            prefRank = 0;
            item.setDelYn("Y");
        }

        item.setLoginId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = 0;

            // 서비스 공지사항 중요도가 '일반'인 경우 0, 그렇지 않은 경우 3: 보통, 2: 높음, 1: 가장 높음
            // 중요도가 설정된 경우 동일 레벨 및 이전의 중요 공지사항은 한단계씩 레벨을 아래로 조정함.
            if (prefRank > 0) {
                int mostImptNotice = 0;
                int imptNotice = 0;
                int normalNotice = 0;
                boolean doInsert = false;

                for (int i = prefRank; i < 4; i++) {
                    ServiceNoticeVO vo = new ServiceNoticeVO();
                    vo.setSvcNoticeType(item.getSvcNoticeType());
                    vo.setPrefRank(i);
                    vo = svcNoticeService.getImptSvcNotice(vo);

                    if (i == 1) {
                        if (vo == null) {
                            item.setPrefRank(prefRank);
                            result = svcNoticeService.insertOlSvcNotice(item);
                            doInsert = true;
                            break;
                        } else {
                            mostImptNotice = vo.getSvcNoticeSeq();
                        }
                    } else if (i == 2) {
                        if (vo == null) {
                            if (mostImptNotice > 0) {
                                break;
                            } else {
                                item.setPrefRank(prefRank);
                                result = svcNoticeService.insertOlSvcNotice(item);
                                doInsert = true;
                                break;
                            }
                        } else {
                            imptNotice = vo.getSvcNoticeSeq();
                        }
                    } else if (i == 3) {
                        if (vo == null) {
                            if (imptNotice > 0) {
                                break;
                            } else {
                                item.setPrefRank(prefRank);
                                result = svcNoticeService.insertOlSvcNotice(item);
                                doInsert = true;
                                break;
                            }
                        } else {
                            normalNotice = vo.getSvcNoticeSeq();
                        }
                    }
                }

                if (!doInsert) {
                    ServiceNoticeVO vo = new ServiceNoticeVO();
                    vo.setLoginId(userVO.getMbrId());
                    if (normalNotice > 0) {
                        vo.setSvcNoticeSeq(normalNotice);
                        vo.setPrefRank(0);
                        svcNoticeService.updateImptSvcNotice(vo);
                    }

                    if (imptNotice > 0) {
                        vo.setSvcNoticeSeq(imptNotice);
                        vo.setPrefRank(3);
                        svcNoticeService.updateImptSvcNotice(vo);
                    }

                    if (mostImptNotice > 0) {
                        vo.setSvcNoticeSeq(mostImptNotice);
                        vo.setPrefRank(2);
                        svcNoticeService.updateImptSvcNotice(vo);
                    }

                    item.setPrefRank(prefRank);
                    result = svcNoticeService.insertOlSvcNotice(item);
                }
            } else {
                item.setPrefRank(prefRank);
                result = svcNoticeService.insertOlSvcNotice(item);
            }

            if (result > 0) {
                if (hasFile.equals("Y")) {
                    String resultStr = fileUtil.insertFile(request, userVO, item.getSvcNoticeSeq(), "NOTI_POPUP");
                    if (resultStr.equals(CommonFileUtil.success)) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setData("svcNoticeSeq", item.getSvcNoticeSeq());
                        rsModel.setResultType("insertOlSvcNotice");
                        transactionManager.commit(status);
                    } else {
                        if (resultStr.equals(CommonFileUtil.serverErr)) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                            transactionManager.rollback(status);
                        } else {
                            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + resultStr + ")");
                            transactionManager.rollback(status);
                        }
                    }
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("svcNoticeSeq", item.getSvcNoticeSeq());
                    rsModel.setResultType("insertOlSvcNotice");
                    transactionManager.commit(status);
                }
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch(Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView editOnlineSvcNotice(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"svcNoticeSeq,svcNoticeType,dispType,targetType,prefRank,svcNoticeTitle,isPost");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        int svcNoticeSeq = CommonFnc.emptyCheckInt("svcNoticeSeq", request);
        if (svcNoticeSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcNoticeSeq)");
            return rsModel.getModelAndView();
        }

        int svcNoticeType = CommonFnc.emptyCheckInt("svcNoticeType", request);
        if (!isAllowOlSvcNotice(userVO, svcNoticeType)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcNoticeType(svcSeq))");
            return rsModel.getModelAndView();
        }

        String dispType = CommonFnc.strFilter(CommonFnc.emptyCheckString("dispType", request));
        String targetType = CommonFnc.strFilter(CommonFnc.emptyCheckString("targetType", request));
        int prefRank = CommonFnc.emptyCheckInt("prefRank", request);
        String svcNoticeTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("svcNoticeTitle", request));
        String isPost = CommonFnc.strFilter(CommonFnc.emptyCheckString("isPost", request)); // 사용 여부 (게시/중지) -> delYn으로 치환
        String svcNoticeSbst = CommonFnc.strFilter(CommonFnc.emptyCheckString("svcNoticeSbst", request));
        String postViewYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("postViewYn", request));
        String popupViewYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupViewYn", request));
        String popupUrl = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupUrl", request));
        String popupPerdYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupPerdYn", request)); // 팝업 기간 설정 여부 (N: 미설정, Y: 기간 설정)
        String popupViewStDt = CommonFnc.emptyCheckString("popupViewStDt", request);
        String popupViewFnsDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("popupViewFnsDt", request));
        String hasFile = CommonFnc.strFilter(CommonFnc.emptyCheckString("hasFile", request));
        String chgFile = CommonFnc.strFilter(CommonFnc.emptyCheckString("chgFile", request));

        ServiceNoticeVO item = new ServiceNoticeVO();
        item.setSvcNoticeSeq(svcNoticeSeq);
        item.setSvcNoticeType(svcNoticeType);
        item.setDispType(dispType);
        item.setTargetType(targetType);
        item.setSvcNoticeTitle(svcNoticeTitle);
        item.setSvcNoticeSbst(svcNoticeSbst);
        item.setPostViewYn(postViewYn);
        item.setPopupViewYn(popupViewYn);

        // escape 문자들 원복하여 저장
        popupUrl = CommonFnc.unescapeStr(popupUrl);
        item.setPopupUrl(popupUrl);
        item.setPopupPerdYn(popupPerdYn);
        item.setPopupViewStDt(popupViewStDt);
        item.setPopupViewFnsDt(popupViewFnsDt);

        if (isPost.equals("Y")) {
            item.setDelYn("N");
        } else {
            // 게시 중지일 경우 중요도 값을 0으로 변경 (일반 공지사항으로 변경)
            prefRank = 0;
            item.setDelYn("Y");
        }

        item.setLoginId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = 0;

            // 서비스 공지사항 중요도가 '일반'인 경우 0, 그렇지 않은 경우 3: 보통, 2: 높음, 1: 가장 높음
            // 중요도가 설정된 경우 동일 레벨 및 이전의 중요 공지사항은 한단계씩 레벨을 아래로 조정함.
            if (prefRank > 0) {
                int mostImptNotice = 0;
                int imptNotice = 0;
                int normalNotice = 0;
                boolean doUpdate = false;

                for (int i = prefRank; i < 4; i++) {
                    ServiceNoticeVO vo = new ServiceNoticeVO();
                    vo.setSvcNoticeType(item.getSvcNoticeType());
                    vo.setPrefRank(i);
                    vo = svcNoticeService.getImptSvcNotice(vo);

                    if (i == 1) {
                        if (vo == null) {
                            item.setPrefRank(prefRank);
                            result = svcNoticeService.updateOlSvcNotice(item);
                            doUpdate = true;
                            break;
                        } else {
                            if (vo.getSvcNoticeSeq() == svcNoticeSeq) {
                                break;
                            }
                            mostImptNotice = vo.getSvcNoticeSeq();
                        }
                    } else if (i == 2) {
                        if (vo == null) {
                            if (mostImptNotice > 0) {
                                break;
                            } else {
                                item.setPrefRank(prefRank);
                                result = svcNoticeService.updateOlSvcNotice(item);
                                doUpdate = true;
                                break;
                            }
                        } else {
                            if (vo.getSvcNoticeSeq() == svcNoticeSeq) {
                                break;
                            }
                            imptNotice = vo.getSvcNoticeSeq();
                        }
                    } else if (i == 3) {
                        if (vo == null) {
                            if (imptNotice > 0) {
                                break;
                            } else {
                                item.setPrefRank(prefRank);
                                result = svcNoticeService.updateOlSvcNotice(item);
                                doUpdate = true;
                                break;
                            }
                        } else {
                            if (vo.getSvcNoticeSeq() == svcNoticeSeq) {
                                break;
                            }
                            normalNotice = vo.getSvcNoticeSeq();
                        }
                    }
                }

                if (!doUpdate) {
                    ServiceNoticeVO vo = new ServiceNoticeVO();
                    vo.setLoginId(userVO.getMbrId());
                    if (normalNotice > 0) {
                        vo.setSvcNoticeSeq(normalNotice);
                        vo.setPrefRank(0);
                        svcNoticeService.updateImptSvcNotice(vo);
                    }

                    if (imptNotice > 0) {
                        vo.setSvcNoticeSeq(imptNotice);
                        vo.setPrefRank(3);
                        svcNoticeService.updateImptSvcNotice(vo);
                    }

                    if (mostImptNotice > 0) {
                        vo.setSvcNoticeSeq(mostImptNotice);
                        vo.setPrefRank(2);
                        svcNoticeService.updateImptSvcNotice(vo);
                    }

                    item.setPrefRank(prefRank);
                    result = svcNoticeService.updateOlSvcNotice(item);
                }
            } else {
                item.setPrefRank(prefRank);
                result = svcNoticeService.updateOlSvcNotice(item);
            }

            if (result == 1) {
                String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);

                if (chgFile.equals("Y") || hasFile.equals("Y")) {
                    String resultStr = fileUtil.insertFile(request, userVO, item.getSvcNoticeSeq(), "NOTI_POPUP");
                    if (!resultStr.equals(CommonFileUtil.success)) {
                        if (resultStr.equals(CommonFileUtil.serverErr)) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                            transactionManager.rollback(status);
                        } else {
                            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + resultStr + ")");
                            transactionManager.rollback(status);
                        }
                        return rsModel.getModelAndView();
                    }
                }

                if (!delFileSeqs.equals("")) {
                    fileUtil.deleteFiles(delFileSeqs);
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.rollback(status);
            }
        } catch (Exception e){
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if(!status.isCompleted()){
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView delOnlineSvcNotice(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String noticeSeqList = CommonFnc.emptyCheckString("delSvcNoticeSeqList", request);
        String[] delNoticeSeqList = noticeSeqList.split(",");

        if (delNoticeSeqList.length == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No notice list to delete.)");
            return rsModel.getModelAndView();
        }

        List<Integer> deleteList = new ArrayList<Integer>();
        String delFileSeqs = "";
        for(int i = 0; i < delNoticeSeqList.length; i++) {
            int delSeq = Integer.parseInt(delNoticeSeqList[i]);
            deleteList.add(delSeq);

            FileVO fvo = new FileVO();
            fvo.setFileContsSeq(delSeq);
            fvo.setFileSe("NOTI_POPUP");
            FileVO fItem = fileService.fileDetailInfo(fvo);
            if (fItem != null) {
                delFileSeqs += String.valueOf(fItem.getFileSeq()) + ",";
            }
            fvo.setFileSe("SVC_NOTI");
            fItem = fileService.fileDetailInfo(fvo);
            if (fItem != null) {
                delFileSeqs += String.valueOf(fItem.getFileSeq()) + ",";
            }
        }

        if (delFileSeqs.length() > 0 && delFileSeqs.charAt(delFileSeqs.length()-1)==',') {
            delFileSeqs = delFileSeqs.substring(0, delFileSeqs.length()-1);
        }

        ServiceNoticeVO item = new ServiceNoticeVO();

        item.setLoginId(userVO.getMbrId());
        item.setDeleteSvcNoticeSeqList(deleteList);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcNoticeService.deleteOlSvcNotice(item);
            if (delNoticeSeqList.length == result) {
                if (!delFileSeqs.isEmpty()) {
                    fileUtil.deleteFiles(delFileSeqs);
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.rollback(status);
            }
        } catch(Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOnlineSvcNoticeList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        /*
        int svcNoticeSeq = CommonFnc.emptyCheckInt("svcNoticeSeq", request);

        // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
        if (svcNoticeSeq == 0 && CommonFnc.checkReqParameter(request, "svcNoticeSeq")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            return rsModel.getModelAndView();
        }
        */

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        // 서비스 관리자는 해당 서비스의 공지사항만 조회 가능
        if (!isAllowOlSvcNotice(userVO, svcSeq)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            return rsModel.getModelAndView();
        }

        ServiceNoticeVO item = new ServiceNoticeVO();

        item.setSvcNoticeType(svcSeq);
        item.setMbrSe(userVO.getMbrSe());

        // 중요 서비스 공지사항 목록 조회
        List<ServiceNoticeVO> svcImptNoticeList = svcNoticeService.olSvcNoticeImptList(item);
        int imptTotCnt = svcNoticeService.olSvcNoticeImptListTotalCount(item);

        String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
        item.setSearchConfirm(searchConfirm);
        // 검색 여부가 true일 경우
        if(searchConfirm.equals("true")){
            String searchField = CommonFnc.emptyCheckString("searchField", request);
            String searchString = CommonFnc.emptyCheckString("searchString", request);

            if (searchField =="svcNoticeTitle") {
                searchField = "SVC_NOTICE_TITLE";
            } else if (searchField =="cretrNm") {
                searchField = "CRETR_NM";
            }

            item.setSearchTarget(searchField);
            item.setSearchKeyword(searchString);
        }
        /*
         * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
         */
        int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
        int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 13);
        // 중요 공지사항은 최대 3개까지 지정 가능하므로 한 페이지에 표시할 row 갯수는 13로 하여 조회 쿼리에서는 10개 단위로 조회함.
        limit = limit - 3;
        String sidx = CommonFnc.emptyCheckString("sidx", request);
        String sord = CommonFnc.emptyCheckString("sord", request);

        int page = (currentPage - 1) * limit + 1;
        int pageEnd = page + limit - 1;

        // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
        if (sidx.equals("num")) {
            sidx="SVC_NOTICE_SEQ";
        }

        item.setSidx(sidx);
        item.setSord(sord);
        item.setOffset(page);
        item.setLimit(pageEnd);

        List<ServiceNoticeVO> svcNoticeList = svcNoticeService.olSvcNoticeList(item);
        int totCnt = svcNoticeService.olSvcNoticeListTotalCount(item);

        List<ServiceNoticeVO> resultList = new ArrayList<ServiceNoticeVO>();
        resultList.addAll(svcImptNoticeList);
        resultList.addAll(svcNoticeList);

        if (resultList.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            /* 개인정보 마스킹 (등록자 이름) */
            for (int i = 0; i < resultList.size(); i++) {
                resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultList);
            rsModel.setData("imptNoticeTotCnt", imptTotCnt);
            rsModel.setData("noticeTotCnt", totCnt);
            int totalCount = imptTotCnt + totCnt;
            rsModel.setData("totalCount", totalCount);
            rsModel.setData("row", limit);
            rsModel.setData("currentPage", currentPage);
            int totalPage = (limit > 0) ? (int) Math.ceil((double) totCnt / limit) : 1;
            rsModel.setData("totalPage", totalPage);
            rsModel.setResultType("olSvcNoticeList");
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOnlineSvcNotice(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        int svcNoticeSeq = CommonFnc.emptyCheckInt("svcNoticeSeq", request);
        if (svcNoticeSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcNoticeSeq)");
            return rsModel.getModelAndView();
        }

        FileVO fileVO = new FileVO();

        fileVO.setFileContsSeq(svcNoticeSeq);
        fileVO.setFileSe("NOTI_POPUP");
        List<FileVO> popupImgFList = fileService.fileList(fileVO);

        fileVO.setFileSe("SVC_NOTI");
        List<FileVO> attachFList = fileService.fileList(fileVO);

        ServiceNoticeVO item = new ServiceNoticeVO();
        item.setSvcNoticeSeq(svcNoticeSeq);
        item.setMbrSe(userVO.getMbrSe());
        ServiceNoticeVO resultItem = svcNoticeService.olSvcNoticeDetail(item);

        if (resultItem == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            if (!isAllowOlSvcNotice(userVO, resultItem.getSvcNoticeType())) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            String searchType = CommonFnc.emptyCheckString("searchType", request);
            // 서비스 FAQ 수정을 위해 상세 정보 요청 시에는 조회 수 업데이트 하지 않음.
            if (!searchType.equals("edit")) {
                svcNoticeService.updateSvcNoticeRetvNum(item);
                resultItem.setRetvNum(resultItem.getRetvNum() + 1);
            }

            // 개인정보 마스킹 (등록자 이름)
            resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

            // 내용 복원
            resultItem.setSvcNoticeSbst(CommonFnc.unescapeStr(resultItem.getSvcNoticeSbst()));

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultItem);
            rsModel.setData("popupImgFileList",popupImgFList);
            rsModel.setData("attachFileList",attachFList);
            rsModel.setResultType("olSvcNoticeDetail");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 외부 서비스 사이트를 위한 서비스 공지사항 목록 및 상세정보 조회 요청 API (온라인 서비스 관리 내 공지사항 (GLT 런처))
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/getSvcNoticeAPI")
    public ModelAndView getOlSvcNoticeProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(olSvcViewName);

        request.setCharacterEncoding("UTF-8");
        String method = CommonFnc.getMethod(request);

        if (method.equals("GET")) {
            String appType = CommonFnc.emptyCheckString("appType", request);
            if (appType.equals("") || (!appType.equals("SA") && !appType.equals("MA"))) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(appType)");
                return rsModel.getModelAndView();
            }

            boolean isList = (request.getParameter("noticeSeq") == null);
            if (isList) {
                return getOnlineNoticeList(request, rsModel, appType);
            }

            return getOnlineNotice(request, rsModel, appType);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 외부 서비스 사이트를 위한 서비스 공지사항 목록 및 상세정보 조회 요청 API (서비스 매장 관리 내 공지사항으로 현재 LiveOn에서 사용 중)
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/getNoticeAPI")
    public ModelAndView getNoticeProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        String method = CommonFnc.getMethod(request);

        if (method.equals("GET")) {
            boolean isList = (request.getParameter("noticeSeq") == null);
            if (isList) {
                return getOfflineNoticeList(request, rsModel);
            }

            return getOfflineNotice(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getOfflineNoticeList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        ServiceNoticeVO item = new ServiceNoticeVO();
        item.setSvcNoticeType(svcSeq);

        /*
         * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
         */
        int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
        int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);

        int page = (currentPage - 1) * limit + 1;
        int pageEnd = page + limit - 1;

        item.setSord("desc");
        item.setOffset(page);
        item.setLimit(pageEnd);

        List<ServiceNoticeVO> resultList = svcNoticeService.svcNoticeList(item);

        if (resultList.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            /* 개인정보 마스킹 (등록자 이름) */
            for (int i = 0; i < resultList.size(); i++) {
                resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
            }

            int totalCount = svcNoticeService.svcNoticeListTotalCount(item);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultList);
            rsModel.setData("totalCount", totalCount);
            rsModel.setData("row", limit);
            rsModel.setData("currentPage", currentPage);
            int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;
            rsModel.setData("totalPage", totalPage);
            rsModel.setResultType("getNoticeList");
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOfflineNotice(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        int noticeSeq = CommonFnc.emptyCheckInt("noticeSeq", request);
        if (noticeSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(noticeSeq)");
            return rsModel.getModelAndView();
        }

        ServiceNoticeVO item = new ServiceNoticeVO();
        FileVO fileVO = new FileVO();

        item.setSvcNoticeType(svcSeq);
        item.setSvcNoticeSeq(noticeSeq);

        fileVO.setFileContsSeq(noticeSeq);
        fileVO.setFileSe("SVC_NOTI");

        List<FileVO> flist = fileService.fileList(fileVO);

        ServiceNoticeVO resultItem = svcNoticeService.svcNoticeDetail(item);

        if (resultItem == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            // 개인정보 마스킹 (등록자 이름)
            resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

            // 서비스 공지사항 내용에서 html 태그 원복 (&lt;, &amp; 등으로 치환된 코드를 태그 문자로 원복) ==> response 받은 쪽에서 다시 치환 필요
            String originSbst = CommonFnc.unescapeStr(resultItem.getSvcNoticeSbst());
            resultItem.setSvcNoticeSbst(originSbst);

            svcNoticeService.updateSvcNoticeRetvNum(item);
            resultItem.setRetvNum(resultItem.getRetvNum() + 1); 
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultItem);
            rsModel.setData("fileList",flist);
            rsModel.setResultType("getNoticeDetail");
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOnlineNoticeList(HttpServletRequest request, ResultModel rsModel, String appType) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        ServiceNoticeVO item = new ServiceNoticeVO();
        item.setSvcNoticeType(svcSeq);
        item.setDispType(appType);
        item.setTargetType(CommonFnc.emptyCheckString("targetType", request));

        List<ServiceNoticeVO> imptNoticeList = svcNoticeService.olSvcNoticeImptList(item);
        int imptTotCnt = svcNoticeService.olSvcNoticeImptListTotalCount(item);

        /*
         * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
         */
        int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
        int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 13);

        int page = (currentPage - 1) * limit + 1;
        int pageEnd = page + limit - 1;

        item.setSord("desc");
        item.setOffset(page);
        item.setLimit(pageEnd);

        List<ServiceNoticeVO> noticeList = svcNoticeService.olSvcNoticeList(item);
        int totCnt = svcNoticeService.olSvcNoticeListTotalCount(item);

        List<ServiceNoticeVO> resultList = new ArrayList<ServiceNoticeVO>();
        resultList.addAll(imptNoticeList);
        resultList.addAll(noticeList);

        if (resultList.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            /* 개인정보 마스킹 (등록자 이름) */
            for (int i = 0; i < resultList.size(); i++) {
                resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));

                // 서비스 공지사항 내용에서 html 태그 원복 (&lt;, &amp; 등으로 치환된 코드를 태그 문자로 원복) ==> response 받은 쪽에서 다시 치환 필요
                String originSbst = CommonFnc.unescapeStr(resultList.get(i).getSvcNoticeSbst());
                resultList.get(i).setSvcNoticeSbst(originSbst);
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultList);
            rsModel.setData("imptNoticeTotCnt", imptTotCnt);
            rsModel.setData("noticeTotCnt", totCnt);
            int totalCount = imptTotCnt + totCnt;
            rsModel.setData("totalCount", totalCount);
            rsModel.setData("row", limit);
            rsModel.setData("currentPage", currentPage);
            int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;
            rsModel.setData("totalPage", totalPage);
            rsModel.setResultType("getOlSvcNoticeList");
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOnlineNotice(HttpServletRequest request, ResultModel rsModel, String appType) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq,noticeSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        int noticeSeq = CommonFnc.emptyCheckInt("noticeSeq", request);
        if (noticeSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(noticeSeq)");
            return rsModel.getModelAndView();
        }

        FileVO fileVO = new FileVO();

        fileVO.setFileContsSeq(noticeSeq);
        fileVO.setFileSe("NOTI_POPUP");
        List<FileVO> popupImgFList = fileService.fileList(fileVO);

        fileVO.setFileSe("SVC_NOTI");
        List<FileVO> attachFList = fileService.fileList(fileVO);

        ServiceNoticeVO item = new ServiceNoticeVO();
        item.setSvcNoticeType(svcSeq);
        item.setSvcNoticeSeq(noticeSeq);
        item.setDispType(appType);
        item.setTargetType(CommonFnc.emptyCheckString("targetType", request));

        ServiceNoticeVO resultItem = svcNoticeService.olSvcNoticeDetail(item);

        if (resultItem == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            // 개인정보 마스킹 (등록자 이름)
            resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

            // 서비스 공지사항 내용에서 html 태그 원복 (&lt;, &amp; 등으로 치환된 코드를 태그 문자로 원복) ==> response 받은 쪽에서 다시 치환 필요
            String originSbst = CommonFnc.unescapeStr(resultItem.getSvcNoticeSbst());
            resultItem.setSvcNoticeSbst(originSbst);

            svcNoticeService.updateSvcNoticeRetvNum(item);
            resultItem.setRetvNum(resultItem.getRetvNum() + 1); 
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultItem);
            rsModel.setData("popupImgFileList",popupImgFList);
            rsModel.setData("attachFileList",attachFList);
            rsModel.setResultType("getOlSvcNoticeDetail");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 외부 서비스 사이트 요청에 따른 최신 서비스 공지사항 조회 API (온라인 서비스 관리 내 공지사항 (GLT 런처))
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/getSvcLatestNoticeAPI")
    public ModelAndView getSvcLatestNotice(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(olSvcViewName);

        request.setCharacterEncoding("UTF-8");
        String method = CommonFnc.getMethod(request);

        if (method.equals("GET")) {
            String appType = CommonFnc.emptyCheckString("appType", request);
            if (appType.equals("") || (!appType.equals("SA") && !appType.equals("MA"))) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(appType)");
                return rsModel.getModelAndView();
            }

            rsModel.setViewName(olSvcViewName);
            return getLatestOnlineNotice(request, rsModel, appType);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 외부 서비스 사이트 요청에 따른 최신 서비스 공지사항 조회 API (서비스 매장 관리 내 공지사항 (현재 LiveOn에서 사용 중)
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/getLatestNoticeAPI")
    public ModelAndView getLatestNotice(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        String method = CommonFnc.getMethod(request);

        if (method.equals("GET")) {
            rsModel.setViewName(viewName);
            return getLatestOfflineNotice(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getLatestOfflineNotice(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);

        ServiceNoticeVO item = new ServiceNoticeVO();

        item.setSvcNoticeType(svcSeq);
        ServiceNoticeVO resultItem = svcNoticeService.getLatestSvcNoticeDetail(item);

        if (resultItem != null) {
            // 서비스 공지사항 내용에서 html 태그 원복 (&lt;, &amp; 등으로 치환된 코드를 태그 문자로 원복) ==> response 받은 쪽에서 다시 치환 필요
            String originSbst = CommonFnc.unescapeStr(resultItem.getSvcNoticeSbst());
            resultItem.setSvcNoticeSbst(originSbst);

            FileVO fileVO = new FileVO();
            fileVO.setFileContsSeq(resultItem.getSvcNoticeSeq());
            fileVO.setFileSe("SVC_NOTI");
            List<FileVO> flist = fileService.fileList(fileVO);

            // 개인정보 마스킹 (등록자 이름)
            resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

            svcNoticeService.updateSvcNoticeRetvNum(resultItem);
            resultItem.setRetvNum(resultItem.getRetvNum() + 1);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultItem);
            rsModel.setData("fileList",flist);
            rsModel.setResultType("latestNoticeDetail");
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getLatestOnlineNotice(HttpServletRequest request, ResultModel rsModel, String appType) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        ServiceNoticeVO item = new ServiceNoticeVO();
        item.setSvcNoticeType(svcSeq);
        item.setDispType(appType);
        item.setTargetType(CommonFnc.emptyCheckString("targetType", request));

        ServiceNoticeVO resultItem = svcNoticeService.latestOlSvcNoticeDetail(item);

        if (resultItem == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            FileVO fileVO = new FileVO();
            fileVO.setFileSeq(resultItem.getSvcNoticeSeq());
            fileVO.setFileSe("NOTI_POPUP");
            List<FileVO> popupImgFList = fileService.fileList(fileVO);

            fileVO.setFileSe("SVC_NOTI");
            List<FileVO> attachFList = fileService.fileList(fileVO);

            // 개인정보 마스킹 (등록자 이름)
            resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

            // 서비스 공지사항 내용에서 html 태그 원복 (&lt;, &amp; 등으로 치환된 코드를 태그 문자로 원복)
            String originSbst = CommonFnc.unescapeStr(resultItem.getSvcNoticeSbst());
            resultItem.setSvcNoticeSbst(originSbst);

            svcNoticeService.updateSvcNoticeRetvNum(item);
            resultItem.setRetvNum(resultItem.getRetvNum() + 1); 
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultItem);
            rsModel.setData("popupImgFileList",popupImgFList);
            rsModel.setData("attachFileList",attachFList);
            rsModel.setResultType("getLatestOlSvcNoticeDetail");
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO, String svcType) {
        String mbrSe = userVO.getMbrSe();
        if (svcType.equals("ON")) {
            return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe);
        }

        if (svcType.equals("")) {
            return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
        }

        return false;
    }

    private boolean isAllowNotice(MemberVO userVO, ServiceNoticeVO resultItem, String searchType) {
        /*
         * 서비스 공지사항 구분은 서비스 번호 값으로 로그인한 사용자의 서비스 번호가 다를 경우 접근 불가
         * 서비스 공지사항 구분 값이 0인 경우는 전체서비스 항목으로 접근 가능
         */
        if (!userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_MASTER) && !userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_CMS)
            && resultItem.getSvcNoticeType() != 0 && resultItem.getSvcNoticeType() != userVO.getSvcSeq()) {
            return false;
        }

        /*
         * 매장관리자는 비활성화된 서비스 공지사항은 접근 불가
         * 매장관리자는 자신의 매장관라지가 작성한 서비스 공지사항이 아닌 경우 edit 불가
         */
        if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)
            && ((resultItem.getDelYn().equals("Y")) || (searchType.equals("edit") && userVO.getStorSeq() != resultItem.getCretrMbrStorSeq()))) {
            return false;
        }

        return true;
    }

    private boolean isAllowOlSvcNotice(MemberVO userVO, int svcSeq) {
        if (svcSeq == 0 || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != svcSeq)) {
            return false;
        }

        return true;
    }

}
