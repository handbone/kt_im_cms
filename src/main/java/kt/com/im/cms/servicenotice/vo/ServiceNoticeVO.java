/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicenotice.vo;

import java.io.Serializable;
import java.util.List;

/**
*
* ServiceNoticeVO
*
 * @author A2TEC
 * @since 2018.06.20
 * @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 20.   A2TEC      최초생성
*
*
* </pre>
*/

public class ServiceNoticeVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8218798947251045577L;

    /** 서비스 공지사항 번호 */
    private int svcNoticeSeq;

    /** 서비스 공지사항 구분 */
    private int svcNoticeType;

    /** 서비스 공지사항 구분명 */
    private String svcNoticeTypeNm;

    /** 서비스 공지사항 제목 */
    private String svcNoticeTitle;

    /** 서비스 공지사항 내용 (noticeSbst: notice Substance) */
    private String svcNoticeSbst;

    /** 조회수 */
    private int retvNum;

    /** 첨부파일 수 */
    private int fileCount;

    /** 서비스 공지사항 게시 시작 일자 */
    private String stDt;

    /** 서비스 공지사항 게시 종료 일자 */
    private String fnsDt;

    /** 서비스 공지사항 등록 일시 */
    private String regDt;

    /** 등록자 이름 */
    private String cretrNm;

    /** 서비스 공지사항 팝업 노출 여부 */
    private String popupViewYn;

    /** 서비스 공지사항 삭제 여부 */
    private String delYn;

    /** 노출 범위 - 앱 구분 (ALL: 전체, SA: 서비스 앱, MA: 미러링 앱) */
    private String dispType;

    /** 단말 구분 - 공통코드 POST_TARGET_SE */
    private String targetType;

    /** 단말 구분 명 */
    private String targetTypeNm;

    /** 우선 순위 - 중요도 */
    private int prefRank;

    /** 서비스 공지사항 게시글 여부 */
    private String postViewYn;

    /** 서비스 공지사항 팝업 URL */
    private String popupUrl;

    /** 서비스 공지사항 팝업 기간 설정 여부 */
    private String popupPerdYn;

    /** 팝업 공개 시작 일시 */
    private String popupViewStDt;

    /** 팝업 공개 종료 일시 */
    private String popupViewFnsDt;

    /** 팝업 이미지 경로 */
    private String popupImgFilePath;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** 사용자 아이디 */
    private String loginId;

    /** 사용자 권한 */
    private String mbrSe;

    /** 등록자 권한 정보 */
    private String cretrMbrSe;

    /** 등록자 서비스 번호 */
    private int cretrMbrSvcSeq;

    /** 등록자 매장 번호 */
    private int cretrMbrStorSeq;

    /** 사용자 계정의 서비스 번호 */
    private int mbrSvcSeq;

    /** 사용자 계정의 매장 번호 */
    private int mbrStorSeq;

    /** 첨부파일 번호 */
    private int fileSeq;

    /** 첨부파일 저장경로 */
    private String filePath;

    /** 첨부파일 삭제 리스트 */
    private List<Integer> deleteFileSeqList;

    /** 서비스 공지사항 삭제 리스트 */
    private List<Integer> deleteSvcNoticeSeqList;

    /** 공통 코드 번호 */
    //int comnCdSeq;

    /** 공통 코드 분류 */
    //String comnCdCtg;

    /** 공통 코드 명 */
    //String comnCdNm;

    /** 공통 코드 값 */
    //String comnCdValue;

    /** 공통 코드 삭제 여부 */
    //String comnDelYn;

    public int getSvcNoticeSeq() {
        return svcNoticeSeq;
    }

    public void setSvcNoticeSeq(int svcNoticeSeq) {
        this.svcNoticeSeq = svcNoticeSeq;
    }

    public int getSvcNoticeType() {
        return svcNoticeType;
    }

    public void setSvcNoticeType(int svcNoticeType) {
        this.svcNoticeType = svcNoticeType;
    }

    public String getSvcNoticeTypeNm() {
        return svcNoticeTypeNm;
    }

    public void setSvcNoticeTypeNm(String svcNoticeTypeNm) {
        this.svcNoticeTypeNm = svcNoticeTypeNm;
    }

    public String getSvcNoticeTitle() {
        return svcNoticeTitle;
    }

    public void setSvcNoticeTitle(String svcNoticeTitle) {
        this.svcNoticeTitle = svcNoticeTitle;
    }

    public String getSvcNoticeSbst() {
        return svcNoticeSbst;
    }

    public void setSvcNoticeSbst(String svcNoticeSbst) {
        this.svcNoticeSbst = svcNoticeSbst;
    }

    public int getRetvNum() {
        return retvNum;
    }

    public void setRetvNum(int retvNum) {
        this.retvNum = retvNum;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public String getStDt() {
        return stDt;
    }

    public void setStDt(String stDt) {
        this.stDt = stDt;
    }

    public String getFnsDt() {
        return fnsDt;
    }

    public void setFnsDt(String fnsDt) {
        this.fnsDt = fnsDt;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getPopupViewYn() {
        return popupViewYn;
    }

    public void setPopupViewYn(String popupViewYn) {
        this.popupViewYn = popupViewYn;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getDispType() {
        return dispType;
    }

    public void setDispType(String dispType) {
        this.dispType = dispType;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getTargetTypeNm() {
        return targetTypeNm;
    }

    public void setTargetTypeNm(String targetTypeNm) {
        this.targetTypeNm = targetTypeNm;
    }

    public int getPrefRank() {
        return prefRank;
    }

    public void setPrefRank(int prefRank) {
        this.prefRank = prefRank;
    }

    public String getPostViewYn() {
        return postViewYn;
    }

    public void setPostViewYn(String postViewYn) {
        this.postViewYn = postViewYn;
    }

    public String getPopupUrl() {
        return popupUrl;
    }

    public void setPopupUrl(String popupUrl) {
        this.popupUrl = popupUrl;
    }

    public String getPopupPerdYn() {
        return popupPerdYn;
    }

    public void setPopupPerdYn(String popupPerdYn) {
        this.popupPerdYn = popupPerdYn;
    }

    public String getPopupViewStDt() {
        return popupViewStDt;
    }

    public void setPopupViewStDt(String popupViewStDt) {
        this.popupViewStDt = popupViewStDt;
    }

    public String getPopupViewFnsDt() {
        return popupViewFnsDt;
    }

    public void setPopupViewFnsDt(String popupViewFnsDt) {
        this.popupViewFnsDt = popupViewFnsDt;
    }

    public String getPopupImgFilePath() {
        return popupImgFilePath;
    }

    public void setPopupImgFilePath(String popupImgFilePath) {
        this.popupImgFilePath = popupImgFilePath;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getMbrSe() {
        return mbrSe;
    }

    public void setMbrSe(String mbrSe) {
        this.mbrSe = mbrSe;
    }

    public String getCretrMbrSe() {
        return cretrMbrSe;
    }

    public void setCretrMbrSe(String cretrMbrSe) {
        this.cretrMbrSe = cretrMbrSe;
    }

    public int getCretrMbrSvcSeq() {
        return cretrMbrSvcSeq;
    }

    public void setCretrMbrSvcSeq(int cretrMbrSvcSeq) {
        this.cretrMbrSvcSeq = cretrMbrSvcSeq;
    }

    public int getCretrMbrStorSeq() {
        return cretrMbrStorSeq;
    }

    public void setCretrMbrStorSeq(int cretrMbrStorSeq) {
        this.cretrMbrStorSeq = cretrMbrStorSeq;
    }

    public int getMbrSvcSeq() {
        return mbrSvcSeq;
    }

    public void setMbrSvcSeq(int mbrSvcSeq) {
        this.mbrSvcSeq = mbrSvcSeq;
    }

    public int getMbrStorSeq() {
        return mbrStorSeq;
    }

    public void setMbrStorSeq(int mbrStorSeq) {
        this.mbrStorSeq = mbrStorSeq;
    }

    public int getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(int fileSeq) {
        this.fileSeq = fileSeq;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<Integer> getDeleteFileSeqList() {
        return deleteFileSeqList;
    }

    public void setDeleteFileSeqList(List<Integer> deleteFileSeqList) {
        this.deleteFileSeqList = deleteFileSeqList;
    }

    public List<Integer> getDeleteSvcNoticeSeqList() {
        return deleteSvcNoticeSeqList;
    }

    public void setDeleteSvcNoticeSeqList(List<Integer> deleteSvcNoticeSeqList) {
        this.deleteSvcNoticeSeqList = deleteSvcNoticeSeqList;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
