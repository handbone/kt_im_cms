/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.release.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * CMS 편성 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 13.   정규현      최초생성
 *
 *
 *      </pre>
 */

public class ReleaseVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -3396729819964436549L;

    /** 콘텐츠 카테고리 명(첫번째) */
    private String firstCtgNm;

    /** 콘텐츠 카테고리 명(두번째) */
    private String secondCtgNm;

    /** 생성 일시 */
    private String cretDt;

    /** 서비스 명 */
    private String svcNm;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** 삭제 여부 */
    private String delYn;

    /** 콘텐츠 목록 유형 판별 */
    private String listType;

    /** 리스트 */
    private List list;

    /** 매장 번호 */
    private int storSeq;

    /** 편성 콘텐츠 개수 */
    private int contsCount;

    /** 매장 명 */
    private String storNm;

    /** 로그인 아이디 */
    private String loginId;

    /** 활성화 여부 */
    private String useYn;

    /** 검색 유무 */
    private String searchConfirm;

    /** 검색 키워드 */
    private String keyword;

    /** 페이지 값 */
    private int offset;

    /** 리스트 개수 */
    private int limit;

    /** 정렬 필드 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 조건(카테고리) */
    private String target;

    /** 장비 번호 */
    private int devSeq;

    /** 편성 번호 */
    private int frmtnSeq;

    /** 편성 명 */
    private String frmtnNm;

    /** 등록자 */
    private String cretrId;

    /** 서비스 번호 */
    private int svcSeq;

    /** 콘텐츠 편성 번호 */
    private int frmtnSetSeq;

    /** 콘텐츠 번호 */
    private int contsSeq;

    /** 편성 상태 */
    private String frmtnSttus;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 편성 콘텐츠 번호 */
    private int frmtnContsSeq;

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAamdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getoffset() {
        return offset;
    }

    public void setoffset(int offset) {
        this.offset = offset;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public int getFrmtnSeq() {
        return frmtnSeq;
    }

    public void setFrmtnSeq(int frmtnSeq) {
        this.frmtnSeq = frmtnSeq;
    }

    public String getFrmtnNm() {
        return frmtnNm;
    }

    public void setFrmtnNm(String frmtnNm) {
        this.frmtnNm = frmtnNm;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public int getFrmtnSetSeq() {
        return frmtnSetSeq;
    }

    public void setFrmtnSetSeq(int frmtnSetSeq) {
        this.frmtnSetSeq = frmtnSetSeq;
    }

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public String getFrmtnSttus() {
        return frmtnSttus;
    }

    public void setFrmtnSttus(String frmtnSttus) {
        this.frmtnSttus = frmtnSttus;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public int getFrmtnContsSeq() {
        return frmtnContsSeq;
    }

    public void setFrmtnContsSeq(int frmtnContsSeq) {
        this.frmtnContsSeq = frmtnContsSeq;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getlimit() {
        return limit;
    }

    public void setlimit(int limit) {
        this.limit = limit;
    }

    public String getFirstCtgNm() {
        return firstCtgNm;
    }

    public void setFirstCtgNm(String firstCtgNm) {
        this.firstCtgNm = firstCtgNm;
    }

    public String getSecondCtgNm() {
        return secondCtgNm;
    }

    public void setSecondCtgNm(String secondCtgNm) {
        this.secondCtgNm = secondCtgNm;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public int getDevSeq() {
        return devSeq;
    }

    public void setDevSeq(int devSeq) {
        this.devSeq = devSeq;
    }

    public int getContsCount() {
        return contsCount;
    }

    public void setContsCount(int contsCount) {
        this.contsCount = contsCount;
    }

}
