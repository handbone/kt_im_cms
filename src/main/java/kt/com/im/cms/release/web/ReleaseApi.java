/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.release.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.release.service.ReleaseService;
import kt.com.im.cms.release.vo.ReleaseVO;

/**
 *
 * CMS 편성 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 13.   정규현      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class ReleaseApi {

    private final static String viewName = "../resources/api/release/releaseProcess";

    @Resource(name = "ReleaseService")
    private ReleaseService releaseService;

    @Autowired
    private DataSourceTransactionManager transactionManager;

    /**
     * 편성 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/cms/release")
    public ModelAndView releaseProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 로그인 되지 않았거나 Master, CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAdministrator = this.isAdministrator(userVO);
        if (userVO == null || !isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ReleaseVO item = new ReleaseVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                // frmtnNm svcSeq contsSeqs frmtnSeq data.result.frmtnSeq
                String checkString = CommonFnc.requiredChecking(request, "frmtnNm,frmtnSeq,svcSeq,contsSeqs");
                item.setAmdrId((String) session.getAttribute("id"));

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                int frmtnSeq = CommonFnc.emptyCheckInt("frmtnSeq", request);

                String frmtnNm = CommonFnc.emptyCheckString("frmtnNm", request);
                item.setSvcSeq(svcSeq);
                item.setFrmtnSeq(frmtnSeq);
                item.setFrmtnNm(frmtnNm);
                item.setSvcSeq(svcSeq);

                try {
                    releaseService.releaseUpdate(item);

                    /** 제외 코드값 */
                    String contsSeqs = CommonFnc.emptyCheckString("contsSeqs", request);

                    String[] contsSeq = contsSeqs.split(",");

                    List<Integer> list = new ArrayList<Integer>();
                    for (int i = 0; i < contsSeq.length; i++) {
                        list.add(Integer.parseInt(contsSeq[i]));
                    }
                    item.setList(list);

                    releaseService.releaseContsInsert(item);

                    releaseService.releaseContsDelete(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                    transactionManager.commit(status);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("releaseUpdate");
                return rsModel.getModelAndView();

            } else if (Method.equals("DELETE")) { // delete
                /* Checking Mendatory S */
                String checkString = CommonFnc.requiredChecking(request, "frmtnSeqs");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                /* Checking Mendatory E */

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    String frmtnSeqs = request.getParameter("frmtnSeqs");
                    String[] frmtnSeq = frmtnSeqs.split(",");

                    List<Integer> list = new ArrayList<Integer>();
                    for (int i = 0; i < frmtnSeq.length; i++) {
                        list.add(Integer.parseInt(frmtnSeq[i]));
                    }
                    item.setList(list);
                    item.setDelYn("Y");
                    int result = releaseService.releaseDelete(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                    transactionManager.commit(status);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("releaseDelete");
                return rsModel.getModelAndView();
            } else { // post
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                // frmtnNm svcSeq contsSeqs frmtnSeq data.result.frmtnSeq
                String checkString = CommonFnc.requiredChecking(request, "frmtnNm,svcSeq,contsSeqs");
                item.setCretrId((String) session.getAttribute("id"));

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                String frmtnNm = CommonFnc.emptyCheckString("frmtnNm", request);
                item.setSvcSeq(svcSeq);
                item.setFrmtnNm(frmtnNm);

                item.setFrmtnNm(frmtnNm);
                item.setSvcSeq(svcSeq);
                try {
                    int result = releaseService.releaseInsert(item);

                    if (result == 1) {
                        item.setCretrId((String) session.getAttribute("id"));

                        /** 제외 코드값 */
                        String contsSeqs = CommonFnc.emptyCheckString("contsSeqs", request);

                        String[] contsSeq = contsSeqs.split(",");

                        List<Integer> list = new ArrayList<Integer>();
                        for (int i = 0; i < contsSeq.length; i++) {
                            list.add(Integer.parseInt(contsSeq[i]));
                        }
                        item.setList(list);

                        result = releaseService.releaseContsInsert(item);

                        if (result != 0) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        }

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                        transactionManager.commit(status);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("releaseInsert");
                return rsModel.getModelAndView();
            }
        } else { // get
            int frmtnSeq = CommonFnc.emptyCheckInt("frmtnSeq", request);
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            String frmtnNm = CommonFnc.emptyCheckString("frmtnNm", request);
            item.setSvcSeq(svcSeq);
            item.setFrmtnNm(frmtnNm);
            if (frmtnSeq != 0 || !frmtnNm.equals("")) {
                item.setFrmtnSeq(frmtnSeq);

                /** 편성 그룹 정보 */
                ReleaseVO resultItem = releaseService.releaseInfo(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (수정자 아이디) */
                    resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));

                    // 편성콘텐츠 정보를 가져올 경우 서비스 정보도 확인하여 편성에 포함되어 있으나 서비스가 변경되었을 경우에는 가져오지 않도록 하기 위함.
                    item.setSvcSeq(resultItem.getSvcSeq());

                    /** 편성 그룹 콘텐츠 정보 */
                    List<ReleaseVO> contsList = releaseService.releaseContsList(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("contsList", contsList);

                }
                rsModel.setResultType("releaseInfo");
                return rsModel.getModelAndView();
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (frmtnSeq == 0 && CommonFnc.checkReqParameter(request, "frmtnSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                String searchType = request.getParameter("searchType") == null ? ""
                        : request.getParameter("searchType");

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    item.setTarget(searchField);
                    item.setKeyword(searchString);

                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("FRMTN_NM")) {
                } else {
                    sidx = "CRET_DT";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setoffset(page);
                item.setlimit(pageEnd);

                List<ReleaseVO> resultItem = releaseService.releaseList(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                        resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));
                    }

                    int totalCount = releaseService.releaseListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", (totalCount - 1) / limit);
                    rsModel.setResultType("releaseList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 편성 이력 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/cms/releaseRecord")
    public ModelAndView releaseRecordProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 로그인 되지 않았거나 Master, CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAdministrator = this.isAdministrator(userVO);
        if (userVO == null || !isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ReleaseVO item = new ReleaseVO();

        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            String checkString = CommonFnc.requiredChecking(request, "frmtnSeq");

            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            int frmtnSeq = CommonFnc.emptyCheckInt("frmtnSeq", request);

            // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
            if (frmtnSeq == 0 && CommonFnc.checkReqParameter(request, "frmtnSeq")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }

            item.setFrmtnSeq(frmtnSeq);
            item.setFrmtnNm("");
            item.setSvcSeq(0);
            ReleaseVO resultItem = releaseService.releaseInfo(item);
            List<ReleaseVO> resultContsItem = releaseService.releaseRecordList(item);

            if (resultItem == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
                rsModel.setData("contsList", resultContsItem);
            }
            rsModel.setResultType("releaseRecordList");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 편성 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/mms/release")
    public ModelAndView mmsReleaseProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/release/mmsReleaseProcess");

        // 로그인 되지 않았거나 검수자, CP사 관례자인 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ReleaseVO item = new ReleaseVO();

        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                // frmtnNm svcSeq contsSeqs frmtnSeq data.result.frmtnSeq
                String checkString = CommonFnc.requiredChecking(request, "frmtnNm,frmtnSeq,storSeq,contsSeqs");
                item.setAmdrId((String) session.getAttribute("id"));

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                int frmtnSeq = CommonFnc.emptyCheckInt("frmtnSeq", request);

                String frmtnNm = CommonFnc.emptyCheckString("frmtnNm", request);
                item.setStorSeq(storSeq);
                item.setFrmtnSeq(frmtnSeq);
                item.setFrmtnNm(frmtnNm);

                try {
                    releaseService.mmsReleaseUpdate(item);

                    /** 제외 코드값 */
                    String contsSeqs = CommonFnc.emptyCheckString("contsSeqs", request);

                    String[] contsSeq = contsSeqs.split(",");

                    List<Integer> list = new ArrayList<Integer>();
                    for (int i = 0; i < contsSeq.length; i++) {
                        list.add(Integer.parseInt(contsSeq[i]));
                    }
                    item.setList(list);

                    releaseService.mmsReleaseContsInsert(item);

                    releaseService.mmsReleaseContsDelete(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                    transactionManager.commit(status);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("releaseUpdate");
                return rsModel.getModelAndView();

            } else if (Method.equals("DELETE")) { // delete
                /* Checking Mendatory S */
                String checkString = CommonFnc.requiredChecking(request, "frmtnSeqs");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                /* Checking Mendatory E */

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    String frmtnSeqs = request.getParameter("frmtnSeqs");
                    String[] frmtnSeq = frmtnSeqs.split(",");

                    List<Integer> list = new ArrayList<Integer>();
                    for (int i = 0; i < frmtnSeq.length; i++) {
                        list.add(Integer.parseInt(frmtnSeq[i]));
                    }
                    item.setList(list);
                    item.setDelYn("Y");
                    int result = releaseService.mmsReleaseDelete(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                    transactionManager.commit(status);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("releaseDelete");
                return rsModel.getModelAndView();
            } else { // post
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                // frmtnNm svcSeq contsSeqs frmtnSeq data.result.frmtnSeq
                String checkString = CommonFnc.requiredChecking(request, "frmtnNm,storSeq,contsSeqs");
                item.setCretrId((String) session.getAttribute("id"));

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                String frmtnNm = CommonFnc.emptyCheckString("frmtnNm", request);
                item.setFrmtnNm(frmtnNm);

                item.setFrmtnNm(frmtnNm);
                item.setStorSeq(storSeq);
                try {
                    int result = releaseService.mmsReleaseInsert(item);

                    if (result == 1) {
                        item.setCretrId((String) session.getAttribute("id"));

                        /** 제외 코드값 */
                        String contsSeqs = CommonFnc.emptyCheckString("contsSeqs", request);

                        String[] contsSeq = contsSeqs.split(",");

                        List<Integer> list = new ArrayList<Integer>();
                        for (int i = 0; i < contsSeq.length; i++) {
                            list.add(Integer.parseInt(contsSeq[i]));
                        }
                        item.setList(list);

                        result = releaseService.mmsReleaseContsInsert(item);

                        if (result != 0) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        }

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                        transactionManager.commit(status);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("releaseInsert");
                return rsModel.getModelAndView();
            }
        } else { // get
            int frmtnSeq = CommonFnc.emptyCheckInt("frmtnSeq", request);
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String frmtnNm = CommonFnc.emptyCheckString("frmtnNm", request);
            item.setSvcSeq(svcSeq);
            item.setFrmtnNm(frmtnNm);
            item.setStorSeq(storSeq);
            if (frmtnSeq != 0 || !frmtnNm.equals("")) {
                item.setFrmtnSeq(frmtnSeq);

                /** 편성 그룹 정보 */
                ReleaseVO resultItem = releaseService.mmsReleaseInfo(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != resultItem.getSvcSeq())
                            || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE) && userVO.getStorSeq() != resultItem.getStorSeq())
                            || resultItem.getDelYn().equals("Y")) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                    resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));

                    // 편성콘텐츠 정보를 가져올 경우 서비스 정보도 확인하여 편성에 포함되어 있으나 서비스가 변경되었을 경우에는 가져오지 않도록 하기 위함.
                    item.setSvcSeq(resultItem.getSvcSeq());

                    /** 편성 그룹 콘텐츠 정보 */
                    List<ReleaseVO> contsList = releaseService.mmsReleaseContsList(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("contsList", contsList);

                }
                rsModel.setResultType("releaseInfo");
                return rsModel.getModelAndView();
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (frmtnSeq == 0 && CommonFnc.checkReqParameter(request, "frmtnSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                String searchType = request.getParameter("searchType") == null ? ""
                        : request.getParameter("searchType");

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    item.setTarget(searchField);
                    item.setKeyword(searchString);

                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("FRMTN_NM")) {
                } else if (sidx.equals("STOR_NM")) {
                } else if (sidx.equals("FRMTN_SEQ")) {
                } else {
                    sidx = "AMD_DT";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setoffset(page);
                item.setlimit(pageEnd);

                List<ReleaseVO> resultItem = releaseService.mmsReleaseList(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    int totalCount = releaseService.mmsReleaseListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", (totalCount - 1) / limit);
                    rsModel.setResultType("releaseList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 편성 동기화 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/releaseSync")
    public ModelAndView releaseSyncProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 로그인 되지 않았거나 검수자, CP사 관계자인 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ReleaseVO item = new ReleaseVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                List<ReleaseVO> result = new ArrayList<ReleaseVO>();
                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                String overlap = CommonFnc.emptyCheckString("overlap", request);
                /* Checking Mendatory S */
                String checkString = CommonFnc.requiredChecking(request, "svcSeq,storSeq");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                /* Checking Mendatory E */

                /** 검색 조건 변수 */
                ReleaseVO searchItem = new ReleaseVO();

                /** CMS 서비스 별 그룹 조회 */
                searchItem.setSearchConfirm("false");
                searchItem.setlimit(10000);
                searchItem.setoffset(1);
                searchItem.setSvcSeq(svcSeq);
                searchItem.setSidx("CRET_DT");
                searchItem.setSord("desc");
                searchItem.setSvcSeq(svcSeq);
                searchItem.setFrmtnNm("");

                List<ReleaseVO> cmsReleaseItem = releaseService.releaseList(searchItem);

                // 중복체크 S
                if (overlap == "") {
                    List<ReleaseVO> resultOverLap = new ArrayList<ReleaseVO>();
                    for (int i = 0; i < cmsReleaseItem.size(); i++) {
                        item = new ReleaseVO();
                        ReleaseVO resItem = cmsReleaseItem.get(i);
                        item.setFrmtnNm(resItem.getFrmtnNm());
                        item.setStorSeq(storSeq);
                        resItem = releaseService.mmsReleaseInfo(item);
                        int frmtnSeq = 0;
                        if (resItem != null) {
                            frmtnSeq = resItem.getFrmtnSeq();
                        }

                        if (frmtnSeq != 0) {
                            item.setFrmtnSeq(frmtnSeq);
                            resultOverLap.add(item);
                        }
                    }
                    if (resultOverLap.size() != 0) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_OVERLAP_RELEASE);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("resultOverLap", resultOverLap);
                        rsModel.setResultType("releaseOverlap");
                        return rsModel.getModelAndView();
                    }
                }

                // 중복체크 E
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    for (int i = 0; i < cmsReleaseItem.size(); i++) {
                        boolean overLap = true;
                        item = new ReleaseVO();
                        ReleaseVO resItem = cmsReleaseItem.get(i);
                        item.setFrmtnNm(resItem.getFrmtnNm());
                        item.setStorSeq(storSeq);
                        item.setFrmtnSeq(resItem.getFrmtnSeq());
                        item.setSvcSeq(svcSeq);

                        /** CMS 그룹내 컨텐츠 검색 */
                        List<ReleaseVO> contsList = releaseService.releaseContsList(item);

                        // MMS 내에서 검색
                        item.setFrmtnSeq(0);
                        int frmtnSeq = 0;
                        ReleaseVO resultValue = releaseService.mmsReleaseInfo(item);
                        if (resultValue != null) {
                            frmtnSeq = resultValue.getFrmtnSeq();
                        }

                        if (frmtnSeq == 0) {
                            resItem.setStorSeq(storSeq);
                            releaseService.mmsReleaseInsert(resItem);
                            frmtnSeq = resItem.getFrmtnSeq();
                        } else {
                            if (overlap.equals("Y")) {
                                item.setFrmtnSeq(frmtnSeq);
                                item.setList(null);
                                releaseService.mmsReleaseContsDelete(item);
                            } else {
                                overLap = false;
                            }
                        }
                        if (overLap) {
                            List<Integer> list = new ArrayList<Integer>();
                            item = new ReleaseVO();
                            item.setFrmtnSeq(frmtnSeq);
                            for (int j = 0; j < contsList.size(); j++) {
                                ReleaseVO jcase = contsList.get(j);
                                int cSeq = jcase.getContsSeq();
                                list.add(cSeq);

                            }

                            if (list.size() != 0) {
                                item.setList(list);
                                releaseService.mmsReleaseContsInsert(item);
                            }
                        }
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setResultType("releaseSync");
                    transactionManager.commit(status);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    private boolean isNotAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

}
