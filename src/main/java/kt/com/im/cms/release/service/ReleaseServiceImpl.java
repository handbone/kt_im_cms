/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.release.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.release.dao.ReleaseDAO;
import kt.com.im.cms.release.vo.ReleaseVO;

/**
 *
 * CMS 편성 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 13.   정규현      최초생성
* 2018. 6. 22.   정규현      mmsReleaseList,mmsReleaseListTotalCount 추가
 *
 *
 *      </pre>
 */

@Service("ReleaseService")
public class ReleaseServiceImpl implements ReleaseService {
    @Resource(name = "ReleaseDAO")
    private ReleaseDAO ReleaseDAO;

    /**
     * 편성 그룹 상세 정보
     *
     * @param ReleaseVO
     * @return 편성 그룹 상세 정보
     */
    @Override
    public ReleaseVO releaseInfo(ReleaseVO data) {
        return ReleaseDAO.releaseInfo(data);
    }

    /**
     * 검색조건에 해당되는 편성 그룹 리스트 정보
     *
     * @param ReleaseVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ReleaseVO> releaseList(ReleaseVO data) {
        return ReleaseDAO.releaseList(data);
    }

    /**
     * 검색조건에 해당되는 편성 그룹 리스트 페이징 정보
     *
     * @param ReleaseVO
     * @return 페이징 정보 조회 결과
     */
    @Override
    public int releaseListTotalCount(ReleaseVO data) {
        int res = ReleaseDAO.releaseListTotalCount(data);
        return res;
    }

    /**
     * 편성 그룹 콘텐츠 목록
     *
     * @param ReleaseVO
     * @return 편성 그룹 콘텐츠 목록 조회 결과
     */
    @Override
    public List<ReleaseVO> releaseContsList(ReleaseVO data) {
        return ReleaseDAO.releaseContsList(data);
    }

    /**
     * 편성 그룹 등록
     *
     * @param ReleaseVO
     * @return 편성 그룹 등록 성공 여부
     */
    @Override
    public int releaseInsert(ReleaseVO data) {
        return ReleaseDAO.releaseInsert(data);
    }

    /**
     * 편성 그룹 콘텐츠 등록
     *
     * @param ReleaseVO
     * @return 편성 그룹 콘텐츠 등록 성공 여부
     */
    @Override
    public int releaseContsInsert(ReleaseVO data) {
        return ReleaseDAO.releaseContsInsert(data);
    }

    /**
     * 편성 그룹 수정
     *
     * @param ReleaseVO
     * @return 편성 그룹 정보 수정 성공 여부
     */
    @Override
    public int releaseUpdate(ReleaseVO data) {
        return ReleaseDAO.releaseUpdate(data);
    }

    /**
     * 편성 그룹 콘텐츠 삭제
     *
     * @param ReleaseVO
     * @return 편성 그룹 콘텐츠 삭제 성공 여부
     */
    @Override
    public int releaseContsDelete(ReleaseVO data) {
        return ReleaseDAO.releaseContsDelete(data);
    }

    /**
     * 편성 그룹 삭제
     *
     * @param ReleaseVO
     * @return 편성 그룹 삭제 성공 여부
     */
    @Override
    public int releaseDelete(ReleaseVO data) {
        return ReleaseDAO.releaseDelete(data);
    }

    /**
     * 편성 검수 이력 리스트
     *
     * @param ReleaseVO
     * @return 편성 검수 이력 리스트 조회 결과
     */
    @Override
    public List<ReleaseVO> releaseRecordList(ReleaseVO data) {
        return ReleaseDAO.releaseRecordList(data);
    }

    /**
     * 매장 편성 리스트
     *
     * @param ReleaseVO
     * @return 매장 편성 리스트 조회 결과
     */
    @Override
    public List<ReleaseVO> mmsReleaseList(ReleaseVO data) {
        return ReleaseDAO.mmsReleaseList(data);
    }

    /**
     * 매장 편성 페이징 정보
     *
     * @param ReleaseVO
     * @return 매장 편성 페이징 정보 조회 결과
     */
    @Override
    public int mmsReleaseListTotalCount(ReleaseVO data) {
        int res = ReleaseDAO.mmsReleaseListTotalCount(data);
        return res;
    }

    /**
     * 매장 편성 그룹 등록
     *
     * @param ReleaseVO
     * @return 매장 편성 그룹 등록 성공 여부
     */
    @Override
    public int mmsReleaseInsert(ReleaseVO data) {
        return ReleaseDAO.mmsReleaseInsert(data);
    }

    /**
     * 매장 편성 그룹 콘텐츠 등록
     *
     * @param ReleaseVO
     * @return 매장 편성 그룹 콘텐츠 등록 성공 여부
     */
    @Override
    public int mmsReleaseContsInsert(ReleaseVO data) {
        return ReleaseDAO.mmsReleaseContsInsert(data);
    }

    /**
     * 매장 편성 그룹 상세 정보
     *
     * @param ReleaseVO
     * @return 매장 편성 그룹 상세 정보
     */
    @Override
    public ReleaseVO mmsReleaseInfo(ReleaseVO data) {
        return ReleaseDAO.mmsReleaseInfo(data);
    }

    /**
     * 매장 편성 그룹 콘텐츠 목록
     *
     * @param ReleaseVO
     * @return 매장 편성 그룹 콘텐츠 목록 조회 결과
     */
    @Override
    public List<ReleaseVO> mmsReleaseContsList(ReleaseVO data) {
        return ReleaseDAO.mmsReleaseContsList(data);
    }

    /**
     * 매장 편성 그룹 수정
     *
     * @param ReleaseVO
     * @return 매장 편성 그룹 정보 수정 성공 여부
     */
    @Override
    public int mmsReleaseUpdate(ReleaseVO data) {
        return ReleaseDAO.mmsReleaseUpdate(data);
    }

    /**
     * 매장 편성 그룹 콘텐츠 삭제
     *
     * @param ReleaseVO
     * @return 매장 편성 그룹 콘텐츠 삭제 성공 여부
     */
    @Override
    public int mmsReleaseContsDelete(ReleaseVO data) {
        return ReleaseDAO.mmsReleaseContsDelete(data);
    }

    /**
     * 매장 편성 그룹 삭제
     *
     * @param ReleaseVO
     * @return 매장 편성 그룹 삭제 성공 여부
     */
    @Override
    public int mmsReleaseDelete(ReleaseVO data) {
        return ReleaseDAO.mmsReleaseDelete(data);
    }

}
