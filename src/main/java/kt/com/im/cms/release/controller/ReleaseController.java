/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.release.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;

/**
 *
 * CMS 편성 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 13.   정규현      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class ReleaseController {

    /**
     * CMS 편성 리스트 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/cms/release", method = RequestMethod.GET)
    public ModelAndView releaseList(ModelAndView mv) {
        mv.setViewName("/views/release/cms/ReleaseList");
        return mv;
    }

    /**
     * CMS 편성 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/cms/release/regist", method = RequestMethod.GET)
    public ModelAndView releaseRegist(ModelAndView mv, HttpServletRequest request) {
        mv.addObject("svcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.setViewName("/views/release/cms/ReleaseRegist");
        return mv;
    }

    /**
     * CMS 편성 관리 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/cms/release/{frmtnSeq}/edit", method = RequestMethod.GET)
    public ModelAndView releaseEdit(ModelAndView mv, @PathVariable(value = "frmtnSeq") String frmtnSeq) {
        mv.addObject("frmtnSeq", frmtnSeq);
        mv.setViewName("/views/release/cms/ReleaseEdit");
        return mv;
    }

    /**
     * CMS 편성 이력 상세 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/cms/release/{frmtnSeq}/stat", method = RequestMethod.GET)
    public ModelAndView releaseStat(ModelAndView mv, @PathVariable(value = "frmtnSeq") String frmtnSeq) {
        mv.addObject("frmtnSeq", frmtnSeq);
        mv.setViewName("/views/release/cms/ReleaseHistory");
        return mv;
    }

    /**
     * 매장 콘텐츠 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/mms/release/regist", method = RequestMethod.GET)
    public ModelAndView mmsReleaseRegist(ModelAndView mv) {
        mv.setViewName("/views/service/contents/ReleaseRegist");
        return mv;
    }

    /**
     * 매장 콘텐츠 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/mms/release/{frmtnSeq}", method = RequestMethod.GET)
    public ModelAndView mmsReleaseRegist(ModelAndView mv, @PathVariable(value = "frmtnSeq") String frmtnSeq) {
        mv.addObject("frmtnSeq", frmtnSeq);
        mv.setViewName("/views/service/contents/ReleaseEdit");
        return mv;
    }
}