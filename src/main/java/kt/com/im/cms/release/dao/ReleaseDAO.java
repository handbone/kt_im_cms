/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.release.dao;

import java.util.List;

import kt.com.im.cms.release.vo.ReleaseVO;

/**
 *
 * CMS 편성 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 13.   정규현      최초생성
 *
 *
 *      </pre>
 */

public interface ReleaseDAO {

    ReleaseVO releaseInfo(ReleaseVO data);

    List<ReleaseVO> releaseList(ReleaseVO data);

    int releaseListTotalCount(ReleaseVO data);

    List<ReleaseVO> releaseContsList(ReleaseVO data);

    int releaseInsert(ReleaseVO data);

    int releaseContsInsert(ReleaseVO data);

    int releaseContsDelete(ReleaseVO data);

    int releaseUpdate(ReleaseVO data);

    int releaseDelete(ReleaseVO data);

    List<ReleaseVO> releaseRecordList(ReleaseVO data);

    int mmsReleaseListTotalCount(ReleaseVO data);

    List<ReleaseVO> mmsReleaseList(ReleaseVO data);

    int mmsReleaseInsert(ReleaseVO data);

    int mmsReleaseContsInsert(ReleaseVO data);

    ReleaseVO mmsReleaseInfo(ReleaseVO data);

    List<ReleaseVO> mmsReleaseContsList(ReleaseVO data);

    int mmsReleaseUpdate(ReleaseVO data);

    int mmsReleaseContsDelete(ReleaseVO data);

    int mmsReleaseDelete(ReleaseVO data);
}