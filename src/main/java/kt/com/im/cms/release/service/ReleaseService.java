/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.release.service;

import java.util.List;

import kt.com.im.cms.release.vo.ReleaseVO;

/**
 *
 * CMS 편성 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 13.   정규현      최초생성
 *
 *
 *      </pre>
 */

public interface ReleaseService {

    List<ReleaseVO> releaseList(ReleaseVO item);

    int releaseListTotalCount(ReleaseVO item);

    ReleaseVO releaseInfo(ReleaseVO item);

    List<ReleaseVO> releaseContsList(ReleaseVO item);

    int releaseInsert(ReleaseVO item);

    int releaseContsInsert(ReleaseVO item);

    int releaseUpdate(ReleaseVO item);

    int releaseContsDelete(ReleaseVO item);

    int releaseDelete(ReleaseVO item);

    List<ReleaseVO> releaseRecordList(ReleaseVO item);

    List<ReleaseVO> mmsReleaseList(ReleaseVO item);

    int mmsReleaseListTotalCount(ReleaseVO item);

    int mmsReleaseInsert(ReleaseVO item);

    int mmsReleaseContsInsert(ReleaseVO item);

    ReleaseVO mmsReleaseInfo(ReleaseVO item);

    List<ReleaseVO> mmsReleaseContsList(ReleaseVO item);

    int mmsReleaseUpdate(ReleaseVO item);

    int mmsReleaseContsDelete(ReleaseVO item);

    int mmsReleaseDelete(ReleaseVO item);

}
