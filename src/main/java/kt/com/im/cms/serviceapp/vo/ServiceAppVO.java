/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceapp.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * ServiceAppVO
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 04. 18.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class ServiceAppVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1823365142196674189L;

    /** app 관리 번호 */
    private int appSeq;

    /** 서비스 번호 */
    private int svcSeq;

    /** 서비스 명 */
    private String svcNm;

    /** 구분 (App/콘텐츠 구분) */
    private String type;

    /** app 명(또는 콘텐츠 명) */
    private String appNm;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 패키지 명 */
    private String pkgNm;

    /** 스토어 URL 정보 */
    private String storUrl;

    /** uri 정보 */
    private String uri;

    /** 메인 이미지 경로 */
    private String appImgPath;

    /** 노출 순서 */
    private int orderNum;

    /** 등록자 아이디 */
    private String cretrId;

    /** 등록 일시 */
    private String cretDt;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** 생성 일시 */
    private String regDt;

    /** 삭제한 App 고유 번호 */
    private int delSvcAppSeq;

    /** 검색 요청 구분 (빈 값일 경우 내부 API 요청, launcher 일 경우 외부 API 요청) */
    private String searchType;

    /** App 삭제 리스트 */
    private List<Integer> delSvcAppList;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** 앱 이미지 파일 존재 여부 */
    private String isFile;

    public int getAppSeq() {
        return appSeq;
    }

    public void setAppSeq(int appSeq) {
        this.appSeq = appSeq;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAppNm() {
        return appNm;
    }

    public void setAppNm(String appNm) {
        this.appNm = appNm;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public String getPkgNm() {
        return pkgNm;
    }

    public void setPkgNm(String pkgNm) {
        this.pkgNm = pkgNm;
    }

    public String getStorUrl() {
        return storUrl;
    }

    public void setStorUrl(String storUrl) {
        this.storUrl = storUrl;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getAppImgPath() {
        return appImgPath;
    }

    public void setAppImgPath(String appImgPath) {
        this.appImgPath = appImgPath;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public int getDelSvcAppSeq() {
        return delSvcAppSeq;
    }

    public void setDelSvcAppSeq(int delSvcAppSeq) {
        this.delSvcAppSeq = delSvcAppSeq;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public List<Integer> getDelSvcAppList() {
        return delSvcAppList;
    }

    public void setDelSvcAppList(List<Integer> delSvcAppList) {
        this.delSvcAppList = delSvcAppList;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public String getIsFile() {
        return isFile;
    }

    public void setIsFile(String isFile) {
        this.isFile = isFile;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
