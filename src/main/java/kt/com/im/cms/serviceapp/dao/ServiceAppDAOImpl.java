/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceapp.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.serviceapp.vo.ServiceAppVO;

/**
 *
 * 온라인 서비스 App 관리 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 04. 18.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("ServiceAppDAO")
public class ServiceAppDAOImpl extends mysqlAbstractMapper implements ServiceAppDAO {

    /**
     * 온라인 서비스 app 정보 등록
     * @param ServiceAppVO
     * @return 등록 결과
     */
    @Override
    public int insertSvcApp(ServiceAppVO vo) throws Exception {
        return insert("ServiceAppDAO.insertSvcApp", vo);
    }

    /**
     * 온라인 서비스 app 정보 수정
     * @param ServiceAppVO
     * @return 수정 결과
     */
    @Override
    public int updateSvcApp(ServiceAppVO vo) throws Exception {
        return update("ServiceAppDAO.updateSvcApp", vo);
    }

    /**
     * 온라인 서비스 app 목록 조회
     * @param ServiceAppVO
     * @return 검색 조건에 부합하는 온라인 서비스 app 목록
     */
    @Override
    public List<ServiceAppVO> svcAppList(ServiceAppVO vo) throws Exception {
        return selectList("ServiceAppDAO.selectSvcAppList", vo);
    }

    /**
     * 온라인 서비스 app 목록 합계 조회
     * @param ServiceAppVO
     * @return 검색 조건에 부합하는 온라인 서비스 app 목록 합계
     */
    @Override
    public int svcAppListTotalCount(ServiceAppVO vo) throws Exception {
         return selectOne("ServiceAppDAO.selectSvcAppListTotalCount", vo);
    }

    /**
     * 온라인 서비스 app 상세 정보 조회
     * @param ServiceAppVO
     * @return 검색 조건에 부합하는 온라인 서비스 app 상세 정보
     */
    @Override
    public ServiceAppVO svcAppDetail(ServiceAppVO vo) throws Exception {
        return selectOne("ServiceAppDAO.selectSvcAppDetail", vo);
    }

    /**
     * 온라인 서비스 app 중 마지막 노출 순서의 app 정보 조회
     * @param ServiceAppVO
     * @return 검색 조건에 부합하는 온라인 서비스 app 정보
     */
    @Override
    public ServiceAppVO getLastSvcAppInfo(ServiceAppVO vo) throws Exception {
        return selectOne("ServiceAppDAO.selectLastSvcAppInfo", vo);
    }

    /**
     * 온라인 서비스 app 삭제
     * @param ServiceAppVO
     * @return 삭제 결과
     */
    @Override
    public int deleteSvcApp(ServiceAppVO vo) throws Exception {
        return delete("ServiceAppDAO.deleteSvcApp", vo);
    }

}
