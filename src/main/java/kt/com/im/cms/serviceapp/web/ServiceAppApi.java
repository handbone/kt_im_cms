/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceapp.web;

import java.io.File;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFileUtil;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.file.web.FileApi;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.serviceapp.service.ServiceAppService;
import kt.com.im.cms.serviceapp.vo.ServiceAppVO;

/**
 *
 *온라인 서비스 관련 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 7. 02.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class ServiceAppApi {

    private final static String viewName = "../resources/api/onlineService/app/appProcess";

    @Resource(name = "ServiceAppService")
    private ServiceAppService svcAppService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name="FileService")
    private FileService fileService;

    @Autowired
    private DataSourceTransactionManager transactionManager;

    @Inject
    private FileSystemResource fsResource;

    @Autowired
    private CommonFileUtil fileUtil;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * 온라인 서비스 앱 등록 및 노출 순서 관리 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/svcApp")
    public ModelAndView svcAppProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAllowMember(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return addSvcApp(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            if (request.getParameter("orderNums") != null) {
                return editSvcAppDispOrder(request, rsModel, userVO);
            }

            return editSvcApp(request, rsModel, userVO);
        }

        if (method.equals("GET")) {
            if (request.getParameter("appSeq") == null) {
                return svcAppList(request, rsModel, userVO);
            }
            return svcApp(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return delSvcApp(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addSvcApp(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String checkString = CommonFnc.requiredChecking(request, "svcSeq,type,appNm");
        if (!checkString.equals("")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + checkString + ")");
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (!isAllowService(userVO, svcSeq)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        String type = CommonFnc.strFilter(CommonFnc.emptyCheckString("type", request));
        if (!type.equals("APP") && !type.equals("CONTS")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(type)");
            return rsModel.getModelAndView();
        }

        String contsTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("contsTitle", request));
        if (type.equals("CONT") && contsTitle.equals("")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(contTitle)");
            return rsModel.getModelAndView();
        }

        String pkgNm = CommonFnc.strFilter(CommonFnc.emptyCheckString("pkgNm", request));
        ServiceAppVO item = new ServiceAppVO();
        item.setPkgNm(pkgNm);
        if (type.equals("APP")) {
            boolean isExistPkgNm = (svcAppService.svcAppDetail(item) != null);
            if (isExistPkgNm) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist pkgNm");
                return rsModel.getModelAndView();
            }
        }

        String appNm = CommonFnc.strFilter(CommonFnc.emptyCheckString("appNm", request));
        String storUrl = CommonFnc.strFilter(CommonFnc.emptyCheckString("storUrl", request));
        String uri = CommonFnc.strFilter(CommonFnc.emptyCheckString("uri", request));

        item.setSvcSeq(svcSeq);
        item.setType(type);
        item.setAppNm(appNm);
        item.setContsTitle(contsTitle);
        item.setPkgNm(pkgNm);

        // escape 문자들 원복하여 저장
        storUrl = CommonFnc.unescapeStr(storUrl);
        uri = CommonFnc.unescapeStr(uri);
        item.setStorUrl(storUrl);
        item.setUri(uri);
        item.setCretrId(userVO.getMbrId());

        // 앱 등록 시 우선 순위 초기 값은 설정되어 있지 않음. 설정되지 않았다는 의미로 -1로 설정.
        item.setOrderNum(-1);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcAppService.insertSvcApp(item);
            if (result > 0) {
                String resultStr = fileUtil.insertFile(request, userVO, item.getAppSeq(), "APP_IMG");
                if (resultStr.equals(CommonFileUtil.success)) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("appSeq", item.getAppSeq());
                    rsModel.setResultType("insertSvcApp");
                    transactionManager.commit(status);
                } else {
                    if (resultStr.equals(CommonFileUtil.serverErr)) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        transactionManager.rollback(status);
                    } else {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + resultStr + ")");
                        transactionManager.rollback(status);
                    }
                }
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView editSvcApp(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String checkString = CommonFnc.requiredChecking(request, "appSeq,type,appNm");
        if (!checkString.equals("")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + checkString + ")");
            return rsModel.getModelAndView();
        }

        int appSeq = CommonFnc.emptyCheckInt("appSeq", request);
        if (appSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(appSeq)");
            return rsModel.getModelAndView();
        }

        String type = CommonFnc.strFilter(CommonFnc.emptyCheckString("type", request));
        if (!type.equals("APP") && !type.equals("CONTS")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(type)");
            return rsModel.getModelAndView();
        }

        String contsTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("contsTitle", request));
        if (type.equals("CONTS") && contsTitle.equals("")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(contTitle)");
            return rsModel.getModelAndView();
        }

        ServiceAppVO item = new ServiceAppVO();

        // 패키지 명이 변경되었을 경우에만 값이 들어옴
        String pkgNm = CommonFnc.strFilter(CommonFnc.emptyCheckString("pkgNm", request));
        if (!pkgNm.equals("")) {
            item.setPkgNm(pkgNm);
            if (type.equals("APP")) {
                boolean isExistPkgNm = (svcAppService.svcAppDetail(item) != null);
                if (isExistPkgNm) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage("exist pkgNm");
                    return rsModel.getModelAndView();
                }
            }
        }

        String appNm = CommonFnc.strFilter(CommonFnc.emptyCheckString("appNm", request));
        String storUrl = CommonFnc.strFilter(CommonFnc.emptyCheckString("storUrl", request));
        String uri = CommonFnc.strFilter(CommonFnc.emptyCheckString("uri", request));

        item.setAppSeq(appSeq);
        item.setAppNm(appNm);
        item.setContsTitle(contsTitle);

        // escape 문자들 원복하여 저장
        storUrl = CommonFnc.unescapeStr(storUrl);
        uri = CommonFnc.unescapeStr(uri);
        item.setStorUrl(storUrl);
        item.setUri(uri);
        item.setAmdrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcAppService.updateSvcApp(item);
            if (result > 0) {
                String resultStr = fileUtil.insertFile(request, userVO, item.getAppSeq(), "APP_IMG");
                if (!resultStr.equals(CommonFileUtil.success)) {
                    if (resultStr.equals(CommonFileUtil.serverErr)) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        transactionManager.rollback(status);
                    } else {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + resultStr + ")");
                        transactionManager.rollback(status);
                    }
                    return rsModel.getModelAndView();
                }

                String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);
                if (!delFileSeqs.equals("")) {
                    fileUtil.deleteFiles(delFileSeqs);
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("appSeq", item.getAppSeq());
                rsModel.setResultType("insertSvcApp");
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView editSvcAppDispOrder(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String checkString = CommonFnc.requiredChecking(request, "appSeqs,orderNums");
        if (!checkString.equals("")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + checkString + ")");
            return rsModel.getModelAndView();
        }

        String appSeqs = CommonFnc.emptyCheckString("appSeqs", request);
        String[] appSeqList = appSeqs.split(",");
        //String orgnlOrderNums = CommonFnc.emptyCheckString("orgnlOrderNums", request);
        //String[] orgnlOrderNumList = orgnlOrderNums.split(",");
        String orderNums = CommonFnc.emptyCheckString("orderNums", request);
        String[] orderNumList = orderNums.split(",");

        // appSeqs(노출 순서 변경 app의 고유 등록 번호)와 orgnlOrderNums(변경 전 순서), orderNums(변경할 순서)는 매핑되어 있는 값으로 길이 비교하여 다를 경우 오류 발생
        // if (appSeqList.length != orgnlOrderNumList.length || appSeqList.length != orderNumList.length) {
        //     rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(error (appSeqList length) or (orderNumList length))");
        //     return rsModel.getModelAndView();
        // }
        if (appSeqList.length != orderNumList.length) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(error (appSeqList length) or (orderNumList length))");
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int resultCnt = 0;
            for (int i = 0; i < appSeqList.length; i++) {
                int appSeq = Integer.parseInt(appSeqList[i]);
                int chgOrderNum = Integer.parseInt(orderNumList[i]);

                ServiceAppVO item = new ServiceAppVO();
                item.setAppSeq(appSeq);
                item.setOrderNum(chgOrderNum);

                int result = svcAppService.updateSvcApp(item);
                resultCnt = resultCnt + result;
            }

            if (appSeqList.length == resultCnt) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView delSvcApp(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String checkString = CommonFnc.requiredChecking(request, "svcSeq,delAppSeqs");
        if (!checkString.equals("")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + checkString + ")");
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (!isAllowService(userVO, svcSeq)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        String delAppSeqs = CommonFnc.emptyCheckString("delAppSeqs", request);
        String[] delAppSeqList = delAppSeqs.split(",");

        if (delAppSeqList.length == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No app list to delete.)");
            return rsModel.getModelAndView();
        }

        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

        for (int i = 0; i < delAppSeqList.length; i++) {
            ServiceAppVO item = new ServiceAppVO();
            int delAppSeq = Integer.parseInt(delAppSeqList[i]);

            // app에 첨부된 app 이미지 파일 정보 가져옴.
            FileVO fvo = new FileVO();
            fvo.setFileContsSeq(delAppSeq);
            fvo.setFileSe("APP_IMG");
            FileVO fItem = fileService.fileDetailInfo(fvo);
            String filePath = fItem.getFilePath();

            item.setDelSvcAppSeq(delAppSeq);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = svcAppService.deleteSvcApp(item);

                if (result > 0) {
                    if (fileService.fileSeqDelete(fItem) == 1) {
                        File f = new File(fsResource.getPath() + filePath);
                        f.delete();
                    }
                }

                transactionManager.commit(status);
            } catch(Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView svcAppList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        // 서비스 관리자는 해당 서비스의 공지사항만 조회 가능
        if (!isAllowService(userVO, svcSeq)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            return rsModel.getModelAndView();
        }

        ServiceAppVO item = new ServiceAppVO();
        item.setSvcSeq(svcSeq);

        /*
         * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
         */
        int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
        int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10000);
        String sidx = CommonFnc.emptyCheckString("sidx", request);
        String sord = CommonFnc.emptyCheckString("sord", request);

        int page = (currentPage - 1) * limit + 1;
        int pageEnd = page + limit - 1;

        /*
        // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
        if (sidx.equals("num") || sidx.equals("ORDER_NUM") || sidx.equals("APP_IMG")) {
            sidx="SVC_NOTICE_SEQ";
        }
        */

        item.setSidx(sidx);
        item.setSord(sord);
        item.setOffset(page);
        item.setLimit(pageEnd);

        List<ServiceAppVO> resultList = svcAppService.svcAppList(item);
        if (resultList.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            int totalCount = svcAppService.svcAppListTotalCount(item);

            // 실제 서버에 파일이 존재하는지 여부 확인
            for (int i = 0; i < resultList.size(); i++) {
                resultList.get(i).setIsFile("false");
                boolean isExistFile = fileUtil.isExistFile(resultList.get(i).getAppImgPath());
                if (isExistFile) {
                    resultList.get(i).setIsFile("true");
                }
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultList);
            rsModel.setData("totalCount", totalCount);
            rsModel.setData("row", limit);
            rsModel.setData("currentPage", currentPage);
            int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;
            rsModel.setData("totalPage", totalPage);
            rsModel.setResultType("svcAppList");
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView svcApp(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        int appSeq = CommonFnc.emptyCheckInt("appSeq", request);
        if (appSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(appSeq)");
            return rsModel.getModelAndView();
        }

        FileVO fileVO = new FileVO();
        fileVO.setFileContsSeq(appSeq);
        fileVO.setFileSe("APP_IMG");
        List<FileVO> fList = fileService.fileList(fileVO);

        ServiceAppVO item = new ServiceAppVO();
        item.setAppSeq(appSeq);
        
        ServiceAppVO resultItem = svcAppService.svcAppDetail(item);

        if (resultItem == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            if (!isAllowService(userVO, resultItem.getSvcSeq())) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultItem);
            rsModel.setData("appImgFileList",fList);
            rsModel.setResultType("svcAppInfo");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 외부 서비스 사이트 요청에 따른 App 노출 목록 조회 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/getAppDispOrderAPI")
    public ModelAndView getLatestNotice(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        String method = CommonFnc.getMethod(request);

        if (method.equals("GET")) {
            return getSvcAppList(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getSvcAppList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        ServiceAppVO item = new ServiceAppVO();
        item.setSvcSeq(svcSeq);

        /*
         * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
         */
        int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
        int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 1000);
        int page = (currentPage - 1) * limit + 1;
        int pageEnd = page + limit - 1;

        item.setSidx("ORDER_NUM");
        item.setSord("asc");
        item.setOffset(page);
        item.setLimit(pageEnd);

        // 외부 API에서 요청 시 searchType을 지정하여 내부 API와 구분
        item.setSearchType("launcher");

        List<ServiceAppVO> resultList = svcAppService.svcAppList(item);
        if (resultList.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            int totalCount = svcAppService.svcAppListTotalCount(item);

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultList);
            rsModel.setData("totalCount", totalCount);
            rsModel.setResultType("getSvcAppList");
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe);
    }

    private boolean isAllowService(MemberVO userVO, int svcSeq) {
        if (svcSeq == 0 || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != svcSeq)) {
            return false;
        }

        return true;
    }

}
