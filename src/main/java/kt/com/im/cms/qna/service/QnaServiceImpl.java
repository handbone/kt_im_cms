/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.qna.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.qna.dao.QnaDAO;
import kt.com.im.cms.qna.vo.QnaVO;

/**
 *
 * 문의사항 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 11.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("QnaService")
public class QnaServiceImpl implements QnaService {

    @Resource(name = "QnaDAO")
    private QnaDAO qnaDAO;

    /**
     * 문의사항 목록 조회
     * @param QnaVO
     * @return 검색 조건에 부합하는 문의사항 목록
     */
    @Override
    public List<QnaVO> qnaList(QnaVO vo) {
        return qnaDAO.qnaList(vo);
    }

    /**
     * 문의사항 목록 합계 조회
     * @param QnaVO
     * @return 검색 조건에 부합하는 문의사항 목록 합계
     */
    @Override
    public int qnaListTotalCount(QnaVO vo) {
        int res = qnaDAO.qnaListTotalCount(vo);
        return res;
    }

    /**
     * 문의사항 상세 정보 조회
     * @param QnaVO
     * @return 검색 조건에 부합하는 문의사항 상세 정보
     */
    @Override
    public QnaVO qnaDetail(QnaVO vo) {
        return qnaDAO.qnaDetail(vo);
    }

    /**
     * 문의사항 조회수 업데이트
     * @param QnaVO
     * @return
     */
    @Override
    public void updateQnaRetvNum(QnaVO vo) {
        qnaDAO.updateQnaRetvNum(vo);
    }

    /**
     * 문의사항 답변 등록
     * @param QnaVO
     * @return
     */
    @Override
    public void insertQnaAnswer(QnaVO vo) {
        qnaDAO.insertQnaAnswer(vo);
    }

    /**
     * 첨부파일 정보
     * @param QnaVO
     * @return 첨부파일 정보
     */
    @Override
    public QnaVO fileInfo(QnaVO vo) {
        return qnaDAO.fileInfo(vo);
    }

    /**
     * 첨부파일 삭제
     * @param QnaVO
     * @return 삭제 결과
     */
    @Override
    public int attachFileDelete(QnaVO vo) {
        return qnaDAO.attachFileDelete(vo);
    }

    /**
     * 문의사항 수정
     * @param QnaVO
     * @return
     */
    /*
    @Override
    public int updateQna(QnaVO vo) {
        return qnaDAO.updateQna(vo);
    }
    */

    /**
     * 문의사항 삭제 (DB 상에서 DEL_YN = 'N'로 변경)
     * @param QnaVO
     * @return
     */
    /*
    @Override
    public int deleteQna(QnaVO vo) {
        return qnaDAO.deleteQna(vo);
    }
    */

}
