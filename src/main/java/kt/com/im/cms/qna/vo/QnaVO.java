/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.qna.vo;

import java.io.Serializable;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 3.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class QnaVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2780227036497515022L;

    /** QNA 번호 */
    private int qnaSeq;

    /** QNA 구분 */
    private String qnaType;

    /** QNA 구분 코드 */
    private String qnaTypeCd;

    /** QNA 카테고리명*/
    private String qnaCtg;

    /** QNA 카테고리 코드 */
    private String qnaCtgCd;

    /** QNA 제목 */
    private String qnaTitle;

    /** QNA 내용 */
    private String qnaSbst;

    /** 조회수 */
    private int retvNum;

    /** 첨부파일 수 */
    private int fileCount;

    /** 첨부파일 번호 */
    private int fileSeq;

    /** 첨부파일 저장경로 */
    private String filePath;

    /** QNA 등록자 아이디 */
    private String cretrId;

    /** QNA 수정자 아이디 */
    private String amdrId;

    /** QNA 등록자 이름 */
    private String cretrNm;

    /** QNA 등록 일시 */
    private String regDt;

    /** QNA 답변 내용 */
    private String ansConts;

    /** QNA 답변 등록자 아이디 */
    private String ansCretrId;

    /** QNA 답변 수정자 아이디 */
    private String ansAmdrId;

    /** QNA 답변 등록자 이름 */
    private String ansCretrNm;

    /** QNA 답변 일시 */
    private String ansRegDt;

    /** QNA 삭제 여부 */
    private String delYn;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    public int getQnaSeq() {
        return qnaSeq;
    }

    public void setQnaSeq(int qnaSeq) {
        this.qnaSeq = qnaSeq;
    }

    public String getQnaType() {
        return qnaType;
    }

    public void setQnaType(String qnaType) {
        this.qnaType = qnaType;
    }

    public String getQnaTypeCd() {
        return qnaTypeCd;
    }

    public void setQnaTypeCd(String qnaTypeCd) {
        this.qnaTypeCd = qnaTypeCd;
    }

    public String getQnaCtg() {
        return qnaCtg;
    }

    public void setQnaCtg(String qnaCtg) {
        this.qnaCtg = qnaCtg;
    }

    public String getQnaCtgCd() {
        return qnaCtgCd;
    }

    public void setQnaCtgCd(String qnaCtgCd) {
        this.qnaCtgCd = qnaCtgCd;
    }

    public String getQnaTitle() {
        return qnaTitle;
    }

    public void setQnaTitle(String qnaTitle) {
        this.qnaTitle = qnaTitle;
    }

    public String getQnaSbst() {
        return qnaSbst;
    }

    public void setQnaSbst(String qnaSbst) {
        this.qnaSbst = qnaSbst;
    }

    public int getRetvNum() {
        return retvNum;
    }

    public void setRetvNum(int retvNum) {
        this.retvNum = retvNum;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public int getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(int fileSeq) {
        this.fileSeq = fileSeq;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getAnsConts() {
        return ansConts;
    }

    public void setAnsConts(String ansConts) {
        this.ansConts = ansConts;
    }

    public String getAnsCretrId() {
        return ansCretrId;
    }

    public void setAnsCretrId(String ansCretrId) {
        this.ansCretrId = ansCretrId;
    }

    public String getAnsAmdrId() {
        return ansAmdrId;
    }

    public void setAnsAmdrId(String ansAmdrId) {
        this.ansAmdrId = ansAmdrId;
    }

    public String getAnsCretrNm() {
        return ansCretrNm;
    }

    public void setAnsCretrNm(String ansCretrNm) {
        this.ansCretrNm = ansCretrNm;
    }

    public String getAnsRegDt() {
        return ansRegDt;
    }

    public void setAnsRegDt(String ansRegDt) {
        this.ansRegDt = ansRegDt;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

}
