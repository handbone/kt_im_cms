/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.qna.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.qna.service.QnaService;
import kt.com.im.cms.qna.vo.QnaVO;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;

@Controller
public class QnaApi {

    private final static String viewName = "../resources/api/customer/qna/qnaProcess";

    @Resource(name="QnaService")
    private QnaService qnaService;

    @Resource(name="FileService")
    private FileService fileService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    /**
     * 문의사항 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/qna")
    public ModelAndView qnaProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 로그인 되지 않았거나 Master or CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAdministrator = this.isAdministrator(userVO);
        if (userVO == null || !isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        QnaVO item = new QnaVO();
        FileVO fileVO = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request,"qnaSeq,ansSbst");
            if(!checkString.equals("")){
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter("+checkString+")");
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int qnaSeq = CommonFnc.emptyCheckInt("qnaSeq", request);
            String ansContent   = CommonFnc.emptyCheckString("ansSbst", request);

            item.setQnaSeq(qnaSeq);
            item.setAnsConts(ansContent);
            item.setAnsCretrId(userVO.getMbrId());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                qnaService.insertQnaAnswer(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } catch (Exception e){
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int qnaSeq = CommonFnc.emptyCheckInt("qnaSeq", request);

            if (qnaSeq != 0) {
                item.setQnaSeq(qnaSeq);
                fileVO.setFileContsSeq(qnaSeq);
                // 문의사항 첨부파일
                fileVO.setFileSe("QNA");
                List<FileVO> fList = fileService.fileList(fileVO);
                // 문의사항 답변 첨부파일
                fileVO.setFileSe("QNA_ANS");
                List<FileVO> ansFList = fileService.fileList(fileVO);

                QnaVO resultItem = qnaService.qnaDetail(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                // QNA 답변 등록을 위해 상세 정보 요청 시에는 조회 수 업데이트 하지 않음.
                String searchType = CommonFnc.emptyCheckString("searchType", request);
                if (!searchType.equals("edit")) {
                    qnaService.updateQnaRetvNum(item);
                    resultItem.setRetvNum(resultItem.getRetvNum() + 1); 
                }

                /* 개인정보 마스킹 (등록자 이름, 아이디, 답변자 이름, 아이디, 답변 수정자 아이디) */
                resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));
                resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                resultItem.setAnsCretrNm(CommonFnc.getNameMask(resultItem.getAnsCretrNm()));
                resultItem.setAnsCretrId(CommonFnc.getIdMask(resultItem.getAnsCretrId()));
                resultItem.setAnsAmdrId(CommonFnc.getIdMask(resultItem.getAnsAmdrId()));

                // 내용 태그 원복
                resultItem.setQnaSbst(CommonFnc.unescapeStr(resultItem.getQnaSbst()));
                resultItem.setAnsConts(CommonFnc.unescapeStr(resultItem.getAnsConts()));

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("fileList", fList);
                rsModel.setData("ansFileList", ansFList);
                rsModel.setResultType("qnaDetail");
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (qnaSeq == 0 && CommonFnc.checkReqParameter(request, "qnaSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if(searchConfirm.equals("true")){
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    if (searchField =="qnaTitle") {
                        searchField = "QNA_TITLE";
                    } else if (searchField =="cretrNm") {
                        searchField = "CRETR_NM";
                    }

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }
                /*
                 * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                 */
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);

                int page = (currentPage -1) * limit+1;
                int pageEnd = page + limit - 1;

                if (sidx.equals("num")) {
                    sidx="QNA_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<QnaVO> resultItem = qnaService.qnaList(item);

                if(resultItem.isEmpty()){
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                /* 개인정보 마스킹 (등록자 이름, 아이디, 답변자 아이디) */
                for (int i = 0; i < resultItem.size(); i++) {
                    resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));
                    resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                    resultItem.get(i).setAnsCretrId(CommonFnc.getIdMask(resultItem.get(i).getAnsCretrId()));
                }

                int totalCount = qnaService.qnaListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("qnaList");
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

}
