/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.reservate.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.device.service.DeviceService;
import kt.com.im.cms.device.vo.DeviceVO;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.product.service.ProductService;
import kt.com.im.cms.product.vo.ProductVO;
import kt.com.im.cms.reservate.service.ReservateService;
import kt.com.im.cms.reservate.vo.ReservateVO;
import kt.com.im.cms.stats.service.StatsService;

/**
 *
 * 이용권 관련 메뉴에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 23.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class ReservateApi {

    @Resource(name = "ReservateService")
    private ReservateService reservateService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "ProductService")
    private ProductService productService;

    @Resource(name = "DeviceService")
    private DeviceService deviceService;

    @Resource(name = "StatsService")
    private StatsService statsService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    /**
     * 발급된 이용권 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/reservate")
    public ModelAndView reservateProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/reservate/reservateProcess");

        // 로그인 되지 않았거나 권한이 없을경우 접근불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ReservateVO item = new ReservateVO();

        String loginId = userVO.getMbrId();
        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request,
                    "tagSeq,usageHour,usageMinute,rmndCnt,useYn,useSttus");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String timeChange = CommonFnc.emptyCheckString("timeChange", request);

            item.setLoginId(loginId);

            if (timeChange.equals("N")) {
                item.setChanged(false);
            } else {
                item.setChanged(true);
            }

            int tagSeq = CommonFnc.emptyCheckInt("tagSeq", request);
            int usageHour = CommonFnc.emptyCheckInt("usageHour", request);
            int usageMinute = CommonFnc.emptyCheckInt("usageMinute", request);
            int rmndCnt = CommonFnc.emptyCheckInt("rmndCnt", request);
            String useYn = CommonFnc.emptyCheckString("useYn", request);
            String useSttus = CommonFnc.emptyCheckString("useSttus", request);

            item.setTagSeq(tagSeq);
            item.setUsageHour(usageHour);
            item.setUsageMinute(usageMinute);
            item.setRmndCnt(rmndCnt);
            item.setUseYn(useYn);
            item.setUseSttus(useSttus);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = reservateService.reservateUpdate(item);
                if (result == 1) {
                    if (useSttus.equals("B")) {
                        // 조회
                        ReservateVO resultItem = reservateService.reservateDetail(item);

                        // 카테고리 삭제
                        reservateService.reservateCategoryDelete(resultItem);

                        // 카테고리 입력
                        ProductVO Pitem = new ProductVO();
                        Pitem.setGsrProdCode("");
                        Pitem.setProdSeq(resultItem.getProdSeq());

                        List<ProductVO> resultCategoryItem = productService.productCategoryList(Pitem);
                        for (int j = 0; j < resultCategoryItem.size(); j++) {
                            item.setProdSeq(resultCategoryItem.get(j).getProdSeq());
                            item.setDevCtgNm(resultCategoryItem.get(j).getDevCtgNm());
                            item.setLmtCnt(resultCategoryItem.get(j).getLmtCnt());
                            item.setDeduction(resultCategoryItem.get(j).getDeduction());
                            reservateService.reservateCategoryInsert(item);
                        }
                    } else {
                        String selCategorys = CommonFnc.emptyCheckString("selCategorys", request);
                        String selCategoryCounts = CommonFnc.emptyCheckString("selCategoryCounts", request);
                        String selCategorySeqs = CommonFnc.emptyCheckString("selCategorySeq", request);
                        String[] selCategorySeq = {};
                        String[] selCategory = selCategorys.split(",");
                        String[] selCategoryCount = selCategoryCounts.split(",");

                        if (selCategorySeqs != null)
                            selCategorySeq = selCategorySeqs.split(",");
                        if (selCategorySeqs != "") {
                            for (int i = 0; i < selCategory.length; i++) {
                                int categorySeq = Integer.parseInt(selCategorySeq[i]);
                                item.setTagCtgSeq(categorySeq);
                                item.setDevCtgNm(selCategory[i]);
                                item.setLmtCnt(Integer.parseInt(selCategoryCount[i]));

                                reservateService.reservateCategoryUpdate(item);
                            }
                        }
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            String tagSeqList = CommonFnc.emptyCheckString("tagSeqList", request);
            String[] delTagSeqList = tagSeqList.split(",");

            if (delTagSeqList.length == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }

            for (int i = 0; i < delTagSeqList.length; i++) {
                System.out.println("del[" + i + "] tagSeq : " + delTagSeqList[i]);
            }

            item.setLoginId(loginId);
            item.setDelTagSeqList(delTagSeqList);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = reservateService.reservateDelete(item);
                if (result > 0) {
                    reservateService.reservateDeleteCategory(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            int tagSeq = CommonFnc.emptyCheckInt("tagSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String useSttus = CommonFnc.emptyCheckString("useSttus", request);
            String startDt = CommonFnc.emptyCheckString("startDt", request);
            String endDt = CommonFnc.emptyCheckString("endDt", request);

            if (tagSeq != 0) {
                item.setTagSeq(tagSeq);
                ReservateVO resultItem = reservateService.reservateDetail(item);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                            && userVO.getSvcSeq() != resultItem.getSvcSeq())
                            || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)
                                    && userVO.getStorSeq() != resultItem.getStorSeq())) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    item.setTagSeq(tagSeq);
                    item.setDevCtgNm("");
                    List<ReservateVO> resultCategoryItem = reservateService.reservateCategoryInfoList(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("CItem", resultCategoryItem);
                    rsModel.setData("resultType", "reservateDetail");
                }
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (tagSeq == 0 && CommonFnc.checkReqParameter(request, "tagSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if (searchConfirm.equals("true")) {
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }
                /*
                 * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                 */
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);

                int page = (currentPage - 1) * limit + 1;
                int pageEnd = page + limit - 1;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("num")) {
                    sidx = "TAG_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);
                item.setStorSeq(storSeq);
                item.setUseSttus(useSttus);
                item.setDevSeq(0);
                item.setStartDt(startDt);
                item.setEndDt(endDt);
                List<ReservateVO> resultItem = reservateService.reservateList(item);
                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    int totalCount = reservateService.reservateListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setData("resultType", "reservateList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 상품(이용권) 사용 카테고리 정보 API (2차 카테고리 리스트 조회)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/reservateCategory")
    public ModelAndView productContsCategoryList(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/reservate/reservateProcess");

        // 로그인 되지 않았거나 권한이 없을경우 접근불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ReservateVO item = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            item.setStorSeq(storSeq);
            List<ReservateVO> resultItem = reservateService.reservateSecondCategoryList(item);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
            }
            rsModel.setData("resultType", "reservateSecondCategoryList");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 발급된 이용권의 사용 콘텐츠 조회 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/reservateUseContent")
    public ModelAndView reservateUseContent(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/reservate/reservateProcess");

        // 로그인 되지 않았거나 권한이 없을경우 접근불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ReservateVO item = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "tagSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E
            int tagSeq = CommonFnc.emptyCheckInt("tagSeq", request);
            int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
            String sidx = CommonFnc.emptyCheckString("sidx", request);
            String sord = CommonFnc.emptyCheckString("sord", request);

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            if (sidx.equals("num")) {
                sidx = "CONTS_USE_TIME_SEQ";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setOffset(page);
            item.setLimit(pageEnd);
            item.setTagSeq(tagSeq);

            List<ReservateVO> resultItem = reservateService.reservateUseContent(item);

            int totalCount = reservateService.reservateUseContentTotalCount(item);

            if (resultItem.size() == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("row", limit);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setData("resultType", "reservateUseContentList");
                rsModel.setData("item", resultItem);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 발급된 이용권의 사용 카테고리(디바이스) 관련 사용 정보 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/reservateUse")
    public ModelAndView reservateUse(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/reservate/reservateProcess");

        // 로그인 되지 않았을경우 접근불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ReservateVO item = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "devSeq,storSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            item.setLoginId(userVO.getMbrId());

            String tagId = CommonFnc.emptyCheckString("tagId", request);
            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String fnsTime = CommonFnc.emptyCheckString("fnsTime", request);
            String stTime = CommonFnc.emptyCheckString("stTime", request);

            item.setTagId(tagId);
            item.setDevSeq(devSeq);
            item.setStorSeq(storSeq);

            DeviceVO dItem = new DeviceVO();
            dItem.setDevSeq(devSeq);
            dItem.setStorSeq(storSeq);
            dItem.setTagId(tagId);
            dItem.setStTime(stTime);
            dItem.setFnsTime(fnsTime);

            try {
                deviceService.contentsPlayTimeOut(dItem);

                int result = reservateService.reservateUseUpdate(item);
                deviceService.deviceUseStateUpdate(dItem);
                if (result == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "tagId");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String tagId = CommonFnc.emptyCheckString("tagId", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

            item.setTagId(tagId);
            item.setStorSeq(storSeq);

            List<ReservateVO> resultItem = reservateService.reservateUseList(item);

            if (resultItem.size() == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("resultType", "reservateUseList");
                rsModel.setData("item", resultItem);
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe) || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
    }

}
