/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.reservate.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.reservate.vo.ReservateVO;

/**
 *
 * 이용권 발급 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 23.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("ReservateDAO")
@Transactional
public class ReservateDAOImpl extends mysqlAbstractMapper implements ReservateDAO {

    /**
     * 검색조건에 해당되는 이용권 발급 리스트 정보
     *
     * @param ReservateVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ReservateVO> reservateList(ReservateVO vo) throws Exception {
        return selectList("ReservateDAO.reservateList", vo);
    }

    /**
     * 검색조건에 해당되는 이용권 발급 리스트 합계 정보
     *
     * @param ReservateVO
     * @return 조회 목록 합계
     */
    @Override
    public int reservateListTotalCount(ReservateVO vo) throws Exception {
        return selectOne("ReservateDAO.reservateListTotalCount", vo);
    }

    /**
     * 검색조건에 해당되는 발급 이용권의 상세 정보
     *
     * @param ReservateVO
     * @return 조회 결과
     */
    @Override
    public ReservateVO reservateDetail(ReservateVO vo) throws Exception {
        return selectOne("ReservateDAO.reservateDetail", vo);
    }

    /**
     * 검색조건에 해당되는 이용권의 카테고리 정보
     *
     * @param ReservateVO
     * @return 조회 목록 결과
     */
    @Override
    public ReservateVO reservateCategoryInfo(ReservateVO vo) throws Exception {
        return selectOne("ReservateDAO.reservateCategoryInfo", vo);
    }

    /**
     * 검색조건에 해당되는 이용권의 카테고리 목록 정보
     *
     * @param ReservateVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ReservateVO> reservateCategoryInfoList(ReservateVO vo) throws Exception {
        return selectList("ReservateDAO.reservateCategoryInfo", vo);
    }

    /**
     * 검색조건에 해당되는 이용권 사용 리스트 정보
     *
     * @param ReservateVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ReservateVO> reservateUseContent(ReservateVO vo) throws Exception {
        return selectList("ReservateDAO.reservateUseContent", vo);
    }

    /**
     * 검색조건에 해당되는 이용권 사용 리스트 합계 정보
     *
     * @param ReservateVO
     * @return 조회 목록 합계
     */
    @Override
    public int reservateUseContentTotalCount(ReservateVO vo) throws Exception {
        return selectOne("ReservateDAO.reservateUseContentTotalCount", vo);
    }

    /**
     * 발급된 이용권의 정보 수정
     *
     * @param ReservateVO
     * @return result(int)
     */
    @Override
    public int reservateUpdate(ReservateVO vo) throws Exception {
        return update("ReservateDAO.reservateUpdate", vo);
    }

    /**
     * 발급된 이용권의 카테고리 정보 생성
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public void reservateCategoryInsert(ReservateVO vo) throws Exception {
        insert("ReservateDAO.reservateCategoryInsert", vo);
    }

    /**
     * 발급된 이용권의 카테고리 정보 수정
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public void reservateCategoryUpdate(ReservateVO vo) throws Exception {
        update("ReservateDAO.reservateCategoryUpdate", vo);
    }

    /**
     * 발급된 이용권의 카테고리 정보 삭제
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public void reservateCategoryDelete(ReservateVO vo) throws Exception {
        delete("ReservateDAO.reservateCategoryDelete", vo);
    }

    /**
     * 발급된 이용권의 사용 정보 목록
     *
     * @param ReservateVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ReservateVO> reservateUseList(ReservateVO vo) throws Exception {
        return selectList("ReservateDAO.reservateUseList", vo);
    }

    /**
     * 발급된 이용권의 사용 정보 수정
     *
     * @param ReservateVO
     * @return result(int)
     */
    @Override
    public int reservateUseUpdate(ReservateVO vo) throws Exception {
        return update("ReservateDAO.reservateUseUpdate", vo);
    }

    /**
     * 발급된 이용권 삭제
     *
     * @param ReservateVO
     * @return int(result)
     */
    @Override
    public int reservateDelete(ReservateVO vo) throws Exception {
        return update("ReservateDAO.reservateDelete", vo);
    }

    /**
     * 발급된 이용권 삭제에 따른 카테고리 정보 삭제
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public void reservateDeleteCategory(ReservateVO vo) throws Exception {
        update("ReservateDAO.reservateDeleteCategory", vo);
    }

    /**
     * 이용권 발급
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public int reservateInsert(ReservateVO vo) throws Exception {
        return insert("ReservateDAO.reservateInsert", vo);
    }

    /**
     * 동일 태그(이용권 태그) 존재 여부 확인
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public int existTagCount(ReservateVO vo) throws Exception {
        return selectOne("ReservateDAO.existTagCount", vo);
    }

    /**
     * 이용권 발급 결제 정보 업데이트
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public void reservateSettlUpdate(ReservateVO vo) throws Exception {
        update("ReservateDAO.reservateSettlUpdate", vo);
    }

    /**
     * 이용권 발급 관련 2차 카테고리 리스트
     *
     * @param ReservateVO
     * @return 카테고리 목록
     */
    @Override
    public List<ReservateVO> reservateSecondCategoryList(ReservateVO vo) throws Exception {
        return selectList("ReservateDAO.reservateSecondCategoryList", vo);
    }

    /**
     * 검색조건에 해당되는 이용권의 카테고리 목록 정보 (상품정보까지 매칭하여 조회)
     *
     * @param ReservateVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ReservateVO> reservateCategoryList(ReservateVO vo) throws Exception {
        return selectList("ReservateDAO.reservateCategoryList", vo);
    }

    /**
     * 이용권의 카테고리 사용 정보 업데이트
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public void reservateCategoryCountUpdate(ReservateVO vo) throws Exception {
        update("ReservateDAO.reservateCategoryCountUpdate", vo);
    }

    /**
     * TAG 사용여부 조회
     *
     * @param ReservateVO
     * @return
     */
    @Override
    public ReservateVO reservateUsedInfo(ReservateVO vo) {
        return selectOne("ReservateDAO.reservateUsedInfo", vo);
    }

    /**
     * TAG 추가시간 업데이트
     * 
     * @param ReservateVO
     * @return
     */
    @Override
    public int reservateAddTime(ReservateVO vo) {
        return update("ReservateDAO.reservateAddTime", vo);
    }

}
