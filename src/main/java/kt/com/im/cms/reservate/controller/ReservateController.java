/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.reservate.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * 이용권 발급 메뉴 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 15.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Component
@Controller
public class ReservateController {

    /**
     * 발급된 이용권 리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/reservate", method = RequestMethod.GET)
    public ModelAndView reservateList(ModelAndView mv) {
        mv.setViewName("/views/service/reservate/ReservateList");
        return mv;
    }

    /**
     * 발급된 이용권 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/reservate/{tagSeq}", method = RequestMethod.GET)
    public ModelAndView tagDetail(ModelAndView mv, @PathVariable(value="tagSeq") String tagSeq) {
        System.out.println(">> [TagDetail] tagSeq : " + tagSeq);
        mv.addObject("tagSeq", tagSeq);
        mv.setViewName("/views/service/reservate/ReservateDetail");
        return mv;
    }

    /**
     * 이용권 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/reservate/{tagSeq}/edit", method = RequestMethod.GET)
    public ModelAndView tagEdit(ModelAndView mv, @PathVariable(value="tagSeq") String tagSeq) {
        System.out.println(">> [TagEdit] tagSeq : " + tagSeq);
        mv.addObject("tagSeq", tagSeq);
        mv.setViewName("/views/service/reservate/ReservateEdit");
        return mv;
    }

}
