/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.reservate.dao;

import java.util.List;

import kt.com.im.cms.reservate.vo.ReservateVO;

/**
 *
 * 이용권 발급 관련 데이터 접근 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 23.   A2TEC      최초생성
*
*
* </pre>
*/

public interface ReservateDAO {

    public List<ReservateVO> reservateList(ReservateVO vo) throws Exception;

    public int reservateListTotalCount(ReservateVO vo) throws Exception;

    public ReservateVO reservateDetail(ReservateVO vo) throws Exception;

    public ReservateVO reservateCategoryInfo(ReservateVO vo) throws Exception;

    public List<ReservateVO> reservateCategoryInfoList(ReservateVO vo) throws Exception;

    public List<ReservateVO> reservateUseContent(ReservateVO vo) throws Exception;

    public int reservateUseContentTotalCount(ReservateVO vo) throws Exception;

    public int reservateUpdate(ReservateVO vo) throws Exception;

    public void reservateCategoryInsert(ReservateVO vo) throws Exception;

    public void reservateCategoryUpdate(ReservateVO vo) throws Exception;

    public void reservateCategoryDelete(ReservateVO vo) throws Exception;

    public List<ReservateVO> reservateUseList(ReservateVO vo) throws Exception;

    public int reservateUseUpdate(ReservateVO vo) throws Exception;

    public int reservateDelete(ReservateVO vo) throws Exception;

    public void reservateDeleteCategory(ReservateVO vo) throws Exception;

    public int reservateInsert(ReservateVO vo) throws Exception;

    public int existTagCount(ReservateVO vo) throws Exception;

    public void reservateSettlUpdate(ReservateVO vo) throws Exception;

    public List<ReservateVO> reservateSecondCategoryList(ReservateVO vo) throws Exception;

    public List<ReservateVO> reservateCategoryList(ReservateVO vo) throws Exception;

    public void reservateCategoryCountUpdate(ReservateVO vo) throws Exception;

    public ReservateVO reservateUsedInfo(ReservateVO vo);

    public int reservateAddTime(ReservateVO vo);

}
