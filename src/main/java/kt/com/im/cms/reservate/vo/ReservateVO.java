/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.reservate.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 이용권 발급 메뉴 관련 데이터 객체
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 23.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
public class ReservateVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2843195678011954647L;

    /** 태그 번호 */
    private int tagSeq;

    /** 태그 아이디 */
    private String tagId;

    /* 결제 유형 */
    private String settlType;

    /* 반품 가능 여부 */
    private String resetYn;

    /** 태그 사용 상태 */
    private String useSttus;

    /** 태그 사용 유무 */
    private String useYn;

    /** 태그 사용 상태 코드 */
    private String useYnCode;

    /** 태그 사용 상태명 */
    private String useYnNm;

    /** 잔여 횟수 */
    private int rmndCnt;

    /** 추가 시간 */
    private int apdTime;

    /** 잔여 시간 */
    private String rmndTime;

    /** 상품 번호 */
    private int prodSeq;

    /** 상품 이름 */
    private String prodNm;

    /** 상품(이용권) 제한 횟수 */
    private int prodLmtCnt;

    /** 상품(이용권) 제한 시간 */
    private int prodTimeLmt;

    /** 서비스 번호 */
    private int svcSeq;

    /** 매장 번호 */
    private int storSeq;

    /** 매장명 */
    private String storNm;

    /** 상품 카테고리 - 사용시 전체횟수 차감 여부 */
    private String deduction;

    /** 결제 번호 */
    private int settlSeq;

    /** 장비 번호 */
    private int devSeq;

    /** 장비 유형 명 */
    private String devTypeNm;

    /** 사용 시간 시 */
    private int usageHour;

    /** 사용 시간 분 */
    private int usageMinute;

    /** 로그인 아이디 */
    private String loginId;

    /** 발매일 */
    private String regDt;

    /** 콘텐츠 리스트 조회시 정렬 칼럼명 */
    private String sidx;

    /** 콘텐츠 리스트 조회시 정렬 방법 */
    private String sord;

    /** 검색 필드 */
    private String searchTarget;

    /** 검색 키워드 */
    private String searchKeyword;

    /** 콘텐츠 리스트 조회시 검색 여부 확인 */
    private String searchConfirm;

    /** 시작 일시 */
    private String startDt;

    /** 종료 일시 */
    private String endDt;

    /** offset */
    private int offset;

    /** limit (한 페이지에 보여질 갯수) */
    private int limit;

    /** 태그 카테고리 번호 */
    private int tagCtgSeq;

    /** 태그 카테고리 장비 종류 명 */
    private String devCtgNm;

    /** 제한 횟수 */
    private int lmtCnt;

    /** 태그 카테고리 사용 유무 */
    private String ctgUseYn;

    /** 콘텐츠 사용 시간 번호 */
    private int contsUseTimeSeq;

    /** 콘텐츠 번호 */
    private int contsSeq;

    /** 시작 시간 */
    private String stTime;

    /** 종료 시간 */
    private String fnsTime;

    /** 콘텐츠명 */
    private String contsTitle;

    /** 디바이스 아이디 */
    private String devId;

    /** 변경 여부 */
    private Boolean changed;

    /** 삭제할 발급 이용권 목록 */
    private int[] delTagSeqList;

    /** 점포 코드 */
    private String storCode;

    /** GSR 상품 코드 */
    private String gsrProdCode;

    /** 할인 가격 */
    private int discount;

    /** 상품 가격 */
    private int prodPrc;

    /** 총 결제 금액 */
    private int totSettlAmt;

    /** 발매 순서 */
    private int sleNo;

    /** 콘텐츠 1차 카테고리 명 */
    private String firstCtgNm;

    /** 콘텐츠 2차 카테고리 명 */
    private String secondCtgNm;

    /** 상품 카테고리 목록 */
    private List<ReservateVO> reservateCategoryList;

    public int getTagSeq() {
        return tagSeq;
    }

    public void setTagSeq(int tagSeq) {
        this.tagSeq = tagSeq;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getUseSttus() {
        return useSttus;
    }

    public void setUseSttus(String useSttus) {
        this.useSttus = useSttus;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public int getRmndCnt() {
        return rmndCnt;
    }

    public void setRmndCnt(int rmndCnt) {
        this.rmndCnt = rmndCnt;
    }

    public String getRmndTime() {
        return rmndTime;
    }

    public void setRmndTime(String rmndTime) {
        this.rmndTime = rmndTime;
    }

    public int getProdSeq() {
        return prodSeq;
    }

    public void setProdSeq(int prodSeq) {
        this.prodSeq = prodSeq;
    }

    public String getProdNm() {
        return prodNm;
    }

    public void setProdNm(String prodNm) {
        this.prodNm = prodNm;
    }

    public int getProdLmtCnt() {
        return prodLmtCnt;
    }

    public void setProdLmtCnt(int prodLmtCnt) {
        this.prodLmtCnt = prodLmtCnt;
    }

    public int getProdTimeLmt() {
        return prodTimeLmt;
    }

    public void setProdTimeLmt(int prodTimeLmt) {
        this.prodTimeLmt = prodTimeLmt;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public int getSettlSeq() {
        return settlSeq;
    }

    public void setSettlSeq(int settlSeq) {
        this.settlSeq = settlSeq;
    }

    public int getDevSeq() {
        return devSeq;
    }

    public void setDevSeq(int devSeq) {
        this.devSeq = devSeq;
    }

    public String getDevTypeNm() {
        return devTypeNm;
    }

    public void setDevTypeNm(String devTypeNm) {
        this.devTypeNm = devTypeNm;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public int getUsageHour() {
        return usageHour;
    }

    public void setUsageHour(int usageHour) {
        this.usageHour = usageHour;
    }

    public int getUsageMinute() {
        return usageMinute;
    }

    public void setUsageMinute(int usageMinute) {
        this.usageMinute = usageMinute;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getStartDt() {
        return startDt;
    }

    public void setStartDt(String startDt) {
        this.startDt = startDt;
    }

    public String getEndDt() {
        return endDt;
    }

    public void setEndDt(String endDt) {
        this.endDt = endDt;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTagCtgSeq() {
        return tagCtgSeq;
    }

    public void setTagCtgSeq(int tagCtgSeq) {
        this.tagCtgSeq = tagCtgSeq;
    }

    public String getDevCtgNm() {
        return devCtgNm;
    }

    public void setDevCtgNm(String devCtgNm) {
        this.devCtgNm = devCtgNm;
    }

    public int getLmtCnt() {
        return lmtCnt;
    }

    public void setLmtCnt(int lmtCnt) {
        this.lmtCnt = lmtCnt;
    }

    public String getCtgUseYn() {
        return ctgUseYn;
    }

    public void setCtgUseYn(String ctgUseYn) {
        this.ctgUseYn = ctgUseYn;
    }

    public int getContsUseTimeSeq() {
        return contsUseTimeSeq;
    }

    public void setContsUseTimeSeq(int contsUseTimeSeq) {
        this.contsUseTimeSeq = contsUseTimeSeq;
    }

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public String getStTime() {
        return stTime;
    }

    public void setStTime(String stTime) {
        this.stTime = stTime;
    }

    public String getFnsTime() {
        return fnsTime;
    }

    public void setFnsTime(String fnsTime) {
        this.fnsTime = fnsTime;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public String getDevId() {
        return devId;
    }

    public void setDevId(String devId) {
        this.devId = devId;
    }

    public Boolean getChanged() {
        return changed;
    }

    public void setChanged(Boolean changed) {
        this.changed = changed;
    }

    public int[] getDelTagSeqList() {
        return delTagSeqList;
    }

    public void setDelTagSeqList(String[] delTagSeqList) {
        this.delTagSeqList = new int[delTagSeqList.length];
        for (int i = 0; i < delTagSeqList.length; i++) {
            this.delTagSeqList[i] = Integer.parseInt(delTagSeqList[i]);
        }
    }

    public String getStorCode() {
        return storCode;
    }

    public void setStorCode(String storCode) {
        this.storCode = storCode;
    }

    public String getGsrProdCode() {
        return gsrProdCode;
    }

    public void setGsrProdCode(String gsrProdCode) {
        this.gsrProdCode = gsrProdCode;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getProdPrc() {
        return prodPrc;
    }

    public void setProdPrc(int prodPrc) {
        this.prodPrc = prodPrc;
    }

    public int getTotSettlAmt() {
        return totSettlAmt;
    }

    public void setTotSettlAmt(int totSettlAmt) {
        this.totSettlAmt = totSettlAmt;
    }

    public int getSleNo() {
        return sleNo;
    }

    public void setSleNo(int sleNo) {
        this.sleNo = sleNo;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getUseYnCode() {
        return useYnCode;
    }

    public void setUseYnCode(String useYnCode) {
        this.useYnCode = useYnCode;
    }

    public String getUseYnNm() {
        return useYnNm;
    }

    public void setUseYnNm(String useYnNm) {
        this.useYnNm = useYnNm;
    }

    public String getFirstCtgNm() {
        return firstCtgNm;
    }

    public void setFirstCtgNm(String firstCtgNm) {
        this.firstCtgNm = firstCtgNm;
    }

    public String getSecondCtgNm() {
        return secondCtgNm;
    }

    public void setSecondCtgNm(String secondCtgNm) {
        this.secondCtgNm = secondCtgNm;
    }

    public List<ReservateVO> getReservateCategoryList() {
        return reservateCategoryList;
    }

    public void setReservateCategoryList(List<ReservateVO> reservateCategoryList) {
        for (int i = 0; i < reservateCategoryList.size(); i++) {
            this.reservateCategoryList.add(reservateCategoryList.get(i));
        }
    }

    public int getApdTime() {
        return apdTime;
    }

    public void setApdTime(int apdTime) {
        this.apdTime = apdTime;
    }

    public String getSttlType() {
        return settlType;
    }

    public void setSttlType(String settlType) {
        this.settlType = settlType;
    }

    public String getResetYn() {
        return resetYn;
    }

    public void setResetYn(String resetYn) {
        this.resetYn = resetYn;
    }

    public String getDeduction() {
        return deduction;
    }

    public void setDeduction(String deduction) {
        this.deduction = deduction;
    }

}
