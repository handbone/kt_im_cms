/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceterms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.serviceterms.service.ServiceTermsService;
import kt.com.im.cms.serviceterms.vo.ServiceTermsVO;

/**
 *
 * 서비스 공지사항 관련 처리 API
 *
 * @author A2TEC
 * @since 2019.04.02
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 04. 02.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class ServiceTermsApi {

    private final static String viewName = "../resources/api/onlineService/terms/serviceTermsProcess";

    @Resource(name="ServiceTermsService")
    private ServiceTermsService svcTermsService;

    @Resource(name="MemberService")
    private MemberService memberService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * 외부 서비스 사이트 요청에 따른 약관 정보 조회 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/getLatestTermsAPI")
    public ModelAndView getTerms(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        ServiceTermsVO item = new ServiceTermsVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            String stpltType = CommonFnc.emptyCheckString("stpltType", request);

            item.setSvcSeq(svcSeq);
            item.setStpltType(stpltType);

            List<ServiceTermsVO> resultItem = svcTermsService.getLatestSvcTermsInfoList(item);

            if (!resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setResultType("latestSvcTermsInfoList");
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 서비스 약관 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/serviceTerms")
    public ModelAndView serviceTermsProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAllowMember(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ServiceTermsVO item = new ServiceTermsVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return addServiceTerms(request, rsModel, userVO);
        }
        if (method.equals("PUT")) {
            return editServiceTerms(request, rsModel, userVO);
        }
        if (method.equals("DELETE")) {
            return delServiceTerms(request, rsModel, userVO);
        }
        if (method.equals("GET")) {
            return getServiceTerms(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addServiceTerms(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"svcSeq,stpltType,stpltVer,stpltSbst,useYn,perdYn,mandYn");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }
        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        String stpltType = CommonFnc.emptyCheckString("stpltType", request);
        String stpltVer = CommonFnc.emptyCheckString("stpltVer", request);
        String stpltSbst = CommonFnc.emptyCheckString("stpltSbst", request);
        String useYn = CommonFnc.emptyCheckString("useYn", request);
        String perdYn = CommonFnc.emptyCheckString("perdYn", request);
        String mandYn = CommonFnc.emptyCheckString("mandYn", request);
        int changeStpltSeq = CommonFnc.emptyCheckInt("changeStpltSeq", request);
        String stopStpltYn = CommonFnc.emptyCheckString("stopStpltYn", request);

        ServiceTermsVO item = new ServiceTermsVO();

        item.setSvcSeq(svcSeq);
        item.setStpltType(stpltType);
        item.setStpltVer(stpltVer);

        // escape 문자들 원복하여 저장
        stpltSbst = CommonFnc.unescapeStr(stpltSbst);

        item.setStpltSbst(stpltSbst);
        item.setCretrId(userVO.getMbrId());
        item.setDelYn("N");
        item.setUseYn(useYn);
        item.setPerdYn(perdYn);
        item.setMandYn(mandYn);

        if (perdYn.equals("Y")) {
            String stDt = CommonFnc.emptyCheckString("stDt", request);
            String fnsDt = CommonFnc.emptyCheckString("fnsDt", request);

            item.setStDt(stDt);
            item.setFnsDt(fnsDt);
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcTermsService.insertSvcTerms(item);
            if (result == 1) {
                if (stopStpltYn.equals("Y")) { // 사용중인 약관 전체 철회
                    item.setSvcSeq(svcSeq);
                    item.setStpltType(stpltType);
                    item.setAmdrId(userVO.getMbrId());
                    item.setStopStpltYn(stopStpltYn);
                    item.setChangeStpltSeq(0);
                    item.setStpltSeq(item.getStpltSeq());
                    int change = svcTermsService.updateSvcTerms(item);
                    if (change < 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                        return rsModel.getModelAndView();
                    }
                } else {
                    if (changeStpltSeq != 0) {
                        item.setAmdrId(userVO.getMbrId());
                        item.setChangeStpltSeq(changeStpltSeq);
                        int change = svcTermsService.updateSvcTerms(item);
                        if (change != 1) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                            return rsModel.getModelAndView();
                        }
                    }
                }
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setResultType("insertSvcTerms");
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView editServiceTerms(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception { // put
        String checkString = CommonFnc.requiredChecking(request,
                "stpltSeq,svcSeq,stpltType,stpltVer,stpltSbst,useYn,perdYn,mandYn");
        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage("invalid parameter(" + checkString + ")");
            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        int stpltSeq = CommonFnc.emptyCheckInt("stpltSeq", request);
        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        String stpltType = CommonFnc.emptyCheckString("stpltType", request);
        String stpltSbst = CommonFnc.emptyCheckString("stpltSbst", request);
        String stpltVer = CommonFnc.emptyCheckString("stpltVer", request);
        String perdYn = CommonFnc.emptyCheckString("perdYn", request);
        String useYn = CommonFnc.emptyCheckString("useYn", request);
        String mandYn = CommonFnc.emptyCheckString("mandYn", request);
        int changeStpltSeq = CommonFnc.emptyCheckInt("changeStpltSeq", request);
        String stopStpltYn = CommonFnc.emptyCheckString("stopStpltYn", request);

        ServiceTermsVO item = new ServiceTermsVO();

        item.setStpltSeq(stpltSeq);
        item.setSvcSeq(svcSeq);
        item.setStpltType(stpltType);

        // escape 문자들 원복하여 저장
        stpltSbst = CommonFnc.unescapeStr(stpltSbst);

        item.setStpltSbst(stpltSbst);
        item.setStpltVer(stpltVer);
        item.setAmdrId(userVO.getMbrId());
        item.setPerdYn(perdYn);
        item.setUseYn(useYn);
        item.setMandYn(mandYn);
        item.setDelYn("N");

        if (perdYn.equals("Y")) {
            String stDt = CommonFnc.emptyCheckString("stDt", request);
            String fnsDt = CommonFnc.emptyCheckString("fnsDt", request);

            item.setStDt(stDt);
            item.setFnsDt(fnsDt);
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcTermsService.updateSvcTerms(item);
            if (result == 1) {
                if (stopStpltYn.equals("Y")) { // 사용중인 약관 전체 철회
                    item.setSvcSeq(svcSeq);
                    item.setStpltType(stpltType);
                    item.setAmdrId(userVO.getMbrId());
                    item.setStopStpltYn(stopStpltYn);
                    item.setChangeStpltSeq(0);
                    item.setStpltSeq(item.getStpltSeq());
                    int change = svcTermsService.updateSvcTerms(item);
                    if (change < 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                        return rsModel.getModelAndView();
                    }
                } else {
                    if (changeStpltSeq != 0) {
                        item.setAmdrId(userVO.getMbrId());
                        item.setChangeStpltSeq(changeStpltSeq);
                        int change = svcTermsService.updateSvcTerms(item);
                        if (change != 1) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                            return rsModel.getModelAndView();
                        }
                    }
                }
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView delServiceTerms(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception { // delete
        String stpltSeqList = CommonFnc.emptyCheckString("stpltSeqList", request);
        String[] delStpltSeqList = stpltSeqList.split(",");

        if (delStpltSeqList.length == 0) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            return rsModel.getModelAndView();
        }

        String delYn = request.getParameter("delYn") == null ? "Y" : request.getParameter("delYn");

        ServiceTermsVO item = new ServiceTermsVO();

        item.setDelYn(delYn);
        item.setAmdrId(userVO.getMbrId());
        item.setDelStpltSeqList(delStpltSeqList);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcTermsService.deleteSvcTerms(item);
            if (result == 1) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getServiceTerms(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception { // get
        int stpltSeq = request.getParameter("stpltSeq") == null ? 0 : Integer.parseInt(request.getParameter("stpltSeq"));
        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        String comnCdValue = CommonFnc.emptyCheckString("comnCdValue", request); // select box에서 선택된 약관 값
        String searchType = CommonFnc.emptyCheckString("searchType", request);
        String existVer = CommonFnc.emptyCheckString("existVer", request);
        String useYn = CommonFnc.emptyCheckString("useYn", request);
        ServiceTermsVO item = new ServiceTermsVO();

        item.setSvcSeq(svcSeq);
        item.setStpltType(comnCdValue);
        if (useYn.equals("Y")) {
            item.setUseYn(useYn);
        }

        if (stpltSeq != 0  && !existVer.equals("Y")) {
            item.setStpltSeq(stpltSeq);

            /** 해당 약관 상세정보 */
            ServiceTermsVO resultItem = svcTermsService.svcTermsInfo(item);

            if (resultItem == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {

                /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));
                resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
            }
            rsModel.setResultType("svcTermsInfo");
        } else {
            // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
            if (stpltSeq == 0 && CommonFnc.checkReqParameter(request, "stpltSeq")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                return rsModel.getModelAndView();
            }

            List<ServiceTermsVO> resultItemList;

            /** 검색여부 */
            String searchConfirm = request.getParameter("_search") == null ? "false"
                    : request.getParameter("_search");
            item.setSearchConfirm(searchConfirm);

            if (searchConfirm.equals("true")) {
                /** 검색 카테고리 */
                String searchField = request.getParameter("searchField") == null ? ""
                        : request.getParameter("searchField");
                /** 검색어 */
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.setSearchTarget(searchField);
                item.setSearchKeyword(searchString);
            }
            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            /** 현재 페이지 */
            int currentPage = request.getParameter("page") == null ? 1
                    : Integer.parseInt(request.getParameter("page"));
            /** 검색에 출력될 개수 */
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));
            /** 정렬할 필드 */
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");
            /** 정렬 방법 */
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            if (!existVer.equals("Y")) {
                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);
            }

            resultItemList = svcTermsService.svcTermsInfoList(item);
            if (resultItemList.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                /* 개인정보 마스킹 (등록자) */
                for (int i = 0; i < resultItemList.size(); i++) {
                    resultItemList.get(i).setCretrNm(CommonFnc.getNameMask(resultItemList.get(i).getCretrNm()));
                }

                /** 가져온 리스트 총 개수 */
                int totalCount = svcTermsService.svcTermsInfoListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItemList);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", (totalCount -1) / limit);
                if (!existVer.equals("Y")) {
                    rsModel.setResultType("svcTermsInfoList");
                } else {
                    rsModel.setResultType("svcTermsExistInfoList");
                }
            }
        }
        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
    }

    /**
     * 서비스 공지사항 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    /*
    @RequestMapping(value = "/api/svcTerms")
    public ModelAndView svcNoticeProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAllowMember(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");

        ServiceNoticeVO item = new ServiceNoticeVO();
        FileVO fileVO = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // Checking Mendatory S
            String invalidParams = CommonFnc.requiredChecking(request,"svcNoticeTitle,svcNoticeType,startDt,endDt,svcNoticeSbst");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String svcNoticeTitle = CommonFnc.emptyCheckString("svcNoticeTitle", request);
            svcNoticeTitle = CommonFnc.unescapeStr(svcNoticeTitle);


            int svcNoticeType = CommonFnc.emptyCheckInt("svcNoticeType", request);
            String startDt = CommonFnc.emptyCheckString("startDt", request);
            String endDt = CommonFnc.emptyCheckString("endDt", request);
            String svcNoticeSbst = CommonFnc.emptyCheckString("svcNoticeSbst", request);
            String delYn = CommonFnc.emptyCheckString("delYn", request);

            item.setSvcNoticeTitle(svcNoticeTitle);
            item.setSvcNoticeType(svcNoticeType);
            item.setStDt(startDt);
            item.setFnsDt(endDt);
            item.setSvcNoticeSbst(svcNoticeSbst);
            item.setDelYn(delYn);
            item.setLoginId(userVO.getMbrId());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = svcNoticeService.insertSvcNotice(item);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("svcNoticeSeq", item.getSvcNoticeSeq());
                    rsModel.setResultType("svcNoticeInsert");
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch(Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String invalidParams = CommonFnc.requiredChecking(request,"svcNoticeSeq,svcNoticeTitle,svcNoticeType,startDt,endDt,svcNoticeSbst");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int svcNoticeSeq = CommonFnc.emptyCheckInt("svcNoticeSeq", request);
            String svcNoticeTitle = CommonFnc.emptyCheckString("svcNoticeTitle", request);
            svcNoticeTitle = CommonFnc.unescapeStr(svcNoticeTitle);

            int svcNoticeType = CommonFnc.emptyCheckInt("svcNoticeType", request);
            String startDt = CommonFnc.emptyCheckString("startDt", request);
            String endDt = CommonFnc.emptyCheckString("endDt", request);
            String svcNoticeSbst = CommonFnc.emptyCheckString("svcNoticeSbst", request);
            String delYn = CommonFnc.emptyCheckString("delYn", request);

            item.setSvcNoticeSeq(svcNoticeSeq);
            item.setSvcNoticeTitle(svcNoticeTitle);
            item.setSvcNoticeType(svcNoticeType);
            item.setStDt(startDt);
            item.setFnsDt(endDt);
            item.setSvcNoticeSbst(svcNoticeSbst);
            item.setDelYn(delYn);
            item.setLoginId(userVO.getMbrId());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = svcNoticeService.updateSvcNotice(item);
                if (result == 1) {
                    String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);
                    List<String> deleteFilePath = new ArrayList<String>();

                    if (delFileSeqs != "") {
                        String[] delFileSeq = delFileSeqs.split(",");

                        for (int i = 0; i < delFileSeq.length; i++) {
                            item.setFileSeq(Integer.parseInt(delFileSeq[i]));
                            ServiceNoticeVO data = svcNoticeService.fileInfo(item);
                            String filePath = data.getFilePath();
                            if (svcNoticeService.attachFileDelete(item) == 1) {
                                deleteFilePath.add(filePath);
                            }
                        }
                    }

                    for (int i = 0; i < deleteFilePath.size(); i++) {
                        File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                        f.delete();
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.rollback(status);
                }
            } catch (Exception e){
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if(!status.isCompleted()){
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            String noticeSeqList = CommonFnc.emptyCheckString("delSvcNoticeSeqList", request);
            String[] delNoticeSeqList = noticeSeqList.split(",");

            if (delNoticeSeqList.length == 0) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No notice list to delete.)");
                return rsModel.getModelAndView();
            }

            List<Integer> deleteList = new ArrayList<Integer>();
            for(int i = 0; i < delNoticeSeqList.length; i++) {
                deleteList.add(Integer.parseInt(delNoticeSeqList[i]));
            }

            item.setLoginId(userVO.getMbrId());
            item.setDeleteSvcNoticeSeqList(deleteList);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                svcNoticeService.deleteSvcNotice(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } catch(Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            int svcNoticeSeq = CommonFnc.emptyCheckInt("svcNoticeSeq", request);

            if (svcNoticeSeq != 0) {
                item.setSvcNoticeSeq(svcNoticeSeq);
                fileVO.setFileContsSeq(svcNoticeSeq);
                fileVO.setFileSe("SVC_NOTI");
                List<FileVO> flist = fileService.fileList(fileVO);

                ServiceNoticeVO resultItem = svcNoticeService.svcNoticeDetail(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    String searchType = CommonFnc.emptyCheckString("searchType", request);
                    boolean isAllowNotice = this.isAllowNotice(userVO, resultItem, searchType);
                    if (!isAllowNotice) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    // 서비스 공지사항 수정을 위해 상세 정보 요청 시에는 조회 수 업데이트 하지 않음.
                    if (!searchType.equals("edit")) {
                        svcNoticeService.updateSvcNoticeRetvNum(item);
                        resultItem.setRetvNum(resultItem.getRetvNum() + 1);
                    }

                    // 개인정보 마스킹 (등록자 이름)
                    resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("fileList",flist);
                    rsModel.setResultType("svcNoticeDetail");
                }
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (svcNoticeSeq == 0 && CommonFnc.checkReqParameter(request, "svcNoticeSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if(searchConfirm.equals("true")){
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    if (searchField =="svcNoticeTitle") {
                        searchField = "SVC_NOTICE_TITLE";
                    } else if (searchField =="cretrNm") {
                        searchField = "CRETR_NM";
                    }

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }

                //page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);

                int page = (currentPage - 1) * limit + 1;
                int pageEnd = page + limit - 1;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("num")) {
                    sidx="SVC_NOTICE_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<ServiceNoticeVO> resultList = svcNoticeService.svcNoticeList(item);

                if (resultList.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    // 개인정보 마스킹 (등록자 이름)
                    for (int i = 0; i < resultList.size(); i++) {
                        resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
                    }

                    int totalCount = svcNoticeService.svcNoticeListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultList);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("svcNoticeList");
                }
            }
        }

        return rsModel.getModelAndView();
    }
    */

}
