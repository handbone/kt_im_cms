/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceterms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.serviceterms.dao.ServiceTermsDAO;
import kt.com.im.cms.serviceterms.vo.ServiceTermsVO;

/**
 *
 * 서비스 공지사항 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.11.30
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("ServiceTermsService")
public class ServiceTermsServiceImpl implements ServiceTermsService {

    @Resource(name = "ServiceTermsDAO")
    private ServiceTermsDAO svcTermsDAO;

    /**
     * 최신의 서비스 약관 리스트 조회
     * 
     * @param ServiceTermsVO
     * @return 서비스 약관 조회 목록 결과
     */
    @Override
    public List<ServiceTermsVO> getLatestSvcTermsInfoList(ServiceTermsVO vo) {
        return svcTermsDAO.getLatestSvcTermsInfoList(vo);
    }

    /**
     * 서비스 약관 상세 정보
     *
     * @param ServiceTermsVO
     * @return 서비스 약관 정보
     */
    public ServiceTermsVO svcTermsInfo(ServiceTermsVO item) {
        return svcTermsDAO.svcTermsInfo(item);
    }

    /**
     * 검색조건에 해당되는 서비스 약관 리스트 정보
     *
     * @param ServiceTermsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ServiceTermsVO> svcTermsInfoList(ServiceTermsVO data) {
        return svcTermsDAO.svcTermsInfoList(data);
    }

    /**
     * 검색조건에 해당되는 서비스 약관 리스트의 총 개수
     *
     * @param ServiceTermsVO
     * @return 총 서비스 약관 수 /
     */
    @Override
    public int svcTermsInfoListTotalCount(ServiceTermsVO data) {
        int res = svcTermsDAO.svcTermsInfoListTotalCount(data);
        return res;
    }

    /**
     * 서비스 약관 등록
     *
     * @param ServiceTermsVO
     * @return 서비스 약관 등록 성공 여부
     */
    @Override
    public int insertSvcTerms(ServiceTermsVO data) {
        int res = svcTermsDAO.insertSvcTerms(data);
        return res;
    }

    /**
     * 서비스 약관 정보 수정
     *
     * @param ServiceTermsVO
     * @return 서비스 약관 정보 수정 성공 여부
     */
    @Override
    public int updateSvcTerms(ServiceTermsVO data) {
        int res = svcTermsDAO.updateSvcTerms(data);
        return res;
    }

    /**
     * 서비스 약관 정보 삭제
     *
     * @param ServiceTermsVO
     * @return 서비스 약관 정보 삭제 성공 여부
     */
    @Override
    public int deleteSvcTerms(ServiceTermsVO data) {
        int res = svcTermsDAO.deleteSvcTerms(data);
        return res;
    }

    @Override
    public int updateSvcTermsPstngFinish(ServiceTermsVO data) {
        int res = svcTermsDAO.updateSvcTermsPstngFinish(data);
        return res;
    }

    /**
     * 비활성화될 서비스 약관 리스트 정보
     *
     * @param ServiceTermsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ServiceTermsVO> searchFinishSvcTerms(ServiceTermsVO data) {
        return svcTermsDAO.searchFinishSvcTerms(data);
    }

    /**
     * 당일을 기준으로 예약된 약관보다 게시 시작 일시가 과거인 약관 리스트 정보
     *
     * @param ServiceTermsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ServiceTermsVO> searchPastSvcTerms(ServiceTermsVO data) {
        return svcTermsDAO.searchPastSvcTerms(data);
    }

}
