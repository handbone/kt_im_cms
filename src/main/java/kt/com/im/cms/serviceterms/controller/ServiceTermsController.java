/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceterms.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;

/**
*
* 약관 관련 페이지 컨트롤러
*
* @author A2TEC
* @since 2019
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 4. 16.   A2TEC      최초생성
*
*
* </pre>
*/
@Component
@Controller
public class ServiceTermsController {

    /**
     * 약관관리 리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/terms", method = RequestMethod.GET)
    public ModelAndView serviceTermsList(ModelAndView mv) {
        mv.setViewName("/views/onlineService/terms/ServiceTermsList");
        return mv;
    }

    /**
     * 약관관리 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/terms/{stpltSeq}", method = RequestMethod.GET)
    public ModelAndView serviceTermsDetail(ModelAndView mv, @PathVariable(value="stpltSeq") String stpltSeq) {
        mv.addObject("stpltSeq", stpltSeq);
        mv.setViewName("/views/onlineService/terms/ServiceTermsDetail");
        return mv;
    }

    /**
     * 약관관리 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/terms/regist", method = RequestMethod.GET)
    public ModelAndView serviceTermsRegist(ModelAndView mv, HttpServletRequest request) {
        mv.addObject("svcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.addObject("stpltType", CommonFnc.emptyCheckString("stpltType", request));
        mv.setViewName("/views/onlineService/terms/ServiceTermsRegist");
        return mv;
    }

    /**
     * 약관관리 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/terms/{stpltSeq}/edit", method = RequestMethod.GET)
    public ModelAndView serviceTermsEdit(ModelAndView mv, @PathVariable(value="stpltSeq") String stpltSeq) {
        mv.addObject("stpltSeq", stpltSeq);
        mv.setViewName("/views/onlineService/terms/ServiceTermsEdit");
        return mv;
    }
}
