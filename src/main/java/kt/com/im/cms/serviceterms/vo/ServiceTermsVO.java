/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceterms.vo;

import java.io.Serializable;
import java.util.List;

/**
*
* ServiceTermsVO
*
 * @author A2TEC
 * @since 2019.04.02
 * @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 04. 02.   A2TEC      최초생성
*
*
* </pre>
*/

public class ServiceTermsVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7729629628489918368L;

    /** 서비스 약관 번호 */
    private int stpltSeq;

    /** 서비스 고유 번호 */
    private int svcSeq;

    /** 서비스 명 */
    private String svcNm;

    /** 서비스 약관 구분 코드 (01: 이용약관, 02: 개인정보제공) */
    private String stpltType;

    /** 서비스 약관 구분 명 */
    private String stpltTypeNm;

    /** 서비스 약관 내용 */
    private String stpltSbst;

    /** 서비스 약관 버전 정보 */
    private String stpltVer;

    /** 생성자 아이디 */
    private String cretrId;

    /** 생성 일시 */
    private String cretDt;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** 등록 일시 */
    private String regDt;

    /** 등록자 */
    private String cretrNm;

    /** 서비스 약관 삭제 여부 */
    private String delYn;

    /** 서비스 약관 사용 여부 */
    private String useYn;

    /** 서비스 약관 노출 기간(시작) */
    private String stDt;

    /** 서비스 약관 노출 기간(종료) */
    private String fnsDt;

    /** 기간 설정 여부 */
    private String perdYn;

    /** 삭제할 약관 목록 */
    private int[] delStpltSeqList;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** 사용 여부 변경될 약관 번호 */
    private int changeStpltSeq;

    /** 사용 여부 비활성화 */
    private String stopStpltYn;

    /** 필수 여부 확인 */
    private String mandYn;

    /** foreach 리스트 정보 */
    private List list;

    public int getStpltSeq() {
        return stpltSeq;
    }

    public void setStpltSeq(int stpltSeq) {
        this.stpltSeq = stpltSeq;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getStpltType() {
        return stpltType;
    }

    public void setStpltType(String stpltType) {
        this.stpltType = stpltType;
    }

    public String getStpltTypeNm() {
        return stpltTypeNm;
    }

    public void setStpltTypeNm(String stpltTypeNm) {
        this.stpltTypeNm = stpltTypeNm;
    }

    public String getStpltSbst() {
        return stpltSbst;
    }

    public void setStpltSbst(String stpltSbst) {
        this.stpltSbst = stpltSbst;
    }

    public String getStpltVer() {
        return stpltVer;
    }

    public void setStpltVer(String stpltVer) {
        this.stpltVer = stpltVer;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getStDt() {
        return stDt;
    }

    public void setStDt(String stDt) {
        this.stDt = stDt;
    }

    public String getFnsDt() {
        return fnsDt;
    }

    public void setFnsDt(String fnsDt) {
        this.fnsDt = fnsDt;
    }

    public String getPerdYn() {
        return perdYn;
    }

    public void setPerdYn(String perdYn) {
        this.perdYn = perdYn;
    }

    public int[] getDelStpltSeqList() {
        return delStpltSeqList;
    }

    public void setDelStpltSeqList(String[] delStpltSeqList) {
        this.delStpltSeqList = new int[delStpltSeqList.length];
        for (int i = 0; i < delStpltSeqList.length; i++) {
            this.delStpltSeqList[i] = Integer.parseInt(delStpltSeqList[i]);
        }
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public int getChangeStpltSeq() {
        return changeStpltSeq;
    }

    public void setChangeStpltSeq(int changeStpltSeq) {
        this.changeStpltSeq = changeStpltSeq;
    }

    public String getMandYn() {
        return mandYn;
    }

    public void setMandYn(String mandYn) {
        this.mandYn = mandYn;
    }

    public String getStopStpltYn() {
        return stopStpltYn;
    }

    public void setStopStpltYn(String stopStpltYn) {
        this.stopStpltYn = stopStpltYn;
    }

    public void setList(List list) {
        this.list = list;
    }

}
