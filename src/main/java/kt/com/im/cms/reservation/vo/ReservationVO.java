/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.reservation.vo;

import java.io.Serializable;

/**
 *
 * 예약 정보 관련 객체
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 01.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
public class ReservationVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2843195678011954647L;

    /** 예약 번호 */
    private int resveSeq;

    /** 예약 날짜 */
    private String resveDt;

    /** 고객 명 */
    private String cstmrNm;

    /** 예약 이용 시간 */
    private String usageDate;

    /** 고객 전화 번호 */
    private String cstmrTelNo;

    /** 매장 번호 */
    private int storSeq;

    /** 매장명 */
    private String storNm;

    /** 장비 번호 */
    private int devSeq;

    /** 장비 유형 명 */
    private String devTypeNm;

    /** 사용 시간 시 */
    private int usageHour;

    /** 사용 시간 분 */
    private int usageMinute;

    /**  */
    private String regDt;

    /** 정렬 칼럼명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 필드 */
    private String searchTarget;

    /** 검색 키워드 */
    private String searchKeyword;

    /** 콘텐츠 리스트 조회시 검색 여부 확인 */
    private String searchConfirm;

    /** 시작 일시 */
    private String startDt;

    /** 종료 일시 */
    private String endDt;

    /** offset */
    private int offset;

    /** limit (한 페이지에 보여질 갯수) */
    private int limit;

    /** 태그 카테고리 장비 종류 명 */
    private String devCtgNm;

    /** 제한 횟수 */
    private int lmtCnt;

    /** 시작 시간 */
    private String stTime;

    /** 종료 시간 */
    private String fnsTime;

    /** 디바이스 아이디 */
    private String devId;

    /** 점포 코드 */
    private String storCode;

    /** 예약 시간 설정 주기 */
    private int rsrvTimeSetCycle;

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public int getDevSeq() {
        return devSeq;
    }

    public void setDevSeq(int devSeq) {
        this.devSeq = devSeq;
    }

    public String getDevTypeNm() {
        return devTypeNm;
    }

    public void setDevTypeNm(String devTypeNm) {
        this.devTypeNm = devTypeNm;
    }

    public int getUsageHour() {
        return usageHour;
    }

    public void setUsageHour(int usageHour) {
        this.usageHour = usageHour;
    }

    public int getUsageMinute() {
        return usageMinute;
    }

    public void setUsageMinute(int usageMinute) {
        this.usageMinute = usageMinute;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getStartDt() {
        return startDt;
    }

    public void setStartDt(String startDt) {
        this.startDt = startDt;
    }

    public String getEndDt() {
        return endDt;
    }

    public void setEndDt(String endDt) {
        this.endDt = endDt;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getDevCtgNm() {
        return devCtgNm;
    }

    public void setDevCtgNm(String devCtgNm) {
        this.devCtgNm = devCtgNm;
    }

    public int getLmtCnt() {
        return lmtCnt;
    }

    public void setLmtCnt(int lmtCnt) {
        this.lmtCnt = lmtCnt;
    }

    public String getStTime() {
        return stTime;
    }

    public void setStTime(String stTime) {
        this.stTime = stTime;
    }

    public String getFnsTime() {
        return fnsTime;
    }

    public void setFnsTime(String fnsTime) {
        this.fnsTime = fnsTime;
    }

    public String getDevId() {
        return devId;
    }

    public void setDevId(String devId) {
        this.devId = devId;
    }

    public String getStorCode() {
        return storCode;
    }

    public void setStorCode(String storCode) {
        this.storCode = storCode;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getResveSeq() {
        return resveSeq;
    }

    public void setResveSeq(int resveSeq) {
        this.resveSeq = resveSeq;
    }

    public String getUsageDate() {
        return usageDate;
    }

    public void setUsageDate(String usageDate) {
        this.usageDate = usageDate;
    }

    public String getResveDt() {
        return resveDt;
    }

    public void setResveDt(String resveDt) {
        this.resveDt = resveDt;
    }

    public String getCstmrNm() {
        return cstmrNm;
    }

    public void setCstmrNm(String cstmrNm) {
        this.cstmrNm = cstmrNm;
    }

    public String getCstmrTelNo() {
        return cstmrTelNo;
    }

    public void setCstmrTelNo(String cstmrTelNo) {
        this.cstmrTelNo = cstmrTelNo;
    }

    public int getRsrvTimeSetCycle() {
        return rsrvTimeSetCycle;
    }

    public void setRsrvTimeSetCycle(int rsrvTimeSetCycle) {
        this.rsrvTimeSetCycle = rsrvTimeSetCycle;
    }

}
