/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.reservation.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.reservation.service.ReservationService;
import kt.com.im.cms.reservation.vo.ReservationVO;

/**
 *
 * 이용권 관련 메뉴에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 13.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class ReservationApi {

    @Resource(name = "ReservationService")
    private ReservationService reservationService;

    @Autowired
    private DataSourceTransactionManager transactionManager;

    /**
     * 예약 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    /**
     * API RESERVATION RESERVATION Process
     */
    @RequestMapping(value = "/api/reservation")
    public ModelAndView ContentsDownStats(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/reservation/reservationProcess");

        // 로그인 되지 않았거나 검수자, CP사 관계자인 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ReservationVO item = new ReservationVO();

        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

                String type = CommonFnc.emptyCheckString("type", request);

                if (type.equals("setting")) {
                    String checkString = CommonFnc.requiredChecking(request, "stTime,fnsTime,rsrvTimeSetCycle");
                    if (!checkString.equals("")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                        rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }

                    String stTime = CommonFnc.emptyCheckString("stTime", request);
                    String fnsTime = CommonFnc.emptyCheckString("fnsTime", request);
                    int rsrvTimeSetCycle = CommonFnc.emptyCheckInt("rsrvTimeSetCycle", request);

                    int stHour = Integer.parseInt(stTime.split(":")[0]);
                    int stMinute = Integer.parseInt(stTime.split(":")[1]);
                    int fnsHour = Integer.parseInt(fnsTime.split(":")[0]);
                    int fnsMinute = Integer.parseInt(fnsTime.split(":")[1]);

                    if (stTime != null && fnsTime != null) {
                        String stHourStr = String.format("%02d", stHour);
                        String stMinuteStr = String.format("%02d", stMinute);
                        String fnsHourStr = String.format("%02d", fnsHour);
                        String fnsMinuteStr = String.format("%02d", fnsMinute);

                        stTime = stHourStr + ":" + stMinuteStr;
                        fnsTime = fnsHourStr + ":" + fnsMinuteStr;
                    }

                    item.setStTime(stTime);
                    item.setFnsTime(fnsTime);
                    item.setStorSeq(storSeq);
                    item.setRsrvTimeSetCycle(rsrvTimeSetCycle);

                    int result = reservationService.reservationSettingUpdate(item);

                    if (result == 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    }

                    rsModel.setResultType("reservationSettinUpdate");

                } else {

                }
            } else if (Method.equals("DELETE")) { // delete
                String checkString = CommonFnc.requiredChecking(request, "resveSeq");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int resveSeq = CommonFnc.emptyCheckInt("resveSeq", request);

                item.setResveSeq(resveSeq);

                int result = reservationService.resveDelete(item);

                if (result == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
            } else { // post
                String checkString = CommonFnc.requiredChecking(request,
                        "devCtgNm,usageDate,storSeq,cstmrNm,cstmrTelNo");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

                String devCtgNm = CommonFnc.emptyCheckString("devCtgNm", request);
                String usageDate = CommonFnc.emptyCheckString("usageDate", request);
                String cstmrNm = CommonFnc.emptyCheckString("cstmrNm", request);
                String cstmrTelNo = CommonFnc.emptyCheckString("cstmrTelNo", request);

                item.setDevCtgNm(devCtgNm);
                item.setUsageDate(usageDate);
                item.setStorSeq(storSeq);
                item.setCstmrNm(cstmrNm);
                item.setCstmrTelNo(cstmrTelNo);

                int result = reservationService.resveInsert(item);

                if (result == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setResultType("resveInsert");
                    rsModel.setData("item", item);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("resveInfo");
            }
        } else { // get
            int resveSeq = CommonFnc.emptyCheckInt("resveSeq", request);

            if (resveSeq != 0) {
                item.setResveSeq(resveSeq);

                ReservationVO resultItem = reservationService.resveInfo(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (예약자 이름, 전화번호) */
                    resultItem.setCstmrNm(CommonFnc.getNameMask(resultItem.getCstmrNm()));
                    resultItem.setCstmrTelNo(CommonFnc.getTelnoMask(resultItem.getCstmrTelNo()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }

                rsModel.setResultType("resveInfo");

            } else {
                String checkString = CommonFnc.requiredChecking(request, "storSeq");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                String usageDate = CommonFnc.emptyCheckString("usageDate", request);

                item.setStorSeq(storSeq);
                item.setUsageDate(usageDate);

                List<ReservationVO> resultItem = reservationService.resveList(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (예약자 이름, 전화번호) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCstmrNm(CommonFnc.getNameMask(resultItem.get(i).getCstmrNm()));
                        resultItem.get(i).setCstmrTelNo(CommonFnc.getTelnoMask(resultItem.get(i).getCstmrTelNo()));
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }
                rsModel.setResultType("resveList");
            }
        }
        return rsModel.getModelAndView();
    }

    private boolean isNotAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

}
