/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.reservation.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.reservation.vo.ReservationVO;

/**
 *
 * 예약 정보 관련 객체
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 01.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
@Repository("ReservationDAO")
@Transactional
public class ReservationDAOImpl extends mysqlAbstractMapper implements ReservationDAO {

    /**
     * 예약 상세 정보
     *
     * @param ReservationVO
     * @return 예약 상세 정보 조회 결과
     */
    @Override
    public ReservationVO resveInfo(ReservationVO data) {
        return selectOne("ReservationDAO.resveInfo", data);
    }

    /**
     * 예약 정보 리스트
     *
     * @param ReservationVO
     * @return 예약 정보 리스트 목록 결과
     */
    @Override
    public List<ReservationVO> resveList(ReservationVO data) {
        return selectList("ReservationDAO.resveList", data);
    }

    /**
     * 매장 등록 시 매장 예약 설정 정보 등록
     *
     * @param ReservationVO
     * @return
     */
    @Override
    public void resveSetInsert(ReservationVO data) {
        insert("ReservationDAO.resveSetInsert", data);
    }

    /**
     * 예약 시간 설정 정보 업데이트
     *
     * @param ReservateVO
     * @return 예약 시간 설정 정보 업데이트 성공 여부
     */
    @Override
    public int reservationSettingUpdate(ReservationVO data) {
        int res = update("ReservationDAO.reservationSettingUpdate", data);
        return res;
    }

    /**
     * 예약 정보 삭제
     *
     * @param ReservationVO
     * @return 예약 정보 삭제 성공 여부
     */
    @Override
    public int resveDelete(ReservationVO data) {
        int res = delete("ReservationDAO.resveDelete", data);
        return res;
    }

    /**
     * 예약 정보 등록
     *
     * @param ReservationVO
     * @return 예약 정보 등록 성공 여부
     */
    @Override
    public int resveInsert(ReservationVO data) {
        int res = insert("ReservationDAO.resveInsert", data);
        return res;
    }

}
