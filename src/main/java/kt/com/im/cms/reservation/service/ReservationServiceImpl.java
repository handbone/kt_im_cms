/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.reservation.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.reservation.dao.ReservationDAO;
import kt.com.im.cms.reservation.vo.ReservationVO;

/**
 *
 * 이용권 발급 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 23.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
@Service("ReservationService")
public class ReservationServiceImpl implements ReservationService {

    @Resource(name = "ReservationDAO")
    private ReservationDAO reservationDAO;

    /**
     * 예약 상세 정보
     *
     * @param ReservateVO
     * @return 예약 상세 정보 조회 결과
     */
    @Override
    public ReservationVO resveInfo(ReservationVO data) {
        return reservationDAO.resveInfo(data);
    }

    /**
     * 검색조건에 해당되는 이용권 발급 리스트 정보
     *
     * @param ReservateVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ReservationVO> resveList(ReservationVO data) {
        return reservationDAO.resveList(data);
    }

    /**
     * 매장 등록 시 매장 예약 설정 정보 등록
     *
     * @param ReservationVO
     * @return
     */
    @Override
    public void resveSetInsert(ReservationVO data) {
        reservationDAO.resveSetInsert(data);
    }

    /**
     * 예약 시간 설정 정보 업데이트
     *
     * @param ReservationVO
     * @return 예약 시간 설정 정보 업데이트 성공 여부
     */
    @Override
    public int reservationSettingUpdate(ReservationVO data) {
        return reservationDAO.reservationSettingUpdate(data);
    }

    /**
     * 예약 정보 삭제
     *
     * @param ReservationVO
     * @return 예약 정보 삭제 성공 여부
     */
    @Override
    public int resveDelete(ReservationVO data) {
        return reservationDAO.resveDelete(data);
    }

    /**
     * 예약 정보 추가
     *
     * @param ReservationVO
     * @return 예약 정보 추가 성공 여부
     */
    @Override
    public int resveInsert(ReservationVO data) {
        return reservationDAO.resveInsert(data);
    }

}
