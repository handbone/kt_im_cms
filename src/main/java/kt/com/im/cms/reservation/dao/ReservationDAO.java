/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.reservation.dao;

import java.util.List;

import kt.com.im.cms.reservation.vo.ReservationVO;

/**
 *
 * 예약 정보 관련 객체
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 01.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public interface ReservationDAO {
    ReservationVO resveInfo(ReservationVO item);

    List<ReservationVO> resveList(ReservationVO item);

    void resveSetInsert(ReservationVO data);

    int reservationSettingUpdate(ReservationVO data);

    int resveDelete(ReservationVO data);

    int resveInsert(ReservationVO data);

}
