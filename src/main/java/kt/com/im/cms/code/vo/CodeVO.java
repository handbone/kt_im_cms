/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.code.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 코드 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.05.02
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 2.   A2TEC      최초생성
 *
 *
 * </pre>
 */
public class CodeVO implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3996231483088969474L;

    /** 코드 번호 */
    int comnCdSeq;

    /** 코드 분류 */
    String comnCdCtg;

    /** 코드 명 */
    String comnCdNm;

    /** 코드 값 */
    String comnCdValue;

    /** 삭제 여부 */
    String delYn;

    /** 검수 검색 여부 */
    String verify;

    /** 콘텐츠 상태 값 */
    String sttusVal;

    /** 리스트 값 */
    List list;

    /** 앱 구분 */
    String appType;

    /** 전시 진행상태 여뷰 */
    String display;

    public int getComnCdSeq() {
        return comnCdSeq;
    }

    public void setComnCdSeq(int comnCdSeq) {
        this.comnCdSeq = comnCdSeq;
    }

    public String getComnCdCtg() {
        return comnCdCtg;
    }

    public void setComnCdCtg(String comnCdCtg) {
        this.comnCdCtg = comnCdCtg;
    }

    public String getComnCdNm() {
        return comnCdNm;
    }

    public void setComnCdNm(String comnCdNm) {
        this.comnCdNm = comnCdNm;
    }

    public String getComnCdValue() {
        return comnCdValue;
    }

    public void setComnCdValue(String comnCdValue) {
        this.comnCdValue = comnCdValue;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getSttusVal() {
        return sttusVal;
    }

    public void setSttusVal(String sttusVal) {
        this.sttusVal = sttusVal;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

}
