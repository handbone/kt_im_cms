/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.code.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.code.service.CodeService;
import kt.com.im.cms.code.vo.CodeVO;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;

/**
 *
 * 코드 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.02
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 02.   A2TEC      최초생성
 * 2018. 6. 01.   정규현           remoteCode 추가 codeInfo [GET] Method
 * 2018. 6. 07.   정규현           codeInfo API 파라미터 contsSeq 추가 [상태값관련]
 * </pre>
 */

@Controller
public class CodeApi {

    private final static String viewName = "../resources/api/code/codeProcess";

    @Resource(name = "CodeService")
    private CodeService codeService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager txManager;

    /**
     * 공통 코드 관련 처리 API
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param comnCdCtg - 조건값 : 코드 분류
     * @param comnCdValue - 조건값 : 코드 값
     * @return modelAndView & API 메세지값 & WebStatus
     *         + GET - 검색조건에 따른 코드 리스트
     */
    @RequestMapping(value = "/api/codeInfo")
    public ModelAndView CodeInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            boolean isList = (request.getParameter("comnCdSeq") == null);
            if (isList)
                return this.getCodeList(request, rsModel);

            return this.getCode(request, rsModel);
        }

        if (method.equals("POST")) {
            return this.insertCode(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return this.updateCode(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return this.deleteCode(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getCodeList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String comnCdCtg = request.getParameter("comnCdCtg") == null ? "" : request.getParameter("comnCdCtg"); // 코드 분류
        String comnCdValue = request.getParameter("comnCdValue") == null ? "" : request.getParameter("comnCdValue"); // 코드
        String verify = request.getParameter("verify") == null ? "" : request.getParameter("verify"); // 코드 값
        String remoteCodes = request.getParameter("remoteCode") == null ? "" : request.getParameter("remoteCode"); // 제외코드
        String display = request.getParameter("display") == null ? "" : request.getParameter("display"); // 코드 값
        String delYn = request.getParameter("delYn") == null ? "N" : request.getParameter("delYn"); // 코드 값
        String sttusVal = CommonFnc.emptyCheckString("sttusVal", request);
        String appType = request.getParameter("appType") == null ? "" : request.getParameter("appType");

        CodeVO item = new CodeVO();
        item.setComnCdCtg(comnCdCtg);
        item.setComnCdValue(comnCdValue);
        item.setDelYn(delYn);
        item.setVerify(verify);
        item.setDisplay(display);
        item.setSttusVal(sttusVal);
        item.setAppType(appType);

        String[] remoteCode = remoteCodes.split(",");

        List<String> list = new ArrayList<String>();
        for (int i = 0; i < remoteCode.length; i++) {
            list.add(remoteCode[i]);
        }
        item.setList(list);

        List<CodeVO> resultItem = codeService.getCodeList(item);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("codeList");
        rsModel.setData("item", resultItem);

        return rsModel.getModelAndView();
    }

    private ModelAndView getCode(HttpServletRequest request, ResultModel rsModel) throws Exception {
        CodeVO vo = new CodeVO();
        vo.setComnCdSeq(Integer.parseInt(request.getParameter("comnCdSeq")));

        CodeVO resultItem = codeService.getCode(vo);
        if (resultItem == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("codeInfo");
        rsModel.setData("item", resultItem);

        return rsModel.getModelAndView();
    }

    private ModelAndView insertCode(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "comnCdCtg:{" + Validator.MAX_LENGTH + "=15};"
                + "comnCdNm:{" + Validator.MAX_LENGTH + "=50};"
                + "comnCdValue:{" + Validator.MAX_LENGTH + "=15}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        CodeVO vo = new CodeVO();
        vo.setComnCdCtg(request.getParameter("comnCdCtg"));
        vo.setComnCdNm(request.getParameter("comnCdNm"));
        vo.setComnCdValue(request.getParameter("comnCdValue"));

        boolean alreadyExists = (codeService.getDuplicationCode(vo) > 0);
        if (alreadyExists) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = codeService.insertCode(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView updateCode(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "comnCdSeq:{" + Validator.NUMBER + "};"
                + "comnCdNm:{" + Validator.MAX_LENGTH + "=50};"
                + "comnCdValue:{" + Validator.MAX_LENGTH + "=15}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        CodeVO vo = new CodeVO();
        vo.setComnCdSeq(Integer.parseInt(request.getParameter("comnCdSeq")));
        vo.setComnCdNm(request.getParameter("comnCdNm"));
        vo.setComnCdValue(request.getParameter("comnCdValue"));

        // 이전 데이터 조회
        CodeVO oldCodeVO = codeService.getCode(vo);
        if (oldCodeVO == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            return rsModel.getModelAndView();
        }

        // 데이터 변경 여부 확인
        boolean notChanged = oldCodeVO.getComnCdNm().equals(vo.getComnCdNm()) && oldCodeVO.getComnCdValue().equals(vo.getComnCdValue());
        if (notChanged) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            return rsModel.getModelAndView();
        }

        // 기존 코드 카테고리 내 중복 여부 확인
        vo.setComnCdCtg(oldCodeVO.getComnCdCtg());
        boolean alreadyExists = (codeService.getDuplicationCode(vo) > 0);
        if (alreadyExists) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = codeService.updateCode(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView deleteCode(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "comnCdSeq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        CodeVO vo = new CodeVO();
        vo.setComnCdSeq(Integer.parseInt(request.getParameter("comnCdSeq")));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = codeService.deleteCode(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_DELETE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 공통 코드 구분 목록 처리 API
     * @param comnCdCtg - 조건값 : 코드 분류
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/codeSecInfo")
    public ModelAndView CodeSectionInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            return this.getCodeSectionList(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getCodeSectionList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        CodeVO vo = new CodeVO();
        List<CodeVO> resultItem = codeService.getCodeSectionList(vo);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("codeList");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }
}
