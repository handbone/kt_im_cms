/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.code.service;

import java.util.List;

import kt.com.im.cms.code.vo.CodeVO;

/**
 *
 * 코드 관리에 관한 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.02
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 2.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public interface CodeService {

    List<CodeVO> getCodeList(CodeVO item) throws Exception;

    CodeVO getCode(CodeVO vo) throws Exception;

    int insertCode(CodeVO vo) throws Exception;

    int updateCode(CodeVO vo) throws Exception;

    int deleteCode(CodeVO vo) throws Exception;

    int getDuplicationCode(CodeVO vo) throws Exception;

    List<CodeVO> getCodeSectionList(CodeVO vo) throws Exception;
}
