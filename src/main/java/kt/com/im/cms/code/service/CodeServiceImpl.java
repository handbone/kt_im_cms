/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.code.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.code.dao.CodeDao;
import kt.com.im.cms.code.vo.CodeVO;


/**
*
* 클래스에 대한 상세 설명
*
* @author A2TEC
* @since 2018.05.02
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 2.   A2TEC      최초생성
*
*
* </pre>
*/


@Service("CodeService")
public class CodeServiceImpl implements CodeService {

    @Resource(name = "CodeDao")
    private CodeDao codeDao;

    /**
     * 코드 리스트 정보 조회
     * 
     * @param CodeVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public List<CodeVO> getCodeList(CodeVO vo) throws Exception {
        return codeDao.getCodeList(vo);
    }

    /**
     * 코드  정보 조회
     * 
     * @param CodeVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public CodeVO getCode(CodeVO vo) throws Exception {
        return codeDao.getCode(vo);
    }

    /**
     * 코드  정보 추가
     * 
     * @param CodeVO
     * @return 처리 결과 
     */
    @Override
    public int insertCode(CodeVO vo) throws Exception {
        return codeDao.insertCode(vo);
    }

    /**
     * 코드  정보 수정
     * 
     * @param CodeVO
     * @return 처리 결과 
     */
    @Override
    public int updateCode(CodeVO vo) throws Exception {
        return codeDao.updateCode(vo);
    }

    /**
     * 코드  정보 삭제
     * 
     * @param CodeVO
     * @return 처리 결과 
     */
    @Override
    public int deleteCode(CodeVO vo) throws Exception {
        return codeDao.deleteCode(vo);
    }

    /**
     * 코드  중복 여부 조회
     * 
     * @param CodeVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public int getDuplicationCode(CodeVO vo) throws Exception {
        return codeDao.getDuplicationCode(vo);
    }

    /**
     * 최상위 코드 리스트 정보
     * 
     * @param CodeVO
     * @return 최상위 코드 리스트 정보
     */
    @Override
    public List<CodeVO> getCodeSectionList(CodeVO vo) throws Exception {
        return codeDao.getCodeSectionList(vo);
    }
}
