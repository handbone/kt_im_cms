/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.launcher.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.AesAlgorithm;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ConfigProperty;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.common.util.jsonPostAsync;
import kt.com.im.cms.content.service.ContentService;
import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.device.service.DeviceService;
import kt.com.im.cms.device.vo.DeviceVO;
import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.genre.service.GenreService;
import kt.com.im.cms.genre.vo.GenreVO;
import kt.com.im.cms.launcher.service.LauncherService;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.LoginVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.olsvc.service.OlsvcService;
import kt.com.im.cms.olsvc.vo.OlsvcVO;
import kt.com.im.cms.product.service.ProductService;
import kt.com.im.cms.product.vo.ProductVO;
import kt.com.im.cms.release.service.ReleaseService;
import kt.com.im.cms.release.vo.ReleaseVO;
import kt.com.im.cms.reservate.service.ReservateService;
import kt.com.im.cms.reservate.vo.ReservateVO;
import kt.com.im.cms.store.service.StoreService;
import kt.com.im.cms.store.vo.StoreVO;

/**
 *
 * Launcher와 연동되는 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 7. 02.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class LauncherApi {

    @Resource(name = "ReservateService")
    private ReservateService reservateService;

    @Resource(name = "ProductService")
    private ProductService productService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "DeviceService")
    private DeviceService deviceService;

    @Resource(name = "FileService")
    private FileService fileService;

    @Resource(name = "LauncherService")
    private LauncherService launcherService;

    @Resource(name = "ReleaseService")
    private ReleaseService releaseService;

    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "GenreService")
    private GenreService genreService;

    @Resource(name = "StoreService")
    private StoreService storeService;

    @Resource(name = "OlsvcService")
    private OlsvcService olsvcService;

    @Autowired
    private DataSourceTransactionManager transactionManager;

    @Autowired
    MessageSource messageSource;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * Build Facility Process
     */
    @Inject
    private FileSystemResource fsResource;

    @Autowired
    private ConfigProperty configProperty;

    /**
     * 이용권 발급 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/tagAPI")
    public ModelAndView launcherTagAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/reservate/reservateProcess");

        request.setCharacterEncoding("UTF-8");
        ReservateVO item = new ReservateVO();
        ProductVO pItem = new ProductVO();
        MemberVO mItem = new MemberVO();

        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storCode,tagId,gsrProdCode");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String mbrTokn = request.getParameter("mbrTokn") == null ? "" : request.getParameter("mbrTokn");
            String storCode = request.getParameter("storCode") == null ? "" : request.getParameter("storCode");
            String encodeToken = "";
            encodeToken = mbrTokn.replaceAll(" ", "+");

            mItem.setStorCode(storCode);
            mItem.setMbrTokn(encodeToken);

            int tokenSearchResult = 0;
            try {
                tokenSearchResult = memberService.searchMemberToken(mItem);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                String tagId = request.getParameter("tagId") == null ? "" : request.getParameter("tagId");
                String gsrProdCode = request.getParameter("gsrProdCode") == null ? ""
                        : request.getParameter("gsrProdCode");

                if (tagId.length() == 46) {
                    DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                    def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                    TransactionStatus status = transactionManager.getTransaction(def);

                    try {
                        item.setStorCode(storCode);
                        item.setGsrProdCode(gsrProdCode);
                        item.setTagId(tagId);
                        item.setRegDt("");

                        // int year = Integer.parseInt(tagId.substring(19, 4));
                        int month = Integer.parseInt(tagId.substring(22, 24));
                        int day = Integer.parseInt(tagId.substring(24, 26));
                        int hour = Integer.parseInt(tagId.substring(26, 28));
                        int time = Integer.parseInt(tagId.substring(28, 30));

                        if (month < 1 || month > 12 || day < 1 || day > 31 || hour < 0 || hour > 24 || time > 59
                                || time < 0) {
                            item.setRegDt("sysdate");
                        }

                        item.setSettlSeq(0);
                        int result = reservateService.reservateInsert(item);
                        if (result == 1) {
                            // 상품 카테고리 조회
                            // pItem = new ProductsVo();
                            pItem.setGsrProdCode(gsrProdCode);
                            pItem.setStorCode(storCode);
                            List<ProductVO> resultCategoryItem = productService.productCategoryList(pItem);
                            for (int j = 0; j < resultCategoryItem.size(); j++) {
                                item.setProdSeq(resultCategoryItem.get(j).getProdSeq());
                                item.setDevCtgNm(resultCategoryItem.get(j).getDevCtgNm());
                                item.setLmtCnt(resultCategoryItem.get(j).getLmtCnt());
                                item.setDeduction(resultCategoryItem.get(j).getDeduction());
                                item.setTagSeq(0);
                                reservateService.reservateCategoryInsert(item);
                            }
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        } else {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                            rsModel.setResultMessage("Insert Fail Pelease Check Product State");
                            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        }
                        transactionManager.commit(status);
                    } catch (Exception e) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage("Insert Fail Pelease Check Product State");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        transactionManager.rollback(status);
                    } finally {
                        if (!status.isCompleted()) {
                            transactionManager.rollback(status);
                        }
                    }
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage("Not validate tokenID Info");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();
    }

    /**
     * Tag 이용 횟수 API (Device에서 사용)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/tagUsageCount")
    public ModelAndView TagUsageCount(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO item = new DeviceVO();
        ReservateVO rItem = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "tagId,devSeq,devTokn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            String tagId = request.getParameter("tagId") == null ? "" : request.getParameter("tagId"); // tag ID
            int devSeq = request.getParameter("devSeq") == null ? 0 : Integer.parseInt(request.getParameter("devSeq"));

            ProductVO pItem = new ProductVO();
            DeviceVO deviceVO = new DeviceVO();
            String devTokn = request.getParameter("devTokn") == null ? "" : request.getParameter("devTokn"); // tag ID
            String encodeToken = devTokn.replaceAll(" ", "+");

            deviceVO.setDevSeq(devSeq);
            deviceVO.setDevTokn(encodeToken);
            deviceVO.setStorSeq(0);
            if (deviceService.searchDeviceToken(deviceVO) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                item.setDevSeq(devSeq);
                item.setTagId(tagId);

                int result = 0;
                DeviceVO resultItem = deviceService.reservateUsageCount(item); // 태그 정보 총괄 조회
                if (resultItem != null) {
                    // 환불된 태그인지 조회
                    if (resultItem.getUseSttus().equals("B")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                        rsModel.setResultMessage("Returned tag");
                        return rsModel.getModelAndView();
                    }

                    if (resultItem.getDevSeq() == devSeq) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setResultType("tagUsageCountUpdate");
                        return rsModel.getModelAndView();
                    }

                    if (resultItem.getUseSttus().equals("E")) {
                        resultItem.setUseYn("N");
                        resultItem.setUseSttus("N");
                    }

                    // 전체시간 비교
                    if (resultItem.getUseYn().equals("N") && !resultItem.getUseSttus().equals("N")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                        rsModel.setResultMessage("Exhaust TAG");
                    } else {
                        int categoryCount = -100;
                        String deduction = "Y";
                        // 상품 카테고리 조회
                        rItem.setTagSeq(resultItem.getTagSeq());
                        rItem.setDevCtgNm(resultItem.getDevTypeNm());
                        ReservateVO resultCategoryItem = reservateService.reservateCategoryInfo(rItem);

                        if (resultCategoryItem != null) {
                            categoryCount = resultCategoryItem.getLmtCnt();
                            deduction = resultCategoryItem.getDeduction();
                        }

                        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                        TransactionStatus status = transactionManager.getTransaction(def);

                        try {
                            if (categoryCount == 0) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                                rsModel.setResultMessage("Not available Product");
                            } else {
                                if (categoryCount != -100 && deduction.equals("Y")) { // 설정 카테고리 목록에 있음
                                    // category 사용횟수 차감
                                    reservateService.reservateCategoryCountUpdate(rItem);
                                }

                                if (resultItem.getProdLmtCnt() == 0) { // 자유이용권
                                    result = 1;
                                } else { // 일반
                                    item.setTagSeq(resultItem.getTagSeq());
                                    item.setStorSeq(resultItem.getStorSeq());
                                    if (deduction.equals("N")) {
                                        result = 1;
                                    } else if (resultItem.getUsageCount() == 0) {
                                        result = 0;
                                    } else {
                                        result = deviceService.updateReservateUsageCount(item);
                                    }
                                }
                            }

                            if (result == 1) {
                                rItem.setTagSeq(resultItem.getTagSeq());
                                rItem.setUseYn("Y");
                                rItem.setUseSttus("Y");
                                rItem.setRmndCnt(-1);
                                reservateService.reservateUpdate(rItem);

                                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                                transactionManager.commit(status);
                            } else {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                                rsModel.setResultMessage("Tag UsageCount ALL USE");
                                transactionManager.rollback(status);
                            }
                        } catch (Exception e) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                            transactionManager.rollback(status);
                        } finally {
                            if (!status.isCompleted()) {
                                transactionManager.rollback(status);
                            }
                        }
                    }
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage("No Tag Info");
                }
                rsModel.setResultType("tagUsageCountUpdate");
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            /*
             * TAG 이용 횟수 조회
             */
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "tagId,devSeq,devTokn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String tagId = request.getParameter("tagId") == null ? "" : request.getParameter("tagId"); // tag ID
            int deviceSeq = request.getParameter("devSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("devSeq"));
            String devTokn = request.getParameter("devTokn") == null ? "" : request.getParameter("devTokn");
            String usingYn = "Y"; // 초기 이용권 상태값

            DeviceVO deviceVO = new DeviceVO();
            String encodeToken = devTokn.replaceAll(" ", "+");
            String useYn = "";

            deviceVO.setDevSeq(deviceSeq);
            deviceVO.setDevTokn(encodeToken);
            deviceVO.setStorSeq(0);
            if (deviceService.searchDeviceToken(deviceVO) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                item.setTagId(tagId);
                item.setDevSeq(deviceSeq);
                int result = 0;
                DeviceVO resultItem = deviceService.reservateUsageCount(item);
                // 배포 콘텐츠 카테고리
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage("No Tag Info");
                } else {
                    usingYn = resultItem.getUseYn();
                    if (resultItem.getTimeLimit().equals("Y") && !resultItem.getUseSttus().equals("N")) {
                        resultItem.setUseYn("N");
                    }

                    // 환불된 태그인지 조회
                    if (resultItem.getUseSttus().equals("B") || resultItem.getUseSttus().equals("E")) {
                        useYn = resultItem.getUseSttus();
                    }

                    int categoryCount = -1;
                    /* 차감없이 이용할수있는 콘텐츠카테고리가 있는 여부 체크 */
                    String totDeductionYn = "N";
                    String deduction = "Y";

                    // 상품 카테고리 조회
                    rItem.setTagSeq(resultItem.getTagSeq());
                    rItem.setDevCtgNm(resultItem.getDevTypeNm());
                    ReservateVO resultCategoryItems = reservateService.reservateCategoryInfo(rItem);
                    List<ReservateVO> ctgList = reservateService.reservateCategoryList(rItem);
                    for (int z = 0; z < ctgList.size(); z++) {
                        if (ctgList.get(z).getDeduction().equals("N")) {
                            totDeductionYn = "Y";
                            break;
                        }

                    }

                    if (resultCategoryItems != null) {
                        categoryCount = resultCategoryItems.getLmtCnt();
                        deduction = resultCategoryItems.getDeduction();
                    } else {
                        resultItem.setUseYn("N");
                    }

                    if (categoryCount == 0) { // 이용 카테고리 값(제한횟수)이 0 일 때
                        resultItem.setUseYn("N");
                        result = -1;
                    } else {
                        if (result != -1) { // 이용카테고리 값이 0이 아니면서 단일 카테고리가 아닐 시
                            if (resultItem.getProdLmtCnt() == 0) { // 자유이용권 (무제한)
                                result = 1;
                            } else { // 일반
                                if (resultItem.getUsageCount() == 0) { // 3,4,5...회 이용권
                                    resultItem.setUseYn("N");
                                }
                            }
                        }
                    }

                    Map<String, Integer> useCategoryCountList = new HashMap<String, Integer>();

                    ProductVO Pitem = new ProductVO();

                    Pitem.setStorSeq(resultItem.getStorSeq());
                    List<ProductVO> allCtgList = productService.productContsCategoryList(Pitem);

                    if (allCtgList != null) {
                        for (int j = 0; j < allCtgList.size(); j++) {
                            useCategoryCountList.put(allCtgList.get(j).getSecondCtgNm(), 0);
                        }
                    }

                    if (resultItem.getProdLmtCnt() == 1) { // 1회일 경우
                        rItem.setTagSeq(resultItem.getTagSeq());

                        List<ReservateVO> resultCategoryItem = reservateService.reservateCategoryList(rItem);

                        for (int i = 0; i < resultCategoryItem.size(); i++) {
                            if (resultCategoryItem.get(i).getLmtCnt() == -1) { // 무제한인 경우
                                useCategoryCountList.put(resultCategoryItem.get(i).getDevCtgNm(), 100);
                            } else {
                                useCategoryCountList.put(resultCategoryItem.get(i).getDevCtgNm(),
                                        resultCategoryItem.get(i).getLmtCnt());
                            }
                        }
                    } else { // 3, 5.. 회 이용권일 경우 - 무제한인 경우 100회로 처리함 (이전 IMEX에서 동일하게 처리됨)
                        rItem.setTagSeq(resultItem.getTagSeq());
                        List<ReservateVO> resultCategoryItem = reservateService.reservateCategoryList(rItem);

                        for (int i = 0; i < resultCategoryItem.size(); i++) {
                            if (resultCategoryItem.get(i).getLmtCnt() == -1) { // 무제한인 경우
                                useCategoryCountList.put(resultCategoryItem.get(i).getDevCtgNm(), 100);
                            } else {
                                useCategoryCountList.put(resultCategoryItem.get(i).getDevCtgNm(),
                                        resultCategoryItem.get(i).getLmtCnt());
                            }
                        }
                    }

                    List<ReservateVO> resultRItem = new ArrayList<ReservateVO>();
                    for (Map.Entry<String, Integer> entry : useCategoryCountList.entrySet()) {
                        ReservateVO RCategoryitem = new ReservateVO();
                        RCategoryitem.setDevCtgNm(entry.getKey());
                        RCategoryitem.setLmtCnt(entry.getValue());

                        resultRItem.add(RCategoryitem);
                    }

                    result = 0;

                    if (resultItem.getDevSeq() == deviceSeq) {
                        resultItem.setUseYn("Y");
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("useCategoryList", resultRItem);
                    } else {
                        if (resultItem.getUseYn() == "N") {
                            rItem.setTagSeq(resultItem.getTagSeq());
                            rItem.setDevTypeNm(resultItem.getDevTypeNm());
                            item.setTagSeq(resultItem.getTagSeq());
                            item.setProdSeq(resultItem.getProdSeq());
                            if ((totDeductionYn.equals("N") && deduction.equals("Y")) || resultItem.getTimeLimit().equals("Y")) {
                                result = deviceService.updateReservateUsageExpiry(item);
                            }

                        } else {
                            if (resultItem.getProdLmtCnt() != 0) { // 자유이용권
                                item.setTagSeq(resultItem.getTagSeq());
                                if (resultItem.getUsageCount() == 0 && deduction.equals("Y")) {
                                    item.setProdSeq(resultItem.getProdSeq());
                                    if (totDeductionYn.equals("N") && deduction.equals("Y") || resultItem.getTimeLimit().equals("Y")) {
                                        result = deviceService.updateReservateUsageExpiry(item);
                                    }
                                    resultItem.setUseYn("N");
                                }
                            }
                        }

                        if (resultItem != null) {
                            if (useYn.equals("B") || useYn.equals("E")) {
                                resultItem.setUseYn(useYn);
                            } else if (resultItem.getUseSttus().equals("N") && resultItem.getTimeLimit().equals("Y")) {
                                rItem.setUseYn("N");
                                rItem.setUseSttus("E");
                                rItem.setChanged(false);
                                rItem.setRmndCnt(-1);
                                rItem.setTagSeq(rItem.getTagSeq());
                                reservateService.reservateUpdate(rItem);
                                resultItem.setUseYn("E");
                            } else if (deduction.equals("N") && usingYn.equals("Y") && resultItem.getTimeLimit().equals("N") && resultItem.getUseSttus().equals("Y")) {
                                resultItem.setUseYn("Y");
                            }

                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setData("item", resultItem);
                            rsModel.setData("useCategoryList", resultRItem);
                        }
                    }

                    if (resultItem != null) {
                        if (useYn.equals("B") || useYn.equals("E")) {
                            resultItem.setUseYn(useYn);
                        } else if (resultItem.getUseSttus().equals("N") && resultItem.getTimeLimit().equals("Y")) {
                            rItem.setUseYn("N");
                            rItem.setUseSttus("E");
                            rItem.setChanged(false);
                            rItem.setTagSeq(rItem.getTagSeq());
                            rItem.setRmndCnt(-1);
                            reservateService.reservateUpdate(rItem);
                            resultItem.setUseYn("E");
                        } else if (deduction.equals("N") && usingYn.equals("Y") && resultItem.getTimeLimit().equals("N") && resultItem.getUseSttus().equals("Y")) {
                            resultItem.setUseYn("Y");
                        }
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("useCategoryList", resultRItem);
                    }
                    rsModel.setResultType("tagUsageCount");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 이용권 사용상태 API (태그 사용 정보 업데이트)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/reservateUseAPI")
    public ModelAndView reservateUseAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/reservate/reservateProcess");

        request.setCharacterEncoding("UTF-8");
        ReservateVO item = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "devSeq,storSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String tagId = request.getParameter("tagId") == null ? "" : request.getParameter("tagId");
            int devSeq = request.getParameter("devSeq") == null ? 0 : Integer.parseInt(request.getParameter("devSeq"));
            // String clear = request.getParameter("clear") == null ? "" : request.getParameter("clear");
            int storSeq = request.getParameter("storSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("storSeq"));
            String fnsTime = CommonFnc.emptyCheckString("fnsTime", request);
            String stTime = CommonFnc.emptyCheckString("stTime", request);

            item.setTagId(tagId);
            item.setDevSeq(devSeq);
            item.setStorSeq(storSeq);
            item.setFnsTime(fnsTime);
            item.setStTime(stTime);
            DeviceVO dItem = new DeviceVO();
            dItem.setDevSeq(devSeq);
            dItem.setFnsTime(fnsTime);
            dItem.setStTime(stTime);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            int result = 0;
            boolean chkresult = false;
            try {
                if (!tagId.equals("")) {
                    /* 강제 종료된 정보 업데이트 */
                    dItem.setStorSeq(storSeq);
                    dItem.setTagId(tagId);

                    result = deviceService.contentsPlayTimeOut(dItem);
                    if (result > 0) {
                        chkresult = true;
                    }

                    dItem = deviceService.deviceUseInfo(dItem);
                    // 태그정보가 있다면 ( 추가 , 시작)
                    if (dItem == null && devSeq != 0) { // 시작
                        dItem = new DeviceVO();
                        dItem.setDevSeq(devSeq);
                        dItem.setUseYn("Y");
                        dItem.setUseFnsTime("");
                        dItem.setUsageMinute(0);
                        dItem.setStTime(stTime);

                        deviceService.deviceUseInsert(dItem);
                    }
                } else {
                    result = deviceService.contentsPlayTimeOut(dItem);
                    if (result > 0) {
                        chkresult = true;
                    }
                }
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);

                // 태그정보 없데이트
                result = reservateService.reservateUseUpdate(item);
                if (result > 0) {
                    chkresult = true;
                }

                if (devSeq == 0 || tagId.equals("")) {
                    /*
                     * TAG
                     * 조건 USE_YN = 'N'
                     */

                    /*
                     * 강제종료된 태그 종료시간 업데이트
                     * 조건 USE_YN = 'N' LIMIT 1 NOW()
                     */

                    deviceService.deviceUseStateUpdate(dItem);

                }

                if (result > 0 || chkresult) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "tagId,storSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String tagId = request.getParameter("tagId") == null ? "" : request.getParameter("tagId");
            int storSeq = request.getParameter("storSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("storSeq"));

            item.setTagId(tagId);
            item.setStorSeq(storSeq);

            List<ReservateVO> resultItem = reservateService.reservateUseList(item);

            if (resultItem.size() == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("resultType", "reservateUseList");
                rsModel.setData("item", resultItem);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 런처와 연동하여 상품 목록 및 상세보기 정보를 제공하는 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/productListAPI")
    public ModelAndView productListAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/product/productProcess");

        request.setCharacterEncoding("UTF-8");
        ProductVO item = new ProductVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            MemberVO mItem = new MemberVO();

            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String mbrTokn = request.getParameter("mbrTokn") == null ? "" : request.getParameter("mbrTokn");
            int storSeq = request.getParameter("storSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("storSeq"));
            String encodeToken = "";
            encodeToken = mbrTokn.replaceAll(" ", "+");

            mItem.setStorSeq(storSeq);
            mItem.setMbrTokn(encodeToken);

            int tokenSearchResult = 0;
            tokenSearchResult = memberService.searchMemberToken(mItem);

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                item.setSidx("PROD_SEQ");
                item.setSord("");
                item.setOffset(1);
                item.setLimit(10000);
                item.setStorSeq(storSeq);
                item.setStorCode("");
                item.setUseYn("Y");

                List<ProductVO> resultItem = productService.productList(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {

                    for (int i = 0; i < resultItem.size(); i++) {
                        ProductVO pItem = new ProductVO();
                        pItem.setGsrProdCode("");
                        pItem.setProdSeq(resultItem.get(i).getProdSeq());
                        List<ProductVO> resultCategoryItem = productService.productCategoryList(pItem);

                        Map<String, Integer> useCategoryCountList = new HashMap<String, Integer>();

                        ProductVO Pitem = new ProductVO();

                        Pitem.setStorSeq(storSeq);
                        List<ProductVO> allCtgList = productService.productContsCategoryList(Pitem);

                        if (allCtgList != null) {
                            for (int j = 0; j < allCtgList.size(); j++) {
                                useCategoryCountList.put(allCtgList.get(j).getSecondCtgNm(), 0);
                            }
                        }

                        for (int s = 0; s < resultCategoryItem.size(); s++) {
                            if (resultItem.get(i).getProdLmtCnt() == -1) {
                                useCategoryCountList.put(resultCategoryItem.get(s).getDevCtgNm(), 0);
                            } else if (resultCategoryItem.get(s).getLmtCnt() == -1) { // 무제한인 경우
                                if (resultCategoryItem.get(s).getDeduction().equals("Y")
                                        && resultItem.get(i).getProdLmtCnt() != 0) {
                                    useCategoryCountList.put(resultCategoryItem.get(s).getDevCtgNm(),
                                            resultItem.get(i).getProdLmtCnt());
                                } else {
                                    useCategoryCountList.put(resultCategoryItem.get(s).getDevCtgNm(), 100);
                                }

                            } else {
                                useCategoryCountList.put(resultCategoryItem.get(s).getDevCtgNm(),
                                        resultCategoryItem.get(s).getLmtCnt());
                            }
                        }

                        List<ProductVO> ctgList = new ArrayList<ProductVO>();

                        Set set = useCategoryCountList.keySet();

                        Iterator iterator = set.iterator();

                        while (iterator.hasNext()) {
                            ProductVO ctgInfo = new ProductVO();
                            String key = (String) iterator.next();
                            ctgInfo.setDevCtgNm(key);
                            ctgInfo.setLmtCnt(useCategoryCountList.get(key));
                            if (key != null)
                                ctgList.add(ctgInfo);
                        }

                        resultItem.get(i).setProdCategoryList(ctgList);
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                }
                rsModel.setData("resultType", "productsListAPI");
            }
        }

        return rsModel.getModelAndView();
    }
    /* IMEX API */
    /*
     * @RequestMapping(value = "/api/productListAPI")
     * public ModelAndView productListAPI(HttpServletRequest request, HttpServletResponse response,HttpSession session)
     * throws Exception{
     * ResultModel rsModel = new ResultModel(response);
     * rsModel.setViewName("../resources/api/service/product/productProcess");
     * request.setCharacterEncoding("UTF-8");
     * ProductVO item = new ProductVO();
     * String method = CommonFnc.getMethod(request);
     * if (method.equals("POST")) {
     * rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
     * rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
     * rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
     * } else if (method.equals("PUT")) {
     * rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
     * rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
     * rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
     * } else if (method.equals("DELETE")) {
     * rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
     * rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
     * rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
     * } else { // GET
     * MemberVO mItem = new MemberVO();
     * // Checking Mendatory S
     * String checkString = CommonFnc.requiredChecking(request,"token,officeSeq");
     * if (!checkString.equals("")) {
     * rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
     * rsModel.setResultMessage("invalid parameter("+checkString+")");
     * rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
     * return rsModel.getModelAndView();
     * }
     * // Checking Mendatory E
     * String token = request.getParameter("token") == null? "" : request.getParameter("token");
     * int storSeq = request.getParameter("officeSeq") == null ? 0 :
     * Integer.parseInt(request.getParameter("officeSeq"));
     * String encodeToken ="";
     * encodeToken = token.replaceAll(" ", "+");
     * mItem.setStorSeq(storSeq);
     * mItem.setMbrTokn(encodeToken);
     * int tokenSearchResult = 0;
     * tokenSearchResult = memberService.searchMemberToken(mItem);
     * if (tokenSearchResult == 0) {
     * rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
     * rsModel.setResultMessage("Not Token Info");
     * rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
     * } else {
     * item.setSidx("PROD_SEQ");
     * item.setSord("");
     * item.setOffset(1);
     * item.setLimit(10000);
     * item.setStorSeq(storSeq);
     * item.setStorCode("");
     * item.setUseYn("Y");
     * List<ProductVO> resultItem = productService.productList(item);
     * if (resultItem.isEmpty()) {
     * rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
     * rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
     * } else {
     * for (int i=0; i<resultItem.size(); i++) {
     * ProductVO pItem =new ProductVO();
     * pItem.setGsrProdCode("");
     * pItem.setProdSeq(resultItem.get(i).getProdSeq());
     * List<ProductVO> resultCategoryItem = productService.productCategoryList(pItem);
     * resultItem.get(i).setProdCategoryList(resultCategoryItem);
     * }
     * rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
     * rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
     * rsModel.setData("item", resultItem);
     * }
     * rsModel.setData("resultType","productsListAPI");
     * }
     * }
     * return rsModel.getModelAndView();
     * }
     */

    /**
     * 외부 콘텐츠 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceConts")
    public ModelAndView deviceContentsProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);

        rsModel.setViewName("../resources/api/contents/contentsProcess");

        DeviceVO device = new DeviceVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put 콘텐츠 다운로드 로그 등록
                String checkString = CommonFnc.requiredChecking(request, "storSeq,devSeq,devTokn,contsSeqs");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                String devTokn = CommonFnc.emptyCheckString("devTokn", request);
                String encodeToken = devTokn.replaceAll(" ", "+");
                device.setDevSeq(devSeq);
                device.setDevTokn(encodeToken);
                if (deviceService.searchDeviceToken(device) == 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                } else {
                    device.setStorSeq(storSeq);
                    String contentsSeqs = request.getParameter("contsSeqs");
                    String[] contentsSeq = contentsSeqs.split(",");

                    DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                    def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                    TransactionStatus status = transactionManager.getTransaction(def);

                    try {
                        for (int i = 0; i < contentsSeq.length; i++) {
                            device.setContsSeq(Integer.parseInt(contentsSeq[i]));
                            launcherService.deviceContentsConditionUpdate(device);
                            launcherService.contentsDownInsert(device);
                        }

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setResultType("contentsUpdate");
                        transactionManager.commit(status);
                    } catch (Exception e) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        transactionManager.rollback(status);
                    } finally {
                        if (!status.isCompleted()) {
                            transactionManager.rollback(status);
                        }
                    }
                }
            } else if (Method.equals("DELETE")) { // delete
                String checkString = CommonFnc.requiredChecking(request, "devSeq");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int devSeq = CommonFnc.emptyCheckInt("devSeq", request);

                device.setDevSeq(devSeq);

                try {
                    if (deviceService.contsUpdateDelete(device) == 0) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    }
                } catch (MyBatisSystemException e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get Device 에서 서버로 콘텐츠 목록 가져오기

            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "devSeq,storSeq,devTokn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String token = CommonFnc.emptyCheckString("devTokn", request);
            String encodeToken = token.replaceAll(" ", "+");
            device.setDevSeq(devSeq);
            device.setDevTokn(encodeToken);
            device.setStorSeq(storSeq);

            if (deviceService.searchDeviceToken(device) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                String listType = CommonFnc.emptyCheckString("listType", request);
                device.setDevId("");

                DeviceVO deviceInfo = deviceService.deviceInfo(device);

                if (deviceInfo != null) {
                    ReleaseVO ritem = new ReleaseVO();
                    ritem.setStorSeq(0);
                    ritem.setFrmtnNm("");
                    ritem.setFrmtnSeq(deviceInfo.getFrmtnSeq());
                    ritem.setListType(listType);
                    ritem.setDevSeq(devSeq);

                    List<ReleaseVO> contsList = releaseService.mmsReleaseContsList(ritem);
                    List<ContentVO> resultItem = new ArrayList<ContentVO>();
                    for (int j = 0; j < contsList.size(); j++) {
                        ContentVO Citem = new ContentVO();
                        Citem.setContsSeq(contsList.get(j).getContsSeq());
                        Citem.setCpSeq(0);
                        Citem.setContsCtgSeq(0);
                        Citem.setSttusVal("");
                        Citem.setGenreSeq(0);
                        Citem.setList(null);
                        Citem.setSvcSeq(0);
                        /** 해당 콘텐츠 상세정보 */
                        resultItem.add(contentService.contentInfo(Citem));

                        /** 해당 콘텐츠 장르 리스트 */
                        List<ContentVO> genreList = contentService.contentsGenreList(Citem);
                        if (!genreList.isEmpty() && genreList.size() != 0) {
                            resultItem.get(j).setGenreList(genreList);
                        }

                        /** 해당 콘텐츠 서비스 리스트 */
                        ContentVO coverImg = contentService.contentCoverImageInfo(Citem);
                        if (coverImg != null) {
                            resultItem.get(j).setCoverImg(coverImg);
                        }

                        /** 해당 콘텐츠 서비스 리스트 */
                        List<ContentVO> serviceList = contentService.contentServiceList(Citem);
                        if (!serviceList.isEmpty() && serviceList.size() != 0) {
                            resultItem.get(j).setServiceList(serviceList);
                        }

                        /** 해당 콘텐츠 갤러리 이미지 리스트 */
                        List<ContentVO> thumbnailList = contentService.contentsThumbnailList(Citem);
                        if (!thumbnailList.isEmpty() && thumbnailList.size() != 0) {
                            resultItem.get(j).setThumbnailList(thumbnailList);
                        }

                        /** 해당 콘텐츠 비디오 파일 리스트 */
                        List<ContentVO> videoList = contentService.contentsVideoList(Citem);

                        /** 해당 콘텐츠 실행 파일 리스트 */
                        List<ContentVO> fileList = contentService.fileDataInfo(Citem);
                        if (!fileList.isEmpty() && fileList.size() != 0) {
                            resultItem.get(j).setFileDataList(fileList);
                        }

                        /** 미리보기 영상 파일 리스트 */
                        List<ContentVO> prevList = contentService.contentsPrevList(Citem);
                        if (!prevList.isEmpty() && prevList.size() != 0) {
                            resultItem.get(j).setPrevList(prevList);
                        }

                        for (ContentVO videoItem : videoList) {
                            ContentVO metaItem = new ContentVO();
                            metaItem.setContsSeq(contsList.get(j).getContsSeq());
                            metaItem.setFilePath(videoItem.getFilePath());
                            ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                            ContentVO contsMetaInfo = contentService.contsMetaInfo(metaItem);
                            videoItem.setMetadataXmlInfo(metadataInfo);
                            videoItem.setContsXmlInfo(contsMetaInfo);
                        }

                        resultItem.get(j).setVideoList(videoList);

                    }

                    if (contsList.isEmpty() || contsList == null) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setResultType("devContsDetailList");
                        rsModel.setData("item", resultItem);
                    }
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                }

            }

        }

        return rsModel.getModelAndView();
    }

    /**
     * 매장 장비 정보 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceAPI")
    public ModelAndView deviceProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO item = new DeviceVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,token");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            MemberVO member = new MemberVO();

            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String token = CommonFnc.emptyCheckString("token", request);
            String encodeToken = token.replaceAll(" ", "+");

            int macSearch = CommonFnc.emptyCheckInt("macSearch", request);

            int tokenSearchResult = 0;

            if (devSeq == 0) {
                member.setStorSeq(storSeq);
                member.setMbrTokn(encodeToken);
                tokenSearchResult = memberService.searchMemberToken(member);
            } else {
                item.setDevSeq(devSeq);
                item.setDevTokn(encodeToken);
                item.setStorSeq(storSeq);
                tokenSearchResult = deviceService.searchDeviceToken(item);
            }

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            } else {
                item.setSvcSeq(0);
                item.setDevId("");
                item.setStorSeq(storSeq);

                String searchType = request.getParameter("searchType") == null ? ""
                        : request.getParameter("searchType");

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm("false");

                /** 현재 페이지 */
                int currentPage = 1;

                /** 검색에 출력될 개수 */
                int limit = 10000;

                /** 정렬할 필드 */
                String sidx = "CRET_DT";

                /** 정렬 방법 */
                String sord = "";

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                item.setSidx(sidx);
                item.setSord(sord);
                item.setoffset(page);
                item.setlimit(pageEnd);
                item.setMacSearch(macSearch);

                List<DeviceVO> resultItem = deviceService.deviceList(item);

                for (int i = 0; i < resultItem.size(); i++) {
                    DeviceVO device = resultItem.get(i);
                    String typeNm = device.getDevTypeNm();
                    if (typeNm != null) {
                        String[] type = typeNm.split("_");
                        resultItem.get(i).setDevTypeNm(type[0]);
                        resultItem.get(i).setDevTypeNmFull(typeNm);
                        ReservateVO ritem = new ReservateVO();
                        ritem.setDevSeq(device.getDevSeq());
                        ritem.setTagId("");
                        List<ReservateVO> Sritem = reservateService.reservateUseList(ritem);
                        resultItem.get(i).setUseTagList(reservateService.reservateUseList(ritem));
                    }

                }

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    int totalCount = deviceService.deviceListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", (totalCount - 1) / limit);
                }

                rsModel.setResultType("deviceList");
            }

        }

        return rsModel.getModelAndView();
    }

    /**
     * 매장 회원 정보 / 회원토큰 / 장비 토큰 조회 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/memberAPIInfo")
    public ModelAndView memberInfoProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/member/memberProcess");

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String validateParams = "mbrId;mbrPwd";
            String invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                        "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            LoginVO vo = new LoginVO();
            vo.setMbrId(request.getParameter("mbrId"));

            String memberPwd = MemberApi.makeEncryptPassword(request.getParameter("mbrPwd"), memberService.getSalt(vo));
            vo.setMbrPwd(memberPwd);
            vo.setMbrSe(MemberApi.MEMBER_SECTION_STORE);

            LoginVO memberResult = memberService.getLoginInfo(vo);
            if (memberResult == null) {
                rsModel.setNoData();
                rsModel.setResultMessage("Please check your id and password");
                return rsModel.getModelAndView();
            }

            if (!MemberApi.MEMBER_STATUS_ENABLE.equals(memberResult.getMbrSttus())) {
                rsModel.setNoData();
                rsModel.setResultMessage(ResultModel.MESSAGE_UNAVAILABLE_MEMBER);
                return rsModel.getModelAndView();
            }

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);
            try {
                String token = "";
                String deviceId = CommonFnc.emptyCheckString("devId", request);
                if (deviceId != "") {
                    deviceId = deviceId.replaceAll(" ", "+");
                }
                boolean needsMemberInfo = (deviceId == null || deviceId.isEmpty());
                if (needsMemberInfo) {
                    token = this.generateToken(memberResult.getMbrId());
                    String oldToken = memberResult.getMbrTokn();

                    if (oldToken != null) {
                        token = oldToken;
                    }

                    vo.setMbrTokn(token);

                    // 토큰 유효기간 만료된 토큰에 대해서 갱신 처리
                    int resultCode = memberService.updateMemberToken(vo);
                    if (resultCode != 1) {
                        token = oldToken;
                    }
                } else {
                    DeviceVO deviceVo = new DeviceVO();
                    deviceVo.setDevId(deviceId);
                    deviceVo.setStorSeq(memberResult.getStorSeq());

                    token = this.generateToken(deviceId);

                    DeviceVO deviceResult = deviceService.deviceInfo(deviceVo);
                    if (deviceResult == null) {
                        rsModel.setNoData();
                        rsModel.setResultMessage("Please check your device id");
                        return rsModel.getModelAndView();
                    }
                    String oldToken = deviceResult.getDevTokn();
                    deviceResult.setDevTokn(token);

                    // 토큰 유효기간 만료된 토큰에 대해서 갱신 처리
                    int resultCode = deviceService.updateDeviceToken(deviceResult);
                    if (resultCode != 1) {
                        token = oldToken;
                    }
                    rsModel.setData("devSeq", deviceResult.getDevSeq());
                }

                // 멤버 정보 마스킹 처리
                memberResult.setMbrId(CommonFnc.getIdMask(memberResult.getMbrId()));
                memberResult.setMbrNm(CommonFnc.getNameMask(memberResult.getMbrNm()));
                memberResult.setMbrTelNo(CommonFnc.getTelnoMask(memberResult.getMbrTelNo()));
                memberResult.setMbrMphonNo(CommonFnc.getTelnoMask(memberResult.getMbrMphonNo()));
                memberResult.setMbrEmail(CommonFnc.getEmailMask(memberResult.getMbrEmail()));

                token = token.replaceAll(System.getProperty("line.separator"), "");
                rsModel.setResultType("storeMemberInfo");
                rsModel.setData("item", memberResult);
                rsModel.setData("token", token);
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
            return rsModel.getModelAndView();
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 장비 사용 시간 수정 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceUseTime")
    public ModelAndView deviceUseTime(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        String method = CommonFnc.getMethod(request);
        if (method.equals("PUT")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "devSeq,endTime,devTokn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);
            try {
                DeviceVO item = new DeviceVO();
                String devToken = request.getParameter("devTokn").replaceAll(" ", "+");
                item.setDevSeq(Integer.parseInt(request.getParameter("devSeq")));
                item.setDevTokn(devToken);
                item.setStorSeq(0);

                int tokenSearchResult = deviceService.searchDeviceToken(item);

                if (tokenSearchResult == 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                } else {
                    item.setDevSeq(Integer.parseInt(request.getParameter("devSeq")));
                    item.setUsageMinute(Integer.parseInt(request.getParameter("endTime")));
                    int resultCode = deviceService.deviceUseUpdate(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                }

                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
            return rsModel.getModelAndView();
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 장비에서 장르 목록 조회 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    // @RequestMapping(value = "/api/codeGenre")
    //
    // public ModelAndView codeInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session)
    // throws Exception {
    // ResultModel rsModel = new ResultModel(response);
    // rsModel.setViewName("../resources/api/service/device/deviceProcess");
    //
    // String method = CommonFnc.getMethod(request);
    // if (method.equals("GET")) {
    // String invalidParams = validateDeviceToken(request);
    // if (!invalidParams.isEmpty()) {
    // rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
    // "invalid parameter(" + invalidParams + ")");
    // return rsModel.getModelAndView();
    // }
    //
    // GenreVO vo = new GenreVO();
    // List<GenreVO> resultItem = genreService.getCategoryGenreList(vo);
    // if (resultItem.isEmpty()) {
    // rsModel.setNoData();
    // return rsModel.getModelAndView();
    // }
    //
    // rsModel.setData("item", resultItem);
    // rsModel.setResultType("codeNameAPIList");
    // return rsModel.getModelAndView();
    // }
    //
    // rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
    // return rsModel.getModelAndView();
    // }

    /**
     * 장비에 등록된 콘텐츠의 장르 목록 조회 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    // @RequestMapping(value = "/api/useGenre")
    @RequestMapping({ "/api/useGenre", "/api/codeGenre" })
    public ModelAndView useGenre(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {

            String invalidParams = validateDeviceToken(request);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                        "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            DeviceVO item = new DeviceVO();
            item.setDevSeq(Integer.parseInt(request.getParameter("devSeq")));

            List<DeviceVO> resultItem = deviceService.useGenreList(item);
            if (resultItem.isEmpty()) {
                rsModel.setNoData();
                return rsModel.getModelAndView();
            }

            int totalCount = deviceService.useGenreListTotalCount(item);


            rsModel.setData("item", resultItem);
            rsModel.setData("totalCount", totalCount);
            rsModel.setResultType("codeNameAPIList");
            return rsModel.getModelAndView();
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 장비와 서버간 네트워크 테스트용 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/testAPI")
    public ModelAndView testAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String validateParams = "flag:{" + Validator.NUMBER + "}";
            String invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                        "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            int flag = Integer.parseInt(request.getParameter("flag"));
            if (flag == 0) {
                // nothing
            } else if (flag == 1) {
                rsModel.setResultMessage("Not Token Info");
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else if (flag == 2) {
                rsModel.setResultMessage("Fail");
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            } else {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                        "invalid parameter(" + invalidParams + ")");
            }
            return rsModel.getModelAndView();
        }

        if (method.equals("GET")) {
            String validateParams = "flag:{" + Validator.NUMBER + "}";
            String invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                        "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            int flag = Integer.parseInt(request.getParameter("flag"));
            if (flag == 0) {
                // nothing
            } else if (flag == 1) {
                rsModel.setResultMessage("Not Token Info");
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else if (flag == 2) {
                rsModel.setResultMessage("Fail");
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            } else {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(flag");
            }
            return rsModel.getModelAndView();
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private String validateDeviceToken(HttpServletRequest request) throws Exception {
        String validateParams = "devSeq:{" + Validator.NUMBER + "};devTokn;";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            return invalidParams;
        }

        String devToken = request.getParameter("devTokn").replaceAll(" ", "+");

        DeviceVO item = new DeviceVO();
        item.setDevSeq(Integer.parseInt(request.getParameter("devSeq")));
        item.setDevTokn(devToken);

        int tokenSearchResult = deviceService.searchDeviceToken(item);
        if (tokenSearchResult == 0) {
            invalidParams = "devTokn";
        }

        return invalidParams;
    }

    /**
     * 이용 콘텐츠 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/playContents")
    public ModelAndView useContentsProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO device = new DeviceVO();
        ReservateVO Ritem = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,devSeq,devTokn,contsSeq,tagId");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String token = CommonFnc.emptyCheckString("devTokn", request);
            String tagId = CommonFnc.emptyCheckString("tagId", request);
            String stTime = CommonFnc.emptyCheckString("stTime", request);
            String fnsTime = CommonFnc.emptyCheckString("fnsTime", request);

            String encodeToken = token.replaceAll(" ", "+");
            device.setDevSeq(devSeq);
            device.setDevTokn(encodeToken);
            device.setStorSeq(0);
            if (deviceService.searchDeviceToken(device) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                device.setStorSeq(storSeq);
                device.setContsSeq(contsSeq);
                device.setTagId(tagId);
                device.setDevSeq(devSeq);
                device.setStTime(stTime);
                device.setFnsTime(fnsTime);

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    Ritem.setStorSeq(storSeq);
                    Ritem.setTagId(tagId);
                    String[] tagIds = tagId.split(",");

                    boolean noexistTag = false;
                    for (int i = 0; i < tagIds.length; i++) {
                        Ritem.setTagId(tagIds[i]);
                        List<ReservateVO> resultItem = reservateService.reservateUseList(Ritem);

                        if (resultItem.isEmpty()) {
                            noexistTag = true;
                        }
                    }

                    if (noexistTag) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TAG);
                    } else {
                        deviceService.contentsPlayTimeOut(device);
                        launcherService.contentsPlayLogInsert(device);
                        deviceService.contentPlayTimeInsert(device);
                        if (fnsTime.equals("")) {
                            device.setContsSeq(contsSeq);
                            deviceService.devicePlayUseUpdate(device);
                        }
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    }
                    transactionManager.commit(status);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            }
        } else if (method.equals("PUT")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,devSeq,devTokn,tagId");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String token = CommonFnc.emptyCheckString("devTokn", request);
            String tagId = CommonFnc.emptyCheckString("tagId", request);
            String fnsTime = CommonFnc.emptyCheckString("fnsTime", request);
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String encodeToken = token.replaceAll(" ", "+");
            device.setDevSeq(devSeq);
            device.setDevTokn(encodeToken);
            device.setStorSeq(storSeq);

            if (deviceService.searchDeviceToken(device) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                device.setStorSeq(storSeq);
                device.setContsSeq(contsSeq);
                device.setTagId(tagId);
                device.setFnsTime(fnsTime);

                String forceUpd = CommonFnc.emptyCheckString("forceUpd", request);
                String stTime = CommonFnc.emptyCheckString("stTime", request);

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {

                    if (forceUpd.equals("Y") && !stTime.equals("") && !fnsTime.equals("") && !tagId.equals("")) {
                        device.setStTime(stTime);
                        device.setContsSeq(contsSeq);
                        int count = deviceService.contentPastPlayTimeUpdate(device);

                        if (count > 0) {
                            device.setContsSeq(0);
                            deviceService.devicePlayUseUpdate(device);

                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);

                        } else {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        }

                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        transactionManager.commit(status);
                        return rsModel.getModelAndView();
                    }

                    /* 강제 종료 되었거나 업데이트안된 Launcher 정보 업데이트 */
                    deviceService.contentPlayTimeUpdate(device);

                    device.setContsSeq(0);
                    deviceService.devicePlayUseUpdate(device);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,devSeq,devTokn,tagId,stTime,contsSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String token = CommonFnc.emptyCheckString("devTokn", request);
            String tagId = CommonFnc.emptyCheckString("tagId", request);
            String fnsTime = CommonFnc.emptyCheckString("fnsTime", request);
            String stTime = CommonFnc.emptyCheckString("stTime", request);
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String encodeToken = token.replaceAll(" ", "+");
            device.setDevSeq(devSeq);
            device.setDevTokn(encodeToken);
            device.setStorSeq(storSeq);
            if (deviceService.searchDeviceToken(device) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                device.setStorSeq(storSeq);
                device.setContsSeq(contsSeq);
                device.setTagId(tagId);
                device.setFnsTime(fnsTime);
                device.setStTime(stTime);

                DeviceVO resultItem = deviceService.selectUseTimeInfo(device);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }
            }
            rsModel.setResultType("useTimeInfo");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 장비 MAC & IP 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceMacIP")
    public ModelAndView devInfoProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO device = new DeviceVO();
        ReservateVO Ritem = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            String macAdr = CommonFnc.emptyCheckString("macAdr", request);
            String devIpadr = CommonFnc.emptyCheckString("devIpadr", request);

            if (macAdr.equals("") && devIpadr.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(macAdr,devIpadr)");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            // search
            device.setMacAdr(macAdr);
            device.setDevIpadr(devIpadr);
            device.setDevSeq(devSeq);
            device.setDevId("");

            DeviceVO resultItem = deviceService.deviceInfo(device);
            if (resultItem == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                if (resultItem.getMacAdr() != null && !resultItem.getMacAdr().equals("") && !macAdr.equals("")) {
                    if (resultItem.getMacAdr() != macAdr) {
                        device.setMacAdr(resultItem.getMacAdr());
                        deviceService.deviceMacUpdate(device);
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                        resultItem.setDevIpadr(devIpadr);
                        rsModel.setData("item", resultItem);
                    } else {
                        try {
                            int result = deviceService.deviceMacUpdate(device);
                            resultItem.setMacAdr(macAdr);
                            resultItem.setDevIpadr(devIpadr);

                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                            rsModel.setData("item", resultItem);
                        } catch (Exception e) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        }
                    }
                } else {
                    if (resultItem.getMacAdr() == null || resultItem.getMacAdr().equals("")) {
                        try {
                            int result = deviceService.deviceMacUpdate(device);
                            resultItem.setMacAdr(macAdr);
                            resultItem.setDevIpadr(devIpadr);

                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                            rsModel.setData("item", resultItem);
                        } catch (Exception e) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        }
                    }
                }

            }
            rsModel.setResultType("deviceMacInfo");
        } else if (method.equals("DELETE")) {
            String macAdr = CommonFnc.emptyCheckString("macAdr", request);
            String devIpadr = CommonFnc.emptyCheckString("devIpadr", request);

            if (macAdr.equals("") && devIpadr.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(macAdr,devIpadr)");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            // search
            device.setMacAdr(macAdr);
            device.setDevIpadr(devIpadr);
            device.setDevSeq(0);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = 0;
                if (!macAdr.equals("")) {
                    result = deviceService.deviceMacUpdate(device);
                }
                if (!devIpadr.equals("")) {
                    result = deviceService.deviceIpUpdate(device);
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();

    }

    /**
     * 매장별 장비 이용현황 & 장비 이용시간 관련
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceConnectAPI")
    public ModelAndView deviceConnectProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO device = new DeviceVO();
        ReservateVO Ritem = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,mbrTokn,devSeq,useFnsTime");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            MemberVO member = new MemberVO();
            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String mbrTokn = CommonFnc.emptyCheckString("mbrTokn", request);
            String encodeToken = mbrTokn.replaceAll(" ", "+");

            encodeToken = mbrTokn.replaceAll(" ", "+");
            member.setStorSeq(storSeq);
            member.setMbrTokn(encodeToken);
            int tokenSearchResult = memberService.searchMemberToken(member);

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            } else {
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);
                rsModel.setResultType("deviceUseUpdate");
                try {
                    String useFnsTime = CommonFnc.emptyCheckString("useFnsTime", request);
                    device.setDevSeq(devSeq);
                    device.setStorSeq(storSeq);
                    device.setUseFnsTime(useFnsTime);
                    device.setUsageMinute(0);
                    device.setUseYn("");
                    int result = deviceService.deviceUseUpdate(device);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,mbrTokn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            MemberVO member = new MemberVO();

            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String mbrTokn = CommonFnc.emptyCheckString("mbrTokn", request);
            String encodeToken = mbrTokn.replaceAll(" ", "+");

            encodeToken = mbrTokn.replaceAll(" ", "+");
            device.setStorSeq(storSeq);
            member.setStorSeq(storSeq);
            member.setMbrTokn(encodeToken);
            int tokenSearchResult = memberService.searchMemberToken(member);

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            } else {
                List<DeviceVO> resultItem = deviceService.deviceDetailList(device);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    for (int i = 0; i < resultItem.size(); i++) {
                        if (resultItem.get(i).getFilePath() != null) {
                            resultItem.get(i)
                                    .setFilePath("http://" + request.getServerName() + resultItem.get(i).getFilePath());
                        }

                        int currentPage = 1;
                        int limit = 1000;
                        int page = (currentPage - 1) * limit + 1;
                        int pageEnd = page + limit - 1;

                        Ritem = new ReservateVO();
                        Ritem.setDevSeq(resultItem.get(i).getDevSeq());
                        Ritem.setSidx("seq");
                        Ritem.setSord("DESC");
                        Ritem.setOffset(page);
                        Ritem.setLimit(pageEnd);
                        Ritem.setStorSeq(resultItem.get(i).getStorSeq());
                        Ritem.setStartDt("");
                        Ritem.setEndDt("");
                        Ritem.setSearchConfirm("false");
                        List<ReservateVO> resultRitem = reservateService.reservateList(Ritem);

                        resultItem.get(i).setUseTag(resultRitem);

                    }

                    for (int i = 0; i < resultItem.size(); i++) {
                        DeviceVO deviceInfo = resultItem.get(i);
                        String typeNm = deviceInfo.getDevTypeNm();
                        if (typeNm != null) {
                            String[] type = deviceInfo.getDevTypeNm().split("_");
                            resultItem.get(i).setDevTypeNm(type[0]);
                            resultItem.get(i).setDevTypeNmFull(typeNm);
                        }
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }
                rsModel.setResultType("deviceInfoAPI");
            }
        }

        return rsModel.getModelAndView();

    }

    @RequestMapping(value = "/api/network", method = RequestMethod.GET)
    public ModelAndView networkProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/network");

        request.setCharacterEncoding("UTF-8");

        MemberVO member = new MemberVO();
        DeviceVO device = new DeviceVO();

        int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
        String token = CommonFnc.emptyCheckString("token", request);
        String encodeToken = token.replaceAll(" ", "+");

        encodeToken = token.replaceAll(" ", "+");

        int macSearch = CommonFnc.emptyCheckInt("macSearch", request);

        if (token.equals("")) {
            try {
                launcherService.noneList();
                rsModel.setData("message", "OK");
            } catch (Exception e) {
                rsModel.setData("message", "Fail");
            }
        } else {
            if (storSeq == 0) {
                rsModel.setData("message", "Fail");
            } else {
                member.setStorSeq(storSeq);
                member.setMbrTokn(encodeToken);
                int result = memberService.searchMemberToken(member);

                device.setDevSeq(0);
                device.setDevTokn(encodeToken);
                device.setStorSeq(storSeq);
                int result2 = deviceService.searchDeviceToken(device);

                if (result == 1 || result2 == 1) {
                    rsModel.setData("message", "OK");
                } else {
                    rsModel.setData("message", "Fail");
                }
            }

        }

        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/downloadFile/{fileSeq}")
    public void downloadFile(HttpServletRequest request, HttpServletResponse response,
            @PathVariable(value = "fileSeq") String fileSeq) throws Exception {
        String invalidParams = validateDeviceToken(request);
        if (!invalidParams.isEmpty()) {
            response.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        FileVO item = new FileVO();
        item.setFileSeq(Integer.parseInt(fileSeq));
        item.setFilePath("");
        FileVO metadataRes = fileService.MedatadataInfo(item);
        String filePath = metadataRes.getFilePath();
        File downfile = new File(fsResource.getPath() + filePath);
        long L = downfile.length();
        if (!downfile.exists()) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }

        ServletOutputStream outStream = null;
        FileInputStream inputStream = null;
        try {
            outStream = response.getOutputStream();
            inputStream = new FileInputStream(downfile);

            String downName = null;
            String browser = request.getHeader("User-Agent");

            // 파일 인코딩
            if (browser.contains("MSIE") || browser.contains("Trident")) {
                downName = URLEncoder.encode(metadataRes.getOrginlFileNm(), "UTF-8").replaceAll("\\+", "%20");
            } else if (browser.contains("Chrome")) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < metadataRes.getOrginlFileNm().length(); i++) {
                    char c = metadataRes.getOrginlFileNm().charAt(i);
                    if (c > '~') {
                        sb.append(URLEncoder.encode("" + c, "UTF-8"));
                    } else {
                        sb.append(c);
                    }
                }
                downName = sb.toString();

            } else {
                downName = new String(metadataRes.getOrginlFileNm().getBytes("UTF-8"), "ISO-8859-1");
            }

            // Setting Resqponse Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            // byte[] outByte = new byte[(int) L];
            // while (inputStream.read(outByte, 0, (int) L) != -1) {
            // outStream.write(outByte, 0, (int) L);
            // }

            byte[] buf = new byte[4096];
            int bytesread = 0, bytesBuffered = 0;
            while ((bytesread = inputStream.read(buf)) > -1) {
                outStream.write(buf, 0, bytesread);
                bytesBuffered += bytesread;
                if (bytesBuffered > 1024 * 1024) { // flush after 1MB
                    bytesBuffered = 0;
                    outStream.flush();
                }
            }
        } catch (Exception e) {
            response.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
        } finally {
            inputStream.close();
            outStream.flush();
            outStream.close();
        }
        return;
    }

    /**
     * 멤버 토큰을 이용한 컨텐츠 다운로드 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param fileSeq - 파일번호
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/memberDownloadFile/{fileSeq}")
    public void memberFileDownload(HttpServletRequest request, HttpServletResponse response,
            @PathVariable(value = "fileSeq") String fileSeq) throws Exception {
        MemberVO mItem = new MemberVO();

        // Checking Mendatory S
        String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq");
        // Checking Mendatory E
        if (!checkString.equals("")) {
            response.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        // 멤버 토큰
        String mbrTokn = request.getParameter("mbrTokn") == null ? "" : request.getParameter("mbrTokn");
        // 상점 번호
        int storSeq = request.getParameter("storSeq") == null ? 0 : Integer.parseInt(request.getParameter("storSeq"));
        // 시리얼 번호(각 장비별 고유 번호)
        String serialNum = request.getParameter("serialNum") == null ? "" : request.getParameter("serialNum");
        String encodeToken = "";
        encodeToken = mbrTokn.replaceAll(" ", "+");

        mItem.setStorSeq(storSeq);
        mItem.setMbrTokn(encodeToken);

        int tokenSearchResult = 0;
        tokenSearchResult = memberService.searchMemberToken(mItem);

        if (tokenSearchResult == 0) {
            response.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        FileVO item = new FileVO();
        item.setFileSeq(Integer.parseInt(fileSeq));
        item.setFilePath("");
        FileVO metadataRes = fileService.MedatadataInfo(item);
        String filePath = metadataRes.getFilePath();
        File downfile = new File(fsResource.getPath() + filePath);
        if (!downfile.exists()) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }

        ServletOutputStream outStream = null;
        FileInputStream inputStream = null;
        try {
            outStream = response.getOutputStream();
            inputStream = new FileInputStream(downfile);

            String downName = null;
            String browser = request.getHeader("User-Agent");

            // 파일 인코딩
            if (browser.contains("MSIE") || browser.contains("Trident")) {
                downName = URLEncoder.encode(metadataRes.getOrginlFileNm(), "UTF-8").replaceAll("\\+", "%20");
            } else if (browser.contains("Chrome")) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < metadataRes.getOrginlFileNm().length(); i++) {
                    char c = metadataRes.getOrginlFileNm().charAt(i);
                    if (c > '~') {
                        sb.append(URLEncoder.encode("" + c, "UTF-8"));
                    } else {
                        sb.append(c);
                    }
                }
                downName = sb.toString();

            } else {
                downName = new String(metadataRes.getOrginlFileNm().getBytes("UTF-8"), "ISO-8859-1");
            }

            // Setting Resqponse Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            // byte[] outByte = new byte[(int) L];
            // while (inputStream.read(outByte, 0, (int) L) != -1) {
            // outStream.write(outByte, 0, (int) L);
            // }

            byte[] buf = new byte[4096];
            int bytesread = 0, bytesBuffered = 0;
            System.out.println("[/api/memberDownloadFile][filePath]:" + filePath + "  [fileSeq]:" + fileSeq + " (START)");
            while ((bytesread = inputStream.read(buf)) > -1) {
                outStream.write(buf, 0, bytesread);
                bytesBuffered += bytesread;
                if (bytesBuffered > 1024 * 1024) { // flush after 1MB
                    bytesBuffered = 0;
                    outStream.flush();
                }
            }
            System.out.println("[/api/memberDownloadFile][filePath]:"+filePath + "  [fileSeq]:"+fileSeq +" (END)");
        } catch (Exception e) {
            System.out.println("[_______FILE ERROR LOG_____API : memberDownloadFile][SEQ:"+fileSeq+"]");
            response.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
        } finally {
            inputStream.close();
            outStream.flush();
            outStream.close();
        }
        return;
    }

    /**
     * token 생성 함수
     *
     * @param id
     * @return String
     * @exception Exception
     */
    public String generateToken(String id) throws Exception {
        return AesAlgorithm.encrypt(id + new Date().toString(), configProperty.getPropertyAfterDecrypt("aes.key"));
    }

    /**
     * 장비 종료 처리 (사용자)
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceEndUp")
    public ModelAndView deviceEndTimeUpdate(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        MemberVO mItem = new MemberVO();
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        String method = CommonFnc.getMethod(request);
        if (method.equals("PUT")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "devSeq,mbrTokn,storCode");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            String mbrTokn = request.getParameter("mbrTokn") == null ? "" : request.getParameter("mbrTokn");
            String storCode = request.getParameter("storCode") == null ? "" : request.getParameter("storCode");
            String encodeToken = "";
            encodeToken = mbrTokn.replaceAll(" ", "+");

            mItem.setStorCode(storCode);
            mItem.setMbrTokn(encodeToken);

            int tokenSearchResult = 0;
            try {
                tokenSearchResult = memberService.searchMemberToken(mItem);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                DeviceVO item = new DeviceVO();
                item.setDevSeq(Integer.parseInt(request.getParameter("devSeq")));
                item.setUsageMinute(0);
                item.setUseFnsTime(null);
                item.setUseYn("N");
                int resultCode = deviceService.deviceUseUpdate(item);

                if (resultCode > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                }
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            }

            return rsModel.getModelAndView();
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 이용권 사용상태 API (태그 사용 정보 업데이트)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/reservateEndUp")
    public ModelAndView reservateEndUpAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/reservate/reservateProcess");

        request.setCharacterEncoding("UTF-8");
        ReservateVO item = new ReservateVO();
        MemberVO mItem = new MemberVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "storCode,mbrTokn,tagId");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String mbrTokn = request.getParameter("mbrTokn") == null ? "" : request.getParameter("mbrTokn");
            String storCode = request.getParameter("storCode") == null ? "" : request.getParameter("storCode");
            String encodeToken = "";
            encodeToken = mbrTokn.replaceAll(" ", "+");

            mItem.setStorCode(storCode);
            mItem.setMbrTokn(encodeToken);

            int tokenSearchResult = 0;
            try {
                tokenSearchResult = memberService.searchMemberToken(mItem);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                String tagId = request.getParameter("tagId") == null ? "" : request.getParameter("tagId");
                int devSeq = 0;
                // String clear = request.getParameter("clear") == null ? "" : request.getParameter("clear");
                int storSeq = request.getParameter("storSeq") == null ? 0
                        : Integer.parseInt(request.getParameter("storSeq"));

                String fnsTime = CommonFnc.emptyCheckString("fnsTime", request);
                String stTime = CommonFnc.emptyCheckString("stTime", request);

                item.setTagId(tagId);
                item.setDevSeq(devSeq);
                DeviceVO dItem = new DeviceVO();
                dItem.setDevSeq(devSeq);
                dItem.setTagId(tagId);
                dItem.setStTime(stTime);
                dItem.setFnsTime(fnsTime);

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {

                    List<ReservateVO> Ritem = reservateService.reservateUseList(item);

                    if (Ritem.size() == 0) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        return rsModel.getModelAndView();
                    } else {
                        item.setStorSeq(Ritem.get(0).getStorSeq());
                        dItem.setStorSeq(Ritem.get(0).getStorSeq());
                    }

                    // 태그정보 없데이트
                    deviceService.contentsPlayTimeOut(dItem);
                    int result = reservateService.reservateUseUpdate(item);

                    if (devSeq == 0 || tagId.equals("")) {
                        deviceService.deviceUseStateUpdate(dItem);
                    }

                    if (result > 0) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        transactionManager.commit(status);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();
    }

    /**
     * 사용 중 장비 상세 정보 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     *
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceUseDetailInfo")
    public ModelAndView deviceUseDetailInfoProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session, @RequestBody String jsonString) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO device = new DeviceVO();

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj = (JSONObject) jsonParser.parse(jsonString);

        String devSeq = (String) jsonObj.get("DEVSEQ") == null ? "" : (String) jsonObj.get("DEVSEQ");
        String devTokn = (String) jsonObj.get("DEVTOKN") == null ? "" : (String) jsonObj.get("DEVTOKN");

        String checkString = "";
        if (devSeq.equals("")) {
            checkString += "DEVSEQ";
        }
        if (devTokn.equals("")) {
            if (!checkString.equals(""))
                checkString += ",";
            checkString += "DEVTOKN";
        }

        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage("invalid parameter(" + checkString + ")");
            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        String encodeToken = devTokn.replaceAll(" ", "+");

        device.setDevSeq(Integer.parseInt(devSeq));
        device.setDevTokn(encodeToken);
        device.setStorSeq(0);

        if (deviceService.searchDeviceToken(device) == 0) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
        } else {
            String useCpuRate = (String) jsonObj.get("CPU") == null ? "" : (String) jsonObj.get("CPU");
            String totRam = (String) jsonObj.get("TotalRAM") == null ? "" : (String) jsonObj.get("TotalRAM");
            String usedRam = (String) jsonObj.get("UsedRAM") == null ? "" : (String) jsonObj.get("UsedRAM");
            JSONArray drvInfo = (JSONArray) jsonObj.get("DRVINFO");

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < drvInfo.size(); i++) {
                JSONObject info = (JSONObject) drvInfo.get(i);
                sb.append(info.get("NAME"));
                sb.append(" ");
                sb.append(info.get("TotalFreeSpace") + " / " + info.get("TotalSize"));
                if (i < drvInfo.size() - 1) {
                    sb.append(", ");
                }
            }

            device.setDevUseCpuRate(useCpuRate);
            device.setDevUsageMem(usedRam + " / " + totRam);
            device.setDevStrgeSpace(sb.toString());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                deviceService.updateDeviceUseDetailInfo(device);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * For Test API - 사용 중 장비 상세정보 확인용
     *
     * @param modelView - 화면 정보를 저장하는 변수
     *
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/getDeviceUseDetailInfo")
    public ModelAndView getDeviceUseDetailInfoProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO device = new DeviceVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "devSeq,devTokn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            String token = CommonFnc.emptyCheckString("devTokn", request);
            String encodeToken = token.replaceAll(" ", "+");

            device.setDevSeq(devSeq);
            device.setDevTokn(encodeToken);
            device.setStorSeq(0);

            if (deviceService.searchDeviceToken(device) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                DeviceVO result = deviceService.getDeviceUseDetailInfo(device);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", result);
                rsModel.setResultType("deviceUseDetailInfoAPI");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 멤버 토큰을 이용한 콘텐츠 상세 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsdetail")
    public ModelAndView contentsDetailProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");
        ContentVO item = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq,contsSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            MemberVO mItem = new MemberVO();

            String mbrTokn = request.getParameter("mbrTokn") == null ? "" : request.getParameter("mbrTokn");
            int storSeq = request.getParameter("storSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("storSeq"));
            String encodeToken = "";
            encodeToken = mbrTokn.replaceAll(" ", "+");

            mItem.setStorSeq(storSeq);
            mItem.setMbrTokn(encodeToken);

            int tokenSearchResult = 0;
            tokenSearchResult = memberService.searchMemberToken(mItem);

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                int contsSeq = request.getParameter("contsSeq") == null ? 0
                        : Integer.parseInt(request.getParameter("contsSeq"));

                item.setContsSeq(contsSeq);
                /** 해당 콘텐츠 장르 리스트 */
                List<ContentVO> genreList = contentService.contentsGenreList(item);

                /** 해당 콘텐츠 상세정보 */
                ContentVO resultItem = contentService.contentInfo(item);

                /** 해당 콘텐츠 서비스 리스트 */
                ContentVO coverImg = contentService.contentCoverImageInfo(item);

                /** 해당 콘텐츠 서비스 리스트 */
                List<ContentVO> serviceList = contentService.contentServiceList(item);

                /** 해당 콘텐츠 갤러리 이미지 리스트 */
                List<ContentVO> thumbnailList = contentService.contentsThumbnailList(item);

                /** 해당 콘텐츠 비디오 파일 리스트 */
                List<ContentVO> videoList = contentService.contentsVideoList(item);

                /** 해당 콘텐츠 실행 파일 리스트 */
                List<ContentVO> fileList = contentService.fileDataInfo(item);

                /** 미리보기 영상 파일 리스트 */
                List<ContentVO> prevList = contentService.contentsPrevList(item);

                for (ContentVO videoItem : videoList) {
                    ContentVO metaItem = new ContentVO();
                    metaItem.setContsSeq(contsSeq);
                    metaItem.setFilePath(videoItem.getFilePath());
                    ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                    ContentVO contsMetaInfo = contentService.contsMetaInfo(metaItem);
                    videoItem.setMetadataXmlInfo(metadataInfo);
                    videoItem.setContsXmlInfo(contsMetaInfo);
                }

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    // 전시페이지 관리에서 콘텐츠 상세정보 요청 시에는 상태값이 승인완료 또는 전시완료 일 경우에만 접근 가능
                    String reqType = CommonFnc.emptyCheckString("reqType", request);
                    if (reqType.equals("display") && !resultItem.getSttusVal().equals("05")
                            && !resultItem.getSttusVal().equals("06")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage("Bad Request");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }

                    // 콘텐츠 서브 메타데이터 정보 확인
                    int subMetaSeq = resultItem.getContsSubMetadataSeq();
                    String subMetadata = resultItem.getMetaData();
                    Map<String, String> mData = new HashMap<String, String>();
                    if (subMetaSeq > 0 && !subMetadata.equals("")) {
                        String subMetaCommCtgCode = "CONTS_SUB_META";

                        if (!subMetaCommCtgCode.equals("")) {

                            JSONParser jsonParse = new JSONParser();
                            JSONObject jsonObj = (JSONObject) jsonParse.parse(subMetadata);
                            for (Object key : jsonObj.keySet()) {
                                String keyStr = (String) key;
                                String keyVal = jsonObj.get(keyStr).toString();

                                if (!keyVal.equals("")) {
                                    ContentVO vo = new ContentVO();
                                    vo.setMetaDataCtgCode(subMetaCommCtgCode);
                                    vo.setMetaDataKeyVal(keyStr);
                                    String subMetaCommCtgNm = contentService.getSubMetaCodeNm(vo);

                                    // 서브메타데이터 항목 이름에 'RT'이라는 단어가 포함되어 있을경우 해당 단어의 '''를 '분', '"'를 '초'로 표시함
                                    String rtTime = messageSource.getMessage("submeta.rt", null, Locale.KOREA);
                                    String rtMinute = messageSource.getMessage("common.minute", null, Locale.KOREA);
                                    String rtSecond = messageSource.getMessage("common.second", null, Locale.KOREA);
                                    if (subMetaCommCtgNm.contains(rtTime)) {
                                        subMetaCommCtgNm = subMetaCommCtgNm.replace("\'", rtMinute);
                                        subMetaCommCtgNm = subMetaCommCtgNm.replace("\"", rtSecond);
                                    }

                                    // 서브메타데이터 항목 이름에 '시간'이라는 단어가 포함되어 있을경우 해당 값에 '분'을 표시함
                                    String strTime = messageSource.getMessage("common.time", null, Locale.KOREA);
                                    if (subMetaCommCtgNm.contains(strTime)) {
                                        keyVal = keyVal + messageSource.getMessage("common.minute", null, Locale.KOREA);
                                    }
                                    // 서브메타데이터 항목 이름에 'RT'이라는 단어가 포함되어 있을경우 해당 값의 '#a8484'를 '분', '#q0808'를 '초'로 표시함
                                    if (subMetaCommCtgNm.contains(rtTime)) {
                                        keyVal = keyVal.replace("#a8484", rtMinute);
                                        keyVal = keyVal.replace("#q0808", rtSecond);
                                    }

                                    mData.put(subMetaCommCtgNm, keyVal);
                                }
                            }
                        }
                    }

                    // 콘텐츠 클릭 횟수는 서비스 번호와 함께 저장, 어떤 서비스에서 콘텐츠 상세 조회를 한 것인지 확인하기 위함.
                    int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                    if (svcSeq > 0) {
                        // 콘텐츠 상세 정보 조회 이력 추가
                        OlsvcVO vo = new OlsvcVO();
                        vo.setSvcSeq(svcSeq);
                        vo.setContsSeq(contsSeq);
                        olsvcService.insertContsDetailClickHst(vo);
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("subMetadataVal", mData);
                    rsModel.setData("genreList", genreList);
                    rsModel.setData("serviceList", serviceList);
                    rsModel.setData("videoList", videoList);
                    rsModel.setData("thumbnailList", thumbnailList);
                    rsModel.setData("fileList", fileList);
                    rsModel.setData("prevList", prevList);
                    rsModel.setData("coverImg", coverImg);
                }
                rsModel.setResultType("contentsDetail");
            }
        }
        return rsModel.getModelAndView();
    }

    /**
     * BLCK CHAIN POST API - 블록체인 정보 보내는 것과 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/playContsBlock")
    public ModelAndView playContsBlock(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO device = new DeviceVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "devSeq,devTokn,storSeq,contsSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String token = CommonFnc.emptyCheckString("devTokn", request);
            String encodeToken = token.replaceAll(" ", "+");

            device.setDevSeq(devSeq);
            device.setDevTokn(encodeToken);
            device.setStorSeq(storSeq);

            if (deviceService.searchDeviceToken(device) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                /* BLOCK CHAIN Start */
                ContentVO Citem = new ContentVO();
                Citem.setContsSeq(contsSeq);
                Citem.setCpSeq(0);
                Citem.setContsCtgSeq(0);
                Citem.setSttusVal("");
                Citem.setGenreSeq(0);
                Citem.setList(null);
                Citem.setSvcSeq(0);
                /** 해당 콘텐츠 상세정보 */

                ContentVO cntInfo = contentService.contentCdtNotInfo(Citem);
                JSONObject json = new JSONObject();
                StoreVO storItem = new StoreVO();
                storItem.setStorSeq(storSeq);
                StoreVO resultSItem = storeService.storeDetail(storItem);

                json.put("cpSeq", String.valueOf(cntInfo.getCpSeq()));
                json.put("svcSeq", String.valueOf(resultSItem.getSvcSeq()));
                json.put("devSeq", String.valueOf(devSeq));
                json.put("status", "Start");
                json.put("storSeq", String.valueOf(storSeq));
                json.put("contsSeq", String.valueOf(contsSeq));

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                sdf1.setTimeZone(TimeZone.getTimeZone("GMT"));
                String datetime1 = sdf1.format(cal.getTime());

                json.put("dateTime", datetime1);

                String Url_Block = configProperty.getProperty("chain.url");
                System.out.println("[JsonData]:" + json.toString() + "[URL]:" + Url_Block);
                jsonPostAsync.getObjectAsync(json.toString(), Url_Block);
                /* BLOCK CHAIN End */

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setResultType("blockChainPut");
            }
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "devSeq,devTokn,storSeq,contsSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String token = CommonFnc.emptyCheckString("devTokn", request);
            String encodeToken = token.replaceAll(" ", "+");

            device.setDevSeq(devSeq);
            device.setDevTokn(encodeToken);
            device.setStorSeq(0);

            if (deviceService.searchDeviceToken(device) == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                /* BLOCK CHAIN Start */
                ContentVO Citem = new ContentVO();
                Citem.setContsSeq(contsSeq);
                Citem.setCpSeq(0);
                Citem.setContsCtgSeq(0);
                Citem.setSttusVal("");
                Citem.setGenreSeq(0);
                Citem.setList(null);
                Citem.setSvcSeq(0);

                /** 해당 콘텐츠 상세정보 */
                ContentVO cntInfo = contentService.contentCdtNotInfo(Citem);

                JSONObject json = new JSONObject();
                StoreVO storItem = new StoreVO();
                storItem.setStorSeq(storSeq);
                StoreVO resultSItem = storeService.storeDetail(storItem);

                json.put("cpSeq", String.valueOf(cntInfo.getCpSeq()));
                json.put("svcSeq", String.valueOf(resultSItem.getSvcSeq()));
                json.put("devSeq", String.valueOf(devSeq));
                json.put("status", "Stop");
                json.put("storSeq", String.valueOf(storSeq));
                json.put("contsSeq", String.valueOf(contsSeq));

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                sdf1.setTimeZone(TimeZone.getTimeZone("GMT"));
                String datetime1 = sdf1.format(cal.getTime());

                json.put("dateTime", datetime1);
                System.out.println(datetime1);

                String Url_Block = configProperty.getProperty("chain.url");
                System.out.println("[JsonData]:" + json.toString() + "[URL]:" + Url_Block);
                jsonPostAsync.getObjectAsync(json.toString(), Url_Block);

                /* BLOCK CHAIN End */

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setResultType("blockChainPost");
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();
    }

    /* 외부 URL 정보 POST 함수_ 현재 이용: [블록체인 데이터 전송] */
    public static String postJson(String serverUrl, String host, String jsonobject, String Method) {

        StringBuilder sb = new StringBuilder();

        String http = serverUrl;

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(http);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(Method);
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(100000);
            urlConnection.setReadTimeout(10);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Host", host);
            urlConnection.connect();
            // You Can also Create JSONObject here
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonobject);// here i sent the parameter
            out.close();
            int HttpResult = urlConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                // Log.e("new Test", "" + sb.toString());
                return sb.toString();
            } else {
                // Log.e(" ", "" + urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        } catch (JSONException e) {
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }
    /*
     * private String getSubMetaCommCtgCode(String ctgNm, List<ContentVO> genreList) {
     * String videoStr = messageSource.getMessage("contents.category.video", null, Locale.KOREA);
     * String subMetaCommCtg = "";
     * if (!ctgNm.contains(videoStr)) {
     * return subMetaCommCtg;
     * }
     * String ctgNmLiveOn = messageSource.getMessage("sub.liveon", null, Locale.KOREA);
     * String genreNmSport = messageSource.getMessage("submeta.sports", null, Locale.KOREA);
     * String genreNmMv = messageSource.getMessage("submeta.mv", null, Locale.KOREA);
     * String genreNmMovie = messageSource.getMessage("submeta.movie", null, Locale.KOREA);
     * String genreNmDramaMovie = messageSource.getMessage("submeta.drama.movie", null, Locale.KOREA);
     * String genreNmLife = messageSource.getMessage("submeta.life", null, Locale.KOREA);
     * String genreNmSportsActivity = messageSource.getMessage("submeta.sports.activity", null, Locale.KOREA);
     * String genreNmEntertainment = messageSource.getMessage("submeta.entertainment", null, Locale.KOREA);
     * String genreNmGenieMusic = messageSource.getMessage("submeta.geniemusic", null, Locale.KOREA);
     * String genreNmWebtoon = messageSource.getMessage("submeta.webtoon", null, Locale.KOREA);
     * String genreNmLivetv = messageSource.getMessage("submeta.livetv", null, Locale.KOREA);
     * for (int i = 0; i < genreList.size(); i++) {
     * String genreNm = genreList.get(i).getGenreNm();
     * if (genreNm.equals(genreNmDramaMovie) || genreNm.equals(genreNmLife) || genreNm.equals(genreNmSportsActivity)
     * || genreNm.equals(genreNmEntertainment) || genreNm.equals(genreNmGenieMusic) || genreNm.equals(genreNmWebtoon)
     * || genreNm.equals(genreNmLivetv)) {
     * subMetaCommCtg = "SM_LIVEON";
     * } else if (genreNm.equals(genreNmSport)) {
     * subMetaCommCtg = "SMS";
     * } else if (genreNm.equals(genreNmMv)) {
     * subMetaCommCtg = "SMMV";
     * } else if (genreNm.equals(genreNmMovie)) {
     * subMetaCommCtg = "SMM";
     * }
     * }
     * return subMetaCommCtg;
     * }
     */

    /**
     * 장르 노출 순서 조회 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/genreDispOrder")
    public ModelAndView genreDispOrderProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/genre/genreProcess");

        request.setCharacterEncoding("UTF-8");
        GenreVO vo = new GenreVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String invalidParams = CommonFnc.requiredChecking(request, "mbrTokn,storSeq,firstCtgId");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            String firstCtgId = CommonFnc.emptyCheckString("firstCtgId", request);
            vo.setFirstCtgId(firstCtgId);

            List<GenreVO> resultList = genreService.selectGenreDispOrderList(vo);
            if (resultList.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultList);
                rsModel.setResultType("genreOrderList");
            }
        }

        return rsModel.getModelAndView();
    }

}
