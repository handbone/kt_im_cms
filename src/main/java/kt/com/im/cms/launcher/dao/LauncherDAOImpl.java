/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.launcher.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.StateAbstractMapper;
import kt.com.im.cms.device.vo.DeviceVO;

/**
 *
 * 외부 API 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 28.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("LauncherDAO")
@Transactional
public class LauncherDAOImpl extends StateAbstractMapper implements LauncherDAO {

    /**
     * (외부) 장비에서 정보 가져갈 경우 컨텐츠 상태 수정
     *
     * @param DeviceVO
     * @return 컨텐츠 상태 수정 성공 여부
     */
    @Override
    public int deviceContentsConditionUpdate(DeviceVO vo) {
        int res = update("DeviceStatDAO.deviceContentsConditionUpdate", vo);
        return res;
    }

    /**
     * 장비 - 콘텐츠 실행 로그 등록
     *
     * @param DeviceVO
     * @return 등록 성공 여부
     */
    @Override
    public int contentsPlayLogInsert(DeviceVO vo) {
        int res = insert("DeviceStatDAO.contentsPlayLogInsert", vo);
        return res;
    }

    /**
     * (외부) 컨텐츠 다운로드 로그 등록
     *
     * @param DeviceVO
     * @return 컨텐츠 다운로드 로그 등록 성공 여부
     */
    @Override
    public int contentsDownInsert(DeviceVO vo) {
        int res = insert("DeviceStatDAO.contentsDownInsert", vo);
        return res;
    }

    /**
     * 장비 - API 정상동작 테스트
     *
     * @param
     * @return
     */
    @Override
    public void noneList() {
        selectList("DeviceStatDAO.noneList");
    }

}
