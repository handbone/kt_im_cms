/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.controller;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.LocaleMessageSource;
import kt.com.im.cms.member.web.MemberApi;

/**
 * Handles requests for the application home page.
 */
@Controller
public class CmsPageController {

    @Resource(name="localeMessageSource")
    LocaleMessageSource messageSource;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(Locale locale, Model model, HttpSession session, HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        if (session != null && session.getAttribute("id") != null) {
            if (MemberApi.MEMBER_SECTION_STORE.equals(session.getAttribute("memberSec"))) {
                mv.setViewName("redirect:/dashboard/contents/registered");
            } else {
                mv.setViewName("redirect:/dashboard/contents/verify");
            }
            return mv;
        }

        String requestUri = request.getParameter("uri");
        if (requestUri != null && !requestUri.isEmpty()) {
            mv.addObject("requestUri", requestUri);
        }
        mv.setViewName("/views/login");
        return mv;
    }

    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    public ModelAndView auth(Locale locale, Model model, HttpSession session, HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        if (session == null || session.getAttribute(MemberApi.TEMPORARY_ID) == null) {
            mv.setViewName("redirect:/");
            return mv;
        }

        boolean mustChangePassword = (boolean)session.getAttribute(MemberApi.MUST_CHANGE_PWD);
        if (mustChangePassword) {
            mv.setViewName("/views/cautionPwd");
            return mv;
        }

        String requestUri = request.getParameter("uri");
        if (requestUri != null && !requestUri.isEmpty()) {
            mv.addObject("requestUri", requestUri);
        }

        mv.setViewName("/views/auth");
        return mv;
    }

    @RequestMapping(value = "/crossdomain.xml", method = RequestMethod.GET)
    public ModelAndView crossdomain(Locale locale, Model model) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/views/crossdomain");
        return mv;
    }

    /**
     * 대시보드 콘텐츠 현황 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환   
     */
    @RequestMapping(value = "/dashboard/contents", method = RequestMethod.GET)
    public ModelAndView contents(Locale locale, HttpSession session, Model model) {
        ModelAndView mv = new ModelAndView();

        if (MemberApi.MEMBER_SECTION_CP.equals(session.getAttribute("memberSec"))) {
            mv.setViewName("redirect:/");
            return mv;
        }

        if (MemberApi.MEMBER_SECTION_STORE.equals(session.getAttribute("memberSec"))) {
            mv.setViewName("redirect:/dashboard/contents/registered");
            return mv;
        }

        mv.setViewName("redirect:/dashboard/contents/verify");
        return mv;
    }

    /**
     * 대시보드 콘텐츠 현황 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환   
     */
    @RequestMapping(value = "/dashboard/contents/verify", method = RequestMethod.GET)
    public ModelAndView totalContents(Locale locale, HttpSession session, Model model) {
        ModelAndView mv = new ModelAndView();
        if (MemberApi.MEMBER_SECTION_STORE.equals(session.getAttribute("memberSec"))
         || MemberApi.MEMBER_SECTION_CP.equals(session.getAttribute("memberSec"))) {
            mv.setViewName("redirect:/");
            return mv;
        }

        mv.setViewName("/views/dashboard/TotalContents");
        return mv;
    }

    /**
     * 대시보드 콘텐츠 현황 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환   
     */
    @RequestMapping(value = "/dashboard/contents/registered", method = RequestMethod.GET)
    public ModelAndView registeredContents(Locale locale, HttpSession session, Model model) {
        ModelAndView mv = new ModelAndView();
        if (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(session.getAttribute("memberSec"))
         || MemberApi.MEMBER_SECTION_CP.equals(session.getAttribute("memberSec"))) {
                mv.setViewName("redirect:/");
            return mv;
        }

        mv.setViewName("/views/dashboard/RegisteredContents");
        return mv;
    }

    /**
     * 대시보드 매장 현황 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환   
     */
    @RequestMapping(value = "/dashboard/store", method = RequestMethod.GET)
    public ModelAndView home(Locale locale, Model model) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/views/dashboard/Store");
        return mv;
    }

    @RequestMapping(value = "/resources/js/message.js", method = RequestMethod.GET)
    public void message(Locale locale, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Properties properties = messageSource.getProperties(locale);
        if (properties == null)
            return;

        String category = request.getParameter("category");
        response.resetBuffer();
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        StringBuilder builder = new StringBuilder();
        if (category != null && !category.isEmpty())
            builder.append("get" + category.substring(0, 1).toUpperCase() + category.substring(1) + "Message = function(id) {\r\n");
        else
            builder.append("getMessage = function(id) {\r\n");

        Iterator iterator = properties.keySet().iterator();
        while (iterator.hasNext()) {
            String key = (String)iterator.next();

            if ("login".equals(category)) {
                if (!key.contains("info.")) {
                    if ((category != null && !category.isEmpty()) && !key.contains(category)) {
                        continue;
                    }
                }
            } else {
                if ((category != null && !category.isEmpty()) && !key.contains(category)) {
                    continue;
                }
            }

            String value = (String)properties.get(key);
            builder.append("    if (id == \"" + key + "\") {\r\n");
            builder.append("        return \"" + value + "\";\r\n");
            builder.append("    }\r\n");
        }
        builder.append("    return '';\r\n");
        builder.append("}\r\n");

        out.println(builder.toString());

        response.flushBuffer();
    }

    /*Error Page S*/
    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView error(Locale locale, Model model) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/views/error");
        return mv;
    }

    /*Error Page E*/
}
