/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicefaq.service;

import java.util.List;

import kt.com.im.cms.servicefaq.vo.ServiceFaqVO;

/**
 *
 * 서비스 FAQ 관련 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public interface ServiceFaqService {

    List<ServiceFaqVO> svcFaqList(ServiceFaqVO vo) throws Exception;

    int svcFaqListTotalCount(ServiceFaqVO vo) throws Exception;

    ServiceFaqVO svcFaqDetail(ServiceFaqVO vo) throws Exception;

    int updateSvcFaqRetvNum(ServiceFaqVO vo) throws Exception;

    int insertSvcFaq(ServiceFaqVO vo) throws Exception;

    int updateSvcFaq(ServiceFaqVO vo) throws Exception;

    int deleteSvcFaq(ServiceFaqVO vo) throws Exception;

    List<ServiceFaqVO> getOlSvcFaqDetailList(ServiceFaqVO vo) throws Exception;

    int getOlSvcFaqDetailListTotalCount(ServiceFaqVO vo) throws Exception;

    List<ServiceFaqVO> olSvcFaqList(ServiceFaqVO vo) throws Exception;

    int olSvcFaqListTotalCount(ServiceFaqVO vo) throws Exception;

    ServiceFaqVO olSvcFaqDetail(ServiceFaqVO vo) throws Exception;

    int insertOlSvcFaq(ServiceFaqVO vo) throws Exception;

    int updateOlSvcFaq(ServiceFaqVO vo) throws Exception;

    int deleteOlSvcFaq(ServiceFaqVO vo) throws Exception;

}
