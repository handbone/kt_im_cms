/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicefaq.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.servicefaq.service.ServiceFaqService;
import kt.com.im.cms.servicefaq.vo.ServiceFaqVO;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ResultModel;

/**
 *
 * 서비스 FAQ 관련 처리 API
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class ServiceFaqApi {

    private final static String viewName = "../resources/api/service/customer/serviceFaq/serviceFaqProcess";
    private final static String olSvcViewName = "../resources/api/onlineService/customer/serviceFaq/serviceFaqProcess";

    @Resource(name = "ServiceFaqService")
    private ServiceFaqService svcFaqService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * 서비스 FAQ 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/svcFaq")
    public ModelAndView svcFaqProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        String svcType = CommonFnc.emptyCheckString("svcType", request);
        if (userVO == null || !isAllowMember(userVO, svcType)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        if (svcType.equals("")) {
            rsModel.setViewName(viewName);
            return offlineSvcFaqProcess(request, rsModel, userVO);
        }

        if (svcType.equals("ON")) {
            rsModel.setViewName(olSvcViewName);
            return onlineSvcFaqProcess(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * (Offline) 서비스 FAQ - 현재는 서비스 매장관리 > 고객센터 관리 > FAQ
     */
    private ModelAndView offlineSvcFaqProcess(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        request.setCharacterEncoding("UTF-8");

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return addOfflineSvcFaq(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return editOfflineSvcFaq(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return delOfflineSvcFaq(request, rsModel, userVO);
        }

        if (method.equals("GET")) {
            return getOfflineSvcFaq(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addOfflineSvcFaq(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String checkString = CommonFnc.requiredChecking(request, "svcFaqTitle,svcFaqType,svcFaqCtg,svcFaqSbst");
        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage("invalid parameter(" + checkString + ")");
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        String svcFaqTitle = CommonFnc.emptyCheckString("svcFaqTitle", request);
        int svcFaqType = CommonFnc.emptyCheckInt("svcFaqType", request);
        String svcFaqCtg = CommonFnc.emptyCheckString("svcFaqCtg", request);
        String svcFaqSbst = CommonFnc.emptyCheckString("svcFaqSbst", request);

        ServiceFaqVO item = new ServiceFaqVO();

        item.setSvcFaqTitle(svcFaqTitle);
        item.setSvcFaqType(svcFaqType);
        item.setSvcFaqCtg(svcFaqCtg);
        item.setSvcFaqSbst(svcFaqSbst);
        item.setLoginId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcFaqService.insertSvcFaq(item);
            if (result > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView editOfflineSvcFaq(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String checkString = CommonFnc.requiredChecking(request, "svcFaqSeq,svcFaqTitle,svcFaqType,svcFaqCtg,svcFaqSbst,delYn");
        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage("invalid parameter(" + checkString + ")");
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        int svcFaqSeq = CommonFnc.emptyCheckInt("svcFaqSeq", request);
        String svcFaqTitle = CommonFnc.emptyCheckString("svcFaqTitle", request);
        int svcFaqType = CommonFnc.emptyCheckInt("svcFaqType", request);
        String svcFaqCtg = CommonFnc.emptyCheckString("svcFaqCtg", request);
        String svcFaqSbst = CommonFnc.emptyCheckString("svcFaqSbst", request);
        String delYn = request.getParameter("delYn") == null ? "" : request.getParameter("delYn");

        ServiceFaqVO item = new ServiceFaqVO();

        item.setSvcFaqSeq(svcFaqSeq);
        item.setSvcFaqTitle(svcFaqTitle);
        item.setSvcFaqType(svcFaqType);
        item.setSvcFaqCtg(svcFaqCtg);
        item.setSvcFaqSbst(svcFaqSbst);
        item.setDelYn(delYn);
        item.setLoginId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcFaqService.updateSvcFaq(item);
            if (result > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView delOfflineSvcFaq(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String svcFaqSeqList = CommonFnc.emptyCheckString("delSvcFaqSeqList", request);
        String[] delFaqSeqList = svcFaqSeqList.split(",");

        if (delFaqSeqList.length == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No FAQ list to delete.)");
            return rsModel.getModelAndView();
        }

        List<Integer> deleteList = new ArrayList<Integer>();
        for (int i = 0; i < delFaqSeqList.length; i++) {
            deleteList.add(Integer.parseInt(delFaqSeqList[i]));
        }

        ServiceFaqVO item = new ServiceFaqVO();

        item.setLoginId(userVO.getMbrId());
        item.setDeleteSvcFaqSeqList(deleteList);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            svcFaqService.deleteSvcFaq(item);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            transactionManager.commit(status);
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOfflineSvcFaq(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        ServiceFaqVO item = new ServiceFaqVO();
        int svcFaqSeq = CommonFnc.emptyCheckInt("svcFaqSeq", request);

        if (svcFaqSeq != 0) {
            item.setSvcFaqSeq(svcFaqSeq);

            ServiceFaqVO resultItem = svcFaqService.svcFaqDetail(item);

            if (resultItem == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                String searchType = CommonFnc.emptyCheckString("searchType", request);
                boolean isAllowFaq = this.isAllowFaq(userVO, resultItem, searchType);
                if (!isAllowFaq) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                // 서비스 FAQ 수정을 위해 상세 정보 요청 시에는 조회 수 업데이트 하지 않음.
                if (!searchType.equals("edit")) {
                    svcFaqService.updateSvcFaqRetvNum(item);
                    resultItem.setRetvNum(resultItem.getRetvNum() + 1);
                }

                // 개인정보 마스킹 (등록자 이름)
                resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                // 내용 태그 원복
                resultItem.setSvcFaqSbst(CommonFnc.unescapeStr(resultItem.getSvcFaqSbst()));

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setResultType("svcFaqDetail");
            }
        } else {
            // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
            if (svcFaqSeq == 0 && CommonFnc.checkReqParameter(request, "svcFaqSeq")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                return rsModel.getModelAndView();
            }

            item.setMbrSe(userVO.getMbrSe());
            item.setMbrSvcSeq(userVO.getSvcSeq());
            item.setMbrStorSeq(userVO.getStorSeq());

            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            item.setSearchConfirm(searchConfirm);
            // 검색 여부가 true일 경우
            if (searchConfirm.equals("true")) {
                String searchField = CommonFnc.emptyCheckString("searchField", request);
                String searchString = CommonFnc.emptyCheckString("searchString", request);

                if (searchField == "svcFaqTitle") {
                    searchField = "SVC_FAQ_TITLE";
                } else if (searchField == "cretrNm") {
                    searchField = "CRETR_NM";
                } else if (searchField == "svcFaqCtgNm") {
                    searchField = "SVC_FAQ_CTG_NM";
                }

                item.setSearchTarget(searchField);
                item.setSearchKeyword(searchString);
            }
            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
            String sidx = CommonFnc.emptyCheckString("sidx", request);
            String sord = CommonFnc.emptyCheckString("sord", request);

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
            if (sidx.equals("num")) {
                sidx = "SVC_FAQ_SEQ";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setOffset(page);
            item.setLimit(pageEnd);

            List<ServiceFaqVO> resultList = svcFaqService.svcFaqList(item);
            if (resultList.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (등록자 이름) */
                for (int i = 0; i < resultList.size(); i++) {
                    resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
                }

                int totalCount = svcFaqService.svcFaqListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultList);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("svcFaqList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * (Online) 서비스 FAQ - 현재는 서비스 매장관리 > 고객센터 관리 > FAQ
     */
    private ModelAndView onlineSvcFaqProcess(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        request.setCharacterEncoding("UTF-8");

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return addOnlineSvcFaq(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return editOnlineSvcFaq(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return delOnlineSvcFaq(request, rsModel, userVO);
        }

        if (method.equals("GET")) {
            boolean isList = (request.getParameter("svcFaqSeq") == null);
            if (isList) {
                return getOnlineSvcFaqList(request, rsModel, userVO);
            }

            return getOnlineSvcFaq(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addOnlineSvcFaq(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String checkString = CommonFnc.requiredChecking(request, "svcFaqType,targetType,svcFaqCtg,svcFaqTitle,svcFaqSbst,isPost");
        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage("invalid parameter(" + checkString + ")");
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        int svcFaqType = CommonFnc.emptyCheckInt("svcFaqType", request);
        if (!isAllowOlSvcFaq(userVO, svcFaqType)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcFaqType(svcSeq))");
            return rsModel.getModelAndView();
        }

        String targetType = CommonFnc.emptyCheckString("targetType", request);
        String svcFaqCtg = CommonFnc.emptyCheckString("svcFaqCtg", request);
        String svcFaqTitle = CommonFnc.emptyCheckString("svcFaqTitle", request);
        String svcFaqSbst = CommonFnc.emptyCheckString("svcFaqSbst", request);
        String isPost = CommonFnc.emptyCheckString("isPost", request);

        ServiceFaqVO item = new ServiceFaqVO();
        item.setSvcFaqType(svcFaqType);
        item.setTargetType(targetType);
        item.setSvcFaqCtg(svcFaqCtg);
        item.setSvcFaqTitle(svcFaqTitle);
        item.setSvcFaqSbst(svcFaqSbst);

        if (isPost.equals("Y")) {
            item.setDelYn("N");
        } else {
            item.setDelYn("Y");
        }

        item.setLoginId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcFaqService.insertOlSvcFaq(item);
            if (result > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView editOnlineSvcFaq(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String checkString = CommonFnc.requiredChecking(request, "svcFaqSeq,svcFaqType,targetType,svcFaqCtg,svcFaqTitle,svcFaqSbst,isPost");
        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage("invalid parameter(" + checkString + ")");
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        int svcFaqSeq = CommonFnc.emptyCheckInt("svcFaqSeq", request);
        if (svcFaqSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcNoticeSeq)");
            return rsModel.getModelAndView();
        }

        int svcFaqType = CommonFnc.emptyCheckInt("svcFaqType", request);
        if (!isAllowOlSvcFaq(userVO, svcFaqType)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcFaqType(svcSeq))");
            return rsModel.getModelAndView();
        }

        String targetType = CommonFnc.emptyCheckString("targetType", request);
        String svcFaqCtg = CommonFnc.emptyCheckString("svcFaqCtg", request);
        String svcFaqTitle = CommonFnc.emptyCheckString("svcFaqTitle", request);
        String svcFaqSbst = CommonFnc.emptyCheckString("svcFaqSbst", request);
        String isPost = CommonFnc.emptyCheckString("isPost", request);

        ServiceFaqVO item = new ServiceFaqVO();
        item.setSvcFaqSeq(svcFaqSeq);
        item.setSvcFaqType(svcFaqType);
        item.setTargetType(targetType);
        item.setSvcFaqCtg(svcFaqCtg);
        item.setSvcFaqTitle(svcFaqTitle);
        item.setSvcFaqSbst(svcFaqSbst);

        if (isPost.equals("Y")) {
            item.setDelYn("N");
        } else {
            item.setDelYn("Y");
        }

        item.setLoginId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcFaqService.updateOlSvcFaq(item);
            if (result > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView delOnlineSvcFaq(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String svcFaqSeqList = CommonFnc.emptyCheckString("delSvcFaqSeqList", request);
        String[] delFaqSeqList = svcFaqSeqList.split(",");

        if (delFaqSeqList.length == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No FAQ list to delete.)");
            return rsModel.getModelAndView();
        }

        List<Integer> deleteList = new ArrayList<Integer>();
        for (int i = 0; i < delFaqSeqList.length; i++) {
            deleteList.add(Integer.parseInt(delFaqSeqList[i]));
        }

        ServiceFaqVO item = new ServiceFaqVO();

        item.setLoginId(userVO.getMbrId());
        item.setDeleteSvcFaqSeqList(deleteList);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcFaqService.deleteOlSvcFaq(item);
            if (delFaqSeqList.length == result) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOnlineSvcFaqList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        /*
        int svcFaqSeq = CommonFnc.emptyCheckInt("svcFaqSeq", request);

        // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
        if (svcFaqSeq == 0 && CommonFnc.checkReqParameter(request, "svcFaqSeq")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            return rsModel.getModelAndView();
        }
        */

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        // 서비스 관리자는 해당 서비스의 FAQ만 조회 가능
        if (!isAllowOlSvcFaq(userVO, svcSeq)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            return rsModel.getModelAndView();
        }

        ServiceFaqVO item = new ServiceFaqVO();

        item.setSvcFaqType(svcSeq);
        item.setMbrSe(userVO.getMbrSe());

        String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
        item.setSearchConfirm(searchConfirm);
        // 검색 여부가 true일 경우
        if (searchConfirm.equals("true")) {
            String searchField = CommonFnc.emptyCheckString("searchField", request);
            String searchString = CommonFnc.emptyCheckString("searchString", request);

            if (searchField == "svcFaqTitle") {
                searchField = "SVC_FAQ_TITLE";
            } else if (searchField == "cretrNm") {
                searchField = "CRETR_NM";
            } else if (searchField == "svcFaqCtgNm") {
                searchField = "SVC_FAQ_CTG_NM";
            }

            item.setSearchTarget(searchField);
            item.setSearchKeyword(searchString);
        }
        /*
         * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
         */
        int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
        int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
        String sidx = CommonFnc.emptyCheckString("sidx", request);
        String sord = CommonFnc.emptyCheckString("sord", request);

        int page = (currentPage - 1) * limit + 1;
        int pageEnd = page + limit - 1;

        // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
        if (sidx.equals("num")) {
            sidx = "SVC_FAQ_SEQ";
        }

        item.setSidx(sidx);
        item.setSord(sord);
        item.setOffset(page);
        item.setLimit(pageEnd);

        List<ServiceFaqVO> resultList = svcFaqService.olSvcFaqList(item);
        if (resultList.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            /* 개인정보 마스킹 (등록자 이름) */
            for (int i = 0; i < resultList.size(); i++) {
                resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
            }

            int totalCount = svcFaqService.olSvcFaqListTotalCount(item);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultList);
            rsModel.setData("totalCount", totalCount);
            rsModel.setData("row", limit);
            rsModel.setData("currentPage", currentPage);
            int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;
            rsModel.setData("totalPage", totalPage);
            rsModel.setResultType("olSvcFaqList");
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getOnlineSvcFaq(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        int svcFaqSeq = CommonFnc.emptyCheckInt("svcFaqSeq", request);
        if (svcFaqSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(noticeSeq)");
            return rsModel.getModelAndView();
        }

        ServiceFaqVO item = new ServiceFaqVO();
        item.setSvcFaqSeq(svcFaqSeq);
        item.setMbrSe(userVO.getMbrSe());

        ServiceFaqVO resultItem = svcFaqService.olSvcFaqDetail(item);

        if (resultItem == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            if (!isAllowOlSvcFaq(userVO, resultItem.getSvcFaqType())) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            String searchType = CommonFnc.emptyCheckString("searchType", request);
            // 서비스 FAQ 수정을 위해 상세 정보 요청 시에는 조회 수 업데이트 하지 않음.
            if (!searchType.equals("edit")) {
                svcFaqService.updateSvcFaqRetvNum(item);
                resultItem.setRetvNum(resultItem.getRetvNum() + 1);
            }

            // 개인정보 마스킹 (등록자 이름)
            resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

            // 내용 태그 원복
            resultItem.setSvcFaqSbst(CommonFnc.unescapeStr(resultItem.getSvcFaqSbst()));

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultItem);
            rsModel.setResultType("olSvcFaqDetail");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 외부 서비스 사이트를 위한 서비스 FAQ 목록 및 상세정보 조회 요청 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    /*
    @RequestMapping(value = "/api/getFaqAPI")
    public ModelAndView getFaqProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        ServiceFaqVO item = new ServiceFaqVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int faqSeq = CommonFnc.emptyCheckInt("faqSeq", request);

            item.setSvcFaqType(svcSeq);

            if (faqSeq != 0) {
                item.setSvcFaqSeq(faqSeq);

                ServiceFaqVO resultItem = svcFaqService.svcFaqDetail(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    // 개인정보 마스킹 (등록자 이름)
                    resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                    // 서비스 FAQ 내용에서 html 태그 원복 (&lt;, &amp; 등으로 치환된 코드를 태그 문자로 원복) ==> response 받은 쪽에서 다시 치환 필요
                    String originSbst = StringEscapeUtils.unescapeHtml3(resultItem.getSvcFaqSbst().replaceAll("&amp;", "&"));
                    resultItem.setSvcFaqSbst(originSbst);

                    svcFaqService.updateSvcFaqRetvNum(item);
                    resultItem.setRetvNum(resultItem.getRetvNum() + 1);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setResultType("getFaqDetail");
                }
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (faqSeq == 0 && CommonFnc.checkReqParameter(request, "faqSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if (searchConfirm.equals("true")) {
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    if (searchField == "faqTitle") {
                        searchField = "SVC_FAQ_TITLE";
                    } else if (searchField == "cretrNm") {
                        searchField = "CRETR_NM";
                    }

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }

                // page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);

                int page = (currentPage - 1) * limit + 1;
                int pageEnd = page + limit - 1;

                item.setSord("desc");
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<ServiceFaqVO> resultList = svcFaqService.svcFaqList(item);
                if (resultList.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    // 개인정보 마스킹 (등록자 이름)
                    for (int i = 0; i < resultList.size(); i++) {
                        resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
                    }

                    int totalCount = svcFaqService.svcFaqListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultList);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("getFaqList");
                }
            }
        }

        return rsModel.getModelAndView();
    }
    */

    /**
     * 외부 서비스 사이트의 온라인 서비스 FAQ 상세 정보 목록 요청 API (온라인 서비스 관리 내 FAQ (GLT 런처))
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/getSvcFaqDetailAPI")
    public ModelAndView getSvcFaqDetailProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(olSvcViewName);

        request.setCharacterEncoding("UTF-8");
        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String appType = CommonFnc.emptyCheckString("appType", request);
            if (appType.equals("") || (!appType.equals("SA") && !appType.equals("MA"))) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(appType)");
                return rsModel.getModelAndView();
            }

            rsModel.setViewName(olSvcViewName);
            return getOnlineFaqDetailAPI(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 외부 서비스 사이트의 온라인 서비스 FAQ 상세 정보 목록 요청 API (서비스 매장 관리 내 FAQ (현재 미사용 중))
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/getFaqDetailAPI")
    public ModelAndView getFaqDetailProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        request.setCharacterEncoding("UTF-8");

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            // TODO : 필요 시 코드 구현 필요
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getOnlineFaqDetailAPI(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ServiceFaqVO item = new ServiceFaqVO();

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        item.setSvcFaqType(svcSeq);
        item.setTargetType(CommonFnc.emptyCheckString("targetType", request));

        /*
         * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
         */
        int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
        int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);

        int page = (currentPage - 1) * limit + 1;
        int pageEnd = page + limit - 1;

        item.setOffset(page);
        item.setLimit(pageEnd);

        List<ServiceFaqVO> resultList = svcFaqService.getOlSvcFaqDetailList(item);
        if (resultList.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            /* 개인정보 마스킹 (등록자 이름) */
            for (int i = 0; i < resultList.size(); i++) {
                resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
                // 서비스 FAQ 내용에서 html 태그 원복 (&lt;, &amp; 등으로 치환된 코드를 태그 문자로 원복)
                String originSbst = CommonFnc.unescapeStr(resultList.get(i).getSvcFaqSbst());
                resultList.get(i).setSvcFaqSbst(originSbst);
            }

            int totalCount = svcFaqService.getOlSvcFaqDetailListTotalCount(item);
            int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultList);
            rsModel.setData("totalCount", totalCount);
            rsModel.setData("row", limit);
            rsModel.setData("currentPage", currentPage);
            rsModel.setData("totalPage", totalPage);
            rsModel.setResultType("getOlSvcFaqDetailList");
        }

        return rsModel.getModelAndView();
    }


    /**
     * 외부 서비스 사이트의 서비스 FAQ 상세 정보 요청 시 조회수 업데이트를 위한 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/updateSvcFaqRetvNumAPI")
    public ModelAndView updateSvcFaqRetvNum(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(olSvcViewName);

        request.setCharacterEncoding("UTF-8");
        ServiceFaqVO item = new ServiceFaqVO();

        String method = CommonFnc.getMethod(request);
        if (!method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            return rsModel.getModelAndView();
        }

        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcFaqSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcFaqSeq = CommonFnc.emptyCheckInt("svcFaqSeq", request);
        item.setSvcFaqSeq(svcFaqSeq);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcFaqService.updateSvcFaqRetvNum(item);
            if (result > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO, String svcType) {
        String mbrSe = userVO.getMbrSe();
        if (svcType.equals("ON")) {
            return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe);
        }

        if (svcType.equals("")) {
            return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
        }

        return false;
    }

    private boolean isAllowFaq(MemberVO userVO, ServiceFaqVO resultItem, String searchType) {
        /*
         * 서비스 FAQ 구분은 서비스 번호 값으로  로그인한 사용자의 서비스 번호가 다를 경우 접근 불가
         * 서비스 FAQ 구분 값이 0인 경우는 전체서비스 항목으로 접근 가능
         */
        if (!userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_MASTER) && !userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_CMS)
            && resultItem.getSvcFaqType() != 0 && resultItem.getSvcFaqType() != userVO.getSvcSeq()) {
            return false;
        }

        /*
         * 매장관리자는 비활성화된 서비스 FAQ는 접근 불가
         * 매장관리자는 자신의 매장관라지가 작성한 서비스 FAQ가 아닌 경우 edit 불가
         */
        if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)
            && ((resultItem.getDelYn().equals("Y")) || (searchType.equals("edit") && userVO.getStorSeq() != resultItem.getCretrMbrStorSeq()))) {
            return false;
        }

        return true;
    }

    private boolean isAllowOlSvcFaq(MemberVO userVO, int svcSeq) {
        if (svcSeq == 0 || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != svcSeq)) {
            return false;
        }

        return true;
    }

}
