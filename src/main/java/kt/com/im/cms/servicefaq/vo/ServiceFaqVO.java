/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicefaq.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * ServiceFaqVO
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class ServiceFaqVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4016870716949670580L;

    /** 서비스 FAQ 번호 */
    private int svcFaqSeq;

    /** 서비스 FAQ 구분 */
    private int svcFaqType;

    /** 서비스 FAQ 구분명 */
    private String svcFaqTypeNm;

    /** 서비스 FAQ 카테고리 */
    private String svcFaqCtg;

    /** 서비스 FAQ 카테고리명 */
    private String svcFaqCtgNm;

    /** 단말 구분 - 공통코드 POST_TARGET_SE */
    private String targetType;

    /** 단말 구분 명 */
    private String targetTypeNm;

    /** 서비스 FAQ 제목 (질문) */
    private String svcFaqTitle;

    /** 서비스 FAQ 내용 (답변) */
    private String svcFaqSbst;

    /** 조회수 */
    private int retvNum;

    /** 서비스 FAQ 등록 일시 */
    private String regDt;

    /** 서비스 FAQ 등록자 이름 */
    private String cretrNm;

    /** 서비스 FAQ 삭제 여부 */
    private String delYn;

    /** 서비스 구분 - ON: 온라인, OF: 오프라인 */
    private String svcType;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** 사용자 아이디 */
    private String loginId;

    /** 사용자 권한 */
    private String mbrSe;

    /** 등록자 권한 정보 */
    private String cretrMbrSe;

    /** 등록자 서비스 번호 */
    private int cretrMbrSvcSeq;

    /** 등록자 매장 번호 */
    private int cretrMbrStorSeq;

    /** 사용자 계정의 서비스 번호 */
    private int mbrSvcSeq;

    /** 사용자 계정의 매장 번호 */
    private int mbrStorSeq;

    /** 서비스 FAQ 삭제 리스트 */
    private List<Integer> deleteSvcFaqSeqList;

    public int getSvcFaqSeq() {
        return svcFaqSeq;
    }

    public void setSvcFaqSeq(int svcFaqSeq) {
        this.svcFaqSeq = svcFaqSeq;
    }

    public int getSvcFaqType() {
        return svcFaqType;
    }

    public void setSvcFaqType(int svcFaqType) {
        this.svcFaqType = svcFaqType;
    }

    public String getSvcFaqTypeNm() {
        return svcFaqTypeNm;
    }

    public void setSvcFaqTypeNm(String svcFaqTypeNm) {
        this.svcFaqTypeNm = svcFaqTypeNm;
    }

    public String getSvcFaqCtg() {
        return svcFaqCtg;
    }

    public void setSvcFaqCtg(String svcFaqCtg) {
        this.svcFaqCtg = svcFaqCtg;
    }

    public String getSvcFaqCtgNm() {
        return svcFaqCtgNm;
    }

    public void setSvcFaqCtgNm(String svcFaqCtgNm) {
        this.svcFaqCtgNm = svcFaqCtgNm;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getTargetTypeNm() {
        return targetTypeNm;
    }

    public void setTargetTypeNm(String targetTypeNm) {
        this.targetTypeNm = targetTypeNm;
    }

    public String getSvcFaqTitle() {
        return svcFaqTitle;
    }

    public void setSvcFaqTitle(String svcFaqTitle) {
        this.svcFaqTitle = svcFaqTitle;
    }

    public String getSvcFaqSbst() {
        return svcFaqSbst;
    }

    public void setSvcFaqSbst(String svcFaqSbst) {
        this.svcFaqSbst = svcFaqSbst;
    }

    public int getRetvNum() {
        return retvNum;
    }

    public void setRetvNum(int retvNum) {
        this.retvNum = retvNum;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getSvcType() {
        return svcType;
    }

    public void setSvcType(String svcType) {
        this.svcType = svcType;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getMbrSe() {
        return mbrSe;
    }

    public void setMbrSe(String mbrSe) {
        this.mbrSe = mbrSe;
    }

    public String getCretrMbrSe() {
        return cretrMbrSe;
    }

    public void setCretrMbrSe(String cretrMbrSe) {
        this.cretrMbrSe = cretrMbrSe;
    }

    public int getCretrMbrSvcSeq() {
        return cretrMbrSvcSeq;
    }

    public void setCretrMbrSvcSeq(int cretrMbrSvcSeq) {
        this.cretrMbrSvcSeq = cretrMbrSvcSeq;
    }

    public int getCretrMbrStorSeq() {
        return cretrMbrStorSeq;
    }

    public void setCretrMbrStorSeq(int cretrMbrStorSeq) {
        this.cretrMbrStorSeq = cretrMbrStorSeq;
    }

    public int getMbrSvcSeq() {
        return mbrSvcSeq;
    }

    public void setMbrSvcSeq(int mbrSvcSeq) {
        this.mbrSvcSeq = mbrSvcSeq;
    }

    public int getMbrStorSeq() {
        return mbrStorSeq;
    }

    public void setMbrStorSeq(int mbrStorSeq) {
        this.mbrStorSeq = mbrStorSeq;
    }

    public List<Integer> getDeleteSvcFaqSeqList() {
        return deleteSvcFaqSeqList;
    }

    public void setDeleteSvcFaqSeqList(List<Integer> deleteSvcFaqSeqList) {
        this.deleteSvcFaqSeqList = deleteSvcFaqSeqList;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
