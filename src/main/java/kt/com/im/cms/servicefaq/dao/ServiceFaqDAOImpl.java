/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicefaq.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.servicefaq.vo.ServiceFaqVO;

/**
 *
 * 서비스 FAQ 관련 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("ServiceFaqDAO")
public class ServiceFaqDAOImpl extends mysqlAbstractMapper implements ServiceFaqDAO {

    /**
     * 서비스 FAQ 리스트 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 리스트
     */
    @Override
    public List<ServiceFaqVO> svcFaqList(ServiceFaqVO vo) throws Exception {
        return selectList("ServiceFaqDAO.svcFaqList", vo);
    }

    /**
     * 서비스 FAQ 리스트 합계 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 리스트 합계
     */
    @Override
    public int svcFaqListTotalCount(ServiceFaqVO vo) throws Exception {
         int res = (Integer) selectOne("ServiceFaqDAO.svcFaqListTotalCount", vo);
         return res;
    }

    /**
     * 서비스 FAQ 상세 정보 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 상세 정보
     */
    @Override
    public ServiceFaqVO svcFaqDetail(ServiceFaqVO vo) throws Exception {
        return selectOne("ServiceFaqDAO.svcFaqDetail", vo);
    }

    /**
     * 서비스 FAQ 조회수 업데이트
     * @param ServiceFaqVO
     * @return
     */
    @Override
    public int updateSvcFaqRetvNum(ServiceFaqVO vo) throws Exception {
        int res = update("ServiceFaqDAO.updateSvcFaqRetvNum", vo);
        return res;
    }

    /**
     * 서비스 FAQ 등록
     * @param ServiceFaqVO
     * @return 등록 결과
     */
    @Override
    public int insertSvcFaq(ServiceFaqVO vo) throws Exception {
        return insert("ServiceFaqDAO.insertSvcFaq", vo);
    }

    /**
     * 서비스 FAQ 수정
     * @param ServiceFaqVO
     * @return 수정 결과
     */
    @Override
    public int updateSvcFaq(ServiceFaqVO vo) throws Exception {
        return update("ServiceFaqDAO.updateSvcFaq", vo);
    }

    /**
     * 서비스 FAQ 삭제
     * @param ServiceFaqVO
     * @return 삭제 결과
     */
    @Override
    public int deleteSvcFaq(ServiceFaqVO vo) throws Exception {
        return update("ServiceFaqDAO.deleteSvcFaq", vo);
    }

    /**
     * 서비스 FAQ 상세 리스트 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 상세 리스트
     */
    @Override
    public List<ServiceFaqVO> getOlSvcFaqDetailList(ServiceFaqVO vo) throws Exception {
        return selectList("ServiceFaqDAO.selectOlSvcFaqDetailList", vo);
    }

    /**
     * 서비스 FAQ 상세 리스트 합계 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 상세 리스트 합계
     */
    @Override
    public int getOlSvcFaqDetailListTotalCount(ServiceFaqVO vo) throws Exception {
         int res = (Integer) selectOne("ServiceFaqDAO.selectOlSvcFaqDetailListTotalCount", vo);
         return res;
    }

    /**
     * 온라인 서비스 FAQ 리스트 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 온라인 서비스 FAQ 리스트
     */
    @Override
    public List<ServiceFaqVO> olSvcFaqList(ServiceFaqVO vo) throws Exception {
        return selectList("ServiceFaqDAO.selectOlSvcFaqList", vo);
    }

    /**
     * 온라인 서비스 FAQ 리스트 합계 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 온라인 서비스 FAQ 리스트 합계
     */
    @Override
    public int olSvcFaqListTotalCount(ServiceFaqVO vo) throws Exception {
         return selectOne("ServiceFaqDAO.selectOlSvcFaqListTotalCount", vo);
    }

    /**
     * 온라인 서비스 FAQ 상세 정보 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 상세 정보
     */
    @Override
    public ServiceFaqVO olSvcFaqDetail(ServiceFaqVO vo) throws Exception {
        return selectOne("ServiceFaqDAO.selectOlSvcFaqDetail", vo);
    }

    /**
     * 온라인 서비스 FAQ 등록
     * @param ServiceFaqVO
     * @return 등록 결과
     */
    @Override
    public int insertOlSvcFaq(ServiceFaqVO vo) throws Exception {
        return insert("ServiceFaqDAO.insertOlSvcFaq", vo);
    }

    /**
     * 온라인 서비스 FAQ 수정
     * @param ServiceFaqVO
     * @return 수정 결과
     */
    @Override
    public int updateOlSvcFaq(ServiceFaqVO vo) throws Exception {
        return update("ServiceFaqDAO.updateOlSvcFaq", vo);
    }

    /**
     * 온라인 서비스 FAQ 삭제 (DB에서 삭제)
     * @param ServiceFaqVO
     * @return 삭제 결과
     */
    @Override
    public int deleteOlSvcFaq(ServiceFaqVO vo) throws Exception {
        return delete("ServiceFaqDAO.deleteOlSvcFaq", vo);
    }

}
