/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicefaq.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.servicefaq.dao.ServiceFaqDAO;
import kt.com.im.cms.servicefaq.vo.ServiceFaqVO;

/**
 *
 * 서비스 FAQ 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 11. 30.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("ServiceFaqService")
public class ServiceFaqServiceImpl implements ServiceFaqService {

    @Resource(name = "ServiceFaqDAO")
    private ServiceFaqDAO svcFaqDAO;

    /**
     * 서비스 FAQ 리스트 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 리스트
     */
    @Override
    public List<ServiceFaqVO> svcFaqList(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.svcFaqList(vo);
    }

    /**
     * 서비스 FAQ 리스트 합계 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 리스트 합계
     */
    @Override
    public int svcFaqListTotalCount(ServiceFaqVO vo) throws Exception {
        int res = svcFaqDAO.svcFaqListTotalCount(vo);
        return res;
    }

    /**
     * 서비스 FAQ 상세 정보 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 상세 정보
     */
    @Override
    public ServiceFaqVO svcFaqDetail(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.svcFaqDetail(vo);
    }

    /**
     * 서비스 FAQ 조회수 업데이트
     * @param ServiceFaqVO
     * @return
     */
    @Override
    public int updateSvcFaqRetvNum(ServiceFaqVO vo) throws Exception {
        int res = svcFaqDAO.updateSvcFaqRetvNum(vo);
        return res;
    }

    /**
     * 서비스 FAQ 등록
     * @param ServiceFaqVO
     * @return 등록 결과
     */
    @Override
    public int insertSvcFaq(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.insertSvcFaq(vo);
    }

    /**
     * 서비스 FAQ 수정
     * @param ServiceFaqVO
     * @return 수정 결과
     */
    @Override
    public int updateSvcFaq(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.updateSvcFaq(vo);
    }

    /**
     * 서비스 FAQ 삭제
     * @param ServiceFaqVO
     * @return 삭제 결과
     */
    @Override
    public int deleteSvcFaq(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.deleteSvcFaq(vo);
    }

    /**
     * 서비스 FAQ 상세 리스트 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 상세 리스트
     */
    @Override
    public List<ServiceFaqVO> getOlSvcFaqDetailList(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.getOlSvcFaqDetailList(vo);
    }

    /**
     * 서비스 FAQ 상세 리스트 합계 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 상세 리스트 합계
     */
    @Override
    public int getOlSvcFaqDetailListTotalCount(ServiceFaqVO vo) throws Exception {
         int res = (Integer) svcFaqDAO.getOlSvcFaqDetailListTotalCount(vo);
         return res;
    }

    /**
     * 온라인 서비스 FAQ 리스트 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 온라인 서비스 FAQ 리스트
     */
    @Override
    public List<ServiceFaqVO> olSvcFaqList(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.olSvcFaqList(vo);
    }

    /**
     * 온라인 서비스 FAQ 리스트 합계 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 온라인 서비스 FAQ 리스트 합계
     */
    @Override
    public int olSvcFaqListTotalCount(ServiceFaqVO vo) throws Exception {
         return svcFaqDAO.olSvcFaqListTotalCount(vo);
    }

    /**
     * 온라인 서비스 FAQ 상세 정보 조회
     * @param ServiceFaqVO
     * @return 검색 조건에 부합하는 서비스 FAQ 상세 정보
     */
    @Override
    public ServiceFaqVO olSvcFaqDetail(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.olSvcFaqDetail(vo);
    }

    /**
     * 온라인 서비스 FAQ 등록
     * @param ServiceFaqVO
     * @return 등록 결과
     */
    @Override
    public int insertOlSvcFaq(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.insertOlSvcFaq(vo);
    }

    /**
     * 온라인 서비스 FAQ 수정
     * @param ServiceFaqVO
     * @return 수정 결과
     */
    @Override
    public int updateOlSvcFaq(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.updateOlSvcFaq(vo);
    }

    /**
     * 온라인 서비스 FAQ 삭제 (DB에서 삭제)
     * @param ServiceFaqVO
     * @return 삭제 결과
     */
    @Override
    public int deleteOlSvcFaq(ServiceFaqVO vo) throws Exception {
        return svcFaqDAO.deleteOlSvcFaq(vo);
    }

}
