/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.log.vo;

/**
 *
 * 로그 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2019.01.11
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 1. 11.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class LogVO {

    /** 로그 번호 */
    private int logSeq;

    /** 요청 API 명 */
    private String apiNm;

    /** 요청 파라미터 데이터의 길이 */
    private int reqSize;

    /** 요청 파라미터 값 */
    private String reqData;

    /** response 결과 메시지 값 */
    private String repMsg;

    /** 요청자(구분 : WEB,LiveOn360,Launcher) */
    private String requester;

    /** 로그 생성 일시 */
    private String cretDt;

    /** view Page에 던져줄 Data 값 */
    private String resultData;

    /** response Page 명 */
    private String viewNm;

    /** 메소드 구분 값 */
    private String method;

    /** API 구분번호 */
    private String apiNo;

    /** 결과 viewPage 경로 */
    String viewPath;

    public int getLogSeq() {
        return logSeq;
    }

    public void setLogSeq(int logSeq) {
        this.logSeq = logSeq;
    }

    public String getApiNm() {
        return apiNm;
    }

    public void setApiNm(String apiNm) {
        this.apiNm = apiNm;
    }

    public int getReqSize() {
        return reqSize;
    }

    public void setReqSize(int reqSize) {
        this.reqSize = reqSize;
    }

    public String getReqData() {
        return reqData;
    }

    public void setReqData(String reqData) {
        this.reqData = reqData;
    }

    public String getRepMsg() {
        return repMsg;
    }

    public void setRepMsg(String repMsg) {
        this.repMsg = repMsg;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }

    public String getViewNm() {
        return viewNm;
    }

    public void setViewNm(String viewNm) {
        this.viewNm = viewNm;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getApiNo() {
        return apiNo;
    }

    public void setApiNo(String apiNo) {
        this.apiNo = apiNo;
    }

    public String getViewPath() {
        return viewPath;
    }

    public void setViewPath(String viewPath) {
        this.viewPath = viewPath;
    }

}
