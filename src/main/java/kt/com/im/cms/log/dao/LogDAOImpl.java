/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.log.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.log.vo.LogVO;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2019
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 1. 11.   A2TEC      최초생성
 *      </pre>
 */

@Repository("LogDAO")
@Transactional
public class LogDAOImpl extends mysqlAbstractMapper implements LogDAO {

    /**
     * 로그 등록
     *
     * @param GenreVO
     * @return 로그 등록 성공 여부
     */
    @Override
    public int logInsert(LogVO vo) throws Exception {
        return insert("LogDAO.insertLog", vo);
    }

}
