/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.genre.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.genre.vo.GenreVO;

/**
 *
 * 회원 관리에 관한 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 30.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("GenreDAO")
public class GenreDAOImpl extends mysqlAbstractMapper implements GenreDAO {

    /**
     * 장르 목록 정보 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> getGenreList(GenreVO vo) throws Exception {
        return selectList("GenreDAO.selectGenreList", vo);
    }

    /**
     * 장르 목록 수 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public int getGenreListTotalCount(GenreVO vo) throws Exception {
        return selectOne("GenreDAO.selectGenreListTotalCount", vo);
    }

    /**
     * 장르  정보 조회
     * 
     * @param GenreVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public GenreVO getGenre(GenreVO vo) throws Exception {
        return selectOne("GenreDAO.selectGenre", vo);
    }

    /**
     * 장르  정보 추가
     * 
     * @param GenreVO
     * @return 처리 결과 
     */
    @Override
    public int insertGenre(GenreVO vo) throws Exception {
        return insert("GenreDAO.insertGenre", vo);
    }

    /**
     * 장르  정보 수정
     * 
     * @param GenreVO
     * @return 처리 결과 
     */
    @Override
    public int updateGenre(GenreVO vo) throws Exception {
        return update("GenreDAO.updateGenre", vo);
    }

    /**
     * 장르  정보 삭제
     * 
     * @param GenreVO
     * @return 처리 결과 
     */
    @Override
    public int deleteGenre(GenreVO vo) throws Exception {
        return delete("GenreDAO.deleteGenre", vo);
    }

    /**
     * 장르  중복 여부 조회
     * 
     * @param GenreVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public int getDuplicationGenre(GenreVO vo) throws Exception {
        return selectOne("GenreDAO.selectDuplicationGenre", vo);
    }

    /**
     * 카테고리 장르 목록 정보 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> getCategoryGenreList(GenreVO vo) throws Exception {
        return selectList("GenreDAO.selectCategoryGenreList", vo);
    }

    /**
     * 장르 전체 목록 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> getGenreAllList(GenreVO vo) {
        return selectList("GenreDAO.contentsGenreListAll", vo);
    }

    /**
     * 장르 노출 순서 목록 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> selectGenreDispOrderList(GenreVO vo) {
        return selectList("GenreDAO.selectGenreDispOrderList", vo);
    }

}
