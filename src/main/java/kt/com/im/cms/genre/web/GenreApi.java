/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.genre.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.content.service.ContentService;
import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.genre.service.GenreService;
import kt.com.im.cms.genre.vo.GenreVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;

/**
 *
 * 장르 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 30.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class GenreApi {

    private final static String viewName = "../resources/api/genre/genreProcess";

    @Resource(name = "GenreService")
    private GenreService genreService;

    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager txManager;

    /**
     * 장르 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contents/genre")
    public ModelAndView genreProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            boolean isList = request.getParameter("genreSeq") == null;
            if (isList) {
                return this.getGenreList(request, rsModel);
            }
            return this.getGenre(request, rsModel);
        }

        if (method.equals("POST")) {
            return this.insertGenre(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return this.updateGenre(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return this.deleteGenre(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * [GIGA_LIVE] 외부 API _ 콘텐츠 장르 조회
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/genreList")
    public ModelAndView genreAPIProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            return this.getGenreAllList(request, rsModel);
        }

        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }

        if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }

        if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getGenreAllList(HttpServletRequest request, ResultModel rsModel) {
        GenreVO vo = new GenreVO();
        String firstCtgID = CommonFnc.emptyCheckString("firstCtgID", request);
        vo.setFirstCtgId(firstCtgID);

        List<GenreVO> result = genreService.getGenreAllList(vo);

        if (result.isEmpty()) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", result);
            rsModel.setResultType("contentsGenreList");
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getGenreList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "firstCtgId:{" + Validator.MAX_LENGTH + "=1}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        GenreVO vo = new GenreVO();
        vo.setFirstCtgId(request.getParameter("firstCtgId"));

        List<GenreVO> result = genreService.getGenreList(vo);
        if (result == null || result.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("genreList");
        rsModel.setData("item", result);

        return rsModel.getModelAndView();
    }

    private ModelAndView getGenre(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "genreSeq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        GenreVO vo = new GenreVO();
        vo.setGenreSeq(Integer.parseInt(request.getParameter("genreSeq")));

        GenreVO resultItem = genreService.getGenre(vo);
        if (resultItem == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("genreInfo");
        rsModel.setData("item", resultItem);

        return rsModel.getModelAndView();
    }

    private ModelAndView insertGenre(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "firstCtgId:{" + Validator.MAX_LENGTH + "=1};"
                + "genreNm:{" + Validator.MAX_LENGTH + "=50};"
                + "genreId:{" + Validator.MAX_LENGTH + "=2}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        GenreVO vo = new GenreVO();
        vo.setFirstCtgId(request.getParameter("firstCtgId"));
        vo.setGenreNm(request.getParameter("genreNm"));
        vo.setGenreId(request.getParameter("genreId"));

        boolean alreadyExists = (genreService.getDuplicationGenre(vo) > 0);
        if (alreadyExists) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
            return rsModel.getModelAndView();
        }

        vo.setCretrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = genreService.insertGenre(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView updateGenre(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "genreSeq:{" + Validator.NUMBER + "};" + "genreNm:{" + Validator.MAX_LENGTH + "=50}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        GenreVO vo = new GenreVO();
        vo.setGenreSeq(Integer.parseInt(request.getParameter("genreSeq")));
        vo.setGenreNm(request.getParameter("genreNm"));

        // 이전 데이터 조회
        GenreVO oldGenreVO = genreService.getGenre(vo);
        if (oldGenreVO == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            return rsModel.getModelAndView();
        }

        // 데이터 변경 여부 확인
        boolean notChanged = oldGenreVO.getGenreNm().equals(vo.getGenreNm());
        if (notChanged) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            return rsModel.getModelAndView();
        }

        // 기존 코드 카테고리 내 중복 여부 확인
        vo.setFirstCtgId(oldGenreVO.getFirstCtgId());
        boolean alreadyExists = (genreService.getDuplicationGenre(vo) > 0);
        if (alreadyExists) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            return rsModel.getModelAndView();
        }

        vo.setAmdrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = genreService.updateGenre(vo);
            if (resultCode < 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView deleteGenre(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "genreSeq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        GenreVO vo = new GenreVO();
        int generSeq = Integer.parseInt(request.getParameter("genreSeq"));
        vo.setGenreSeq(generSeq);

        // 등록된 콘텐츠가 있는 경우 장르 삭제 불가
        ContentVO item = new ContentVO();
        item.setGenreSeq(generSeq);
        int totalCount = contentService.contentsListTotalCount(item);
        if (totalCount > 0) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = genreService.deleteGenre(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_DELETE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }
}
