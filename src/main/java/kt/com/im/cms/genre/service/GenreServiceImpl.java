/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.genre.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.genre.dao.GenreDAO;
import kt.com.im.cms.genre.vo.GenreVO;

/**
 *
 * 장르 관리에 관한 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 30.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("GenreService")
public class GenreServiceImpl implements GenreService {

    @Resource(name = "GenreDAO")
    private GenreDAO genreDAO;

    /**
     * 장르 목록 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> getGenreList(GenreVO vo) throws Exception {
        return genreDAO.getGenreList(vo);
    }

    /**
     * 장르 목록 수 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public int getGenreListTotalCount(GenreVO vo) throws Exception {
        return genreDAO.getGenreListTotalCount(vo);
    }

    /**
     * 장르  정보 조회
     * 
     * @param GenreVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public GenreVO getGenre(GenreVO vo) throws Exception {
        return genreDAO.getGenre(vo);
    }

    /**
     * 장르  정보 추가
     * 
     * @param GenreVO
     * @return 처리 결과 
     */
    @Override
    public int insertGenre(GenreVO vo) throws Exception {
        return genreDAO.insertGenre(vo);
    }

    /**
     * 장르  정보 수정
     * 
     * @param GenreVO
     * @return 처리 결과 
     */
    @Override
    public int updateGenre(GenreVO vo) throws Exception {
        return genreDAO.updateGenre(vo);
    }

    /**
     * 장르  정보 삭제
     * 
     * @param GenreVO
     * @return 처리 결과 
     */
    @Override
    public int deleteGenre(GenreVO vo) throws Exception {
        return genreDAO.deleteGenre(vo);
    }

    /**
     * 장르  중복 여부 조회
     * 
     * @param GenreVO
     * @return 검색조건에 해당되는 코드 리스트 정보
     */
    @Override
    public int getDuplicationGenre(GenreVO vo) throws Exception {
        return genreDAO.getDuplicationGenre(vo);
    }

    /**
     * 카테고리 장르 목록 정보 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> getCategoryGenreList(GenreVO vo) throws Exception {
        return genreDAO.getCategoryGenreList(vo);
    }

    /**
     * 장르 전체 목록 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> getGenreAllList(GenreVO vo) {
        return genreDAO.getGenreAllList(vo);
    }

    /**
     * 장르 노출 순서 목록 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> selectGenreDispOrderList(GenreVO vo) {
        return genreDAO.selectGenreDispOrderList(vo);
    }

}
