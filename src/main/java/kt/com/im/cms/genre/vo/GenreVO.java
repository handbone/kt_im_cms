/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.genre.vo;

import java.io.Serializable;

/**
 *
 * 장르 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 30.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class GenreVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -3396729819964436549L;

    /** 장르 번호 */
    int genreSeq;

    /** 첫번째 카테고리 ID */
    String firstCtgId;

    /** 장르 명 */
    String genreNm;

    /** 장르 ID */
    String genreId;

    /** 생성 일시 */
    String cretDt;

    /** 생성자 아이디 */
    String cretrId;

    /** 수정 일시 */
    String amdDt;

    /** 수정자 아이디 */
    String amdrId;

    /** 노출 순서 관리 번호 */
    int dispOrderSeq;

    /** 첫번째 카테고리 명 */
    String firstCtgNm;

    /** 노출 순서 */
    int orderNum;

    /** 사용 유무 */
    String useYn;

    public int getGenreSeq() {
        return genreSeq;
    }

    public void setGenreSeq(int genreSeq) {
        this.genreSeq = genreSeq;
    }

    public String getFirstCtgId() {
        return firstCtgId;
    }

    public void setFirstCtgId(String firstCtgId) {
        this.firstCtgId = firstCtgId;
    }

    public String getGenreNm() {
        return genreNm;
    }

    public void setGenreNm(String genreNm) {
        this.genreNm = genreNm;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getDispOrderSeq() {
        return dispOrderSeq;
    }

    public void setDispOrderSeq(int dispOrderSeq) {
        this.dispOrderSeq = dispOrderSeq;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public String getFirstCtgNm() {
        return firstCtgNm;
    }

    public void setFirstCtgNm(String firstCtgNm) {
        this.firstCtgNm = firstCtgNm;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

}
