/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicecustomer.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;

/**
 *
 * 서비스매장관리 고객센터 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 28.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Component
@Controller
public class ServiceCustomerController {

    /**
     * 서비스 매장관리 공지사항 리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/servicecustomer", method = RequestMethod.GET)
    public ModelAndView serviceCustomerList(ModelAndView mv) {
        mv.setViewName("/views/service/customer/ServiceCustomerList");
        return mv;
    }

    /**
     * 서비스 매장관리 공지사항 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/servicecustomer/notice/{svcNoticeSeq}", method = RequestMethod.GET)
    public ModelAndView serviceNoticeDetail(ModelAndView mv, @PathVariable(value="svcNoticeSeq") String svcNoticeSeq) {
        mv.addObject("svcNoticeSeq", svcNoticeSeq);
        mv.setViewName("/views/service/customer/ServiceCustomerNoticeDetail");
        return mv;
    }

    /**
     * 서비스 고객센터 공지사항 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/servicecustomer/notice/regist", method = RequestMethod.GET)
    public ModelAndView serviceNoticeRegist(ModelAndView mv) {
        mv.setViewName("/views/service/customer/ServiceCustomerNoticeRegist");
        return mv;
    }

    /**
     * 서비스 매장관리 공지사항 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/servicecustomer/notice/{svcNoticeSeq}/edit", method = RequestMethod.GET)
    public ModelAndView serviceNoticeEdit(ModelAndView mv, @PathVariable(value="svcNoticeSeq") String svcNoticeSeq) {
        mv.addObject("svcNoticeSeq", svcNoticeSeq);
        mv.setViewName("/views/service/customer/ServiceCustomerNoticeEdit");
        return mv;
    }

    /**
     * 서비스 매장관리 FAQ 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/servicecustomer/faq/{svcFaqSeq}", method = RequestMethod.GET)
    public ModelAndView serviceFaqDetail(ModelAndView mv, @PathVariable(value="svcFaqSeq") String svcFaqSeq) {
        mv.addObject("svcFaqSeq", svcFaqSeq);
        mv.setViewName("/views/service/customer/ServiceCustomerFaqDetail");
        return mv;
    }

    /**
     * 서비스 고객센터 FAQ 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/servicecustomer/faq/regist", method = RequestMethod.GET)
    public ModelAndView serviceFaqRegist(ModelAndView mv) {
        mv.setViewName("/views/service/customer/ServiceCustomerFaqRegist");
        return mv;
    }

    /**
     * 서비스 매장관리 FAQ 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/servicecustomer/faq/{svcFaqSeq}/edit", method = RequestMethod.GET)
    public ModelAndView serviceFaqEdit(ModelAndView mv, @PathVariable(value="svcFaqSeq") String svcFaqSeq) {
        mv.addObject("svcFaqSeq", svcFaqSeq);
        mv.setViewName("/views/service/customer/ServiceCustomerFaqEdit");
        return mv;
    }

    /**
     * 온라인 서비스 고객센터 내 공지사항/FAQ 목록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/onlineSvc/customer", method = RequestMethod.GET)
    public ModelAndView olSvcCustomer(ModelAndView mv, HttpServletRequest request) {
        mv.setViewName("/views/onlineService/customer/OlSvcCustomerList");
        return mv;
    }

    /**
     * 온라인 서비스 고객센터 내 공지사항 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/onlineSvc/customer/notice/{svcNoticeSeq}", method = RequestMethod.GET)
    public ModelAndView olSvcNoticeDetail(ModelAndView mv, HttpServletRequest request, @PathVariable(value="svcNoticeSeq") String svcNoticeSeq) {
        mv.addObject("svcNoticeSeq", svcNoticeSeq);
        mv.addObject("currSvcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.setViewName("/views/onlineService/customer/OlSvcCustomerNoticeDetail");
        return mv;
    }

    /**
     * 온라인 서비스 고객센터 내 공지사항 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/onlineSvc/customer/notice/regist", method = RequestMethod.GET)
    public ModelAndView olSvcNoticeRegist(ModelAndView mv, HttpServletRequest request) {
        mv.addObject("currSvcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.setViewName("/views/onlineService/customer/OlSvcCustomerNoticeRegist");
        return mv;
    }

    /**
     * 온라인 서비스 공지사항 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/onlineSvc/customer/notice/{svcNoticeSeq}/edit", method = RequestMethod.GET)
    public ModelAndView olSvcNoticeEdit(ModelAndView mv, HttpServletRequest request, @PathVariable(value="svcNoticeSeq") String svcNoticeSeq) {
        mv.addObject("svcNoticeSeq", svcNoticeSeq);
        mv.addObject("currSvcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.setViewName("/views/onlineService/customer/OlSvcCustomerNoticeEdit");
        return mv;
    }

    /**
     * 온라인 서비스 FAQ 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/onlineSvc/customer/faq/{svcFaqSeq}", method = RequestMethod.GET)
    public ModelAndView olSvcFaqDetail(ModelAndView mv, HttpServletRequest request, @PathVariable(value="svcFaqSeq") String svcFaqSeq) {
        mv.addObject("svcFaqSeq", svcFaqSeq);
        mv.addObject("currSvcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.setViewName("/views/onlineService/customer/OlSvcCustomerFaqDetail");
        return mv;
    }
    
    /**
     * 온라인 서비스 고객센터 내 FAQ 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/onlineSvc/customer/faq/regist", method = RequestMethod.GET)
    public ModelAndView olSvcFaqRegist(ModelAndView mv, HttpServletRequest request) {
        mv.addObject("currSvcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.setViewName("/views/onlineService/customer/OlSvcCustomerFaqRegist");
        return mv;
    }

    /**
     * 온라인 서비스 FAQ 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/onlineSvc/customer/faq/{svcFaqSeq}/edit", method = RequestMethod.GET)
    public ModelAndView olSvcFaqEdit(ModelAndView mv, HttpServletRequest request, @PathVariable(value="svcFaqSeq") String svcFaqSeq) {
        mv.addObject("svcFaqSeq", svcFaqSeq);
        mv.addObject("currSvcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.setViewName("/views/onlineService/customer/OlSvcCustomerFaqEdit");
        return mv;
    }

}
