/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.file.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.file.vo.FileVO;

/**
 *
 * 파일 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 9.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("FileDAO")
public class FileDAOImpl extends mysqlAbstractMapper implements FileDAO {

    /**
     * 파일 리스트 조회
     *
     * @param FileVO
     * @return 검색 조건에 부합하는 파일 리스트
     */
    @Override
    public List<FileVO> fileList(FileVO vo) {
        return selectList("FileDAO.fileList", vo);
    }

    /**
     * 파일 신규 등록
     *
     * @param FileVO
     * @return 파일 신규 등록 성공 여부
     */
    @Override
    public int fileInsert(FileVO data) {
        int res = Integer.valueOf(insert("FileDAO.fileInsert", data));
        return res;
    }

    /**
     * 파일 삭제
     *
     * @param FileVO
     * @return 파일 삭제 성공 여부
     */
    @Override
    public int fileDelete(FileVO data) {
        int res;
        res = delete("FileDAO.fileDelete", data);
        return res;
    }

    /**
     * contentsSeq, 파일 Type으로 파일 정보 찾기
     *
     * @param FileVO
     * @return 파일 정보
     */
    @Override
    public FileVO fileInfo(FileVO data) {
        return selectOne("FileDAO.fileInfo", data);
    }

    /**
     * fileSeq, 파일 Type으로 파일 정보 찾기
     *
     * @param FileVO
     * @return 파일 정보
     */
    @Override
    public FileVO fileSeqInfo(FileVO data) {
        return selectOne("FileDAO.fileSeqInfo", data);
    }

    /**
     * fileSeq로 파일 찾기
     *
     * @param FileVO
     * @return 파일 정보
     */
    public FileVO medatadataInfo(FileVO data) {
        FileVO item = (FileVO) selectOne("FileDAO.medatadataInfo", data);
        return item;
    }

    /**
     * 파일 Path 정보로 파일 찾기
     *
     * @param FileVO
     * @return 파일 정보
     */
    public FileVO filePahtInfo(FileVO data) {
        FileVO item = (FileVO) selectOne("FileDAO.filePahtInfo", data);
        return item;
    }

    /**
     * 비디오 메타데이터 정보 쌍 신규 등록
     *
     * @param FileVO
     * @return 등록 성공 여부
     */
    @Override
    public int fileMatch(FileVO data) {
        int res = Integer.valueOf(insert("FileDAO.fileMatch", data));
        return res;
    }

    /**
     * contentsSeq 로 비디오/메타데이터 파일 정보 쌍 삭제
     *
     * @param FileVO
     * @return 삭제 성공 여부
     */
    @Override
    public int fileMatchDelete(FileVO data) {
        int res;
        res = delete("FileDAO.fileMatchDelete", data);
        return res;
    }

    /**
     * 컨텐츠 메타데이터 리스트
     *
     * @param FileVO
     * @return
     */
    @Override
    public List<FileVO> metadataDeletList(FileVO data) {
        return selectList("FileDAO.metadataDeletList", data);
    }

    /**
     * fileSeq로 파일 삭제
     *
     * @param FileVO
     * @return 삭제 성공 여부
     */
    @Override
    public int fileSeqDelete(FileVO data) {
        int res;
        res = delete("FileDAO.fileSeqDelete", data);
        return res;
    }

    /**
     * file Path로 파일 삭제
     *
     * @param FileVO
     * @return 삭제 성공 여부
     */
    public int filePathDelete(FileVO data) {
        int res;
        res = delete("FileDAO.filePathDelete", data);
        return res;
    }

    /**
     * contentsSeq, fileType으로 파일 정보 조회
     *
     * @param FileVO
     * @return 파일 정보
     */
    public List<FileVO> fileLists(FileVO data) {
        return selectList("FileDAO.filesList", data);
    }

    /**
     * 파일 다운로드 (정보 조회)
     *
     * @param FileVO
     * @return 파일 정보 조회
     */
    @Override
    public FileVO MedatadataInfo(FileVO data) {
        FileVO item = (FileVO) selectOne("FileDAO.medatadataInfo", data);
        return item;
    }

    /**
     * 파일 총 용량 조회
     *
     * @param FileVO
     * @return 파일 정보 조회
     */
    @Override
    public FileVO getTotalFileSize(FileVO data) {
        FileVO item = (FileVO) selectOne("FileDAO.selectTotalFileSize", data);
        return item;
    }

    /**
     * contentsSeq, fileSe 정보로 파일의 상세 정보 조회
     *
     * @param FileVO
     * @return 파일 상세 정보
     */
    @Override
    public FileVO fileDetailInfo(FileVO data) {
        return selectOne("FileDAO.fileDetailInfo", data);
    }

}
