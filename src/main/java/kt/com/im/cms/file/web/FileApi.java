/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.file.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CompressionUtil;
import kt.com.im.cms.common.util.FormatCheckUtil;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;

@Controller
public class FileApi {

    @Resource(name = "FileService")
    private FileService fileService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    public void matrixTime(int delayTime) {
        long saveTime = System.currentTimeMillis();
        long currTime = 0;
        while (currTime - saveTime < delayTime) {
            currTime = System.currentTimeMillis();
        }
    }

    /**
     * Build Facility Process
     */
    @Inject
    private FileSystemResource fsResource;

    @RequestMapping(value = "/api/file")
    public ModelAndView videoProcess(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fileupload") MultipartFile file, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        String tempId = CommonFnc.emptyCheckString("id", request);
        String otpCreateTime = CommonFnc.emptyCheckString("otpCreateTime", request);
        boolean clearSession = false;
        if (session.getAttribute("id") == null && tempId != "" && otpCreateTime != "") {
            userVO = new MemberVO();
            userVO.setMbrId(tempId);
            userVO.setOtpCreateTime(Long.parseLong(otpCreateTime));

            userVO = memberService.getExistOtpCreateTime(userVO);

            if (userVO != null) {
                clearSession = true;
            }
        }

        if (session.getAttribute("id") == null && !clearSession) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/file/fileProcess");
            FileVO item = new FileVO();
            if (request.getMethod().equals("POST")) {
                if (request.getParameter("_method") == "PUT") { // put
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else if (request.getParameter("_method") == "DELETE") { // delete
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else { // post
                    if (!file.isEmpty()) {
                        String checkString = CommonFnc.requiredChecking(request, "imgMode");
                        if (!checkString.equals("")) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                            return rsModel.getModelAndView();
                        }

                        String imgMode = request.getParameter("imgMode");
                        String contsSeq = request.getParameter("contsSeq") == null ? ""
                                : request.getParameter("contsSeq");
                        String filename = file.getOriginalFilename();
                        filename = new String(filename.getBytes("UTF-8"), "UTF-8");
                        if (!isValidFileName(filename)) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                            return rsModel.getModelAndView();
                        }

                        String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
                        String filePath = request.getParameter("filePath") == null ? ""
                                : request.getParameter("filePath");
                        String dir = "";
                        String root = "";
                        matrixTime(100);
                        long time;

                        if (filePath == "") {
                            time = System.currentTimeMillis();
                            // Creating the directory to store file
                            if (imgMode.equals("notice") || imgMode.equals("svcNotice") || imgMode.equals("version")) {
                                dir = "/" + imgMode + "/" + contsSeq + "/" + time + "." + ext;
                                root = fsResource.getPath() + "/" + imgMode + "/" + contsSeq + "/" + time + "." + ext;
                            } else if (imgMode.equals("qnaAns")) {
                                dir = "/" + "qna" + "/" + contsSeq + "/" + time + "." + ext;
                                root = fsResource.getPath() + "/" + "qna" + "/" + contsSeq + "/" + time + "." + ext;
                            } else if (contsSeq.equals("")) {
                                dir = "/" + imgMode + "/" + time + "." + ext;
                                root = fsResource.getPath() + "/" + imgMode + "/" + time + "." + ext;
                            } else {
                                dir = "/" + contsSeq + "/" + imgMode + "/" + time + "." + ext;
                                root = fsResource.getPath() + "/" + contsSeq + "/" + imgMode + "/" + time + "." + ext;
                            }
                            item.setStreFileNm(time + "." + ext);
                        } else {
                            dir = filePath;
                            root = fsResource.getPath() + filePath;
                            String pathTemp = filePath.split("/")[2];
                            item.setStreFileNm(pathTemp);
                        }

                        if (!isValidFilePath(root)) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                            return rsModel.getModelAndView();
                        }

                        File lOutFile = new File(root);
                        if (!lOutFile.exists())
                            lOutFile.mkdirs();

                        file.transferTo(lOutFile);

                        item.setOrginlFileNm(filename);
                        item.setFileDir(dir);
                        item.setFileExt(ext);
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setResultType("fileUpload");
                        rsModel.setData("item", item);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                    }

                }
            } else { // get
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        }
        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/fileProcess")
    public ModelAndView fileProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        if (session.getAttribute("id") == null) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/file/fileProcess");
            FileVO item = new FileVO();
            item.setCretrId((String) session.getAttribute("id"));
            if (request.getMethod().equals("POST")) {

                if (request.getParameter("_method") == "PUT") { // put
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else if (request.getParameter("_method") == "DELETE") { // delete
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else { // post
                    String checkString = CommonFnc.requiredChecking(request,
                            "contsSeq,fileExt,fileDir,orginlFileNm,fileSize,streFileNm");
                    if (!checkString.equals("")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                        rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }

                    item.setFileContsSeq(Integer.parseInt(request.getParameter("contsSeq")));
                    String ext = request.getParameter("fileExt");
                    String fileSe = request.getParameter("fileSe") == null ? "" : request.getParameter("fileSe");
                    if (fileSe.equals("notice")) {
                        item.setFileSe("NOTICE");
                    } else if (fileSe.equals("svcNotice")) {
                        item.setFileSe("SVC_NOTI");
                    } else if (fileSe.equals("qnaAns")) {
                        item.setFileSe("QNA_ANS");
                    } else if (fileSe.equals("version")) {
                        item.setFileSe("VERSION");
                    } else {
                        if (ext.toUpperCase().equalsIgnoreCase("JPG") || ext.toUpperCase().equalsIgnoreCase("JPEG")
                                || ext.toUpperCase().equalsIgnoreCase("GIF")
                                || ext.toUpperCase().equalsIgnoreCase("PNG")) { // upload

                            if (fileSe.equals("img")) {
                                item.setFileSe("IMAGE");
                            } else {
                                item.setFileSe("COVERIMG");
                                FileVO resData = fileService.fileInfo(item);
                                String filePath = "";
                                if (resData != null) {
                                    filePath = resData.getFilePath();
                                }
                                if (fileService.fileDelete(item) == 1) {
                                    File file = new File(fsResource.getPath() + filePath);
                                    file.delete();
                                }
                            }
                        } else if (ext.toUpperCase().equalsIgnoreCase("XML")) {
                            if (fileSe.equals("prevmeta")) {
                                item.setFileSe("PREVMETA");
                            } else if (fileSe.equals("contents")) {
                                item.setFileSe("CONTENTS");
                            } else {
                                item.setFileSe("METADATA");
                            }
                            FileVO resData = fileService.fileInfo(item);
                            String filePath = "";
                            if (resData != null) {
                                filePath = resData.getFilePath();
                            }
                            if (fileService.fileDelete(item) == 1) {
                                File file = new File(fsResource.getPath() + filePath);
                                file.delete();
                            }

                            /*
                             * if(fileService.filePathDelete(item) == 1){
                             * File f = new File(fsResource.getPath()+filePath);
                             * f.delete();
                             * }
                             * item.setfileDir(request.getParameter("fileDir"));
                             * if(fileService.filePathDelete(item) == 1){
                             * File f = new File(fsResource.getPath()+request.getParameter("fileDir"));
                             * f.delete();
                             * }
                             */
                            item.setFileSe("METADATA");
                        } else if (ext.toUpperCase().equalsIgnoreCase("MP4")) {
                            if (fileSe.equals("prev")) {
                                item.setFileSe("PREV");
                            } else {
                                item.setFileSe("VIDEO");
                            }

                            FileVO resData = fileService.fileInfo(item);
                            String filePath = "";
                            if (resData != null) {
                                filePath = resData.getFilePath();
                            }
                            if (fileService.fileDelete(item) == 1) {
                                File file = new File(fsResource.getPath() + filePath);
                                file.delete();
                            }
                        } else {
                            item.setFileSe("FILE");
                            /*
                             * FileVO resData = fileService.fileInfo(item);
                             * String filePath = "";
                             * if(resData != null){
                             * filePath = resData.getfilePath();
                             * }
                             * if(fileService.fileDelete(item) == 1){
                             * File f = new File(fsResource.getPath()+filePath);
                             * f.delete();
                             * }
                             */
                        }
                    }
                    item.setOrginlFileNm(request.getParameter("orginlFileNm"));
                    item.setFileDir(request.getParameter("fileDir"));
                    String fileSize = request.getParameter("fileSize");
                    item.setFileSize(BigInteger.valueOf(Long.parseLong(fileSize)));
                    item.setFileExt(ext);
                    item.setStreFileNm(request.getParameter("streFileNm"));
                    if (fileService.fileInsert(item) == 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    }
                }
            } else { // get
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        }
        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/contentsXml")
    public ModelAndView contentsXmlCreate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcess");

        String checkString = CommonFnc.requiredChecking(request, "contsSeq");
        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        FileVO item = new FileVO();
        String contsSeq = request.getParameter("contsSeq") == null ? "" : request.getParameter("contsSeq");
        String contentsId = request.getParameter("contsID") == null ? "" : request.getParameter("contsID");
        String arrayParams = request.getParameter("videoFormData") == null ? "" : request.getParameter("videoFormData");
        String[] videoResult = arrayParams.split("GNB");

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("contents");
        doc.appendChild(rootElement);

        item.setFileContsSeq(Integer.parseInt(contsSeq));
        fileService.fileMatchDelete(item);

        if (getActualSize(videoResult) == 0) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            return rsModel.getModelAndView();
        }

        for (int i = 0; i < videoResult.length; i++) {
            String[] data = videoResult[i].split(",");
            // staff elements
            Element staff = doc.createElement("Content");

            rootElement.appendChild(staff);
            // set attribute to staff element
            Attr attr1 = doc.createAttribute("media");
            attr1.setValue(data[0].replace("/", "\\"));
            staff.setAttributeNode(attr1);
            item.setvideoPath(data[0]);
            try {
                if (!data[1].equals("")) {
                    Attr attr2 = doc.createAttribute("metadata");
                    attr2.setValue(data[1].replace("/", "\\"));
                    staff.setAttributeNode(attr2);

                    item.setMetadatas(data[1]);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                item.setMetadataPath("");
            }
            fileService.fileMatch(item);

        }

        item.setFileSe("CONTENTS");
        FileVO resData = fileService.fileInfo(item);
        String filePath = "";
        if (resData != null) {
            filePath = resData.getFilePath();
        }
        /*
         * if(fileDaoImpl.fileDelete(item) == 1){
         * File f = new File(fsResource.getPath()+filePath);
         * f.delete();
         * }
         */

        List<FileVO> metadataDeletList = fileService.metadataDeletList(item);
        for (FileVO itemList : metadataDeletList) {
            FileVO metaItem = new FileVO();
            String filePath1 = itemList.getFilePath();
            metaItem.setFileSeq(itemList.getFileSeq());
            if (fileService.fileSeqDelete(metaItem) == 1) {
                File file = new File(fsResource.getPath() + filePath1);
                file.delete();
            }
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        long time = System.currentTimeMillis();
        // Creating the directory to store file
        // Creating the directory to store file
        String dir = "";
        String root = "";
        if (resData == null) {
            dir = "/" + contsSeq + "/contents/" + time + ".xml";
            root = fsResource.getPath() + "/" + contsSeq + "/contents/" + time + ".xml";
        } else {
            dir = filePath;
            root = fsResource.getPath() + filePath;
        }
        File lOutFile = new File(fsResource.getPath() + "/" + contsSeq + "/contents");
        if (!lOutFile.exists()) {
            lOutFile.mkdirs();
        }
        StreamResult resultXml = new StreamResult(root);
        // Output to console for testing
        // StreamResult result = new StreamResult(System.out);
        transformer.transform(source, resultXml);

        if (resData == null) {
            item.setOrginlFileNm("Contents.xml");
            item.setFileDir(dir);
            item.setFileExt("xml");
            item.setStreFileNm(time + ".xml");
            if (fileService.fileInsert(item) == 1) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            }
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
        }
        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/package/{contsSeq}")
    public void packagedownload(HttpServletResponse response, @PathVariable(value = "contsSeq") String contsSeq)
            throws Exception {
        CompressionUtil compressutil = new CompressionUtil();
        FileVO item = new FileVO();
        item.setFileContsSeq(Integer.parseInt(contsSeq));
        List<FileVO> fileList = fileService.fileList(item);
        List<File> ziplist = new ArrayList<File>();
        List<String> nameList = new ArrayList<String>();
        String contentsName = "";
        for (FileVO fileInfo : fileList) {
            ziplist.add(new File(fsResource.getPath() + fileInfo.getFilePath()));
            nameList.add(fileInfo.getOrginlFileNm());
            contentsName = fileInfo.getTitle();
        }
        // Setting Resqponse Header
        File zippedFile = new File(fsResource.getPath() + "/zip/" + contentsName + "_xml.zip");
        compressutil.zip(ziplist, new FileOutputStream(zippedFile), nameList);

        response.setContentType("application/x-msdownload");

        String fileName = zippedFile.getName().toString(); // 리퀘스트로 넘어온 파일명
        String docName = URLEncoder.encode(fileName, "UTF-8"); // UTF-8로 인코딩

        response.setHeader("Content-Disposition", "attachment;filename=" + docName.replace("+", " ") + ";");

        int nRead = 0;
        byte btReadByte[] = new byte[(int) zippedFile.length()];

        if (zippedFile.length() > 0 && zippedFile.isFile()) {
            BufferedInputStream objBIS = new BufferedInputStream(new FileInputStream(zippedFile));
            BufferedOutputStream objBOS = new BufferedOutputStream(response.getOutputStream());

            while ((nRead = objBIS.read(btReadByte)) != -1) {
                objBOS.write(btReadByte, 0, nRead);
            }

            objBOS.flush();
            objBOS.close();
            objBIS.close();
            return;
        }

        zippedFile.delete();
    }

    /* String 배열 길이 */
    public static int getActualSize(String[] items) {
        int size = 0;
        for (int i = 0; i < items.length; i++) {

            if (items[i] != null) {
                size = size + 1;
            }
        }
        return size;

    }

    @RequestMapping(value = "/api/fileDownload/{fileSeq}")
    public void fileDownload(HttpServletRequest request, HttpServletResponse response,
            @PathVariable(value = "fileSeq") String fileSeq) throws Exception {
        FileVO item = new FileVO();
        if (!FormatCheckUtil.checkNumber(fileSeq)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        item.setFileSeq(Integer.parseInt(fileSeq));
        item.setFilePath("");
        FileVO metadataRes = fileService.MedatadataInfo(item);
        if (metadataRes == null) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }

        String filePath = fsResource.getPath() + metadataRes.getFilePath();
        if (!isValidFilePath(filePath)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        File downfile = new File(filePath);
        long L = downfile.length();
        if (!downfile.exists()) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }
        ServletOutputStream outStream = null;
        FileInputStream inputStream = null;
        try {
            outStream = response.getOutputStream();
            inputStream = new FileInputStream(downfile);

            String downName = null;
            String browser = request.getHeader("User-Agent");

            // 파일 인코딩
            if (browser.contains("MSIE") || browser.contains("Trident")) {
                downName = URLEncoder.encode(metadataRes.getOrginlFileNm(), "UTF-8").replaceAll("\\+", "%20");
            } else if (browser.contains("Chrome")) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < metadataRes.getOrginlFileNm().length(); i++) {
                    char c = metadataRes.getOrginlFileNm().charAt(i);
                    if (c > '~') {
                        sb.append(URLEncoder.encode("" + c, "UTF-8"));
                    } else {
                        sb.append(c);
                    }
                }
                downName = sb.toString();

            } else {
                downName = new String(metadataRes.getOrginlFileNm().getBytes("UTF-8"), "ISO-8859-1");
            }

            // Setting Resqponse Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            // byte[] outByte = new byte[(int) L];
            // while (inputStream.read(outByte, 0, (int) L) != -1) {
            // outStream.write(outByte, 0, (int) L);
            // }
            byte[] buf = new byte[4096];
            int bytesread = 0, bytesBuffered = 0;
            System.out.println("[/api/fileDownload][filePath]:" + filePath + "  [fileSeq]:" + fileSeq + " (START)");
            while ((bytesread = inputStream.read(buf)) > -1) {
                outStream.write(buf, 0, bytesread);
                bytesBuffered += bytesread;
                if (bytesBuffered > 1024 * 1024) { // flush after 1MB
                    bytesBuffered = 0;
                    outStream.flush();
                }
            }
            System.out.println("[/api/fileDownload][filePath]:" + filePath + "  [fileSeq]:" + fileSeq + " (END)");
        } catch (Exception e) {
            System.out.println("[_______FILE ERROR LOG_____API : fileDownload][SEQ:"+fileSeq+"]");
            throw new IOException();
        } finally {
            inputStream.close();
            outStream.flush();
            outStream.close();
        }
        return;
    }

    @RequestMapping(value = "/api/imgUrl")
    public void fileDownload(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // ResultModel rsModel = new ResultModel(response);
        FileVO item = new FileVO();

        String filePath = request.getParameter("filePath");
        String oriName = CommonFnc.emptyCheckString("oriName", request);

        String widthStr = CommonFnc.emptyCheckString("width", request);
        String heightStr = CommonFnc.emptyCheckString("height", request);

        if (oriName != null && !oriName.isEmpty()) {
            if (!isValidFileName(oriName)) {
                response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return;
            }
        }

        if (!widthStr.equals("") && !heightStr.equals("")) {
            String[] pathStrArr = filePath.split("/");
            String fName = pathStrArr[pathStrArr.length -1];
            String name = fName.substring(0, fName.lastIndexOf("."));
            String ext = fName.substring(fName.lastIndexOf(".") + 1, fName.length());

            String filePathWithSize = "";
            for (int i = 0; i < pathStrArr.length - 1; i++) { // 마지막 인자는 파일 명으로 제외
                if (i == 0) {
                    filePathWithSize += "/";
                }

                if (!pathStrArr[i].equals("")) {
                    filePathWithSize += pathStrArr[i] + "/";
                }
            }
            if (!filePathWithSize.equals("")) {
                filePathWithSize += name + "_" + widthStr + "x" + heightStr + "." + ext;
            }
            item.setFilePath(filePathWithSize);
        } else {
            item.setFilePath(filePath);
        }

        FileVO metadataRes = fileService.MedatadataInfo(item);

        if (metadataRes == null) {
            // 썸네일 이미지가 존재하지 않을 경우 원본 이미지 가져옴.
            if (!widthStr.equals("") && !heightStr.equals("")) {
                item.setFilePath(filePath);
                metadataRes = fileService.MedatadataInfo(item);

                if (metadataRes == null) {
                    metadataRes = new FileVO();
                    metadataRes.setFilePath(filePath);
                    metadataRes.setOrginlFileNm(oriName);
                }
            } else {
                metadataRes = new FileVO();
                metadataRes.setFilePath(filePath);
                metadataRes.setOrginlFileNm(oriName);
            }
        }

        String fullFilePath = fsResource.getPath() + metadataRes.getFilePath();
        if (!isValidFilePath(fullFilePath)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        File downfile = new File(fullFilePath);
        long L = downfile.length();
        if (!downfile.exists()) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }
        ServletOutputStream outStream = null;
        FileInputStream inputStream = null;
        try {
            outStream = response.getOutputStream();
            inputStream = new FileInputStream(downfile);

            String downName = null;
            String browser = request.getHeader("User-Agent");

            // 파일 인코딩
            if (browser.contains("MSIE") || browser.contains("Trident")) {
                downName = URLEncoder.encode(metadataRes.getOrginlFileNm(), "UTF-8").replaceAll("\\+", "%20");
            } else if (browser.contains("Chrome")) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < metadataRes.getOrginlFileNm().length(); i++) {
                    char c = metadataRes.getOrginlFileNm().charAt(i);
                    if (c > '~') {
                        sb.append(URLEncoder.encode("" + c, "UTF-8"));
                    } else {
                        sb.append(c);
                    }
                }
                downName = sb.toString();

            } else {
                downName = new String(metadataRes.getOrginlFileNm().getBytes("UTF-8"), "ISO-8859-1");
            }

            // Setting Resqponse Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            byte[] outByte = new byte[(int) L];
            while (inputStream.read(outByte, 0, (int) L) != -1) {
                outStream.write(outByte, 0, (int) L);
            }
        } catch (Exception e) {
            throw new IOException();
        } finally {
            inputStream.close();
            outStream.flush();
            outStream.close();
        }
        return;
    }

    @RequestMapping(value = "/api/fileCK")
    public ModelAndView ckProcess(HttpServletRequest request, HttpServletResponse response, FileVO item,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcessCK");
        MultipartFile file = item.getUpload();

        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        int CKEditorFuncNum = 0;

        if (item.getCKEditorFuncNum() != "" && item.getCKEditorFuncNum() != null) {
            CKEditorFuncNum = Integer.parseInt(item.getCKEditorFuncNum());
        }

        if (request.getMethod().equals("POST")) {
            if (!file.isEmpty()) {
                String filename = file.getOriginalFilename();
                filename = new String(filename.getBytes("ISO-8859-1"), "utf-8");
                if (!isValidFileName(filename)) {
                    rsModel.setData("errorMsg", "Not allowed file name.");
                    rsModel.setResultType("fileUploadCKFail");
                    return rsModel.getModelAndView();
                }

                String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());

                if (!isAllowFileType(ext)) {
                    rsModel.setData("errorMsg", "Not allowed file type.");
                    rsModel.setResultType("fileUploadCKFail");
                    return rsModel.getModelAndView();
                }

                String dir = "";
                String root = "";
                String filePath = "";

                matrixTime(100);
                long time;
                if (filePath == "") {
                    time = System.currentTimeMillis();
                    // Creating the directory to store file
                    dir = "/editor/" + time + "." + ext;
                    root = fsResource.getPath() + "/editor" + "/" + time + "." + ext;
                    item.setStreFileNm(time + "." + ext);
                }

                if (!isValidFilePath(root)) {
                    rsModel.setData("errorMsg", "Not allowed file.");
                    rsModel.setResultType("fileUploadCKFail");
                    return rsModel.getModelAndView();
                }

                File lOutFile = new File(root);
                if (!lOutFile.exists())
                    lOutFile.mkdirs();

                file.transferTo(lOutFile);

                item.setOrginlFileNm(filename);
                item.setFileDir(dir);
                item.setFileExt(ext);

                // 저장된 cms url 경로로 ckeditor로 전달
                rsModel.setData("filename", filename);
                rsModel.setData("file_path", dir);
                rsModel.setResultType("fileUploadCKSuccess");
            } else {
                rsModel.setData("errorMsg", "File is empty!");
                rsModel.setResultType("fileUploadCKFail");
            }
        }

        return rsModel.getModelAndView();
    }

    protected boolean isAllowFileType(String fileType) {
        boolean isAllow = false;
        String[] allowFileTypeArr = { "jpg", "jpeg", "gif", "bmp", "png" };

        for (String allowFileType : allowFileTypeArr) {
            if (allowFileType.equals(fileType.toLowerCase())) {
                isAllow = true;
                break;
            }
        }

        return isAllow;
    }

    static public boolean isAllowUploadFileType(String fileType) {
        boolean isAllow = false;
        String[] allowFileTypeArr = { "pdf", "hwp", "txt", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "png", "jpg", "jpeg", "bmp", "gif", "mp4", "xml", "zip", "exe" };

        for (String allowFileType : allowFileTypeArr) {
            if (allowFileType.equalsIgnoreCase(fileType)) {
                isAllow = true;
                break;
            }
        }

        return isAllow;
    }

    static public boolean isValidFileName(String fileName) {
        if (fileName == null || fileName.isEmpty() || fileName.length() > 50) {
            return false;
        }

        if ((fileName.indexOf("\0")) != -1 || (fileName.indexOf(";")) != -1 || (fileName.indexOf("./")) != -1 || (fileName.indexOf(".\\")) != -1) {
            return false;
        }

        String fileExt = fileName.substring(fileName.lastIndexOf('.' ) + 1);
        if (fileExt.equals("")) {
            return false;
        }

        if (!isAllowUploadFileType(fileExt)) {
            return false;
        }

        return true;
    }

    static public boolean isValidFilePath(String filePath) {
        if (filePath == null || filePath.isEmpty()) {
            return false;
        }

        if ((filePath.indexOf("\0")) != -1 || (filePath.indexOf(";")) != -1 || (filePath.indexOf("..")) != -1
                || (filePath.indexOf("./")) != -1 || (filePath.indexOf(".\\")) != -1 || (filePath.indexOf(":")) != -1) {
            return false;
        }

        String fileExt = filePath.substring(filePath.lastIndexOf('.' ) + 1);
        if (fileExt.equals("")) {
            return false;
        }

        if (!isAllowUploadFileType(fileExt)) {
            return false;
        }

        return true;
    }
}
