/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.file.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.file.dao.FileDAO;
import kt.com.im.cms.file.vo.FileVO;

/**
 *
 * 파일 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 9.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("FileService")
public class FileServiceImpl implements FileService {

    @Resource(name = "FileDAO")
    private FileDAO FileDAO;

    /**
     * 파일 리스트 조회
     *
     * @param FileVO
     * @return 검색 조건에 부합하는 파일 리스트
     */
    @Override
    public List<FileVO> fileList(FileVO vo) {
        return FileDAO.fileList(vo);
    }

    @Override
    public FileVO fileInfo(FileVO item) {
        return FileDAO.fileInfo(item);
    }

    @Override
    public int fileDelete(FileVO item) {
        return FileDAO.fileDelete(item);
    }

    @Override
    public int fileInsert(FileVO item) {
        return FileDAO.fileInsert(item);
    }

    @Override
    public int fileMatch(FileVO item) {
        return FileDAO.fileMatch(item);
    }

    @Override
    public int fileMatchDelete(FileVO item) {
        return FileDAO.fileMatchDelete(item);
    }

    @Override
    public List<FileVO> metadataDeletList(FileVO item) {
        return FileDAO.metadataDeletList(item);
    }

    @Override
    public int fileSeqDelete(FileVO item) {
        return FileDAO.fileSeqDelete(item);
    }

    @Override
    public FileVO MedatadataInfo(FileVO data) {
        return FileDAO.MedatadataInfo(data);
    }

    /**
     * 파일 총 용량 조회
     *
     * @param FileVO
     * @return 파일 정보 조회
     */
    @Override
    public FileVO getTotalFileSize(FileVO data) {
        return FileDAO.getTotalFileSize(data);
    }

    @Override
    public FileVO fileSeqInfo(FileVO data) {
        return FileDAO.fileSeqInfo(data);
    }

    /**
     * contentsSeq, fileSe 정보로 파일의 상세 정보 조회
     *
     * @param FileVO
     * @return 파일 상세 정보
     */
    @Override
    public FileVO fileDetailInfo(FileVO data) {
        return FileDAO.fileDetailInfo(data);
    }
}
