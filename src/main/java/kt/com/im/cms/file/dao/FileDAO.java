/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.file.dao;

import java.util.List;

import kt.com.im.cms.file.vo.FileVO;

/**
 *
 * 파일 관련 데이터 접근 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 9.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public interface FileDAO {

    public List<FileVO> fileList(FileVO vo);

    public FileVO fileInfo(FileVO item);

    public int fileDelete(FileVO item);

    public int fileInsert(FileVO item);

    public int fileMatch(FileVO item);

    public int fileMatchDelete(FileVO item);

    public List<FileVO> metadataDeletList(FileVO item);

    public int fileSeqDelete(FileVO item);

    public FileVO MedatadataInfo(FileVO data);

    public FileVO getTotalFileSize(FileVO data);

    public FileVO fileSeqInfo(FileVO data);

    public FileVO fileDetailInfo(FileVO data);
}
