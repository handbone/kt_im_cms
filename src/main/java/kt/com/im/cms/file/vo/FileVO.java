/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.file.vo;

import java.io.Serializable;
import java.math.BigInteger;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * FileVO
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 8.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class FileVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6408771815107022417L;

    /* 파일 번호 */
    private int fileSeq;

    /* 파일 콘텐츠 번호 */
    private int fileContsSeq;

    /* 파일 크기 */
    private BigInteger fileSize;

    /* 첨부파일 번호 */
    private int atchFileSeq;

    /* 첨부 비디오 파일 번호 */
    private int atchVideoFileSeq;

    /* 메타 데이터 번호 */
    private int metaDataSeq;

    private String title;

    private String type;

    private String logo;

    /* 원본 파일명 */
    private String orginlFileNm;

    /* 파일 타입 */
    private String fileSe;

    private String fileExt;

    private String fileDir;

    /* 저장 파일명 */
    private String streFileNm;

    /* 파일 경로 */
    private String filePath;

    /* 비디오 경로 */
    private String videoPath;

    /* 메타 데이터 경로 */
    private String metadataPath;

    /* 메타 데이터 */
    private String metadatas;

    /* 생성자 아이디 */
    private String cretrId;

    /* 생성 일시 */
    private String cretDt;

    private MultipartFile upload;

    private String CKEditorFuncNum;

    public int getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(int fileSeq) {
        this.fileSeq = fileSeq;
    }

    public int getFileContsSeq() {
        return fileContsSeq;
    }

    public void setFileContsSeq(int fileContsSeq) {
        this.fileContsSeq = fileContsSeq;
    }

    public BigInteger getFileSize() {
        return fileSize;
    }

    public void setFileSize(BigInteger fileSize) {
        this.fileSize = fileSize;
    }

    public int getAtchFileSeq() {
        return atchFileSeq;
    }

    public void setAtchFileSeq(int atchFileSeq) {
        this.atchFileSeq = atchFileSeq;
    }

    public int getAtchVideoFileSeq() {
        return atchVideoFileSeq;
    }

    public void setAtchVideoFileSeq(int atchVideoFileSeq) {
        this.atchVideoFileSeq = atchVideoFileSeq;
    }

    public int getMetaDataSeq() {
        return metaDataSeq;
    }

    public void setMetaDataSeq(int metaDataSeq) {
        this.metaDataSeq = metaDataSeq;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getOrginlFileNm() {
        return orginlFileNm;
    }

    public void setOrginlFileNm(String orginlFileNm) {
        this.orginlFileNm = orginlFileNm;
    }

    public String getFileSe() {
        return fileSe;
    }

    public void setFileSe(String fileSe) {
        this.fileSe = fileSe;
    }

    public String getFileExt() {
        return fileExt;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public String getFileDir() {
        return fileDir;
    }

    public void setFileDir(String fileDir) {
        this.fileDir = fileDir;
    }

    public String getStreFileNm() {
        return streFileNm;
    }

    public void setStreFileNm(String streFileNm) {
        this.streFileNm = streFileNm;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setvideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getMetadataPath() {
        return metadataPath;
    }

    public void setMetadataPath(String metadataPath) {
        this.metadataPath = metadataPath;
    }

    public String getMetadatas() {
        return metadatas;
    }

    public void setMetadatas(String metadatas) {
        this.metadatas = metadatas;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public MultipartFile getUpload() {
        return upload;
    }

    public void setUpload(MultipartFile upload) {
        this.upload = upload;
    }

    public String getCKEditorFuncNum() {
        return this.CKEditorFuncNum;
    }

    public void setCKEditorFuncNum(String CKEditorFuncNum) {
        this.CKEditorFuncNum = CKEditorFuncNum;
    }

}
