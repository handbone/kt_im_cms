package kt.com.im.cms.banr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.banr.vo.BanrVO;
import kt.com.im.cms.common.dao.mysqlAbstractMapper;

/**
 *
 * 배너 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2019
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 4. 2.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("BanrDAO")
public class BanrDAOImpl extends mysqlAbstractMapper implements BanrDAO {

    /**
     * 배너 정책 등록
     *
     * @param BanrVO
     * @return 배너 정책 등록 성공 여부 (int)
     */
    @Override
    public int banrPlcyInsert(BanrVO data) {
        return insert("BanrDAO.banrPlcyInsert", data);
    }

    /**
     * 배너 정책 수정
     *
     * @param BanrVO
     * @return 배너 정책 수정 성공 여부 (int)
     */
    @Override
    public int banrPlcyUpdate(BanrVO data) {
        return update("BanrDAO.banrPlcyUpdate", data);
    }

    /**
     * 배너 정책 삭제
     *
     * @param BanrVO
     * @return 배너 정책 삭제 성공 여부 (int)
     */
    @Override
    public int banrPlcyDeleteUpdate(BanrVO data) {
        return update("BanrDAO.banrPlcyDeleteUpdate", data);
    }

    /**
     * 배너 정책 상세 정보
     *
     * @param BanrVO
     * @return 배너 정책 상세 정보
     */
    @Override
    public BanrVO banrPlcyInfo(BanrVO data) {
        return selectOne("BanrDAO.banrPlcyInfo", data);
    }

    /**
     * 배너 정책 목록 조회
     *
     * @param BanrVO
     * @return 검색 조건에 부합하는 배너 정책 목록
     */
    @Override
    public List<BanrVO> banrPlcyList(BanrVO data) {
        return selectList("BanrDAO.banrPlcyList", data);
    }

    /**
     * 배너 정책 목록 합계 조회
     *
     * @param BanrVO
     * @return 검색 조건에 부합하는 배너 정책 목록 합계
     */
    @Override
    public int banrPlcyListTotalCount(BanrVO data) {
        return selectOne("BanrDAO.banrPlcyListTotalCount", data);
    }

    /**
     * 제휴 서비스 등록
     *
     * @param BanrVO
     * @return 검색 조건에 부합하는 배너 정책 목록 합계
     */
    @Override
    public int banrCprtInsert(BanrVO data) {
        return insert("BanrDAO.banrCprtInsert", data);
    }

    /**
     * 제휴 서비스 정보 수정
     *
     * @param BanrVO
     * @return 제휴 서비스 정보 수정 성공 여부
     */
    @Override
    public int banrCprtUpdates(BanrVO data) {
        return update("BanrDAO.banrCprtUpdate", data);
    }

    /**
     * 제휴 서비스 정보 삭제
     *
     * @param BanrVO
     * @return 제휴 서비스 정보 삭제 성공 여부 (int)
     */
    @Override
    public int banrCprtDeleteUpdate(BanrVO data) {
        return update("BanrDAO.banrCprtDeleteUpdate", data);
    }

    /**
     * 제휴 서비스 상세 정보 조회
     *
     * @param BanrVO
     * @return 검색 조건에 부합하는 제휴 서비스 상세 정보
     */
    @Override
    public BanrVO banrCprtInfo(BanrVO data) {
        return selectOne("BanrDAO.banrCprtInfo", data);
    }

    /**
     * 제휴 서비스 리스트 조회
     *
     * @param BanrVO
     * @return 제휴 서비스 리스트
     */
    @Override
    public List<BanrVO> banrCprtLists(BanrVO data) {
        return selectList("BanrDAO.banrCprtList", data);
    }

    /**
     * 제휴 서비스 리스트 총합계 조회
     *
     * @param BanrVO
     * @return 제휴 서비스 총합계
     */
    @Override
    public int banrCprtListTotalCount(BanrVO data) {
        return selectOne("BanrDAO.banrCprtListTotalCount", data);
    }

    /**
     * 배너 캠페인 등록
     *
     * @param BanrVO
     * @return 배너 캠페인 등록 성공 여부
     */
    @Override
    public int banrInsert(BanrVO data) {
        return insert("BanrDAO.banrInsert", data);
    }

    /**
     * 배너 캠페인 수정
     *
     * @param BanrVO
     * @return 배너 캠페인 수정 성공 여부
     */
    @Override
    public int banrUpdate(BanrVO data) {
        int res = update("BanrDAO.banrUpdate", data);
        return res;
    }

    /**
     * 배너 캠페인 삭제 (삭제 여부 컬럼값 업데이트)
     *
     * @param BanrVO
     * @return 배너 캠페인 삭제 성공 여부
     */
    @Override
    public int banrDeleteUpdate(BanrVO data) {
        return update("BanrDAO.banrDeleteUpdate", data);
    }

    /**
     * 배너 캠페인 상제 정보 조회
     *
     * @param BanrVO
     * @return 배너 캠페인 상제 정보
     */
    @Override
    public BanrVO banrInfo(BanrVO data) {
        return selectOne("BanrDAO.banrInfo", data);
    }

    /**
     * 배너 캠페인 상제 정보 조회
     *
     * @param BanrVO
     * @return 배너 캠페인 상제 정보
     */
    @Override
    public List<BanrVO> banrList(BanrVO data) {
        return selectList("BanrDAO.banrList", data);
    }

    /**
     * 배너 캠페인 리스트 목록 조회
     *
     * @param BanrVO
     * @return 배너 캠페인 리스트 목록
     */
    @Override
    public int banrListTotalCount(BanrVO data) {
        return selectOne("BanrDAO.banrListTotalCount", data);
    }

}