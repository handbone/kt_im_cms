package kt.com.im.cms.banr.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.banr.dao.BanrDAO;
import kt.com.im.cms.banr.vo.BanrVO;

/**
 *
 * 배너 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2019
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 4. 2.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("BanrService")
public class BanrServiceImpl implements BanrService {

    @Resource(name = "BanrDAO")
    private BanrDAO BanrDAO;

    /**
     * 배너 정책 등록
     *
     * @param BanrVO
     * @return 배너 정책 등록 성공 여부 (int)
     */
    @Override
    public int banrPlcyInsert(BanrVO item) {
        return BanrDAO.banrPlcyInsert(item);
    }

    /**
     * 배너 정책 수정
     *
     * @param BanrVO
     * @return 배너 정책 수정 성공 여부 (int)
     */
    @Override
    public int banrPlcyUpdate(BanrVO item) {
        return BanrDAO.banrPlcyUpdate(item);
    }

    /**
     * 배너 정책 삭제
     *
     * @param BanrVO
     * @return 배너 정책 삭제 성공 여부 (int)
     */
    @Override
    public int banrPlcyDeleteUpdate(BanrVO item) {
        return BanrDAO.banrPlcyDeleteUpdate(item);
    }

    /**
     * 배너 정책 상세 정보
     *
     * @param BanrVO
     * @return 배너 정책 상세 정보
     */
    @Override
    public BanrVO banrPlcyInfo(BanrVO item) {
        return BanrDAO.banrPlcyInfo(item);
    }

    /**
     * 배너 정책 목록 조회
     *
     * @param BanrVO
     * @return 검색 조건에 부합하는 배너 정책 목록
     */
    @Override
    public List<BanrVO> banrPlcyList(BanrVO item) {
        return BanrDAO.banrPlcyList(item);
    }

    /**
     * 배너 정책 목록 합계 조회
     *
     * @param BanrVO
     * @return 검색 조건에 부합하는 배너 정책 목록 합계
     */
    @Override
    public int banrPlcyListTotalCount(BanrVO item) {
        return BanrDAO.banrPlcyListTotalCount(item);
    }

    /**
     * 제휴 서비스 등록
     *
     * @param BanrVO
     * @return 제휴 서비스 등록 성공 여부
     */
    @Override
    public int banrCprtInsert(BanrVO item) {
        return BanrDAO.banrCprtInsert(item);
    }

    /**
     * 제휴 서비스 정보 수정
     *
     * @param BanrVO
     * @return 제휴 서비스 정보 수정 성공 여부
     */
    @Override
    public int banrCprtUpdate(BanrVO item) {
        return BanrDAO.banrCprtUpdates(item);
    }

    /**
     * 제휴 서비스 정보 삭제
     *
     * @param BanrVO
     * @return 제휴 서비스 정보 삭제 성공 여부 (int)
     */
    @Override
    public int banrCprtDeleteUpdate(BanrVO item) {
        return BanrDAO.banrCprtDeleteUpdate(item);
    }

    /**
     * 제휴 서비스 상세 정보 조회
     *
     * @param BanrVO
     * @return 제휴 서비스 상세 정보 조회
     */
    @Override
    public BanrVO banrCprtInfo(BanrVO item) {
        return BanrDAO.banrCprtInfo(item);
    }

    /**
     * 제휴 서비스 리스트 조회
     *
     * @param BanrVO
     * @return 제휴 서비스 리스트
     */
    @Override
    public List<BanrVO> banrCprtLists(BanrVO item) {
        return BanrDAO.banrCprtLists(item);
    }

    /**
     * 제휴 서비스 리스트 총합계 조회
     *
     * @param BanrVO
     * @return 제휴 서비스 총합계
     */
    @Override
    public int banrCprtListTotalCount(BanrVO item) {
        return BanrDAO.banrCprtListTotalCount(item);
    }

    /**
     * 배너 캠페인 등록
     *
     * @param BanrVO
     * @return 배너 캠페인 등록 성공 여부
     */
    @Override
    public int banrInsert(BanrVO item) {
        return BanrDAO.banrInsert(item);
    }

    /**
     * 배너 캠페인 수정
     *
     * @param BanrVO
     * @return 배너 캠페인 수정 성공 여부
     */
    @Override
    public int banrUpdate(BanrVO item) {
        return BanrDAO.banrUpdate(item);
    }

    /**
     * 배너 캠페인 삭제 (삭제 여부 컬럼값 업데이트)
     *
     * @param BanrVO
     * @return 배너 캠페인 삭제 성공 여부
     */
    @Override
    public int banrDeleteUpdate(BanrVO item) {
        return BanrDAO.banrDeleteUpdate(item);
    }

    /**
     * 배너 캠페인 상제 정보 조회
     *
     * @param BanrVO
     * @return 배너 캠페인 상제 정보
     */
    @Override
    public BanrVO banrInfo(BanrVO item) {
        return BanrDAO.banrInfo(item);
    }

    /**
     * 배너 캠페인 리스트 목록 조회
     *
     * @param BanrVO
     * @return 배너 캠페인 리스트 목록
     */
    @Override
    public List<BanrVO> banrList(BanrVO item) {
        return BanrDAO.banrList(item);
    }

    /**
     * 배너 캠페인 리스트 목록 총합계 조회
     *
     * @param BanrVO
     * @return 배너 캠페인 목록 총합계
     */
    @Override
    public int banrListTotalCount(BanrVO item) {
        return BanrDAO.banrListTotalCount(item);
    }

}
