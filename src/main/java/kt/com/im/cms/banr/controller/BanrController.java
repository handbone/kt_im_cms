/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.banr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * DEVICE 관리에 관한 컨트롤러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 28.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class BanrController {
    /**
     * 온라인 서비스 > 배너캠페인관리
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/banr", method = RequestMethod.GET)
    public ModelAndView banrMng(ModelAndView mv) {
        mv.setViewName("/views/onlineService/banr/banrMng");
        return mv;
    }

    /**
     * 온라인 서비스 > 배너 캠페인 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/banr/{banrSeq}", method = RequestMethod.GET)
    public ModelAndView banrMngDetail(ModelAndView mv, @PathVariable(value = "banrSeq") String banrSeq) {
        mv.addObject("banrSeq", banrSeq);
        mv.setViewName("/views/onlineService/banr/banrDetail");
        return mv;
    }

    /**
     * 온라인 서비스 > 배너 캠페인 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/banr/edit/{banrSeq}", method = RequestMethod.GET)
    public ModelAndView banrMngEdit(ModelAndView mv, @PathVariable(value = "banrSeq") String banrSeq) {
        mv.addObject("banrSeq", banrSeq);
        mv.setViewName("/views/onlineService/banr/banrEdit");
        return mv;
    }

    /**
     * 온라인 서비스 > 배너캠페인관리 > 등록
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/banr/regist", method = RequestMethod.GET)
    public ModelAndView banrRegist(ModelAndView mv) {
        mv.setViewName("/views/onlineService/banr/banrRegist");
        return mv;
    }

    /**
     * 온라인 서비스 > 배너캠페인이력관리
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/banr/hst", method = RequestMethod.GET)
    public ModelAndView banrHst(ModelAndView mv) {
        // mv.setViewName("/views/onlineService/banr/banrMng");
        return mv;
    }

    /**
     * 온라인 서비스 > 제휴서비스 관리
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/cprtSvc", method = RequestMethod.GET)
    public ModelAndView banrCprtSvcMng(ModelAndView mv) {
        mv.setViewName("/views/onlineService/cprt/cprtMng");
        return mv;
    }

    /**
     * 온라인 서비스 > 제휴서비스 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/cprtSvc/{cprtSvcSeq}", method = RequestMethod.GET)
    public ModelAndView cprtSvcDetail(ModelAndView mv, @PathVariable(value = "cprtSvcSeq") String cprtSvcSeq) {
        mv.addObject("cprtSvcSeq", cprtSvcSeq);
        mv.setViewName("/views/onlineService/cprt/cprtDetail");
        return mv;
    }

    /**
     * 온라인 서비스 > 제휴서비스 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/cprtSvc/edit/{cprtSvcSeq}", method = RequestMethod.GET)
    public ModelAndView cprtSvcEdit(ModelAndView mv, @PathVariable(value = "cprtSvcSeq") String cprtSvcSeq) {
        mv.addObject("cprtSvcSeq", cprtSvcSeq);
        mv.setViewName("/views/onlineService/cprt/cprtEdit");
        return mv;
    }

    /**
     * 온라인 서비스 > 제휴서비스 관리 > 등록
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/cprtSvc/regist", method = RequestMethod.GET)
    public ModelAndView cprtSvcRegist(ModelAndView mv) {
        mv.setViewName("/views/onlineService/cprt/cprtRegist");
        return mv;
    }

    /**
     * 온라인 서비스 > 배너정책관리
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineService/banr/plcyMng", method = RequestMethod.GET)
    public ModelAndView banrPlcyMng(ModelAndView mv) {
        // mv.setViewName("/views/onlineService/banr/banrMng");
        return mv;
    }

}