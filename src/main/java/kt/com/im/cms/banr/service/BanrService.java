/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.banr.service;

import java.util.List;

import kt.com.im.cms.banr.vo.BanrVO;

/**
 *
 * 배너 관련 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2019
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 4. 2.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public interface BanrService {

    int banrPlcyInsert(BanrVO item);

    int banrPlcyUpdate(BanrVO item);

    int banrPlcyDeleteUpdate(BanrVO item);

    BanrVO banrPlcyInfo(BanrVO item);

    List<BanrVO> banrPlcyList(BanrVO item);

    int banrPlcyListTotalCount(BanrVO item);

    int banrCprtInsert(BanrVO item);

    int banrCprtUpdate(BanrVO item);

    int banrCprtDeleteUpdate(BanrVO item);

    BanrVO banrCprtInfo(BanrVO item);

    List<BanrVO> banrCprtLists(BanrVO item);

    int banrCprtListTotalCount(BanrVO item);

    int banrInsert(BanrVO item);

    int banrUpdate(BanrVO item);

    int banrDeleteUpdate(BanrVO item);

    BanrVO banrInfo(BanrVO item);

    List<BanrVO> banrList(BanrVO item);

    int banrListTotalCount(BanrVO item);
}
