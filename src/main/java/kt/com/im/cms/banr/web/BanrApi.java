package kt.com.im.cms.banr.web;

import java.io.File;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.banr.service.BanrService;
import kt.com.im.cms.banr.vo.BanrVO;
import kt.com.im.cms.common.util.CommonFileUtil;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.file.web.FileApi;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;

/**
 *
 * 배너에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2019.04.02
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 4. 2.   A2TEC      최초생성
* 2019. 4. 2.   A2TEC      /api/banr (POST,GET,PUT,DELETE)추가
 *
 *      </pre>
 */

@Controller
public class BanrApi {
    @Resource(name = "BanrService")
    private BanrService banrService;

    @Resource(name = "FileService")
    private FileService fileService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private CommonFileUtil fileUtil;

    @Inject
    private FileSystemResource fsResource;

    private final static String viewName = "../resources/api/banr/banrProcess";

    /**
     * 배너 정책 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/plcyMng")
    public ModelAndView banrPlcyProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAllowMember(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");

        BanrVO item = new BanrVO();
        item.setExistChk("N");
        FileVO Fitem = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String checkString = "";
            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request,
                    "terrNm,svcSeq,sizeWdth,sizeHght,fileType,lmtFileSize,terrUrl");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int sizeWdth = CommonFnc.emptyCheckInt("sizeWdth", request);
            int sizeHght = CommonFnc.emptyCheckInt("sizeHght", request);
            int lmtFileSize = CommonFnc.emptyCheckInt("lmtFileSize", request);

            String terrNm = CommonFnc.emptyCheckString("terrNm", request);
            String useYn = CommonFnc.emptyCheckString("useYn", request);
            String terrUrl = CommonFnc.emptyCheckString("terrUrl", request);

            item.setSvcSeq(svcSeq);
            item.setSizeWdth(sizeWdth);
            item.setSizeHght(sizeHght);
            item.setLmtFileSize(lmtFileSize);
            item.setTerrNm(terrNm);
            item.setUseYn(useYn);
            item.setTerrUrl(terrUrl);
            item.setCretrId((String) session.getAttribute("id"));

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                /* 중복 체크 할것 */
                item.setExistChk("Y");
                item.setPlcySeq(0);
                BanrVO citem = banrService.banrPlcyInfo(item);
                if (citem != null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_EXIST_INFO);
                    return rsModel.getModelAndView();
                }

                int result = banrService.banrPlcyInsert(item);
                if (result == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", item);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrPlcyInsert");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            String checkString = "";
            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request, "plcySeq,sizeWdth,sizeHght,fileType,lmtFileSize,terrUrl");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            int plcySeq = CommonFnc.emptyCheckInt("plcySeq", request);
            int sizeWdth = CommonFnc.emptyCheckInt("sizeWdth", request);
            int sizeHght = CommonFnc.emptyCheckInt("sizeHght", request);
            int lmtFileSize = CommonFnc.emptyCheckInt("lmtFileSize", request);

            String useYn = CommonFnc.emptyCheckString("useYn", request);
            String terrUrl = CommonFnc.emptyCheckString("terrUrl", request);

            item.setPlcySeq(plcySeq);
            item.setSizeWdth(sizeWdth);
            item.setSizeHght(sizeHght);
            item.setLmtFileSize(lmtFileSize);
            item.setUseYn(useYn);
            item.setTerrUrl(terrUrl);
            item.setAmdrId((String) session.getAttribute("id"));

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = banrService.banrPlcyUpdate(item);
                if (result == 1) {
                    String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);
                    List<String> deleteFilePath = new ArrayList<String>();

                    if (delFileSeqs != "") {
                        String[] delFileSeq = delFileSeqs.split(",");

                        for (int i = 0; i < delFileSeq.length; i++) {
                            if (Integer.parseInt(delFileSeq[i]) == 0) {
                                break;
                            }
                            Fitem.setFileSeq(Integer.parseInt(delFileSeq[i]));
                            Fitem.setFileSe("BANR_PLCY");
                            FileVO resultFitem = fileService.fileSeqInfo(Fitem);
                            String filePath = resultFitem.getFilePath();
                            if (fileService.fileSeqDelete(Fitem) == 1) {
                                deleteFilePath.add(filePath);
                            }
                        }
                    }

                    for (int i = 0; i < deleteFilePath.size(); i++) {
                        File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                        f.delete();
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", item);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrPlcyUpdate");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }

        } else if (method.equals("DELETE")) {
            String checkString = "";
            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request, "delSeqs");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            String delSeqs = CommonFnc.emptyCheckString("delSeqs", request);

            if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)) {
                item.setSvcSeq(userVO.getSvcSeq());
            } else {
                item.setSvcSeq(0);
            }

            item.setDelSeqs(delSeqs);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = banrService.banrPlcyDeleteUpdate(item);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrPlcyDelete");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }

        } else { // GET
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int plcySeq = CommonFnc.emptyCheckInt("plcySeq", request);

            item.setSvcSeq(svcSeq);
            if (plcySeq != 0) {
                item.setPlcySeq(plcySeq);

                BanrVO resultItem = banrService.banrPlcyInfo(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                            && userVO.getSvcSeq() != resultItem.getSvcSeq())) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    // /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                    // String getType = CommonFnc.emptyCheckString("getType", request);
                    // if (getType.equals("")) {
                    // resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    // resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                    // }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }

                rsModel.setResultType("banrPlcyInfo");
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (plcySeq == 0 && CommonFnc.checkReqParameter(request, "plcySeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }
                if (svcSeq != 0) {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != svcSeq)) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }
                }

                String searchType = request.getParameter("searchType") == null ? ""
                        : request.getParameter("searchType");

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    item.setTarget(searchField);
                    item.setKeyword(searchString);
                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("PLCY_SEQ")) {
                } else if (sidx.equals("TERR_NM")) {
                } else if (sidx.equals("TERR_URL")) {
                } else if (sidx.equals("SVC_NM")) {
                } else if (sidx.equals("USE_YN")) {
                } else if (sidx.equals("CRET_DT")) {
                } else {
                    sidx = "PLCY_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<BanrVO> resultItem = banrService.banrPlcyList(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                        resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));
                    }

                    int totalCount = banrService.banrPlcyListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", (totalCount - 1) / limit);
                    rsModel.setResultType("banrPlcyList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 배너 제휴 서비스 관리 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/cprtMng")
    public ModelAndView banrCprtProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAllowMember(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");

        BanrVO item = new BanrVO();
        FileVO Fitem = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String checkString = "";
            // Checking Mendatory S

            checkString = CommonFnc.requiredChecking(request, "cprtSvcNm,menuUrl,stDt,fnsDt,perdYn,useYn,svcSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            String cprtSvcNm = CommonFnc.strFilter(CommonFnc.emptyCheckString("cprtSvcNm", request));
            String menuUrl = CommonFnc.strFilter(CommonFnc.emptyCheckString("menuUrl", request));
            String stDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("stDt", request));
            String fnsDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("fnsDt", request));
            String perdYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("perdYn", request));
            String useYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("useYn", request));

            item.setSvcSeq(svcSeq);
            item.setCprtSvcNm(cprtSvcNm);

            // escape 문자들 원복하여 저장
            menuUrl = CommonFnc.unescapeStr(menuUrl);
            item.setMenuUrl(menuUrl);
            item.setStDt(stDt);
            item.setFnsDt(fnsDt);
            item.setPerdYn(perdYn);
            item.setUseYn(useYn);
            item.setCretrId((String) session.getAttribute("id"));

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = banrService.banrCprtInsert(item);
                if (result == 1) {
                    String pathBasic = fsResource.getPath();
                    FileVO file = new FileVO();

                    MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
                    List<MultipartFile> files = multipartHttpServletRequest.getFiles("file");

                    Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
                    MultipartFile multipartFile = null;

                    // 디레토리가 없다면 생성
                    File dir = new File(pathBasic);
                    if (!dir.isDirectory()) {
                        dir.mkdirs();
                    }

                    while (iterator.hasNext()) {
                        int contsSeq = item.getCprtSvcSeq();
                        String fileSe = "";
                        multipartFile = multipartHttpServletRequest.getFile(iterator.next());

                        if (multipartFile.isEmpty() == false) {
                            CommonsMultipartFile commonsMultipartFile = (CommonsMultipartFile) multipartFile;
                            FileItem fileItem = commonsMultipartFile.getFileItem();
                            DiskFileItem diskFileItem = (DiskFileItem) fileItem;

                            if (diskFileItem.getFieldName().equals("cprtImg")) {
                                fileSe = "CPRT_IMG";
                            } else {
                                fileSe = "CPRT_IMG_T";
                            }

                            String flieName = multipartFile.getName();
                            String orginlFileNm = new String(multipartFile.getOriginalFilename().getBytes("UTF-8"),
                                    "UTF-8"); // 한글꺠짐
                            if (!FileApi.isValidFileName(orginlFileNm)) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                                return rsModel.getModelAndView();
                            }
                            long filesize = multipartFile.getSize();
                            // System.out.println("------------- file start -------------");
                            // System.out.println("name : "+flieName);
                            // System.out.println("filename : "+origName);
                            // System.out.println("size : "+filesize);
                            // System.out.println("-------------- file end --------------\n");

                            String ext = orginlFileNm.substring(orginlFileNm.lastIndexOf('.')); //
                            String now = new SimpleDateFormat("yyMMddHmsS").format(new Date()); // 현재시간
                            String path = "";
                            if (CommonFileUtil.isAllowUploadImgFileType(ext)) {
                                path += "/cprt/" + item.getCprtSvcSeq();
                            } else {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_FILE_INSERT);
                                transactionManager.rollback(status);
                                return rsModel.getModelAndView();
                            }

                            // 디레토리가 없다면 생성
                            File dir2 = new File(pathBasic + path);
                            if (!dir2.isDirectory()) {
                                dir2.mkdirs();
                            }

                            String saveFileName = now + ext;
                            File serverFile = new File(pathBasic + path + File.separator + saveFileName);

                            multipartFile.transferTo(serverFile);
                            file.setOrginlFileNm(orginlFileNm);
                            file.setFileContsSeq(contsSeq);
                            file.setStreFileNm(saveFileName);
                            file.setFileSize(BigInteger.valueOf(filesize));
                            file.setFileDir(path + "/" + saveFileName);
                            file.setFileSe(fileSe);
                            file.setCretrId((String) session.getAttribute("id"));

                            int resultFile = fileService.fileInsert(file);
                            if (resultFile != 1) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_FILE_INSERT);
                                transactionManager.rollback(status);
                                return rsModel.getModelAndView();
                            }
                        }
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", item);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrCprtInsert");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            String checkString = "";
            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request, "cprtSvcSeq,menuUrl,stDt,fnsDt,perdYn,useYn");

            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            int cprtSvcSeq = CommonFnc.emptyCheckInt("cprtSvcSeq", request);
            String menuUrl = CommonFnc.strFilter(CommonFnc.emptyCheckString("menuUrl", request));
            String stDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("stDt", request));
            String fnsDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("fnsDt", request));
            String perdYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("perdYn", request));
            String useYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("useYn", request));

            item.setCprtSvcSeq(cprtSvcSeq);
            item.setStDt(stDt);
            item.setFnsDt(fnsDt);
            item.setPerdYn(perdYn);
            item.setUseYn(useYn);

            // escape 문자들 원복하여 저장
            menuUrl = CommonFnc.unescapeStr(menuUrl);
            item.setMenuUrl(menuUrl);
            item.setAmdrId((String) session.getAttribute("id"));

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)) {
                item.setSvcSeq(userVO.getSvcSeq());
            } else {
                item.setSvcSeq(0);
            }

            try {
                int result = banrService.banrCprtUpdate(item);
                if (result == 1) {
                    String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);
                    List<String> deleteFilePath = new ArrayList<String>();

                    if (delFileSeqs != "") {
                        String[] delFileSeq = delFileSeqs.split(",");

                        for (int i = 0; i < delFileSeq.length; i++) {
                            if (Integer.parseInt(delFileSeq[i]) == 0) {
                                break;
                            }
                            Fitem.setFileSeq(Integer.parseInt(delFileSeq[i]));
                            Fitem.setFileSe("CPRT_ALL");
                            FileVO resultFitem = fileService.fileSeqInfo(Fitem);
                            String filePath = resultFitem.getFilePath();
                            if (fileService.fileSeqDelete(Fitem) == 1) {
                                deleteFilePath.add(filePath);
                            }
                        }
                    }

                    String pathBasic = fsResource.getPath();
                    FileVO file = new FileVO();

                    MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
                    List<MultipartFile> files = multipartHttpServletRequest.getFiles("file");

                    Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
                    MultipartFile multipartFile = null;

                    // 디레토리가 없다면 생성
                    File dir = new File(pathBasic);
                    if (!dir.isDirectory()) {
                        dir.mkdirs();
                    }

                    while (iterator.hasNext()) {
                        int contsSeq = item.getCprtSvcSeq();
                        String fileSe = "";
                        multipartFile = multipartHttpServletRequest.getFile(iterator.next());

                        if (multipartFile.isEmpty() == false) {
                            CommonsMultipartFile commonsMultipartFile = (CommonsMultipartFile) multipartFile;
                            FileItem fileItem = commonsMultipartFile.getFileItem();
                            DiskFileItem diskFileItem = (DiskFileItem) fileItem;

                            if (diskFileItem.getFieldName().equals("cprtImg")) {
                                fileSe = "CPRT_IMG";
                            } else {
                                fileSe = "CPRT_IMG_T";
                            }

                            String flieName = multipartFile.getName();
                            String orginlFileNm = new String(multipartFile.getOriginalFilename().getBytes("UTF-8"),
                                    "UTF-8"); // 한글꺠짐
                            if (!FileApi.isValidFileName(orginlFileNm)) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                                return rsModel.getModelAndView();
                            }
                            long filesize = multipartFile.getSize();
                            // System.out.println("------------- file start -------------");
                            // System.out.println("name : "+flieName);
                            // System.out.println("filename : "+origName);
                            // System.out.println("size : "+filesize);
                            // System.out.println("-------------- file end --------------\n");

                            String ext = orginlFileNm.substring(orginlFileNm.lastIndexOf('.')); //
                            String now = new SimpleDateFormat("yyMMddHmsS").format(new Date()); // 현재시간
                            String path = "";
                            if (CommonFileUtil.isAllowUploadImgFileType(ext)) {
                                path += "/cprt/" + item.getCprtSvcSeq();
                            } else {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_FILE_INSERT);
                                transactionManager.rollback(status);
                                return rsModel.getModelAndView();
                            }

                            // 디레토리가 없다면 생성
                            File dir2 = new File(pathBasic + path);
                            if (!dir2.isDirectory()) {
                                dir2.mkdirs();
                            }

                            String saveFileName = now + ext;
                            File serverFile = new File(pathBasic + path + File.separator + saveFileName);

                            multipartFile.transferTo(serverFile);
                            file.setOrginlFileNm(orginlFileNm);
                            file.setFileContsSeq(contsSeq);
                            file.setStreFileNm(saveFileName);
                            file.setFileSize(BigInteger.valueOf(filesize));
                            file.setFileDir(path + "/" + saveFileName);
                            file.setFileSe(fileSe);
                            file.setCretrId((String) session.getAttribute("id"));

                            int resultFile = fileService.fileInsert(file);
                            if (resultFile != 1) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_FILE_INSERT);
                                transactionManager.rollback(status);
                                return rsModel.getModelAndView();
                            }
                        }
                    }

                    for (int i = 0; i < deleteFilePath.size(); i++) {
                        File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                        f.delete();
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", item);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrPlcyUpdate");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            String checkString = "";
            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request, "delSeqs");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            String delSeqs = CommonFnc.emptyCheckString("delSeqs", request);

            if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)) {
                item.setSvcSeq(userVO.getSvcSeq());
            } else {
                item.setSvcSeq(0);
            }

            item.setDelSeqs(delSeqs);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                item.setAmdrId((String) session.getAttribute("id"));
                int result = banrService.banrCprtDeleteUpdate(item);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrCprtDelete");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int cprtSvcSeq = CommonFnc.emptyCheckInt("cprtSvcSeq", request);

            item.setSvcSeq(svcSeq);
            if (cprtSvcSeq != 0) {
                item.setCprtSvcSeq(cprtSvcSeq);

                BanrVO resultItem = banrService.banrCprtInfo(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                            && userVO.getSvcSeq() != resultItem.getSvcSeq())) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    /* 파일정보 */
                    FileVO fitem = new FileVO();
                    fitem.setFileContsSeq(resultItem.getCprtSvcSeq());
                    fitem.setFileSe("CPRT_IMG");
                    List<FileVO> fileImgList = fileService.fileList(fitem);

                    fitem.setFileContsSeq(resultItem.getCprtSvcSeq());
                    fitem.setFileSe("CPRT_IMG_T");

                    List<FileVO> fileThumbnailList = fileService.fileList(fitem);

                    resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                    resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("cprtImg", fileImgList);
                    rsModel.setData("cprtThumbImg", fileThumbnailList);

                }

                rsModel.setResultType("banrCprtInfo");
                return rsModel.getModelAndView();
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (cprtSvcSeq == 0 && CommonFnc.checkReqParameter(request, "cprtSvcSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                if (svcSeq != 0) {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != svcSeq)) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }
                }

                String perdSttus = CommonFnc.emptyCheckString("perdSttus", request);

                item.setPerdSttus(perdSttus);

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    item.setTarget(searchField);
                    item.setKeyword(searchString);
                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = ((currentPage - 1) * limit);

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("CPRT_SVC_SEQ")) {
                } else if (sidx.equals("CPRT_SVC_NM")) {
                } else if (sidx.equals("MENU_URL")) {
                } else if (sidx.equals("CRETR_NM")) {
                } else if (sidx.equals("CRET_DT")) {
                } else if (sidx.equals("PERD_STTUS")) {
                } else {
                    sidx = "CPRT_SVC_SEQ";
                }

                if (page < 0 && limit > 0) {
                    page = 0;
                    pageEnd = 0;
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<BanrVO> resultItem = banrService.banrCprtLists(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                        resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));
                        resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));
                        /* 파일정보 */
                        FileVO fitem = new FileVO();
                        fitem.setFileContsSeq(resultItem.get(i).getCprtSvcSeq());
                        fitem.setFileSe("CPRT_IMG");
                        List<FileVO> fileImgList = fileService.fileList(fitem);
                        resultItem.get(i).setCprtImgList(fileImgList);

                        // 실제 서버에 파일이 존재하는지 여부 확인
                        resultItem.get(i).setIsImgFile("false");
                        boolean isExistImgFile = false;
                        for (int j = 0; j < fileImgList.size(); j++) {
                            isExistImgFile = fileUtil.isExistFile(fileImgList.get(j).getFilePath());
                        }

                        if (isExistImgFile) {
                            resultItem.get(i).setIsImgFile("true");
                        }

                        fitem.setFileContsSeq(resultItem.get(i).getCprtSvcSeq());
                        fitem.setFileSe("CPRT_IMG_T");

                        List<FileVO> fileThumbnailList = fileService.fileList(fitem);
                        resultItem.get(i).setCprtThmbImgList(fileThumbnailList);

                        // 실제 서버에 파일이 존재하는지 여부 확인
                        resultItem.get(i).setIsThumbImgFile("false");
                        boolean isExistTImgFile = false;
                        for (int j = 0; j < fileThumbnailList.size(); j++) {
                            isExistTImgFile = fileUtil.isExistFile(fileThumbnailList.get(j).getFilePath());
                        }

                        if (isExistTImgFile) {
                            resultItem.get(i).setIsThumbImgFile("true");
                        }
                    }

                    int totalCount = banrService.banrCprtListTotalCount(item);

                    int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;

                    if (currentPage > totalPage) {
                        currentPage = 1;
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalPage);
                    rsModel.setResultType("banrCprtList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 배너 캠페인 관리 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/banrAPI")
    public ModelAndView banrAPICampProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");

        BanrVO item = new BanrVO();
        FileVO Fitem = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            MemberVO member = new MemberVO();

            int banrSeq = CommonFnc.emptyCheckInt("banrSeq", request);
            int plcySeq = CommonFnc.emptyCheckInt("plcySeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

            String perdSttus = CommonFnc.emptyCheckString("perdSttus", request);

            String mbrTokn = CommonFnc.emptyCheckString("mbrTokn", request);
            String encodeToken = mbrTokn.replaceAll(" ", "+");

            encodeToken = mbrTokn.replaceAll(" ", "+");
            member.setStorSeq(storSeq);
            member.setMbrTokn(encodeToken);
            int tokenSearchResult = memberService.searchMemberToken(member);

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            } else {
                MemberVO result = memberService.getMember(member);

                item.setPerdSttus(perdSttus);
                item.setSvcSeq(result.getSvcSeq());
                if (banrSeq != 0) {
                    item.setBanrSeq(banrSeq);
                    item.setPerdSttus("DISPLAY");

                    BanrVO resultItem = banrService.banrInfo(item);

                    if (resultItem == null) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        // /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                        // String getType = CommonFnc.emptyCheckString("getType", request);
                        // if (getType.equals("")) {
                        // resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                        // resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                        // }

                        /* 파일정보 */
                        FileVO fitem = new FileVO();
                        fitem.setFileContsSeq(resultItem.getBanrSeq());
                        fitem.setFileSe("BANR_IMG");
                        List<FileVO> fileImgList = fileService.fileList(fitem);

                        resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                        resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                        resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("banrImg", fileImgList);

                    }

                    rsModel.setResultType("banrAPIInfo");
                    return rsModel.getModelAndView();
                } else {
                    // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                    if (banrSeq == 0 && CommonFnc.checkReqParameter(request, "banrSeq")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        return rsModel.getModelAndView();
                    }

                    item.setPerdSttus("DISPLAY");

                    String searchConfirm = request.getParameter("_search") == null ? "false"
                            : request.getParameter("_search");

                    item.setSearchConfirm(searchConfirm);

                    /** 정렬할 필드 */
                    String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                    /** 정렬 방법 */
                    String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                    sidx = "PERD_STTUS";

                    item.setSidx(sidx);
                    item.setSord(sord);
                    item.setOffset(-1);
                    item.setLimit(-1);
                    item.setPlcySeq(plcySeq);

                    List<BanrVO> resultItem = banrService.banrList(item);

                    if (resultItem.isEmpty()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    } else {
                        /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                        for (int i = 0; i < resultItem.size(); i++) {
                            resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                            resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));
                            resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));
                        }

                        int totalCount = banrService.banrListTotalCount(item);

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("totalCount", totalCount);
                        rsModel.setResultType("banrAPIList");
                    }
                }
            }

        }

        return rsModel.getModelAndView();
    }

    /**
     * 배너 제휴 서비스 관리 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/cprtAPI")
    public ModelAndView banrCprtAPIProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);
        request.setCharacterEncoding("UTF-8");

        BanrVO item = new BanrVO();
        FileVO Fitem = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int cprtSvcSeq = CommonFnc.emptyCheckInt("cprtSvcSeq", request);

            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            MemberVO member = new MemberVO();

            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String perdSttus = CommonFnc.emptyCheckString("perdSttus", request);
            String mbrTokn = CommonFnc.emptyCheckString("mbrTokn", request);
            String encodeToken = mbrTokn.replaceAll(" ", "+");

            encodeToken = mbrTokn.replaceAll(" ", "+");
            member.setStorSeq(storSeq);
            member.setMbrTokn(encodeToken);
            int tokenSearchResult = memberService.searchMemberToken(member);

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_TOKN);
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            } else {
                MemberVO result = memberService.getMember(member);

                item.setPerdSttus(perdSttus);
                item.setSvcSeq(result.getSvcSeq());

                if (cprtSvcSeq != 0) {
                    item.setCprtSvcSeq(cprtSvcSeq);
                    item.setPerdSttus("DISPLAY");

                    BanrVO resultItem = banrService.banrCprtInfo(item);

                    if (resultItem == null) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        // /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                        // String getType = CommonFnc.emptyCheckString("getType", request);
                        // if (getType.equals("")) {
                        // resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                        // resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                        // }

                        /* 파일정보 */
                        FileVO fitem = new FileVO();
                        fitem.setFileContsSeq(resultItem.getCprtSvcSeq());
                        fitem.setFileSe("CPRT_IMG");
                        List<FileVO> fileImgList = fileService.fileList(fitem);

                        fitem.setFileContsSeq(resultItem.getCprtSvcSeq());
                        fitem.setFileSe("CPRT_IMG_T");

                        List<FileVO> fileThumbnailList = fileService.fileList(fitem);

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("cprtImg", fileImgList);
                        rsModel.setData("cprtThumbImg", fileThumbnailList);

                    }

                    rsModel.setResultType("banrCprtAPIInfo");
                } else {
                    // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                    if (cprtSvcSeq == 0 && CommonFnc.checkReqParameter(request, "cprtSvcSeq")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        return rsModel.getModelAndView();
                    }

                    item.setPerdSttus("DISPLAY");

                    String searchConfirm = request.getParameter("_search") == null ? "false"
                            : request.getParameter("_search");

                    item.setSearchConfirm(searchConfirm);

                    /** 정렬할 필드 */
                    String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                    /** 정렬 방법 */
                    String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                    // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                    sidx = "PERD_STTUS";

                    item.setSidx(sidx);
                    item.setSord(sord);
                    item.setOffset(-1);
                    item.setLimit(-1);

                    List<BanrVO> resultItem = banrService.banrCprtLists(item);

                    if (resultItem.isEmpty()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    } else {
                        /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                        for (int i = 0; i < resultItem.size(); i++) {
                            resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                            resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));

                            /* 파일정보 */
                            FileVO fitem = new FileVO();
                            fitem.setFileContsSeq(resultItem.get(i).getCprtSvcSeq());
                            fitem.setFileSe("CPRT_IMG");
                            List<FileVO> fileImgList = fileService.fileList(fitem);

                            resultItem.get(i).setCprtImgList(fileImgList);

                            fitem.setFileContsSeq(resultItem.get(i).getCprtSvcSeq());
                            fitem.setFileSe("CPRT_IMG_T");

                            List<FileVO> fileThumbnailList = fileService.fileList(fitem);

                            resultItem.get(i).setCprtThmbImgList(fileThumbnailList);

                        }

                        int totalCount = banrService.banrCprtListTotalCount(item);

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("totalCount", totalCount);
                        rsModel.setResultType("banrCprtAPIList");
                    }
                }
            }

        }

        return rsModel.getModelAndView();
    }

    /**
     * 배너 캠페인 관리 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/banrMng")
    public ModelAndView banrCampProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAllowMember(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");

        BanrVO item = new BanrVO();
        FileVO Fitem = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request,
                    "banrCampNm,svcSeq,plcySeq,perdYn,stDt,fnsDt,useYn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int orderNum = CommonFnc.emptyCheckInt("orderNum", request);
            int plcySeq = CommonFnc.emptyCheckInt("plcySeq", request);
            String stDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("stDt", request));
            String fnsDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("fnsDt", request));
            String perdYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("perdYn", request));
            String useYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("useYn", request));
            String banrCampNm = CommonFnc.strFilter(CommonFnc.emptyCheckString("banrCampNm", request));
            String linkUri = CommonFnc.strFilter(CommonFnc.emptyCheckString("linkUri", request));
            String linkUrl = CommonFnc.strFilter(CommonFnc.emptyCheckString("linkUrl", request));
            String useTitleType = CommonFnc.strFilter(CommonFnc.emptyCheckString("useTitleType", request));
            String btnTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("btnTitle", request));
            String banrTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("banrTitle", request));
            String banrSubTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("banrSubTitle", request));

            item.setSvcSeq(svcSeq);
            item.setStDt(stDt);
            item.setFnsDt(fnsDt);
            item.setPerdYn(perdYn);
            item.setUseYn(useYn);
            item.setOrderNum(orderNum);
            item.setPlcySeq(plcySeq);
            item.setStDt(stDt);
            item.setFnsDt(fnsDt);
            item.setPerdYn(perdYn);
            item.setBanrCampNm(banrCampNm);

            // escape 문자들 원복하여 저장
            linkUri = CommonFnc.unescapeStr(linkUri);
            linkUrl = CommonFnc.unescapeStr(linkUrl);
            item.setLinkUri(linkUri);
            item.setLinkUrl(linkUrl);
            item.setUseTitleType(useTitleType);
            item.setBtnTitle(btnTitle);
            item.setBanrSubTitle(banrSubTitle);
            item.setBanrTitle(banrTitle);
            item.setCretrId((String) session.getAttribute("id"));

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                item.setPlcySeq(plcySeq);

                BanrVO resultPlcytem = banrService.banrPlcyInfo(item);

                if (resultPlcytem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                int result = banrService.banrInsert(item);
                if (result == 1) {
                    String pathBasic = fsResource.getPath();
                    FileVO file = new FileVO();

                    MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
                    List<MultipartFile> files = multipartHttpServletRequest.getFiles("file");

                    Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
                    MultipartFile multipartFile = null;
                    List resultList = new ArrayList();

                    int lmtFileSize = resultPlcytem.getLmtFileSize();

                    // 디레토리가 없다면 생성
                    File dir = new File(pathBasic);
                    if (!dir.isDirectory()) {
                        dir.mkdirs();
                    }

                    while (iterator.hasNext()) {
                        int contsSeq = item.getBanrSeq();
                        String fileSe = "BANR_IMG";
                        multipartFile = multipartHttpServletRequest.getFile(iterator.next());
                        if (multipartFile.isEmpty() == false) {
                            String flieName = multipartFile.getName();
                            String orginlFileNm = new String(multipartFile.getOriginalFilename().getBytes("UTF-8"),
                                    "UTF-8"); // 한글꺠짐
                            if (!FileApi.isValidFileName(orginlFileNm)) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                                return rsModel.getModelAndView();
                            }
                            long filesize = multipartFile.getSize();
                            // System.out.println("------------- file start -------------");
                            // System.out.println("name : "+flieName);
                            // System.out.println("filename : "+origName);
                            // System.out.println("size : "+filesize);
                            // System.out.println("-------------- file end --------------\n");

                            if (lmtFileSize != -1 && filesize > MegToBytes(lmtFileSize)) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_OVER_FILE_SIZE);
                                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                                transactionManager.rollback(status);
                                return rsModel.getModelAndView();
                            }

                            String ext = orginlFileNm.substring(orginlFileNm.lastIndexOf('.')); //
                            String now = new SimpleDateFormat("yyMMddHmsS").format(new Date()); // 현재시간
                            String path = "";
                            if (CommonFileUtil.isAllowUploadImgFileType(ext)) {
                                path += "/banr/" + item.getBanrSeq();
                            } else {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_FILE_INSERT);
                                transactionManager.rollback(status);
                                return rsModel.getModelAndView();
                            }

                            // 디레토리가 없다면 생성
                            File dir2 = new File(pathBasic + path);
                            if (!dir2.isDirectory()) {
                                dir2.mkdirs();
                            }

                            String saveFileName = now + ext;
                            File serverFile = new File(pathBasic + path + File.separator + saveFileName);

                            multipartFile.transferTo(serverFile);
                            file.setOrginlFileNm(orginlFileNm);
                            file.setFileContsSeq(contsSeq);
                            file.setStreFileNm(saveFileName);
                            file.setFileSize(BigInteger.valueOf(filesize));
                            file.setFileDir(path + "/" + saveFileName);
                            file.setFileSe(fileSe);
                            file.setCretrId((String) session.getAttribute("id"));

                            resultList.add(file);

                            int resultFile = fileService.fileInsert(file);
                            if (resultFile != 1) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_FILE_INSERT);
                                transactionManager.rollback(status);
                                return rsModel.getModelAndView();
                            }
                        }
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", item);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrInsert");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            String checkString = "";
            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request, "banrSeq,perdYn,stDt,fnsDt,useYn");

            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            int banrSeq = CommonFnc.emptyCheckInt("banrSeq", request);
            int orderNum = CommonFnc.emptyCheckInt("orderNum", request);
            int plcySeq = CommonFnc.emptyCheckInt("plcySeq", request);

            String perdYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("perdYn", request));
            String stDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("stDt", request));
            String fnsDt = CommonFnc.strFilter(CommonFnc.emptyCheckString("fnsDt", request));
            String linkUri = CommonFnc.strFilter(CommonFnc.emptyCheckString("linkUri", request));
            String linkUrl = CommonFnc.strFilter(CommonFnc.emptyCheckString("linkUrl", request));
            String useTitleType = CommonFnc.strFilter(CommonFnc.emptyCheckString("useTitleType", request));
            String btnTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("btnTitle", request));
            String banrTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("banrTitle", request));
            String banrSubTitle = CommonFnc.strFilter(CommonFnc.emptyCheckString("banrSubTitle", request));
            String useYn = CommonFnc.strFilter(CommonFnc.emptyCheckString("useYn", request));

            item.setBanrSeq(banrSeq);
            item.setOrderNum(orderNum);
            item.setPlcySeq(plcySeq);
            item.setPerdYn(perdYn);
            item.setStDt(stDt);
            item.setFnsDt(fnsDt);

            // escape 문자들 원복하여 저장
            linkUri = CommonFnc.unescapeStr(linkUri);
            linkUrl = CommonFnc.unescapeStr(linkUrl);
            item.setLinkUri(linkUri);
            item.setLinkUrl(linkUrl);
            item.setUseTitleType(useTitleType);
            item.setBtnTitle(btnTitle);
            item.setBanrTitle(banrTitle);
            item.setBanrSubTitle(banrSubTitle);
            item.setUseYn(useYn);
            item.setAmdrId((String) session.getAttribute("id"));

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)) {
                item.setSvcSeq(userVO.getSvcSeq());
            } else {
                item.setSvcSeq(0);
            }

            try {
                int result = banrService.banrUpdate(item);
                if (result == 1) {
                    String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);
                    List<String> deleteFilePath = new ArrayList<String>();

                    if (delFileSeqs != "") {
                        String[] delFileSeq = delFileSeqs.split(",");

                        for (int i = 0; i < delFileSeq.length; i++) {
                            if (Integer.parseInt(delFileSeq[i]) == 0) {
                                break;
                            }
                            Fitem.setFileSeq(Integer.parseInt(delFileSeq[i]));
                            Fitem.setFileSe("BANR_IMG");
                            FileVO resultFitem = fileService.fileSeqInfo(Fitem);
                            String filePath = resultFitem.getFilePath();
                            if (fileService.fileSeqDelete(Fitem) == 1) {
                                deleteFilePath.add(filePath);
                            }
                        }
                    }

                    if (delFileSeqs != "") {
                        String pathBasic = fsResource.getPath();
                        FileVO file = new FileVO();

                        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
                        List<MultipartFile> files = multipartHttpServletRequest.getFiles("file");

                        Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
                        MultipartFile multipartFile = null;
                        List resultList = new ArrayList();

                        // 디레토리가 없다면 생성
                        File dir = new File(pathBasic);
                        if (!dir.isDirectory()) {
                            dir.mkdirs();
                        }
                        BanrVO resultPlcytem = null;
                        while (iterator.hasNext()) {
                            if (resultPlcytem == null) {
                                resultPlcytem = banrService.banrPlcyInfo(item);
                            }

                            if (resultPlcytem == null) {
                                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                                transactionManager.rollback(status);
                                return rsModel.getModelAndView();
                            }
                            int lmtFileSize = resultPlcytem.getLmtFileSize();
                            int contsSeq = item.getBanrSeq();
                            String fileSe = "BANR_IMG";
                            multipartFile = multipartHttpServletRequest.getFile(iterator.next());
                            if (multipartFile.isEmpty() == false) {
                                String flieName = multipartFile.getName();
                                String orginlFileNm = new String(multipartFile.getOriginalFilename().getBytes("UTF-8"),
                                        "UTF-8"); // 한글꺠짐
                                if (!FileApi.isValidFileName(orginlFileNm)) {
                                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                                    return rsModel.getModelAndView();
                                }
                                long filesize = multipartFile.getSize();

                                if (lmtFileSize != -1 && bytesToMeg(filesize) > lmtFileSize) {
                                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                    rsModel.setResultMessage(ResultModel.MESSAGE_OVER_FILE_SIZE);
                                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                                    transactionManager.rollback(status);
                                    return rsModel.getModelAndView();
                                }
                                // System.out.println("------------- file start -------------");
                                // System.out.println("name : "+flieName);
                                // System.out.println("filename : "+origName);
                                // System.out.println("size : "+filesize);
                                // System.out.println("-------------- file end --------------\n");

                                String ext = orginlFileNm.substring(orginlFileNm.lastIndexOf('.')); //
                                String now = new SimpleDateFormat("yyMMddHmsS").format(new Date()); // 현재시간
                                String path = "";
                                if (CommonFileUtil.isAllowUploadImgFileType(ext)) {
                                    path += "/banr/" + item.getBanrSeq();
                                } else {
                                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_FILE_INSERT);
                                    transactionManager.rollback(status);
                                    return rsModel.getModelAndView();
                                }

                                // 디레토리가 없다면 생성
                                File dir2 = new File(pathBasic + path);
                                if (!dir2.isDirectory()) {
                                    dir2.mkdirs();
                                }

                                String saveFileName = now + ext;
                                File serverFile = new File(pathBasic + path + File.separator + saveFileName);

                                multipartFile.transferTo(serverFile);
                                file.setOrginlFileNm(orginlFileNm);
                                file.setFileContsSeq(contsSeq);
                                file.setStreFileNm(saveFileName);
                                file.setFileSize(BigInteger.valueOf(filesize));
                                file.setFileDir(path + "/" + saveFileName);
                                file.setFileSe(fileSe);
                                file.setCretrId((String) session.getAttribute("id"));

                                resultList.add(file);

                                int resultFile = fileService.fileInsert(file);
                                if (resultFile != 1) {
                                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_FILE_INSERT);
                                    transactionManager.rollback(status);
                                    return rsModel.getModelAndView();
                                }
                            }
                        }
                    }
                    for (int i = 0; i < deleteFilePath.size(); i++) {
                        File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                        f.delete();
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", item);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrUpdate");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            String checkString = "";
            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request, "delSeqs");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            String delSeqs = CommonFnc.emptyCheckString("delSeqs", request);

            if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)) {
                item.setSvcSeq(userVO.getSvcSeq());
            } else {
                item.setSvcSeq(0);
            }

            item.setDelSeqs(delSeqs);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                item.setAmdrId((String) session.getAttribute("id"));
                int result = banrService.banrDeleteUpdate(item);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("banrDelete");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int banrSeq = CommonFnc.emptyCheckInt("banrSeq", request);
            int plcySeq = CommonFnc.emptyCheckInt("plcySeq", request);
            String perdSttus = CommonFnc.emptyCheckString("perdSttus", request);

            item.setPerdSttus(perdSttus);
            item.setSvcSeq(svcSeq);
            if (banrSeq != 0) {
                item.setBanrSeq(banrSeq);

                BanrVO resultItem = banrService.banrInfo(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {

                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                            && userVO.getSvcSeq() != resultItem.getSvcSeq())) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    // /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                    // String getType = CommonFnc.emptyCheckString("getType", request);
                    // if (getType.equals("")) {
                    // resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    // resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                    // }

                    /* 파일정보 */
                    FileVO fitem = new FileVO();
                    fitem.setFileContsSeq(resultItem.getBanrSeq());
                    fitem.setFileSe("BANR_IMG");
                    List<FileVO> fileImgList = fileService.fileList(fitem);

                    resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                    resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("banrImg", fileImgList);

                }

                rsModel.setResultType("banrInfo");
                return rsModel.getModelAndView();
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (banrSeq == 0 && CommonFnc.checkReqParameter(request, "banrSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                if (svcSeq != 0) {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != svcSeq)) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }
                }

                String searchType = request.getParameter("searchType") == null ? ""
                        : request.getParameter("searchType");

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    item.setTarget(searchField);
                    item.setKeyword(searchString);
                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = ((currentPage - 1) * limit);

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("BANR_SEQ")) {
                } else if (sidx.equals("SVC_NM")) {
                } else if (sidx.equals("TERR_NM")) {
                } else if (sidx.equals("CRETR_NM")) {
                } else if (sidx.equals("CRET_DT")) {
                } else if (sidx.equals("PERD_STTUS")) {
                } else if (sidx.equals("PERD_DT")) {
                } else {
                    sidx = "BANR_SEQ";
                }

                if (page < 0 && limit > 0) {
                    page = 0;
                    pageEnd = 0;
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);
                item.setPlcySeq(plcySeq);

                List<BanrVO> resultItem = banrService.banrList(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                        resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));
                        resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));

                        // 실제 서버에 파일이 존재하는지 여부 확인
                        resultItem.get(i).setIsImgFile("false");
                        boolean isExistFile = fileUtil.isExistFile(resultItem.get(i).getFilePath());
                        if (isExistFile) {
                            resultItem.get(i).setIsImgFile("true");
                        }
                    }

                    int totalCount = banrService.banrListTotalCount(item);

                    int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;

                    if (currentPage > totalPage) {
                        currentPage = 1;
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalPage);
                    rsModel.setResultType("banrList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || (MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe) && userVO.getSvcType().equals("ONLINE"));
    }

    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMeg(long bytes) {
        return bytes / MEGABYTE;
    }

    public static long MegToBytes(int meg) {
        return meg * 1024 * 1024;
    }

}
