package kt.com.im.cms.banr.vo;

import java.io.Serializable;
import java.util.List;

import kt.com.im.cms.file.vo.FileVO;

public class BanrVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5623993864090514123L;

    /** 배너 번호 */
    private int banrSeq;

    /** 배너 크기 */
    private String size;

    /** 배너 노출 순서 */
    private int orderNum;

    /** 배너 캠페인 명 */
    private String banrCampNm;

    /** 링크 유형 */
    private String linkType;

    /** 배너 링크 주소 (URI) */
    private String linkUri;

    /** 배너 링크 주소 */
    private String linkUrl;

    /** 배너 사용 타이틀 */
    private String useTitleType;

    /** 배너 버튼 명 */
    private String btnTitle;

    /** 배너 명 */
    private String banrTitle;

    /** 배너 부제목 */
    private String banrSubTitle;

    /** 배너 공개 여부 */
    private String banrHdn;

    /** 생성 일시 */
    private String cretDt;

    /** 생성자 아이디 */
    private String cretrId;

    /** 생성자 이름 */
    private String cretrNm;

    /** 수정 일시 */
    private String amdDt;

    /** 수정 회원 아이디 */
    private String amdrId;

    /** 서비스 번호 */
    private int svcSeq;

    /** 제약 서비스 번호 */
    private int cprtSvcSeq;

    /** 제약 서비스 명 */
    private String cprtSvcNm;

    /** 메뉴 URL */
    private String menuUrl;

    /** 노출 시작 날짜 */
    private String stDt;

    /** 노출 종료 날짜 */
    private String fnsDt;

    /** 노출 기간 설정 여부 (Y : 기간설정 , N 무제한 ) */
    private String perdYn;

    /** 노출 기간 */
    private String perdDt;

    /** 서비스 명 */
    private String svcNm;

    /** 배너 타입 */
    private String banrType;

    /** 배너 링크 */
    private String banrLink;

    /** 파일 번호 */
    private int fileSeq;

    /** 파일 이름 */
    private String orginlFileNm;

    /** 파일 경로 */
    private String filePath;

    /** 파일 크기 */
    private int fileSize;

    /** 제한 파일 크기 */
    private int lmtFileSize;

    /** 배너 정책 번호 */
    private int plcySeq;

    /** 배너 영역 명 */
    private String terrNm;

    /** 노출 상태 */
    private String perdSttus;

    /** 배너 가로 크기 */
    private int sizeWdth;

    /** 배너 세로 크기 */
    private int sizeHght;

    /** 파일 유형 */
    private String fileType;

    /** 사용 유무 */
    private String useYn;

    /** 배너 영역 URL */
    private String terrUrl;

    /** 삭제 시퀀스 번호들 */
    private String delSeqs;

    /** 삭제 여부 */
    private String delYn;

    /** 중복 체크 여부 */
    private String existChk;

    /** 이미지 파일 존재 여부 */
    private String isImgFile;

    /** 썸네일 파일 존재 여부 */
    private String isThumbImgFile;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 검색 유무 */
    private String searchConfirm;

    /** 검색 키워드 */
    private String keyword;

    /** 페이지 값 */
    private int offset;

    /** 리스트 개수 */
    private int limit;

    /** 정렬 필드 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 조건(카테고리) */
    private String target;

    /** 제휴 서비스 이미지 리스트 */
    private List<FileVO> cprtImgList;

    /** 제휴 서비스 이미지 (썸네일) 리스트 */
    private List<FileVO> cprtThmbImgList;

    public int getBanrSeq() {
        return banrSeq;
    }

    public void setBanrSeq(int banrSeq) {
        this.banrSeq = banrSeq;
    }

    public String getBanrTitle() {
        return banrTitle;
    }

    public void setBanrTitle(String banrTitle) {
        this.banrTitle = banrTitle;
    }

    public String getBanrSubTitle() {
        return banrSubTitle;
    }

    public void setBanrSubTitle(String banrSubTitle) {
        this.banrSubTitle = banrSubTitle;
    }

    public String getBanrHdn() {
        return banrHdn;
    }

    public void setBanrHdn(String banrHdn) {
        this.banrHdn = banrHdn;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getBanrType() {
        return banrType;
    }

    public void setBanrType(String banrType) {
        this.banrType = banrType;
    }

    public String getBanrLink() {
        return banrLink;
    }

    public void setBanrLink(String banrLink) {
        this.banrLink = banrLink;
    }

    public int getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(int fileSeq) {
        this.fileSeq = fileSeq;
    }

    public String getOrginlFileNm() {
        return orginlFileNm;
    }

    public void setOrginlFileNm(String orginlFileNm) {
        this.orginlFileNm = orginlFileNm;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getTerrNm() {
        return terrNm;
    }

    public void setTerrNm(String terrNm) {
        this.terrNm = terrNm;
    }

    public int getSizeWdth() {
        return sizeWdth;
    }

    public void setSizeWdth(int sizeWdth) {
        this.sizeWdth = sizeWdth;
    }

    public int getSizeHght() {
        return sizeHght;
    }

    public void setSizeHght(int sizeHght) {
        this.sizeHght = sizeHght;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public int getPlcySeq() {
        return plcySeq;
    }

    public void setPlcySeq(int plcySeq) {
        this.plcySeq = plcySeq;
    }

    public String getTerrUrl() {
        return terrUrl;
    }

    public void setTerrUrl(String terrUrl) {
        this.terrUrl = terrUrl;
    }

    public String getDelSeqs() {
        return delSeqs;
    }

    public void setDelSeqs(String delSeqs) {
        this.delSeqs = delSeqs;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getExistChk() {
        return existChk;
    }

    public void setExistChk(String existChk) {
        this.existChk = existChk;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getLmtFileSize() {
        return lmtFileSize;
    }

    public void setLmtFileSize(int lmtFileSize) {
        this.lmtFileSize = lmtFileSize;
    }

    public int getCprtSvcSeq() {
        return cprtSvcSeq;
    }

    public void setCprtSvcSeq(int cprtSvcSeq) {
        this.cprtSvcSeq = cprtSvcSeq;
    }

    public String getCprtSvcNm() {
        return cprtSvcNm;
    }

    public void setCprtSvcNm(String cprtSvcNm) {
        this.cprtSvcNm = cprtSvcNm;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getStDt() {
        return stDt;
    }

    public void setStDt(String stDt) {
        this.stDt = stDt;
    }

    public String getFnsDt() {
        return fnsDt;
    }

    public void setFnsDt(String fnsDt) {
        this.fnsDt = fnsDt;
    }

    public String getPerdYn() {
        return perdYn;
    }

    public void setPerdYn(String perdYn) {
        this.perdYn = perdYn;
    }

    public String getPerdSttus() {
        return perdSttus;
    }

    public void setPerdSttus(String perdSttus) {
        this.perdSttus = perdSttus;
    }

    public List<FileVO> getCprtImgList() {
        return cprtImgList;
    }

    public void setCprtImgList(List<FileVO> cprtImgList) {
        this.cprtImgList = cprtImgList;
    }

    public List<FileVO> getCprtThmbImgList() {
        return cprtThmbImgList;
    }

    public void setCprtThmbImgList(List<FileVO> cprtThmbImgList) {
        this.cprtThmbImgList = cprtThmbImgList;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public String getBanrCampNm() {
        return banrCampNm;
    }

    public void setBanrCampNm(String banrCampNm) {
        this.banrCampNm = banrCampNm;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getUseTitleType() {
        return useTitleType;
    }

    public void setUseTitleType(String useTitleType) {
        this.useTitleType = useTitleType;
    }

    public String getBtnTitle() {
        return btnTitle;
    }

    public void setBtnTitle(String btnTitle) {
        this.btnTitle = btnTitle;
    }

    public String getPerdDt() {
        return perdDt;
    }

    public void setPerdDt(String perdDt) {
        this.perdDt = perdDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getIsImgFile() {
        return isImgFile;
    }

    public void setIsImgFile(String isImgFile) {
        this.isImgFile = isImgFile;
    }

    public String getIsThumbImgFile() {
        return isThumbImgFile;
    }

    public void setIsThumbImgFile(String isThumbImgFile) {
        this.isThumbImgFile = isThumbImgFile;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getLinkUri() {
        return linkUri;
    }

    public void setLinkUri(String linkUri) {
        this.linkUri = linkUri;
    }

}
