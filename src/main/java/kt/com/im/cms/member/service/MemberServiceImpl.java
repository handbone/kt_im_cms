/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.member.service;

import java.util.List;

import javax.annotation.Resource;
import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kt.com.im.cms.common.util.ConfigProperty;
import kt.com.im.cms.common.util.MailSender;
import kt.com.im.cms.member.dao.MemberDAO;
import kt.com.im.cms.member.vo.IntegratedMemberVO;
import kt.com.im.cms.member.vo.LoginVO;
import kt.com.im.cms.member.vo.MemberHistoryVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.sms.web.SmsApi;

/**
 *
 * 회원 관리에 관한 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.16
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 16.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("MemberService")
public class MemberServiceImpl implements MemberService {

    @Resource(name = "MemberDAO")
    private MemberDAO memberDAO;

    @Autowired
    private ConfigProperty configProperty;

    /**
     * 로그인  정보 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public LoginVO getLoginInfo(LoginVO vo) throws Exception {
        return memberDAO.getLoginInfo(vo);
    }

    /**
     * 로그인 이력  추가
     * 
     * @param LoginVO
     * @return 처리 결과
     */
    @Override
    public int insertLoginHistory(LoginVO vo) throws Exception {
        return memberDAO.insertLoginHistory(vo);
    }

    /**
     * OTP 발급 시간 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public long getOtpCreateTime(LoginVO vo) throws Exception {
        return memberDAO.getOtpCreateTime(vo);
    }

    /**
     * OTP 발급 시간 저장
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updateOtpCreateTime(LoginVO vo) throws Exception {
        return memberDAO.updateOtpCreateTime(vo);
    }

    /**
     * 로그인 실패 회수 증가
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int increaseLoginFailCount(MemberVO vo) throws Exception {
        return memberDAO.increaseLoginFailCount(vo);
    }

    /**
     * 로그인 실패 회수 초기화
     * 
     * @param LoginVO
     * @return 처리 결과
     */
    @Override
    public int resetLoginFailCount(MemberVO vo) throws Exception {
        return memberDAO.resetLoginFailCount(vo);
    }

    /**
     * 멤버 목록 정보 조회
     * 
     * @param MemberVO
     * @return 조회 목록 결과
     */
    @Override
    public List<MemberVO> getMemberList(MemberVO vo) throws Exception {
        return memberDAO.getMemberList(vo);
    }

    /**
     * 멤버 목록 수 조회
     * 
     * @param MemberVO
     * @return 조회 목록 결과
     */
    @Override
    public int getMemberListTotalCount(MemberVO vo) throws Exception {
        return memberDAO.getMemberListTotalCount(vo);
    }

    /**
     * 멤버  정보 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public MemberVO getMember(MemberVO vo) throws Exception {
        return memberDAO.getMember(vo);
    }

    /**
     * 멤버 정보 추가
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int addMember(MemberVO vo) throws Exception {
        return memberDAO.addMember(vo);
    }

    /**
     * 멤버 정보 수정
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updateMember(MemberVO vo) throws Exception {
        return memberDAO.updateMember(vo);
    }

    /**
     * 멤버 정보 삭제
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int deleteMember(MemberVO vo) throws Exception {
        return memberDAO.deleteMember(vo);
    }

    /**
     * 멤버 정보 수정
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updateMemberStatus(MemberVO vo) throws Exception {
        return memberDAO.updateMemberStatus(vo);
    }

    /**
     * 멤버  비밀번호 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public String getPassword(MemberVO vo) throws Exception {
        return memberDAO.getPassword(vo);
    }

    /**
     * 멤버 비밀번호 수정
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updatePassword(MemberVO vo) throws Exception {
        return memberDAO.updatePassword(vo);
    }

    /**
     * 멤버 비밀번호 초기화
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int initPassword(MemberVO vo, boolean shouldUnblock) throws Exception {
        int resultCode = memberDAO.updatePassword(vo);

        if (shouldUnblock) {
            vo.setMbrSttus(MemberApi.MEMBER_STATUS_ENABLE);
            memberDAO.updateMemberStatus(vo);

            memberDAO.resetLoginFailCount(vo);
        }

        if (resultCode == 1) {
            MailSender mailSender = new MailSender(configProperty);
            String subject = (MemberApi.MEMBER_SECTION_CP.equals(vo.getMbrSe())) ? SmsApi.MSG_TITLE + " Store 비밀번호 찾기" : SmsApi.MSG_TITLE + " OMS 비밀번호 찾기";
            mailSender.addRecipient(vo.getMbrEmail());
            mailSender.setSubject(subject);
            mailSender.setContent(mailSender.getPasswordContent(vo));
            boolean sendResult = mailSender.send();
            if (!sendResult) {
                throw new MessagingException();
            }
        }

        return resultCode;
    }

    /**
     * 멤버 비밀번호 생성용 고유값 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public String getSalt(MemberVO vo) throws Exception {
        return memberDAO.getSalt(vo);
    }

    /**
     * 멤버 토큰 조회
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int searchMemberToken(MemberVO vo) throws Exception {
        return memberDAO.searchMemberToken(vo);
    }

    /**
     * 토큰의 멤버 정보 조회
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public MemberVO selectMemberInfoByToken(MemberVO vo) throws Exception {
        return memberDAO.selectMemberInfoByToken(vo);
    }

    /**
     * 멤버 토큰 수정
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updateMemberToken(MemberVO vo) throws Exception {
        return memberDAO.updateMemberToken(vo);
    }

    /**
     * 유저 토큰을 통한 아이디 조회
     *
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public MemberVO searchMbrId(MemberVO vo) throws Exception {
        return memberDAO.searchMbrId(vo);
    }

    /**
     * 멤버 수정 이력 목록 정보 조회
     * 
     * @param MemberHistoryVO
     * @return 조회 목록 결과
     */
    @Override
    public List<MemberHistoryVO> getMemberHistoryList(MemberHistoryVO vo) throws Exception {
        return memberDAO.getMemberHistoryList(vo);
    }

    /**
     * 비밀번호 변경 필수 여부
     * 
     * @param MemberHistoryVO
     * @return 조회 목록 결과
     */
    @Override
    public boolean mustChangePassword(MemberVO vo) throws Exception {
        return memberDAO.mustChangePassword(vo);
    }

    /**
     * 통합인증 멤버 정보 추가
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int insertIntegratedMember(IntegratedMemberVO vo) throws Exception {
        return memberDAO.insertIntegratedMember(vo);
    }

    /**
     * 통합인증 멤버 목록 정보 조회
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public List<IntegratedMemberVO> selectIntegratedMemberList(IntegratedMemberVO vo) throws Exception {
        return memberDAO.selectIntegratedMemberList(vo);
    }

    /**
     * 통합인증 멤버 목록 수 조회
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int selectIntegratedMemberListTotalCount(IntegratedMemberVO vo) throws Exception {
        return memberDAO.selectIntegratedMemberListTotalCount(vo);
    }

    /**
     * 통합인증 멤버 정보 조회
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public IntegratedMemberVO selectIntegratedMember(IntegratedMemberVO vo) throws Exception {
        return memberDAO.selectIntegratedMember(vo);
    }

    /**
     * 통합인증 멤버 UID 수정
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int updateIntegratedMemberUID(IntegratedMemberVO vo) throws Exception {
        return memberDAO.updateIntegratedMemberUID(vo);
    }

    /**
     * 통합인증 멤버 정보 수정
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int updateIntegratedMember(IntegratedMemberVO vo) throws Exception {
        return memberDAO.updateIntegratedMember(vo);
    }

    /**
     * 통합인증 멤버 정보 삭제
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int deleteIntegratedMember(IntegratedMemberVO vo) throws Exception {
        return memberDAO.deleteIntegratedMember(vo);
    }

    /**
     * 통합인증 멤버 로그인/로그아웃 이력 추가
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int insertIntegratedMbrLoginHst(IntegratedMemberVO vo) throws Exception {
        return memberDAO.insertIntegratedMbrLoginHst(vo);
    }

    /**
     * 약관 동의 여부 정보 추가
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int insertMemberTermsHst(IntegratedMemberVO vo) throws Exception {
        return memberDAO.insertMemberTermsHst(vo);
    }

    /**
     * 약관 동의 여부 정보 수정
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int updateMemberTermsHst(IntegratedMemberVO vo) throws Exception {
        return memberDAO.updateMemberTermsHst(vo);
    }

    /**
     * 회원 약관 동의 목록 조회
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public List<IntegratedMemberVO> searchMemberTermsHst(IntegratedMemberVO vo) throws Exception {
        return memberDAO.searchMemberTermsHst(vo);
    }

    /**
     * 회원 약관 동의 목록 합계
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public int searchMemberTermsHstTotalCount(MemberVO vo) throws Exception {
        return memberDAO.searchMemberTermsHstTotalCount(vo);
    }

    /**
     * 약관 동의 여부 목록 조회
     * 
     * @param IntegratedMemberVO
     * @return 처리 결과
     */
    @Override
    public List<IntegratedMemberVO> searchMemberDetailTermsHst(IntegratedMemberVO vo) throws Exception {
        return memberDAO.searchMemberDetailTermsHst(vo);
    }

    @Override
    public MemberVO getExistOtpCreateTime(MemberVO vo) {
        return memberDAO.getExistOtpCreateTime(vo);
    }

}
