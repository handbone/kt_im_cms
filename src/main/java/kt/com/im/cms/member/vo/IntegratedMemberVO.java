/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.member.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 통합 인증 회원 관리 VO 클래스 
 *
 * @author A2TEC
 * @since 2018.09.18
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 9. 18.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class IntegratedMemberVO extends MemberVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -8701143028413136521L;

    /** 통합 인증 회원 번호 */
    int intgrateAthnMbrSeq;

    /** 인증 회원 아이디 */
    String uid;

    /** 닉네임 */
    String nickName;

    /** 계정 인증 경로 (KT or SNS(KAKAO, Naver, etc)) */
    String accRoute;

    /** 가입 경로 (서비스 명) */
    String joinRoute;

    /** 계정 사용 여부 */
    String useYn;

    /** 개인 정보 제공 동의 여부 (약관들 전체 동의 여부) */
    String indvInfoAgreeYn;

    /** 로그인 구분 */
    String loginType;
    
    /** 로그인 로그아웃 일시 */
    String loginOutDt;

    /** 로그인 일시 */
    String loginDt;

    /** 로그아웃 일시 */
    String logoutDt;

    /** 등록 일시 */
    String regDt;

    /** 약관 동의 이력 번호 */
    int stpltAgreeSeq;

    /** 약관 번호 */
    int stpltSeq;

    /** 약관 버전 */
    String stpltVer;

    /** 약관 동의 여부 */
    String stpltAgreeYn;

    /** 약관 타입 */
    String stpltType;

    /** 약관 타입 명 */
    String stpltTypeNm;

    /** 약관 내용 */
    String stpltSbst;

    /** foreach 리스트 정보 */
    private List list;

    /** 해당 서비스 약관 전체 철회 여부 */
    String stpltAllDisAgreeYn;

    /** 장치 시리얼번호 */
    String devSerial;

    public int getIntgrateAthnMbrSeq() {
        return intgrateAthnMbrSeq;
    }

    public void setIntgrateAthnMbrSeq(int intgrateAthnMbrSeq) {
        this.intgrateAthnMbrSeq = intgrateAthnMbrSeq;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAccRoute() {
        return accRoute;
    }

    public void setAccRoute(String accRoute) {
        this.accRoute = accRoute;
    }

    public String getJoinRoute() {
        return joinRoute;
    }

    public void setJoinRoute(String joinRoute) {
        this.joinRoute = joinRoute;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getIndvInfoAgreeYn() {
        return indvInfoAgreeYn;
    }

    public void setIndvInfoAgreeYn(String indvInfoAgreeYn) {
        this.indvInfoAgreeYn = indvInfoAgreeYn;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getLoginOutDt() {
        return loginOutDt;
    }

    public void setLoginOutDt(String loginOutDt) {
        this.loginOutDt = loginOutDt;
    }

    public String getLoginDt() {
        return loginDt;
    }

    public void setLoginDt(String loginDt) {
        this.loginDt = loginDt;
    }

    public String getLogoutDt() {
        return logoutDt;
    }

    public void setLogoutDt(String logoutDt) {
        this.logoutDt = logoutDt;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public int getStpltAgreeSeq() {
        return stpltAgreeSeq;
    }

    public void setStpltAgreeSeq(int stpltAgreeSeq) {
        this.stpltAgreeSeq = stpltAgreeSeq;
    }

    public int getStpltSeq() {
        return stpltSeq;
    }

    public void setStpltSeq(int stpltSeq) {
        this.stpltSeq = stpltSeq;
    }

    public String getStpltVer() {
        return stpltVer;
    }

    public void setStpltVer(String stpltVer) {
        this.stpltVer = stpltVer;
    }

    public String getStpltAgreeYn() {
        return stpltAgreeYn;
    }

    public void setStpltAgreeYn(String stpltAgreeYn) {
        this.stpltAgreeYn = stpltAgreeYn;
    }

    public String getStpltType() {
        return stpltType;
    }

    public void setStpltType(String stpltType) {
        this.stpltType = stpltType;
    }

    public String getStpltTypeNm() {
        return stpltTypeNm;
    }

    public void setStpltTypeNm(String stpltTypeNm) {
        this.stpltTypeNm = stpltTypeNm;
    }

    public String getStpltSbst() {
        return stpltSbst;
    }

    public void setStpltSbst(String stpltSbst) {
        this.stpltSbst = stpltSbst;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getStpltAllDisAgreeYn() {
        return stpltAllDisAgreeYn;
    }

    public void setStpltAllDisAgreeYn(String stpltAllDisAgreeYn) {
        this.stpltAllDisAgreeYn = stpltAllDisAgreeYn;
    }

    public String getDevSerial() {
        return devSerial;
    }

    public void setDevSerial(String devSerial) {
        this.devSerial = devSerial;
    }

}
