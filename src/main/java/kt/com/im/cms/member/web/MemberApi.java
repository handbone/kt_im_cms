/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.member.web;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mashape.unirest.http.JsonNode;

import kt.com.im.cms.common.util.AesAlgorithm;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ConfigProperty;
import kt.com.im.cms.common.util.FormatCheckUtil;
import kt.com.im.cms.common.util.OTP;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.SHA256;
import kt.com.im.cms.common.util.SmsSender;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.IntegratedMemberVO;
import kt.com.im.cms.member.vo.LoginVO;
import kt.com.im.cms.member.vo.MemberHistoryVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.sms.service.SmsService;
import kt.com.im.cms.sms.vo.SmsVO;
import kt.com.im.cms.sms.web.SmsApi;

/**
 *
 * 회원 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.16
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 16.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class MemberApi {

    public final static String TEMPORARY_ID = "tempLoginId";
    public final static String MUST_CHANGE_PWD = "mustChangePassword";

    // 멤버 상태 코드
    public final static String MEMBER_STATUS_WAITING = "01";
    public final static String MEMBER_STATUS_ENABLE = "02";
    public final static String MEMBER_STATUS_DISABLE = "03";
    public final static String MEMBER_STATUS_BLOCKED = "04";

    // 멤버 권한 코드
    public final static String MEMBER_SECTION_MASTER = "01";
    public final static String MEMBER_SECTION_CMS = "02";
    public final static String MEMBER_SECTION_ACCEPTOR = "03";
    public final static String MEMBER_SECTION_SERVICE = "04";
    public final static String MEMBER_SECTION_STORE = "05";
    public final static String MEMBER_SECTION_CP = "06";

    private final static String[] ALLOW_SECTION_LIST_FOR_MASTER = { MEMBER_SECTION_MASTER, MEMBER_SECTION_CMS, MEMBER_SECTION_ACCEPTOR, MEMBER_SECTION_SERVICE, MEMBER_SECTION_STORE, MEMBER_SECTION_CP };
    private final static String[] ALLOW_SECTION_LIST_FOR_CMS = { MEMBER_SECTION_CMS, MEMBER_SECTION_ACCEPTOR, MEMBER_SECTION_SERVICE, MEMBER_SECTION_STORE, MEMBER_SECTION_CP };
    private final static String[] ALLOW_SECTION_LIST_FOR_SERVICE = { MEMBER_SECTION_SERVICE, MEMBER_SECTION_STORE };

    private final static String viewName = "../resources/api/member/memberProcess";

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "SmsService")
    private SmsService smsService;

    @Resource(name = "memberTransactionManager")
    private DataSourceTransactionManager txManager;

    @Autowired
    private ConfigProperty configProperty;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * 로그인 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/login")
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // 파라미터 유효성 여부 확인
            String invalidParams = Validator.validate(request, "id;pwd");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            LoginVO vo = new LoginVO();
            vo.setMbrId(request.getParameter("id"));

            String memberPwd = makeEncryptPassword(request.getParameter("pwd"), memberService.getSalt(vo));
            vo.setMbrPwd(memberPwd);

            String clientIp = CommonFnc.getClientIp(request);
            vo.setIpadr(clientIp);

            String loginYn = "N";
            LoginVO result = memberService.getLoginInfo(vo);
            if (result == null) {
                // 로그인 실패 회수 증가
                memberService.increaseLoginFailCount(vo);

                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, "아이디 비밀번호 오류");

                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }

            // 로그인 실패 회수 초기화
            memberService.resetLoginFailCount(vo);

            // 데이터 베이스에 등록 되어 있는 아이피와 동일 여부 체크 (등록 된 아이피가 없을 경우 확인 하지 않도록 예외추가)
            boolean hasIp = (result.getIpadr() != null && !result.getIpadr().isEmpty());
            boolean hasIp2 = (result.getIpadr2() != null && !result.getIpadr2().isEmpty());
            System.out.println("clientIp = " + clientIp + " Ipadr = " + result.getIpadr() + " Ipadr2 = " + result.getIpadr2());
            if (hasIp && !clientIp.equals(result.getIpadr())) {
                if (!hasIp2) {
                    // 로그인 이력 추가
                    this.insertLoginHistory(vo, loginYn, "아이피 차단");

                    rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
                    rsModel.setResultMessage("fail.login.blocked.ip");
                    return rsModel.getModelAndView();
                }

                if (!clientIp.equals(result.getIpadr2())) {
                    // 로그인 이력 추가
                    this.insertLoginHistory(vo, loginYn, "아이피 차단");

                    rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
                    rsModel.setResultMessage("fail.login.blocked.ip");
                    return rsModel.getModelAndView();
                }
            }

            // 계정 비활성화 여부 확인 (대기:01, 활성:02, 비활성:03, 잠금:04)
            boolean isUseable = MEMBER_STATUS_ENABLE.equals(result.getMbrSttus()) ? true : false;
            if (!isUseable) {
                String messageId = "";
                String loginDesc = "";
                if (MEMBER_STATUS_WAITING.equals(result.getMbrSttus())) {
                    messageId = "fail.login.waiting";
                    loginDesc = "계정 대기 상태";
                } else if (MEMBER_STATUS_DISABLE.equals(result.getMbrSttus())) {
                    messageId = "fail.login";
                    loginDesc = "계정 비활성화 상태";
                } else if (MEMBER_STATUS_BLOCKED.equals(result.getMbrSttus())) {
                    messageId = "fail.login.alert.block";
                    loginDesc = "계정 잠금 상태";
                }

                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, loginDesc);

                rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
                rsModel.setResultMessage(messageId);
                return rsModel.getModelAndView();
            }

            session.setAttribute(MUST_CHANGE_PWD, memberService.mustChangePassword(result));
            session.setAttribute(TEMPORARY_ID, result.getMbrId());
            return rsModel.getModelAndView();
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 로그아웃 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/logout", method = RequestMethod.POST)
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 임시 정보가 있는 경우(1차 인증 상태)에서는 로그아웃 시 임시 정보만 삭제 처리
        if (session.getAttribute(TEMPORARY_ID) != null) {
            session.removeAttribute(TEMPORARY_ID);
            if (session.getAttribute(MUST_CHANGE_PWD) != null) {
                session.removeAttribute(MUST_CHANGE_PWD);
            }
            return rsModel.getModelAndView();
        }

        // 로그아웃 이력 추가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO != null) {
            LoginVO vo = new LoginVO();
            vo.setMbrId(userVO.getMbrId());
            vo.setIpadr(userVO.getIpadr());
            this.insertLoginHistory(vo, "", "로그아웃");
        }

        session.invalidate();
        return rsModel.getModelAndView();
    }

    /**
     * 로그인 추가 인증 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/login/auth")
    public ModelAndView auth(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return this.verifyOtpCode(request, rsModel, session);
        }

        if (method.equals("GET")) {
            return this.getOtpCode(request, rsModel, session);
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView verifyOtpCode(HttpServletRequest request, ResultModel rsModel, HttpSession session) throws Exception {
        String loginYn = "N";

        // 1차 로그인 시 세션에 회원 아이디를 저장, 해당 값이 없는 경우 추가 인증 API에 접근 방지
        String id = (String) session.getAttribute(TEMPORARY_ID);
        if (id == null || id.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            rsModel.setResultMessage("session is disconnected");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrId(id);

        String clientIp = CommonFnc.getClientIp(request);
        vo.setIpadr(clientIp);

        String validateParams = "otpCode";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            rsModel.setResultMessage("invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO result = memberService.getLoginInfo(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            return rsModel.getModelAndView();
        }

        long otpCreateTime = memberService.getOtpCreateTime(vo);
        String secretKey = result.getMbrId() + result.getSalt();

        boolean didVerify = OTP.vertify(secretKey, otpCreateTime, request.getParameter("otpCode"));
        if (!didVerify) {
            // 로그인 이력 추가
            this.insertLoginHistory(vo, loginYn, "OTP 인증번호 오류");
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            return rsModel.getModelAndView();
        }

        if (MEMBER_SECTION_SERVICE.equals(result.getMbrSe())) {
            // 서비스 활성화 여부 확인
            String svcUseYn = result.getSvcUseYn();
            if (svcUseYn == null || svcUseYn.isEmpty() || svcUseYn.equals("N")) {
                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, "서비스 비활성화");
                rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                rsModel.setResultMessage("fail.login.unavailable.service");
                return rsModel.getModelAndView();
            }
            session.setAttribute("svcName", result.getSvcNm());
            session.setAttribute("svcType", result.getSvcType());
        } else if (MEMBER_SECTION_STORE.equals(result.getMbrSe())) {
            // 매장 활성화 여부 확인
            String storeUseYn = result.getStorUseYn();
            if (storeUseYn == null || storeUseYn.isEmpty() || storeUseYn.equals("N")) {
                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, "매장 비활성화");
                rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                rsModel.setResultMessage("fail.login.unavailable.store");
                return rsModel.getModelAndView();
            }
            session.setAttribute("storeName", result.getStorNm());
        } else if (MEMBER_SECTION_CP.equals(result.getMbrSe())) {
            // CP 활성화 여부 확인
            String cpUseYn = result.getCpUseYn();
            if (cpUseYn == null || cpUseYn.isEmpty() || cpUseYn.equals("N")) {
                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, "CP사 비활성화");
                rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                rsModel.setResultMessage("fail.login.unavailable.cp");
                return rsModel.getModelAndView();
            }
            session.setAttribute("cpName", result.getCpNm());
        }

        // 로그인 이력 추가
        loginYn = "Y";
        this.insertLoginHistory(vo, loginYn, "성공");

        session.setAttribute("seq", String.valueOf(result.getMbrSeq()));
        session.setAttribute("id", result.getMbrId());
        // 멤버 이름 마스킹 처리
        session.setAttribute("name", CommonFnc.getNameMask(result.getMbrNm()));
        // 멤버 구분(01:MASTER, 02:CMS 관리자, 03:검수자, 04:서비스 관리자, 05:ㅣ매장 관리자, 06:CP사 관계자)
        session.setAttribute("memberSec", result.getMbrSe());
        // 멤버 레벨(8:최고 관리자, 5:일반 관리자, 2:사용자)
        session.setAttribute("level", String.valueOf(result.getMbrLevel()));
        session.setAttribute("svcSeq", String.valueOf(result.getSvcSeq()));
        session.setAttribute("storSeq", String.valueOf(result.getStorSeq()));
        session.setAttribute("cpSeq", String.valueOf(result.getCpSeq()));
        session.setAttribute("otpCreateTime", String.valueOf(otpCreateTime));

        rsModel.setData("result", result);
        return rsModel.getModelAndView();
    }

    private ModelAndView getOtpCode(HttpServletRequest request, ResultModel rsModel, HttpSession session) throws Exception {
        rsModel.setViewName("jsonView");

        // 1차 로그인 시 세션에 회원 아이디를 저장, 해당 값이 없는 경우 추가 인증 API에 접근 방지
        String id = (String) session.getAttribute(TEMPORARY_ID);
        if (id == null || id.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            rsModel.setResultMessage("session is disconnected");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrId(id);

        LoginVO result = memberService.getLoginInfo(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            return rsModel.getModelAndView();
        }

        long otpCreateTime = new Date().getTime();
        vo.setOtpCretTime(otpCreateTime);
        memberService.updateOtpCreateTime(vo);

        String secretKey = result.getMbrId() + result.getSalt();
        String otpCode = OTP.create(secretKey, otpCreateTime);

        // TODO : jbwook 운영 서버 반영 시 usesOtpSms 변수 Y로 변경 필요
        boolean usesOtpSms = "Y".equalsIgnoreCase(configProperty.getProperty("use.otp.sms"));
        if (usesOtpSms) {
            SmsVO smsVo = new SmsVO();
            String[] phonList = result.getMbrMphonNo().split(";");
            smsVo.setConnectUrl(configProperty.getProperty("sms.connect.url"));
            smsVo.setDestName(result.getMbrNm());
            smsVo.setMsgBody("[" + SmsApi.MSG_TITLE + " OMS]\r\n본인인증번호는[" + otpCode + "]입니다. 정확히 입력해주세요.");

            for (int i = 0; i < phonList.length; i++) {
                SmsVO smsVoTemp = new SmsVO();
                smsVoTemp.setConnectUrl(smsVo.getConnectUrl());
                smsVoTemp.setDestName(smsVo.getDestName());
                smsVoTemp.setMsgBody(smsVo.getMsgBody());

                smsVoTemp.setDestPhone(phonList[i].replaceAll("-", "").trim());
                JsonNode sendResult = SmsSender.send(smsVoTemp);
                String resultCode = (String) sendResult.getObject().get("result_code");

                // SMS 발송 이력은 2차 개발에 포함되어 있어 신규 기능 Flag Enable시 동작되도록 처리
                boolean usesAutoSend = "Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.sms"));
                if (usesAutoSend) {
                    smsVoTemp.setMsgBody("[" + SmsApi.MSG_TITLE + " OMS]\r\n본인인증번호는[" + otpCode.substring(0, 3)
                            + "***]입니다. 정확히 입력해주세요.");
                    smsVoTemp.setSendDesc("[OTP] NO." + result.getMbrSeq());
                    smsVoTemp.setCretrId(result.getMbrId());

                    smsVoTemp.setResultCode(resultCode);
                    smsService.insertSentSmsInfo(smsVoTemp);

                }

                if (!"200".equals(resultCode)) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
            }

        } else {
            rsModel.setData("otpCode", otpCode);
        }
        return rsModel.getModelAndView();
    }

    /**
     * 아이디 중복 확인 관련  API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/check")
    public ModelAndView checkId(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            return checkId(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView checkId(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = Validator.validate(request, "id");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            rsModel.setResultMessage("invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        vo.setMbrId(request.getParameter("id"));

        MemberVO result = memberService.getMember(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
        }
        return rsModel.getModelAndView();
    }

    /**
     * 비밀번호 찾기 관련  API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/find")
    public ModelAndView find(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String invalidParams = Validator.validate(request, "type");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            String type = request.getParameter("type");
            if ("id".equals(type)) {
                return this.findId(request, rsModel);
            }
            if ("password".equals(type)) {
                return this.findPassword(request, rsModel);
            }

            if ("comfirmPassword".equals(type) || "comfirmEdit".equals(type)) {
                return this.comfirmPassword(request, rsModel, session);
            }
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView comfirmPassword(HttpServletRequest request, ResultModel rsModel, HttpSession session)
            throws Exception {
        String validateParams = "pwd";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        vo.setMbrId(userVO.getMbrId());

        String memberPwd;
        if (request.getParameter("type").equals("comfirmPassword")) {
            memberPwd = this.makeEncryptPassword(request.getParameter("pwd"), memberService.getSalt(vo));
        } else {
            memberPwd = request.getParameter("pwd");
        }
        vo.setMbrPwd(memberPwd);

        MemberVO result = memberService.getLoginInfo(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage("fail.login.alert.id");
            return rsModel.getModelAndView();
        }

        boolean isBlocked = MEMBER_STATUS_BLOCKED.equals(result.getMbrSttus());
        if (isBlocked) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
            rsModel.setResultMessage("fail.login.alert.block");
            return rsModel.getModelAndView();
        }
        rsModel.setResultType("cofirmPasswd");
        rsModel.setData("item", result);
        session.setAttribute("comfirmPwd", true);
        return rsModel.getModelAndView();
    }

    private ModelAndView findId(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "name;email";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrNm(request.getParameter("name"));
        vo.setMbrEmail(request.getParameter("email"));

        try {
            MemberVO result = memberService.getLoginInfo(vo);
            if (result == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage("fail.login.findId");
                return rsModel.getModelAndView();
            }

            // 아이디 뒷자리 별표 처리
            int asteriskLength = 3;
            if (result.getMbrId().length() <= asteriskLength) {
                asteriskLength = result.getMbrId().length() - 1;
            }

            String id = result.getMbrId().substring(0, result.getMbrId().length() - asteriskLength);
            for (int i = 0; i < asteriskLength; i++) {
                id += "*";
            }

            rsModel.setData("item", id);
            rsModel.setResultType("findInfo");

        } catch (Exception e) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findId.serverError");
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView findPassword(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "id;email";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrId(request.getParameter("id"));

        MemberVO result = memberService.getLoginInfo(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage("fail.login.alert.id");
            return rsModel.getModelAndView();
        }

        String mbrEmail = request.getParameter("email");
        if (!mbrEmail.equals(result.getMbrEmail())) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage("fail.login.alert.email");
            return rsModel.getModelAndView();
        }
        vo.setMbrEmail(mbrEmail);

        boolean isBlocked = MEMBER_STATUS_BLOCKED.equals(result.getMbrSttus());
        if (isBlocked) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
            rsModel.setResultMessage("fail.login.alert.block");
            return rsModel.getModelAndView();
        }

        String changedPassword = this.makeInitPassword();
        vo.setTempPwd(changedPassword);
        vo.setMbrPwd(makeEncryptPassword(changedPassword, result.getSalt()));
        vo.setAmdrId(request.getParameter("id"));
        vo.setMbrSe(result.getMbrSe());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.initPassword(vo, false);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_PROCESS);
            }
            txManager.commit(status);
        } catch (MessagingException e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findPassword.sendMail");
        } catch(Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findPassword.serverError");
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 비밀번호 초기화 관련  API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/init")
    public ModelAndView init(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return forceInitPassword(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView forceInitPassword(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        vo.setMbrSeq(Integer.parseInt(request.getParameter("seq")));

        MemberVO oldVo = memberService.getMember(vo);
        if (oldVo == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            return rsModel.getModelAndView();
        }

        // MASTER 계정 - 다른 MASTER 계정 이외에 모든 계정의 비밀번호 강제 초기화 가능
        // CMS 관리자 계정 - MASTER 계정, 다른 CMS 관리자 계정의 비밀번호 강제 초기화 가능
        if (MEMBER_SECTION_MASTER.equals(userVO.getMbrSe())) {
            boolean prevented = (MEMBER_SECTION_MASTER.equals(oldVo.getMbrSe()) && (userVO.getMbrSeq() != oldVo.getMbrSeq()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MEMBER_SECTION_CMS.equals(userVO.getMbrSe())) {
            boolean prevented = MEMBER_SECTION_MASTER.equals(oldVo.getMbrSe())
                    || (MEMBER_SECTION_CMS.equals(oldVo.getMbrSe()) && (userVO.getMbrSeq() != oldVo.getMbrSeq()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        vo.setMbrId(oldVo.getMbrId());
        String changedPassword = this.makeInitPassword();
        vo.setTempPwd(changedPassword);
        vo.setMbrPwd(makeEncryptPassword(changedPassword, oldVo.getSalt()));
        vo.setMbrEmail(oldVo.getMbrEmail());
        vo.setAmdrId(userVO.getMbrId());
        vo.setMbrSe(oldVo.getMbrSe());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.initPassword(vo, MEMBER_STATUS_BLOCKED.equals(oldVo.getMbrSttus()));
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_PROCESS);
            }
            txManager.commit(status);
        } catch (MessagingException e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findPassword.sendMail");
        } catch(Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findPassword.serverError");
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 회원 토큰 정보 조회 관련  API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/token")
    public ModelAndView token(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return getMemberToken(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getMemberToken(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "id;pwd";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrId(request.getParameter("id"));

        String memberPwd = MemberApi.makeEncryptPassword(request.getParameter("pwd"), memberService.getSalt(vo));
        vo.setMbrPwd(memberPwd);
        LoginVO memberResult = memberService.getLoginInfo(vo);
        if (memberResult == null) {
            rsModel.setNoData();
            rsModel.setResultMessage("Please check your id and password");
            return rsModel.getModelAndView();
        }

        if (!MemberApi.MEMBER_STATUS_ENABLE.equals(memberResult.getMbrSttus())) {
            rsModel.setNoData();
            rsModel.setResultMessage(ResultModel.MESSAGE_UNAVAILABLE_MEMBER);
            return rsModel.getModelAndView();
        }

        boolean prevented = MemberApi.MEMBER_SECTION_CP.equals(memberResult.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String token = memberResult.getMbrTokn();
        if (token == null) {
            token = this.generateToken(memberResult.getMbrId());
        }

        vo.setMbrTokn(token);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            // 토큰 유효기간 만료된 토큰에 대해서 갱신 처리
            memberService.updateMemberToken(vo);

            rsModel.setResultType("memberTokenInfo");
            rsModel.setData("id", request.getParameter("id"));
            rsModel.setData("token", token);
            txManager.commit(status);
        } catch (Exception e) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_PROCESS);
            txManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 회원 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member")
    public ModelAndView memberProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return addMember(request, rsModel, userVO);
        }

        if (method.equals("GET")) {
            boolean isList = "list".equals(request.getParameter("type"));
            if (isList) {
                return getMemberList(request, rsModel, userVO);
            }
            return getMember(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return updateMember(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return deleteMember(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addMember(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "se:{" + Validator.MIN_LENGTH + "=2};" + "id:{" + Validator.ID + "};" + "pwd1:{" + Validator.PWD + "};"
                + "pwd2:{" + Validator.PWD + "};" + "name:{" + Validator.NAME + "};" + "tel:{" + Validator.TEL_NO + "};"
                + "phone:{" + Validator.PHONE_NO + "};" + "email:{" + Validator.EMAIL + "}";

        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (FormatCheckUtil.containsIdInPassword(request.getParameter("id"), request.getParameter("pwd1"))) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(pwd1)");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        vo.setMbrSe(request.getParameter("se"));
        // CMS 관리자 계정의 경우 마스터 계정은 등록 불가
        if (MEMBER_SECTION_CMS.equals(userVO.getMbrSe())) {
            if (MEMBER_SECTION_MASTER.equals(vo.getMbrSe())) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        // 아이디 중복 여부 체크
        MemberVO searchVO = new MemberVO();
        searchVO.setMbrId(request.getParameter("id"));
        MemberVO result = memberService.getMember(searchVO);
        boolean isExisted = (result != null);
        if (isExisted) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage("login.unavailableId.msg");
            return rsModel.getModelAndView();
        }

        // CP사 관리자 이외의 회원 유형의 경우 IP주소의 필수 여부를 확인
        if (!MEMBER_SECTION_CP.equals(vo.getMbrSe())) {
            validateParams = "ipadr:{" + Validator.IP_ADDRESS + "}";
            invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            if (request.getParameter("ipadr2") != null && !request.getParameter("ipadr2").isEmpty()) {
                validateParams = "ipadr2:{" + Validator.IP_ADDRESS + "}";
                invalidParams = Validator.validate(request, validateParams);
                if (!invalidParams.isEmpty()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                    return rsModel.getModelAndView();
                }
            }

            if (request.getParameter("ipadr").equals(request.getParameter("ipadr2"))) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(ipadr2 can't be the same ipadr)");
                return rsModel.getModelAndView();
            }
        }

        String pwd1 = request.getParameter("pwd1");
        String pwd2 = request.getParameter("pwd2");
        if (!pwd1.equals(pwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(pwd2 didn't same with pwd1)");
            return rsModel.getModelAndView();
        }

        // 이메일 중복 여부 체크
        vo.setMbrEmail(request.getParameter("email"));
        result = memberService.getMember(vo);
        isExisted = (result != null);
        if (isExisted) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage("login.unavailableEmail.msg");
            return rsModel.getModelAndView();
        }

        vo.setMbrId(request.getParameter("id"));
        vo.setMbrNm(request.getParameter("name"));
        vo.setMbrTelNo(request.getParameter("tel"));
        vo.setMbrMphonNo(request.getParameter("phone"));

        String salt = getGeneratedSalt();
        vo.setSalt(salt);
        vo.setMbrPwd(makeEncryptPassword(pwd1, salt));

        vo.setIpadr(request.getParameter("ipadr"));
        vo.setIpadr2(request.getParameter("ipadr2"));

        int memberLevel = 2;

        String memberStatus = MEMBER_STATUS_WAITING;
        if (MEMBER_SECTION_MASTER.equals(vo.getMbrSe())) {
            memberLevel = 8;
            memberStatus = MEMBER_STATUS_ENABLE;
        } else if (MEMBER_SECTION_CMS.equals(vo.getMbrSe())) {
            memberLevel = 5;
            memberStatus = MEMBER_STATUS_ENABLE;
        } else if (MEMBER_SECTION_ACCEPTOR.equals(vo.getMbrSe())) {
            // nothing
            memberStatus = MEMBER_STATUS_ENABLE;
        } else if (MEMBER_SECTION_SERVICE.equals(vo.getMbrSe())) {
            validateParams = "svcSeq:{" + Validator.NUMBER + "}";
            invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            memberLevel = 5;
            memberStatus = MEMBER_STATUS_ENABLE;
            vo.setSvcSeq(Integer.parseInt(request.getParameter("svcSeq")));
        } else if (MEMBER_SECTION_STORE.equals(vo.getMbrSe())) {
            validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "}";
            invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            memberStatus = MEMBER_STATUS_ENABLE;
            vo.setSvcSeq(Integer.parseInt(request.getParameter("svcSeq")));
            vo.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));
        } else if (MEMBER_SECTION_CP.equals(vo.getMbrSe())) {
            validateParams = "cpSeq:{" + Validator.NUMBER + "}";
            invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            vo.setCpSeq(Integer.parseInt(request.getParameter("cpSeq")));
        }

        String cretrId = userVO.getMbrId();
        vo.setCretrId(cretrId);
        vo.setMbrLevel(memberLevel);
        vo.setMbrSttus(memberStatus);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.addMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            } else {
                rsModel.setData("mbrSeq", vo.getMbrSeq());
                rsModel.setResultType("insertMember");
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getMemberList(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        MemberVO vo = new MemberVO();
        // Master, CMS 접근 가능, Service 관리자는 동일 서비스의 계정들만 접근 가능
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator && !MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            return rsModel.getModelAndView();
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));

        String status = request.getParameter("status");
        if (status != null && !status.isEmpty()) {
            vo.setStatus(status);
            String[] statusList = status.split(",");
            vo.setStatusList(statusList);
        }

        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));

        // 관리자 권한에 따른 멤버 조회 대상 변경
        if (MEMBER_SECTION_MASTER.equals(userVO.getMbrSe())) {
            vo.setSectionList(ALLOW_SECTION_LIST_FOR_MASTER);
        } else if (MEMBER_SECTION_CMS.equals(userVO.getMbrSe())) {
            vo.setSectionList(ALLOW_SECTION_LIST_FOR_CMS);
        } else if (MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            vo.setSectionList(ALLOW_SECTION_LIST_FOR_SERVICE);
            vo.setSvcSeq(userVO.getSvcSeq());
        }

        int totalCount = memberService.getMemberListTotalCount(vo);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = (currentPage - 1) * rows;
        vo.setOffset(offset);
        vo.setLimit(rows);

        List<MemberVO> result = memberService.getMemberList(vo);
        if (result == null || result.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        /* 개인정보 마스킹 */
        for (int i = 0; i < result.size(); i++) {
            result.get(i).setMbrId(CommonFnc.getIdMask(result.get(i).getMbrId()));
            result.get(i).setMbrNm(CommonFnc.getNameMask(result.get(i).getMbrNm()));
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setData("item", result);
        rsModel.setResultType("memberList");

        return rsModel.getModelAndView();
    }

    private ModelAndView getMember(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt(request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean isAdministrator = this.isAdministrator(userVO);
        boolean prevented = !(isAdministrator || vo.getMbrSeq() == userVO.getMbrSeq() || MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        MemberVO result = memberService.getMember(vo);
        if (result == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        String getType = CommonFnc.emptyCheckString("getType", request);
        String getPath = CommonFnc.emptyCheckString("path", request);
        boolean isEdit = getType.equals("edit");
        boolean isMypage = getPath.equals("mypage");

        // MASTER 계정 - Master 계정들을 모두 볼 수 있으나 수정은 불가
        // CMS 관리자 계정 - MASTER 계정 접근 불가. CMS 이하 모든 권한에 접근 가능하지만 CMS 계정은 수정 불가
        // 서비스 관리자 계정 - 동일 서비스에 포함되는 서비스 관리자, 매장 관리자 확인 가능. 서비스 관리자 계정은 수정 불가
        // 접속 경로가 mypage일 경우 본인 계정 수정 가능
        if (MEMBER_SECTION_MASTER.equals(userVO.getMbrSe())) {
            prevented = (!isMypage && isEdit && MEMBER_SECTION_MASTER.equals(result.getMbrSe()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MEMBER_SECTION_CMS.equals(userVO.getMbrSe())) {
            prevented = (MEMBER_SECTION_MASTER.equals(result.getMbrSe()) || (!isMypage && isEdit && MEMBER_SECTION_CMS.equals(result.getMbrSe())));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            prevented = MEMBER_SECTION_MASTER.equals(result.getMbrSe())
                    || MEMBER_SECTION_CMS.equals(result.getMbrSe())
                    || (!isMypage && isEdit && MEMBER_SECTION_SERVICE.equals(result.getMbrSe()))
                    || result.getSvcSeq() != userVO.getSvcSeq();
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /* 개인정보 마스킹 - edit일 경우 마스킹 없이 데이터 전달 */
        if (isEdit && userVO.getMbrId().equals(result.getMbrId())) {
            rsModel.setData("item", result);
        } else {
            result.setMbrNm(CommonFnc.getNameMask(result.getMbrNm()));
            result.setMbrId(CommonFnc.getIdMask(result.getMbrId()));
            result.setMbrTelNo(CommonFnc.getTelnoMask(result.getMbrTelNo()));
            result.setMbrMphonNo(CommonFnc.getTelnoMask(result.getMbrMphonNo()));
            result.setMbrEmail(CommonFnc.getEmailMask(result.getMbrEmail()));
            result.setIpadr(CommonFnc.getIpAddrMask(result.getIpadr()));
            result.setIpadr2(CommonFnc.getIpAddrMask(result.getIpadr2()));
            result.setCretrId(CommonFnc.getIdMask(result.getCretrId()));
            result.setAmdrId(CommonFnc.getIdMask(result.getAmdrId()));
            result.setApvrId(CommonFnc.getIdMask(result.getApvrId()));

            rsModel.setData("item", result);
        }

        rsModel.setResultType("memberInfo");
        return rsModel.getModelAndView();
    }

    private ModelAndView updateMember(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt((String)request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean isAdministrator = this.isAdministrator(userVO);
        boolean prevented = !(isAdministrator || vo.getMbrSeq() == userVO.getMbrSeq() || MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        vo = memberService.getMember(vo);
        if (vo == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_NO_DATA);
            return rsModel.getModelAndView();
        }

        // MASTER 계정 - 다른 MASTER 계정 이외에 모든 계정 수정 가능
        // CMS 관리자 계정 - MASTER 계정, 다른 CMS 관리자 계정 이외에 모든 계정 수정 가능
        // 서비스 관리자 계정 - 동일 서비스의 매장 관리자 계정만 수정 가능
        if (MEMBER_SECTION_MASTER.equals(userVO.getMbrSe())) {
            prevented = (MEMBER_SECTION_MASTER.equals(vo.getMbrSe()) && (userVO.getMbrSeq() != vo.getMbrSeq()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MEMBER_SECTION_CMS.equals(userVO.getMbrSe())) {
            prevented = MEMBER_SECTION_MASTER.equals(vo.getMbrSe())
                    || (MEMBER_SECTION_CMS.equals(vo.getMbrSe()) && (userVO.getMbrSeq() != vo.getMbrSeq()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            prevented = MEMBER_SECTION_MASTER.equals(vo.getMbrSe())
                    || MEMBER_SECTION_CMS.equals(vo.getMbrSe())
                    || (MEMBER_SECTION_SERVICE.equals(vo.getMbrSe()) && userVO.getMbrSeq() != vo.getMbrSeq())
                    || (vo.getSvcSeq() != userVO.getSvcSeq());
            System.out.println(">> [SECTION SERVICE] prevented : " + prevented);
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        String name = CommonFnc.emptyCheckString("name", request);
        String tel = CommonFnc.emptyCheckString("tel", request);
        String phone = CommonFnc.emptyCheckString("phone", request);
        String email = CommonFnc.emptyCheckString("email", request);
        String delphone = CommonFnc.emptyCheckString("delphone", request);

        /* 개인정보 마스킹 처리된 데이터인지 확인하여 validator 구성 */
        validateParams = "se:{" + Validator.MIN_LENGTH + "=2};";
        boolean chgName = name.contains("*");
        boolean chgTel = tel.contains("*");

        boolean chgPhone = true;
        String[] phonList = phone.split(";");

        for (int i = 0; i < phonList.length; i++) {
            if (!phonList[i].contains("*")) {
                chgPhone = false;
                break;
            }
        }

        if (!delphone.equals("")) {
            chgPhone = false;
        }

        boolean chgEmail = email.contains("*");
        String mailChgYn = CommonFnc.emptyCheckString("mailChgYn", request);

        if (!chgName) {
            validateParams += "name:{" + Validator.NAME + "};";
        }

        if (!chgTel) {
            validateParams += "tel:{" + Validator.TEL_NO + "};";
        }

        if (!chgPhone || !delphone.equals("")) {
            validateParams += "phone:{" + Validator.PHONE_NO + "};";
        }

        if (!chgEmail) {
            validateParams += "email:{" + Validator.EMAIL + "}";
        }

        invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        // 이메일 중복 여부 체크
        if (!chgEmail) {
            MemberVO searchVO = new MemberVO();
            searchVO.setMbrEmail(request.getParameter("email"));
            MemberVO result = memberService.getMember(searchVO);
            boolean isExisted = (result != null) && !vo.getMbrId().equals(result.getMbrId());
            if (isExisted) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                return rsModel.getModelAndView();
            }
        }

        vo.setMbrSe(request.getParameter("se"));

        String ipadr = CommonFnc.emptyCheckString("ipadr", request);
        String ipadr2 = CommonFnc.emptyCheckString("ipadr2", request);
        boolean chgIpadr = ipadr.contains("*");
        boolean chgIpadr2 = ipadr2.contains("*");

        // CP사 관계자 계정 이외에는 IP 주소가 필수 입력 항목
        if (!MEMBER_SECTION_CP.equals(vo.getMbrSe())) {
            if (!chgIpadr) {
                validateParams = "ipadr:{" + Validator.IP_ADDRESS + "}";
                invalidParams = Validator.validate(request, validateParams);
                if (!invalidParams.isEmpty()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                    return rsModel.getModelAndView();
                }
            }

            if (!ipadr2.isEmpty() && !chgIpadr2) {
                validateParams = "ipadr2:{" + Validator.IP_ADDRESS + "}";
                invalidParams = Validator.validate(request, validateParams);
                if (!invalidParams.isEmpty()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                    return rsModel.getModelAndView();
                }
            }

            if (ipadr.equals(ipadr2)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(ipadr2 can't be the same ipadr)");
                return rsModel.getModelAndView();
            }
        }

        /* 개인정보 마스킹으로 인해 변경된 정보만 확인하여 저장 */
        if (!chgName) {
            vo.setMbrNm(request.getParameter("name"));
        }
        if (!chgTel) {
            vo.setMbrTelNo(request.getParameter("tel"));
        }
        if (!chgPhone || !delphone.equals("")) {
            vo.setMbrMphonNos(request.getParameter("phone").split(";"));
            vo.setDelphone(delphone);
        }

        if (!chgEmail) {
            vo.setMbrEmail(request.getParameter("email"));
        } else if (mailChgYn.equals("Y") && chgEmail) {
            String[] oriEmail = vo.getMbrEmail().split("@");
            String[] reEmail = request.getParameter("email").split("@");

            vo.setMbrEmail(oriEmail[0] + "@" + reEmail[1]);
        }
        if (!chgIpadr) {
            vo.setIpadr(request.getParameter("ipadr"));
        }
        if (!chgIpadr2) {
            vo.setIpadr2(request.getParameter("ipadr2"));
        }

        int memberLevel = 2;
        int svcSeq = 0;
        int storSeq = 0;
        int cpSeq = 0;
        if (MEMBER_SECTION_MASTER.equals(vo.getMbrSe())) {
            memberLevel = 8;
        } else if (MEMBER_SECTION_CMS.equals(vo.getMbrSe())) {
            memberLevel = 5;
        } else if (MEMBER_SECTION_ACCEPTOR.equals(vo.getMbrSe())) {
            // nothing
        } else if (MEMBER_SECTION_SERVICE.equals(vo.getMbrSe())) {
            validateParams = "svcSeq:{" + Validator.NUMBER + "}";
            invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            memberLevel = 5;
            svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
        } else if (MEMBER_SECTION_STORE.equals(vo.getMbrSe())) {
            validateParams = "svcSeq:{" + Validator.NUMBER + "};" + "storSeq:{" + Validator.NUMBER + "}";
            invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            svcSeq = Integer.parseInt(request.getParameter("svcSeq"));
            storSeq = Integer.parseInt(request.getParameter("storSeq"));
        } else if (MEMBER_SECTION_CP.equals(vo.getMbrSe())) {
            validateParams = "cpSeq:{" + Validator.NUMBER + "}";
            invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            cpSeq = Integer.parseInt(request.getParameter("cpSeq"));
        }

        vo.setAmdrId(userVO.getMbrId());
        vo.setMbrLevel(memberLevel);

        vo.setSvcSeq(svcSeq);
        vo.setStorSeq(storSeq);
        vo.setCpSeq(cpSeq);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updateMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            } else {
                // 마이페이지에서 사용자명 바꾼 경우 세션 내 저장된 name도 업데이트 처리
                if (request.getSession().getAttribute("name") != null) {
                    if (vo.getMbrSeq() == userVO.getMbrSeq()) {
                        if (!vo.getMbrNm().equals(userVO.getMbrNm())) {
                            request.getSession().setAttribute("name", vo.getMbrNm());
                        }
                    }
                }
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView deleteMember(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt((String)request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean isAdministrator = this.isAdministrator(userVO);
        boolean prevented = !(isAdministrator || vo.getMbrSeq() == userVO.getMbrSeq() || MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        // MASTER 계정 - 다른 MASTER 계정 이외에 모두 삭제 가능
        // CMS 관리자 계정 - MASTER 계정, 다른 CMS 관리자 계정 이외에 모두 삭제 가능
        // 서비스 관리자 계정 - 동일 서비스의 매장 관리자 계정만 삭제 가능
        if (!this.isAvailable(memberSeq, userVO, rsModel)) {
            return rsModel.getModelAndView();
        }

        boolean shouldDeleteFromDataBase = "complete".equals(request.getParameter("type"));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = 0;
            if (shouldDeleteFromDataBase) {
                resultCode = memberService.deleteMember(vo);
            } else {
                vo.setAmdrId(userVO.getMbrId());
                vo.setMbrSttus(MEMBER_STATUS_DISABLE);
                resultCode = memberService.updateMemberStatus(vo);
            }
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_DELETE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_DELETE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 비밀번호 변경 관련  API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/password")
    public ModelAndView password(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null && !this.mustChangePassword(session)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            if (this.mustChangePassword(session)) {
                return updateOldestPassword(request, rsModel, session);
            }
            return updatePassword(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView updatePassword(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt((String)request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean prevented = vo.getMbrSeq() != userVO.getMbrSeq();
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        validateParams = "oldPwd;" + "pwd1:{" + Validator.PWD + "};" + "pwd2:{" + Validator.PWD + "}";
        invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String salt = memberService.getSalt(vo);
        String oldPwd1 = makeEncryptPassword(request.getParameter("oldPwd"), salt);
        String oldPwd2 = memberService.getPassword(vo);
        if (!oldPwd1.equals(oldPwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.member.notMatched.oldPassword");
            return rsModel.getModelAndView();
        }

        String newPwd1 = request.getParameter("pwd1");
        String newPwd2 = request.getParameter("pwd2");
        if (!newPwd1.equals(newPwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.member.notMatched.newPassword");
            return rsModel.getModelAndView();
        }

        newPwd1 = makeEncryptPassword(newPwd1, salt);
        boolean notChanged = (oldPwd2.equals(newPwd1));
        if (notChanged) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.member.matched.oldPassword");
            return rsModel.getModelAndView();
        }

        MemberVO memberVO = memberService.getMember(vo);
        if (memberVO != null) {
            if (FormatCheckUtil.containsIdInPassword(memberVO.getMbrId(), request.getParameter("pwd1"))) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(pwd1)");
                return rsModel.getModelAndView();
            }
        }

        vo.setMbrPwd(newPwd1);
        vo.setAmdrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updatePassword(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView updateOldestPassword(HttpServletRequest request, ResultModel rsModel, HttpSession session) throws Exception {
        if (session.getAttribute(TEMPORARY_ID) == null || session.getAttribute(MUST_CHANGE_PWD) == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        String mbrId = (String) session.getAttribute(TEMPORARY_ID);
        vo.setMbrId(mbrId);

        String validateParams = "oldPwd;" + "pwd1:{" + Validator.PWD + "};" + "pwd2:{" + Validator.PWD + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (FormatCheckUtil.containsIdInPassword(mbrId, request.getParameter("pwd1"))) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(pwd1)");
            return rsModel.getModelAndView();
        }

        String salt = memberService.getSalt(vo);
        String oldPwd1 = makeEncryptPassword(request.getParameter("oldPwd"), salt);
        String oldPwd2 = memberService.getPassword(vo);
        if (!oldPwd1.equals(oldPwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.login.change.notMatched.oldPassword");
            return rsModel.getModelAndView();
        }

        String newPwd1 = request.getParameter("pwd1");
        String newPwd2 = request.getParameter("pwd2");
        if (!newPwd1.equals(newPwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.login.change.notMatched.newPassword");
            return rsModel.getModelAndView();
        }

        newPwd1 = makeEncryptPassword(newPwd1, salt);
        boolean notChanged = (oldPwd2.equals(newPwd1));
        if (notChanged) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.login.change.matched.oldPassword");
            return rsModel.getModelAndView();
        }

        vo.setMbrPwd(newPwd1);
        vo.setAmdrId(mbrId);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updatePassword(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 회원 상태 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/status")
    public ModelAndView memberStatus(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("PUT")) {
            return updateMemberStatus(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView updateMemberStatus(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt((String)request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean isAdministrator = this.isAdministrator(userVO);
        boolean prevented = !(isAdministrator ||  MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        MemberVO oldVo = memberService.getMember(vo);
        if (oldVo == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        // MASTER 계정 - 다른 MASTER 계정 이외에 모든 계정의 상태 변경 가능
        // CMS 관리자 계정 - MASTER 계정, 다른 CMS 관리자 계정 이외에 모든 계정 상태 변경 가능
        // 서비스 관리자 계정 - 동일 서비스의 매장 관리자 계정만 상태 변경 가능
        if (MEMBER_SECTION_MASTER.equals(userVO.getMbrSe())) {
            prevented = (MEMBER_SECTION_MASTER.equals(oldVo.getMbrSe()) && (userVO.getMbrSeq() != oldVo.getMbrSeq()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MEMBER_SECTION_CMS.equals(userVO.getMbrSe())) {
            prevented = MEMBER_SECTION_MASTER.equals(oldVo.getMbrSe())
                    || (MEMBER_SECTION_CMS.equals(oldVo.getMbrSe()) && (userVO.getMbrSeq() != oldVo.getMbrSeq()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        } else if (MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            prevented = MEMBER_SECTION_MASTER.equals(oldVo.getMbrSe())
                    || MEMBER_SECTION_CMS.equals(oldVo.getMbrSe())
                    || (MEMBER_SECTION_SERVICE.equals(oldVo.getMbrSe()) && userVO.getMbrSeq() != oldVo.getMbrSeq())
                    || (oldVo.getSvcSeq() != userVO.getSvcSeq());
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        validateParams = "sttus:{" + Validator.MIN_LENGTH + "=2}";
        invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (MEMBER_STATUS_WAITING.equals(oldVo.getMbrSttus())) {
            // 대기 상태 가입 승인 시에는 증빙 서류 제출 여부 필수
            invalidParams = Validator.validate(request, "docSubmYn");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            vo.setDocSubmYn(request.getParameter("docSubmYn"));
            vo.setApvrId(userVO.getMbrId());
        }

        vo.setAmdrId(userVO.getMbrId());
        vo.setMbrSttus(request.getParameter("sttus"));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updateMemberStatus(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 회원 수정 이력 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/history")
    public ModelAndView memberHistory(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            return getMemberHistory(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getMemberHistory(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        MemberHistoryVO vo = new MemberHistoryVO();
        int memberSeq = Integer.parseInt(request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean isAdministrator = this.isAdministrator(userVO);
        boolean prevented = !(isAdministrator || vo.getMbrSeq() == userVO.getMbrSeq());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        // MASTER 계정 - 다른 MASTER 계정 이외에 모든 계정의 변경 내역 조회 가능
        // CMS 관리자 계정 - MASTER 계정, 다른 CMS 관리자 계정 이외에 변경 내역 조회 가능
        // 서비스 관리자 계정 - 매장 관리자 계정만 변경 내역 조회 가능
        if (!this.isAvailable(memberSeq, userVO, rsModel)) {
            return rsModel.getModelAndView();
        }

        List<MemberHistoryVO> resultList = memberService.getMemberHistoryList(vo);
        if (resultList == null || resultList.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("item", resultList);
        rsModel.setResultType("memberHistoryList");
        return rsModel.getModelAndView();
    }

    /**
     * 통합인증 회원 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/integrate")
    public ModelAndView integratedMemberProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        // 통합 인증에 따른 계정 정보 저장으로 session 확인하지 않음.
        if (method.equals("POST")) {
            return this.addIntegratedMember(request, rsModel);
        }

        // 통합 인증에 따른 계정 정보 삭제로 session 확인하지 않음.
        if (method.equals("DELETE")) {
            return this.deleteIntegratedMember(request, rsModel, session);
        }

        if (method.equals("GET")) {
            boolean isList = "list".equals(request.getParameter("type"));
            if (isList) {
                return this.getIntegratedMemberList(request, rsModel, session);
            }
            return this.getIntegratedMember(request, rsModel, session);
        }

        if (method.equals("PUT")) {
            return this.updateIntegratedMember(request, rsModel, session);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addIntegratedMember(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,id,svcSeq,accRoute,indvInfoAgreeYn");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        // id 중복 여부 체크
        String id = CommonFnc.emptyCheckString("id", request);
        if (id != null && !id.isEmpty()) {
            IntegratedMemberVO searchVO = new IntegratedMemberVO();
            searchVO.setUid(request.getParameter("id"));
            searchVO.setSvcSeq(svcSeq);
            MemberVO result = memberService.selectIntegratedMember(searchVO);
            boolean isExisted = (result != null);
            if (isExisted) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("login.unavailableId.msg");
                return rsModel.getModelAndView();
            }
        }

        String name = CommonFnc.emptyCheckString("name", request);
        // 인증회원이름 길이 값 체크
        if (name != null && !name.isEmpty()) {
            String validateOptionalParams = "name:{" + Validator.MAX_LENGTH + "=15}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        String accRoute = CommonFnc.emptyCheckString("accRoute", request);
        // accRoute(계정 인증 경로) 길이 값 체크
        if (accRoute != null && !accRoute.isEmpty()) {
            String validateOptionalParams = "accRoute:{" + Validator.MAX_LENGTH + "=100}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        // nickName(닉네임) 길이 값 체크
        String nickName = CommonFnc.emptyCheckString("nickName", request);
        if (nickName != null && !nickName.isEmpty()) {
            String validateOptionalParams = "nickName:{" + Validator.MAX_LENGTH + "=100}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        // 이메일 중복 여부 체크
        String email = CommonFnc.emptyCheckString("email", request);
        if (email != null && !email.isEmpty()) {
            IntegratedMemberVO searchVO = new IntegratedMemberVO();
            searchVO.setSvcSeq(svcSeq);
            searchVO.setMbrEmail(request.getParameter("email"));
            MemberVO result = memberService.selectIntegratedMember(searchVO);
            boolean isExisted = (result != null);
            if (isExisted) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("login.unavailableEmail.msg");
                return rsModel.getModelAndView();
            }
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();

        vo.setSvcSeq(svcSeq);
        vo.setUid(id);
        vo.setMbrNm(name);
        vo.setNickName(nickName);
        vo.setAccRoute(accRoute);
        vo.setMbrTelNo(request.getParameter("tel"));
        vo.setMbrMphonNo(request.getParameter("phone"));
        vo.setMbrEmail(request.getParameter("email"));
        vo.setIndvInfoAgreeYn(request.getParameter("indvInfoAgreeYn"));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.insertIntegratedMember(vo);
            if (resultCode > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                txManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                txManager.rollback(status);
            }
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getIntegratedMemberList(HttpServletRequest request, ResultModel rsModel, HttpSession session) throws Exception {
        // OMS에서 통합 인증 계정들 확인하기 위한 것으로 로그인되지 않은 사용자 또는 Master, CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !this.isAdministrator(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));

        vo.setUseYn(request.getParameter("useYn"));

        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));

        int totalCount = memberService.selectIntegratedMemberListTotalCount(vo);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = (currentPage - 1) * rows;
        vo.setOffset(offset);
        vo.setLimit(rows);

        List<IntegratedMemberVO> result = memberService.selectIntegratedMemberList(vo);
        if (result == null || result.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        /* 개인 정보 마스킹 */
        for (int i = 0; i < result.size(); i++) {
            result.get(i).setUid(CommonFnc.getIdMask(result.get(i).getUid()));
            if (!result.get(i).getMbrNm().equals("-")) {
                result.get(i).setMbrNm(CommonFnc.getNameMask(result.get(i).getMbrNm()));
            }

            if (result.get(i).getMbrEmail().equals("-")) {
                result.get(i).setMbrEmail(CommonFnc.getEmailMask(result.get(i).getMbrEmail()));
            }
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setData("item", result);
        rsModel.setResultType("integratedMemberList");

        return rsModel.getModelAndView();
    }

    private ModelAndView getIntegratedMember(HttpServletRequest request, ResultModel rsModel, HttpSession session) throws Exception {
        // 로그인 하지 않았거나 Master, CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !this.isAdministrator(userVO)) {
            return this.getIntegratedMemberInfoFromDev(request, rsModel);
        }

        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        int memberSeq = Integer.parseInt(request.getParameter("seq"));
        vo.setIntgrateAthnMbrSeq(memberSeq);

        IntegratedMemberVO result = memberService.selectIntegratedMember(vo);
        if (result == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        /* 개인 정보 마스킹 */
        result.setUid(CommonFnc.getIdMask(result.getUid()));
        result.setMbrNm(CommonFnc.getNameMask(result.getMbrNm()));
        result.setMbrTelNo(CommonFnc.getTelnoMask(result.getMbrTelNo()));
        result.setMbrMphonNo(CommonFnc.getTelnoMask(result.getMbrMphonNo()));
        result.setMbrEmail(CommonFnc.getEmailMask(result.getMbrEmail()));

        rsModel.setData("item", result);
        rsModel.setResultType("integratedMemberInfo");
        return rsModel.getModelAndView();
    }

    private ModelAndView getIntegratedMemberInfoFromDev(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,id,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "Unauthorized or invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        vo.setSvcSeq(svcSeq);
        vo.setUid(request.getParameter("id"));

        IntegratedMemberVO result = memberService.selectIntegratedMember(vo);
        if (result == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        /* 개인 정보 마스킹 */
        result.setUid(CommonFnc.getIdMask(result.getUid()));
        result.setMbrNm(CommonFnc.getNameMask(result.getMbrNm()));
        result.setMbrTelNo(CommonFnc.getTelnoMask(result.getMbrTelNo()));
        result.setMbrMphonNo(CommonFnc.getTelnoMask(result.getMbrMphonNo()));
        result.setMbrEmail(CommonFnc.getEmailMask(result.getMbrEmail()));

        rsModel.setData("item", result);
        rsModel.setResultType("integratedMemberInfo");

        return rsModel.getModelAndView();
    }

    private ModelAndView updateIntegratedMember(HttpServletRequest request, ResultModel rsModel, HttpSession session) throws Exception {
        // 로그인 하지 않았거나 Master, CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !this.isAdministrator(userVO)) {
            return this.updateIntegratedMemberFromDev(request, rsModel);
        }

        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        int memberSeq = Integer.parseInt(request.getParameter("seq"));
        vo.setIntgrateAthnMbrSeq(memberSeq);

        /* 상황에 따라 필요 시 주석 제거
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }
        */

        vo = memberService.selectIntegratedMember(vo);
        if (vo == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_NO_DATA);
            return rsModel.getModelAndView();
        }

        validateParams = "phone:{" + Validator.PHONE_NO + "};" + "email:{" + Validator.EMAIL + "}";
        invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "required parameter("+ invalidParams +")");
            return rsModel.getModelAndView();
        }

        if (request.getParameter("nickName") != null && !request.getParameter("nickName").isEmpty()) {
            String validateOptionalParams = "nickName:{" + Validator.MAX_LENGTH + "=100}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        if (request.getParameter("tel") != null && !request.getParameter("tel").isEmpty()) {
            String validateOptionalParams = "tel:{" + Validator.TEL_NO + "}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        // 이메일 중복 여부 체크
        IntegratedMemberVO searchVO = new IntegratedMemberVO();
        searchVO.setMbrEmail(request.getParameter("email"));
        IntegratedMemberVO result = memberService.selectIntegratedMember(searchVO);
        boolean isExisted = (result != null) && memberSeq != (result.getIntgrateAthnMbrSeq());
        if (isExisted) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            return rsModel.getModelAndView();
        }

        vo.setNickName(request.getParameter("nickName"));
        vo.setMbrTelNo(request.getParameter("tel"));
        vo.setMbrMphonNo(request.getParameter("phone"));
        vo.setMbrEmail(request.getParameter("email"));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updateIntegratedMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView updateIntegratedMemberFromDev(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,id,svcSeq,accRoute");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "Unauthorized or invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        vo.setSvcSeq(svcSeq);
        vo.setUid(request.getParameter("id"));

        vo = memberService.selectIntegratedMember(vo);
        if (vo == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_NO_DATA);
            return rsModel.getModelAndView();
        }

        String accRoute = CommonFnc.emptyCheckString("accRoute", request);
        // accRoute(계정 인증 경로) 길이 값 체크
        if (accRoute != null && !accRoute.isEmpty()) {
            String validateOptionalParams = "accRoute:{" + Validator.MAX_LENGTH + "=100}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        vo.setAccRoute(accRoute);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updateIntegratedMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView deleteIntegratedMember(HttpServletRequest request, ResultModel rsModel, HttpSession session) throws Exception {
        // 로그인 하지 않았거나 Master, CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !this.isAdministrator(userVO)) {
            return this.deleteIntegratedMemberFromDev(request, rsModel);
        }

        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        int memberSeq = Integer.parseInt(request.getParameter("seq"));
        vo.setIntgrateAthnMbrSeq(memberSeq);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.deleteIntegratedMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_DELETE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_DELETE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView deleteIntegratedMemberFromDev(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,id,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "Unauthorized or invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        vo.setSvcSeq(svcSeq);
        vo.setUid(request.getParameter("id"));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.deleteIntegratedMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_DELETE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_DELETE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 통합인증 회원 관련 로그인 이력 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/integrateLoginHst")
    public ModelAndView integratedMemberLoginHistoryProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return this.addIntegratedMbrHst(request, rsModel);
        }

        /*
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        if (method.equals("GET")) {
            boolean isList = "list".equals(request.getParameter("type"));
            if (isList) {
                return this.getIntegratedMemberList(request, rsModel, userVO);
            }
            return this.getIntegratedMember(request, rsModel, userVO);
        }
        */

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addIntegratedMbrHst(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,id,svcSeq,loginType");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        // 로그인 타입이 LI(로그인), LO(로그아웃)인 경우에만 이력 저장
        String loginType = CommonFnc.emptyCheckString("loginType", request);
        if (!loginType.equals("LI") && !loginType.equals("LO")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(loginType)");
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        String id = CommonFnc.emptyCheckString("id", request);

        vo.setSvcSeq(svcSeq);
        vo.setUid(id);
        vo.setLoginType(loginType);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.insertIntegratedMbrLoginHst(vo);
            if (resultCode > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                txManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                txManager.rollback(status);
            }
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 통합인증 회원 관련 약관 동의 이력 관리 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/termsHst")
    public ModelAndView memberTermsHistoryProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                return this.updateMemberTermsHst(request, rsModel);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                return this.addMemberTermsHst(request, rsModel);
            }
        } else { // get
            return this.getMemberTermsHstList(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addMemberTermsHst(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,uid,svcSeq,stpltSeqList,devSerial");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String uid = CommonFnc.emptyCheckString("uid", request);
        // uid 길이 값 체크
        if (uid != null && !uid.isEmpty()) {
            String validateOptionalParams = "uid:{" + Validator.MAX_LENGTH + "=15}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        String devSerial = CommonFnc.emptyCheckString("devSerial", request);
        // devSerial 길이 값 체크
        if (devSerial != null && !devSerial.isEmpty()) {
            String validateOptionalParams = "devSerial:{" + Validator.MAX_LENGTH + "=50}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        String stpltSeqList = request.getParameter("stpltSeqList") == null ? "" : request.getParameter("stpltSeqList");
        String[] stpltSeq = stpltSeqList.split(",");

        if (svcSeq == 0 || stpltSeqList == "") {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq or stpltSeqList)");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        vo.setSvcSeq(svcSeq);
        vo.setUid(uid);
        vo.setDevSerial(devSerial);
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < stpltSeq.length; i++) {
            list.add(Integer.parseInt(stpltSeq[i]));
        }
        vo.setList(list);

        // 약관 동의 정보 저장 시 현재 등록된 회원인지 여부 확인
        MemberVO resultMemVO = memberService.selectIntegratedMember(vo);
        boolean isExisted = (resultMemVO != null);
        if (!isExisted) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage("uid is not exist.");
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int result = memberService.insertMemberTermsHst(vo);
            if (result > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                txManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                txManager.rollback(status);
            }
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView updateMemberTermsHst(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,uid,svcSeq,stpltSeqList");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String uid = CommonFnc.emptyCheckString("uid", request);
        // uid 길이 값 체크
        if (uid != null && !uid.isEmpty()) {
            String validateOptionalParams = "uid:{" + Validator.MAX_LENGTH + "=15}";
            invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        String stpltSeqList = request.getParameter("stpltSeqList") == null ? "" : request.getParameter("stpltSeqList");

        if (svcSeq == 0 || stpltSeqList == "") {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq or stpltSeqList)");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        vo.setSvcSeq(svcSeq);
        vo.setUid(uid);
        if (stpltSeqList.equals("ALL")) {
            vo.setStpltAllDisAgreeYn("Y");
            vo.setList(null);
        } else {
            vo.setStpltAllDisAgreeYn("N");
            String[] stpltSeq = stpltSeqList.split(",");
            List<Integer> list = new ArrayList<Integer>();
            for (int i = 0; i < stpltSeq.length; i++) {
                list.add(Integer.parseInt(stpltSeq[i]));
            }
            vo.setList(list);
        }

        // 약관 동의 정보 저장 시 현재 등록된 회원인지 여부 확인
        MemberVO resultMemVO = memberService.selectIntegratedMember(vo);
        boolean isExisted = (resultMemVO != null);
        if (!isExisted) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage("uid is not exist.");
            return rsModel.getModelAndView();
        }

        List<IntegratedMemberVO> resultVO = memberService.searchMemberDetailTermsHst(vo);
        if (resultVO == null) {
            // 약관 동의 여부 정보 추가
            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

            TransactionStatus status = txManager.getTransaction(def);
            try {
                int result = memberService.insertMemberTermsHst(vo);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    txManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    txManager.rollback(status);
                }
            } catch (Exception e) {
                txManager.rollback(status);
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
            } finally {
                if (!status.isCompleted()) {
                    txManager.rollback(status);
                }
            }
        } else {
            // 약관 동의 여부 정보 수정
            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

            TransactionStatus status = txManager.getTransaction(def);
            try {
                int result = memberService.updateMemberTermsHst(vo);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    txManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    txManager.rollback(status);
                }
            } catch (Exception e) {
                txManager.rollback(status);
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
            } finally {
                if (!status.isCompleted()) {
                    txManager.rollback(status);
                }
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getMemberTermsHstList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,uid,svcSeq");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "Unauthorized or invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (!comnUtil.validMbrToken(request)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        IntegratedMemberVO vo = new IntegratedMemberVO();
        vo.setSvcSeq(svcSeq);
        vo.setUid(request.getParameter("uid"));

        List<IntegratedMemberVO> resultList = memberService.searchMemberTermsHst(vo);
        if (resultList == null || resultList.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        /* 개인 정보 마스킹 */
        for (int i = 0; i < resultList.size(); i++) {
            resultList.get(i).setUid(CommonFnc.getIdMask(resultList.get(i).getUid()));
        }

        rsModel.setData("item", resultList);
        rsModel.setResultType("memberTermsHstList");

        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MEMBER_SECTION_MASTER.equals(mbrSe)
            || MEMBER_SECTION_CMS.equals(mbrSe);
    }

    private boolean isAvailable(int mbrSeq, MemberVO userVO, ResultModel rsModel) throws Exception {
        MemberVO vo = new MemberVO();
        vo.setMbrSeq(mbrSeq);

        MemberVO result = memberService.getMember(vo);
        if (result == null) {
            rsModel.setNoData();
            return false;
        }

        if (MEMBER_SECTION_MASTER.equals(userVO.getMbrSe())) {
            boolean prevented = (MEMBER_SECTION_MASTER.equals(result.getMbrSe()) && (userVO.getMbrSeq() != result.getMbrSeq()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return false;
            }
        } else if (MEMBER_SECTION_CMS.equals(userVO.getMbrSe())) {
            boolean prevented = MEMBER_SECTION_MASTER.equals(result.getMbrSe())
                    || (MEMBER_SECTION_CMS.equals(result.getMbrSe()) && (userVO.getMbrSeq() != result.getMbrSeq()));
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return false;
            }
        } else if (MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())) {
            boolean prevented = MEMBER_SECTION_MASTER.equals(result.getMbrSe())
                    || MEMBER_SECTION_CMS.equals(vo.getMbrSe())
                    || (MEMBER_SECTION_SERVICE.equals(vo.getMbrSe()) && userVO.getMbrSeq() != vo.getMbrSeq())
                    || (vo.getSvcSeq() != userVO.getSvcSeq());
            if (prevented) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return false;
            }
        }
        return true;
    }

    /**
     * 임시 비밀번호 생성
     *
     * @param String salt(고유값)
     * @return String
     * @exception Exception
     */
    private String makeInitPassword() throws Exception {
        Random rnd = new Random();
        StringBuffer buf = new StringBuffer();
        for (int i=0; i < 15; i++) {
            if (rnd.nextBoolean()) {
                buf.append((char)((int)(rnd.nextInt(26))+97));
            } else {
                buf.append(rnd.nextInt(10));
            }
        }
        return buf.toString();
    }

    /**
     * 비밀번호 문자열 암호화
     *
     * @param String plainPwd(암호화할 문자열), salt(고유값)
     * @return String
     * @exception Exception
     */
    static public String makeEncryptPassword(String plainPwd, String salt) throws Exception {
        if (plainPwd == null || plainPwd.isEmpty())
            return plainPwd;

        String combinedPwd = getCombinedPassword(plainPwd, salt);

        byte[] pszMessage = combinedPwd.getBytes();
        int uPlainTextLen = pszMessage.length;
        byte[] pszDigest = new byte[32];

        SHA256.SHA256_Encrpyt(pszMessage, uPlainTextLen, pszDigest);

        return bytesToHex(pszDigest);
    }

    /**
     * 비밀번호 암호화 대상 문자열 생성
     *
     * @param String text(암호화할 문자열), salt(고유값)
     * @return String
     * @exception Exception
     */
    static public String getCombinedPassword(String plainPwd, String salt) throws Exception {
        if (plainPwd == null || plainPwd.isEmpty() || salt == null || salt.isEmpty())
            return plainPwd;

        StringBuilder builder = new StringBuilder();
        builder.append(salt);
        builder.append(plainPwd);
        builder.append(salt);

        return builder.toString();
    }

    /**
     * 비밀번호 고유 문자열 생성
     *
     * @param
     * @return String salt(고유값)
     * @exception Exception
     */
    static public String getGeneratedSalt() throws Exception {
        byte[] saltBytes = new byte[16];
        try {
            SecureRandom secureRandom = SecureRandom.getInstance(("SHA1PRNG"));
            secureRandom.nextBytes(saltBytes);
        } catch (Exception e) {

        }

        return bytesToHex(saltBytes);
    }

    /**
     * byte를 hex로 변경 함수
     *
     * @param byte[] bytes
     * @return String
     * @exception Exception
     */
    static public String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte b : bytes) {
            result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }

    /**
     * token 생성 함수
     *
     * @param id
     * @return String
     * @exception Exception
     */
    public String generateToken(String id) throws Exception {
        return AesAlgorithm.encrypt(id + new Date().toString(), configProperty.getPropertyAfterDecrypt("aes.key"));
    }

    private int getCurrentPage(HttpServletRequest request) {
        boolean validPageNumber = FormatCheckUtil.checkNumber(request.getParameter("page"));
        int currentPage = (validPageNumber) ? Integer.parseInt(request.getParameter("page")) : 1;
        if (currentPage <= 0) {
            currentPage = 1;
        }
        return currentPage;
    }

    private int getRowNumber(HttpServletRequest request) {
        boolean validRowNumber = FormatCheckUtil.checkNumber(request.getParameter("rows"));
        int rowNumber = (validRowNumber) ? Integer.parseInt(request.getParameter("rows")) : 10;
        return rowNumber;
    }

    private void insertLoginHistory(LoginVO vo, String loginYn, String msg) throws Exception {
        vo.setLoginYn(loginYn);
        vo.setLoginDesc("[  CMS  ] " + msg);
        // 로그인 이력 추가
        memberService.insertLoginHistory(vo);
    }

    private boolean mustChangePassword(HttpSession session) {
        if (session.getAttribute(TEMPORARY_ID) == null || session.getAttribute(MUST_CHANGE_PWD) == null) {
            return false;
        }

        return (boolean)session.getAttribute(MemberApi.MUST_CHANGE_PWD);
    }

}
