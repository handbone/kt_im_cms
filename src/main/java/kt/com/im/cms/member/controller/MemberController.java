/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.member.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * 회원 관리에 관한 컨트롤러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.16
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 16.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class MemberController {
    /**
     * 회원  리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/member", method = RequestMethod.GET)
    public ModelAndView memberList(ModelAndView mv) {
        mv.setViewName("/views/member/MemberList");
        return mv;
    }

    /**
     * 회원 상세 정보 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @param seq - 회원 번호값 저장, JSP 변수값으로 전달
     * 
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/member/{seq}", method = RequestMethod.GET)
    public ModelAndView memberDetail(ModelAndView mv, @PathVariable(value="seq") String seq) {
        mv.addObject("seq", seq);
        mv.setViewName("/views/member/MemberDetail");
        return mv;
    }

    /**
     * 회원 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * 
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/member/regist", method = RequestMethod.GET)
    public ModelAndView memberRegist(ModelAndView mv) {
        mv.setViewName("/views/member/MemberRegist");
        return mv;
    }

    /**
     * 회원 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @param seq - 멤버 번호값 저장, JSP 변수값으로 전달
     * 
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/member/edit/{seq}", method = RequestMethod.GET)
    public ModelAndView memberEdit(ModelAndView mv, @PathVariable(value="seq") String seq) {
        mv.addObject("seq", seq);
        mv.setViewName("/views/member/MemberEdit");
        return mv;
    }

    /**
     * 회원 승인 대기  리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */ 
    @RequestMapping(value = "/member/waiting", method = RequestMethod.GET)
    public ModelAndView memberWaitingList(ModelAndView mv) {
        mv.setViewName("/views/member/MemberWaitingList");
        return mv;
    }

    /**
     * 회원 승인 대기 상세 정보 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @param seq - 회원 번호값 저장, JSP 변수값으로 전달
     * 
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/member/waiting/{seq}", method = RequestMethod.GET)
    public ModelAndView memberWaitingDetail(ModelAndView mv, @PathVariable(value="seq") String seq) {
        mv.addObject("seq", seq);
        mv.setViewName("/views/member/MemberWaitingDetail");
        return mv;
    }

    /**
     * 유저 정보 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @param JSP 변수값으로 전달
     * 
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/mypage", method = RequestMethod.GET)
    public ModelAndView userDetail(ModelAndView mv) {
        mv.setViewName("/views/mypage/MyPageDetail");
        return mv;
    }

    /**
     * 유저 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @param JSP 변수값으로 전달
     * 
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/mypage/edit", method = RequestMethod.GET)
    public ModelAndView userEdit(ModelAndView mv) {
        mv.setViewName("/views/mypage/MyPageEdit");
        return mv;
    }

    /**
     * 통합 인증 회원  리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */ 
    @RequestMapping(value = "/member/integrate", method = RequestMethod.GET)
    public ModelAndView integratedMemberList(ModelAndView mv) {
        mv.setViewName("/views/member/IntegratedMemberList");
        return mv;
    }
}