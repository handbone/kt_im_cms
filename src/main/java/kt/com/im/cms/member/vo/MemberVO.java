/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.member.vo;

import java.io.Serializable;

/**
 *
 * 회원 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.05.16
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 16.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class MemberVO extends DefaultMemberVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -2341183106118297657L;

    /** 회원 번호 */
    int mbrSeq;

    /** 회원 아이디 */
    String mbrId;

    /** 회원 비밀번호 */
    String mbrPwd;

    /** 회원 이름 */
    String mbrNm;

    /** 회원 전화번호 */
    String mbrTelNo;

    /** 회원 이동전화번호 */
    String mbrMphonNo;

    /** 회원 전화번호들 */
    String[] mbrMphonNos;

    /** 회원 삭제전화번호 */
    String delphone;

    /** 회원 메일 */
    String mbrEmail;

    /** 회원 레벨 */
    int mbrLevel;

    /* OTP 인증시간 */
    long otpCreateTime;

    /** 회원 구분 */
    String mbrSe;

    /** 회원 구분명 */
    String mbrSeNm;

    /** 회원 상태 */
    String mbrSttus;

    /** 회원 상태명 */
    String mbrSttusNm;

    /** 회원 토큰 */
    String mbrTokn;

    /** 회원 토큰 만료 일시 */
    String mbrToknExpDt;

    /** 생성 일시 */
    String cretDt;

    /** 생성자 아이디 */
    String cretrId;

    /** 수정 일시 */
    String amdDt;

    /** 수정자 아이디 */
    String amdrId;

    /** 서비스 번호 */
    int svcSeq;

    /** 서비스 명 */
    String svcNm;

    /** 서비스 구분 */
    String svcType;

    /** 매장 번호 */
    int storSeq;

    /** 매장 명 */
    String storNm;

    /** 콘텐츠 제공사 번호 */
    int cpSeq;

    /** 콘텐츠 제공사 명 */
    String cpNm;

    /** 승인 일시 */
    String apvDt;

    /** 승인자 아이디 */
    String apvrId;

    /** 비밀번호 질문 */
    String pwdQstn;

    /** 비밀번호 답변 */
    String pwdAns;

    /** 비밀번호 생성용 고유값 */
    String salt;

    /** 증빙 서류 제출 여부 */
    String docSubmYn;

    /** 점포 코드 */
    String storCode;

    /** ip 주소 */
    String ipadr;

    /** 두번째 ip 주소 */
    String ipadr2;

    /** 로그인 실패 회수 */
    int loginFailCascnt;

    /** 임시 비밀번호 */
    String tempPwd;

    public int getMbrSeq() {
        return mbrSeq;
    }

    public void setMbrSeq(int mbrSeq) {
        this.mbrSeq = mbrSeq;
    }

    public String getMbrId() {
        return mbrId;
    }

    public void setMbrId(String mbrId) {
        this.mbrId = mbrId;
    }

    public String getMbrPwd() {
        return mbrPwd;
    }

    public void setMbrPwd(String mbrPwd) {
        this.mbrPwd = mbrPwd;
    }

    public String getMbrNm() {
        return mbrNm;
    }

    public void setMbrNm(String mbrNm) {
        this.mbrNm = mbrNm;
    }

    public String getMbrTelNo() {
        return mbrTelNo;
    }

    public void setMbrTelNo(String mbrTelNo) {
        this.mbrTelNo = mbrTelNo;
    }

    public String getMbrMphonNo() {
        return mbrMphonNo;
    }

    public void setMbrMphonNo(String mbrMphonNo) {
        this.mbrMphonNo = mbrMphonNo;
    }

    public String getMbrEmail() {
        return mbrEmail;
    }

    public void setMbrEmail(String mbrEmail) {
        this.mbrEmail = mbrEmail;
    }

    public int getMbrLevel() {
        return mbrLevel;
    }

    public void setMbrLevel(int mbrLevel) {
        this.mbrLevel = mbrLevel;
    }

    public String getMbrSe() {
        return mbrSe;
    }

    public void setMbrSe(String mbrSe) {
        this.mbrSe = mbrSe;
    }

    public String getMbrSeNm() {
        return mbrSeNm;
    }

    public void setMbrSeNm(String mbrSeNm) {
        this.mbrSeNm = mbrSeNm;
    }

    public String getMbrSttus() {
        return mbrSttus;
    }

    public void setMbrSttus(String mbrSttus) {
        this.mbrSttus = mbrSttus;
    }

    public String getMbrSttusNm() {
        return mbrSttusNm;
    }

    public void setMbrSttusNm(String mbrSttusNm) {
        this.mbrSttusNm = mbrSttusNm;
    }

    public String getMbrTokn() {
        return mbrTokn;
    }

    public void setMbrTokn(String mbrTokn) {
        this.mbrTokn = mbrTokn;
    }

    public String getMbrToknExpDt() {
        return mbrToknExpDt;
    }

    public void setMbrToknExpDt(String mbrToknExpDt) {
        this.mbrToknExpDt = mbrToknExpDt;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getSvcType() {
        return svcType;
    }

    public void setSvcType(String svcType) {
        this.svcType = svcType;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public int getCpSeq() {
        return cpSeq;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public void setCpSeq(int cpSeq) {
        this.cpSeq = cpSeq;
    }

    public String getCpNm() {
        return cpNm;
    }

    public void setCpNm(String cpNm) {
        this.cpNm = cpNm;
    }

    public String getApvDt() {
        return apvDt;
    }

    public void setApvDt(String apvDt) {
        this.apvDt = apvDt;
    }

    public String getApvrId() {
        return apvrId;
    }

    public void setApvrId(String apvrId) {
        this.apvrId = apvrId;
    }

    public String getPwdQstn() {
        return pwdQstn;
    }

    public void setPwdQstn(String pwdQstn) {
        this.pwdQstn = pwdQstn;
    }

    public String getPwdAns() {
        return pwdAns;
    }

    public void setPwdAns(String pwdAns) {
        this.pwdAns = pwdAns;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getDocSubmYn() {
        return docSubmYn;
    }

    public void setDocSubmYn(String docSubmYn) {
        this.docSubmYn = docSubmYn;
    }

    public String getStorCode() {
        return storCode;
    }

    public void setStorCode(String storCode) {
        this.storCode = storCode;
    }

    public String getIpadr() {
        return ipadr;
    }

    public void setIpadr(String ipadr) {
        this.ipadr = ipadr;
    }

    public String getIpadr2() {
        return ipadr2;
    }

    public void setIpadr2(String ipadr2) {
        this.ipadr2 = ipadr2;
    }

    public int getLoginFailCascnt() {
        return loginFailCascnt;
    }

    public void setLoginFailCascnt(int loginFailCascnt) {
        this.loginFailCascnt = loginFailCascnt;
    }

    public String getTempPwd() {
        return tempPwd;
    }

    public void setTempPwd(String tempPwd) {
        this.tempPwd = tempPwd;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getDelphone() {
        return delphone;
    }

    public void setDelphone(String delphone) {
        this.delphone = delphone;
    }

    public String[] getMbrMphonNos() {
        return mbrMphonNos;
    }

    public void setMbrMphonNos(String[] mbrMphonNos) {
        this.mbrMphonNos = mbrMphonNos;
    }

    public long getOtpCreateTime() {
        return otpCreateTime;
    }

    public void setOtpCreateTime(long otpCreateTime) {
        this.otpCreateTime = otpCreateTime;
    }



}
