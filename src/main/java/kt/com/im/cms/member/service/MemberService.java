/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.member.service;

import java.util.List;

import kt.com.im.cms.member.vo.IntegratedMemberVO;
import kt.com.im.cms.member.vo.LoginVO;
import kt.com.im.cms.member.vo.MemberHistoryVO;
import kt.com.im.cms.member.vo.MemberVO;

/**
 *
 * 회원 관리에 관한 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.16
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 16.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public interface MemberService {

    LoginVO getLoginInfo(LoginVO vo) throws Exception;

    int insertLoginHistory(LoginVO vo) throws Exception;

    long getOtpCreateTime(LoginVO vo) throws Exception;

    int updateOtpCreateTime(LoginVO vo) throws Exception;

    int increaseLoginFailCount(MemberVO vo) throws Exception;

    int resetLoginFailCount(MemberVO vo) throws Exception;

    List<MemberVO> getMemberList(MemberVO vo) throws Exception;

    int getMemberListTotalCount(MemberVO vo) throws Exception;

    MemberVO getMember(MemberVO vo) throws Exception;

    int addMember(MemberVO vo) throws Exception;

    int updateMember(MemberVO vo) throws Exception;

    int deleteMember(MemberVO vo) throws Exception;

    int updateMemberStatus(MemberVO vo) throws Exception;

    String getPassword(MemberVO vo) throws Exception;

    int updatePassword(MemberVO vo) throws Exception;

    int initPassword(MemberVO vo, boolean shouldUnblock) throws Exception;

    String getSalt(MemberVO vo) throws Exception;

    int searchMemberToken(MemberVO vo) throws Exception;

    MemberVO selectMemberInfoByToken(MemberVO vo) throws Exception;

    int updateMemberToken(MemberVO vo) throws Exception;

    MemberVO searchMbrId(MemberVO vo) throws Exception;

    List<MemberHistoryVO> getMemberHistoryList(MemberHistoryVO vo) throws Exception;

    boolean mustChangePassword(MemberVO vo) throws Exception;

    int insertIntegratedMember(IntegratedMemberVO vo) throws Exception;

    List<IntegratedMemberVO> selectIntegratedMemberList(IntegratedMemberVO vo) throws Exception;

    int selectIntegratedMemberListTotalCount(IntegratedMemberVO vo) throws Exception;

    IntegratedMemberVO selectIntegratedMember(IntegratedMemberVO vo) throws Exception;

    int updateIntegratedMemberUID(IntegratedMemberVO vo) throws Exception;

    int updateIntegratedMember(IntegratedMemberVO vo) throws Exception;
 
    int deleteIntegratedMember(IntegratedMemberVO vo) throws Exception;

    int insertIntegratedMbrLoginHst(IntegratedMemberVO vo) throws Exception;

    int insertMemberTermsHst(IntegratedMemberVO vo) throws Exception;

    int updateMemberTermsHst(IntegratedMemberVO vo) throws Exception;

    List<IntegratedMemberVO> searchMemberTermsHst(IntegratedMemberVO vo) throws Exception;

    int searchMemberTermsHstTotalCount(MemberVO vo) throws Exception;

    List<IntegratedMemberVO> searchMemberDetailTermsHst(IntegratedMemberVO vo) throws Exception;

    MemberVO getExistOtpCreateTime(MemberVO userVO);

}
