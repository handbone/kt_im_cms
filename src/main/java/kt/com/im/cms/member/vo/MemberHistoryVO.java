/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.member.vo;

import java.io.Serializable;

/**
 *
 * 회원 관리 VO 클래스 
 *
 * @author A2TEC
 * @since 2018.06.05
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 05.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class MemberHistoryVO extends DefaultMemberVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = 8918815346621396624L;

    /** 회원 변경이력 번호 */
    int mbrChghstSeq;

    /** 회원 번호 */
    int mbrSeq;

    /** 변경 컬럼 */
    String changeCol;

    /** 기존 값 */
    String legacyValue;

    /** 변경 값 */
    String changeValue;

    /** 생성자 아이디 */
    String cretrId;

    /** 생성 일시 */
    String cretDt;

    public int getMbrChghstSeq() {
        return mbrChghstSeq;
    }

    public void setMbrChghstSeq(int mbrChghstSeq) {
        this.mbrChghstSeq = mbrChghstSeq;
    }

    public int getMbrSeq() {
        return mbrSeq;
    }

    public void setMbrSeq(int mbrSeq) {
        this.mbrSeq = mbrSeq;
    }

    public String getChangeCol() {
        return changeCol;
    }

    public void setChangeCol(String changeCol) {
        this.changeCol = changeCol;
    }

    public String getLegacyValue() {
        return legacyValue;
    }

    public void setLegacyValue(String legacyValue) {
        this.legacyValue = legacyValue;
    }

    public String getChangeValue() {
        return changeValue;
    }

    public void setChangeValue(String changeValue) {
        this.changeValue = changeValue;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
