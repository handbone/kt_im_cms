/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.contentprovider.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * 콘텐츠 제공사(CP) 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class CpVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8564544520206052029L;

    /** 콘텐츠 제공사 번호 */
    private int cpSeq;

    /** 정보 개수 */
    private int count;

    /** 콘텐츠 제공사명 */
    private String cpNm;

    /** 콘텐츠 제공사 전화 번호 */
    private String cpTelNo;

    /** 검색 필드 */
    private String target;

    /** foreach 리스트 정보 */
    private List<Integer> list;

    /** 검색어 */
    private String keyword;

    /** 주소 (우편번호 + 기본 주소 + 상세 주소) */
    private String cpAddr;

    /** 우편번호 */
    private String zipcd;

    /** 기본 주소 */
    private String cpBasAddr;

    /** 상세 주소 */
    private String cpDtlAddr;

    /** 사업자등록번호 */
    private String bizno;

    /** 서비스 번호 */
    private int svcSeq;

    /** 서비스 명 */
    private String svcNm;

    /** 서비스 리스트 */
    private List<Integer> serviceList;

    /** 계약 시작 일시 */
    private String contStDt;

    /** 계약 종료 일시 */
    private String contFnsDt;

    /** 계약 번호 */
    private String buyinContNo;

    /** 중복 계약 번호 개수 */
    private int buyinContCnt;

    /** 계약 일시 (시작일시 + 종료일시) */
    private String contDt;

    /** 사용 여부 */
    private String useYn;

    /** 생성자 아이디 */
    private String cretrId;

    /** 생성 일시 */
    private String cretDt;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** 등록 일시 (생성 일시 or 수정 일시) */
    private String regDt;

    /** 정렬 컬럼명 */
    private String sidx;

    /** 정렬 방법 (DESC, ASC) */
    private String sord;

    /** 검색 영역 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** CP 정보 조회 타입 */
    private String acesType;

    /*
     * 과금관련
     */

    /** 승인 콘텐츠 개수 */
    private int contsCount;

    /** 사용금액 */
    private int totPrice;

    /** 업데이트일(계약) */
    private String buyinAmdDt;

    /** 화면 타입 */
    private String pageType;

    public int getCpSeq() {
        return cpSeq;
    }

    public void setCpSeq(int cpSeq) {
        this.cpSeq = cpSeq;
    }

    public String getCpNm() {
        return cpNm;
    }

    public void setCpNm(String cpNm) {
        this.cpNm = cpNm;
    }

    public String getCpTelNo() {
        return cpTelNo;
    }

    public void setCpTelNo(String cpTelNo) {
        this.cpTelNo = cpTelNo;
    }

    public String getZipcd() {
        return zipcd;
    }

    public void setZipcd(String zipcd) {
        this.zipcd = zipcd;
    }

    public String getCpBasAddr() {
        return cpBasAddr;
    }

    public void setCpBasAddr(String cpBasAddr) {
        this.cpBasAddr = cpBasAddr;
    }

    public String getCpDtlAddr() {
        return cpDtlAddr;
    }

    public void setCpDtlAddr(String cpDtlAddr) {
        this.cpDtlAddr = cpDtlAddr;
    }

    public String getBizno() {
        return bizno;
    }

    public void setBizno(String bizno) {
        this.bizno = bizno;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public List<Integer> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Integer> serviceList) {
        this.serviceList = new ArrayList<Integer>(serviceList.size());
        for (int i = 0; i < serviceList.size(); i++) {
            this.serviceList.add(serviceList.get(i));
        }
    }

    public String getContStDt() {
        return contStDt;
    }

    public void setContStDt(String contStDt) {
        this.contStDt = contStDt;
    }

    public String getContFnsDt() {
        return contFnsDt;
    }

    public void setContFnsDt(String contFnsDt) {
        this.contFnsDt = contFnsDt;
    }

    public String getContDt() {
        return contDt;
    }

    public void setContDt(String contDt) {
        this.contDt = contDt;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getCpAddr() {
        return cpAddr;
    }

    public void setCpAddr(String cpAddr) {
        this.cpAddr = cpAddr;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getAcesType() {
        return acesType;
    }

    public void setAcesType(String acesType) {
        this.acesType = acesType;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotPrice() {
        return totPrice;
    }

    public void setTotPrice(int totPrice) {
        this.totPrice = totPrice;
    }

    public String getBuyinAmdDt() {
        return buyinAmdDt;
    }

    public void setBuyinAmdDt(String buyinAmdDt) {
        this.buyinAmdDt = buyinAmdDt;
    }

    public int getContsCount() {
        return contsCount;
    }

    public void setContsCount(int contsCount) {
        this.contsCount = contsCount;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getBuyinContNo() {
        return buyinContNo;
    }

    public void setBuyinContNo(String buyinContNo) {
        this.buyinContNo = buyinContNo;
    }

    public int getBuyinContCnt() {
        return buyinContCnt;
    }

    public void setBuyinContCnt(int buyinContCnt) {
        this.buyinContCnt = buyinContCnt;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

}
