/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.contentprovider.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.contentprovider.dao.CpDAO;
import kt.com.im.cms.contentprovider.vo.CpVO;

/**
 *
 * 콘텐츠 제공사(CP) 관리에 관한 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("CpService")
public class CpServiceImpl implements CpService {

    @Resource(name = "CpDAO")
    private CpDAO cpDAO;

    /**
     * 콘텐츠 제공사(CP) 목록 정보 조회
     *
     * @param CpVO
     * @return 조회 목록 결과
     */
    @Override
    public List<CpVO> cpList(CpVO vo) throws Exception {
        return cpDAO.cpList(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 목록 합계 조회
     *
     * @param CpVO
     * @return 조회 목록 결과
     */
    @Override
    public int cpListTotalCount(CpVO vo) throws Exception {
        return cpDAO.cpListTotalCount(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 상세 정보
     *
     * @param CpVO
     * @return 조회 결과
     */
    @Override
    public CpVO cpDetail(CpVO vo) throws Exception {
        return cpDAO.cpDetail(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 상세 정보 목록
     *
     * @param CpVO
     * @return 조회 목록 결과
     */
    @Override
    public List<CpVO> cpDetailList(CpVO vo) {
        return cpDAO.cpDetailList(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 서비스 목록
     *
     * @param CpVO
     * @return 조회 목록 결과
     */
    @Override
    public List<CpVO> cpServiceList(CpVO vo) throws Exception {
        return cpDAO.cpServiceList(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 사업자 등록번호 존재 유무
     *
     * @param CpVO
     * @return 조회 결과
     */
    @Override
    public int searchCpInfo(CpVO vo) throws Exception {
        return cpDAO.searchCpInfo(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 등록
     *
     * @param CpVO
     * @return 등록 결과
     */
    @Override
    public int insertCp(CpVO vo) throws Exception {
        return cpDAO.insertCp(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 수정
     *
     * @param CpVO
     * @return 수정 결과
     */
    @Override
    public int updateCp(CpVO vo) throws Exception {
        return cpDAO.updateCp(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 삭제 (비활성화)
     *
     * @param CpVO
     * @return
     */
    @Override
    public int deleteCp(CpVO vo) throws Exception {
        return cpDAO.deleteCp(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 서비스 등록
     *
     * @param CpVO
     * @return
     */
    @Override
    public void insertCpService(CpVO vo) throws Exception {
        cpDAO.insertCpService(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 서비스 삭제
     *
     * @param CpVO
     * @return
     */
    @Override
    public void deleteCpService(CpVO vo) throws Exception {
        cpDAO.deleteCpService(vo);
    }

    /**
     * 콘텐츠 제공사(CP) 스케줄링을 통해 계약 기간 종료 시 비활성화
     *
     * @param CpVO
     * @return 수정 결과
     */
    @Override
    public int updateCpContractFinish(CpVO vo) throws Exception {
        return cpDAO.updateCpContractFinish(vo);
    }

    /**
     * 콘텐츠 제공사(CP) CRUD 이력 저장
     *
     * @param CpVO
     * @return
     */
    @Override
    public int insertCpInfoAcesHst(CpVO vo) throws Exception {
        return cpDAO.insertCpInfoAcesHst(vo);
    }

    /**
     * 수급 정보 목록
     *
     * @param CpVO
     * @return
     */
    @Override
    public List<CpVO> contractList(CpVO vo) {
        return cpDAO.contractList(vo);
    }

    /**
     * 수급 정보 합계
     *
     * @param CpVO
     * @return
     */
    @Override
    public int contractListTotalCount(CpVO vo) {
        return cpDAO.contractListTotalCount(vo);
    }
}
