/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.contentprovider.service;

import java.util.List;

import kt.com.im.cms.contentprovider.vo.CpVO;

/**
 *
 * 콘텐츠 제공사(CP) 관리에 관한 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public interface CpService {

    List<CpVO> cpList(CpVO vo) throws Exception;

    int cpListTotalCount(CpVO vo) throws Exception;

    CpVO cpDetail(CpVO vo) throws Exception;

    List<CpVO> cpServiceList(CpVO vo) throws Exception;

    int searchCpInfo(CpVO vo) throws Exception;

    int insertCp(CpVO vo) throws Exception;

    int updateCp(CpVO vo) throws Exception;

    int deleteCp(CpVO vo) throws Exception;

    void insertCpService(CpVO vo) throws Exception;

    void deleteCpService(CpVO vo) throws Exception;

    int updateCpContractFinish(CpVO vo) throws Exception;

    int insertCpInfoAcesHst(CpVO vo) throws Exception;

    List<CpVO> contractList(CpVO item);

    int contractListTotalCount(CpVO item);

    List<CpVO> cpDetailList(CpVO searchCps);

}
