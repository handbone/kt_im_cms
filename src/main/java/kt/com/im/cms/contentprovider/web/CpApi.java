/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.contentprovider.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import atg.taglib.json.util.JSONObject;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ConfigProperty;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.content.service.ContentService;
import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.contentprovider.service.CpService;
import kt.com.im.cms.contentprovider.vo.CpVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.store.service.StoreService;
import kt.com.im.cms.store.vo.StoreVO;

/**
 *
 * CP사 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 19.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class CpApi {

    @Resource(name = "CpService")
    private CpService cpService;

    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "StoreService")
    private StoreService storeService;

    @Resource(name = "memberTransactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private ConfigProperty configProperty;

    /**
     * 콘텐츠 제공사(CP) 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/cp")
    public ModelAndView cpProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/member/contentprovider/cpProcess");

        // Master 관리자, CMS 관리자만 접근 가능
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        CpVO item = new CpVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "cpNm,bizno,contStDt,contFnsDt");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            String cpNm = CommonFnc.emptyCheckString("cpNm", request);
            item.setCpNm(cpNm);

            boolean isExistCpNm = (cpService.searchCpInfo(item) > 0) ? true : false;
            if (isExistCpNm) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist cpNm");
                return rsModel.getModelAndView();
            }

            String bizno = CommonFnc.emptyCheckString("bizno", request);
            item.setBizno(bizno);

            boolean isExistBizno = (cpService.searchCpInfo(item) > 0) ? true : false;
            if (isExistBizno) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist bizno");
                return rsModel.getModelAndView();
            }

            String cpTelNo = CommonFnc.emptyCheckString("cpTelNo", request);
            String zipcd = CommonFnc.emptyCheckString("zipcode", request);
            String basAddr = CommonFnc.emptyCheckString("basAddr", request);
            String dtlAddr = CommonFnc.emptyCheckString("dtlAddr", request);
            String services = CommonFnc.emptyCheckString("service", request);
            String[] service = services.split(",");
            String contStDt = CommonFnc.emptyCheckString("contStDt", request);
            String contFnsDt = CommonFnc.emptyCheckString("contFnsDt", request);

            item.setCpTelNo(cpTelNo);
            item.setZipcd(zipcd);
            item.setCpBasAddr(basAddr);
            item.setCpDtlAddr(dtlAddr);
            item.setContStDt(contStDt);
            item.setContFnsDt(contFnsDt);
            item.setCretrId(userVO.getMbrId());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = cpService.insertCp(item);

                if (result == 1) {
                    List<Integer> list = new ArrayList<Integer>();
                    for (int i = 0; i < service.length; i++) {
                        list.add(Integer.parseInt(service[i]));
                    }
                    item.setServiceList(list);
                    cpService.insertCpService(item);

                    this.insertCpInfoAcesHst(item.getCpSeq(), "C", userVO.getMbrId());

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "cpSeq,cpNm,bizno,contStDt,contFnsDt");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);
            String cpNm = CommonFnc.emptyCheckString("cpNm", request);
            item.setCpSeq(cpSeq);
            item.setCpNm(cpNm);

            boolean isExistCpNm = (cpService.searchCpInfo(item) > 0) ? true : false;
            if (isExistCpNm) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist cpNm");
                return rsModel.getModelAndView();
            }

            String bizno = CommonFnc.emptyCheckString("bizno", request);
            item.setBizno(bizno);

            boolean isExistBizno = (cpService.searchCpInfo(item) > 0) ? true : false;
            if (isExistBizno) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist bizno");
                return rsModel.getModelAndView();
            }

            String cpTelNo = CommonFnc.emptyCheckString("cpTelNo", request);
            String zipcd = CommonFnc.emptyCheckString("zipcode", request);
            String basAddr = CommonFnc.emptyCheckString("basAddr", request);
            String dtlAddr = CommonFnc.emptyCheckString("dtlAddr", request);
            String services = CommonFnc.emptyCheckString("service", request);
            String[] service = services.split(",");
            String contStDt = CommonFnc.emptyCheckString("contStDt", request);
            String contFnsDt = CommonFnc.emptyCheckString("contFnsDt", request);
            String useYn = CommonFnc.emptyCheckString("useYn", request);

            item.setCpTelNo(cpTelNo);
            item.setZipcd(zipcd);
            item.setCpBasAddr(basAddr);
            item.setCpDtlAddr(dtlAddr);
            item.setContStDt(contStDt);
            item.setContFnsDt(contFnsDt);
            item.setAmdrId(userVO.getMbrId());
            item.setUseYn(useYn);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = cpService.updateCp(item);

                if (result == 1) {
                    List<Integer> list = new ArrayList<Integer>();
                    for (int i = 0; i < service.length; i++) {
                        list.add(Integer.parseInt(service[i]));
                    }
                    item.setServiceList(list);

                    cpService.insertCpService(item);
                    cpService.deleteCpService(item);

                    /*
                     * CP사 비활성화 또는 CP사 계약 종료 날짜가 오늘보다 이전으로 설정될 경우 CP사 추천 콘텐츠 검색하여 비활성화 시킴.
                     * CP사 비활성화에 따라 비활성화된 추천 콘텐츠는 CP사를 다시 활성화 시키더라도 활성화 되지 않음.
                     * 관리자가 다시 추천 콘텐츠 등록을 해주어야 함.
                     */
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar c1 = Calendar.getInstance();
                    String strToday = sdf.format(c1.getTime());
                    System.out.println(">> strToday : " + strToday);
                    Date today = sdf.parse(strToday);
                    Date contFnsdate = sdf.parse(contFnsDt);

                    if (useYn.equals("N") || today.compareTo(contFnsdate) > 0) {
                        ContentVO contVO = new ContentVO();
                        contVO.setCpSeq(cpSeq);
                        List<ContentVO> contList = contentService.searchCpRcmdContentList(contVO);

                        if (!contList.isEmpty()) {
                            List<Integer> contSeqList = new ArrayList<Integer>();
                            for (int i = 0; i < contList.size(); i++) {
                                contSeqList.add(contList.get(i).getContsSeq());
                            }
                            contVO.setAmdrID(userVO.getMbrId());
                            contVO.setDelRcmdContsSeqList(contSeqList);
                            contentService.deleteRecommendContents(contVO);
                        }
                    }

                    this.insertCpInfoAcesHst(item.getCpSeq(), "U", userVO.getMbrId());

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);
            item.setCpSeq(cpSeq);
            item.setAmdrId(session.getAttribute("id").toString());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = cpService.deleteCp(item);

                if (result > 0) {
                    /*
                     * CP사 비활성화 시 추천 콘텐츠 검색하여 비활성화 시킴.
                     * CP사 비활성화에 따라 비활성화된 추천 콘텐츠는 CP사를 다시 활성화 시키더라도 활성화 되지 않음.
                     * 관리자가 다시 추천 콘텐츠 등록을 해주어야 함.
                     */
                    ContentVO contVO = new ContentVO();
                    contVO.setCpSeq(cpSeq);
                    List<ContentVO> contList = contentService.searchCpRcmdContentList(contVO);

                    if (!contList.isEmpty()) {
                        List<Integer> contSeqList = new ArrayList<Integer>();
                        for (int i = 0; i < contList.size(); i++) {
                            contSeqList.add(contList.get(i).getContsSeq());
                        }
                        contVO.setAmdrID(userVO.getMbrId());
                        contVO.setDelRcmdContsSeqList(contSeqList);
                        contentService.deleteRecommendContents(contVO);
                    }

                    this.insertCpInfoAcesHst(item.getCpSeq(), "D", userVO.getMbrId());

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            if (cpSeq != 0) {
                if (!this.isAdministrator(userVO)) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                String buyinContNo = CommonFnc.emptyCheckString("buyinContNo", request);

                item.setCpSeq(cpSeq);
                item.setBuyinContNo(buyinContNo);

                CpVO resultItem = cpService.cpDetail(item);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                List<CpVO> resultServiceList = cpService.cpServiceList(item);

                this.insertCpInfoAcesHst(item.getCpSeq(), "R", userVO.getMbrId());

                /* 상세정보 요청일 경우 개인 정보 마스킹 처리 (전화번호, 주소, 사업자등록번호, 등록자 아이디, 수정자 아이디) */
                String getType = CommonFnc.emptyCheckString("getType", request);
                if (getType.equals("")) {
                    resultItem.setCpTelNo(CommonFnc.getTelnoMask(resultItem.getCpTelNo()));
                    resultItem.setCpBasAddr(CommonFnc.getAddrMask(resultItem.getCpBasAddr()));
                    resultItem.setCpDtlAddr("");
                    resultItem.setBizno(CommonFnc.getBiznoMask(resultItem.getBizno()));
                    resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("buyinContNo", buyinContNo);
                rsModel.setData("serviceList", resultServiceList);
                rsModel.setResultType("cpDetail");
            } else {
                if (!this.isAllowMember(userVO)) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (cpSeq == 0 && CommonFnc.checkReqParameter(request, "cpSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if (searchConfirm.equals("true")) {
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }
                /*
                 * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                 */
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);
                String useYn = CommonFnc.emptyCheckString("useYn", request);

                int page = (currentPage - 1) * limit + 1;
                int pageEnd = page + limit - 1;

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<CpVO> resultItem = cpService.cpList(item);
                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    /* 개인정보 마스킹 (전화번호, 주소) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCpTelNo(CommonFnc.getTelnoMask(resultItem.get(i).getCpTelNo()));
                        resultItem.get(i).setCpBasAddr(CommonFnc.getAddrMask(resultItem.get(i).getCpBasAddr()));
                    }

                    int totalCount = cpService.cpListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("cpList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 수급 정보 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contract")
    public ModelAndView contractProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/member/contentprovider/cpProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isAdministrator(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        CpVO item = new CpVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get

            /** CP 번호(검색조건) */
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            if (cpSeq != 0) {
                item.setCpSeq(cpSeq);
            }

            /** 검색여부 */
            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");

            item.setSearchConfirm(searchConfirm);
            if (searchConfirm.equals("true")) {
                /** 검색 카테고리 */
                String searchField = request.getParameter("searchField") == null ? ""
                        : request.getParameter("searchField");

                /** 검색어 */
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.setTarget(searchField);
                item.setSearchKeyword(searchString);
            }

            /** 현재 페이지 */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));

            /** 검색에 출력될 개수 */
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

            /** 정렬할 필드 */
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

            /** 정렬 방법 */
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
            int page = (currentPage - 1) * limit + 1;

            /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
            int pageEnd = (currentPage - 1) * limit + limit;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정

            if (sidx.equals("CP_NM")) {
            } else if (sidx.equals("CONTS_COUNT")) {
            } else if (sidx.equals("TOT_PRICE")) {
            } else if (sidx.equals("BUYIN_AMD_DT")) {
            } else if (sidx.equals("CP_SEQ")) {
            } else if (sidx.equals("CRET_DT")) {
            } else {
                sidx = "BUYIN_AMD_DT";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setOffset(page);
            item.setLimit(pageEnd);
            item.setPageType("contract");
            // item.setUseYn("Y");

            /** 로그인 중인 계정 레벨 값 */
            int user_level = session.getAttribute("level") == null ? 0
                    : Integer.parseInt((String) session.getAttribute("level"));

            // CP사 정보(사용자 로그인 일 경우 해당 멤버가 작성한 콘텐츠만 출력)
            if (user_level < 5) {
                cpSeq = session.getAttribute("cpSeq") == null ? 0
                        : Integer.parseInt((String) session.getAttribute("cpSeq"));

                item.setCpSeq(cpSeq);
            }

            List<CpVO> resultItem = cpService.contractList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                /** 가져온 리스트 총 개수 */
                int totalCount = cpService.contractListTotalCount(item);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", (totalCount - 1) / limit);
                rsModel.setResultType("cpBuyinList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 계약 정보 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contractDetail")
    public ModelAndView contractDetailProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/member/contentprovider/cpProcess");

        String Url_Block = configProperty.getProperty("chain.url");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isAdministrator(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        CpVO item = new CpVO();
        StoreVO sitem = new StoreVO();
        ContentVO Citem = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                String checkString = CommonFnc.requiredChecking(request,
                        "buyinSeq,cpSeq,buyinContNo,ratYn,buyinContStDt,buyinContFnsDt,buyinContType,runLmtCnt,runPerPrc,ratTime,ratPrc,lmsmpyPrc,contsSeqs");

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int buyinSeq = CommonFnc.emptyCheckInt("buyinSeq", request);
                int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);
                int runLmtCnt = CommonFnc.emptyCheckInt("runLmtCnt", request);
                int runPerPrc = CommonFnc.emptyCheckInt("runPerPrc", request);
                int ratTime = CommonFnc.emptyCheckInt("ratTime", request);
                int ratPrc = CommonFnc.emptyCheckInt("ratPrc", request);
                int lmsmpyPrc = CommonFnc.emptyCheckInt("lmsmpyPrc", request);

                String buyinContNo = CommonFnc.emptyCheckString("buyinContNo", request);
                String ratYn = CommonFnc.emptyCheckString("ratYn", request);
                String buyinContStDt = CommonFnc.emptyCheckString("buyinContStDt", request);
                String buyinContFnsDt = CommonFnc.emptyCheckString("buyinContFnsDt", request);
                String buyinContType = CommonFnc.emptyCheckString("buyinContType", request);

                sitem.setBuyinSeq(buyinSeq);
                sitem.setCpSeq(cpSeq);
                sitem.setRunLmtCnt(runLmtCnt);
                sitem.setRunPerPrc(runPerPrc);
                sitem.setRatTime(ratTime);
                sitem.setRatPrc(ratPrc);
                sitem.setLmsmpyPrc(lmsmpyPrc);
                sitem.setBuyinContNo(buyinContNo);
                sitem.setRatYn(ratYn);
                sitem.setBuyinContStDt(buyinContStDt);
                sitem.setBuyinContFnsDt(buyinContFnsDt);
                sitem.setBuyinContType(buyinContType);
                sitem.setCretrId((String) session.getAttribute("id"));

                try {

                    int result = storeService.contractUpdate(sitem);

                    if (result == 1) {
                        String contsSeqs = CommonFnc.emptyCheckString("contsSeqs", request);

                        String[] contsSeq = contsSeqs.split(",");

                        List<Integer> list = new ArrayList<Integer>();

                        storeService.contractContsDelete(sitem);

                        for (int i = 0; i < contsSeq.length; i++) {
                            sitem.setContsSeq(Integer.parseInt(contsSeq[i]));
                            result = storeService.contractContsUpdate(sitem);
                        }

                        String changed = CommonFnc.emptyCheckString("changed", request);
                        if (changed.equals("true")) {
                            sitem.setContsSeqs(contsSeqs);
                            result = storeService.contractHstInsert(sitem);
                        }

                        if (result != 0) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        }

                        /* BLOCK CHAIN Start */
                        String postParamUrl = Url_Block + "/api/v1/contract/cp";
                        int totCount = runLmtCnt;
                        int time = ratTime;
                        int totPrice = lmsmpyPrc + runPerPrc + ratPrc;

                        String message;
                        JSONObject json = new JSONObject();
                        json.put("buyinContNo", String.valueOf(buyinContNo));
                        json.put("cpSeq", String.valueOf(cpSeq));
                        json.put("contsSeq", contsSeqs);
                        json.put("ratYn", ratYn);
                        json.put("ratContStDt", buyinContStDt + "T23:59:00.000Z");
                        json.put("ratContFnsDt", buyinContFnsDt + "T23:59:00.000Z");
                        json.put("buyinContType", buyinContType);
                        json.put("price", String.valueOf(totPrice));
                        json.put("count", String.valueOf(totCount));
                        json.put("time", String.valueOf(ratTime));

                        postJson(postParamUrl, "127.0.0.1", json.toString(), "PUT");
                        /* BLOCK CHAIN End */

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                        transactionManager.commit(status);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("contractUpdate");
                return rsModel.getModelAndView();
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                String checkString = CommonFnc.requiredChecking(request,
                        "cpSeq,buyinContNo,ratYn,buyinContStDt,buyinContFnsDt,buyinContType,runLmtCnt,runPerPrc,ratTime,ratPrc,lmsmpyPrc,contsSeqs");

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);
                int runLmtCnt = CommonFnc.emptyCheckInt("runLmtCnt", request);
                int runPerPrc = CommonFnc.emptyCheckInt("runPerPrc", request);
                int ratTime = CommonFnc.emptyCheckInt("ratTime", request);
                int ratPrc = CommonFnc.emptyCheckInt("ratPrc", request);
                int lmsmpyPrc = CommonFnc.emptyCheckInt("lmsmpyPrc", request);

                String buyinContNo = CommonFnc.emptyCheckString("buyinContNo", request);
                String ratYn = CommonFnc.emptyCheckString("ratYn", request);
                String buyinContStDt = CommonFnc.emptyCheckString("buyinContStDt", request);
                String buyinContFnsDt = CommonFnc.emptyCheckString("buyinContFnsDt", request);
                String buyinContType = CommonFnc.emptyCheckString("buyinContType", request);

                sitem.setCpSeq(cpSeq);
                sitem.setRunLmtCnt(runLmtCnt);
                sitem.setRunPerPrc(runPerPrc);
                sitem.setRatTime(ratTime);
                sitem.setRatPrc(ratPrc);
                sitem.setLmsmpyPrc(lmsmpyPrc);
                sitem.setBuyinContNo(buyinContNo);
                sitem.setRatYn(ratYn);
                sitem.setBuyinContStDt(buyinContStDt);
                sitem.setBuyinContFnsDt(buyinContFnsDt);
                sitem.setBuyinContType(buyinContType);
                sitem.setCretrId((String) session.getAttribute("id"));

                try {
                    int result = storeService.contractInsert(sitem);

                    if (result == 1) {
                        String contsSeqs = CommonFnc.emptyCheckString("contsSeqs", request);

                        String[] contsSeq = contsSeqs.split(",");

                        List<Integer> list = new ArrayList<Integer>();
                        for (int i = 0; i < contsSeq.length; i++) {
                            sitem.setContsSeq(Integer.parseInt(contsSeq[i]));
                            result = storeService.contractContsInsert(sitem);
                        }

                        /* BLOCK CHAIN Start */
                        String postParamUrl = Url_Block + "/api/v1/contract/cp";
                        int totCount = runLmtCnt;
                        int time = ratTime;
                        int totPrice = lmsmpyPrc + runPerPrc + ratPrc;

                        String message;
                        JSONObject json = new JSONObject();
                        json.put("buyinContNo", String.valueOf(buyinContNo));
                        json.put("cpSeq", String.valueOf(cpSeq));
                        json.put("contsSeq", contsSeqs);
                        json.put("ratYn", ratYn);
                        json.put("ratContStDt", buyinContStDt + "T23:59:00.000Z");
                        json.put("ratContFnsDt", buyinContFnsDt + "T23:59:00.000Z");
                        json.put("buyinContType", buyinContType);
                        json.put("price", String.valueOf(totPrice));
                        json.put("count", String.valueOf(totCount));
                        json.put("time", String.valueOf(ratTime));
                        postJson(postParamUrl, "127.0.0.1", json.toString(), "POST");
                        /* BLOCK CHAIN End */

                        if (result != 0) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        }

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                        transactionManager.commit(status);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("contractInsert");
                return rsModel.getModelAndView();
            }
        } else { // get

            /** CP 번호(검색조건) */
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);
            int buyinSeq = CommonFnc.emptyCheckInt("buyinSeq", request);

            item.setCpSeq(cpSeq);
            sitem.setCpSeq(cpSeq);

            if (buyinSeq != 0) {
                sitem.setBuyinSeq(buyinSeq);

                StoreVO resultItem = storeService.contractInfo(sitem);
                item.setCpSeq(resultItem.getCpSeq());
                /** 해당 콘텐츠 상세정보 */

                CpVO resultCItem = cpService.cpDetail(item);
                if (resultCItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_PRECED_NO_DATA);
                    return rsModel.getModelAndView();
                }

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                    resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                    String[] contsSeqs = resultItem.getContsSeqs().split(",");
                    List<ContentVO> resultCntListItem = new ArrayList<ContentVO>();
                    for (int i = 0; i < contsSeqs.length; i++) {
                        Citem = new ContentVO();
                        Citem.setList(null);
                        Citem.setDelYn("N");
                        Citem.setCpSeq(0);
                        Citem.setSvcSeq(0);
                        Citem.setCntrctFnsDt("");
                        Citem.setContsSeq(Integer.parseInt(contsSeqs[i]));
                        Citem.setSttusVal("05");

                        ContentVO resultCntItem = contentService.contentCdtNotInfo(Citem);
                        if (resultCntItem != null) {
                            resultCntListItem.add(resultCntItem);
                        }
                    }

                    rsModel.setData("item", resultItem);
                    rsModel.setData("cntItem", resultCntListItem);
                    rsModel.setData("cpNm", resultCItem.getCpNm());
                    rsModel.setResultType("contractInfo");
                }
            } else {
                CpVO resultItem = cpService.cpDetail(item);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_PRECED_NO_DATA);
                    return rsModel.getModelAndView();
                }

                /** 검색여부 */
                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                sitem.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    sitem.setTarget(searchField);
                    sitem.setSearchKeyword(searchString);
                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정

                if (sidx.equals("BUYIN_SEQ")) {
                } else if (sidx.equals("BUYIN_CONT_NO")) {
                } else if (sidx.equals("CONTS_TITLE")) {
                } else if (sidx.equals("BUYIN_CONT_TYPE")) {
                } else if (sidx.equals("REG_DT")) {
                } else if (sidx.equals("SET_VAL")) {
                } else if (sidx.equals("TOT_PRICE")) {
                } else if (sidx.equals("USE_PRICE")) {
                } else if (sidx.equals("CRETR_NM")) {
                } else if (sidx.equals("AMD_DT")) {
                } else if (sidx.equals("CONTS_COUNT")) {
                } else {
                    sidx = "BUYIN_SEQ";
                }

                sitem.setSidx(sidx);
                sitem.setSord(sord);
                sitem.setOffset(page);
                sitem.setLimit(pageEnd);
                // sitem.setUseYn("Y");

                /** 로그인 중인 계정 레벨 값 */
                int user_level = session.getAttribute("level") == null ? 0
                        : Integer.parseInt((String) session.getAttribute("level"));

                // CP사 정보(사용자 로그인 일 경우 해당 멤버가 작성한 콘텐츠만 출력)
                if (user_level < 5) {
                    cpSeq = session.getAttribute("cpSeq") == null ? 0
                            : Integer.parseInt((String) session.getAttribute("cpSeq"));

                    sitem.setCpSeq(cpSeq);
                }

                List<StoreVO> resultCItem = storeService.contractList(sitem);

                rsModel.setData("cpNm", resultItem.getCpNm());

                if (resultCItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (이름, 아이디) */
                    for (int i = 0; i < resultCItem.size(); i++) {
                        resultCItem.get(i).setCretrNm(CommonFnc.getNameMask(resultCItem.get(i).getCretrNm()));
                        resultCItem.get(i).setCretrId(CommonFnc.getIdMask(resultCItem.get(i).getCretrId()));
                    }

                    /** 가져온 리스트 총 개수 */
                    int totalCount = storeService.contractListTotalCount(sitem);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultCItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("contractList");
                }
            }

        }

        return rsModel.getModelAndView();
    }

    /**
     * 검수 이력 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contractHistory")
    public ModelAndView contractHistoryProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/member/contentprovider/cpProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isAdministrator(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        CpVO item = new CpVO();
        StoreVO sitem = new StoreVO();
        ContentVO Citem = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            /** CP 번호(검색조건) */
            int buyinSeq = CommonFnc.emptyCheckInt("buyinSeq", request);
            sitem.setBuyinSeq(buyinSeq);

            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            sitem.setSearchConfirm(searchConfirm);
            // 검색 여부가 true일 경우
            if (searchConfirm.equals("true")) {
                String searchField = CommonFnc.emptyCheckString("searchField", request);
                String searchString = CommonFnc.emptyCheckString("searchString", request);

                sitem.setSearchTarget(searchField);
                sitem.setSearchKeyword(searchString);
            }
            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
            String sidx = CommonFnc.emptyCheckString("sidx", request);
            String sord = CommonFnc.emptyCheckString("sord", request);
            String useYn = CommonFnc.emptyCheckString("useYn", request);

            if (sidx.equals("BUYIN_CONT_HST_SEQ")) {
            } else if (sidx.equals("CONTS_COUNT")) {
            } else if (sidx.equals("TOT_PRICE")) {
            } else if (sidx.equals("BUYIN_AMD_DT")) {
            } else if (sidx.equals("REG_DT")) {
            } else if (sidx.equals("AMD_DT")) {
            } else if (sidx.equals("AMDR_ID")) {
            } else if (sidx.equals("CONTS_COUNT")) {
            } else {
                sidx = "BUYIN_CONT_HST_SEQ";
            }

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            sitem.setSidx(sidx);
            sitem.setSord(sord);
            sitem.setOffset(page);
            sitem.setLimit(pageEnd);

            List<StoreVO> resultItem = storeService.contractHistoryList(sitem);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (수정자 아이디) */
                for (int i = 0; i < resultItem.size(); i++) {
                    resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));
                }

                int totalCount = storeService.contractHistoryListTotalCount(sitem);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("contractHistoryList");
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe);
    }

    private boolean isManageAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe);
    }

    private boolean isNotAllowMember(MemberVO userVO) {
        if (userVO == null) {
            return true;
        }
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

    private void insertCpInfoAcesHst(int cpSeq, String acesType, String cretrId) throws Exception {
        CpVO cpVO = new CpVO();
        cpVO.setCpSeq(cpSeq);
        cpVO.setAcesType(acesType);
        cpVO.setCretrId(cretrId);
        CommonFnc.voInfo(cpVO);
        cpService.insertCpInfoAcesHst(cpVO);
    }

    /* 외부 URL 정보 POST 함수_ 현재 이용: [블록체인 데이터 전송] */
    public static String postJson(String serverUrl, String host, String jsonobject, String Method) {

        StringBuilder sb = new StringBuilder();

        String http = serverUrl;

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(http);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(Method);
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(100000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Host", host);
            urlConnection.connect();
            // You Can also Create JSONObject here
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonobject);// here i sent the parameter
            out.close();
            int HttpResult = urlConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                // Log.e("new Test", "" + sb.toString());
                return sb.toString();
            } else {
                System.out.println(urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        } catch (JSONException e) {
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }
}
