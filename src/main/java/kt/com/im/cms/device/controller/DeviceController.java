/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.device.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * DEVICE 관리에 관한 컨트롤러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 28.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class DeviceController {
    /**
     * DEVICE 목록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/device", method = RequestMethod.GET)
    public ModelAndView deviceList(ModelAndView mv) {
        mv.setViewName("/views/device/DeviceList");
        return mv;
    }

    /**
     * DEVICE 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/device/{devSeq}", method = RequestMethod.GET)
    public ModelAndView deviceDetail(ModelAndView mv, @PathVariable(value = "devSeq") String devSeq) {
        mv.addObject("devSeq", devSeq);
        mv.setViewName("/views/device/DeviceDetail");
        return mv;
    }

    /**
     * DEVICE 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/device/regist", method = RequestMethod.GET)
    public ModelAndView deviceRegist(ModelAndView mv) {
        mv.setViewName("/views/device/DeviceRegist");
        return mv;
    }

    /**
     * DEVICE 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/device/{devSeq}/edit", method = RequestMethod.GET)
    public ModelAndView deviceEdit(ModelAndView mv, @PathVariable(value = "devSeq") String devSeq) {
        mv.addObject("devSeq", devSeq);
        mv.setViewName("/views/device/DeviceEdit");
        return mv;
    }

}