/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.device.service;

import java.util.List;

import kt.com.im.cms.device.vo.DeviceVO;

/**
 *
 * 장비 관련 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 23.   A2TEC      최초생성
* 2018. 6. 28.   정규현            deviceList,deviceListTotalCount 추가
 *
 *      </pre>
 */

public interface DeviceService {

    public List<DeviceVO> categoryList(DeviceVO vo) throws Exception;

    public DeviceVO deviceUseInfo(DeviceVO vo) throws Exception;

    public void deviceUseInsert(DeviceVO vo) throws Exception;

    public void deviceUseStateUpdate(DeviceVO vo) throws Exception;

    public List<DeviceVO> deviceList(DeviceVO item);

    public int deviceListTotalCount(DeviceVO item);

    public DeviceVO deviceInfo(DeviceVO item);

    public int deviceInsert(DeviceVO item);

    public int deviceFrmtnInsert(DeviceVO item);

    public int searchDeviceToken(DeviceVO vo);

    public int updateDeviceToken(DeviceVO vo) throws Exception;

    public DeviceVO reservateUsageCount(DeviceVO vo);

    public int updateReservateUsageExpiry(DeviceVO vo);

    public int updateReservateUsageCount(DeviceVO vo);

    public List<DeviceVO> useGenreList(DeviceVO vo) throws Exception;

    public int contentPlayTimeUpdate(DeviceVO device);

    public int devicePlayUseUpdate(DeviceVO device);

    public int contentPlayTimeInsert(DeviceVO device);

    public int deviceMacUpdate(DeviceVO device);

    public int deviceIpUpdate(DeviceVO device);

    public int deviceUseUpdate(DeviceVO vo) throws Exception;

    public List<DeviceVO> deviceDetailList(DeviceVO device);

    public int deviceMacUpdateReset(DeviceVO item);

    public int contsUpdateDelete(DeviceVO device);

    public int deviceUpdate(DeviceVO item);

    public int deviceFrmtnUpdate(DeviceVO item);

    public int deviceDelete(DeviceVO item);

    public List<DeviceVO> deviceContsDownList(DeviceVO item);

    public int deviceContsDownListTotalCount(DeviceVO item);

    public int deviceCategoryUpdate(DeviceVO vo);

    public int contentsPlayTimeOut(DeviceVO device);

    public int updateDeviceUseDetailInfo(DeviceVO vo);

    public DeviceVO getDeviceUseDetailInfo(DeviceVO vo);

    public DeviceVO playerChk(DeviceVO device);

    public DeviceVO playerEndChk(DeviceVO device);

    public int contentPastPlayTimeUpdate(DeviceVO device);

    public DeviceVO selectUseTimeInfo(DeviceVO device);

    public int useGenreListTotalCount(DeviceVO item);

}
