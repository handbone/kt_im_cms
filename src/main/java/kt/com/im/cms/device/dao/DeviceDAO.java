/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.device.dao;

import java.util.List;

import kt.com.im.cms.device.vo.DeviceVO;

/**
 *
 * 장비 관련 데이터 접근 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 23.   A2TEC      최초생성
* 2018. 6. 28.   정규현            deviceList,deviceListTotalCount 추가
 *
 *      </pre>
 */

public interface DeviceDAO {

    public List<DeviceVO> categoryList(DeviceVO vo) throws Exception;

    public DeviceVO deviceUseInfo(DeviceVO vo) throws Exception;

    public void deviceUseInsert(DeviceVO vo) throws Exception;

    public void deviceUseStateUpdate(DeviceVO vo) throws Exception;

    public List<DeviceVO> deviceList(DeviceVO vo);

    public int deviceListTotalCount(DeviceVO vo);

    public DeviceVO deviceInfo(DeviceVO vo);

    public int deviceInsert(DeviceVO vo);

    public int deviceFrmtnInsert(DeviceVO vo);

    public int searchDeviceToken(DeviceVO vo);

    public int updateDeviceToken(DeviceVO vo) throws Exception;

    public DeviceVO reservateUsageCount(DeviceVO vo);

    public int updateReservateUsageExpiry(DeviceVO vo);

    public int updateReservateUsageCount(DeviceVO vo);

    public List<DeviceVO> useGenreList(DeviceVO vo) throws Exception;

    public int contentPlayTimeUpdate(DeviceVO vo);

    public int devicePlayUseUpdate(DeviceVO vo);

    public int contentPlayTimeInsert(DeviceVO vo);

    public int deviceMacUpdate(DeviceVO vo);

    public int deviceIpUpdate(DeviceVO vo);

    public int deviceUseUpdate(DeviceVO vo) throws Exception;

    public List<DeviceVO> deviceDetailList(DeviceVO vo);

    public int deviceMacUpdateReset(DeviceVO vo);

    public int contsUpdateDelete(DeviceVO vo);

    public int deviceUpdate(DeviceVO vo);

    public int deviceFrmtnUpdate(DeviceVO vo);

    public int deviceDelete(DeviceVO vo);

    public List<DeviceVO> deviceContsDownList(DeviceVO vo);

    public int deviceContsDownListTotalCount(DeviceVO vo);

    public int deviceCategoryUpdate(DeviceVO vo);

    public int contentsPlayTimeOut(DeviceVO vo);

    public int updateDeviceUseDetailInfo(DeviceVO vo);

    public DeviceVO getDeviceUseDetailInfo(DeviceVO vo);

    public DeviceVO playerChk(DeviceVO vo);

    public DeviceVO playerEndChk(DeviceVO vo);

    public int contentPastPlayTimeUpdate(DeviceVO vo);

    public DeviceVO selectUseTimeInfo(DeviceVO vo);

    public int useGenreListTotalCount(DeviceVO vo);

}
