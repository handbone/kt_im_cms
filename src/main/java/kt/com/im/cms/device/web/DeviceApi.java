/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.device.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.device.service.DeviceService;
import kt.com.im.cms.device.vo.DeviceVO;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.product.service.ProductService;
import kt.com.im.cms.reservate.service.ReservateService;
import kt.com.im.cms.reservate.vo.ReservateVO;

/**
 *
 * 이용권 관련 메뉴에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 23.   A2TEC      최초생성
* 2018. 6. 28.   정규현            deviceList,deviceListTotalCount 추가
 *
 *      </pre>
 */

@Controller
public class DeviceApi {

    @Resource(name = "ReservateService")
    private ReservateService reservateService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "ProductService")
    private ProductService productService;

    @Resource(name = "DeviceService")
    private DeviceService deviceService;

    // @Resource(name="VerifyService")
    // private VerifyService verifyService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    /**
     * 매장 카테고리 정보 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/storCategory")
    public ModelAndView category(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        // 로그인 되지 않았거나 검수자, CP사 관계자인 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        DeviceVO item = new DeviceVO();

        int storSeq = request.getParameter("storSeq") == null ? 0 : Integer.parseInt(request.getParameter("storSeq"));
        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "storSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            item.setStorSeq(storSeq);

            List<DeviceVO> resultItem = deviceService.categoryList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("resultType", "categoryList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 매장 장비 정보 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/device")
    public ModelAndView deviceProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        // 로그인 되지 않았거나 검수자, CP사 관계자인 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        DeviceVO item = new DeviceVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String checkString = CommonFnc.requiredChecking(request, "devId,devTypeNm,frmtnSeq,storSeq,playTime");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            item.setDevId(CommonFnc.emptyCheckString("devId", request));
            item.setDevTypeNm(CommonFnc.emptyCheckString("devTypeNm", request));
            item.setFrmtnSeq(CommonFnc.emptyCheckInt("frmtnSeq", request));
            item.setStorSeq(CommonFnc.emptyCheckInt("storSeq", request));
            item.setDevIpadr(CommonFnc.emptyCheckString("devIpadr", request));
            item.setPlayTime(CommonFnc.emptyCheckInt("playTime", request));

            item.setCretrId((String) session.getAttribute("id"));

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = deviceService.deviceInsert(item);

                if (result == 1) {
                    deviceService.deviceFrmtnInsert(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
                rsModel.setResultType("deviceInsert");
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            String macAdrReset = CommonFnc.emptyCheckString("macAdrReset", request);
            String checkString = "";
            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            if (macAdrReset.equals("Y")) {
                // Checking Mendatory S
                checkString = CommonFnc.requiredChecking(request, "devSeq");

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                // Checking Mendatory E

                item.setMacAdr("");
                item.setDevSeq(devSeq);
                try {
                    int result = deviceService.deviceMacUpdateReset(item);
                    if (result == 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("devSeq", devSeq);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                }
            } else {
                // Checking Mendatory S
                checkString = CommonFnc.requiredChecking(request, "devSeq,frmtnSeq,storSeq,devTypeNm,devId,playTime");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                // Checking Mendatory E

                int frmtnSeq = CommonFnc.emptyCheckInt("frmtnSeq", request);
                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                int playTime = CommonFnc.emptyCheckInt("playTime", request);
                String devTypeNm = CommonFnc.emptyCheckString("devTypeNm", request);
                String devId = CommonFnc.emptyCheckString("devId", request);
                String devIpadr = CommonFnc.emptyCheckString("devIpadr", request);

                item.setDevSeq(devSeq);
                item.setFrmtnSeq(frmtnSeq);
                item.setStorSeq(storSeq);
                item.setDevTypeNm(devTypeNm);
                item.setDevId(devId);
                item.setPlayTime(playTime);
                item.setDevIpadr(devIpadr);

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    int result = deviceService.deviceUpdate(item);
                    if (result == 1) {
                        result = deviceService.deviceFrmtnUpdate(item);
                        if (result == 0) {
                            deviceService.deviceFrmtnInsert(item);
                        }
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("devSeq", devSeq);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    }
                    transactionManager.commit(status);
                    rsModel.setResultType("deviceUpdate");
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            }
        } else if (method.equals("DELETE")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "devSeqs");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                String devSeqs = request.getParameter("devSeqs");
                String[] devSeq = devSeqs.split(",");

                List<Integer> list = new ArrayList<Integer>();
                for (int i = 0; i < devSeq.length; i++) {
                    list.add(Integer.parseInt(devSeq[i]));
                }
                item.setList(list);
                item.setDelYn("Y");
                int result = deviceService.deviceDelete(item);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }

            rsModel.setResultType("deviceDelete");
            return rsModel.getModelAndView();
        } else { // GET
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            String devId = CommonFnc.emptyCheckString("devId", request);

            item.setSvcSeq(svcSeq);
            item.setStorSeq(storSeq);
            if (devId != "" || devSeq != 0) {
                item.setDevId(devId);
                item.setDevSeq(devSeq);

                DeviceVO resultItem = deviceService.deviceInfo(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                            && userVO.getSvcSeq() != resultItem.getSvcSeq())
                            || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)
                                    && userVO.getStorSeq() != resultItem.getStorSeq())) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                    String getType = CommonFnc.emptyCheckString("getType", request);
                    if (getType.equals("")) {
                        resultItem.setMbrNm(CommonFnc.getNameMask(resultItem.getMbrNm()));
                        resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                        resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                        resultItem.setDevIpadr(CommonFnc.getIpAddrMask(resultItem.getDevIpadr()));
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }

                rsModel.setResultType("deviceInfo");
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (devSeq == 0 && CommonFnc.checkReqParameter(request, "devSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                String searchType = request.getParameter("searchType") == null ? ""
                        : request.getParameter("searchType");

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    item.setTarget(searchField);
                    item.setKeyword(searchString);
                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("FRMTN_NM")) {
                } else if (sidx.equals("STOR_NM")) {
                } else if (sidx.equals("DEV_ID")) {
                } else {
                    sidx = "CRET_DT";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setoffset(page);
                item.setlimit(pageEnd);
                item.setMacSearch(0);

                List<DeviceVO> resultItem = deviceService.deviceList(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디, device ip) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setMbrNm(CommonFnc.getNameMask(resultItem.get(i).getMbrNm()));
                        resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                        resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));
                        resultItem.get(i).setDevIpadr(CommonFnc.getIpAddrMask(resultItem.get(i).getDevIpadr()));
                    }

                    int totalCount = deviceService.deviceListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", (totalCount - 1) / limit);
                    rsModel.setResultType("deviceList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 매장별 장비 이용현황 & 장비 이용시간 관련
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceConnect")
    public ModelAndView deviceConnectProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        request.setCharacterEncoding("UTF-8");
        DeviceVO device = new DeviceVO();
        ReservateVO Ritem = new ReservateVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            if (devSeq != 0) {
                device.setDevSeq(devSeq);
                DeviceVO resultVO = deviceService.getDeviceUseDetailInfo(device);

                if (resultVO == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultVO);
                }

                rsModel.setResultType("deviceUseDetailInfo");
            } else {
                /* Checking Mendatory S */
                String checkString = CommonFnc.requiredChecking(request, "storSeq");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                /* Checking Mendatory E */

                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

                device.setStorSeq(storSeq);

                List<DeviceVO> resultItem = deviceService.deviceDetailList(device);
                if (resultItem == null || resultItem.size() == 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    for (int i = 0; i < resultItem.size(); i++) {
                        if (resultItem.get(i).getFilePath() != null) {
                            resultItem.get(i).setFilePath(resultItem.get(i).getFilePath());
                        }

                        int currentPage = 1;
                        int limit = 1000;
                        int page = (currentPage - 1) * limit + 1;
                        int pageEnd = page + limit - 1;

                        Ritem = new ReservateVO();
                        Ritem.setDevSeq(resultItem.get(i).getDevSeq());
                        Ritem.setSidx("seq");
                        Ritem.setSord("DESC");
                        Ritem.setOffset(page);
                        Ritem.setLimit(pageEnd);
                        Ritem.setStorSeq(resultItem.get(i).getStorSeq());
                        Ritem.setStartDt("");
                        Ritem.setEndDt("");
                        Ritem.setSearchConfirm("false");
                        List<ReservateVO> resultRitem = reservateService.reservateList(Ritem);

                        resultItem.get(i).setUseTag(resultRitem);

                        /* 개인정보 마스킹 (등록자 아이디, device ip) */
                        resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                        resultItem.get(i).setDevIpadr(CommonFnc.getIpAddrMask(resultItem.get(i).getDevIpadr()));
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }
                rsModel.setResultType("deviceConnect");
            }
        }

        return rsModel.getModelAndView();

    }

    /**
     * 장비 콘텐츠 다운로그 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/deviceDownLog")
    public ModelAndView deviceDownLog(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/device/deviceProcess");

        // 로그인 되지 않았거나 검수자, CP사 관계자인 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        DeviceVO item = new DeviceVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType");
            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");

            item.setSearchConfirm(searchConfirm);
            // 검색 여부가 true일 경우
            if (searchConfirm.equals("true")) {
                /** 검색 카테고리 */
                String searchField = request.getParameter("searchField") == null ? ""
                        : request.getParameter("searchField");

                /** 검색어 */
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.setTarget(searchField);
                item.setKeyword(searchString);
            }

            /** 현재 페이지 */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));

            /** 검색에 출력될 개수 */
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

            /** 정렬할 필드 */
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

            /** 정렬 방법 */
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
            int page = (currentPage - 1) * limit + 1;

            /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
            int pageEnd = (currentPage - 1) * limit + limit;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
            if (sidx.equals("DEV_ID")) {
            } else if (sidx.equals("CONTS_TITLE")) {
            } else {
                sidx = "CRET_DT";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setoffset(page);
            item.setlimit(pageEnd);

            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int devSeq = CommonFnc.emptyCheckInt("devSeq", request);
            item.setDevSeq(devSeq);
            item.setStorSeq(storSeq);

            List<DeviceVO> resultItem = deviceService.deviceContsDownList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                int totalCount = deviceService.deviceContsDownListTotalCount(item);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", (totalCount - 1) / limit);
                rsModel.setResultType("deviceDownLog");
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isNotAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe) || MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

}
