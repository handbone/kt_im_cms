/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.device.vo;

import java.io.Serializable;
import java.util.List;

import kt.com.im.cms.reservate.vo.ReservateVO;

/**
 *
 * 이용권 발급 메뉴 관련 데이터 객체
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 23.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
public class DeviceVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9197663276818918359L;

    /** 장비 번호 */
    private int devSeq;

    /** 매장 번호 */
    private int storSeq;

    /** 로그 번호 */
    private int contsUseTimeSeq;

    /** 장비 유형명 */
    private String devTypeNm;

    /** 장비 유형명 (Full_Temp) */
    private String devTypeNmFull;

    /** 장비 유형 갯수 */
    private int devCtgCnt;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 콘텐츠 상세 설명 */
    private String contsDesc;

    /** 콘텐츠 부가 설명 */
    private String contsSubTitle;

    /** 이용중인 태그 */
    List<ReservateVO> useTag;

    /** 파일경로 */
    private String filePath;

    /** 이용 시작 시간 */
    private String useStTime;

    /** 이용 종료 시간 */
    private String useFnsTime;

    /** 이용 시간 */
    private int usageMinute;

    /** 콘텐츠 다운 번호 */
    private int contsDownSeq;

    /** 장비 아이디 */
    private String devId;

    /** 등록자 아이디 */
    private String cretrId;

    /** 회원 이름 */
    private String mbrNm;

    /** 서비스 명 */
    private String svcNm;

    /** 리스트 */
    private List list;

    /** 장비 IP 주소 */
    private String devIpadr;

    /** 콘텐츠 번호 */
    private int contsSeq;

    /** 장비 플레이 시간 */
    private int playTime;

    /** 장비 mac 주소 */
    private String macAdr;

    /** 매장 명 */
    private String storNm;

    /** 종료 시간 */
    private String fnsTime;

    /** 시작 시간 */
    private String stTime;

    /** 서비스 번호 */
    private int svcSeq;

    /** 편성 콘텐츠 목록 */
    private int frmtnCnt;

    /** MAC 주소 검색 판별 변수 */
    private int macSearch;

    /** 태그 아이디 */
    private String tagId;

    /** 태그 번호 */
    private int tagSeq;

    /** 잔여 횟수 */
    private int usageCount;

    /** 상품 번호 */
    private int prodSeq;

    /** 사용 상태 */
    private String useSttus;

    /** 종료 시간 */
    private int shutTime;

    /** 상품 제한 횟수 */
    private int prodLmtCnt;

    /** 상품 제한 시간 */
    private int prodTimeLmt;

    /** 제한 시간 여부 */
    private String timeLimit;

    /** 장비 다운로드 콘텐츠 수 */
    private int devContsCnt;

    /** 장비 다운로드 콘텐츠 수 */
    private String devTokn;

    /** 장비 다운로드 콘텐츠 수 */
    private String devToknExpDt;

    /* 이전 Device Type 명 */
    private String befDevTypeNm;

    /** 장비 다운로드 콘텐츠 수 */
    private String devToknUpdDt;

    private List<ReservateVO> useTagList;

    /** 장르 고유 번호 */
    private int genreSeq;

    /** 장르 명 */
    private String genreNm;

    /** 등록일 */
    private String regDt;

    /** 장비 사용 중 유무 (On/Off) */
    private String devUseYn;

    /** 사용 중 장비의 CPU 사용량 */
    private String devUseCpuRate;

    /** 사용 중 장비의 메모리 사용량 */
    private String devUsageMem;

    /** 사용 중 장비의 저장 공간 정보 */
    private String devStrgeSpace;

    /** 사용 중 장비 상세 정보 업데이트 일시 */
    private String devUseInfoUpdDt;

    public int getDevSeq() {
        return devSeq;
    }

    public void setDevSeq(int devSeq) {
        this.devSeq = devSeq;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public String getDevTypeNm() {
        return devTypeNm;
    }

    public void setDevTypeNm(String devTypeNm) {
        this.devTypeNm = devTypeNm;
    }

    public int getDevCtgCnt() {
        return devCtgCnt;
    }

    public void setDevCtgCnt(int devCtgCnt) {
        this.devCtgCnt = devCtgCnt;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getUseStTime() {
        return useStTime;
    }

    public void setUseStTime(String useStTime) {
        this.useStTime = useStTime;
    }

    public String getUseFnsTime() {
        return useFnsTime;
    }

    public void setUseFnsTime(String useFnsTime) {
        this.useFnsTime = useFnsTime;
    }

    public int getUsageMinute() {
        return usageMinute;
    }

    public void setUsageMinute(int usageMinute) {
        this.usageMinute = usageMinute;
    }

    /** 생성 일시 */
    private String cretDt;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** 삭제 여부 */
    private String delYn;

    /** 로그인 아이디 */
    private String loginId;

    /** 활성화 여부 */
    private String useYn;

    /** 검색 유무 */
    private String searchConfirm;

    /** 검색 키워드 */
    private String keyword;

    /** 페이지 값 */
    private int offset;

    /** 매장 편성명 */
    private String frmtnNm;

    /** 매장 편성 번호 */
    private int frmtnSeq;

    /** 리스트 개수 */
    private int limit;

    /** 정렬 필드 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 조건(카테고리) */
    private String target;

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAamdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getoffset() {
        return offset;
    }

    public void setoffset(int offset) {
        this.offset = offset;
    }

    public int getlimit() {
        return limit;
    }

    public void setlimit(int limit) {
        this.limit = limit;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getDevId() {
        return devId;
    }

    public void setDevId(String devId) {
        this.devId = devId;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getMbrNm() {
        return mbrNm;
    }

    public void setMbrNm(String mbrNm) {
        this.mbrNm = mbrNm;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public String getFrmtnNm() {
        return frmtnNm;
    }

    public void setFrmtnNm(String frmtnNm) {
        this.frmtnNm = frmtnNm;
    }

    public int getFrmtnSeq() {
        return frmtnSeq;
    }

    public void setFrmtnSeq(int frmtnSeq) {
        this.frmtnSeq = frmtnSeq;
    }

    public String getDevIpadr() {
        return devIpadr;
    }

    public void setDevIpadr(String devIpadr) {
        this.devIpadr = devIpadr;
    }

    public int getPlayTime() {
        return playTime;
    }

    public void setPlayTime(int playTime) {
        this.playTime = playTime;
    }

    public String getMacAdr() {
        return macAdr;
    }

    public void setMacAdr(String macAdr) {
        this.macAdr = macAdr;
    }

    public int getFrmtnCnt() {
        return frmtnCnt;
    }

    public void setFrmtnCnt(int frmtnCnt) {
        this.frmtnCnt = frmtnCnt;
    }

    public int getDevContsCnt() {
        return devContsCnt;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public int getTagSeq() {
        return tagSeq;
    }

    public void setTagSeq(int tagSeq) {
        this.tagSeq = tagSeq;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public void setDevContsCnt(int devContsCnt) {
        this.devContsCnt = devContsCnt;
    }

    public String getDevTokn() {
        return devTokn;
    }

    public void setDevTokn(String devTokn) {
        this.devTokn = devTokn;
    }

    public String getDevToknExpDt() {
        return devToknExpDt;
    }

    public void setDevToknExpDt(String devToknExpDt) {
        this.devToknExpDt = devToknExpDt;
    }

    public String getDevToknUpdDt() {
        return devToknUpdDt;
    }

    public void setDevToknUpdDt(String devToknUpdDt) {
        this.devToknUpdDt = devToknUpdDt;
    }

    public int getMacSearch() {
        return macSearch;
    }

    public void setMacSearch(int macSearch) {
        this.macSearch = macSearch;
    }

    public List<ReservateVO> getUseTagList() {
        return useTagList;
    }

    public void setUseTagList(List<ReservateVO> useTagList) {
        this.useTagList = useTagList;
    }

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }

    public int getProdSeq() {
        return prodSeq;
    }

    public void setProdSeq(int prodSeq) {
        this.prodSeq = prodSeq;
    }

    public String getUseSttus() {
        return useSttus;
    }

    public void setUseSttus(String useSttus) {
        this.useSttus = useSttus;
    }

    public int getShutTime() {
        return shutTime;
    }

    public void setShutTime(int shutTime) {
        this.shutTime = shutTime;
    }

    public int getProdLmtCnt() {
        return prodLmtCnt;
    }

    public void setProdLmtCnt(int prodLmtCnt) {
        this.prodLmtCnt = prodLmtCnt;
    }

    public int getProdTimeLmt() {
        return prodTimeLmt;
    }

    public void setProdTimeLmt(int prodTimeLmt) {
        this.prodTimeLmt = prodTimeLmt;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public int getGenreSeq() {
        return genreSeq;
    }

    public void setGenreSeq(int genreSeq) {
        this.genreSeq = genreSeq;
    }

    public String getGenreNm() {
        return genreNm;
    }

    public void setGenreNm(String genreNm) {
        this.genreNm = genreNm;
    }

    public String getFnsTime() {
        return fnsTime;
    }

    public void setFnsTime(String fnsTime) {
        this.fnsTime = fnsTime;
    }

    public String getStTime() {
        return stTime;
    }

    public void setStTime(String stTime) {
        this.stTime = stTime;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public String getContsDesc() {
        return contsDesc;
    }

    public void setContsDesc(String contsDesc) {
        this.contsDesc = contsDesc;
    }

    public String getContsSubTitle() {
        return contsSubTitle;
    }

    public void setContsSubTitle(String contsSubTitle) {
        this.contsSubTitle = contsSubTitle;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<ReservateVO> getUseTag() {
        return useTag;
    }

    public void setUseTag(List<ReservateVO> useTag) {
        this.useTag = useTag;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getContsDownSeq() {
        return contsDownSeq;
    }

    public void setContsDownSeq(int contsDownSeq) {
        this.contsDownSeq = contsDownSeq;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getBefDevTypeNm() {
        return befDevTypeNm;
    }

    public void setBefDevTypeNm(String befDevTypeNm) {
        this.befDevTypeNm = befDevTypeNm;
    }

    public String getDevTypeNmFull() {
        return devTypeNmFull;
    }

    public void setDevTypeNmFull(String devTypeNmFull) {
        this.devTypeNmFull = devTypeNmFull;
    }

    public String getDevUseYn() {
        return devUseYn;
    }

    public void setDevUseYn(String devUseYn) {
        this.devUseYn = devUseYn;
    }

    public String getDevUseCpuRate() {
        return devUseCpuRate;
    }

    public void setDevUseCpuRate(String devUseCpuRate) {
        this.devUseCpuRate = devUseCpuRate;
    }

    public String getDevUsageMem() {
        return devUsageMem;
    }

    public void setDevUsageMem(String devUsageMem) {
        this.devUsageMem = devUsageMem;
    }

    public String getDevStrgeSpace() {
        return devStrgeSpace;
    }

    public void setDevStrgeSpace(String devStrgeSpace) {
        this.devStrgeSpace = devStrgeSpace;
    }

    public String getDevUseInfoUpdDt() {
        return devUseInfoUpdDt;
    }

    public void setDevUseInfoUpdDt(String devUseInfoUpdDt) {
        this.devUseInfoUpdDt = devUseInfoUpdDt;
    }

    public int getContsUseTimeSeq() {
        return contsUseTimeSeq;
    }

    public void setContsUseTimeSeq(int contsUseTimeSeq) {
        this.contsUseTimeSeq = contsUseTimeSeq;
    }

}
