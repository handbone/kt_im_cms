/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.device.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.device.vo.DeviceVO;

/**
 *
 * 장비 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 23.   A2TEC      최초생성
* 2018. 6. 28.   정규현            deviceList,deviceListTotalCount 추가
 *
 *
 *      </pre>
 */

@Repository("DeviceDAO")
@Transactional
public class DeviceDAOImpl extends mysqlAbstractMapper implements DeviceDAO {

    /**
     * 검색조건에 해당되는 장비 종류 리스트 정보
     *
     * @param DeviceVO
     * @return 조회 목록 결과
     */
    @Override
    public List<DeviceVO> categoryList(DeviceVO vo) throws Exception {
        return selectList("DeviceDAO.categoryList", vo);
    }

    /**
     * 검색조건에 해당되는 사용 중인 장비 조회
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO deviceUseInfo(DeviceVO vo) throws Exception {
        return selectOne("DeviceDAO.deviceUseInfo", vo);
    }

    /**
     * 장비 사용 정보 생성
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public void deviceUseInsert(DeviceVO vo) throws Exception {
        insert("DeviceDAO.deviceUseInsert", vo);
    }

    /**
     * 장비 사용 상태 업데이트
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public void deviceUseStateUpdate(DeviceVO vo) throws Exception {
        insert("DeviceDAO.deviceUseStateUpdate", vo);
    }

    /**
     * 검색조건에 해당되는 장비 리스트 정보
     *
     * @param DeviceVO
     * @return 조회 목록 결과
     */
    @Override
    public List<DeviceVO> deviceList(DeviceVO vo) {
        return selectList("DeviceDAO.deviceList", vo);
    }

    /**
     * 검색조건에 해당되는 장비 리스트 페이징 정보
     *
     * @param DeviceVO
     * @return 총 장비 수 /
     */
    @Override
    public int deviceListTotalCount(DeviceVO vo) {
        int res = (Integer) selectOne("DeviceDAO.deviceListTotalCount", vo);
        return res;
    }

    /**
     * 장비 상세정보 조회
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public DeviceVO deviceInfo(DeviceVO vo) {
        return selectOne("DeviceDAO.deviceInfo", vo);
    }

    /**
     * 장비 등록
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public int deviceInsert(DeviceVO vo) {
        int res = insert("DeviceDAO.deviceInsert", vo);
        return res;
    }

    /**
     * 장비 편성정보 등록
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public int deviceFrmtnInsert(DeviceVO vo) {
        int res = insert("DeviceDAO.deviceFrmtnInsert", vo);
        return res;
    }

    /**
     * 장비 토큰 존재 유무 조회
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public int searchDeviceToken(DeviceVO vo) {
        int res = (Integer) selectOne("DeviceDAO.searchDeviceToken", vo);
        return res;
    }

    /**
     * 장비 토큰 수정
     *
     * @param DeviceVO
     * @return 처리 결과
     */
    @Override
    public int updateDeviceToken(DeviceVO vo) throws Exception {
        return update("DeviceDAO.updateDeviceToken", vo);
    }

    /**
     * 태그 이용 횟수 조회
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO reservateUsageCount(DeviceVO vo) {
        return selectOne("DeviceDAO.reservateUsageCount", vo);
    }

    /**
     * 태그 사용 종료 정보 업데이트
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public int updateReservateUsageExpiry(DeviceVO vo) {
        return update("DeviceDAO.updateReservateUsageExpiry", vo);
    }

    /**
     * 태그 사용 정보 업데이트
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public int updateReservateUsageCount(DeviceVO vo) {
        return update("DeviceDAO.updateReservateUsageCount", vo);
    }

    /**
     * 장비에 등록된 콘텐츠의 장르 목록 조회
     *
     * @param DeviceVO
     * @return 장비에 등록된 콘텐츠의 장르 목록 조회
     */
    @Override
    public List<DeviceVO> useGenreList(DeviceVO vo) {
        return selectList("DeviceDAO.useGenreList", vo);
    }

    /**
     * 장비 - 이용중인 콘텐츠 종료 시간 업데이트
     *
     * @param DeviceVO
     * @return 업데이트 성공 여부
     */
    @Override
    public int contentPlayTimeUpdate(DeviceVO vo) {
        return update("DeviceDAO.contentPlayTimeUpdate", vo);
    }

    /**
     * 장비 - 장비 이용 시간 업데이트
     *
     * @param DeviceVO
     * @return 업데이트 성공 여부
     */
    @Override
    public int devicePlayUseUpdate(DeviceVO vo) {
        return update("DeviceDAO.devicePlayUseUpdate", vo);
    }

    /**
     * 장비 - 콘텐츠 실행 시간 등록
     *
     * @param DeviceVO
     * @return 등록 성공 여부
     */
    @Override
    public int contentPlayTimeInsert(DeviceVO vo) {
        int res = insert("DeviceDAO.contentPlayTimeInsert", vo);
        return res;
    }

    /**
     * Device 맥 ID 정보 업데이트
     *
     * @param DeviceVO
     * @return Device Mac 정보 업데이트 성공여부
     */
    @Override
    public int deviceMacUpdate(DeviceVO vo) {
        int res = update("DeviceDAO.deviceMacUpdate", vo);
        return res;
    }

    /**
     * IP정보 업데이트
     *
     * @param DeviceVO
     * @return IP정보 업데이트
     */
    @Override
    public int deviceIpUpdate(DeviceVO vo) {
        int res = update("DeviceDAO.deviceIpUpdate", vo);
        return res;
    }

    /**
     * 장비 종료 시간 수정
     *
     * @param DeviceVO
     * @return 처리 결과
     */
    @Override
    public int deviceUseUpdate(DeviceVO vo) throws Exception {
        return update("DeviceDAO.deviceUseUpdate", vo);
    }

    /**
     * 장비 상세정보 (+콘텐츠 정보)
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public List<DeviceVO> deviceDetailList(DeviceVO vo) {
        return selectList("DeviceDAO.deviceDetailList", vo);
    }

    /**
     * 장비 MAC 주소 초기화
     *
     * @param DeviceVO
     * @return 초기화 성공 여부
     */
    @Override
    public int deviceMacUpdateReset(DeviceVO vo) {
        int res = update("DeviceDAO.deviceMacUpdateReset", vo);
        return res;
    }

    /**
     * Device 다운 콘텐츠 목록 초기화
     *
     * @param DeviceVO
     * @return 초기화 성공 여부
     */
    @Override
    public int contsUpdateDelete(DeviceVO vo) {
        int res = update("DeviceDAO.contsUpdateDelete", vo);
        return res;
    }

    /**
     * Device 정보 수정
     *
     * @param DeviceVO
     * @return 정보 수정 성공 여부
     */
    @Override
    public int deviceUpdate(DeviceVO vo) {
        int res = update("DeviceDAO.deviceUpdate", vo);
        return res;
    }

    /**
     * Device 편성 그룹 정보 수정
     *
     * @param DeviceVO
     * @return 편성 그룹 정보 수정
     */
    @Override
    public int deviceFrmtnUpdate(DeviceVO vo) {
        int res = Integer.valueOf(update("DeviceDAO.deviceFrmtnUpdate", vo));
        return res;
    }

    /**
     * Device 정보 삭제
     *
     * @param DeviceVO
     * @return Device 정보 삭제 성공 여부
     */
    @Override
    public int deviceDelete(DeviceVO vo) {
        int res = Integer.valueOf(update("DeviceDAO.deviceDelete", vo));
        return res;
    }

    /**
     * Device 콘텐츠 다운 목록
     *
     * @param DeviceVO
     * @return Device 콘텐츠 다운 목록 리스트
     */
    @Override
    public List<DeviceVO> deviceContsDownList(DeviceVO vo) {
        return selectList("DeviceDAO.deviceContsDownList", vo);
    }

    /**
     * 검색조건에 해당되는 Device 콘텐츠 다운 페이징 정보
     *
     * @param DeviceVO
     * @return Device 콘텐츠 다운 페이징 정보
     */
    @Override
    public int deviceContsDownListTotalCount(DeviceVO vo) {
        int res = (Integer) selectOne("DeviceDAO.deviceContsDownListTotalCount", vo);
        return res;
    }

    /**
     * 콘텐츠 카테고리 변경될시, Device Type 변경
     *
     * @param DeviceVO
     * @return Device Type 수정 성공 여부
     */
    @Override
    public int deviceCategoryUpdate(DeviceVO vo) {
        int res = Integer.valueOf(update("DeviceDAO.deviceCategoryUpdate", vo));
        return res;
    }

    /*
     * (non-Javadoc)
     * @see kt.com.im.cms.device.dao.DeviceDAO#contentsPlayTimeOut(kt.com.im.cms.device.vo.DeviceVO)
     */
    @Override
    public int contentsPlayTimeOut(DeviceVO vo) {
        int res = Integer.valueOf(update("DeviceDAO.contentsPlayTimeOut", vo));
        return res;
    }

    /**
     * Device 사용 상세 정보 수정 (CPU 사용량, 메모리 사용량, 저장공간 정보)
     *
     * @param DeviceVO
     * @return 정보 수정 성공 여부
     */
    @Override
    public int updateDeviceUseDetailInfo(DeviceVO vo) {
        int res = update("DeviceDAO.updateDeviceUseDetailInfo", vo);
        return res;
    }

    /**
     * Device 사용 상세 정보 (CPU 사용량, 메모리 사용량, 저장공간 정보)
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO getDeviceUseDetailInfo(DeviceVO vo) {
        return selectOne("DeviceDAO.getDeviceUseDetailInfo", vo);
    }

    /**
     * Device 정보 [동시 이용 인원 정보]
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO playerChk(DeviceVO vo) {
        return selectOne("DeviceDAO.playerChk", vo);
    }

    /**
     * Device 정보 [동시 종료 인원 정보]
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO playerEndChk(DeviceVO vo) {
        return selectOne("DeviceDAO.playerEndChk", vo);
    }

    /**
     * Device 정보 [ 조건 값 이용로그 시간 강제 업데이트]
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public int contentPastPlayTimeUpdate(DeviceVO vo) {
        int res = Integer.valueOf(update("DeviceDAO.contentPastPlayTimeUpdate", vo));
        return res;
    }

    /**
     * Device [이용로그 검색 조회]
     *
     * @param DeviceVO
     * @return 조회 목록 결과
     */
    @Override
    public DeviceVO selectUseTimeInfo(DeviceVO vo) {
        return selectOne("DeviceDAO.selectUseTimeInfo", vo);
    }

    /**
     * Device 장르 리스트 검색 조건에 해당되는 콘텐츠 총 개수
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public int useGenreListTotalCount(DeviceVO vo) {
        int res = (Integer) selectOne("DeviceDAO.useGenreListTotalCount", vo);
        return res;
    }

}
