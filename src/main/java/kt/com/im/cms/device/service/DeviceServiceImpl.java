/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.device.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.device.dao.DeviceDAO;
import kt.com.im.cms.device.vo.DeviceVO;

/**
 *
 * 장비 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 23.   A2TEC      최초생성
* 2018. 6. 28.   정규현            deviceList,deviceListTotalCount 추가
 *
 *      </pre>
 */

@Service("DeviceService")
public class DeviceServiceImpl implements DeviceService {

    @Resource(name = "DeviceDAO")
    private DeviceDAO deviceDAO;

    /**
     * 검색조건에 해당되는 장비 종류 리스트 정보
     *
     * @param DeviceVO
     * @return 조회 목록 결과
     */
    @Override
    public List<DeviceVO> categoryList(DeviceVO vo) throws Exception {
        return deviceDAO.categoryList(vo);
    }

    /**
     * 검색조건에 해당되는 사용 중인 장비 조회
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO deviceUseInfo(DeviceVO vo) throws Exception {
        return deviceDAO.deviceUseInfo(vo);
    }

    /**
     * 장비 사용 정보 생성
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public void deviceUseInsert(DeviceVO vo) throws Exception {
        deviceDAO.deviceUseInsert(vo);
    }

    /**
     * 장비 사용 상태 업데이트
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public void deviceUseStateUpdate(DeviceVO vo) throws Exception {
        deviceDAO.deviceUseStateUpdate(vo);
    }

    /**
     * 검색조건에 해당되는 장비 리스트 정보
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public List<DeviceVO> deviceList(DeviceVO vo) {
        return deviceDAO.deviceList(vo);
    }

    /**
     * 검색조건에 해당되는 장비 리스트 페이징 정보
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public int deviceListTotalCount(DeviceVO vo) {
        return deviceDAO.deviceListTotalCount(vo);
    }

    /**
     * 장비 상세정보 조회
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public DeviceVO deviceInfo(DeviceVO vo) {
        return deviceDAO.deviceInfo(vo);
    }

    /**
     * 장비 등록
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public int deviceInsert(DeviceVO vo) {
        return deviceDAO.deviceInsert(vo);
    }

    /**
     * 장비 편성 정보 등록
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public int deviceFrmtnInsert(DeviceVO vo) {
        return deviceDAO.deviceFrmtnInsert(vo);
    }

    /**
     * 장비 토큰 존재 유무 조회
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public int searchDeviceToken(DeviceVO vo) {
        return deviceDAO.searchDeviceToken(vo);
    }

    /**
     * 장비 토큰 수정
     *
     * @param DeviceVO
     * @return 처리 결과
     */
    @Override
    public int updateDeviceToken(DeviceVO vo) throws Exception {
        return deviceDAO.updateDeviceToken(vo);
    }

    /**
     * 태그 이용 횟수 조회
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO reservateUsageCount(DeviceVO vo) {
        return deviceDAO.reservateUsageCount(vo);
    }

    /**
     * 태그 사용 종료 정보 업데이트
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public int updateReservateUsageExpiry(DeviceVO vo) {
        return deviceDAO.updateReservateUsageExpiry(vo);
    }

    /**
     * 태그 사용 정보 업데이트
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public int updateReservateUsageCount(DeviceVO vo) {
        return deviceDAO.updateReservateUsageCount(vo);
    }

    /**
     * 장비에 등록된 콘텐츠의 장르 목록 조회
     *
     * @param DeviceVO
     * @return 장비에 등록된 콘텐츠의 장르 목록 조회
     */
    @Override
    public List<DeviceVO> useGenreList(DeviceVO vo) throws Exception {
        return deviceDAO.useGenreList(vo);
    }

    /**
     * 장비 - 이용중인 콘텐츠 종료 시간 업데이트
     *
     * @param DeviceVO
     * @return 업데이트 성공 여부
     */
    @Override
    public int contentPlayTimeUpdate(DeviceVO vo) {
        return deviceDAO.contentPlayTimeUpdate(vo);
    }

    /**
     * 장비 - 이용 시간 업데이트
     *
     * @param DeviceVO
     * @return 업데이트 성공 여부
     */
    @Override
    public int devicePlayUseUpdate(DeviceVO vo) {
        return deviceDAO.devicePlayUseUpdate(vo);
    }

    /**
     * 장비 - 콘텐츠 실행 시간 등록
     *
     * @param DeviceVO
     * @return 등록 성공 여부
     */
    @Override
    public int contentPlayTimeInsert(DeviceVO vo) {
        return deviceDAO.contentPlayTimeInsert(vo);
    }

    /**
     * Device 맥 ID 정보 업데이트
     *
     * @param DeviceVO
     * @return Device Mac 정보 업데이트 성공여부
     */
    @Override
    public int deviceMacUpdate(DeviceVO vo) {
        return deviceDAO.deviceMacUpdate(vo);
    }

    /**
     * IP정보 업데이트
     *
     * @param DeviceVO
     * @return IP정보 업데이트
     */
    @Override
    public int deviceIpUpdate(DeviceVO vo) {
        return deviceDAO.deviceIpUpdate(vo);
    }

    /**
     * 장비 종료 시간 수정
     *
     * @param DeviceVO
     * @return 처리 결과
     */
    @Override
    public int deviceUseUpdate(DeviceVO vo) throws Exception {
        return deviceDAO.deviceUseUpdate(vo);
    }

    /**
     * 장비 상세정보 (+콘텐츠 정보)
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public List<DeviceVO> deviceDetailList(DeviceVO vo) {
        return deviceDAO.deviceDetailList(vo);
    }

    /**
     * 장비 MAC 주소 초기화
     *
     * @param DeviceVO
     * @return 초기화 성공 여부
     */
    @Override
    public int deviceMacUpdateReset(DeviceVO vo) {
        return deviceDAO.deviceMacUpdateReset(vo);
    }

    /**
     * Device 다운 콘텐츠 목록 초기화
     *
     * @param DeviceVO
     * @return 초기화 성공 여부
     */
    @Override
    public int contsUpdateDelete(DeviceVO vo) {
        return deviceDAO.contsUpdateDelete(vo);
    }

    /**
     * Device 정보 수정
     *
     * @param DeviceVO
     * @return 정보 수정 성공 여부
     */
    @Override
    public int deviceUpdate(DeviceVO vo) {
        return deviceDAO.deviceUpdate(vo);
    }

    /**
     * Device 편성 그룹 정보 수정
     *
     * @param DeviceVO
     * @return 편성 그룹 정보 수정
     */
    @Override
    public int deviceFrmtnUpdate(DeviceVO vo) {
        return deviceDAO.deviceFrmtnUpdate(vo);
    }

    /**
     * Device 정보 삭제
     *
     * @param DeviceVO
     * @return Device 정보 삭제 성공 여부
     */
    @Override
    public int deviceDelete(DeviceVO vo) {
        return deviceDAO.deviceDelete(vo);
    }

    /**
     * Device 콘텐츠 다운 목록
     *
     * @param DeviceVO
     * @return Device 콘텐츠 다운 목록 리스트
     */
    @Override
    public List<DeviceVO> deviceContsDownList(DeviceVO vo) {
        return deviceDAO.deviceContsDownList(vo);
    }

    /**
     * 검색조건에 해당되는 Device 콘텐츠 다운 페이징 정보
     *
     * @param DeviceVO
     * @return Device 콘텐츠 다운 페이징 정보
     */
    @Override
    public int deviceContsDownListTotalCount(DeviceVO vo) {
        return deviceDAO.deviceContsDownListTotalCount(vo);
    }

    /**
     * 콘텐츠 카테고리 변경될시, Device Type 변경
     *
     * @param DeviceVO
     * @return Device Type 수정 성공 여부
     */
    @Override
    public int deviceCategoryUpdate(DeviceVO vo) {
        return deviceDAO.deviceCategoryUpdate(vo);
    }

    /*
     * (non-Javadoc)
     * @see kt.com.im.cms.device.service.DeviceService#contentsPlayTimeOut(kt.com.im.cms.device.vo.DeviceVO)
     */
    @Override
    public int contentsPlayTimeOut(DeviceVO vo) {
        return deviceDAO.contentsPlayTimeOut(vo);
    }

    /**
     * Device 사용 상세 정보 수정 (CPU 사용량, 메모리 사용량, 저장공간 정보)
     *
     * @param DeviceVO
     * @return 정보 수정 성공 여부
     */
    @Override
    public int updateDeviceUseDetailInfo(DeviceVO vo) {
        int res = deviceDAO.updateDeviceUseDetailInfo(vo);
        return res;
    }

    /**
     * Device 사용 상세 정보 (CPU 사용량, 메모리 사용량, 저장공간 정보)
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO getDeviceUseDetailInfo(DeviceVO vo) {
        return deviceDAO.getDeviceUseDetailInfo(vo);
    }

    /**
     * Device [동시 이용 인원 정보]
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO playerChk(DeviceVO vo) {
        return deviceDAO.playerChk(vo);
    }

    /**
     * Device [동시 이용 종료 정보]
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO playerEndChk(DeviceVO vo) {
        return deviceDAO.playerEndChk(vo);
    }

    /**
     * Device [이용로그 강제 업데이트]
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public int contentPastPlayTimeUpdate(DeviceVO vo) {
        return deviceDAO.contentPastPlayTimeUpdate(vo);
    }

    /**
     * Device [이용로그 검색 조회]
     *
     * @param DeviceVO
     * @return 조회 결과
     */
    @Override
    public DeviceVO selectUseTimeInfo(DeviceVO vo) {
        return deviceDAO.selectUseTimeInfo(vo);
    }

    /**
     * Device 장르 리스트 검색 조건에 해당되는 콘텐츠 총 개수
     *
     * @param DeviceVO
     * @return
     */
    @Override
    public int useGenreListTotalCount(DeviceVO vo) {
        return deviceDAO.useGenreListTotalCount(vo);
    }

}
