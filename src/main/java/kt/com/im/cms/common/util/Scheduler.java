/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.common.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import kt.com.im.cms.code.service.CodeService;
import kt.com.im.cms.content.service.ContentService;
import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.contentprovider.service.CpService;
import kt.com.im.cms.contentprovider.vo.CpVO;
import kt.com.im.cms.notice.service.NoticeService;
import kt.com.im.cms.notice.vo.NoticeVO;
import kt.com.im.cms.servicenotice.service.ServiceNoticeService;
import kt.com.im.cms.servicenotice.vo.ServiceNoticeVO;
import kt.com.im.cms.serviceterms.service.ServiceTermsService;
import kt.com.im.cms.serviceterms.vo.ServiceTermsVO;

/**
 *
 * 스케줄러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.22
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 19.   A2TEC      최초생성
 *
 *      </pre>
 */

@Component
public class Scheduler {
    @Resource(name = "CpService")
    private CpService cpService;

    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "NoticeService")
    private NoticeService noticeService;

    @Resource(name = "ServiceNoticeService")
    private ServiceNoticeService svcNoticeService;

    @Resource(name="ServiceTermsService")
    private ServiceTermsService svcTermsService;

    @Resource(name = "CodeService")
    private CodeService codeService;

    @Resource(name = "memberTransactionManager")
    private DataSourceTransactionManager txManager;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Inject
    private FileSystemResource fsResource;

    public Scheduler() {
    }

    /** 매일 자정 스케줄링 동작 */
    @Scheduled(cron = "0 0 0 * * ?")
    public void scheduleRun() {
        // CP사 계약 만료 시 CP사 정보 비활성화
        disableCpUpdate();

        // 콘텐츠 공개기간이 만료되거나 CP사 계약이 만료되었을 경우 해당 추천 콘텐츠 비활성화
        disableRcmdContent();

        // 공지사항 게시 일자가 종료되었을 경우 해당 공지사항을 비활성화
        disableNotice();

        // 서비스 공지사항 게시 일자가 종료되었을 경우 해당 서비스 공지사항을 비활성화
        disableSvcNotice();

        // temp 폴더 (썸네일 생성 시 임시 저장소) 내 임시 파일들 삭제
        deleteTempFiles();

        // 게시 기간이 만료되거나 게시 종료되어야할 약관을 비활성화
        disableSvcTerms();
    }

    private void disableCpUpdate() {
        CpVO item = new CpVO();
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = txManager.getTransaction(def);

        try {
            cpService.updateCpContractFinish(item);
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }
    }

    private void disableRcmdContent() {
        ContentVO cItem = new ContentVO();
        // 공개기간 종료되거나 CP사 계약이 만료된 콘텐츠 조회
        List<ContentVO> resultList = contentService.searchFinishContractContents(cItem);

        if (resultList.size() > 0) {
            List<Integer> deleteRcmdContsSeqList = new ArrayList<Integer>();

            for (int i = 0; i < resultList.size(); i++) {
                deleteRcmdContsSeqList.add(resultList.get(i).getContsSeq());
            }

            if (deleteRcmdContsSeqList.size() > 0) {
                DefaultTransactionDefinition dtd = new DefaultTransactionDefinition();
                dtd.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus tStatus = transactionManager.getTransaction(dtd);

                try {
                    cItem.setDelRcmdContsSeqList(deleteRcmdContsSeqList);
                    cItem.setAmdrID("SYSTEM");
                    contentService.deleteRecommendContents(cItem);
                    transactionManager.commit(tStatus);
                } catch (Exception e) {
                    transactionManager.rollback(tStatus);
                } finally {
                    if (!tStatus.isCompleted()) {
                        transactionManager.rollback(tStatus);
                    }
                }
            }
        }
    }

    private void disableNotice() {
        NoticeVO item = new NoticeVO();
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            noticeService.updateNoticeDisplayFinish(item);
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
    }

    private void disableSvcNotice() {
        ServiceNoticeVO item = new ServiceNoticeVO();
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            svcNoticeService.updateSvcNoticeDisplayFinish(item);
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
    }

    private void deleteTempFiles() {
        File file = new File(fsResource.getPath() + "/temp");
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();

                for (int i = 0; i < files.length; i++) {
                    files[i].delete();
                }
            }
        }
    }

    private void disableSvcTerms() {
        ServiceTermsVO vo = new ServiceTermsVO();
        List<Integer> deleteSeqList = new ArrayList<Integer>();
        vo.setFnsDt("Y");
        List<ServiceTermsVO> deleteList = svcTermsService.searchFinishSvcTerms(vo);
        for (int i = 0; i < deleteList.size(); i++) {
            deleteSeqList.add(deleteList.get(i).getStpltSeq());
        }
        vo.setFnsDt("N");
        List<ServiceTermsVO> resultList = svcTermsService.searchFinishSvcTerms(vo);
        for (int i = 0; i < resultList.size(); i++) {
            vo.setSvcSeq(resultList.get(i).getSvcSeq());
            vo.setStpltType(resultList.get(i).getStpltType());
            vo.setStDt(resultList.get(i).getStDt());
            List<ServiceTermsVO> delList = svcTermsService.searchPastSvcTerms(vo);
            for (int j = 0; j < delList.size(); j++) {
                deleteSeqList.add(delList.get(j).getStpltSeq());
            }
        }
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < deleteSeqList.size(); i++) {
            list.add(deleteSeqList.get(i));
        }
        vo.setList(list);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            svcTermsService.updateSvcTermsPstngFinish(vo);
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
    }
}
