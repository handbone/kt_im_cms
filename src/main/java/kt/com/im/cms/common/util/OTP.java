/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.common.util;

import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
*
* OTP 발급 관리 클래스
*
* @author A2TEC
* @since 2018.06.29
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 29.   A2TEC      최초생성
*
*
* </pre>
*/

public class OTP {
    private static final long LIMIT_SECOND = 180000; // 180 sec : 3 minute
    private static final String ALGORITHM = "HmacSHA1";

    private static long createCode(String secretKey, long time) throws Exception {
        byte[] data = new byte[8];

        long value = time;
        for (int i = 8; i-- > 0; value >>>= 8) {
            data[i] = (byte) value;
        }

        Mac mac = Mac.getInstance(ALGORITHM);
        mac.init(new SecretKeySpec(secretKey.getBytes(), ALGORITHM));

        byte[] hash = mac.doFinal(data);
        int offset = hash[20 - 1] & 0xF;

        long truncatedHash = 0;
        for (int i = 0; i < 4; ++i) {
            truncatedHash <<= 8;
            truncatedHash |= hash[offset + i] & 0xFF;
        }

        truncatedHash &= 0x7FFFFFFF;
        truncatedHash %= 1000000;

        return truncatedHash;
    }

    public static String create(String secretKey, long startTime) throws Exception {
        String result = String.format("%06d", createCode(secretKey, startTime));
        return result;
    }

    public static boolean vertify(String secretKey, long startTime, String code) throws Exception {
        startTime = startTime + (new Date().getTime() - startTime) / LIMIT_SECOND;
        return create(secretKey, startTime).equals(code);
    }
}
