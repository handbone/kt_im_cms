/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.common.util;

import java.io.Reader;
import java.util.Properties;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibatis.common.resources.Resources;

/**
*
* property 파일 관리 클래스를 정의한다
*
* @author A2TEC
* @since 2018.09.03
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 9. 03.   A2TEC      최초생성
*
*
* </pre>
*/
@Service("ConfigProperty")
public class ConfigProperty {

    private final String resource = "egovFramework/spring/com/config/config.properties";

    private Properties properties;

    @Autowired
    private StandardPBEStringEncryptor encryptor;

    public ConfigProperty() {
        properties = new Properties();
        try {
            Reader reader = Resources.getResourceAsReader(resource);
            properties.load(reader);
        } catch (Exception e) {
        }
    }

    public String getProperty(String id) {
        return properties.getProperty(id);
    }

    public String getPropertyAfterDecrypt(String id) {
        return encryptor.decrypt(properties.getProperty(id));
    }

    public String getValueAfterEncrypt(String value) {
        return encryptor.encrypt(value);
    }
}