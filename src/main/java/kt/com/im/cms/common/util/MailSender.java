/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.sms.web.SmsApi;

/**
*
* 이메일 발송 클래스를 정의한다
*
* @author A2TEC
* @since 2018.09.05
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 09. 05.   A2TEC      최초생성
*
*
* </pre>
*/

public class MailSender {

    // TODO : jbwook 도메인 발급 시 주소 변경 필요
    private final String OMS_DOMAIN = "oms.ktgigalive.co.kr";

    private final String STORE_DOMAIN = "store.ktgigalive.co.kr";

    private Session session;

    private String subject;

    private String sender;

    private List<InternetAddress> recipients;

    private String content;

    private ConfigProperty configProperty;

    public MailSender(ConfigProperty configProperty) {
        super();
        this.configProperty = configProperty;

        initialize();
    }

    private void initialize() {
        this.recipients = new ArrayList<InternetAddress>();

        Properties props = System.getProperties();
        props.put("mail.smtp.host", configProperty.getProperty("sendMail.domain"));
        props.put("mail.debug", "true");
        props.put("mail.smtp.port", "25");
        props.put("mail.smtp.auth", "false");
        this.session = Session.getDefaultInstance(props);

        this.sender = configProperty.getPropertyAfterDecrypt("sendMail.sender");
    }

    /**
     * 이메일 발송
     *
     * @param MemberVO
     * @return 처리 결과
     * @exception
     */
    public boolean send() {
        boolean sendResult = false;
        try {
            MimeMessage message = new MimeMessage(session);

            message.setSender(new InternetAddress(this.sender));
            message.addRecipients(Message.RecipientType.TO, this.recipients.toArray(new InternetAddress[this.recipients.size()]));
            message.setSubject(this.subject, "UTF-8");
            message.setContent(this.content, "text/html;charset=UTF-8");

            Transport.send(message);
            sendResult = true;
        } catch (Exception e) {
            System.out.println("[MailSender] Failed to send mail (" + this.subject + ")");
        }
        return sendResult;
    }

    /**
     * 회원 등록 알림 콘텐츠
     *
     * @param MemberVO
     * @return String
     * @exception
     */
    public String getRegistMemberContent(MemberVO vo) {
        String title = SmsApi.MSG_TITLE + " OMS";
        String memberId = vo.getMbrId();
        String password = vo.getTempPwd();
        String memberSection = "Master";
        if (MemberApi.MEMBER_SECTION_CMS.equals(vo.getMbrSe())) {
            memberSection = "CMS 관리자";
        } else if (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(vo.getMbrSe())) {
            memberSection = "검수자";
        } else if (MemberApi.MEMBER_SECTION_SERVICE.equals(vo.getMbrSe())) {
            memberSection = "서비스 관리자";
        } else if (MemberApi.MEMBER_SECTION_STORE.equals(vo.getMbrSe())) {
            memberSection = "매장 관리자";
        }

        String mailText = "";
        mailText += "<!DOCTYPE HTML><html><head><meta charset=\"utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">";
        mailText += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
        mailText += "<title>" + title + "</title></head><body style=\"margin:0; padding:0; font-size:14px; font-weight:500;\">";
        mailText += "<div id=\"emailWrap\" style=\"width:520px; height:612px; background-image:url(https://" + OMS_DOMAIN + "/resources/image/img/top_img_mail.jpg); background-repeat:no-repeat; padding:0px 40px 40px 17px; color:#333; letter-spacing:-1px; border:solid 1px #ddd;\">";
        mailText += "<p style=\"color: #ff1111;font-weight: bold;font-size: 14px;opacity: 0.8;vertical-align: middle;margin-top: 10px;text-indent: -22px;margin-bottom: 99px;\">본 메일은 발신전용 메일입니다.</p><div style=\"padding-left:23px\">";
        mailText += "<div class=\"exp\" style=\"width:520px; height:180px; font-size:14px; font-weight:500; line-height:1.6em;text-align:center;\">";
        mailText += "<h1 class=\"title\" style=\"width:520px; font-size:30px; font-weight:600; text-align:center; letter-spacing:-3px; line-height:1.4em;\">";
        mailText += title + " ! <br>신규 계정 발급 !</h1>" + memberSection + "계정이 발급되었습니다. <br>회원님께 유용하고 편리한 사이트를 만들어가도록 최선을 다하겠습니다.<br></div>";
        mailText += "<div class=\"mem\" style=\"margin-bottom:30px;\">";
        mailText += "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:360px; border:solid 1px #101010; margin:0 auto; border-bottom:0;\">";
        mailText += "<tr><th style=\"width:100px; padding:5px; line-height:1.8em; font-weight:500; text-align:center; background-color:#333; color:#fff; border-bottom:solid 1px #333;\">아이디</th>";
        mailText += "<td style=\"padding:5px; border-bottom:solid 1px #333; text-align:center;\">" + memberId + "</td></tr>";
        mailText += "<tr><th style=\"width:100px; padding:5px; line-height:1.8em; font-weight:500; text-align:center; background-color:#333; color:#fff; border-bottom:solid 1px #333;\">임시비밀번호</th>";
        mailText += "<td style=\"padding:5px; border-bottom:solid 1px #333; text-align:center;\">" + password + "</td></tr></table></div>";
        mailText += "<div class=\"guide\" style=\"width:520px; height:100px; font-size:12px; text-align:center; line-height:1.6em;\">";
        mailText += "회원정보는 홈페이지에 로그인하여 개인정보 페이지를 통해 수정 및 확인이 가능합니다.<br>본 메일은 발신전용 이므로 궁금하신 사항은 언제든지 관리자에게 문의해 주세요.</div>";
        mailText += "<div class=\"btnHome\" style=\"cursor:pointer;width:220px; line-height:50px; background-color:#333; border:solid 1px #0e578f; color:#fff; font-size:16px; font-weight:600; text-align:center; margin:0 auto; border-radius:3px; text-shadow:#999 0 0 2px;\">";
        mailText += "<a href=\"https://" + OMS_DOMAIN + "\" style=\"color:#fff; text-decoration:none;\">홈페이지 바로가기</a></div>";
        mailText += "</div></div></body></html>";

        return mailText;
    }

    /**
     * 패스워드 초기화 알림 콘텐츠
     *
     * @param MemberVO
     * @return String
     * @exception
     */
    public String getPasswordContent(MemberVO vo) {
        boolean isContentStore = (MemberApi.MEMBER_SECTION_CP.equals(vo.getMbrSe()));
        String title = isContentStore ? SmsApi.MSG_TITLE + " Store" : SmsApi.MSG_TITLE + " OMS";
        String url = isContentStore ? STORE_DOMAIN : OMS_DOMAIN;
        String memberId = vo.getMbrId();
        String password = vo.getTempPwd();

        String mailText = "";
        mailText += "<!DOCTYPE HTML><html><head><meta charset=\"utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">";
        mailText += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
        mailText += "<title>" + title + "</title></head><body style=\"margin:0; padding:0; font-size:14px; font-weight:500;\">";
        mailText += "<div id=\"emailWrap\" style=\"width:520px; height:612px; background-image:url(https://" + url + "/resources/image/img/top_img_mail.jpg); background-repeat:no-repeat; padding:0px 40px 40px 17px; color:#333; letter-spacing:-1px; border:solid 1px #ddd;\">";
        mailText += "<p style=\"color: #ff1111;font-weight: bold;font-size: 14px;opacity: 0.8;vertical-align: middle;margin-top: 10px;text-indent: -22px;margin-bottom: 99px;\">본 메일은 발신전용 메일입니다.</p><div style=\"padding-left:23px\">";
        mailText += "<div class=\"exp\" style=\"width:520px; height:180px; font-size:14px; font-weight:500; line-height:1.6em;text-align:center;\">";
        mailText += "<h1 class=\"title\" style=\"width:520px; font-size:30px; font-weight:600; text-align:center; letter-spacing:-3px; line-height:1.4em;\">";
        mailText += title + " ! <br>임시 비밀번호 발급 !</h1>비밀번호변경을 위한 임시비밀번호를 발급하였습니다. <br>회원님께 유용하고 편리한 사이트를 만들어가도록 최선을 다하겠습니다.<br></div>";
        mailText += "<div class=\"mem\" style=\"margin-bottom:30px;\">";
        mailText += "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:360px; border:solid 1px #101010; margin:0 auto; border-bottom:0;\">";
        mailText += "<tr><th style=\"width:100px; padding:5px; line-height:1.8em; font-weight:500; text-align:center; background-color:#333; color:#fff; border-bottom:solid 1px #333;\">아이디</th>";
        mailText += "<td style=\"padding:5px; border-bottom:solid 1px #333; text-align:center;\">" + memberId + "</td></tr>";
        mailText += "<tr><th style=\"width:100px; padding:5px; line-height:1.8em; font-weight:500; text-align:center; background-color:#333; color:#fff; border-bottom:solid 1px #333;\">임시비밀번호</th>";
        mailText += "<td style=\"padding:5px; border-bottom:solid 1px #333; text-align:center;\">" + password + "</td></tr></table></div>";
        mailText += "<div class=\"guide\" style=\"width:520px; height:100px; font-size:12px; text-align:center; line-height:1.6em;\">";
        mailText += "회원정보는 홈페이지에 로그인하여 개인정보 페이지를 통해 수정 및 확인이 가능합니다.<br>본 메일은 발신전용 이므로 궁금하신 사항은 언제든지 관리자에게 문의해 주세요.</div>";
        mailText += "<div class=\"btnHome\" style=\"cursor:pointer;width:220px; line-height:50px; background-color:#333; border:solid 1px #0e578f; color:#fff; font-size:16px; font-weight:600; text-align:center; margin:0 auto; border-radius:3px; text-shadow:#999 0 0 2px;\">";
        mailText += "<a href=\"https://" + url + "\" style=\"color:#fff; text-decoration:none;\">홈페이지 바로가기</a></div>";
        mailText += "</div></div></body></html>";

        return mailText;
    }

    /**
     * 카테고리 신규 등록 알림 콘텐츠
     *
     * @param MemberVO
     * @return String
     * @exception
     */
    public String getRegistCategoryContent(String firstCtgNm, String secondCtgNm) {
        String title = SmsApi.MSG_TITLE + " OMS";

        String mailText = "";
        mailText += "<!DOCTYPE HTML><html><head><meta charset=\"utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">";
        mailText += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
        mailText += "<title>" + title + "</title></head><body style=\"margin:0; padding:0; font-size:14px; font-weight:500;\">";
        mailText += "<div id=\"emailWrap\" style=\"width:520px; height:612px; background-image:url(https://" + OMS_DOMAIN + "/resources/image/img/top_img_mail.jpg); background-repeat:no-repeat; padding:0px 40px 40px 17px; color:#333; letter-spacing:-1px; border:solid 1px #ddd;\">";
        mailText += "<p style=\"color: #ff1111;font-weight: bold;font-size: 14px;opacity: 0.8;vertical-align: middle;margin-top: 10px;text-indent: -22px;margin-bottom: 99px;\">본 메일은 발신전용 메일입니다.</p><div style=\"padding-left:23px\">";
        mailText += "<div class=\"exp\" style=\"width:520px; height:180px; font-size:14px; font-weight:500; line-height:1.6em;text-align:center;\">";
        mailText += "<h1 class=\"title\" style=\"width:520px; font-size:30px; font-weight:600; text-align:center; letter-spacing:-3px; line-height:1.4em;\">";
        mailText += title + " ! <br>신규 카테고리 등록 !</h1>신규 카테고리가 등록 완료되었습니다. <br>회원님께 유용하고 편리한 사이트를 만들어가도록 최선을 다하겠습니다.<br></div>";
        mailText += "<div class=\"mem\" style=\"margin-bottom:30px;\">";
        mailText += "<table cellspacing=\"0\" cellpadding=\"0\" style=\"width:360px; border:solid 1px #101010; margin:0 auto; border-bottom:0;\">";
        mailText += "<tr><th style=\"width:100px; padding:5px; line-height:1.8em; font-weight:500; text-align:center; background-color:#333; color:#fff; border-bottom:solid 1px #333;\">상위 카테고리</th>";
        mailText += "<td style=\"padding:5px; border-bottom:solid 1px #333; text-align:center;\">" + firstCtgNm + "</td></tr>";
        mailText += "<tr><th style=\"width:100px; padding:5px; line-height:1.8em; font-weight:500; text-align:center; background-color:#333; color:#fff; border-bottom:solid 1px #333;\">하위 카테고리</th>";
        mailText += "<td style=\"padding:5px; border-bottom:solid 1px #333; text-align:center;\">" + secondCtgNm + "</td></tr></table></div>";
        mailText += "<div class=\"guide\" style=\"width:520px; height:100px; font-size:12px; text-align:center; line-height:1.6em;\">";
        mailText += "<br>본 메일은 발신전용 이므로 궁금하신 사항은 언제든지 관리자에게 문의해 주세요.</div>";
        mailText += "<div class=\"btnHome\" style=\"cursor:pointer;width:220px; line-height:50px; background-color:#333; border:solid 1px #0e578f; color:#fff; font-size:16px; font-weight:600; text-align:center; margin:0 auto; border-radius:3px; text-shadow:#999 0 0 2px;\">";
        mailText += "<a href=\"https://" + OMS_DOMAIN + "\" style=\"color:#fff; text-decoration:none;\">홈페이지 바로가기</a></div>";
        mailText += "</div></div></body></html>";

        return mailText;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void addRecipient(String address) {
        try {
            InternetAddress item = new InternetAddress(address);
            if (this.recipients.contains(item)) {
                return;
            }
            this.recipients.add(item);
        } catch (Exception e) {
            System.out.println("[MailSender] Failed to add recipient (" + address + ")");
        }
    }

    public void removeRecipient(String address) {
        try {
            InternetAddress item = new InternetAddress(address);
            if (!this.recipients.contains(item)) {
                return;
            }
            this.recipients.remove(item);
        } catch (Exception e) {
            System.out.println("[MailSender] Failed to remove recipient (" + address + ")");
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
