/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.common.util;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 메뉴 관련 VO
 *
 * @author A2TEC
 * @since 2018.07.16
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 7. 16.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class MenuVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = 1953460508486908795L;

    /** 메뉴 명 */
    private String name;

    /** 상위 메뉴 index */
    private int upperIndex = 0;

    /** 메뉴 index */
    private int index = 0;

    /** 메뉴 URI */
    private String uri;

    /** 메뉴명 메시지 아이디 */
    private String nameMsgId;

    /** 추가 Class명 */
    private String addClass;

    /** 접근 권한 */
    private List<String> authorityList;

    /** 포함 된 하위 메뉴 */
    private List<String> containsURIList;

    /** 서비스 구분 (ONLINE, OFFLINE) */
    private String svcType;

    /** 포커스 여부 */
    boolean isFocused = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUpperIndex() {
        return upperIndex;
    }

    public void setUpperIndex(int upperIndex) {
        this.upperIndex = upperIndex;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getNameMsgId() {
        return nameMsgId;
    }

    public void setNameMsgId(String nameMsgId) {
        this.nameMsgId = nameMsgId;
    }

    public String getAddClass() {
        return addClass;
    }

    public void setAddClass(String addClass) {
        this.addClass = addClass;
    }

    public List<String> getAuthorityList() {
        return authorityList;
    }

    public void setAuthorityList(List<String> authorityList) {
        this.authorityList = authorityList;
    }

    public List<String> getContainsURIList() {
        return containsURIList;
    }

    public void setContainsURIList(List<String> containsURIList) {
        this.containsURIList = containsURIList;
    }

    public String getSvcType() {
        return svcType;
    }

    public void setSvcType(String svcType) {
        this.svcType = svcType;
    }

    public boolean getIsFocused() {
        return isFocused;
    }

    public void setIsFocused(boolean isFocused) {
        this.isFocused = isFocused;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
