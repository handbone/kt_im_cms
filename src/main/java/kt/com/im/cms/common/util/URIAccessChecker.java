/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

/**
 *
 * URI 접근 권한 검증 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.07.13
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 7. 13.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Component
public class URIAccessChecker {

    // 멤버 권한 코드
    private final static String MASTER = "01";

    private final static String CMS = "02";

    private final static String ACCEPTOR = "03";

    private final static String SERVICE = "04";

    private final static String STORE = "05";

    private final static String CP = "06";

    public static Map<String, List<String>> URIAuthorityMap;

    public boolean enabledLog = false;

    public URIAccessChecker() {
        initUrlMap();
    }

    static public void initUrlMap() {
        if (URIAuthorityMap != null) {
            return;
        }
        URIAuthorityMap = new HashMap<String, List<String>>();

        /*** 오류 페이지 시작 ***/
        URIAuthorityMap.put("/error", getAuthorityList(MASTER, CMS, ACCEPTOR, SERVICE, STORE));
        /*** 오류 페이지 끝 ***/

        /*** 마이페이지 시작 ***/
        // 사용자 상세정보
        URIAuthorityMap.put("/mypage", getAuthorityList(MASTER, CMS, ACCEPTOR, SERVICE, STORE));

        // 사용자 상세정보 > 사용자 상세정보 수정
        URIAuthorityMap.put("/mypage/edit", getAuthorityList(MASTER, CMS, ACCEPTOR, SERVICE, STORE));
        /*** 마이페이지 끝 ***/

        /*** 대시보드 목록 시작 ***/
        // 대시보드 > 콘텐츠현황
        URIAuthorityMap.put("/dashboard/contents", getAuthorityList(MASTER, CMS, ACCEPTOR, SERVICE, STORE));

        // 대시보드 > 콘텐츠현황 > 콘텐츠 전체 현황
        URIAuthorityMap.put("/dashboard/contents/verify", getAuthorityList(MASTER, CMS, ACCEPTOR, SERVICE));

        // 대시보드 > 콘텐츠현황 > 등록 콘텐츠 현황
        URIAuthorityMap.put("/dashboard/contents/registered", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 대시보드 > 매장현황 > 매장현황
        URIAuthorityMap.put("/dashboard/store", getAuthorityList(MASTER, CMS, SERVICE, STORE));
        /*** 대시보드 목록 끝 ***/

        /*** 사용자 관리 시작 ***/
        // 사용자 관리 > 계정관리 > 등록 계정 목록 & 등록 계정 상세 정보
        URIAuthorityMap.put("/member", getAuthorityList(MASTER, CMS, SERVICE));

        // 사용자 관리 > 계정관리 > 계정 신규 등록
        URIAuthorityMap.put("/member/regist", getAuthorityList(MASTER, CMS, SERVICE));

        // 사용자 관리 > 계정관리 > 등록 계정 정보 수정
        URIAuthorityMap.put("/member/edit", getAuthorityList(MASTER, CMS, SERVICE));

        // 사용자 관리 > 계정 승인 관리 > 승인 요청 목록 & 승인 요청 상세정보
        URIAuthorityMap.put("/member/waiting", getAuthorityList(MASTER, CMS));

        // 사용자 관리 > 서비스 관리 > 서비스 목록 & 서비스 상세정보
        URIAuthorityMap.put("/service", getAuthorityList(MASTER, CMS));

        // 사용자 관리 > 서비스 관리 > 서비스 등록
        URIAuthorityMap.put("/service/regist", getAuthorityList(MASTER, CMS));

        // 사용자 관리 > 서비스 관리 > 서비스 수정
        URIAuthorityMap.put("/service/edit", getAuthorityList(MASTER, CMS));

        // 사용자 관리 > CP사 관리 > CP사 목록 & CP사 상세정보
        URIAuthorityMap.put("/cp", getAuthorityList(MASTER, CMS));

        // 사용자 관리 > CP사 관리 > CP사 등록
        URIAuthorityMap.put("/cp/regist", getAuthorityList(MASTER, CMS));

        // 사용자 관리 > CP사 관리 > CP사 수정
        URIAuthorityMap.put("/cp/edit", getAuthorityList(MASTER, CMS));

        // 사용자 관리 > 인증 회원 관리 > 인증 회원 관리
        URIAuthorityMap.put("/member/integrate", getAuthorityList(MASTER, CMS));
        /*** 사용자 관리 끝 ***/

        /*** 콘텐츠 관리 목록 시작 ***/
        // 콘텐츠 관리 > 콘텐츠 통합 정보 > 콘텐츠 목록 & 콘텐츠 상세정보
        URIAuthorityMap.put("/contents", getAuthorityList(MASTER, CMS, ACCEPTOR));

        // 콘텐츠 관리 > 검수 관리 > 검수 요청 목록 & 검수 결과 목록 & 검수 이력
        URIAuthorityMap.put("/verify", getAuthorityList(MASTER, CMS, ACCEPTOR));

        // 콘텐츠 관리 > 검수 관리 > 검수 콘텐츠 상세정보
        URIAuthorityMap.put("/verify/edit", getAuthorityList(MASTER, CMS, ACCEPTOR));

        // 콘텐츠 관리 > 계약/과금 관리> 수급 정보 목록 / 상세
        URIAuthorityMap.put("/contract", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 계약/과금 관리 > CP사별 수급 정보 상세 정보
        URIAuthorityMap.put("/contractDetail", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 계약/과금 관리 > CP사별 수급 정보 수정
        URIAuthorityMap.put("/contractDetail/edit", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 계약/과금 관리 > CP사별 수급 정보 등록
        URIAuthorityMap.put("/contractDetail/regist", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 계약/과금 관리 > 서비스(매장)별 배포 정보 목록 / 리스트
        URIAuthorityMap.put("/billing", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 계약/과금 관리 > 서비스(매장)별 계약 목록 목록 / 상세
        URIAuthorityMap.put("/billingDetail", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 계약/과금 관리 > 서비스(매장)별 배포 정보 수정
        URIAuthorityMap.put("/billingDetail/edit", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 계약/과금 관리 > 서비스(매장)별 배포 정보 등록
        URIAuthorityMap.put("/billingDetail/regist", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 전시페이지 관리 > 전시관리 & 전시 완료 이력
        URIAuthorityMap.put("/display", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 전시페이지 관리 > 전시 콘텐츠 상세정보
        URIAuthorityMap.put("/display/edit", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 편성관리 > 편성 그룹 목록 & 편성 이력
        URIAuthorityMap.put("/cms/release", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 편성관리 > 편성 그룹 수정
        URIAuthorityMap.put("/cms/release/regist", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 편성관리 > 편성 그룹 수정
        URIAuthorityMap.put("/cms/release/edit", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 편성관리 > 편성 이력 상세정보
        URIAuthorityMap.put("/cms/release/stat", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 카테고리 관리 > 카테고리 목록
        URIAuthorityMap.put("/contents/category", getAuthorityList(MASTER, CMS));

        // 콘텐츠 관리 > 장르 관리 > 장르 목록
        URIAuthorityMap.put("/contents/genre", getAuthorityList(MASTER, CMS));
        /*** 콘텐츠 관리 목록 끝 ***/

        /*** 서비스 매장 관리 시작 ***/
        // 서비스 매장 관리 > 매장관리 > 매장 목록 & 매장 상세정보
        URIAuthorityMap.put("/store", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 매장관리 > 매장 등록
        URIAuthorityMap.put("/store/regist", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 매장관리 > 매장 정보 수정
        URIAuthorityMap.put("/store/edit", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 이용권 관리 > 이용권 목록 & 이용권 상세정보
        URIAuthorityMap.put("/product", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 이용권 관리 > 이용권 등록
        URIAuthorityMap.put("/product/regist", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 이용권 관리 > 이용권 수정
        URIAuthorityMap.put("/product/edit", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 이용권 발급 관리 > 이용권 발급 목록 & 이용권 발급 상세정보
        URIAuthorityMap.put("/reservate", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 이용권 발급 관리 > 이용권 발급 수정
        URIAuthorityMap.put("/reservate/edit", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 예약 관리 > 예약 목록 & 예약 시간 설정
        URIAuthorityMap.put("/reservation", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 콘텐츠 관리 > 콘텐츠 목록 & 콘텐츠 상세정보 & 콘텐츠 그룹 목록
        URIAuthorityMap.put("/mms/contents", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 콘텐츠 관리 > 콘텐츠 그룹 등록
        URIAuthorityMap.put("/mms/release/regist", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 콘텐츠 관리 > 콘텐츠 그룹 수정
        URIAuthorityMap.put("/mms/release", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 과금 관리 > 과금 정보 목록 & 과금 계약 상세정보
        URIAuthorityMap.put("/service/contract", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > Device 관리 > Device 설치 목록 & Device 상세 정보 & Device 사용 현황 & Device별 다운로드 콘텐츠
        URIAuthorityMap.put("/device", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > Device 관리 > Device 등록
        URIAuthorityMap.put("/device/regist", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > Device 관리 > Device 수정
        URIAuthorityMap.put("/device/edit", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > Device 버전관리 > Device 버전 목록 & Device 버전 상세정보
        URIAuthorityMap.put("/version", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > Device 버전관리 > Device 버전 등록
        URIAuthorityMap.put("/version/regist", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > Device 버전관리 > Device 버전 수정
        URIAuthorityMap.put("/version/edit", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 검색 키워드 관리
        URIAuthorityMap.put("/keyword", getAuthorityList(MASTER, CMS, SERVICE));

        // 서비스 매장 관리 > 고객센터 관리 > 서비스 공지사항 목록 & 서비스 FAQ 목록
        URIAuthorityMap.put("/servicecustomer", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 고객센터 관리 > 서비스 공지사항 상세정보
        URIAuthorityMap.put("/servicecustomer/notice", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 고객센터 관리 > 서비스 공지사항 등록
        URIAuthorityMap.put("/servicecustomer/notice/regist", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 고객센터 관리 > 서비스 공지사항 수정
        URIAuthorityMap.put("/servicecustomer/notice/edit", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 고객센터 관리 > 서비스 FAQ 상세정보
        URIAuthorityMap.put("/servicecustomer/faq", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 고객센터 관리 > 서비스 FAQ 등록
        URIAuthorityMap.put("/servicecustomer/faq/regist", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 서비스 매장 관리 > 고객센터 관리 > 서비스 FAQ 수정
        URIAuthorityMap.put("/servicecustomer/faq/edit", getAuthorityList(MASTER, CMS, SERVICE, STORE));
        /*** 서비스 매장 관리 끝 ***/

        /*** 온라인 서비스 관리 시작 ***/
        // 온라인 서비스 관리 > 버전 관리
        URIAuthorityMap.put("/onlineSvc/version", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineSvc/version/regist", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineSvc/version/edit", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 배너 관리
        URIAuthorityMap.put("/onlineService/banr", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineService/banr/regist", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineService/banr/edit", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineService/banr/hst", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineService/cprtSvc", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineService/cprtSvc/edit", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineService/cprtSvc/regist", getAuthorityList(MASTER, CMS, SERVICE));
        URIAuthorityMap.put("/onlineService/banr/plcyMng", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 메인 영역 관리
        URIAuthorityMap.put("/onlineSvc/main/app", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 메인 영역 관리 > App/콘텐츠 등록
        URIAuthorityMap.put("/onlineSvc/main/app/regist", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 메인 영역 관리 > App/콘텐츠 수정
        URIAuthorityMap.put("/onlineSvc/main/app/edit", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 약관 관리 목록 & 약관 관리 상세정보
        URIAuthorityMap.put("/onlineService/terms", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 약관 관리 등록
        URIAuthorityMap.put("/onlineService/terms/regist", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 약관 관리 수정
        URIAuthorityMap.put("/onlineService/terms/edit", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 고객센터 관리 > 공지사항 관리/FAQ 관리 
        URIAuthorityMap.put("/onlineSvc/customer", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 고객센터 관리 > 공지사항 상세정보 
        URIAuthorityMap.put("/onlineSvc/customer/notice", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 고객센터 관리 > 공지사항 등록 
        URIAuthorityMap.put("/onlineSvc/customer/notice/regist", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 고객센터 관리 > 공지사항 수정 
        URIAuthorityMap.put("/onlineSvc/customer/notice/edit", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 고객센터 관리 > FAQ 상세정보 
        URIAuthorityMap.put("/onlineSvc/customer/faq", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 고객센터 관리 > FAQ 등록
        URIAuthorityMap.put("/onlineSvc/customer/faq/regist", getAuthorityList(MASTER, CMS, SERVICE));

        // 온라인 서비스 관리 > 고객센터 관리 > FAQ 수정
        URIAuthorityMap.put("/onlineSvc/customer/faq/edit", getAuthorityList(MASTER, CMS, SERVICE));
        /*** 온라인 서비스 관리 끝 ***/

        /*** 고객센터 시작 ***/
        // 고객센터 > 공지사항 > 공지사항 목록 & 공지사항 상세정보
        URIAuthorityMap.put("/customer/notice", getAuthorityList(MASTER, CMS, ACCEPTOR, SERVICE, STORE));

        // 고객센터 > 공지사항 > 공지사항 등록
        URIAuthorityMap.put("/customer/notice/regist", getAuthorityList(MASTER, CMS));

        // 고객센터 > 공지사항 > 공지사항 수정
        URIAuthorityMap.put("/customer/notice/edit", getAuthorityList(MASTER, CMS));

        // 고객센터 > FAQ > FAQ 목록 & FAQ 상세정보
        URIAuthorityMap.put("/customer/faq", getAuthorityList(MASTER, CMS, ACCEPTOR, SERVICE, STORE));

        // 고객센터 > FAQ > FAQ 등록
        URIAuthorityMap.put("/customer/faq/regist", getAuthorityList(MASTER, CMS));

        // 고객센터 > FAQ > FAQ 수정
        URIAuthorityMap.put("/customer/faq/edit", getAuthorityList(MASTER, CMS));

        // 고객센터 > 문의사항 > 문의사항 목록 & 문의사항 상세정보
        URIAuthorityMap.put("/customer/qna", getAuthorityList(MASTER, CMS));

        // 고객센터 > 문의사항 > 문의사항 답변
        URIAuthorityMap.put("/customer/qna/registAns", getAuthorityList(MASTER, CMS));
        /*** 고객센터 끝 ***/

        /*** 통계 시작 ***/
        // 통계 > 유입통계 > 유입통계
        URIAuthorityMap.put("/statistics/traffic", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > 콘텐츠 현황 통계 > 콘텐츠 등록 현황
        URIAuthorityMap.put("/statistics/contents/registered", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > 콘텐츠 현황 통계 > 콘텐츠 사용 현황
        URIAuthorityMap.put("/statistics/contents/used", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > CP사 통계 > CP사 통계
        URIAuthorityMap.put("/statistics/cp", getAuthorityList(MASTER, CMS));

        // 통계 > 매장 현황 통계 > 이용객 통계
        URIAuthorityMap.put("/statistics/store/visitor", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > 매장 현황 통계 > 이용권 통계
        URIAuthorityMap.put("/statistics/store/tag", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > 매장 현황 통계 > 이용권 통계 상세
        URIAuthorityMap.put("/statistics/store/tagStore", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > 매장 현황 통계 > 이용권별 이용콘텐츠
        URIAuthorityMap.put("/statistics/store/contentsByTag", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > 온라인 현황 통계 > 콘텐츠 사용 현황
        URIAuthorityMap.put("/statistics/online/usedOlsvcContents", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > 검수 현황 통계
        URIAuthorityMap.put("/statistics/verifier", getAuthorityList(MASTER, CMS, ACCEPTOR));

        // 통계 > 매출 통계 > 결제 관리
        URIAuthorityMap.put("/statistics/payInfo", getAuthorityList(MASTER, CMS, SERVICE, STORE));

        // 통계 > 블록체인 통계
        URIAuthorityMap.put("/statistics/blockStats", getAuthorityList(MASTER, CMS));

        // 통계 > 런처 통계 > 유입 통계
        URIAuthorityMap.put("/statistics/olSvcLauncher/traffic", getAuthorityList(MASTER, CMS, SERVICE));

        // 통계 > 런처 통계 > > 이용 통계
        URIAuthorityMap.put("/statistics/olSvcLauncher/usedAppDetail", getAuthorityList(MASTER, CMS, SERVICE));

        // 통계 > 런처 통계 > 콘텐츠 재생 통계
        URIAuthorityMap.put("/statistics/olSvcLauncher/usedConts", getAuthorityList(MASTER, CMS, SERVICE));
        /*** 통계 끝 ***/

        /*** 시스템 관리 시작 ***/
        // 시스템 관리 > DB 관리 > DB 관리
        URIAuthorityMap.put("/system/db", getAuthorityList(MASTER, CMS));

        // 시스템 관리 > DB 관리 > DB 정보 등록
        URIAuthorityMap.put("/system/db/regist", getAuthorityList(MASTER, CMS));

        // 시스템 관리 > DB 관리 > DB 정보 수정
        URIAuthorityMap.put("/system/db/edit", getAuthorityList(MASTER, CMS));

        // 시스템 관리 > 코드 관리 > 코드 관리
        URIAuthorityMap.put("/system/code", getAuthorityList(MASTER, CMS));
        /*** 시스템 관리 끝 ***/
    }

    public boolean check(HttpServletRequest request) {
        boolean result = this.checkAuthority(request);
        return result;
    }

    public boolean checkAuthority(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            if (enabledLog) {
                System.out.println("[URIAccessChecker] Session is null");
            }
            return false;
        }

        if (session.getAttribute("seq") == null || session.getAttribute("id") == null
                || session.getAttribute("name") == null || session.getAttribute("memberSec") == null) {
            if (enabledLog) {
                System.out.println("[URIAccessChecker] Session is invalid");
            }
            return false;
        }

        String uri = getShortURI(request.getRequestURI().replaceAll(request.getContextPath(), ""));
        if (enabledLog) {
            System.out.println("[URIAccessChecker] Check uri = " + uri);
        }
        if (!URIAuthorityMap.containsKey(uri)) {
            if (enabledLog) {
                System.out.println("[URIAccessChecker] URI(" + uri + ") not contains in map");
            }
            return false;
        }

        String memberSection = (String) session.getAttribute("memberSec");
        List<String> authorityList = URIAuthorityMap.get(uri);

        boolean hasAccessAuthority = (authorityList.indexOf(memberSection) != -1);

        if (enabledLog) {
            System.out.println("[URIAccessChecker] hasAccessAuthority = " + hasAccessAuthority + " memberSection = "
                    + memberSection);
        }
        return hasAccessAuthority;
    }

    public static List<String> getAuthorityList(String... args) {
        List<String> list = new ArrayList<String>();
        for (String str : args) {
            list.add(str);
        }
        return list;
    }

    public String getShortURI(String value) {
        if (value == null || value.isEmpty()) {
            return value;
        }

        // 에러페이지의 경우 URI 숫자 제거 하지 않음
        if (value.contains("error")) {
            return removeSlash(value);
        }

        // 숫자 제거 (상세정보화면의 숫자가 포함된 URI의 경우 Map에 정적 데이터로 저장할 수 없어서 예외처리)
        value = value.replaceAll("[0-9]", "");
        value = removeSlash(value);

        return value;
    }

    static public String removeSlash(String value) {
        if (value == null || value.isEmpty()) {
            return value;
        }

        // 슬러시 제거
        int lastSlashIndex = 0;
        String result = "";
        for (int i = 0; i < value.length(); i++) {
            char ch = value.charAt(i);
            if ('/' == ch) {
                if ((lastSlashIndex + 1) == i) {
                    lastSlashIndex++;
                    continue;
                }
                lastSlashIndex = i;
            }
            result += value.charAt(i);
        }
        char ch = result.charAt(result.length() - 1);
        if ('/' == ch) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }
}
