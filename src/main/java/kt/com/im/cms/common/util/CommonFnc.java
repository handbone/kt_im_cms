/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.common.util;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;

public class CommonFnc {
    private static String[] days = { "일", "월", "화", "수", "목", "금", "토"};

    public static String requiredChecking(HttpServletRequest request, String mandatory) {

        String[] mandatoryString = mandatory.split(",");
        String str = "";
        for (int i = 0; i < mandatoryString.length; i++) {
            if (request.getParameter(mandatoryString[i]) == null
                    || request.getParameter(mandatoryString[i]).equals("")) {
                if (!str.equals(""))
                    str += ",";
                str += mandatoryString[i];
            }
        }

        return str;
    }

    public static String getMethod(HttpServletRequest request) {
        String method = "GET";
        if (request.getMethod().equals("GET"))
            return method;

        method = request.getParameter("_method");
        if (method == null)
            method = "POST";
        return method;
    }

    public static void voInfo(Object obj) {
        Class<?> clazz = obj.getClass();
        Class<?> superClazz = clazz.getSuperclass();
        Field[] superFields = superClazz.getDeclaredFields();

        System.out.println("■■■■■■■■■■■■■■■■■■■■■■■ VO INFO Start ■■■■■■■■■■■■■■■■■■■■■■■");
        System.out.print(clazz.getName() + " => ");

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.print(field.getName() + "=" + getValue(field, obj) + ", ");
        }

        for (Field superField : superFields) {
            System.out.print(superField.getName() + "=" + getValue(superField, obj) + ", ");
        }
        System.out.println("");
        System.out.println("■■■■■■■■■■■■■■■■■■■■■■■ VO INFO End ■■■■■■■■■■■■■■■■■■■■■■■");
    }

    private static Object getValue(Field field, Object obj) {
        field.setAccessible(true);
        try {
            return field.get(obj);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            return "ERROR";
        }
    }

    public static String emptyCheckString(String str, HttpServletRequest request) {
        String returnVal = "";
        if (request.getParameter(str) != null && !request.getParameter(str).isEmpty()) {
            returnVal = request.getParameter(str);
        }
        return returnVal;
    }

    public static int emptyCheckInt(String intVal, HttpServletRequest request) {
        int returnVal = 0;
        if (request.getParameter(intVal) != null && !request.getParameter(intVal).isEmpty()) {
            returnVal = Integer.parseInt(request.getParameter(intVal));
        }
        return returnVal;
    }

    public static int emptyCheckIntRetVal(String intVal, HttpServletRequest request, int retVal) {
        int returnVal = retVal;
        if (request.getParameter(intVal) != null && !request.getParameter(intVal).isEmpty()) {
            returnVal = Integer.parseInt(request.getParameter(intVal));
        }
        return returnVal;
    }

    public static String getClientIp(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    public static boolean checkReqParameter(HttpServletRequest request, String chkNm) {
        Map<String, String[]> paramMap = request.getParameterMap();
        return paramMap.containsKey(chkNm);
    }

    public static String getDayOfWeek(String dateString) throws ParseException {
        if (dateString == null || dateString.isEmpty()) {
            return "";
        }

        dateString = dateString.replaceAll("\\.", "").replaceAll("-", "");
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = dateFormat.parse(dateString);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return days[calendar.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public static String getNameMask(String name) {
        if (name == null || name.isEmpty()) {
            return "";
        }

        String maskStr = "";
        String nonMaskStr = "";
        int nameLen = name.length();

        // 이름이 3자 미만일 경우 마지막 글자를 마스킹
        if (nameLen < 3) {
            for (int i = 0; i < nameLen; i++) {
                if (i == nameLen - 1) {
                    maskStr += "*";
                } else {
                    maskStr += name.substring(i, i + 1);
                }
            }
            return maskStr;
        }

        int maskLen = Math.round(nameLen / 3);
        nonMaskStr = name.substring(0, nameLen - maskLen);
        maskStr = name.substring(nameLen - maskLen, nameLen);

        for (int i = 0; i < maskStr.length(); i++) {
            maskStr = maskStr.replace(maskStr.substring(i, i+ 1), "*");
        }

        return nonMaskStr + maskStr;
    }

    public static String getIdMask(String id) {
        if (id == null || id.isEmpty()) {
            return "";
        }

        /* 아이디의 끝 3자리를 마스킹 처리함. 만약 아이디가 3자리 이하일 경우 모두 마스킹 처리함. */
        int idLen = id.length();
        String nonMaskStr = "";
        String maskStr = "";
        if (idLen <= 3) {
            char[] c = new char[idLen];
            Arrays.fill(c, '*');
            maskStr = id.replace(id, String.valueOf(c));
        } else {
            nonMaskStr = id.substring(0, idLen - 3);
            maskStr = id.substring(idLen - 3, idLen);
            maskStr = maskStr.replace(maskStr, "***");
        }

        return nonMaskStr + maskStr;
    }

    public static String getEmailMask(String email) {
        if (email == null || email.isEmpty()) {
            return "";
        }

        String regex = "\\b(\\S+)+@(\\S+.\\S+)";
        Matcher matcher = Pattern.compile(regex).matcher(email);

        String maskEmail = email;
        if (matcher.find()) {
            String id = matcher.group(1); // 마스킹 처리할 부분인 userId

            /*
            * userId의 길이를 기준으로 세글자 초과인 경우 뒤 세자리를 마스킹 처리하고,
            * 세글자인 경우 뒤 두글자만 마스킹,
            * 세글자 미만인 경우 모두 마스킹 처리
            */
            int length = id.length();
            if (length < 3) {
                char[] c = new char[length];
                Arrays.fill(c, '*');
                maskEmail = email.replace(id, String.valueOf(c));
            } else if (length == 3) {
                maskEmail = email.replaceAll("\\b(\\S+)[^@][^@]+@(\\S+)", "$1**@$2");
            } else {
                maskEmail = email.replaceAll("\\b(\\S+)[^@][^@][^@]+@(\\S+)", "$1***@$2");
            }
        }

        return maskEmail;
    }

    public static String getTelnoMask(String telno) {
        if (telno == null || telno.isEmpty()) {
            return "";
        }

        /*
         * 전화번호 2번째 자리는 뒤에 2자 **, ex) 1234 -> 12** 
         * 전화번호 3번째 자리는 첫자만 *, ex) 1234 -> *234
         */
        String regex = "^(\\d{1,3})-?(\\d{1,2})\\d{2}-?\\d(\\d{3})$";

        String[] telNoList = telno.split(";");
        String maskTelno = "";
        for (int i = 0; i < telNoList.length; i++) {
            if (i != 0) {
                maskTelno += ";";
            }
            Matcher matcher = Pattern.compile(regex).matcher(telNoList[i]);
            String Telno = telNoList[i];
            if (matcher.find()) {
                maskTelno += matcher.group(1) + "-" + matcher.group(2) + "**-*" + matcher.group(3);
            }
        }

        return maskTelno;
    }

    public static String getIpAddrMask(String ipAddr) {
        if (ipAddr == null || ipAddr.isEmpty()) {
            return "";
        }

        /* IP Address 숨김 처리 - ex) ***.123.***.123 */
        String regex = "^(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})$";

        Matcher matcher = Pattern.compile(regex).matcher(ipAddr);
        String maskIpAddr = ipAddr;
        if (matcher.find()) {
            maskIpAddr = "***." + matcher.group(2) + ".***." + matcher.group(4);
        }

        return maskIpAddr;
    }

    public static String getBiznoMask(String bizno) {
        if (bizno == null || bizno.isEmpty()) {
            return "";
        }

        /* 사업자번호 뒤 5자리 숨김 */
        String regex = "^(\\d{3})-(\\d{2})-(\\d{5})$";

        Matcher matcher = Pattern.compile(regex).matcher(bizno);
        String maskBizno = bizno;
        if (matcher.find()) {
            maskBizno = matcher.group(1) + "-" + matcher.group(2) + "-*****";
        }

        return maskBizno;
    }

    public static String getAddrMask(String addr) {
        if (addr == null || addr.isEmpty()) {
            return "";
        }

        /* 주소는 구주소인 경우 동까지, 신주소인 경우 길까지 표시 */
        String regex = "(([가-힣]+(\\d{1,5}|\\d{1,5}(,|.)\\d{1,5}|)+(읍|면|동|가|리))(^구|)((\\d{1,5}(~|-)\\d{1,5}|\\d{1,5})(가|리|)|))([ ](산(\\d{1,5}(~|-)\\d{1,5}|\\d{1,5}))|)|(([가-힣]|(\\d{1,5}(~|-)\\d{1,5})|\\d{1,5})+(로|길))";

        String[] addrStrArr = addr.split(" ");
        String str = "";
        String resultStr = "";
        int matchCnt = 0;

        for (int i = 0; i < addrStrArr.length; i++) {
            str += addrStrArr[i] + " ";

            if (addrStrArr[i].matches(regex)) {
                if (matchCnt == 0
                    || addrStrArr[i].substring(addrStrArr[i].length() - 1).equals("로")
                    || addrStrArr[i].substring(addrStrArr[i].length() - 1).equals("길")
                    || addrStrArr[i].substring(addrStrArr[i].length() - 1).equals("동")
                    || addrStrArr[i].substring(addrStrArr[i].length() - 1).equals("리")) {
                    resultStr = str;
                }

                matchCnt++;
            }
        }

        if (!resultStr.isEmpty() && matchCnt > 0) {
            resultStr += "***";
        } else {
            for (int i = 0; i < addrStrArr.length -1; i++) {
                resultStr += addrStrArr[i] + " ";
            }
            resultStr += "***";
        }

        return resultStr;
    }

    // escape 된 문자들(&,",',<,>)을 원복
    public static String unescapeStr(String str) {
        if (str == null || str.isEmpty()) {
            return "";
        }
        str = str.replaceAll("&amp;", "&");
        str = str.replaceAll("&apos;", "\'");
        str = str.replaceAll("&lsquo;", "\'");
        str = str.replaceAll("&rsquo;", "\'");
        str = str.replaceAll("&ldquo;", "\"");
        str = str.replaceAll("&rdquo;", "\"");

        // 아래 API에서 '(&apos)는 변환되지 않으므로 replaceAll로 변경
        return StringEscapeUtils.unescapeHtml3(str);
    }

    // XSS 필터링 공통 함수 - multipart form 전송인 경우 XSS 필터 함수를 거치지 않게 되어 별도 함수로 이를 처리토록 함.
    public static String strFilter(String str) {
        // 필터링할 문자열
        String filstr = "javascript,vbscript,expression,applet,meta,xml,blink,script,"
                + "embed,object,iframe,frame,frameset,ilayer,layer,bgsound,base,eval,"
                + "innerHTML,charset,string,create,append,binding,alert,msgbox,refresh,"
                + "cookie,void,href,onabort,onactivae,onafterprint,onafterupdate,onbefore,onbeforeactivate,"
                + "onbeforecopy,onbeforecut,onbeforedeactivate,onbeforeeditfocus,onbeforepaste,onbeforeprint,"
                + "onbeforeunload,onbeforeupdate,onblur,onbounce,oncellchange,onchange,onclick,oncontextmenu,"
                + "oncontrolselect,oncopy,oncut,ondataavailable,ondatasetchanged,ondatasetcomplete,ondblclick,"
                + "ondeactivate,ondrag,ondragend,ondragenter,ondragleave,ondragover,ondragstart,ondrop,onerror,"
                + "onerrorupdate,onfilterchange,onfinish,onfocus,onfocusin,onfocusout,onhelp,onkeydown,onkeypress,"
                + "onkeyup,onlayoutcomplete,onload,onlosecapture,onmousedown,onmouseenter,onmouseleave,onmousemove,"
                + "onmouseout,onmouseover,onmouseup,onmousewheel,onmove,onmoveend,onmovestart,onpaste,onpropertychange,"
                + "onreadystatechange,onreset,onresize,onresizeend,onresizestart,onrowenter,onrowexit,onrowsdelete,"
                + "onrowsinserted,onscroll,onselect,onselectionchange,onselectstart,onstart,onstop,onsubmit,onunload";

        String convfilstr = "_javascript_,_vbscript_,_expression_,_applet_,_meta_,_xml_,_blink_,_script_,"
                + "_embed_,_object_,_iframe_,_frame_,_frameset_,_ilayer_,_layer_,_bgsound_,_base_,_eval_,"
                + "_innerHTML_,_charset_,_string_,_create_,_append_,_binding_,_alert_,_msgbox_,_refresh_,"
                + "_cookie_,_void_,_href_,_onabort_,_onactivae_,_onafterprint_,_onafterupdate_,_onbefore_,_onbeforeactivate_,"
                + "_onbeforecopy_,_onbeforecut_,_onbeforedeactivate_,_onbeforeeditfocus_,_onbeforepaste_,_onbeforeprint_,"
                + "_onbeforeunload_,_onbeforeupdate_,_onblur_,_onbounce_,_oncellchange_,_onchange_,_onclick_,_oncontextmenu_,"
                + "_oncontrolselect_,_oncopy_,_oncut_,_ondataavailable_,_ondatasetchanged_,_ondatasetcomplete_,_ondblclick_,"
                + "_ondeactivate_,_ondrag_,_ondragend_,_ondragenter_,_ondragleave_,_ondragover_,_ondragstart_,_ondrop_,_onerror_,"
                + "_onerrorupdate_,_onfilterchange_,_onfinish_,_onfocus_,_onfocusin_,_onfocusout_,_onhelp_,_onkeydown_,_onkeypress_,"
                + "_onkeyup_,_onlayoutcomplete_,_onload_,_onlosecapture_,_onmousedown_,_onmouseenter_,_onmouseleave_,_onmousemove_,"
                + "_onmouseout_,_onmouseover_,_onmouseup_,_onmousewheel_,_onmove_,_onmoveend_,_onmovestart_,_onpaste_,_onpropertychange_,"
                + "_onreadystatechange_,_onreset_,_onresize_,_onresizeend_,_onresizestart_,_onrowenter_,_onrowexit_,_onrowsdelete_,"
                + "_onrowsinserted_,_onscroll_,_onselect_,_onselectionchange_,_onselectstart_,_onstart_,_onstop_,_onsubmit_,_onunload_";

        if(!convfilstr.equals("")) {
            convfilstr.replaceAll(" ","");
            String[] convSt = convfilstr.split(",");
            String[] st = filstr.split(",");
            for(int x=0; x<convSt.length; x++) {
                str = str.replaceAll(convSt[x], st[x]);
            }
        }

        if(!filstr.equals("")) {
            filstr.replaceAll(" ","");
            String[] st = filstr.split(",");
            for(int x=0; x<st.length; x++) {
                str = str.replaceAll(st[x], "_"+st[x]+"_");
            }
        }

        StringBuffer strBuff = new StringBuffer();

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            switch (c) {
                case '<':
                    strBuff.append("&lt;");
                    break;
                case '>':
                    strBuff.append("&gt;");
                    break;
                case '&':
                    strBuff.append("&amp;");
                    break;
                case '"':
                    strBuff.append("&quot;");
                    break;
                case '\'':
                    strBuff.append("&apos;");
                    break;
                default:
                    strBuff.append(c);
                    break;
            }
        }

        str = strBuff.toString();

        return str;
    }

}
