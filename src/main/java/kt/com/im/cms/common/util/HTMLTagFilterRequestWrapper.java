/*
 * Copyright 2008-2009 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kt.com.im.cms.common.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * HTMLTagFilterRequestWrapper.java
 *
 * @author 실행환경 개발팀 함철
 * @since 2009.06.01
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.05.30  함철            최초 생성
 *
 *      </pre>
 */
public class HTMLTagFilterRequestWrapper extends HttpServletRequestWrapper {

    public HTMLTagFilterRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    public String xssFilter(String str) {
        // 필터링할 문자열
        String filstr = "javascript,vbscript,expression,applet,meta,xml,blink,link,script,"
                + "embed,object,iframe,frame,frameset,ilayer,layer,bgsound,title,base,eval,"
                + "innerHTML,charset,document,string,create,append,binding,alert,msgbox,refresh,"
                + "cookie,void,href,onabort,onactivae,onafterprint,onafterupdate,onbefore,onbeforeactivate,"
                + "onbeforecopy,onbeforecut,onbeforedeactivate,onbeforeeditfocus,onbeforepaste,onbeforeprint,"
                + "onbeforeunload,onbeforeupdate,onblur,onbounce,oncellchange,onchange,onclick,oncontextmenu,"
                + "oncontrolselect,oncopy,oncut,ondataavailable,ondatasetchanged,ondatasetcomplete,ondblclick,"
                + "ondeactivate,ondrag,ondragend,ondragenter,ondragleave,ondragover,ondragstart,ondrop,onerror,"
                + "onerrorupdate,onfilterchange,onfinish,onfocus,onfocusin,onfocusout,onhelp,onkeydown,onkeypress,"
                + "onkeyup,onlayoutcomplete,onload,onlosecapture,onmousedown,onmouseenter,onmouseleave,onmousemove,"
                + "onmouseout,onmouseover,onmouseup,onmousewheel,onmove,onmoveend,onmovestart,onpaste,onpropertychange,"
                + "onreadystatechange,onreset,onresize,onresizeend,onresizestart,onrowenter,onrowexit,onrowsdelete,"
                + "onrowsinserted,onscroll,onselect,onselectionchange,onselectstart,onstart,onstop,onsubmit,onunload";

        String convfilstr = "_javascript_,_vbscript_,_expression_,_applet_,_meta_,_xml_,_blink_,_link_,_script_,"
                + "_embed_,_object_,_iframe_,_frame_,_frameset_,_ilayer_,_layer_,_bgsound_,_title_,_base_,_eval_,"
                + "_innerHTML_,_charset_,_document_,_string_,_create_,_append_,_binding_,_alert_,_msgbox_,_refresh_,"
                + "_cookie_,_void_,_href_,_onabort_,_onactivae_,_onafterprint_,_onafterupdate_,_onbefore_,_onbeforeactivate_,"
                + "_onbeforecopy_,_onbeforecut_,_onbeforedeactivate_,_onbeforeeditfocus_,_onbeforepaste_,_onbeforeprint_,"
                + "_onbeforeunload_,_onbeforeupdate_,_onblur_,_onbounce_,_oncellchange_,_onchange_,_onclick_,_oncontextmenu_,"
                + "_oncontrolselect_,_oncopy_,_oncut_,_ondataavailable_,_ondatasetchanged_,_ondatasetcomplete_,_ondblclick_,"
                + "_ondeactivate_,_ondrag_,_ondragend_,_ondragenter_,_ondragleave_,_ondragover_,_ondragstart_,_ondrop_,_onerror_,"
                + "_onerrorupdate_,_onfilterchange_,_onfinish_,_onfocus_,_onfocusin_,_onfocusout_,_onhelp_,_onkeydown_,_onkeypress_,"
                + "_onkeyup_,_onlayoutcomplete_,_onload_,_onlosecapture_,_onmousedown_,_onmouseenter_,_onmouseleave_,_onmousemove_,"
                + "_onmouseout_,_onmouseover_,_onmouseup_,_onmousewheel_,_onmove_,_onmoveend_,_onmovestart_,_onpaste_,_onpropertychange_,"
                + "_onreadystatechange_,_onreset_,_onresize_,_onresizeend_,_onresizestart_,_onrowenter_,_onrowexit_,_onrowsdelete_,"
                + "_onrowsinserted_,_onscroll_,_onselect_,_onselectionchange_,_onselectstart_,_onstart_,_onstop_,_onsubmit_,_onunload_";

        if(!convfilstr.equals("")) {
            convfilstr.replaceAll(" ","");
            String[] convSt = convfilstr.split(",");
            String[] st = filstr.split(",");
            for(int x=0; x<convSt.length; x++) {
                str = str.replaceAll(convSt[x], st[x]);
            }
        }

        if(!filstr.equals("")) {
            filstr.replaceAll(" ","");
            String[] st = filstr.split(",");
            for(int x=0; x<st.length; x++) {
                str = str.replaceAll(st[x], "_"+st[x]+"_");
            }
        }
        return str;
    }

    public String[] getParameterValues(String parameter) {
        String[] values = super.getParameterValues(parameter);

        if (values == null) {
            return null;
        }

        for (int i = 0; i < values.length; i++) {
            if (values[i] != null) {
                StringBuffer strBuff = new StringBuffer();
                for (int j = 0; j < values[i].length(); j++) {
                    char c = values[i].charAt(j);
                    switch (c) {
                        case '<':
                            strBuff.append("&lt;");
                            break;
                        case '>':
                            strBuff.append("&gt;");
                            break;
                        case '&':
                            strBuff.append("&amp;");
                            break;
                        case '"':
                            strBuff.append("&quot;");
                            break;
                        case '\'':
                            strBuff.append("&apos;");
                            break;
                        default:
                            strBuff.append(c);
                            break;
                    }
                }
                values[i] = strBuff.toString();
            } else {
                values[i] = null;
            }
        }

        return values;
    }

    public String getParameter(String parameter) {

        String value = super.getParameter(parameter);

        if (value == null) {
            return null;
        }

        value = xssFilter(value);

        StringBuffer strBuff = new StringBuffer();

        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            switch (c) {
                case '<':
                    strBuff.append("&lt;");
                    break;
                case '>':
                    strBuff.append("&gt;");
                    break;
                case '&':
                    strBuff.append("&amp;");
                    break;
                case '"':
                    strBuff.append("&quot;");
                    break;
                case '\'':
                    strBuff.append("&apos;");
                    break;
                default:
                    strBuff.append(c);
                    break;
            }
        }

        value = strBuff.toString();

        return value;
    }

}