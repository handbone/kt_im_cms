package kt.com.im.cms.common.util;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;

@Component
public class CommonUtil {

    @Resource(name = "MemberService")
    private MemberService memberService;

    // Member Token 유효성 검사
    public boolean validMbrToken(HttpServletRequest request) throws Exception {
        MemberVO mItem = new MemberVO();

        String mbrTokn = CommonFnc.emptyCheckString("mbrTokn", request);
        int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

        String encodeToken = "";
        encodeToken = mbrTokn.replaceAll(" ", "+");

        mItem.setStorSeq(storSeq);
        mItem.setMbrTokn(encodeToken);

        MemberVO mbrVO = memberService.selectMemberInfoByToken(mItem);
        if (mbrVO == null) {
            return false;
        }

        // 파라미터 정보에 서비스 번호(svcSeq)가 없을 경우 매장 번호(storSeq)로 비교하여 유효한지 확인
        if (request.getParameter("svcSeq") == null) {
            if (storSeq != mbrVO.getStorSeq()) {
                return false;
            }
        } else {
            // 토큰 정보로 멤버 정보를 조회하여 멤버의 서비스 번호와 일치하지 않는 서비스의 요청일 경우 접근 불가토록 함
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            if (svcSeq != mbrVO.getSvcSeq()) {
                return false;
            }
        }

        return true;
    }
}