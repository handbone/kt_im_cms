/*
 * IM Platform version 1.0
 *
 *  Copyright �� 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.common.util;

import java.util.Locale;
import java.util.Properties;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;
public class LocaleMessageSource extends ReloadableResourceBundleMessageSource {

    public Properties getProperties(Locale locale) {
        PropertiesHolder holder = super.getMergedProperties(locale);
        if (holder == null)
            return null;
        return holder.getProperties();
    }
}
