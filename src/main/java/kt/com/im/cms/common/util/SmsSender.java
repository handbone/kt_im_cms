/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

import kt.com.im.cms.sms.vo.SmsVO;

/**
*
* SMS 발송 클래스를 정의한다
*
* @author A2TEC
* @since 2018.09.04
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 09. 04.   A2TEC      최초생성
*
*
* </pre>
*/

public class SmsSender {
    static private String CLIENT_ID = "ktimp";
    static private String API_KEY = "ODcyOS0xNTMwNjA2MTkwMTM1LWNmZTQ2YjRkLTQ0NDYtNDQxOS1hNDZiLTRkNDQ0NjU0MTlhMw==";

    static public String PARAM_SEND_TIME = "send_time";
    static public String PARAM_DEST_PHONE = "dest_phone";
    static public String PARAM_DEST_NAME = "dest_name";
    static public String PARAM_SEND_PHONE = "send_phone";
    static public String PARAM_SEND_NAME = "send_name";
    static public String PARAM_SUBJECT = "subject";
    static public String PARAM_MSG = "msg_body";

    static public JsonNode send(SmsVO vo) throws Exception {
        JSONObject json = new JSONObject();

        json.put("CLIENT_ID", CLIENT_ID);
        json.put("API_KEY", API_KEY);
        json.put(PARAM_SEND_TIME, vo.getSendTime());
        json.put(PARAM_DEST_PHONE, vo.getDestPhone());
        json.put(PARAM_DEST_NAME, vo.getDestName());
        json.put(PARAM_SEND_PHONE, vo.getSendPhone());
        json.put(PARAM_SEND_NAME, vo.getSendName());
        json.put(PARAM_SUBJECT, vo.getSubject());
        json.put(PARAM_MSG, vo.getMsgBody());

        String resultSMSCode = postJson(vo.getConnectUrl(), "127.0.0.1", json.toString(), "POST");

        JsonNode resultJb = new JsonNode(resultSMSCode);
        // HttpResponse<JsonNode> response = Unirest.post("https://api.apistore.co.kr/ppurio/1/message/sms/" +
        // CLIENT_ID)
        // .header("x-waple-authorization", API_KEY)
        // .field(PARAM_SEND_TIME, vo.getSendTime())
        // .field(PARAM_DEST_PHONE, vo.getDestPhone())
        // .field(PARAM_DEST_NAME, vo.getDestName())
        // .field(PARAM_SEND_PHONE, vo.getSendPhone())
        // .field(PARAM_SEND_NAME, vo.getSendName())
        // .field(PARAM_SUBJECT, vo.getSubject())
        // .field(PARAM_MSG, vo.getMsgBody())
        // .asJson();
        // System.out.println("sms send response = " + response.getBody());
        return resultJb;
    }

    static public JsonNode report(String cmid) throws Exception {
        HttpResponse<JsonNode> response = Unirest
                .get("https://api.apistore.co.kr/ppurio/1/message/report/ktimp?cmid=" + cmid)
                .header("x-waple-authorization", API_KEY).asJson();
        System.out.println("sms report response = " + response.getBody());
        return response.getBody();
    }

    /* 외부 URL 정보 POST 함수_ 현재 이용: [블록체인 데이터 전송] */
    public static String postJson(String serverUrl, String host, String jsonobject, String Method) {

        StringBuilder sb = new StringBuilder();

        String http = serverUrl;

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(http);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(Method);
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(100000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Host", host);
            urlConnection.connect();
            // You Can also Create JSONObject here
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonobject);// here i sent the parameter
            out.close();
            int HttpResult = urlConnection.getResponseCode();
            System.out.println(">>result Code:" + HttpResult);
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                // Log.e("new Test", "" + sb.toString());
                return sb.toString();
            } else {
                System.out.println(urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }
}
