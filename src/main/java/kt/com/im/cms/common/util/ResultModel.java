/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */


package kt.com.im.cms.common.util;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
*
* 공통 ModelAndView 사용을 위한 클래스를 정의한다
*
* @author A2TEC
* @since 2018.05.09
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 9.   A2TEC      최초생성
*
*
* </pre>
*/


public class ResultModel {

    // 결과 key명
    public final static String RESULT_CODE_KEY = "resultCode";
    public final static String RESULT_MSG_KEY = "resultMsg";
    public final static String RESULT_TYPE_KEY = "resultType";
    public final static String RESULT_KEY = "result";

    // 메세지
    public final static String MESSAGE_SUCESS = "Success";
    public final static String MESSAGE_ERROR = "Error";
    public final static String MESSAGE_WARNING = "Warning";
    public final static String MESSAGE_NO_DATA = "No Data";
    public final static String MESSAGE_PRECED_NO_DATA = "preceding No Data";
    public final static String MESSAGE_FAILED_PROCESS = "Failed to process";
    public final static String MESSAGE_FAILED_INSERT = "Failed to insert";
    public final static String MESSAGE_FAILED_UPDATE = "Failed to update";
    public final static String MESSAGE_FAILED_DELETE = "Failed to delete";
    public final static String MESSAGE_INVALID_PARAM = "invalid parameter";
    public final static String MESSAGE_NOT_FOUND = "Not Found";
    public final static String MESSAGE_FAILED_AUTHORIZE = "Failed to authorize";
    public final static String MESSAGE_NOT_IMPLEMENTED = "Not Implemented";
    public final static String MESSAGE_UNAVAILABLE_SERVICE = "Unavailable service";
    public final static String MESSAGE_UNAVAILABLE_MEMBER = "Unavailable member";
    public final static String MESSAGE_UNAVAILABLE_STORE = "Unavailable store";
    public final static String MESSAGE_UNAVAILABLE_CP = "Unavailable content provider";
    public final static String MESSAGE_UNAVAILABLE_CAT_VAL = "contents Category Value Update Failure";
    public final static String MESSAGE_NO_FUNCTION = "No Function";
    public final static String MESSAGE_FAIL_UPDATE = "Update Fail";
    public final static String MESSAGE_FAIL_DELETE = "Delete Fail";
    public final static String MESSAGE_FAIL_INSERT = "Insert Fail";
    public final static String MESSAGE_FAIL_FILE_INSERT = "Insert File Fail";
    public final static String MESSAGE_OVER_FILE_SIZE = "Over File Size";
    public final static String MESSAGE_FAIL_INSERT_CONTS = "Contents Insert Fail";
    public final static String MESSAGE_FAIL_INSERT_CAT = "Category Insert Fail";
    public final static String MESSAGE_EXIST_CAT_INFO = "exist Category ID or Name";
    public final static String MESSAGE_EXIST_INFO = "exist Info";
    public final static String MESSAGE_OVERLAP_RELEASE = "overlap Release";
    public final static String MESSAGE_NOT_TOKN = "Not Token Info";
    public final static String MESSAGE_NOT_TAG = "Not Tag Info";


    // HTTP STATUS
    public final static int HTTP_STATUS_OK = 200;
    public final static int HTTP_STATUS_BAD_REQUEST = 400;
    public final static int HTTP_STATUS_UNAUTHORIZED = 401;
    public final static int HTTP_STATUS_NOT_FOUND = 404;
    public final static int HTTP_STATUS_INTERNAL_SERVER_ERROR = 500;
    public final static int HTTP_STATUS_NOT_IMPLEMENTED = 501;
    public final static int HTTP_STATUS_SERVICE_UNAVAILABLE = 503;

    // RESULT CODE
    public final static String RESULT_CODE_NORMAL = "1000";
    public final static String RESULT_CODE_DATA_ACCESS_LAYER = "1001";
    public final static String RESULT_CODE_SERVICE_LAYER = "1002";
    public final static String RESULT_CODE_REQUEST_HANDLER_LAYER = "1003";
    public final static String RESULT_CODE_INVALID_SERVER = "1005";
    public final static String RESULT_CODE_NO_RESULT = "1010";
    public final static String RESULT_CODE_EXIST_RESULT = "1011";
    public final static String RESULT_CODE_PARTIAL_SUCCESS = "1012";
    public final static String RESULT_CODE_CANCEL_REQUEST = "1013";
    public final static String RESULT_CODE_INVALID_PARAM = "1400";
    public final static String RESULT_CODE_INVALID = "1500";

    private HttpServletResponse response;

    private ModelAndView mv;

    public ResultModel(HttpServletResponse response) {
        this.response = response;
        // 기본 HTTP_STATUS를 200으로 설정
        this.response.setStatus(HTTP_STATUS_OK);

        this.mv = new ModelAndView();
        // 기본 RESULT_CODE를 1000으로 설정
        this.mv.addObject(RESULT_CODE_KEY, RESULT_CODE_NORMAL);
        // 기본 메세지를 Success로 설정
        this.mv.addObject(RESULT_MSG_KEY, MESSAGE_SUCESS);
    }

    public void setResponseStatus(int status) {
        this.mv.addObject(RESULT_CODE_KEY, getResultCodeForStatus(status));
        this.mv.addObject(RESULT_MSG_KEY, getResultMessageForStatus(status));
        this.response.setStatus(status);
    }

    public void setResponseStatus(int status, String msg) {
        this.mv.addObject(RESULT_CODE_KEY, getResultCodeForStatus(status));
        this.mv.addObject(RESULT_MSG_KEY, msg);
        this.response.setStatus(status);
    }

    public void setStatus(int status) {
        this.response.setStatus(status);
    }

    public void setViewName(String viewName) {
        this.mv.setViewName(viewName);
    }

    public void setResultCode(String resultCode) {
        this.mv.addObject(RESULT_CODE_KEY, resultCode);
    }

    public void setResultMessage(String resultMsg) {
        this.mv.addObject(RESULT_MSG_KEY, resultMsg);
    }

    public void setResultType(String resultType) {
        this.mv.addObject(RESULT_TYPE_KEY, resultType);
    }

    public void setData(String id, Object data) {
        this.mv.addObject(id, data);
    }

    public void setResultData(String id, Object data) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(id, data);
        this.mv.addObject(RESULT_KEY, result);
    }

    public void setNoData() {
        this.mv.addObject(RESULT_CODE_KEY, RESULT_CODE_NO_RESULT);
        this.mv.addObject(RESULT_MSG_KEY, MESSAGE_NO_DATA);
    }

    public String getResultCodeForStatus(int status) {
        String resultCode = RESULT_CODE_NORMAL;
        if (status == HTTP_STATUS_BAD_REQUEST) { // 400
            resultCode = RESULT_CODE_INVALID_PARAM;
        } else if (status == HTTP_STATUS_NOT_FOUND) { // 404
            resultCode = RESULT_CODE_INVALID;
        } else if (status == HTTP_STATUS_INTERNAL_SERVER_ERROR) { // 500
            resultCode = RESULT_CODE_INVALID;
        } else if (status == HTTP_STATUS_NOT_IMPLEMENTED) { // 501
            resultCode = RESULT_CODE_INVALID;
        } else if (status == HTTP_STATUS_SERVICE_UNAVAILABLE) { // 503
            resultCode = RESULT_CODE_INVALID_SERVER;
        }
        return resultCode;
    }

    public String getResultMessageForStatus(int status) {
        String resultMsg = MESSAGE_SUCESS;
        if (status == HTTP_STATUS_BAD_REQUEST) { // 400
            resultMsg = MESSAGE_INVALID_PARAM;
        } else if (status == HTTP_STATUS_UNAUTHORIZED) { // 401
            resultMsg = MESSAGE_FAILED_AUTHORIZE;
        } else if (status == HTTP_STATUS_NOT_FOUND) { // 404
            resultMsg = MESSAGE_NOT_FOUND;
        } else if (status == HTTP_STATUS_INTERNAL_SERVER_ERROR) { // 500
            resultMsg = MESSAGE_FAILED_PROCESS;
        } else if (status == HTTP_STATUS_NOT_IMPLEMENTED) { // 501
            resultMsg = MESSAGE_NOT_IMPLEMENTED;
        } else if (status == HTTP_STATUS_SERVICE_UNAVAILABLE) { // 503
            resultMsg = MESSAGE_UNAVAILABLE_SERVICE;
        }
        return resultMsg;
    }

    public ModelAndView getModelAndView() {
        return this.mv;
    }
}