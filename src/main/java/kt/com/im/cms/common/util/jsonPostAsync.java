/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class jsonPostAsync {

    final static Logger log = LogManager.getLogger();

    public static void main(String args) throws InterruptedException, ExecutionException, TimeoutException {
        Future<Object> futureResult = getObjectAsync("", "");

        Object value = futureResult.get(500, TimeUnit.MILLISECONDS);
    }

    public static Future<Object> getObjectAsync(String json, String chainUrl) {
        return CompletableFuture.supplyAsync(() -> doHttpCall(json, chainUrl));
    }

    static Object doHttpCall(String json, String chainUrl) {
        String Url_Block = chainUrl;

        try {

            HttpURLConnection urlConnection = (HttpURLConnection) new URL(Url_Block + "/api/v1/service/log")
                    .openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");

            try (OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream())) {
                out.write(json);// here i sent the parameter
                out.close();
            }

            StringBuilder sb = new StringBuilder();
            try (InputStreamReader in = new InputStreamReader(urlConnection.getInputStream())) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                log.warn("[SUCCESS][JsonData]:{} [URL]:{}", json.toString(), Url_Block);

                return sb.toString();
            }

        } catch (IOException e) {
            log.warn("[ERROR][JsonData]:{} [URL]:{}", json.toString(), Url_Block);
            throw new RuntimeException(e);
        }
    }
}
