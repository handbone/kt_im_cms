/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

/**
*
* Menu 관련 클래스를 정의한다
*
* @author A2TEC
* @since 2018.07.16
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 7. 16.   A2TEC      최초생성
*
*
* </pre>
*/

@Component
public class MenuMaker {

    // 멤버 권한 코드
    private final static String MASTER = "01";
    private final static String CMS = "02";
    private final static String ACCEPTOR = "03";
    private final static String SERVICE = "04";
    private final static String STORE = "05";
    private final static String CP = "06";

    public static List<MenuVO> MenuList;

    @Resource(name="localeMessageSource")
    LocaleMessageSource messageSource;

    public boolean enabledLog = false;

    public MenuMaker() {
        initMenuList();
    }

    static public void initMenuList() {
        if (MenuList != null) {
            return;
        }

        MenuList = new ArrayList<MenuVO>();
        // 대시보드
        MenuList.add(toVo("menu.dashboard.title", 1, 0, "", "leftIconDash", toAuthList(MASTER, CMS, ACCEPTOR, SERVICE, STORE), null, null));
        // 대시보드 > 콘텐츠 현황
        MenuList.add(toVo("menu.dashboard.contents", 1, 1, "/dashboard/contents", "", toAuthList(MASTER, CMS, ACCEPTOR, SERVICE, STORE), toURIList("/dashboard/contents/verify", "/dashboard/contents/registered"), null));
        // 대시보드 > 매장 현황
        MenuList.add(toVo("menu.dashboard.store", 2, 1, "/dashboard/store", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/dashboard/store"), null));

        // 사용자 관리
        MenuList.add(toVo("menu.member.title", 2, 0, "", "leftIconUser", toAuthList(MASTER, CMS, SERVICE), null, null));
        // 사용자 관리 > 계정 관리
        MenuList.add(toVo("menu.member.account", 1, 2, "/member", "", toAuthList(MASTER, CMS, SERVICE), toURIList("/member", "/member/regist", "/member/edit"), null));
        // 사용자 관리 > 계정 승인 관리
        MenuList.add(toVo("menu.member.account.accept", 2, 2, "/member/waiting", "", toAuthList(MASTER, CMS), toURIList("/member/waiting"), null));
        // 사용자 관리 > 서비스 관리
        MenuList.add(toVo("menu.member.service", 3, 2, "/service", "", toAuthList(MASTER, CMS), toURIList("/service", "/service/regist", "/service/edit"), null));
        // 사용자 관리 > CP사 관리
        MenuList.add(toVo("menu.member.contentProvider", 4, 2, "/cp", "", toAuthList(MASTER, CMS), toURIList("/cp", "/cp/regist", "/cp/edit"), null));
        // 사용자 관리 > 인증 회원 관리
        MenuList.add(toVo("menu.member.integrated", 5, 2, "/member/integrate", "", toAuthList(MASTER, CMS), toURIList("/member/integrate"), null));

        // 콘텐츠 관리
        MenuList.add(toVo("menu.contents.title", 3, 0, "", "leftIconMaco", toAuthList(MASTER, CMS, ACCEPTOR), null, null));
        // 콘텐츠 관리 > 콘텐츠 통합 정보
        MenuList.add(toVo("menu.contents.info", 1, 3, "/contents", "", toAuthList(MASTER, CMS, ACCEPTOR), toURIList("/contents"), null));
        // 콘텐츠 관리 > 검수 관리
        MenuList.add(toVo("menu.contents.acceptance", 2, 3, "/verify", "", toAuthList(MASTER, CMS, ACCEPTOR), toURIList("/verify", "/verify/edit"), null));
        // 콘텐츠 관리 >  계약/과금 관리
        MenuList.add(toVo("menu.contents.contract", 3, 3, "/contract", "", toAuthList(MASTER, CMS), toURIList("/contract","/contractDetail","/contractDetail/edit","/contractDetail/regist","/billing","/billingDetail","/billingDetail/edit","/billingDetail/regist"), null));
        // 콘텐츠 관리 > 전시페이지
        MenuList.add(toVo("menu.contents.release", 4, 3, "/display", "", toAuthList(MASTER, CMS), toURIList("/display", "/display/edit"), null));
        // 콘텐츠 관리 > 편성 관리
        MenuList.add(toVo("menu.contents.formation", 5, 3, "/cms/release", "", toAuthList(MASTER, CMS), toURIList("/cms/release", "/cms/release/regist", "/cms/release/edit", "/cms/release/stat"), null));
        // 콘텐츠 관리 > 카테고리 관리
        MenuList.add(toVo("menu.contents.category", 6, 3, "/contents/category", "", toAuthList(MASTER, CMS), toURIList("/contents/category"), null));
        // 콘텐츠 관리 > 장르 관리
        MenuList.add(toVo("menu.contents.genre", 7, 3, "/contents/genre", "", toAuthList(MASTER, CMS), toURIList("/contents/genre"), null));

        // 서비스 매장 관리
        MenuList.add(toVo("menu.service.title", 4, 0, "", "leftIconStore", toAuthList(MASTER, CMS, SERVICE, STORE), null, "OFFLINE"));
        // 서비스 매장 관리 > 매장 관리
        MenuList.add(toVo("menu.service.store", 1, 4, "/store", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/store", "/store/regist", "/store/edit"), "OFFLINE"));
        // 서비스 매장 관리 > 이용권 관리
        MenuList.add(toVo("menu.service.product", 2, 4, "/product", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/product", "/product/regist", "/product/edit"), "OFFLINE"));
        // 서비스 매장 관리 > 이용권 발급 관리
        MenuList.add(toVo("menu.service.tag", 3, 4, "/reservate", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/reservate", "/reservate/regist", "/reservate/edit"), "OFFLINE"));
        // 서비스 매장 관리 > 예약 관리
        MenuList.add(toVo("menu.service.reservation", 4, 4, "/reservation", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/reservation"), "OFFLINE"));
        // 서비스 매장 관리 > 콘텐츠 관리
        MenuList.add(toVo("menu.service.contents", 5, 4, "/mms/contents", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/mms/contents", "/mms/release", "/mms/release/regist"), "OFFLINE"));
        // 서비스 매장 관리 > 과금 관리
        MenuList.add(toVo("menu.service.contract", 6, 4, "/service/contract", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/service/contract"), "OFFLINE"));
        // 서비스 매장 관리 > Device 관리
        MenuList.add(toVo("menu.service.device", 7, 4, "/device", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/device", "/device/regist", "/device/edit"), "OFFLINE"));
        // 서비스 매장 관리 > Device 버전 관리
        MenuList.add(toVo("menu.service.device.version", 8, 4, "/version", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/version", "/version/regist", "/version/edit"), "OFFLINE"));
        // 서비스 매장 관리 > 검색 키워드 관리
        MenuList.add(toVo("menu.service.searchKeyword", 9, 4, "/keyword", "", toAuthList(MASTER, CMS, SERVICE), toURIList("/keyword"), "OFFLINE"));
        // 서비스 매장 관리 > 고객센터 관리
        MenuList.add(toVo("menu.service.customer", 10, 4, "/servicecustomer", "", toAuthList(MASTER, CMS, SERVICE, STORE),
            toURIList("/servicecustomer", "/servicecustomer/notice", "/servicecustomer/notice/regist", "/servicecustomer/notice/edit", "/servicecustomer/faq", "/servicecustomer/faq/regist", "/servicecustomer/faq/edit"), "OFFLINE"));

        // 온라인 서비스 관리
        MenuList.add(toVo("menu.online.service.title", 5, 0, "", "leftIconOnline", toAuthList(MASTER, CMS, SERVICE), null, "ONLINE"));
        // 온라인 서비스 관리 > 버전 관리
        MenuList.add(toVo("menu.online.service.version", 1, 5, "/onlineSvc/version", "", toAuthList(MASTER, CMS, SERVICE), toURIList("/onlineSvc/version", "/onlineSvc/version/regist", "/onlineSvc/version/edit"), "ONLINE"));
        // 온라인 서비스 관리 > 배너 관리
        MenuList.add(toVo("menu.online.service.banr", 2, 5, "/onlineService/banr", "", toAuthList(MASTER, CMS, SERVICE), toURIList("/onlineService/banr","/onlineService/banr/regist","/onlineService/banr/edit", "/onlineService/banr/hst", "/onlineService/cprtSvc", "/onlineService/cprtSvc/edit", "/onlineService/cprtSvc/regist", "/onlineService/banr/plcyMng"),"ONLINE"));
        // 온라인 서비스 관리 > 메인 영역 관리
        MenuList.add(toVo("menu.online.service.main.app", 3, 5, "/onlineSvc/main/app", "", toAuthList(MASTER, CMS, SERVICE), toURIList("/onlineSvc/main/app", "/onlineSvc/main/app/regist", "/onlineSvc/main/app/edit"), "ONLINE"));
        // 온라인 서비스 관리 > 약관 관리
        MenuList.add(toVo("menu.online.service.terms", 4, 5, "/onlineService/terms", "", toAuthList(MASTER, CMS, SERVICE), toURIList("/onlineService/terms", "/onlineService/terms/regist", "/onlineService/terms/edit"), "ONLINE"));
        // 온라인 서비스 관리 > 고객센터 관리
        MenuList.add(toVo("menu.online.service.customer", 5, 5, "/onlineSvc/customer", "", toAuthList(MASTER, CMS, SERVICE),
            toURIList("/onlineSvc/customer", "/onlineSvc/customer/notice", "/onlineSvc/customer/notice/regist", "/onlineSvc/customer/notice/edit", "/onlineSvc/customer/faq", "/onlineSvc/customer/faq/regist", "/onlineSvc/customer/faq/edit"), "ONLINE"));

        // 고객센터
        MenuList.add(toVo("menu.customer.title", 6, 0, "", "leftIconCustom", toAuthList(MASTER, CMS, ACCEPTOR, SERVICE, STORE), null, null));
        // 고객센터 > 공지사항
        MenuList.add(toVo("menu.customer.notice", 1, 6, "/customer/notice", "", toAuthList(MASTER, CMS, ACCEPTOR, SERVICE, STORE), toURIList("/customer/notice", "/customer/notice/regist", "/customer/notice/edit"), null));
        // 고객센터 > FAQ
        MenuList.add(toVo("menu.customer.faq", 2, 6, "/customer/faq", "", toAuthList(MASTER, CMS, ACCEPTOR, SERVICE, STORE), toURIList("/customer/faq", "/customer/faq/regist", "/customer/faq/edit"), null));
        // 고객센터 > 문의사항
        MenuList.add(toVo("menu.customer.qna", 3, 6, "/customer/qna", "", toAuthList(MASTER, CMS), toURIList("/customer/qna", "/customer/qna/registAns"), null));

        // 통계
        MenuList.add(toVo("menu.statistics.title", 7, 0, "", "leftIconStats", toAuthList(MASTER, CMS, ACCEPTOR, SERVICE, STORE), null, null));
        // 통계 > 유입통계
        MenuList.add(toVo("menu.statistics.traffic", 1, 7, "/statistics/traffic", "", toAuthList(MASTER, CMS), toURIList("/statistics/traffic"), null));
        // 통계 > 콘텐츠 현황 통계
        MenuList.add(toVo("menu.statistics.contents", 2, 7, "/statistics/contents/registered", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/statistics/contents/registered", "/statistics/contents/used"), null));
        // 통계 > CP사 통계
        MenuList.add(toVo("menu.statistics.contentProvider", 3, 7, "/statistics/cp", "", toAuthList(MASTER, CMS), toURIList("/statistics/cp"), null));
        // 통계 > 매장 현황 통계
        MenuList.add(toVo("menu.statistics.store", 4, 7, "/statistics/store/visitor", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/statistics/store/visitor", "/statistics/store/tag", "/statistics/store/tagStore", "/statistics/store/contentsByTag"), null));
        // 통계 > 온라인 현황 통계 > 콘텐츠 사용 현황
        MenuList.add(toVo("menu.statistics.online", 5, 7, "/statistics/online/usedOlsvcContents", "", toAuthList(MASTER, CMS, SERVICE, STORE), toURIList("/statistics/online/usedOlsvcContents"), null));
        // 통계 > 검수현황 통계
        MenuList.add(toVo("menu.statistics.verifier", 6, 7, "/statistics/verifier", "", toAuthList(MASTER, CMS, ACCEPTOR), toURIList("/statistics/verifier"), null));
        // 통계 > 매출 통계
        MenuList.add(toVo("menu.statistics.payInfo", 7, 7, "/statistics/payInfo", "", toAuthList(MASTER, CMS, SERVICE, STORE),toURIList("/statistics/payInfo"), null));
        // 통계 > 블록 체인 통계
        MenuList.add(toVo("menu.statistics.blockStats", 8, 7, "/statistics/blockStats", "", toAuthList(MASTER, CMS), toURIList("/statistics/blockStats"), null));
        // 통계 > 런처 통계
        MenuList.add(toVo("menu.statistics.launcher", 9, 7, "/statistics/olSvcLauncher/traffic", "", toAuthList(MASTER, CMS, SERVICE), toURIList("/statistics/olSvcLauncher/traffic", "/statistics/olSvcLauncher/usedAppDetail", "/statistics/olSvcLauncher/usedConts"), null));

        // 시스템 관리
        MenuList.add(toVo("menu.system.title", 8, 0, "", "leftIconServer", toAuthList(MASTER, CMS), null, null));
        // 시스템 관리 > DB 관리
        MenuList.add(toVo("menu.system.db", 1, 8, "/system/db", "", toAuthList(MASTER, CMS), toURIList("/system/db", "/system/db/regist", "/system/db/edit"), null));
        // 시스템 관리 > 코드 관리
        MenuList.add(toVo("menu.system.code", 2, 8, "/system/code", "", toAuthList(MASTER, CMS), toURIList("/system/code"), null));
    }

    public List<MenuVO> get(HttpServletRequest request) {
        List<MenuVO> result = this.getMenu(request);
        return result;
    }

    public List<MenuVO> getMenu(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            if (enabledLog) {
                System.out.println("[MenuController] Session is null");
            }
            return null;
        }

        if (session.getAttribute("seq") == null || session.getAttribute("id") == null
           || session.getAttribute("name") == null || session.getAttribute("memberSec") == null) {
            if (enabledLog) {
                System.out.println("[MenuController] Session is invalid");
            }
            return null;
        }

        List<MenuVO> menuList = (List<MenuVO>)session.getAttribute("menuList");
        if (menuList == null) {
            menuList = this.getList(request);
            session.setAttribute("menuList", menuList);
        }

        this.setFocus(request, menuList);
        return menuList;
    }

    public List<MenuVO> getList(HttpServletRequest request) {
        String memberSection = (String)request.getSession(false).getAttribute("memberSec");
        String serviceType = (String)request.getSession(false).getAttribute("svcType");
        List<MenuVO> resultList = new ArrayList<MenuVO>();
        for (int i = 0; i < MenuList.size(); i++) {
            MenuVO sourceVO = MenuList.get(i);

            boolean hasAuthority = (sourceVO.getAuthorityList().indexOf(memberSection) != -1);
            boolean hasAuthoritySvcType = serviceType == null ? true : sourceVO.getSvcType() == null ? true : sourceVO.getSvcType().equals(serviceType) ? true : false;

            // TODO : 19.04.10
            // 서비스 관리자, 매장 관리자의 경우 해당 서비스가 ONLINE OFFLINE 구분하여 메뉴 생성이 필요할 경우 아래 조건문 내 hasAuthoritySvcType 주석 제거 필요
            if (hasAuthority/* && hasAuthoritySvcType*/) {
                MenuVO targetVO = this.copy(sourceVO);
                // WAS 서버의 locale 설정에 따라 언어가 설정되고 있어 한국으로 강제 변경함 (2018.10.25)
                //targetVO.setName(messageSource.getMessage(sourceVO.getNameMsgId(), null, Locale.getDefault()));
                targetVO.setName(messageSource.getMessage(sourceVO.getNameMsgId(), null, Locale.KOREAN));
                resultList.add(targetVO);
            }
        }

        return resultList;
    }

    public void setFocus(HttpServletRequest request, List<MenuVO> menuList) {
        String uri = getShortURI(request.getRequestURI().replaceAll(request.getContextPath(), ""));
        if (enabledLog) {
            System.out.println("[MenuController] Check uri = " + uri);
        }

        for (int i = 0; i < menuList.size(); i++) {
            MenuVO menuItem = menuList.get(i);

            boolean isFocused = false;
            menuItem.setIsFocused(isFocused);

            boolean isLowerMenu = (menuItem.getUpperIndex() != 0);
            if (isLowerMenu) {
                isFocused = (menuItem.getContainsURIList().indexOf(uri) != -1);
                if (isFocused) {
                    for (int j = 0; j < menuList.size(); j++) {
                        if (menuList.get(j).getUpperIndex() != 0) {
                            continue;
                        }
                        if (menuItem.getUpperIndex() == menuList.get(j).getIndex()) {
                            menuList.get(j).setIsFocused(isFocused);
                            break;
                        }
                    }
                    menuItem.setIsFocused(isFocused);
                }
            }
        }
    }

    private static MenuVO toVo(String nameMsgId, int index, int upperIndex, String uri, String addClass, List<String> authorityList, List<String> containsURIList, String serviceType) {
        MenuVO menuVO = new MenuVO();
        menuVO.setIndex(index);
        menuVO.setUpperIndex(upperIndex);
        menuVO.setUri(uri);
        menuVO.setNameMsgId(nameMsgId);
        menuVO.setAddClass(addClass);
        menuVO.setAuthorityList(authorityList);
        menuVO.setContainsURIList(containsURIList);
        menuVO.setSvcType(serviceType);
        return menuVO;
    }

    public static List<String> toAuthList(String ... args) {
        List<String> list = new ArrayList<String>();
        for (String str : args) {
            list.add(str);
        }
        return list;
    }

    public static List<String> toURIList(String ... args) {
        List<String> list = new ArrayList<String>();
        for (String str : args) {
            list.add(str);
        }
        return list;
    }

    private MenuVO copy(MenuVO oldVO) {
        MenuVO menuVO = new MenuVO();
        menuVO.setIndex(oldVO.getIndex());
        menuVO.setUpperIndex(oldVO.getUpperIndex());
        menuVO.setUri(oldVO.getUri());
        menuVO.setAddClass(oldVO.getAddClass());
        menuVO.setContainsURIList(oldVO.getContainsURIList());
        menuVO.setSvcType(oldVO.getSvcType());
        return menuVO;
    }

    public String getShortURI(String value) {
        if (value == null || value.isEmpty()) {
            return value;
        }

        // 에러페이지의 경우 URI 숫자 제거 하지 않음
        if (value.contains("error")) {
            return removeSlash(value);
        }

        // 숫자 제거 (상세정보화면의 숫자가 포함된 URI의 경우 Map에 정적 데이터로 저장할 수 없어서 예외처리)
        value = value.replaceAll("[0-9]", "");
        value = removeSlash(value);

        return value;
    }

    private String removeSlash(String value) {
        if (value == null || value.isEmpty()) {
            return value;
        }

        // 슬러시 제거
        int lastSlashIndex = 0;
        String result = "";
        for (int i = 0; i < value.length(); i++) {
            char ch = value.charAt(i);
            if ('/' == ch) {
                if ((lastSlashIndex + 1) == i) {
                    lastSlashIndex++;
                    continue;
                }
                lastSlashIndex = i;
            }
            result += value.charAt(i);
        }
        char ch = result.charAt(result.length() - 1);
        if ('/' == ch) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }
}
