/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.common.util;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

public class DOMValidateMETADATA {

    public String validate(String path,String schemaPath) throws SAXException, IOException, Exception {

        String METADATAFILEPATH = path;
        String SCHEMA_LOCATION = schemaPath;
        String resMsg = "";
        // 1. Lookup a factory for the W3C XML Schema language
        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
        // 2. Compile the schema. 
        // Here the schema is loaded from a java.io.File, but you could use 
        // a java.net.URL or a javax.xml.transform.Source instead.
        File schemaLocation = new File(SCHEMA_LOCATION);
        Schema schema = factory.newSchema(schemaLocation);
        // 3. Get a validator from the schema.
        Validator validator = schema.newValidator();
        // 4. Parse the document you want to check.
        Source source = new StreamSource(METADATAFILEPATH);
        // 5. Check the document
        try {
            validator.validate(source);
            resMsg = "OK";
        }
        catch (SAXException ex) {
            resMsg = ex.getMessage();
        }  

        return resMsg;
    }
}