/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.common.util;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.AnnotationMap;

import kt.com.im.cms.log.service.LogService;
import kt.com.im.cms.log.vo.LogVO;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;

public class ApiInterceptor extends HandlerInterceptorAdapter {

    Map<String, HashMap<String, String>> accessAPI = new HashMap<String, HashMap<String, String>>() {
        {
            put("/api/deviceConts", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("PUT", "LC_004");
                    put("GET", "LC_001");
                }
            });
            put("/api/tagUsageCount", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("PUT", "LC_003");
                    put("GET", "LC_002");
                }
            });
            put("/api/playContents", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("POST", "LC_005");
                    put("PUT", "LC_006");
                }
            });
            put("/api/reservateUseAPI", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("PUT", "SET");
                    put("GET", "LC_010");
                }
            });
            put("/api/codeGenre", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("GET", "LC_011");
                }
            });
            put("/api/productListAPI", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("GET", "LC_012");
                }
            });
            put("/api/tagAPI", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("POST", "LC_013");
                }
            });
            put("/api/useGenre", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("GET", "LC_014");
                }
            });
            put("/api/playContsBlock", new HashMap<String, String>() {
                {
                    put("requester", "Launcher");
                    put("POST", "BLOCK_001");
                    put("PUT", "BLOCK_002");
                }
            });
            put("/api/contentsdetail", new HashMap<String, String>() {
                {
                    put("requester", "LiveOn360");
                    put("GET", "LIVE_001");
                }
            });
            put("/api/productAPI", new HashMap<String, String>() {
                {
                    put("requester", "POS");
                    put("GET", "SET");
                }
            });
            put("/api/posAPI", new HashMap<String, String>() {
                {
                    put("requester", "POS");
                    put("POST", "POS_003");
                }
            });
            put("/api/posTagAPI", new HashMap<String, String>() {
                {
                    put("requester", "POS");
                    put("GET", "POS_004");
                }
            });
            put("/api/payAPI", new HashMap<String, String>() {
                {
                    put("requester", "POS");
                    put("GET", "POS_005");
                }
            });

            put("/api/genreList", new HashMap<String, String>() {
                {
                    put("requester", "LiveOn360");
                    put("GET", "LIVE_003");
                }
            });
            put("/api/contentsDispList", new HashMap<String, String>() {
                {
                    put("requester", "LiveOn360");
                    put("GET", "LIVE_004");
                }
            });

        }
    };

    Map<String, String> ntJsonAccess = new HashMap<String, String>() {
        {
            put("/api/fileDownload", "WEB");
            put("/api/downloadFile", "Launcher");
            put("/api/memberDownloadFile", "LiveOn360");
        }
    };

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "LogService")
    private LogService logService;

    final Logger log = LogManager.getLogger();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        try {
            HttpSession session = request.getSession(true);
            if (session.getAttribute("authenticatedUser") == null) {
                MemberVO userVO = this.getAuthenticatedUser(request);
                session.setAttribute("authenticatedUser", userVO);
            }
        } catch (Exception e) {

        }
        return true;
    }

    @Override
    @ResponseBody
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequest();
        String requestedValue = builder.buildAndExpand().getPath(); // I want this.

        File destdir = new File("/data1");
        File detdir = new File("/data1/log");
        if (!destdir.exists()) {
            destdir.mkdirs(); // 디렉토리가 존재하지 않는다면 생성
        }

        if (!detdir.exists()) {
            detdir.mkdirs(); // 디렉토리가 존재하지 않는다면 생성
        }

        String method = CommonFnc.getMethod(request);
        LogVO logVo = new LogVO();

        // 클라이언트의 요청을 처리한 뒤에 호출됨. 컨트롤러에서 예외가 발생되면 실행하지 않는다.

        // 호출 API 주소 변수 저장
        String requestDispatcherPath = (String) request.getAttribute("org.apache.catalina.core.DISPATCHER_REQUEST_PATH");
        if (requestDispatcherPath == null || requestDispatcherPath == "") {
            requestDispatcherPath = request.getServletPath();
        }
        String requester = "";
        String apiNo = "";
        String requestData = "";
        String responseData = "";
        String viewNm = "";
        String viewPath = "";
        String repMsg = "";
        String requesterPos = "";

        logVo.setViewNm("");

        int reqSize = 0;

        if (modelAndView != null) {
            if (modelAndView.getViewName() != null) {
                if (handler.getClass().toString().equals("class org.springframework.web.method.HandlerMethod")) {
                    HandlerMethod handlerMethod = (HandlerMethod) handler;
                    viewNm = handlerMethod.getMethod().getName();
                    viewPath = modelAndView.getViewName();

                    // Annotation[] declaredAnnotations = handlerMethod.getMethod().getDeclaredAnnotations();
                    // AnnotationMap annotationMap = buildAnnotationMap(declaredAnnotations);
                    // System.out.println(annotationMap.toString());
                    // int paramsLength = handlerMethod.getMethod().getParameterAnnotations().length;
                    // AnnotationMap[] paramAnnotations = new AnnotationMap[paramsLength];
                    // for (int i = 0; i < paramsLength; i++) {
                    // AnnotationMap parameterAnnotationMap = buildAnnotationMap(
                    // handlerMethod.getMethod().getParameterAnnotations()[i]);
                    // paramAnnotations[i] = parameterAnnotationMap;
                    // }
                    // System.out.println(handlerMethod.getBeanType().getName().toString());
                }

            }
            // POS API 예외처리

            if (accessAPI.containsKey(requestDispatcherPath)) {
                requester = accessAPI.get(requestDispatcherPath).get("requester");
                if (accessAPI.get(requestDispatcherPath).get(method) != null) {
                    apiNo = accessAPI.get(requestDispatcherPath).get(method);
                }

                LinkedHashMap<String, Object> a = modelAndView.getModelMap();

                /* requester 확인 조건 (START) == method 조회값이 (SET) 인경우 대비 */

                /* requester 확인 조건 (END) */

                Enumeration params = request.getParameterNames();

                // System.out.print("[");
                // System.out.print(method);
                // System.out.print("] ");
                // System.out.println(requestDispatcherPath);
                // System.out.println("----------------------------");
                // System.out.println("[request Data]");
                // System.out.println("----------------------------");

                if (apiNo.equals("POS_003")) {
                    JSONParser jsonParser = new JSONParser();
                    org.springframework.validation.BeanPropertyBindingResult resultMap = (BeanPropertyBindingResult) a
                            .get("org.springframework.validation.BindingResult.string");
                    requestData = resultMap.getTarget().toString();
                    JSONObject jsonObj = (JSONObject) jsonParser.parse(requestData);
                    requesterPos = (String) jsonObj.get("REQUESTER") == null ? "" : (String) jsonObj.get("REQUESTER");
                    reqSize = jsonObj.size();
                    viewNm = "posIO";
                    // System.out.println(requestData);
                } else {
                    while (params.hasMoreElements()) {
                        String name = (String) params.nextElement();
                        if (!name.equals("_")) {
                            reqSize++;
                            requestData += name + " : " + request.getParameter(name) + "\n";
                            // System.out.println(name + " : " + request.getParameter(name));
                        }
                    }
                }
                /* SET 파라미터 함수 정의 */
                if (apiNo.equals("SET")) {
                    if (requestDispatcherPath.equals("/api/productAPI")) {
                        if (request.getParameterMap().containsKey("PROD_ID") == false) {
                            apiNo = "POS_001";
                        } else {
                            apiNo = "POS_002";
                        }
                    } else if (requestDispatcherPath.equals("/api/reservateUseAPI")) {
                        if (request.getParameterMap().containsKey("PROD_ID") == false) {
                            apiNo = "LC_009";
                        } else {
                            int devSeq = request.getParameter("devSeq") == null ? 0
                                    : Integer.parseInt(request.getParameter("devSeq"));
                            if (devSeq == 0) {
                                apiNo = "LC_008";
                            } else {
                                apiNo = "LC_007";
                            }
                        }
                    }
                }

                // System.out.println("----------------------------");
                // System.out.println("[request Size] : " + reqSize);
                // System.out.println("----------------------------");
                // System.out.println("[response Data]");
                // System.out.println("----------------------------");

                if (apiNo.equals("POS_003")) {
                    Map<String, Object> modelMap = modelAndView.getModel();
                    Set<Entry<String, Object>> entrySet = modelMap.entrySet();
                    Iterator iterator = entrySet.iterator();
                    while (iterator.hasNext()) {
                        String result = iterator.next().toString();
                        String[] resultAry = null;
                        if (result.contains("resultCode") || result.contains("resultMsg")) {
                            resultAry = result.split("=");
                            repMsg += resultAry[0] + " : " + resultAry[1] + "\n";
                        }

                    }
                    responseData = repMsg;
                    // System.out.print(repMsg);
                } else {
                    for (Map.Entry<String, Object> entry : a.entrySet()) {
                        String key = entry.getKey();

                        if (key.length() >= 15 && key.contains("org.")) {
                            break;
                        }

                        Object value = entry.getValue();
                        String valClass = value.getClass().toString();
                        if (!valClass.contains("VO") && !valClass.contains("String") && !valClass.contains("ArrayList")
                                && !valClass.contains("HashMap")) {
                            if (valClass.contains("org.springframework")) {
                                break;
                            }
                        }

                        // System.out.print(key);
                        responseData += key;
                        String json = new ObjectMapper().writeValueAsString(value);
                        responseData += " : " + json + "\n";

                        if (key.equals("resultCode") || key.equals("resultMsg")) {
                            repMsg += key + " : " + json + "\n";
                        }

                        // System.out.print(" : ");
                        // System.out.println(json);
                    }
                }

                // System.out.println("");
                // System.out.println("[requester] : " + requester);
                // System.out.println("--------------------------");

                logVo.setApiNm(requestDispatcherPath);
                logVo.setRequester(requester);

                if (request.getParameterMap().containsKey("requester") != false) {
                    logVo.setRequester(request.getParameter("requester"));
                }

                logVo.setReqData(requestData);
                logVo.setResultData(responseData);
                logVo.setRepMsg(repMsg);
                logVo.setReqSize(reqSize);
                logVo.setViewNm(viewNm);
                logVo.setMethod(method);
                logVo.setApiNo(apiNo);
                logVo.setViewPath(viewPath);

                if (requesterPos != "") {
                    logVo.setRequester(requesterPos);
                }

                // DB 처리
                if (apiNo == null || apiNo == "" || apiNo.isEmpty() || apiNo.length() == 0) {
                    return;
                }
                if (logService.logInsert(logVo) == 1) {
                    log.warn("{} _ [{}]  {}        [request Size] : {}  [response Data] :  { {} }",logVo.getRequester(), method, requestDispatcherPath, reqSize, repMsg);
                }

            }
        } else {
            for (String key : ntJsonAccess.keySet()) {
                if (requestDispatcherPath.contains(key)) {
                    requester = ntJsonAccess.get(key);
                    // 파일 다운로드 로그 처리
                    logVo.setApiNm(key);
                    logVo.setRequester(requester);
                    logVo.setReqData(requestDispatcherPath);
                    if (request.getParameterMap().containsKey("requester") != false) {
                        logVo.setRequester(request.getParameter("requester"));
                    }

                    if (response.getStatus() == HttpStatus.SC_OK) {
                        repMsg = "Status : \"200\" \nresultMsg : \"Success\"";
                    } else {
                        repMsg = "Status : " + response.getStatus() + " \nresultMsg : \"ERROR\"";
                    }

                    Enumeration params = request.getParameterNames();
                    while (params.hasMoreElements()) {
                        String name = (String) params.nextElement();
                        if (!name.equals("_")) {
                            reqSize++;
                            requestData += name + " : " + request.getParameter(name) + "\n";
                            // System.out.println(name + " : " + request.getParameter(name));
                        }
                    }

                    logVo.setResultData(repMsg);
                    logVo.setRepMsg(repMsg);
                    logVo.setReqSize(reqSize);
                    logVo.setViewNm("FILE");
                    logVo.setMethod(method);

                    switch (key) {
                        case "/api/fileDownload":
                            logVo.setApiNo("LC_016");
                            apiNo = "LC_016";
                            break;

                        case "/api/downloadFile":
                            logVo.setApiNo("LC_015");
                            apiNo = "LC_015";
                            break;

                        case "/api/memberDownloadFile":
                            logVo.setApiNo("LIVE_002");
                            apiNo = "LIVE_002";
                            break;
                    }

                    // DB 처리
                    if (apiNo == null || apiNo == "" || apiNo.isEmpty() || apiNo.length() == 0) {
                        return;
                    }

                    log.warn("{} _ [{}]  {}        [request Size] : {}  [response Data] :  { {} }",logVo.getRequester(), method, requestDispatcherPath, reqSize, repMsg);

                    logService.logInsert(logVo);

                    // System.out.println("--------------------------");
                    // System.out.print("[");
                    // System.out.print(method);
                    // System.out.print("] ");
                    // System.out.println(requestDispatcherPath);
                    // System.out.println("[requester] : " + requester);
                    // System.out.println("--------------------------");
                }
            }
        }

        if (modelAndView != null && modelAndView.getView() instanceof RedirectView) {
            RedirectView redirect = (RedirectView) modelAndView.getView();
            String url = redirect.getUrl();
            if (url.contains("code=") || url.contains("error=")) {
                HttpSession session = request.getSession(false);
                if (session != null) {
                    session.invalidate();
                }
            }
        }

        // 예외처리 다운로드

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
            Exception exception) throws Exception {
        // 클라이언트 요청 처리뒤 클리이언트에 뷰를 통해 응답을 전송한뒤 실행 됨. 뷰를 생설항때 예외가 발생해도 실행된다
        if (request.getSession().isNew()) {
            request.getSession().invalidate();
        }
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        // 동시에 핸들링
    }

    private MemberVO getAuthenticatedUser(HttpServletRequest request) throws Exception {
        MemberVO userVO = null;

        boolean hasSession = (request.getSession().getAttribute("seq") != null);
        boolean hasToken = (request.getHeader("x-token") != null);
        if (hasSession) {
            userVO = new MemberVO();
            userVO.setMbrSeq(Integer.parseInt((String) request.getSession().getAttribute("seq")));
            userVO.setMbrId((String) request.getSession().getAttribute("id"));
            userVO.setMbrNm((String) request.getSession().getAttribute("name"));
            userVO.setMbrSe((String) request.getSession().getAttribute("memberSec"));
            userVO.setMbrLevel(Integer.parseInt((String) request.getSession().getAttribute("level")));
            userVO.setSvcSeq(Integer.parseInt((String) request.getSession().getAttribute("svcSeq")));
            userVO.setStorSeq(Integer.parseInt((String) request.getSession().getAttribute("storSeq")));
            userVO.setCpSeq(Integer.parseInt((String) request.getSession().getAttribute("cpSeq")));
            userVO.setSvcType((String) request.getSession().getAttribute("svcType"));
        } else if (hasToken) {
            MemberVO vo = new MemberVO();
            vo.setMbrTokn(request.getHeader("x-token"));
            userVO = memberService.getMember(vo);
        }

        if (userVO != null) {
            String clientIp = CommonFnc.getClientIp(request);
            userVO.setIpadr(clientIp);
        }
        return userVO;
    }

    private static AnnotationMap buildAnnotationMap(Annotation[] declaredAnnotations) {
        AnnotationMap annotationMap = new AnnotationMap();
        for (Annotation annotation : declaredAnnotations) {
            annotationMap.add(annotation);
        }
        return annotationMap;
    }

}
