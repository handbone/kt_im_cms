/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.common.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
*
* 파라미터 유효성 검증 관련 클래스를 정의한다
*
* @author A2TEC
* @since 2018.06.15
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 15.   A2TEC      최초생성
*
*
* </pre>
*/

public class Validator {

    public static final String REQUIRED = "required";
    public static final String NUMBER = "number";
    public static final String LENGTH = "length";
    public static final String MIN_LENGTH = "minLength";
    public static final String MAX_LENGTH = "maxLength";
    public static final String TEL_NO = "telNo";
    public static final String PHONE_NO = "phoneNo";
    public static final String EMAIL = "email";
    public static final String BIZ_NO = "bizNo";
    public static final String IP_ADDRESS = "ipAddress";
    public static final String DATE = "date";
    public static final String DATE_TYPE = "dateType";
    public static final String ID = "memberId";
    public static final String PWD = "memberPassword";
    public static final String NAME = "memberName";

    public static final String EMPTY_PARAM = "";

    static private Map<String, Object> parse(String request) {
        if (request == null || request.isEmpty())
            return null;

        Map<String, Object> requestMap = new LinkedHashMap<String, Object>();
        String[] requests = request.split(";");
        for (int i = 0; i < requests.length; i++) {
            String item = requests[i];

            Map<String, String> attributeMap = new LinkedHashMap<String, String>();
            attributeMap.put(REQUIRED, EMPTY_PARAM);
 
            int nameEndIndex = item.indexOf(":");
            if (nameEndIndex == -1) {
                requestMap.put(item, attributeMap);
                continue;
            }

            String objName = item.substring(0, nameEndIndex).trim();
            String attributeText = item.substring(nameEndIndex + 1).trim();

            int openBracketIndex = attributeText.indexOf("{");
            int closeBracketIndex = attributeText.lastIndexOf("}");

            boolean errorSyntax = (openBracketIndex == -1) || (closeBracketIndex == -1);
            if (errorSyntax) {
                return null;
            }
            attributeText = attributeText.substring(openBracketIndex + 1, closeBracketIndex).trim();
            if (!attributeText.isEmpty()) {
                String[] attributes = attributeText.split("&");
                for (int j = 0; j < attributes.length; j++) {
                    String[] attributeInfo = attributes[j].split("=");
                    String key = attributeInfo[0].trim();
                    String value = (attributeInfo.length == 1) ? EMPTY_PARAM : attributeInfo[1].trim();
                    attributeMap.put(key, value);
                }
            }
            requestMap.put(objName, attributeMap);
        }
        return requestMap;
    }

    @SuppressWarnings("unchecked")
    static public String validate(HttpServletRequest request, String requestParam) {
        Map<String, Object> requestMap = parse(requestParam);
        if (requestMap == null || requestMap.isEmpty()) {
            return "error validator";
        }

        String invalidTargets = "";
        Iterator<String> targetIterator = requestMap.keySet().iterator();

        while (targetIterator.hasNext()) {
            String targetName = targetIterator.next();
            String targetValue = request.getParameter(targetName);

            String invalidTarget = "";
            boolean isValidTarget = true;

            Map<String, String> validationMap = (Map<String, String>)requestMap.get(targetName);
            Iterator<String> validationsIterator = validationMap.keySet().iterator();
            outer:
            while (validationsIterator.hasNext()) {
                String validationName = validationsIterator.next();
                String validationValue = (String)validationMap.get(validationName);

                boolean isValidPart = true;

                switch (validationName) {
                case REQUIRED:
                    isValidPart = FormatCheckUtil.checkNotEmpty(targetValue);
                    if (!isValidPart) {
                        invalidTarget = validationName;
                        isValidTarget = isValidPart;
                        break outer;
                    }
                    break;
                case NUMBER:
                    isValidPart = FormatCheckUtil.checkNumber(targetValue);
                    break;
                case LENGTH:
                    String[] length = validationValue.split(",");
                    int minLength = Integer.parseInt(length[0].trim());
                    int maxLength = Integer.parseInt(length[1].trim());
                    isValidPart = FormatCheckUtil.checkLength(targetValue, minLength, maxLength);
                    break;
                case MIN_LENGTH:
                    isValidPart = FormatCheckUtil.checkMinLength(targetValue, Integer.parseInt(validationValue));
                    break;
                case MAX_LENGTH:
                    isValidPart = FormatCheckUtil.checkMaxLength(targetValue, Integer.parseInt(validationValue));
                    break;
                case TEL_NO:
                    isValidPart = FormatCheckUtil.checkFormatTell(targetValue);
                    break;
                case PHONE_NO:
                    String[] targetList = targetValue.split(";");
                    for (int i = 0; i < targetList.length; i++) {
                        String phono = targetList[i].split("=")[0];
                        isValidPart = FormatCheckUtil.checkFormatCell(phono);
                        if (isValidPart == false && !phono.contains("*")) {
                            break;
                        } else {
                            isValidPart = true;
                        }
                    }
                    break;
                case EMAIL:
                    isValidPart = FormatCheckUtil.checkFormatMail(targetValue);
                    break;
                case BIZ_NO:
                    isValidPart = FormatCheckUtil.checkCompNumber(targetValue);
                    break;
                case IP_ADDRESS:
                    isValidPart = FormatCheckUtil.checkIPAddress(targetValue);
                    break;
                case DATE:
                    isValidPart = FormatCheckUtil.checkDate(targetValue);
                    break;
                case DATE_TYPE:
                    isValidPart = FormatCheckUtil.checkDateType(targetValue);
                    break;
                case ID:
                    isValidPart = FormatCheckUtil.checkMemberId(targetValue);
                    validationName = "format";
                    break;
                case PWD:
                    isValidPart = FormatCheckUtil.checkMemberPassword(targetValue);
                    validationName = "format";
                    break;
                case NAME:
                    isValidPart = FormatCheckUtil.checkMemberName(targetValue);
                    validationName = "format";
                    break;
                default:
                    isValidPart = false;
                    validationName += " is not supported";
                    break;
                }

                if (!isValidPart) {
                    if (!invalidTarget.isEmpty()) {
                        invalidTarget += ",";
                    }
                    invalidTarget += validationName;
                    isValidTarget &= isValidPart;
                }
            }

            if (!isValidTarget) {
                if (!invalidTargets.isEmpty()) {
                    invalidTargets += ",";
                }
                invalidTargets += targetName + "(" + invalidTarget + ")";
            }
        }

        return invalidTargets;
    }
}
