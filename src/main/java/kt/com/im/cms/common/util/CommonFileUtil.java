package kt.com.im.cms.common.util;

import java.io.File;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.file.web.FileApi;
import kt.com.im.cms.member.vo.MemberVO;

@Component
public class CommonFileUtil {

    public final static String fileEmpty = "file is empty";
    public final static String fileNameErr = "file name error";
    public final static String fileExtErr = "file extension error";
    public final static String fileSeErr = "fileSe error";
    public final static String serverErr = "server error";
    public final static String success = "success";

    @Resource(name = "FileService")
    private FileService fileService;

    @Inject
    private FileSystemResource fsResource;

    public String insertFile(HttpServletRequest request, MemberVO userVO, int seq, String fileSe) throws Exception {
        String pathBasic = fsResource.getPath();

        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
        //List<MultipartFile> files = multipartHttpServletRequest.getFiles("file");

        Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
        MultipartFile multipartFile = null;
        //List resultList = new ArrayList();

        try {
            // 디레토리가 없다면 생성
            File dir = new File(pathBasic);
            if (!dir.isDirectory()) {
                dir.mkdirs();
            }

            while (iterator.hasNext()) {
                int contsSeq = seq;
                //String fileSe = "APP_IMG";
                multipartFile = multipartHttpServletRequest.getFile(iterator.next());

                if (multipartFile.isEmpty()) {
                    return fileEmpty;
                }

                //String flieName = multipartFile.getName();
                String orginlFileNm = new String(multipartFile.getOriginalFilename().getBytes("UTF-8"), "UTF-8"); // 한글꺠짐
                if (!FileApi.isValidFileName(orginlFileNm)) {
                    return fileNameErr;
                }
                long filesize = multipartFile.getSize();

                String ext = orginlFileNm.substring(orginlFileNm.lastIndexOf('.')); //
                String now = new SimpleDateFormat("yyMMddHmsS").format(new Date()); // 현재시간
                String path = "";

                if (!isAllowUploadImgFileType(ext)) {
                    return fileExtErr;
                }

                switch(fileSe) {
                    case "APP_IMG":
                        path += "/app/" + seq;
                        break;
                    case "NOTI_POPUP":
                        path += "/svcNotice/" + seq + "/popup";
                        break;
                    default:
                        break;
                }

                if (path.isEmpty()) {
                    return fileSeErr;
                }

                // 디레토리가 없다면 생성
                File dir2 = new File(pathBasic + path);
                if (!dir2.isDirectory()) {
                    dir2.mkdirs();
                }

                String saveFileName = now + ext;
                File serverFile = new File(pathBasic + path + File.separator + saveFileName);

                multipartFile.transferTo(serverFile);
                FileVO file = new FileVO();
                file.setOrginlFileNm(orginlFileNm);
                file.setFileContsSeq(contsSeq);
                file.setStreFileNm(saveFileName);
                file.setFileSize(BigInteger.valueOf(filesize));
                file.setFileDir(path + "/" + saveFileName);
                file.setFileSe(fileSe);
                file.setCretrId(userVO.getMbrId());

                //resultList.add(file);

                int resultFile = fileService.fileInsert(file);
                if (resultFile != 1) {
                    return serverErr;
                }
            }
        } catch(Exception e) {
            return serverErr;
        }

        return success;
    }

    public void deleteFiles(String delFileSeqs) throws Exception {
        List<String> deleteFilePath = new ArrayList<String>();
        String[] delFileSeq = delFileSeqs.split(",");

        for (int i = 0; i < delFileSeq.length; i++) {
            FileVO fvo = new FileVO();
            fvo.setFileSeq(Integer.parseInt(delFileSeq[i]));
            FileVO fItem = fileService.fileSeqInfo(fvo);
            String filePath = fItem.getFilePath();
            if (fileService.fileSeqDelete(fvo) == 1) {
                deleteFilePath.add(filePath);
            }
        }

        for (int i = 0; i < deleteFilePath.size(); i++) {
            File f = new File(fsResource.getPath() + deleteFilePath.get(i));
            f.delete();
        }
    }

    // 실제 서버에 파일이 존재하는지 여부 확인
    public boolean isExistFile(String filePath) {
        if (filePath == null || filePath.isEmpty()) {
            return false;
        }

        File file = new File(fsResource.getPath() + filePath);
        if (file.isFile()) {
            return true;
        }

        return false;
    }

    // 공지사항 팝업 URL, 배너 이미지, app 이미지
    static public boolean isAllowUploadImgFileType(String fileType) {
        boolean isAllow = false;
        String[] allowFileTypeArr = { ".png", ".jpg", ".jpeg", ".bmp" };

        for (String allowFileType : allowFileTypeArr) {
            if (allowFileType.equalsIgnoreCase(fileType)) {
                isAllow = true;
                break;
            }
        }

        return isAllow;
    }
}