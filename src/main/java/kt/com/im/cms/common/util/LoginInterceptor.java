/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.common.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private URIAccessChecker uriAccessChecker;

    @Autowired
    private MenuMaker menuMaker;

    @Autowired
    private ConfigProperty configProperty;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            if (!uriAccessChecker.check(request)) {
                String requestUri = URIAccessChecker.removeSlash(request.getRequestURI());
                requestUri = "?uri=" + requestUri.replace(request.getContextPath(), "");
                response.sendRedirect(request.getContextPath() + "/" + requestUri);
                return false;
            }
        } catch (Exception e) {

        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //클라이언트의 요청을 처리한 뒤에 호출됨. 컨트롤러에서 예외가 발생되면 실행하지 않는다.
        modelAndView.addObject("menuList", menuMaker.get(request));
        String useAutoSend = ("Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.sms")) || "Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.email"))) ? "Y" : "N";
        modelAndView.addObject("usesAutoSend", useAutoSend);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
        //클라이언트 요청 처리뒤 클리이언트에 뷰를 통해 응답을 전송한뒤 실행 됨. 뷰를 생설항때 예외가 발생해도 실행된다
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //동시에 핸들링
    }
}
