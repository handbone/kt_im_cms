/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.store.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ConfigProperty;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.content.service.ContentService;
import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.contentprovider.service.CpService;
import kt.com.im.cms.contentprovider.vo.CpVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.reservation.service.ReservationService;
import kt.com.im.cms.reservation.vo.ReservationVO;
import kt.com.im.cms.servics.service.ServicsService;
import kt.com.im.cms.servics.vo.ServicsVO;
import kt.com.im.cms.store.service.StoreService;
import kt.com.im.cms.store.vo.StoreVO;

/**
 *
 * 매장 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 18.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class StoreApi {
    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "CpService")
    private CpService cpService;

    @Resource(name = "ServicsService")
    private ServicsService servicsService;

    @Resource(name = "StoreService")
    private StoreService storeService;

    @Resource(name = "ReservationService")
    private ReservationService reservationService;

    @Resource(name = "memberTransactionManager")
    private DataSourceTransactionManager transactionManager;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager txManager;

    @Autowired
    private ConfigProperty configProperty;

    /**
     * 매장 관련 처리 API (GET-서비스 목록)
     *
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return modelAndView & API 메세지값 & WebStatus
     *         + GET - 검색조건에 따른 코드 리스트
     */
    @RequestMapping(value = "/api/store")
    public ModelAndView storeProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/store/storeProcess");

        // 로그인 되지 않았거나 권한이 없을경우 접근불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        StoreVO item = new StoreVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "svcSeq,storNm,storCode");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            String storNm = CommonFnc.emptyCheckString("storNm", request);
            item.setSvcSeq(svcSeq);
            item.setStorNm(storNm);

            // 동일 서비스에 동일한 매장 명이 존재하는지 확인
            boolean isExistSvcNm = (storeService.searchStoreInfo(item) > 0) ? true : false;
            if (isExistSvcNm) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist storNm");
                return rsModel.getModelAndView();
            }

            String storCode = CommonFnc.emptyCheckString("storCode", request);
            item.setStorCode(storCode);

            boolean isExistStorCode = (storeService.searchStoreInfo(item) > 0) ? true : false;
            if (isExistStorCode) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist storeCode");
                return rsModel.getModelAndView();
            }

            String zipCode = CommonFnc.emptyCheckString("zipCode", request);
            String basAddr = CommonFnc.emptyCheckString("basAddr", request);
            String dtlAddr = CommonFnc.emptyCheckString("dtlAddr", request);
            String storTelNo = CommonFnc.emptyCheckString("storTelNo", request);

            item.setStorTelNo(storTelNo);
            item.setCretrId(userVO.getMbrId());
            item.setStorBasAddr(basAddr);
            item.setStorDtlAddr(dtlAddr);
            item.setZipcd(zipCode);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = storeService.insertStore(item);

                if (result == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);

                    ReservationVO rItem = new ReservationVO();
                    rItem.setStTime("11:00");
                    rItem.setFnsTime("17:00");
                    rItem.setStorSeq(item.getStorSeq());
                    rItem.setRsrvTimeSetCycle(30);
                    reservationService.resveSetInsert(rItem);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            /* Checking Mendatory S */
            String checkString = CommonFnc.requiredChecking(request, "storSeq,storNm,storCode,useYn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            String storNm = CommonFnc.emptyCheckString("storNm", request);

            item.setStorSeq(storSeq);
            item.setSvcSeq(svcSeq);
            item.setStorNm(storNm);

            // 동일 서비스에 동일한 매장 명이 존재하는지 확인
            boolean isExistSvcNm = (storeService.searchStoreInfo(item) > 0) ? true : false;
            if (isExistSvcNm) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist storNm");
                return rsModel.getModelAndView();
            }

            String storCode = CommonFnc.emptyCheckString("storCode", request);
            item.setStorCode(storCode);

            boolean isExistStorCode = (storeService.searchStoreInfo(item) > 0) ? true : false;
            if (isExistStorCode) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                rsModel.setResultMessage("exist storeCode");
                return rsModel.getModelAndView();
            }

            String zipCode = CommonFnc.emptyCheckString("zipCode", request);
            String basAddr = CommonFnc.emptyCheckString("basAddr", request);
            String dtlAddr = CommonFnc.emptyCheckString("dtlAddr", request);
            String storTelNo = CommonFnc.emptyCheckString("storTelNo", request);
            String useYn = CommonFnc.emptyCheckString("useYn", request);

            item.setStorTelNo(storTelNo);
            item.setUseYn(useYn);
            item.setAmdrId(userVO.getMbrId());
            item.setStorBasAddr(basAddr);
            item.setStorDtlAddr(dtlAddr);
            item.setZipcd(zipCode);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = txManager.getTransaction(def);

            try {
                storeService.updateSettlementInfo(item);

                int result = storeService.updateStore(item);
                if (result == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    txManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    txManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                txManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    txManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            item.setLoginId(userVO.getMbrId());
            item.setMbrSe(userVO.getMbrSe());

            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

            String ratContNo = CommonFnc.emptyCheckString("ratContNo", request);

            item.setRatContNo(ratContNo);

            if (storSeq != 0) {
                item.setStorSeq(storSeq);
                StoreVO resultItem = storeService.storeDetail(item);
                List<StoreVO> resultCItem = storeService.storeUseDevCount(item);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                } else {
                    if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                            && userVO.getSvcSeq() != resultItem.getSvcSeq())
                            || (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)
                                    && userVO.getStorSeq() != resultItem.getStorSeq())) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    /* 상세정보 요청일 경우 개인정보 마스킹 (전화번호, 주소, 등록자 아이디, 수정자 아이디), 수정 시의 상세정보는 내용 그대로 전달 */
                    String getType = CommonFnc.emptyCheckString("getType", request);
                    if (getType.equals("")) {
                        resultItem.setStorTelNo(CommonFnc.getTelnoMask(resultItem.getStorTelNo()));
                        resultItem.setStorBasAddr(CommonFnc.getAddrMask(resultItem.getStorBasAddr()));
                        resultItem.setStorDtlAddr("");
                        resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                        resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("ratContNo", ratContNo);
                    rsModel.setData("devCount", resultCItem);
                }
                rsModel.setResultType("storeDetail");
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (storSeq == 0 && CommonFnc.checkReqParameter(request, "storSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                String searchType = CommonFnc.emptyCheckString("searchType", request);
                int searchSvcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                String useYn = CommonFnc.emptyCheckString("useYn", request);
                item.setUseYn(useYn);
                item.setSvcSeq(searchSvcSeq);

                List<StoreVO> resultItem;
                if (searchType.equals("list")) { // select box 등에 사용되는 단순 매장명 목록 조회
                    if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_MASTER)
                            || userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_CMS)
                            || userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)) {

                        resultItem = storeService.storeNameListForManager(item);
                    } else {
                        item.setLoginId(userVO.getMbrId());
                        resultItem = storeService.storeNameListForStoreManager(item);
                    }

                    if (resultItem.isEmpty()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setData("item", resultItem);
                    }
                    rsModel.setResultType("storeNmList");
                } else {
                    String searchConfirm = request.getParameter("_search") == null ? "false"
                            : request.getParameter("_search");
                    item.setSearchConfirm(searchConfirm);
                    // 검색 여부가 true일 경우
                    if (searchConfirm.equals("true")) {
                        String searchField = CommonFnc.emptyCheckString("searchField", request);
                        String searchString = CommonFnc.emptyCheckString("searchString", request);

                        item.setSearchTarget(searchField);
                        item.setSearchKeyword(searchString);
                    }
                    /*
                     * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                     */
                    int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                    int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
                    String sidx = CommonFnc.emptyCheckString("sidx", request);
                    String sord = CommonFnc.emptyCheckString("sord", request);

                    int page = (currentPage - 1) * limit + 1;
                    int pageEnd = page + limit - 1;

                    item.setSidx(sidx);
                    item.setSord(sord);
                    item.setOffset(page);
                    item.setLimit(pageEnd);

                    resultItem = storeService.storeList(item);
                    if (resultItem.isEmpty()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    } else {
                        /* 개인정보 마스킹 (전화번호, 주소) */
                        for (int i = 0; i < resultItem.size(); i++) {
                            resultItem.get(i).setStorTelNo(CommonFnc.getTelnoMask(resultItem.get(i).getStorTelNo()));
                            resultItem.get(i).setStorBasAddr(CommonFnc.getAddrMask(resultItem.get(i).getStorBasAddr()));
                        }

                        int totalCount = storeService.storeListTotalCount(item);
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("totalCount", totalCount);
                        rsModel.setData("row", limit);
                        rsModel.setData("currentPage", currentPage);
                        rsModel.setData("totalPage", totalCount / limit);
                        rsModel.setResultType("storeList");
                    }
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 매장 관련 처리 API (GET-서비스 목록)
     *
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return modelAndView & API 메세지값 & WebStatus
     *         + GET - 검색조건에 따른 코드 리스트
     */
    @RequestMapping(value = "/api/billing")
    public ModelAndView billingProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/store/storeProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isAdministrator(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        StoreVO item = new StoreVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

            item.setSvcSeq(svcSeq);
            item.setStorSeq(storSeq);

            /** 검색여부 */
            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");

            item.setSearchConfirm(searchConfirm);
            if (searchConfirm.equals("true")) {
                /** 검색 카테고리 */
                String searchField = request.getParameter("searchField") == null ? ""
                        : request.getParameter("searchField");

                /** 검색어 */
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.setSearchTarget(searchField);
                item.setSearchKeyword(searchString);
            }

            /** 현재 페이지 */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));

            /** 검색에 출력될 개수 */
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

            /** 정렬할 필드 */
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

            /** 정렬 방법 */
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
            int page = (currentPage - 1) * limit + 1;

            /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
            int pageEnd = (currentPage - 1) * limit + limit;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정

            if (sidx.equals("SVC_NM")) {
            } else if (sidx.equals("STOR_NM")) {
            } else if (sidx.equals("COUNT")) {
            } else if (sidx.equals("TOT_PRICE")) {
            } else if (sidx.equals("SVC_SEQ")) {
            } else {
                sidx = "REG_DT";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setOffset(page);
            item.setLimit(pageEnd);
            // item.setUseYn("Y");

            /** 로그인 중인 계정 레벨 값 */
            int user_level = session.getAttribute("level") == null ? 0
                    : Integer.parseInt((String) session.getAttribute("level"));

            // CP사 정보(사용자 로그인 일 경우 해당 멤버가 작성한 콘텐츠만 출력)
            // if (user_level < 5) {
            // cpSeq = session.getAttribute("cpSeq") == null ? 0
            // : Integer.parseInt((String) session.getAttribute("cpSeq"));
            //
            // item.setCpSeq(cpSeq);
            // }

            List<StoreVO> resultItem = storeService.billingList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                /** 가져온 리스트 총 개수 */
                int totalCount = storeService.billingListTotalCount(item);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("storeBillingList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 계약 정보 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/billingDetail")
    public ModelAndView contractDetailProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/store/storeProcess");

        String Url_Block = configProperty.getProperty("chain.url");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isAdministrator(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        StoreVO item = new StoreVO();
        ContentVO Citem = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                String checkString = CommonFnc.requiredChecking(request,
                        "ratSeq,svcSeq,storSeq,ratContNo,ratYn,ratContStDt,ratContFnsDt,ratContType,runLmtCnt,runPerPrc,ratTime,ratPrc,lmsmpyPrc,contsSeqs");

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int ratSeq = CommonFnc.emptyCheckInt("ratSeq", request);
                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                int runLmtCnt = CommonFnc.emptyCheckInt("runLmtCnt", request);
                int runPerPrc = CommonFnc.emptyCheckInt("runPerPrc", request);
                int ratTime = CommonFnc.emptyCheckInt("ratTime", request);
                int ratPrc = CommonFnc.emptyCheckInt("ratPrc", request);
                int lmsmpyPrc = CommonFnc.emptyCheckInt("lmsmpyPrc", request);

                String ratContNo = CommonFnc.emptyCheckString("ratContNo", request);
                String ratYn = CommonFnc.emptyCheckString("ratYn", request);
                String ratContStDt = CommonFnc.emptyCheckString("ratContStDt", request);
                String ratContFnsDt = CommonFnc.emptyCheckString("ratContFnsDt", request);
                String ratContType = CommonFnc.emptyCheckString("ratContType", request);

                item.setRatSeq(ratSeq);
                item.setStorSeq(storSeq);
                item.setSvcSeq(svcSeq);
                item.setRunLmtCnt(runLmtCnt);
                item.setRunPerPrc(runPerPrc);
                item.setRatTime(ratTime);
                item.setRatPrc(ratPrc);
                item.setLmsmpyPrc(lmsmpyPrc);
                item.setRatContNo(ratContNo);
                item.setRatYn(ratYn);
                item.setRatContStDt(ratContStDt);
                item.setRatContFnsDt(ratContFnsDt);
                item.setRatContType(ratContType);
                item.setCretrId((String) session.getAttribute("id"));

                try {

                    int result = storeService.billingUpdate(item);

                    if (result == 1) {
                        String contsSeqs = CommonFnc.emptyCheckString("contsSeqs", request);

                        String[] contsSeq = contsSeqs.split(",");

                        List<Integer> list = new ArrayList<Integer>();

                        storeService.ratContsDelete(item);

                        for (int i = 0; i < contsSeq.length; i++) {
                            item.setContsSeq(Integer.parseInt(contsSeq[i]));
                            result = storeService.ratContsUpdate(item);
                        }

                        String changed = CommonFnc.emptyCheckString("changed", request);
                        if (changed.equals("true")) {
                            item.setContsSeqs(contsSeqs);
                            result = storeService.ratHstInsert(item);
                        }

                        /* BLOCK CHAIN Start */
                        String postParamUrl = Url_Block + "/api/v1/contract/service";
                        int totCount = runLmtCnt;
                        int time = ratTime;
                        int totPrice = lmsmpyPrc + runPerPrc + ratPrc;

                        String message;
                        JSONObject json = new JSONObject();
                        json.put("ratContNo", String.valueOf(ratContNo));
                        json.put("svcSeq", String.valueOf(svcSeq));
                        json.put("storSeq", String.valueOf(storSeq));
                        json.put("contsSeq", contsSeqs);
                        json.put("ratYn", ratYn);
                        json.put("ratContStDt", ratContStDt + "T23:59:00.000Z");
                        json.put("ratContFnsDt", ratContFnsDt + "T23:59:00.000Z");
                        json.put("ratContType", ratContType);
                        json.put("price", String.valueOf(totPrice));
                        json.put("count", String.valueOf(totCount));
                        json.put("time", String.valueOf(ratTime));

                        postJson(postParamUrl, "127.0.0.1", json.toString(), "PUT");
                        /* BLOCK CHAIN End */

                        if (result != 0) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        }

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                        transactionManager.commit(status);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("billingUpdate");
                return rsModel.getModelAndView();
            } else { // post
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                String checkString = CommonFnc.requiredChecking(request,
                        "svcSeq,storSeq,ratContNo,ratYn,ratContStDt,ratContFnsDt,ratContType,runLmtCnt,runPerPrc,ratTime,ratPrc,lmsmpyPrc,contsSeqs");

                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                int runLmtCnt = CommonFnc.emptyCheckInt("runLmtCnt", request);
                int runPerPrc = CommonFnc.emptyCheckInt("runPerPrc", request);
                int ratTime = CommonFnc.emptyCheckInt("ratTime", request);
                int ratPrc = CommonFnc.emptyCheckInt("ratPrc", request);
                int lmsmpyPrc = CommonFnc.emptyCheckInt("lmsmpyPrc", request);

                String ratContNo = CommonFnc.emptyCheckString("ratContNo", request);
                String ratYn = CommonFnc.emptyCheckString("ratYn", request);
                String ratContStDt = CommonFnc.emptyCheckString("ratContStDt", request);
                String ratContFnsDt = CommonFnc.emptyCheckString("ratContFnsDt", request);
                String ratContType = CommonFnc.emptyCheckString("ratContType", request);

                item.setSvcSeq(svcSeq);
                item.setStorSeq(storSeq);
                item.setRunLmtCnt(runLmtCnt);
                item.setRunPerPrc(runPerPrc);
                item.setRatTime(ratTime);
                item.setRatPrc(ratPrc);
                item.setLmsmpyPrc(lmsmpyPrc);
                item.setRatContNo(ratContNo);
                item.setRatYn(ratYn);
                item.setRatContStDt(ratContStDt);
                item.setRatContFnsDt(ratContFnsDt);
                item.setRatContType(ratContType);
                item.setCretrId((String) session.getAttribute("id"));

                try {
                    int result = storeService.billingInsert(item);

                    if (result == 1) {
                        String contsSeqs = CommonFnc.emptyCheckString("contsSeqs", request);

                        String[] contsSeq = contsSeqs.split(",");

                        List<Integer> list = new ArrayList<Integer>();
                        for (int i = 0; i < contsSeq.length; i++) {
                            item.setContsSeq(Integer.parseInt(contsSeq[i]));
                            result = storeService.billingContsInsert(item);
                        }

                        if (result != 0) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        }

                        /* BLOCK CHAIN Start */
                        String postParamUrl = Url_Block + "/api/v1/contract/service";
                        int totCount = runLmtCnt;
                        int time = ratTime;
                        int totPrice = lmsmpyPrc + runPerPrc + ratPrc;

                        String message;
                        JSONObject json = new JSONObject();
                        json.put("ratContNo", String.valueOf(ratContNo));
                        json.put("svcSeq", String.valueOf(svcSeq));
                        json.put("storSeq", String.valueOf(storSeq));
                        json.put("contsSeq", contsSeqs);
                        json.put("ratYn", ratYn);
                        json.put("ratContStDt", ratContStDt + "T23:59:00.000Z");
                        json.put("ratContFnsDt", ratContFnsDt + "T23:59:00.000Z");
                        json.put("ratContType", ratContType);
                        json.put("price", String.valueOf(totPrice));
                        json.put("count", String.valueOf(totCount));
                        json.put("time", String.valueOf(ratTime));

                        postJson(postParamUrl, "127.0.0.1", json.toString(), "POST");
                        /* BLOCK CHAIN End */

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                        transactionManager.commit(status);
                    }
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }

                rsModel.setResultType("contractInsert");
                return rsModel.getModelAndView();
            }
        } else { // get
            /** 매장 번호(검색조건) */
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            int ratSeq = CommonFnc.emptyCheckInt("ratSeq", request);

            item.setStorSeq(storSeq);

            if (ratSeq != 0) {
                item.setRatSeq(ratSeq);

                StoreVO resultItem = storeService.billingInfo(item);
                item.setStorSeq(resultItem.getStorSeq());

                /** 해당 콘텐츠 상세정보 */
                StoreVO resultCItem = storeService.storeDetail(item);
                if (resultCItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_PRECED_NO_DATA);
                    return rsModel.getModelAndView();
                }

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                    resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    String[] contsSeqs;
                    List<ContentVO> resultCntListItem = new ArrayList<ContentVO>();
                    if (!resultItem.getContsSeqs().equals("")) {
                        contsSeqs = resultItem.getContsSeqs().split(",");

                        resultCntListItem = new ArrayList<ContentVO>();
                        for (int i = 0; i < contsSeqs.length; i++) {
                            Citem = new ContentVO();
                            Citem.setList(null);
                            Citem.setDelYn("N");
                            Citem.setCpSeq(0);
                            Citem.setSvcSeq(0);
                            Citem.setContsSeq(Integer.parseInt(contsSeqs[i]));

                            ContentVO resultCntItem = contentService.contentCdtNotInfo(Citem);
                            if (resultCntItem != null) {
                                resultCntListItem.add(resultCntItem);
                            }

                        }
                    }

                    rsModel.setData("item", resultItem);
                    rsModel.setData("cntItem", resultCntListItem);
                    if (resultCItem != null) {
                        rsModel.setData("marketNm", resultCItem.getSvcNm() + "(" + resultCItem.getStorNm() + ")");
                        rsModel.setData("svcNm", resultCItem.getSvcNm());
                        rsModel.setData("storNm", resultCItem.getStorNm());
                    }
                    rsModel.setResultType("billingInfo");
                }
            } else {
                StoreVO resultItem = storeService.storeDetail(item);
                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_PRECED_NO_DATA);
                    return rsModel.getModelAndView();
                }

                /** 검색여부 */
                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    item.setTarget(searchField);
                    item.setSearchKeyword(searchString);
                }

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정

                if (sidx.equals("RAT_SEQ")) {
                } else if (sidx.equals("RAT_CONT_NO")) {
                } else if (sidx.equals("CONTS_TITLE")) {
                } else if (sidx.equals("RAT_CONT_TYPE")) {
                } else if (sidx.equals("REG_DT")) {
                } else if (sidx.equals("SET_VAL")) {
                } else if (sidx.equals("TOT_PRICE")) {
                } else if (sidx.equals("USE_PRICE")) {
                } else if (sidx.equals("CRETR_NM")) {
                } else if (sidx.equals("AMD_DT")) {
                } else if (sidx.equals("CONTS_COUNT")) {
                } else {
                    sidx = "RAT_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);
                // sitem.setUseYn("Y");

                /** 로그인 중인 계정 레벨 값 */
                // int user_level = session.getAttribute("level") == null ? 0
                // : Integer.parseInt((String) session.getAttribute("level"));

                // // 매장 정보(매장 관리자 일 경우 해당 매장 정보만 추출)
                // if (user_level > 5) {
                // storSeq = session.getAttribute("storSeq") == null ? 0
                // : Integer.parseInt((String) session.getAttribute("storSeq"));
                //
                // item.setStorSeq(storSeq);
                // }

                List<StoreVO> resultCItem = storeService.billingDetailList(item);

                if (resultItem != null) {
                    rsModel.setData("marketNm", resultItem.getSvcNm() + "(" + resultItem.getStorNm() + ")");
                }

                if (resultCItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 이름, 아이디) */
                    for (int i = 0; i < resultCItem.size(); i++) {
                        resultCItem.get(i).setCretrNm(CommonFnc.getNameMask(resultCItem.get(i).getCretrNm()));
                        resultCItem.get(i).setCretrId(CommonFnc.getIdMask(resultCItem.get(i).getCretrId()));
                    }

                    /** 가져온 리스트 총 개수 */
                    int totalCount = storeService.billingDetailListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultCItem);

                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("billingDetailList");
                }
            }

        }

        return rsModel.getModelAndView();
    }

    /**
     * 블록 체인 통계 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/cpContStat")
    public ModelAndView cpContentStat(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/store/storeProcess");

        String Url_Block = configProperty.getProperty("chain.url");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isAdministrator(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        StoreVO item = new StoreVO();
        ContentVO Citem = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            String searchField = CommonFnc.emptyCheckString("searchField", request);
            String searchCp = "";
            if (cpSeq != 0) {
                searchCp = "&cpSeq=" + cpSeq;
            }

            // 검색 여부가 true일 경우
            ContentVO searchConts = new ContentVO();
            CpVO searchCps = new CpVO();

            item.setSearchConfirm(searchConfirm);
            searchConts.setSearchConfirm(searchConfirm);
            searchCps.setSearchConfirm(searchConfirm);

            String searchContsSeq = "";
            String searchCpSeq = "";

            if (searchConfirm.equals("true")) {
                searchConts.setSearchConfirm(searchConfirm);
                if (searchField.equals("CONTS_TITLE")) {
                    String searchString = CommonFnc.emptyCheckString("searchString", request);
                    searchConts.setkeyword(searchString);

                    List<ContentVO> cntInfoList = contentService.contentCdtNotInfoList(searchConts);
                    if (!cntInfoList.isEmpty()) {
                        for (int z = 0; z < cntInfoList.size(); z++) {
                            if (searchContsSeq != "") {
                                searchContsSeq += ",";
                            }
                            searchContsSeq += cntInfoList.get(z).getContsSeq();
                        }
                    }
                } else if (searchField.equals("CP_NM")) {
                    String searchString = CommonFnc.emptyCheckString("searchString", request);
                    searchCps.setKeyword(searchString);

                    List<CpVO> cpInfoList = cpService.cpDetailList(searchCps);
                    if (!cpInfoList.isEmpty()) {
                        for (int z = 0; z < cpInfoList.size(); z++) {
                            if (searchCpSeq != "") {
                                searchCpSeq += ",";
                            }
                            searchCpSeq += cpInfoList.get(z).getCpSeq();
                        }
                    }
                }
            }

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
            String sidx = CommonFnc.emptyCheckString("sidx", request);
            String type = CommonFnc.emptyCheckString("type", request);
            String year = CommonFnc.emptyCheckString("year", request);
            String month = CommonFnc.emptyCheckString("month", request);

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;
            item.setSidx(sidx);
            item.setOffset(1);
            item.setLimit(10000);
            JSONParser jsonParser = new JSONParser();
            // search

            String getUrl = Url_Block + "/api/v1/statistics/cp?date=" + year + "-" + month + "&type=" + type;

            if (searchConfirm.equals("true")) {
                if (searchField.equals("CONTS_TITLE")) {
                    getUrl += "&contSeq=" + searchContsSeq;
                    if (searchCp.equals("")) {
                        searchCps = new CpVO();
                        searchCps.setSidx("");
                        searchCps.setSord("");
                        searchCps.setOffset(1);
                        searchCps.setLimit(10000);

                        List<CpVO> cpAllList = cpService.cpList(searchCps);

                        for (int j = 0; j < cpAllList.size(); j++) {
                            if (!searchCp.equals("")) {
                                searchCp += ",";
                            } else {
                                searchCp = "&cpSeq=";
                            }
                            searchCp += cpAllList.get(j).getCpSeq();
                        }

                    }
                } else if (searchField.equals("CP_NM")) {
                    searchCp = "&cpSeq=" + searchCpSeq;
                }
            }
            getUrl += searchCp;

            String jsonString = getJson(getUrl, "127.0.0.1", "", "GET");

            if (jsonString == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }
            JSONObject jsonObj = (JSONObject) jsonParser.parse(jsonString);
            JSONArray ja = (JSONArray) jsonObj.get("data");

            if (ja == null || ja.size() == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                // 가공
                List<StoreVO> resultItem = new ArrayList<StoreVO>();
                for (int i = page - 1; i < pageEnd; i++) {
                    StoreVO citem = new StoreVO();
                    if (i >= ja.size() || page < 0) {
                        break;
                    }

                    JSONObject jsonObjchild = (JSONObject) ja.get(i);
                    JSONObject jsonVal = (JSONObject) jsonObjchild.get("value");
                    // 공통

                    citem.setCpSeq(Integer.parseInt((String) jsonVal.get("cpSeq")));
                    citem.setContsSeq(Integer.parseInt((String) jsonVal.get("contsSeq")));

                    ContentVO Contitem = new ContentVO();
                    Contitem.setContsSeq(citem.getContsSeq());
                    Contitem.setCpSeq(0);
                    Contitem.setContsCtgSeq(0);
                    Contitem.setSttusVal("");
                    Contitem.setGenreSeq(0);
                    Contitem.setList(null);
                    Contitem.setSvcSeq(0);

                    /** 해당 콘텐츠 상세정보 */
                    ContentVO cntInfo = contentService.contentCdtNotInfo(Contitem);

                    CpVO cpItem = new CpVO();
                    cpItem.setCpSeq(Integer.parseInt((String) jsonVal.get("cpSeq")));
                    cpItem.setBuyinContNo("");

                    /** 해당 CP 상세정보 */
                    CpVO cpInfo = cpService.cpDetail(cpItem);

                    if (cntInfo != null) {
                        citem.setContsTitle(cntInfo.getContsTitle());
                    }

                    if (cpInfo != null) {
                        citem.setCpNm(cpInfo.getCpNm());
                    }

                    String contractType = (String) jsonVal.get("contractType");

                    citem.setContractType(contractType);

                    if (contractType.equals("N")) {
                        citem.setContract("");
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setPricePerSec(0);
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                        citem.setPrice(0);
                    } else if (contractType.equals("L")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setPricePerSec(0);
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                        citem.setPrice(0);

                    } else if (contractType.equals("T")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPricePerSec(((Long) jsonVal.get("pricePerSec")).intValue());
                        citem.setPrice(((Long) jsonVal.get("price")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                    } else if (contractType.equals("C")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPricePerSec(((Long) jsonVal.get("pricePerSec")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setTotalPlayCnts(((Long) jsonVal.get("totalPlayCnts")).intValue());
                        citem.setRemainPlayCnts(((Long) jsonVal.get("remainPlayCnts")).intValue());
                        citem.setPrice(0);
                    }

                    resultItem.add(citem);
                }

                /* 가공 */

                int totalCount = ja.size();
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("cpContStat");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 검수 이력 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/svcContStat")
    public ModelAndView svcContentStat(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/store/storeProcess");

        String Url_Block = configProperty.getProperty("chain.url");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isAdministrator(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        StoreVO item = new StoreVO();
        ContentVO Citem = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            ContentVO searchConts = new ContentVO();
            ServicsVO searchSvcs = new ServicsVO();
            StoreVO searchStors = new StoreVO();

            // SELECT BOX 선택 확인
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

            /* selectBox 값에 따른 검색 조건 세팅 S */
            String searchSvc = "";
            String searchStor = "";
            if (svcSeq != 0) {
                searchSvc = "&svcSeq=" + svcSeq;
                searchStors.setSvcSeq(svcSeq);
                searchStors.setUseYn("ALL");
                List<StoreVO> storAllList = new ArrayList<StoreVO>();
                storAllList = storeService.storeNameListForManager(searchStors);

                for (int j = 0; j < storAllList.size(); j++) {
                    if (!searchStor.equals("")) {
                        searchStor += ",";
                    } else {
                        searchStor = "&storSeq=";
                    }
                    searchStor += storAllList.get(j).getStorSeq();
                }
            }

            if (storSeq != 0) {
                searchStor = "&storSeq=" + storSeq;
            }
            /* selectBox 값에 따른 검색 조건 세팅 E */

            // 검색 상태 확인 S
            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            String searchField = CommonFnc.emptyCheckString("searchField", request);

            item.setSearchConfirm(searchConfirm);
            searchConts.setSearchConfirm(searchConfirm);
            searchSvcs.setSearchConfirm(searchConfirm);
            searchStors.setSearchConfirm(searchConfirm);
            // 검색 상태 확인 E

            String searchContsSeq = "";
            String searchSvcSeq = "";
            int searchStorSeq = 0;

            if (searchConfirm.equals("true")) {
                searchConts.setSearchConfirm(searchConfirm);
                if (searchField.equals("CONTS_TITLE")) {
                    String searchString = CommonFnc.emptyCheckString("searchString", request);
                    searchConts.setkeyword(searchString);

                    List<ContentVO> cntInfoList = contentService.contentCdtNotInfoList(searchConts);

                    if (!cntInfoList.isEmpty()) {
                        for (int z = 0; z < cntInfoList.size(); z++) {
                            if (searchContsSeq != "") {
                                searchContsSeq += ",";
                            }
                            searchContsSeq += cntInfoList.get(z).getContsSeq();
                        }
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        return rsModel.getModelAndView();
                    }
                    // svcChk
                    if (svcSeq == 0) {
                        searchSvcs.setSearchConfirm("false");
                        searchSvcs.setoffset(1);
                        searchSvcs.setlimit(10000);
                        List<ServicsVO> svcAllList = servicsService.serviceList(searchSvcs);
                        searchSvc = "";
                        for (int j = 0; j < svcAllList.size(); j++) {
                            if (!searchSvc.equals("")) {
                                searchSvc += ",";
                            } else {
                                searchSvc = "&svcSeq=";
                            }
                            searchSvc += svcAllList.get(j).getSvcSeq();
                        }

                    }
                    searchStors.setSvcSeq(svcSeq);

                    // storChk
                    if (storSeq == 0) {
                        List<StoreVO> storAllList = storeService.storeNameList(searchStors);
                        searchStor = "";
                        for (int j = 0; j < storAllList.size(); j++) {
                            if (!searchStor.equals("")) {
                                searchStor += ",";
                            } else {
                                searchStor = "&storSeq=";
                            }
                            searchStor += storAllList.get(j).getStorSeq();
                        }
                    }
                } else if (searchField.equals("SVC_NM")) {
                    String searchString = CommonFnc.emptyCheckString("searchString", request);
                    searchSvcs.setKeyword(searchString);

                    List<ServicsVO> svcInfoList = servicsService.serviceInfoList(searchSvcs);

                    if (svcInfoList.isEmpty()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        return rsModel.getModelAndView();
                    } else {
                        searchStor = "";
                        for (int z = 0; z < svcInfoList.size(); z++) {
                            if (searchSvcSeq != "") {
                                searchSvcSeq += ",";
                            }
                            searchSvcSeq += svcInfoList.get(z).getSvcSeq();
                            searchStors.setSvcSeq(svcInfoList.get(z).getSvcSeq());

                            List<StoreVO> storAllList = storeService.storeNameList(searchStors);

                            for (int j = 0; j < storAllList.size(); j++) {
                                if (!searchStor.equals("")) {
                                    searchStor += ",";
                                } else if (searchStor == "") {
                                    searchStor = "&storSeq=";
                                }
                                searchStor += storAllList.get(j).getStorSeq();
                            }
                        }
                        searchSvc = "&svcSeq=" + searchSvcSeq;

                    }

                }
            }

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
            String sidx = CommonFnc.emptyCheckString("sidx", request);
            String type = CommonFnc.emptyCheckString("type", request);
            String year = CommonFnc.emptyCheckString("year", request);
            String month = CommonFnc.emptyCheckString("month", request);

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;
            item.setSidx(sidx);
            item.setOffset(1);
            item.setLimit(10000);
            JSONParser jsonParser = new JSONParser();
            // search
            String getUrl = Url_Block + "/api/v1/statistics/service?date=" + year + "-" + month + "&type=" + type;

            if (searchConfirm.equals("true")) {
                if (searchField.equals("CONTS_TITLE")) {
                    getUrl += "&contSeq=" + searchContsSeq;
                } else if (searchField.equals("SVC_NM")) {
                    searchSvc = "&svcSeq=" + searchSvcSeq;
                }
            }
            getUrl += searchSvc;
            getUrl += searchStor;

            String jsonString = getJson(getUrl, "127.0.0.1", "", "GET");

            if (jsonString == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }
            JSONObject jsonObj = (JSONObject) jsonParser.parse(jsonString);
            JSONArray ja = (JSONArray) jsonObj.get("data");

            if (ja == null || ja.size() == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                // 가공
                List<StoreVO> resultItem = new ArrayList<StoreVO>();
                for (int i = page - 1; i < pageEnd; i++) {
                    StoreVO citem = new StoreVO();
                    if (i >= ja.size() || page < 0) {
                        break;
                    }

                    JSONObject jsonObjchild = (JSONObject) ja.get(i);
                    JSONObject jsonVal = (JSONObject) jsonObjchild.get("value");
                    // 공통

                    citem.setStorSeq(Integer.parseInt((String) jsonVal.get("storSeq")));
                    citem.setSvcSeq(Integer.parseInt((String) jsonVal.get("svcSeq")));
                    citem.setContsSeq(Integer.parseInt((String) jsonVal.get("contsSeq")));

                    ContentVO Contitem = new ContentVO();
                    Contitem.setContsSeq(citem.getContsSeq());
                    Contitem.setCpSeq(0);
                    Contitem.setContsCtgSeq(0);
                    Contitem.setSttusVal("");
                    Contitem.setGenreSeq(0);
                    Contitem.setList(null);
                    Contitem.setSvcSeq(0);

                    /** 해당 콘텐츠 상세정보 */
                    ContentVO cntInfo = contentService.contentCdtNotInfo(Contitem);

                    /** 해당 CP 상세정보 */
                    StoreVO storInfo = storeService.storeDetail(citem);

                    if (cntInfo != null) {
                        citem.setContsTitle(cntInfo.getContsTitle());
                    }

                    if (storInfo != null) {
                        citem.setStorNm(storInfo.getStorNm());
                        citem.setSvcNm(storInfo.getSvcNm());
                    }

                    String contractType = (String) jsonVal.get("contractType");

                    citem.setContractType(contractType);

                    if (contractType.equals("N")) {
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setContract("");
                        citem.setPricePerSec(0);
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                        citem.setPrice(0);
                    } else if (contractType.equals("L")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setPricePerSec(0);
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                        citem.setPrice(0);
                    } else if (contractType.equals("T")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPricePerSec(((Long) jsonVal.get("pricePerSec")).intValue());
                        citem.setPrice(((Long) jsonVal.get("price")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                    } else if (contractType.equals("C")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setTotalPlayCnts(((Long) jsonVal.get("totalPlayCnts")).intValue());
                        citem.setRemainPlayCnts(((Long) jsonVal.get("remainPlayCnts")).intValue());
                        citem.setPrice(0);
                        citem.setPricePerSec(0);
                    }

                    resultItem.add(citem);
                }

                /* 가공 */

                int totalCount = ja.size();
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("svcContStat");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 검수 이력 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/billingHistory")
    public ModelAndView contractHistoryProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/store/storeProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isAdministrator(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        StoreVO item = new StoreVO();
        ContentVO Citem = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            /** CP 번호(검색조건) */
            int ratSeq = CommonFnc.emptyCheckInt("ratSeq", request);
            item.setRatSeq(ratSeq);

            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            item.setSearchConfirm(searchConfirm);
            // 검색 여부가 true일 경우
            if (searchConfirm.equals("true")) {
                String searchField = CommonFnc.emptyCheckString("searchField", request);
                String searchString = CommonFnc.emptyCheckString("searchString", request);

                item.setSearchTarget(searchField);
                item.setSearchKeyword(searchString);
            }
            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
            String sidx = CommonFnc.emptyCheckString("sidx", request);
            String sord = CommonFnc.emptyCheckString("sord", request);
            String useYn = CommonFnc.emptyCheckString("useYn", request);

            if (sidx.equals("RAT_CONT_HST_SEQ")) {
            } else if (sidx.equals("CONTS_COUNT")) {
            } else if (sidx.equals("TOT_PRICE")) {
            } else if (sidx.equals("RAT_AMD_DT")) {
            } else if (sidx.equals("REG_DT")) {
            } else if (sidx.equals("AMD_DT")) {
            } else if (sidx.equals("AMDR_ID")) {
            } else if (sidx.equals("CONTS_COUNT")) {
            } else {
                sidx = "RAT_CONT_HST_SEQ";
            }

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            item.setSidx(sidx);
            item.setSord(sord);
            item.setOffset(page);
            item.setLimit(pageEnd);

            List<StoreVO> resultItem = storeService.billingHistoryList(item);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (수정자 아이디) */
                for (int i = 0; i < resultItem.size(); i++) {
                    resultItem.get(i).setAmdrId(CommonFnc.getIdMask(resultItem.get(i).getAmdrId()));
                }

                int totalCount = storeService.billingHistoryListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("billingHistoryList");
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe) || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
    }

    private boolean isManageAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe);
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    /* 외부 URL 정보 POST 함수_ 현재 이용: [블록체인 데이터 전송] */
    public static String postJson(String serverUrl, String host, String jsonobject, String Method) {

        StringBuilder sb = new StringBuilder();

        String http = serverUrl;

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(http);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(Method);
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(100000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Host", host);
            urlConnection.connect();
            // You Can also Create JSONObject here
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonobject);// here i sent the parameter
            out.close();
            int HttpResult = urlConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                // Log.e("new Test", "" + sb.toString());
                return sb.toString();
            } else {
                System.out.println(urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }

    /* 외부 URL 정보 GET 함수_ 현재 이용: [블록체인 데이터 전송] */
    public static String getJson(String serverUrl, String host, String jsonobject, String Method) throws Exception {

        StringBuilder sb = new StringBuilder();

        String http = serverUrl;

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(http);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(Method);
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(100000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Host", host);
            urlConnection.connect();
            // You Can also Create JSONObject here
            int HttpResult = urlConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                StringBuilder result = new StringBuilder();
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                return sb.toString();

            } else {
                System.out.println(urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {

        } catch (IOException e) {

        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }

}
