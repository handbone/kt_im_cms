/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.store.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.store.vo.StoreVO;

/**
 *
 * 매장 관리와 연동되는 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 18.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("StoreRelationDAO")
@Transactional
public class StoreRelationDAOImpl extends mysqlAbstractMapper implements StoreRelationDAO {

    /**
     * 매장 정보 중 점포코드 수정 시 결제 정보 내 점포코드도 수정
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public void updateSettlementInfo(StoreVO vo) {
        update("StoreRelationDAO.updateSettlementInfo", vo);
    }

    /**
     * 운영 중인 매장 클라이언트 종류 별 수
     *
     * @param StoreVO
     * @return 운영 중인 매장 클라이언트 종류 리스트
     */
    @Override
    public List<StoreVO> storeUseDevCount(StoreVO vo) {
        return selectList("StoreRelationDAO.storeUseDevCount", vo);
    }

    /**
     * 매장별 배포 정보 목록 리스트 조회
     *
     * @param StoreVO
     * @return 매장별 배포 정보 목록 리스트
     */
    @Override
    public List<StoreVO> billingList(StoreVO vo) {
        return selectList("StoreRelationDAO.billingList", vo);
    }

    /**
     * 매장별 배포 정보 목록 리스트 개수 조회
     *
     * @param StoreVO
     * @return 매장별 배포 정보 목록 리스트 개수 조회
     */
    @Override
    public int billingListTotalCount(StoreVO vo) {
        int res = (Integer) selectOne("StoreRelationDAO.billingListTotalCount", vo);
        return res;
    }

    /**
     * CP사별 수급 정보 리스트
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> contractList(StoreVO vo) {
        return selectList("StoreRelationDAO.contractList", vo);
    }

    /**
     * CP사별 수급 정보 리스트 합계
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractListTotalCount(StoreVO vo) {
        return selectOne("StoreRelationDAO.contractListTotalCount", vo);
    }

    /**
     * 계약 콘텐츠 정보 등록 (CP)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractInsert(StoreVO vo) {
        return insert("StoreRelationDAO.contractInsert", vo);
    }

    /**
     * 계약 콘텐츠 정보 등록 (CP)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractContsInsert(StoreVO vo) {
        return insert("StoreRelationDAO.contractContsInsert", vo);
    }

    /**
     * 계약 상세 정보 조회
     *
     * @param StoreVO
     * @return
     */
    @Override
    public StoreVO contractInfo(StoreVO vo) {
        return selectOne("StoreRelationDAO.contractInfo", vo);
    }

    /**
     * 계약 콘텐츠 정보 삭제 (DEL_YN = Y )
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractContsDelete(StoreVO vo) {
        return update("StoreRelationDAO.contractContsDelete", vo);
    }

    /**
     * 계약 정보 수정
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractUpdate(StoreVO vo) {
        return update("StoreRelationDAO.contractUpdate", vo);
    }

    /**
     * 계약 콘텐츠 정보 등록 (중복 DEL_YN = N )
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractContsUpdate(StoreVO vo) {
        return update("StoreRelationDAO.contractContsUpdate", vo);
    }

    /**
     * 계약 수정 이력 정보 리스트
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> contractHistoryList(StoreVO vo) {
        return selectList("StoreRelationDAO.contractHistoryList", vo);
    }

    /**
     * 계약 수정 이력 정보 리스트 합계
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractHistoryListTotalCount(StoreVO vo) {
        return selectOne("StoreRelationDAO.contractHistoryListTotalCount", vo);
    }

    /**
     * 매장별 계약 정보 리스트
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> billingDetailList(StoreVO vo) {
        return selectList("StoreRelationDAO.billingDetailList", vo);
    }

    /**
     * 매장별 계약 정보 리스트 합계
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingDetailListTotalCount(StoreVO vo) {
        return selectOne("StoreRelationDAO.billingDetailListTotalCount", vo);
    }

    /**
     * 매장 콘텐츠 계약 상세 정보
     *
     * @param StoreVO
     * @return
     */
    @Override
    public StoreVO billingInfo(StoreVO vo) {
        return selectOne("StoreRelationDAO.billingInfo", vo);
    }

    /**
     * 매장 계약 콘텐츠 정보 삭제 (DEL_YN = Y )
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int ratContsDelete(StoreVO vo) {
        return update("StoreRelationDAO.ratContsDelete", vo);
    }

    /**
     * 매장 콘텐츠 정보 등록 (중복 DEL_YN = N )
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int ratContsUpdate(StoreVO vo) {
        return update("StoreRelationDAO.ratContsUpdate", vo);
    }

    @Override
    public int billingUpdate(StoreVO vo) {
        return update("StoreRelationDAO.billingUpdate", vo);
    }

    /**
     * 매장 계약 수정 이력 정보 리스트
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> billingHistoryList(StoreVO vo) {
        return selectList("StoreRelationDAO.billingHistoryList", vo);
    }

    /**
     * 매장 계약 수정 이력 정보 리스트 합계
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingHistoryListTotalCount(StoreVO vo) {
        return selectOne("StoreRelationDAO.billingHistoryListTotalCount", vo);
    }

    /**
     * 계약 콘텐츠 정보 등록 (STORE)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingInsert(StoreVO vo) {
        return insert("StoreRelationDAO.billingInsert", vo);
    }

    /**
     * 계약 콘텐츠 정보 등록 (STORE)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingContsInsert(StoreVO vo) {
        return insert("StoreRelationDAO.billingContsInsert", vo);
    }

}
