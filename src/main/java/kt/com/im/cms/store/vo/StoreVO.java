/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.store.vo;

import java.io.Serializable;

/**
 *
 * 매장 정보에 대한 데이터 객체
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 15.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
public class StoreVO implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1606439269723812625L;

    /** 매장 번호 */
    private int storSeq;

    /** 서비스 번호 */
    private int svcSeq;

    /** 콘텐츠 제공사 번호 */
    private int cpSeq;

    /** 콘텐츠 제공사 이름 */
    private String cpNm;

    /** 콘텐츠 번호 */
    private int contsSeq;

    /** 콘텐츠 번호 (복수) */
    private String contsSeqs;

    /** 매장 계약 시퀀스 번호 */
    private int ratSeq;

    /** 매장 계약 유형 */
    private String ratContType;

    /** 매장 계약 번호 */
    private String ratContNo;

    /** 매장 계약 기간 (시작) */
    private String ratContStDt;

    /** 매장 계약 기간 (종료) */
    private String ratContFnsDt;

    /** 수급 계약 시퀀스 번호 */
    private int buyinSeq;

    /** 매장 계약 콘텐츠 시퀀스 번호 */
    private int ratContsSeq;

    /** 계약 콘텐츠 시퀀스 번호 */
    private int buyinContsSeq;

    /** 계약 수정 이력 번호 */
    private int buyinContHstSeq;

    /** 매장 계약 수정 이력 번호 */
    private int ratContHstSeq;

    /** 과금 여부 */
    private String ratYn;

    /** 수급 계약 번호 */
    private String buyinContNo;

    /** 수급 계약 기간 (시작) */
    private String buyinContStDt;

    /** 수급 계약 기간 (종료) */
    private String buyinContFnsDt;

    /** 수급 계약 종류 */
    private String buyinContType;

    /** 수급 실행 횟수 제한 횟수 */
    private int runLmtCnt;

    /** 수급 실행 횟수 제한 금액 */
    private int runPerPrc;

    /** 수급 시간당 과금 시간 */
    private int ratTime;

    /** 수급 시간당 금액 */
    private int ratPrc;

    /** 계약 번호 중복 개수 */
    private int ratContCnt;

    /** 수급 일시금 지급형 금액 */
    private int lmsmpyPrc;

    /** 이용 횟수 */
    private int useCount;

    /** 콘텐츠 개수 */
    private int contsCount;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 타입 별 값 */
    private int setVal;

    /** 수량 */
    private int count;

    /** 금액 */
    private int totPrice;

    /** 실사용금액 */
    private int usePrice;

    /** 서비스명 */
    private String svcNm;

    /** 예약 시간 간격 */
    private int rsrvTimeSetCycle;

    /** 예약 시작 시간 */
    private String stTime;

    /** 예약 종료 시간 */
    private String fnsTime;

    /** 매장 명 */
    private String storNm;

    /** 매장 전화번호 */
    private String storTelNo;

    /** 우편번호 */
    private String zipcd;

    /** 매장 전체 주소 */
    private String storAddr;

    /** 매장 기본 주소 */
    private String storBasAddr;

    /** 매장 상세 주소 */
    private String storDtlAddr;

    /** 점포 코드 */
    private String storCode;

    /** 사용 여부 */
    private String useYn;

    /** 장비 명 */
    private String devTypeNm;

    /** 등록일(수정일시 또는 생성일시) */
    private String regDt;

    /** 생성자 아이디 */
    private String cretrId;

    /** 등록자 */
    private String cretrNm;

    /** 생성 일시 */
    private String cretDt;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** 로그인 아이디 */
    private String loginId;

    /** 로그인 회원 등급 */
    private String mbrSe;

    /** 정렬 컬럼명 */
    private String sidx;

    /** 정렬 방법 (DESC, ASC) */
    private String sord;

    /** 검색 영역 */
    private String searchTarget;

    /** 검색 영역 */
    private String target;

    /** 검색어 */
    private String searchKeyword;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** BLOCK CHAIN 통계 S */

    /** 과금 유형 */
    private String contractType;

    /** 과금 내용 */
    private String contract;

    /** 플레이 시간 */
    private int playTime;

    /** 초당 가격 */
    private int pricePerSec;

    /** 가격 */
    private int price;

    /** 플레이 횟수 */
    private int playCnts;

    /** 플레이 누적 횟수 */
    private int totalPlayCnts;

    /** 남은 횟수 */
    private int remainPlayCnts;

    /** BLOCK CHAIN 통계 E */

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public String getStorTelNo() {
        return storTelNo;
    }

    public void setStorTelNo(String storTelNo) {
        this.storTelNo = storTelNo;
    }

    public String getStorAddr() {
        return storAddr;
    }

    public void setStorAddr(String storAddr) {
        this.storAddr = storAddr;
    }

    public String getZipcd() {
        return zipcd;
    }

    public void setZipcd(String zipcd) {
        this.zipcd = zipcd;
    }

    public String getStorBasAddr() {
        return storBasAddr;
    }

    public void setStorBasAddr(String storBasAddr) {
        this.storBasAddr = storBasAddr;
    }

    public String getStorDtlAddr() {
        return storDtlAddr;
    }

    public void setStorDtlAddr(String storDtlAddr) {
        this.storDtlAddr = storDtlAddr;
    }

    public String getStorCode() {
        return storCode;
    }

    public void setStorCode(String storCode) {
        this.storCode = storCode;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getMbrSe() {
        return mbrSe;
    }

    public void setMbrSe(String mbrSe) {
        this.mbrSe = mbrSe;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getRsrvTimeSetCycle() {
        return rsrvTimeSetCycle;
    }

    public void setRsrvTimeSetCycle(int rsrvTimeSetCycle) {
        this.rsrvTimeSetCycle = rsrvTimeSetCycle;
    }

    public String getStTime() {
        return stTime;
    }

    public void setStTime(String stTime) {
        this.stTime = stTime;
    }

    public String getFnsTime() {
        return fnsTime;
    }

    public void setFnsTime(String fnsTime) {
        this.fnsTime = fnsTime;
    }

    public String getDevTypeNm() {
        return devTypeNm;
    }

    public void setDevTypeNm(String devTypeNm) {
        this.devTypeNm = devTypeNm;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotPrice() {
        return totPrice;
    }

    public void setTotPrice(int totPrice) {
        this.totPrice = totPrice;
    }

    public int getCpSeq() {
        return cpSeq;
    }

    public void setCpSeq(int cpSeq) {
        this.cpSeq = cpSeq;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getBuyinSeq() {
        return buyinSeq;
    }

    public void setBuyinSeq(int buyinSeq) {
        this.buyinSeq = buyinSeq;
    }

    public String getRatYn() {
        return ratYn;
    }

    public void setRatYn(String ratYn) {
        this.ratYn = ratYn;
    }

    public String getBuyinContNo() {
        return buyinContNo;
    }

    public void setBuyinContNo(String buyinContNo) {
        this.buyinContNo = buyinContNo;
    }

    public String getBuyinContStDt() {
        return buyinContStDt;
    }

    public void setBuyinContStDt(String buyinContStDt) {
        this.buyinContStDt = buyinContStDt;
    }

    public String getBuyinContFnsDt() {
        return buyinContFnsDt;
    }

    public void setBuyinContFnsDt(String buyinContFnsDt) {
        this.buyinContFnsDt = buyinContFnsDt;
    }

    public String getBuyinContType() {
        return buyinContType;
    }

    public void setBuyinContType(String buyinContType) {
        this.buyinContType = buyinContType;
    }

    public int getRunLmtCnt() {
        return runLmtCnt;
    }

    public void setRunLmtCnt(int runLmtCnt) {
        this.runLmtCnt = runLmtCnt;
    }

    public int getRunPerPrc() {
        return runPerPrc;
    }

    public void setRunPerPrc(int runPerPrc) {
        this.runPerPrc = runPerPrc;
    }

    public int getRatTime() {
        return ratTime;
    }

    public void setRatTime(int ratTime) {
        this.ratTime = ratTime;
    }

    public int getRatPrc() {
        return ratPrc;
    }

    public void setRatPrc(int ratPrc) {
        this.ratPrc = ratPrc;
    }

    public int getLmsmpyPrc() {
        return lmsmpyPrc;
    }

    public void setLmsmpyPrc(int lmsmpyPrc) {
        this.lmsmpyPrc = lmsmpyPrc;
    }

    public int getContsCount() {
        return contsCount;
    }

    public void setContsCount(int contsCount) {
        this.contsCount = contsCount;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public int getSetVal() {
        return setVal;
    }

    public void setSetVal(int setVal) {
        this.setVal = setVal;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public int getUsePrice() {
        return usePrice;
    }

    public void setUsePrice(int usePrice) {
        this.usePrice = usePrice;
    }

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public int getBuyinContsSeq() {
        return buyinContsSeq;
    }

    public void setBuyinContsSeq(int buyinContsSeq) {
        this.buyinContsSeq = buyinContsSeq;
    }

    public String getContsSeqs() {
        return contsSeqs;
    }

    public void setContsSeqs(String contsSeqs) {
        this.contsSeqs = contsSeqs;
    }

    public int getBuyinContHstSeq() {
        return buyinContHstSeq;
    }

    public void setBuyinContHstSeq(int buyinContHstSeq) {
        this.buyinContHstSeq = buyinContHstSeq;
    }

    public int getRatSeq() {
        return ratSeq;
    }

    public void setRatSeq(int ratSeq) {
        this.ratSeq = ratSeq;
    }

    public String getRatContNo() {
        return ratContNo;
    }

    public void setRatContNo(String ratContNo) {
        this.ratContNo = ratContNo;
    }

    public String getRatContStDt() {
        return ratContStDt;
    }

    public void setRatContStDt(String ratContStDt) {
        this.ratContStDt = ratContStDt;
    }

    public String getRatContFnsDt() {
        return ratContFnsDt;
    }

    public void setRatContFnsDt(String ratContFnsDt) {
        this.ratContFnsDt = ratContFnsDt;
    }

    public String getRatContType() {
        return ratContType;
    }

    public void setRatContType(String ratContType) {
        this.ratContType = ratContType;
    }

    public int getRatContsSeq() {
        return ratContsSeq;
    }

    public void setRatContsSeq(int ratContsSeq) {
        this.ratContsSeq = ratContsSeq;
    }

    public int getRatContHstSeq() {
        return ratContHstSeq;
    }

    public void setRatContHstSeq(int ratContHstSeq) {
        this.ratContHstSeq = ratContHstSeq;
    }

    public int getRatContCnt() {
        return ratContCnt;
    }

    public void setRatContCnt(int ratContCnt) {
        this.ratContCnt = ratContCnt;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public int getPlayTime() {
        return playTime;
    }

    public void setPlayTime(int playTime) {
        this.playTime = playTime;
    }

    public int getPricePerSec() {
        return pricePerSec;
    }

    public void setPricePerSec(int pricePerSec) {
        this.pricePerSec = pricePerSec;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPlayCnts() {
        return playCnts;
    }

    public void setPlayCnts(int playCnts) {
        this.playCnts = playCnts;
    }

    public int getTotalPlayCnts() {
        return totalPlayCnts;
    }

    public void setTotalPlayCnts(int totalPlayCnts) {
        this.totalPlayCnts = totalPlayCnts;
    }

    public int getRemainPlayCnts() {
        return remainPlayCnts;
    }

    public void setRemainPlayCnts(int remainPlayCnts) {
        this.remainPlayCnts = remainPlayCnts;
    }

    public String getCpNm() {
        return cpNm;
    }

    public void setCpNm(String cpNm) {
        this.cpNm = cpNm;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

}
