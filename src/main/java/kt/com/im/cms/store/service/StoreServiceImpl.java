/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.store.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.store.dao.StoreManageDAO;
import kt.com.im.cms.store.dao.StoreRelationDAO;
import kt.com.im.cms.store.dao.StoreStatDAO;
import kt.com.im.cms.store.vo.StoreVO;

/**
 *
 * 매장 정보 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("StoreService")
public class StoreServiceImpl implements StoreService {

    // 매장 관리 부분 정의
    @Resource(name = "StoreManageDAO")
    private StoreManageDAO storeManageDAO;

    // 매장 관리와 연계되는 부분 정의
    @Resource(name = "StoreRelationDAO")
    private StoreRelationDAO storeRelationDAO;

    // 수정 이력 관련 부분 정의
    @Resource(name = "StoreStatDAO")
    private StoreStatDAO storeStatDAO;

    /**
     * 검색조건에 해당되는 매장 목록 정보
     *
     * @param StoreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StoreVO> storeList(StoreVO vo) {
        return storeManageDAO.storeList(vo);
    }

    /**
     * 매장 목록 합계
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 매장 목록 합계
     */
    @Override
    public int storeListTotalCount(StoreVO vo) {
        return storeManageDAO.storeListTotalCount(vo);
    }

    /**
     * Master 관리자, CMS 관리자, 서비스 관리자 조건에 해당되는 매장명 목록 정보 (svcSeq 종속)
     *
     * @param StoreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StoreVO> storeNameListForManager(StoreVO vo) {
        return storeManageDAO.storeNameListForManager(vo);
    }

    /**
     * 매장 관리자 조건에 해당되는 매장명 목록 정보
     *
     * @param StoreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StoreVO> storeNameListForStoreManager(StoreVO vo) {
        return storeManageDAO.storeNameListForStoreManager(vo);
    }

    /**
     * 매장 상세정보
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 매장 상세정보
     */
    @Override
    public StoreVO storeDetail(StoreVO vo) {
        return storeManageDAO.storeDetail(vo);
    }

    /**
     * 점포코드 존재 유무 확인
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 매장 수
     */
    @Override
    public int searchStoreInfo(StoreVO vo) {
        return storeManageDAO.searchStoreInfo(vo);
    }

    /**
     * 매장 등록
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public int insertStore(StoreVO vo) {
        return storeManageDAO.insertStore(vo);
    }

    /**
     * 매장 정보 수정
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public int updateStore(StoreVO vo) {
        return storeManageDAO.updateStore(vo);
    }

    /**
     * 매장 정보 중 점포코드 수정 시 결제 정보 내 점포코드도 수정
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public void updateSettlementInfo(StoreVO vo) {
        storeRelationDAO.updateSettlementInfo(vo);
    }

    /**
     * 운영 중인 매장 클라이언트 종류 별 수
     *
     * @param StoreVO
     * @return 운영 중인 매장 클라이언트 종류 리스트
     */
    @Override
    public List<StoreVO> storeUseDevCount(StoreVO vo) {
        return storeRelationDAO.storeUseDevCount(vo);
    }

    /**
     * 매장별 배포 정보 목록 리스트 조회
     *
     * @param StoreVO
     * @return 매장별 배포 정보 목록 리스트
     */
    @Override
    public List<StoreVO> billingList(StoreVO vo) {
        return storeRelationDAO.billingList(vo);
    }

    /**
     * 매장별 배포 정보 목록 리스트 개수 조회
     *
     * @param StoreVO
     * @return 매장별 배포 정보 목록 리스트 개수 조회
     */
    @Override
    public int billingListTotalCount(StoreVO vo) {
        return storeRelationDAO.billingListTotalCount(vo);
    }

    /**
     * CP사별 수급 정보 리스트
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> contractList(StoreVO vo) {
        return storeRelationDAO.contractList(vo);
    }

    /**
     * CP사별 수급 정보 리스트 합계
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractListTotalCount(StoreVO vo) {
        return storeRelationDAO.contractListTotalCount(vo);
    }

    /**
     * 계약 정보 등록 (CP)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractInsert(StoreVO vo) {
        return storeRelationDAO.contractInsert(vo);
    }

    /**
     * 계약 콘텐츠 정보 등록 (CP)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractContsInsert(StoreVO vo) {
        return storeRelationDAO.contractContsInsert(vo);
    }

    /**
     * 계약 상세 정보 등록 (CP)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public StoreVO contractInfo(StoreVO vo) {
        return storeRelationDAO.contractInfo(vo);
    }

    /**
     * 계약 콘텐츠 정보 삭제 (DEL_YN = Y )
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractContsDelete(StoreVO vo) {
        return storeRelationDAO.contractContsDelete(vo);
    }

    /**
     * 계약 정보 수정
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractUpdate(StoreVO vo) {
        return storeRelationDAO.contractUpdate(vo);
    }

    /**
     * 계약 콘텐츠 정보 등록 (중복 DEL_YN = N )
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractContsUpdate(StoreVO vo) {
        return storeRelationDAO.contractContsUpdate(vo);
    }

    /**
     * 수정 이력 등록
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public int contractHstInsert(StoreVO vo) {
        return storeStatDAO.contractHstInsert(vo);
    }

    /**
     * 계약 수정 이력 정보 리스트
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> contractHistoryList(StoreVO vo) {
        return storeRelationDAO.contractHistoryList(vo);
    }

    /**
     * 계약 수정 이력 정보 리스트 합계
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int contractHistoryListTotalCount(StoreVO vo) {
        return storeRelationDAO.contractHistoryListTotalCount(vo);
    }

    /**
     * 매장별 계약 정보 리스트
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> billingDetailList(StoreVO vo) {
        return storeRelationDAO.billingDetailList(vo);
    }

    /**
     * 매장별 계약 정보 리스트 합계
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingDetailListTotalCount(StoreVO vo) {
        return storeRelationDAO.billingDetailListTotalCount(vo);
    }

    /**
     * 매장 콘텐츠 계약 상세 정보
     *
     * @param StoreVO
     * @return
     */
    @Override
    public StoreVO billingInfo(StoreVO vo) {
        return storeRelationDAO.billingInfo(vo);
    }

    /**
     * 매장 계약 콘텐츠 정보 삭제 (DEL_YN = Y )
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int ratContsDelete(StoreVO vo) {
        return storeRelationDAO.ratContsDelete(vo);
    }

    /**
     * 매장 계약 수정 이력 등록
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public int ratHstInsert(StoreVO vo) {
        return storeStatDAO.ratHstInsert(vo);
    }

    /**
     * 매장 콘텐츠 정보 등록 (중복 DEL_YN = N )
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int ratContsUpdate(StoreVO vo) {
        return storeRelationDAO.ratContsUpdate(vo);
    }

    /**
     * 매장 계약 정보 수정
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingUpdate(StoreVO vo) {
        return storeRelationDAO.billingUpdate(vo);
    }

    /**
     * 매장 계약 수정 이력 정보 리스트
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> billingHistoryList(StoreVO vo) {
        return storeRelationDAO.billingHistoryList(vo);
    }

    /**
     * 매장 계약 수정 이력 정보 리스트 합계
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingHistoryListTotalCount(StoreVO vo) {
        return storeRelationDAO.billingHistoryListTotalCount(vo);
    }

    /**
     * 계약 정보 등록 (STOR)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingInsert(StoreVO vo) {
        return storeRelationDAO.billingInsert(vo);
    }

    /**
     * 계약 콘텐츠 정보 등록 (STOR)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public int billingContsInsert(StoreVO vo) {
        return storeRelationDAO.billingContsInsert(vo);
    }

    /**
     * 서비스 조건 부에 따른 매장 목록 (전체 or 특정)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> storeNameList(StoreVO vo) {
        return storeManageDAO.storeNameList(vo);
    }

}
