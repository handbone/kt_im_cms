/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.store.service;

import java.util.List;

import kt.com.im.cms.store.vo.StoreVO;

/**
 *
 * 매장 정보 관련 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public interface StoreService {
    List<StoreVO> storeList(StoreVO vo);

    int storeListTotalCount(StoreVO vo);

    List<StoreVO> storeNameListForManager(StoreVO vo);

    List<StoreVO> storeNameListForStoreManager(StoreVO vo);

    StoreVO storeDetail(StoreVO vo);

    int searchStoreInfo(StoreVO vo);

    int insertStore(StoreVO vo);

    int updateStore(StoreVO vo);

    void updateSettlementInfo(StoreVO vo);

    List<StoreVO> storeUseDevCount(StoreVO item);

    List<StoreVO> billingList(StoreVO item);

    int billingListTotalCount(StoreVO item);

    List<StoreVO> contractList(StoreVO vo);

    int contractListTotalCount(StoreVO vo);

    int contractInsert(StoreVO sitem);

    int contractContsInsert(StoreVO sitem);

    StoreVO contractInfo(StoreVO sitem);

    int contractContsDelete(StoreVO sitem);

    int contractUpdate(StoreVO sitem);

    int contractContsUpdate(StoreVO sitem);

    int contractHstInsert(StoreVO sitem);

    List<StoreVO> contractHistoryList(StoreVO sitem);

    int contractHistoryListTotalCount(StoreVO sitem);

    List<StoreVO> billingDetailList(StoreVO item);

    int billingDetailListTotalCount(StoreVO item);

    StoreVO billingInfo(StoreVO item);

    int ratContsDelete(StoreVO item);

    int ratHstInsert(StoreVO item);

    int ratContsUpdate(StoreVO item);

    int billingUpdate(StoreVO item);

    List<StoreVO> billingHistoryList(StoreVO item);

    int billingHistoryListTotalCount(StoreVO item);

    int billingInsert(StoreVO item);

    int billingContsInsert(StoreVO item);

    List<StoreVO> storeNameList(StoreVO item);

}
