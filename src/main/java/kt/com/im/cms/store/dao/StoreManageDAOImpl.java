/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.store.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.MemberAbstractMapper;
import kt.com.im.cms.store.vo.StoreVO;

/**
 *
 * 매장 관리 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("StoreManageDAO")
@Transactional
public class StoreManageDAOImpl extends MemberAbstractMapper implements StoreManageDAO {

    /**
     * 매장 목록 정보
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 매장 목록 정보
     */
    @Override
    public List<StoreVO> storeList(StoreVO vo) {
        return selectList("StoreManageDAO.storeList", vo);
    }

    /**
     * 매장 목록 합계
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 매장 목록 합계
     */
    @Override
    public int storeListTotalCount(StoreVO vo) {
        return selectOne("StoreManageDAO.storeListTotalCount", vo);
    }

    /**
     * Master, CMS, 서비스 관리자 조건의 매장명 목록 정보
     *
     * @param StoreVO
     * @return 매장명 목록 정보
     */
    @Override
    public List<StoreVO> storeNameListForManager(StoreVO vo) {
        return selectList("StoreManageDAO.storeNameListForManager", vo);
    }

    /**
     * 매장 관리자 조건의 매장명 목록 정보
     *
     * @param StoreVO
     * @return 매장명 목록 정보
     */
    @Override
    public List<StoreVO> storeNameListForStoreManager(StoreVO vo) {
        return selectList("StoreManageDAO.storeNameListForStoreManager", vo);
    }

    /**
     * 매장 상세정보
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 매장 상세정보
     */
    @Override
    public StoreVO storeDetail(StoreVO vo) {
        return selectOne("StoreManageDAO.storeDetail", vo);
    }

    /**
     * 점포코드 존재 유무 확인
     *
     * @param StoreVO
     * @return 검색조건에 해당되는 매장 수
     */
    @Override
    public int searchStoreInfo(StoreVO vo) {
        return selectOne("StoreManageDAO.searchStoreInfo", vo);
    }

    /**
     * 매장 등록
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public int insertStore(StoreVO vo) {
        return insert("StoreManageDAO.insertStore", vo);
    }

    /**
     * 매장 정보 수정
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public int updateStore(StoreVO vo) {
        return update("StoreManageDAO.updateStore", vo);
    }

    /**
     * 서비스 조건 부에 따른 매장 목록 (전체 or 특정)
     *
     * @param StoreVO
     * @return
     */
    @Override
    public List<StoreVO> storeNameList(StoreVO vo) {
        return selectList("StoreManageDAO.storeNameList", vo);
    }

}
