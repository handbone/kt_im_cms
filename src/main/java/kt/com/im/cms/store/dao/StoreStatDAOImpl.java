/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.store.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.StateAbstractMapper;
import kt.com.im.cms.store.vo.StoreVO;

/**
 *
 * 매장 관리 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 15.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("StoreStatDAO")
@Transactional
public class StoreStatDAOImpl extends StateAbstractMapper implements StoreStatDAO {

    /**
     * 계약 정보 수정 이력 등록
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public int contractHstInsert(StoreVO vo) {
        return insert("DeviceStatDAO.contractHstInsert", vo);
    }

    /**
     * 매장 계약 정보 수정 이력 등록
     *
     * @param StoreVO
     * @return 결과 Int
     */
    @Override
    public int ratHstInsert(StoreVO vo) {
        return insert("DeviceStatDAO.ratHstInsert", vo);
    }

}
