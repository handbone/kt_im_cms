/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.store.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;

/**
 *
 * 매장 관리 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 15.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Component
@Controller
public class StoreController {

    /**
     * 매장 목록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/store", method = RequestMethod.GET)
    public ModelAndView storeList(ModelAndView mv) {
        mv.setViewName("/views/service/store/StoreList");
        return mv;
    }

    /**
     * 매장 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/store/{storSeq}", method = RequestMethod.GET)
    public ModelAndView storeDetail(ModelAndView mv, @PathVariable(value="storSeq") String storSeq) {
        mv.addObject("storSeq", storSeq);
        mv.setViewName("/views/service/store/StoreDetail");
        return mv;
    }

    /**
     * 매장 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/store/regist", method = RequestMethod.GET)
    public ModelAndView storeRegist(ModelAndView mv, HttpServletRequest request) {
        mv.addObject("svcSeq", CommonFnc.emptyCheckInt("svcSeq", request));
        mv.setViewName("/views/service/store/StoreRegist");
        return mv;
    }

    /**
     * 매장 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/store/{storSeq}/edit", method = RequestMethod.GET)
    public ModelAndView storeEdit(ModelAndView mv, @PathVariable(value="storSeq") String storSeq) {
        mv.addObject("storSeq", storSeq);
        mv.setViewName("/views/service/store/StoreEdit");
        return mv;
    }

}
