/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.faq.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * FAQ 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 3.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Component
@Controller
public class FaqController {

    /**
     * 고객센터 FAQ 리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/customer/faq", method = RequestMethod.GET)
    public ModelAndView customerFaqList(ModelAndView mv) {
        mv.setViewName("/views/customer/faq/FaqList");
        return mv;
    }

    /**
     * 고객센터 FAQ 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/customer/faq/{faqSeq}", method = RequestMethod.GET)
    public ModelAndView customerFaqDetail(ModelAndView mv, @PathVariable(value="faqSeq") String faqSeq) {
        mv.addObject("faqSeq", faqSeq);
        mv.setViewName("/views/customer/faq/FaqDetail");
        return mv;
    }

    /**
     * 고객센터 FAQ 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/customer/faq/regist", method = RequestMethod.GET)
    public ModelAndView customerFaqRegist(ModelAndView mv) {
        mv.setViewName("/views/customer/faq/FaqRegist");
        return mv;
    }

    /**
     * 고객센터 FAQ 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/customer/faq/{faqSeq}/edit", method = RequestMethod.GET)
    public ModelAndView customerFaqEdit(ModelAndView mv, @PathVariable(value="faqSeq") String faqSeq) {
        mv.addObject("faqSeq", faqSeq);
        mv.setViewName("/views/customer/faq/FaqEdit");
        return mv;
    }

}
