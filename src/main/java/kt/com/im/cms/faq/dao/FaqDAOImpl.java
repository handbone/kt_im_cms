/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.faq.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.faq.vo.FaqVO;
import kt.com.im.cms.common.dao.mysqlAbstractMapper;

/**
 *
 * FAQ 관련 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("FaqDAO")
public class FaqDAOImpl extends mysqlAbstractMapper implements FaqDAO {

    /**
     * FAQ 리스트 조회
     * @param FaqVO
     * @return 검색 조건에 부합하는 FAQ 리스트
     */
    @Override
    public List<FaqVO> faqList(FaqVO vo) {
        return selectList("FaqDAO.faqList", vo);
    }

    /**
     * FAQ 리스트 합계 조회
     * @param FaqVO
     * @return 검색 조건에 부합하는 FAQ 리스트 합계
     */
    public int faqListTotalCount(FaqVO vo){
         int res = (Integer) selectOne("FaqDAO.faqListTotalCount", vo);
         return res;
    }

    /**
     * FAQ 상세 정보 조회
     * @param FaqVO
     * @return 검색 조건에 부합하는 FAQ 상세 정보
     */
    @Override
    public FaqVO faqDetail(FaqVO vo) {
        return selectOne("FaqDAO.faqDetail", vo);
    }

    /**
     * FAQ 조회수 업데이트
     * @param FaqVO
     * @return
     */
    @Override
    public void updateFaqRetvNum(FaqVO vo) {
        update("FaqDAO.updateFaqRetvNum", vo);
    }

    /**
     * 회원 권한에 따른 서비스 FAQ 타입 목록 조회
     * @param FaqVO
     * @return FAQ 타입 목록
     */
    @Override
    public List<FaqVO> faqTypeList(FaqVO vo) {
        return selectList("FaqDAO.faqTypeList", vo);
    }

    /**
     * FAQ 등록
     * @param FaqVO
     * @return 등록 결과
     */
    @Override
    public int insertFaq(FaqVO vo) {
        return insert("FaqDAO.insertFaq", vo);
    }

    /**
     * FAQ 수정
     * @param FaqVO
     * @return 수정 결과
     */
    @Override
    public int updateFaq(FaqVO vo) {
        return update("FaqDAO.updateFaq", vo);
    }

    /**
     * FAQ 삭제
     * @param FaqVO
     * @return 삭제 결과
     */
    @Override
    public int deleteFaq(FaqVO vo) {
        return update("FaqDAO.deleteFaq", vo);
    }

}
