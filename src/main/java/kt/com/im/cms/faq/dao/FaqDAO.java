/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.faq.dao;

import java.util.List;

import kt.com.im.cms.faq.vo.FaqVO;

/**
 *
 * FAQ 관련 데이터 접근  인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public interface FaqDAO {

    public List<FaqVO> faqList(FaqVO vo);

    public int faqListTotalCount(FaqVO vo);

    public FaqVO faqDetail(FaqVO vo);

    public void updateFaqRetvNum(FaqVO vo);

    public List<FaqVO> faqTypeList(FaqVO vo);

    public int insertFaq(FaqVO vo);

    public int updateFaq(FaqVO vo);

    public int deleteFaq(FaqVO vo);

}
