/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.faq.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.faq.service.FaqService;
import kt.com.im.cms.faq.vo.FaqVO;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;

/**
 *
 * FAQ 관련 처리 API
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class FaqApi {

    private final static String viewName = "../resources/api/customer/faq/faqProcess";

    /** 게시글 구분 */
    private final static String POST_SE_ALL = "01"; // 전체
    private final static String POST_SE_NORMAL = "02"; // 일반
    private final static String POST_SE_SERVICE = "03"; // 서비스
    private final static String POST_SE_STORE = "04"; // 매장
    private final static String POST_SE_CP = "05"; // CP
    private final static String POST_SE_VERIFY = "06"; // 검수

    @Resource(name = "FaqService")
    private FaqService faqService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    /**
     * 고객센터 FAQ 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/faq")
    public ModelAndView faqProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAllowMember(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        FaqVO item = new FaqVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // 고객센터 공지사항은 Master 관리자, CMS 관리자만 관리 가능
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "faqTitle,faqType,faqCtg,faqSbst");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String faqTitle = CommonFnc.emptyCheckString("faqTitle", request);
            String faqType = CommonFnc.emptyCheckString("faqType", request);
            String faqCtg = CommonFnc.emptyCheckString("faqCtg", request);
            String faqSbst = CommonFnc.emptyCheckString("faqSbst", request);

            item.setFaqTitle(faqTitle);
            item.setFaqType(faqType);
            item.setFaqCtg(faqCtg);
            item.setFaqSbst(faqSbst);
            item.setLoginId(userVO.getMbrId());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = faqService.insertFaq(item);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            // 고객센터 FAQ는 Master 관리자, CMS 관리자만 관리 가능
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "faqSeq,faqTitle,faqType,faqCtg,faqSbst,delYn");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int faqSeq = CommonFnc.emptyCheckInt("faqSeq", request);
            String faqTitle = CommonFnc.emptyCheckString("faqTitle", request);
            String faqType = CommonFnc.emptyCheckString("faqType", request);
            String faqCtg = CommonFnc.emptyCheckString("faqCtg", request);
            String faqSbst = CommonFnc.emptyCheckString("faqSbst", request);
            String delYn = CommonFnc.emptyCheckString("delYn", request);

            item.setFaqSeq(faqSeq);
            item.setFaqTitle(faqTitle);
            item.setFaqType(faqType);
            item.setFaqCtg(faqCtg);
            item.setFaqSbst(faqSbst);
            item.setDelYn(delYn);
            item.setLoginId(userVO.getMbrId());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = faqService.updateFaq(item);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            // 고객센터 FAQ는 Master 관리자, CMS 관리자만 관리 가능
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            String faqSeqList = CommonFnc.emptyCheckString("faqSeqList", request);
            String[] delFaqSeqList = faqSeqList.split(",");

            if (delFaqSeqList.length == 0) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No FAQ list to delete.)");
                return rsModel.getModelAndView();
            }

            item.setLoginId(userVO.getMbrId());
            item.setDelFaqSeqList(delFaqSeqList);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                faqService.deleteFaq(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            int faqSeq = CommonFnc.emptyCheckInt("faqSeq", request);

            if (faqSeq != 0) {
                item.setFaqSeq(faqSeq);

                FaqVO resultItem = faqService.faqDetail(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    boolean isAllowFaq = this.isAllowFaq(userVO, resultItem);
                    if (!isAllowFaq) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    // FAQ 수정을 위해 상세 정보 요청 시에는 조회 수 업데이트 하지 않음.
                    String searchType = CommonFnc.emptyCheckString("searchType", request);
                    if (!searchType.equals("edit")) {
                        faqService.updateFaqRetvNum(item);
                        resultItem.setRetvNum(resultItem.getRetvNum() + 1);
                    }

                    /* 개인정보 마스킹 (등록자 이름) */
                    resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                    // 내용 태그 원복
                    resultItem.setFaqSbst(CommonFnc.unescapeStr(resultItem.getFaqSbst()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setResultType("faqDetail");
                }
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (faqSeq == 0 && CommonFnc.checkReqParameter(request, "faqSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                item.setMbrSe(userVO.getMbrSe());
                item.setMbrSvcSeq(userVO.getSvcSeq());
                item.setMbrStorSeq(userVO.getStorSeq());

                String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if (searchConfirm.equals("true")) {
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    if (searchField == "faqTitle") {
                        searchField = "FAQ_TITLE";
                    } else if (searchField == "cretrNm") {
                        searchField = "CRETR_NM";
                    } else if (searchField == "faqCtg") {
                        searchField = "FAQ_CTG";
                    }

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }
                /*
                 * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                 */
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);

                int page = (currentPage - 1) * limit + 1;
                int pageEnd = page + limit - 1;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("num")) {
                    sidx = "FAQ_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<FaqVO> resultItem = faqService.faqList(item);
                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    /* 개인정보 마스킹 (등록자 이름) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));
                    }

                    int totalCount = faqService.faqListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setData("resultType", "faqList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * FAQ 등록 시 구분 항목 조회 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/faqType")
    public ModelAndView faqType(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 로그인되지 않았거나 마스터, CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAdministrator(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        FaqVO item = new FaqVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String cretrMbrSe = CommonFnc.emptyCheckString("cretrMbrSe", request); // 등록자 권한 (수정일 경우에만 값 존재)
            item.setCretrMbrSe(cretrMbrSe);
            List<FaqVO> resultItemList = faqService.faqTypeList(item);

            if (resultItemList.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItemList);
                rsModel.setResultType("faqTypeList");
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    private boolean isAllowFaq(MemberVO userVO, FaqVO resultItem) {
        /**
         * 매장관리자 - 전체, 일반, 매장의 활성화된 공지사항만 접근가능
         * 서비스관리자 - 전체, 일반, 서비스, 매장의 활성화된 공지사항만 접근가능
         * 검수자 - 전체, 검수의 활성화된 공지사항만 접근가능
         * 18.11.30 고객센터 컨셉 변경으로 서비스관리자, 매장관리자도 전체, 일반만 접근 가능 (구분 항목에서 서비스, 매장 항목은 삭제됨)
         */
        if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)
                || userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                || userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_ACCEPTOR))
                && resultItem.getDelYn().equals("Y")) {
            return false;
        }

        if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)) {
            if (!resultItem.getFaqTypeCode().equals(POST_SE_ALL)
                && !resultItem.getFaqTypeCode().equals(POST_SE_NORMAL)
                && !resultItem.getFaqTypeCode().equals(POST_SE_STORE)) {
                return false;
            }
        } else if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)) {
            if (!resultItem.getFaqTypeCode().equals(POST_SE_ALL)
                && !resultItem.getFaqTypeCode().equals(POST_SE_NORMAL)
                && !resultItem.getFaqTypeCode().equals(POST_SE_SERVICE)
                && !resultItem.getFaqTypeCode().equals(POST_SE_STORE)) {
                return false;
            }
        } else if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_ACCEPTOR)) {
            if (!resultItem.getFaqTypeCode().equals(POST_SE_ALL)
                    && !resultItem.getFaqTypeCode().equals(POST_SE_VERIFY)) {
                return false;
            }
        }

        return true;
    }

}
