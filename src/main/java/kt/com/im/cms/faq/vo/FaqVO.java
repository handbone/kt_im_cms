/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.faq.vo;

import java.io.Serializable;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 3.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class FaqVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6295195596989733527L;

    /** FAQ 번호 */
    private int faqSeq;

    /** FAQ 구분 */
    private String faqType;

    /** FAQ 구분 코드 */
    private String faqTypeCode;

    /** FAQ 카테고리 */
    private String faqCtg;
    
    /** FAQ 카테고리 코드 */
    private String faqCtgCode;

    /** FAQ 제목 (질문) */
    private String faqTitle;

    /** FAQ 내용 (답변) */
    private String faqSbst;

    /** 조회수 */
    private int retvNum;

    /** FAQ 등록 일시 */
    private String regDt;

    /** FAQ 등록자 이름 */
    private String cretrNm;

    /** FAQ 삭제 여부 */
    private String delYn;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** 사용자 아이디 */
    private String loginId;

    /** 사용자 권한 */
    private String mbrSe;

    /** 등록자 권한 정보 */
    private String cretrMbrSe;

    /** 사용자 계정의 서비스 번호 */
    private int mbrSvcSeq;

    /** 사용자 계정의 매장 번호 */
    private int mbrStorSeq;

    /** FAQ 리스트 타입 (null: 고객센터 FAQ , service: 서비스매장관리 FAQ */
    private String listType;

    /** 공통 코드 번호 */
    int comnCdSeq;

    /** 공통 코드 분류 */
    String comnCdCtg;

    /** 공통 코드 명 */
    String comnCdNm;

    /** 공통 코드 값 */
    String comnCdValue;

    /** 공통 코드 삭제 여부 */
    String comnDelYn;

    /** 삭제할 FAQ 목록 */
    private int[] delFaqSeqList;

    public int getFaqSeq() {
        return faqSeq;
    }

    public void setFaqSeq(int faqSeq) {
        this.faqSeq = faqSeq;
    }

    public String getFaqType() {
        return faqType;
    }

    public void setFaqType(String faqType) {
        this.faqType = faqType;
    }

    public String getFaqTypeCode() {
        return faqTypeCode;
    }

    public void setFaqTypeCode(String faqTypeCode) {
        this.faqTypeCode = faqTypeCode;
    }

    public String getFaqCtg() {
        return faqCtg;
    }

    public void setFaqCtg(String faqCtg) {
        this.faqCtg = faqCtg;
    }

    public String getFaqCtgCode() {
        return faqCtgCode;
    }

    public void setFaqCtgCode(String faqCtgCode) {
        this.faqCtgCode = faqCtgCode;
    }

    public String getFaqTitle() {
        return faqTitle;
    }

    public void setFaqTitle(String faqTitle) {
        this.faqTitle = faqTitle;
    }

    public String getFaqSbst() {
        return faqSbst;
    }

    public void setFaqSbst(String faqSbst) {
        this.faqSbst = faqSbst;
    }

    public int getRetvNum() {
        return retvNum;
    }

    public void setRetvNum(int retvNum) {
        this.retvNum = retvNum;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getMbrSe() {
        return mbrSe;
    }

    public void setMbrSe(String mbrSe) {
        this.mbrSe = mbrSe;
    }

    public String getCretrMbrSe() {
        return cretrMbrSe;
    }

    public void setCretrMbrSe(String cretrMbrSe) {
        this.cretrMbrSe = cretrMbrSe;
    }

    public int getMbrSvcSeq() {
        return mbrSvcSeq;
    }

    public void setMbrSvcSeq(int mbrSvcSeq) {
        this.mbrSvcSeq = mbrSvcSeq;
    }

    public int getMbrStorSeq() {
        return mbrStorSeq;
    }

    public void setMbrStorSeq(int mbrStorSeq) {
        this.mbrStorSeq = mbrStorSeq;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public int getComnCdSeq() {
        return comnCdSeq;
    }

    public void setComnCdSeq(int comnCdSeq) {
        this.comnCdSeq = comnCdSeq;
    }

    public String getComnCdCtg() {
        return comnCdCtg;
    }

    public void setComnCdCtg(String comnCdCtg) {
        this.comnCdCtg = comnCdCtg;
    }

    public String getComnCdNm() {
        return comnCdNm;
    }

    public void setComnCdNm(String comnCdNm) {
        this.comnCdNm = comnCdNm;
    }

    public String getComnCdValue() {
        return comnCdValue;
    }

    public void setComnCdValue(String comnCdValue) {
        this.comnCdValue = comnCdValue;
    }

    public String getComnDelYn() {
        return comnDelYn;
    }

    public void setComnDelYn(String comnDelYn) {
        this.comnDelYn = comnDelYn;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int[] getDelFaqSeqList() {
        return delFaqSeqList;
    }

    public void setDelFaqSeqList(String[] delFaqSeqList) {
        this.delFaqSeqList = new int[delFaqSeqList.length];
        for (int i = 0; i < delFaqSeqList.length; i++) {
            this.delFaqSeqList[i] = Integer.parseInt(delFaqSeqList[i]);
        }
    }

}
