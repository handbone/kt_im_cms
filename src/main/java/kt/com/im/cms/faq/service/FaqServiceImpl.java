/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.faq.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.faq.dao.FaqDAO;
import kt.com.im.cms.faq.vo.FaqVO;

/**
 *
 * FAQ 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("FaqService")
public class FaqServiceImpl implements FaqService {

    @Resource(name = "FaqDAO")
    private FaqDAO faqDAO;

    /**
     * FAQ 리스트 조회
     * @param FaqVO
     * @return 검색 조건에 부합하는 FAQ 리스트
     */
    @Override
    public List<FaqVO> faqList(FaqVO vo) {
        return faqDAO.faqList(vo);
    }

    /**
     * FAQ 리스트 합계 조회
     * @param FaqVO
     * @return 검색 조건에 부합하는 FAQ 리스트 합계
     */
    @Override
    public int faqListTotalCount(FaqVO vo) {
        int res = faqDAO.faqListTotalCount(vo);
        return res;
    }

    /**
     * FAQ 상세 정보 조회
     * @param FaqVO
     * @return 검색 조건에 부합하는 공지사항 상세 정보
     */
    @Override
    public FaqVO faqDetail(FaqVO vo) {
        return faqDAO.faqDetail(vo);
    }

    /**
     * FAQ 조회수 업데이트
     * @param FaqVO
     * @return
     */
    @Override
    public void updateFaqRetvNum(FaqVO vo) {
        faqDAO.updateFaqRetvNum(vo);
    }

    /**
     * 회원 권한에 따른 서비스 FAQ 타입 목록 조회
     * @param FaqVO
     * @return FAQ 타입 목록
     */
    @Override
    public List<FaqVO> faqTypeList(FaqVO vo) {
        return faqDAO.faqTypeList(vo);
    }

    /**
     * FAQ 등록
     * @param FaqVO
     * @return 등록 결과
     */
    @Override
    public int insertFaq(FaqVO vo) {
        return faqDAO.insertFaq(vo);
    }

    /**
     * FAQ 수정
     * @param FaqVO
     * @return 수정 결과
     */
    @Override
    public int updateFaq(FaqVO vo) {
        return faqDAO.updateFaq(vo);
    }

    /**
     * FAQ 삭제
     * @param FaqVO
     * @return 삭제 결과
     */
    @Override
    public int deleteFaq(FaqVO vo) {
        return faqDAO.deleteFaq(vo);
    }

}
