/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright Version above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceappversion.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.serviceappversion.vo.ServiceAppVersionVO;
import kt.com.im.cms.version.vo.VersionVO;

/**
 *
 * 서비스 앱 버전 관련 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2019.02.12
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 02. 12.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("ServiceAppVersionDAO")
public class ServiceAppVersionDAOImpl extends mysqlAbstractMapper implements ServiceAppVersionDAO {

    /**
     * 서비스 앱 버전 등록
     *
     * @param ServiceAppVersionVO
     * @return 결과
     */
    @Override
    public int insertServiceAppVersion(ServiceAppVersionVO data) {
        return insert("ServiceAppVersionDAO.insertServiceAppVersion", data);
    }

    /**
     * 서비스 앱 버전 삭제
     *
     * @param ServiceAppVersionVO
     * @return 결과
     */
    @Override
    public int deleteServiceAppVersion(ServiceAppVersionVO data) {
        return update("ServiceAppVersionDAO.deleteServiceAppVersion", data);
    }

    /**
     * 서비스 앱 버전 수정
     *
     * @param ServiceAppVersionVO
     * @return 결과
     */
    @Override
    public int updateServiceAppVersion(ServiceAppVersionVO data) {
        return update("ServiceAppVersionDAO.updateServiceAppVersion", data);
    }

    /**
     * 서비스 앱 버전 상세 조회
     *
     * @param ServiceAppVersionVO
     * @return 결과
     */
    @Override
    public ServiceAppVersionVO serviceAppVersionInfo(ServiceAppVersionVO data) {
        return selectOne("ServiceAppVersionDAO.serviceAppVersionInfo", data);
    }

    /**
     * 가장 최근의 서비스 앱 버전 상세 조회
     *
     * @param ServiceAppVersionVO
     * @return 조회 결과
     */
    @Override
    public ServiceAppVersionVO selectLatestServiceAppVersion(ServiceAppVersionVO data) {
        return selectOne("ServiceAppVersionDAO.selectLatestServiceAppVersion", data);
    }

    /**
     * 서비스 앱 버전 리스트 목록
     *
     * @param ServiceAppVersionVO
     * @return 서비스 앱 버전 리스트 목록
     */
    @Override
    public List<ServiceAppVersionVO> serviceAppVersionList(ServiceAppVersionVO data) {
        return selectList("ServiceAppVersionDAO.serviceAppVersionList", data);
    }

    /**
     * 서비스 앱 버전 리스트  정보
     *
     * @param ServiceAppVersionVO
     * @return 서비스 앱 버전 리스트 조회 개수
     */
    @Override
    public int serviceAppVersionListTotalCount(ServiceAppVersionVO data) {
        int res = (Integer) selectOne("ServiceAppVersionDAO.serviceAppVersionListTotalCount", data);
        return res;
    }

    /**
     * 서비스 버전 명 존재 유무
     *
     * @param ServiceAppVersionVO
     * @return 서비스 앱 버전 리스트 조회 개수
     */
    @Override
    public int searchSvcAppVerInfo(ServiceAppVersionVO data) {
        return selectOne("ServiceAppVersionDAO.searchSvcAppVerInfo", data);
    }
}
