/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright Version above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceappversion.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.serviceappversion.dao.ServiceAppVersionDAO;
import kt.com.im.cms.serviceappversion.vo.ServiceAppVersionVO;
import kt.com.im.cms.version.vo.VersionVO;


/**
 *
 * 서비스 앱 버전 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2019.02.12
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 02. 12.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("ServiceAppVersionService")
public class ServiceAppVersionServiceImpl implements ServiceAppVersionService {

    @Resource(name = "ServiceAppVersionDAO")
    private ServiceAppVersionDAO serviceAppVersionDAO;

    /**
     * 서비스 앱 버전 삭제
     *
     * @param ServiceAppVersionVO
     * @return 결과
     */
    @Override
    public int deleteServiceAppVersion(ServiceAppVersionVO data) {
        return serviceAppVersionDAO.deleteServiceAppVersion(data);
    }

    /**
     * 서비스 앱 버전 수정
     *
     * @param ServiceAppVersionVO
     * @return 결과
     */
    @Override
    public int updateServiceAppVersion(ServiceAppVersionVO data) {
        return serviceAppVersionDAO.updateServiceAppVersion(data);
    }

    /**
     * 서비스 앱 버전 등록
     *
     * @param ServiceAppVersionVO
     * @return 결과
     */
    @Override
    public int insertServiceAppVersion(ServiceAppVersionVO data) {
        return serviceAppVersionDAO.insertServiceAppVersion(data);
    }

    /**
     * 서비스 앱 버전 상세조회
     *
     * @param ServiceAppVersionVO
     * @return 결과
     */
    @Override
    public ServiceAppVersionVO serviceAppVersionInfo(ServiceAppVersionVO data) {
        return serviceAppVersionDAO.serviceAppVersionInfo(data);
    }

    /**
     * 가장 최근의 서비스 앱 버전 상세 조회
     *
     * @param ServiceAppVersionVO
     * @return 조회 결과
     */
    @Override
    public ServiceAppVersionVO selectLatestServiceAppVersion(ServiceAppVersionVO data) {
        return serviceAppVersionDAO.selectLatestServiceAppVersion(data);
    }

    /**
     * 서비스 앱 버전 리스트 목록
     *
     * @param ServiceAppVersionVO
     * @return  서비스 앱 버전 리스트 목록
     */
    @Override
    public List<ServiceAppVersionVO> serviceAppVersionList(ServiceAppVersionVO data) {
        return serviceAppVersionDAO.serviceAppVersionList(data);
    }

    /**
     * 서비스 앱 버전 리스트 정보
     *
     * @param ServiceAppVersionVO
     * @return 서비스 앱 버전 리스트 조회 개수
     */
    @Override
    public int serviceAppVersionListTotalCount(ServiceAppVersionVO data) {
        int res = serviceAppVersionDAO.serviceAppVersionListTotalCount(data);
        return res;
    }

    /**
     * 서비스 버전 명 존재 유무
     *
     * @param ServiceAppVersionVO
     * @return 서비스 앱 버전 리스트 조회 개수
     */
    @Override
    public int searchSvcAppVerInfo(ServiceAppVersionVO data) {
        int res = serviceAppVersionDAO.searchSvcAppVerInfo(data);
        return res;
    }
}
