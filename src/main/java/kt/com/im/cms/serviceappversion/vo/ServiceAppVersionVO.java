/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright Version above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.serviceappversion.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * ServiceAppVersionVO
 *
 * @author A2TEC
 * @since 2019.02.12
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 02. 12.   A2TEC      최초생성
 *
 *
 *      </pre>
 */
public class ServiceAppVersionVO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9145300760706958557L;

    /** 서비스 앱 버전 관리 번호 */
    private int svcAppVerSeq;

    /** 서비스 번호 */
    private int svcSeq;

    /** 서비스 명 */
    private String svcNm;

    /** 서비스 앱 버전 */
    private String svcAppVer;

    /** 등록 일시 */
    private String regDt;

    /** 등록자 아이디 */
    private String regrId;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** 서비스 앱 타입 */
    private String dlStoreType;

    /** 수정 내역 */
    private String uptSbst;

    /** 서비스 앱 링크 */
    private String dlStorePath;

    /** 사용 여부 */
    private String useYn;

    /** 리스트 */
    private List list;

    /** 공통 코드 번호 */
    int comnCdSeq;

    /** 공통 코드 분류 */
    String comnCdCtg;

    /** 공통 코드 명 */
    String comnCdNm;

    /** 공통 코드 값 */
    String comnCdValue;

    /** 앱 구분 */
    private String appType;

    /** 로그인 아이디 */
    private String loginId;

    /** 검색 유무 */
    private String searchConfirm;

    /** 검색 키워드 */
    private String keyword;

    /** 페이지 값 */
    private int offset;

    /** 리스트 개수 */
    private int limit;

    /** 정렬 필드 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 조건(카테고리) */
    private String target;

    /** 미러링 앱 여부 */
    private String mirrYn;

    /** 회원 아이디 */
    private String mbrID;

    /** 회원 이름 */
    private String mbrNm;

    /** 서비스 앱 버전 삭제 리스트 */
    private List<Integer> deleteSvcAppVersionSeqList;

    /** 사용자 권한 */
    private String mbrSe;

    private String svcType;

    public int getSvcAppVerSeq() {
        return svcAppVerSeq;
    }

    public void setSvcAppVerSeq(int svcAppVerSeq) {
        this.svcAppVerSeq = svcAppVerSeq;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getSvcAppVer() {
        return svcAppVer;
    }

    public void setSvcAppVer(String svcAppVer) {
        this.svcAppVer = svcAppVer;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getRegrId() {
        return regrId;
    }

    public void setRegrId(String regrId) {
        this.regrId = regrId;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getDlStoreType() {
        return dlStoreType;
    }

    public void setDlStoreType(String dlStoreType) {
        this.dlStoreType = dlStoreType;
    }

    public String getUptSbst() {
        return uptSbst;
    }

    public void setUptSbst(String uptSbst) {
        this.uptSbst = uptSbst;
    }

    public String getDlStorePath() {
        return dlStorePath;
    }

    public void setDlStorePath(String dlStorePath) {
        this.dlStorePath = dlStorePath;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getComnCdSeq() {
        return comnCdSeq;
    }

    public void setComnCdSeq(int comnCdSeq) {
        this.comnCdSeq = comnCdSeq;
    }

    public String getComnCdCtg() {
        return comnCdCtg;
    }

    public void setComnCdCtg(String comnCdCtg) {
        this.comnCdCtg = comnCdCtg;
    }

    public String getComnCdNm() {
        return comnCdNm;
    }

    public void setComnCdNm(String comnCdNm) {
        this.comnCdNm = comnCdNm;
    }

    public String getComnCdValue() {
        return comnCdValue;
    }

    public void setComnCdValue(String comnCdValue) {
        this.comnCdValue = comnCdValue;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getMirrYn() {
        return mirrYn;
    }

    public void setMirrYn(String mirrYn) {
        this.mirrYn = mirrYn;
    }

    public String getMbrID() {
        return mbrID;
    }

    public void setMbrID(String mbrID) {
        this.mbrID = mbrID;
    }

    public String getMbrNm() {
        return mbrNm;
    }

    public void setMbrNm(String mbrNm) {
        this.mbrNm = mbrNm;
    }

    public List<Integer> getDeleteSvcAppVersionSeqList() {
        return deleteSvcAppVersionSeqList;
    }

    public void setDeleteSvcAppVersionSeqList(List<Integer> deleteSvcAppVersionSeqList) {
        this.deleteSvcAppVersionSeqList = deleteSvcAppVersionSeqList;
    }

    public String getMbrSe() {
        return mbrSe;
    }

    public void setMbrSe(String mbrSe) {
        this.mbrSe = mbrSe;
    }

    public String getSvcType() {
        return svcType;
    }

    public void setSvcType(String svcType) {
        this.svcType = svcType;
    }

}
