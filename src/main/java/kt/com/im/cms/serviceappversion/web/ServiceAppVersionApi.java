package kt.com.im.cms.serviceappversion.web;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.serviceappversion.service.ServiceAppVersionService;
import kt.com.im.cms.serviceappversion.vo.ServiceAppVersionVO;
import kt.com.im.cms.servics.service.ServicsService;

/**
*
* 앱 버전 관리 API
*
* @author A2TEC
* @since 2019.04.11
* @version 1.0
* @see
*
*      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019.04.11.   A2TEC      최초생성
*
*      </pre>
*/

@Controller
public class ServiceAppVersionApi {

    private final static String olSvcVerViewName = "../resources/api/onlineService/version/serviceAppVersionProcess";

    @Resource(name = "ServiceAppVersionService")
    private ServiceAppVersionService serviceAppVersionService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "ServicsService")
    private ServicsService servicsService;

    @Autowired
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * service app version 정보 조회 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/svcAppVersion")
    public ModelAndView svcAppVersionProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");

        String svcType = CommonFnc.emptyCheckString("svcType", request);

        if (userVO == null || !isAllowMember(userVO, svcType)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        if (svcType.equals("")) {   // 오프라인 추가 될 시 사용
            if (userVO.getMbrSe().equals("01") || userVO.getMbrSe().equals("02") || userVO.getMbrSe().equals("03")) {
                rsModel.setViewName(olSvcVerViewName);
                return onlineSvcAppVersionProcess(request, rsModel, userVO);
            }
//              rsModel.setViewName(viewName);
//              return offlineSvcAppVersionProcess(request, rsModel, userVO);
        }

        if (svcType.equals("ON")) {
            rsModel.setViewName(olSvcVerViewName);
            return onlineSvcAppVersionProcess(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * (Online) 서비스 앱 버전
     */
    private ModelAndView onlineSvcAppVersionProcess(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        request.setCharacterEncoding("UTF-8");

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) { // post
            return insertSvcAppVersion(request, rsModel, userVO);
        }

        if (method.equals("PUT")) { // put
            return updateSvcAppVersion(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) { // delete
            return deleteSvcAppVersion(request, rsModel);
        }

        if (method.equals("GET")) { // get
            boolean isList = (request.getParameter("svcAppVerSeq") == null);
            if (isList) {
                return getSvcAppVersionList(request, rsModel);
            }

            return getSvcAppVersionInfo(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView insertSvcAppVersion (HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        ServiceAppVersionVO vo = new ServiceAppVersionVO();

        // Checking Mendatory S
        String checkString = CommonFnc.requiredChecking(request,"svcSeq,svcAppVer,appType,dlStorePath,useYn");
        if (!checkString.equals("")) {
            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
            return rsModel.getModelAndView();
        }
        // Checking Mendatory E

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (!isAllowOlSvcVersion(userVO, svcSeq)) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            return rsModel.getModelAndView();
        }

        vo.setSvcAppVer(request.getParameter("svcAppVer"));
        vo.setSvcSeq(svcSeq);

        boolean isExistVerNm = (serviceAppVersionService.searchSvcAppVerInfo(vo) > 0) ? true : false;
        if (isExistVerNm) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage("exist verNm");
            return rsModel.getModelAndView();
        }

        int svcAppVerSeq = CommonFnc.emptyCheckInt("svcAppVerSeq", request);
        String appType = CommonFnc.emptyCheckString("appType", request);
        String dlStoreType = CommonFnc.emptyCheckString("dlStoreType", request);
        String dlStorePath = CommonFnc.emptyCheckString("dlStorePath", request);
        String uptSbst = CommonFnc.emptyCheckString("uptSbst", request);
        String useYn = CommonFnc.emptyCheckString("useYn", request);

        vo.setSvcAppVerSeq(svcAppVerSeq);
        vo.setAppType(appType);
        vo.setDlStoreType(dlStoreType);

        // escape 문자들 원복하여 저장
        dlStorePath = CommonFnc.unescapeStr(dlStorePath);
        uptSbst = CommonFnc.unescapeStr(uptSbst);
        vo.setDlStorePath(dlStorePath);
        vo.setUptSbst(uptSbst);
        vo.setUseYn(useYn);
        vo.setRegrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = serviceAppVersionService.insertServiceAppVersion(vo);
            if (result == 1) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setResultType("insertServiceAppVersion");
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView updateSvcAppVersion(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        /* Checking Mendatory S */
        String checkString = CommonFnc.requiredChecking(request, "svcAppVerSeq,useYn,dlStorePath");
        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }
        /* Checking Mendatory E */

        ServiceAppVersionVO vo = new ServiceAppVersionVO();

        int svcAppVerSeq = CommonFnc.emptyCheckInt("svcAppVerSeq", request);
        String dlStorePath = CommonFnc.emptyCheckString("dlStorePath", request);
        String uptSbst = CommonFnc.emptyCheckString("uptSbst", request);
        String useYn = CommonFnc.emptyCheckString("useYn", request);

        vo.setSvcAppVerSeq(svcAppVerSeq);

        // escape 문자들 원복하여 저장
        dlStorePath = CommonFnc.unescapeStr(dlStorePath);
        uptSbst = CommonFnc.unescapeStr(uptSbst);
        vo.setDlStorePath(dlStorePath);
        vo.setUptSbst(uptSbst);
        vo.setUseYn(useYn);
        vo.setAmdrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = serviceAppVersionService.updateServiceAppVersion(vo);
            if (result == 1) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.rollback(status);
            }
        } catch(Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView deleteSvcAppVersion(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String appVerSeqList = CommonFnc.emptyCheckString("delAppVerSeqList", request);
        String[] delAppVerSeqList = appVerSeqList.split(",");

        if (delAppVerSeqList.length == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No notice list to delete.)");
            return rsModel.getModelAndView();
        }

        List<Integer> deleteSvcAppVersionSeqList = new ArrayList<Integer>();
        for (int i = 0; i < delAppVerSeqList.length; i++) {
            deleteSvcAppVersionSeqList.add(Integer.parseInt(delAppVerSeqList[i]));
        }

        ServiceAppVersionVO vo = new ServiceAppVersionVO();

        vo.setDeleteSvcAppVersionSeqList(deleteSvcAppVersionSeqList);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            serviceAppVersionService.deleteServiceAppVersion(vo);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            transactionManager.commit(status);
        } catch(Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView getSvcAppVersionList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        ServiceAppVersionVO vo = new ServiceAppVersionVO();

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        String appType = CommonFnc.emptyCheckString("appType", request);

        vo.setSvcSeq(svcSeq);
        vo.setAppType(appType);

        String searchConfirm = request.getParameter("_search") == null? "false" : request.getParameter("_search");
        vo.setSearchConfirm(searchConfirm);

        // 검색 여부가 true일 경우
        if (searchConfirm.equals("true")) {
            /** 검색 카테고리 */
            String searchField = request.getParameter("searchField") == null ? "" : request.getParameter("searchField");
            /** 검색어 */
            String searchString = request.getParameter("searchString") == null ? "" : request.getParameter("searchString");

            vo.setTarget(searchField);
            vo.setKeyword(searchString);
        }
        /*
         * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
         */
        int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
        int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));
        String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");
        String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");
        int page = (currentPage - 1) * limit + 1;
        int pageEnd = (currentPage - 1) * limit + limit;

        vo.setSidx(sidx);
        vo.setSord(sord);
        vo.setOffset(page);
        vo.setLimit(pageEnd);

        List<ServiceAppVersionVO> result = serviceAppVersionService.serviceAppVersionList(vo);

        if(result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
        } else {
            /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
            for (int i = 0; i < result.size(); i++) {
                result.get(i).setMbrNm(CommonFnc.getNameMask(result.get(i).getMbrNm()));
            }

            int totalCount = serviceAppVersionService.serviceAppVersionListTotalCount(vo);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            rsModel.setData("item", result);
            rsModel.setData("totalCount", totalCount);
            rsModel.setData("currentPage", currentPage);
            rsModel.setData("totalPage", (totalCount - 1) / limit);
            rsModel.setResultType("svcAppVersionList");
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView getSvcAppVersionInfo(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        int svcAppVerSeq = CommonFnc.emptyCheckInt("svcAppVerSeq", request);

        // Checking Mendatory S
        String checkString = CommonFnc.requiredChecking(request,"svcAppVerSeq");
        if (!checkString.equals("")) {
            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
            return rsModel.getModelAndView();
        }
        // Checking Mendatory E

        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcAppVerSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcAppVerSeq)");
            return rsModel.getModelAndView();
        }

        ServiceAppVersionVO vo = new ServiceAppVersionVO();
        vo.setSvcAppVerSeq(svcAppVerSeq);
        vo.setSvcSeq(svcSeq);

        /** 서비스 앱 버전 상세 정보 */
        ServiceAppVersionVO result = serviceAppVersionService.serviceAppVersionInfo(vo);

        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
        } else {
            if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                    && userVO.getSvcSeq() != result.getSvcSeq())) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            /* 개인정보 마스킹 (등록자 아이디) */
            result.setMbrNm(CommonFnc.getNameMask(result.getMbrNm()));

            /** 서비스 앱 버전 목록 */
            List<ServiceAppVersionVO> resultList = serviceAppVersionService.serviceAppVersionList(vo);

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            rsModel.setData("item", result);
            rsModel.setData("resultList", resultList);
            rsModel.setResultType("svcAppVersionInfo");
        }
        return rsModel.getModelAndView();
    }

    /**
     * service app version 정보 조회 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/serviceAppVersion")
    public ModelAndView serviceAppVersionProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/onlineService/version/serviceAppVersionProcess");

        request.setCharacterEncoding("UTF-8");
        ServiceAppVersionVO vo = new ServiceAppVersionVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String invalidParams = CommonFnc.requiredChecking(request, "mbrTokn,storSeq,svcSeq,svcAppVer");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            if (svcSeq == 0) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
                return rsModel.getModelAndView();
            }

            String svcAppVer = CommonFnc.emptyCheckString("svcAppVer", request);

            vo.setSvcSeq(svcSeq);
            vo.setSvcAppVer(svcAppVer);
            vo.setUseYn("Y");

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = serviceAppVersionService.insertServiceAppVersion(vo);
                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    transactionManager.commit(status);
                }
            } catch(Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            if (svcSeq == 0) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
                return rsModel.getModelAndView();
            }

            vo.setSvcSeq(svcSeq);

            String appType = CommonFnc.emptyCheckString("appType", request);
            if (appType == "") {
                vo.setAppType("SA");
            } else {
                if(appType.equals("SA") || appType.equals("MA")) {
                    vo.setAppType(appType);
                } else {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(appType)");
                    return rsModel.getModelAndView();
                }
            }

            ServiceAppVersionVO result = serviceAppVersionService.selectLatestServiceAppVersion(vo);

            if (result == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (등록자 아이디) */
                result.setRegrId(CommonFnc.getIdMask(result.getRegrId()));

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", result);
                rsModel.setResultType("versionInfo");
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO, String svcType) {
        String mbrSe = userVO.getMbrSe();
        if (svcType.equals("ON")) {
            return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe);
        }

        if (svcType.equals("")) {
            return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe)
                    || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
        }
        return false;
    }

    private boolean isAllowOlSvcVersion(MemberVO userVO, int svcSeq) {
        if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE) && userVO.getSvcSeq() != svcSeq)) {
            return false;
        }
        return true;
    }
}
