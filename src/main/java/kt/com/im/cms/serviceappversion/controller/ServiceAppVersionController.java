package kt.com.im.cms.serviceappversion.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
*
* 서비스 앱 버전 관련 페이지 컨트롤러
*
* @author A2TEC
* @since 2019
* @version 1.0
* @see
*
*      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2019. 4. 9.   A2TEC      최초생성
*
*
*      </pre>
*/

@Component
@Controller
public class ServiceAppVersionController {

    /**
     * 서비스 앱 버전 리스트 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineSvc/version", method = RequestMethod.GET)
    public ModelAndView svcAppVerList(ModelAndView mv) {
        mv.setViewName("/views/onlineService/version/VersionList");
        return mv;
    }

    /**
     * 서비스 앱 버전 상세 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineSvc/version/{svcAppVerSeq}", method = RequestMethod.GET)
    public ModelAndView svcAppVerDetail(ModelAndView mv, @PathVariable(value = "svcAppVerSeq") String svcAppVerSeq) {
        mv.setViewName("/views/onlineService/version/VersionDetail");
        return mv;
    }

    /**
     * 서비스 앱 버전 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineSvc/version/regist", method = RequestMethod.GET)
    public ModelAndView svcAppVerRegist(ModelAndView mv) {
        mv.setViewName("/views/onlineService/version/VersionRegist");
        return mv;
    }

    /**
     * 서비스 앱 버전 수정 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/onlineSvc/version/{svcAppVerSeq}/edit", method = RequestMethod.GET)
    public ModelAndView svcAppVerEdit(ModelAndView mv, @PathVariable(value = "svcAppVerSeq") String svcAppVerSeq) {
        mv.setViewName("/views/onlineService/version/VersionEdit");
        return mv;
    }
}
