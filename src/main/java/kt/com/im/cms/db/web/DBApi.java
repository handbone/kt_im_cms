/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.db.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.db.service.DbService;
import kt.com.im.cms.db.vo.DbVO;
import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;

/**
 *
 * DB 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 14.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class DBApi {

    private final static String viewName = "../resources/api/db/dbProcess";

    @Resource(name = "DbService")
    private DbService dbService;

    @Resource(name = "FileService")
    private FileService fileService;

    @Resource(name = "stateTransactionManager")
    private DataSourceTransactionManager txManager;

    /**
     * DB 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/db")
    public ModelAndView dbProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAdministrator = this.isAdministrator(userVO);
        if (userVO == null || !isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String type = request.getParameter("type");
            boolean isList = "list".equals(type);
            if (isList) {
                return this.getDbList(request, rsModel);
            }

            if ("file".equals(type)) {
                return this.getFileSize(request, rsModel);
            }
            return this.getDb(request, rsModel);
        }

        if (method.equals("POST")) {
            return this.insertDb(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return this.updateDb(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return this.deleteDb(request, rsModel, userVO);
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getDbList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        DbVO vo = new DbVO();
        List<DbVO> result = dbService.getDbList(vo);
        if (result == null || result.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("dbList");
        rsModel.setData("item", result);

        return rsModel.getModelAndView();
    }

    private ModelAndView getDb(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = Validator.validate(request, "dbSeq:{" + Validator.NUMBER +"}");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        DbVO vo = new DbVO();
        vo.setDbSeq(Integer.parseInt(request.getParameter("dbSeq")));

        DbVO resultItem = dbService.getDb(vo);
        if (resultItem == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        /* 개인정보 마스킹 (DB ip, 등록자 아이디, 수정자 아이디) */
        resultItem.setDbIp(CommonFnc.getIpAddrMask(resultItem.getDbIp()));
        resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
        resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));

        rsModel.setResultType("dbInfo");
        rsModel.setData("item", resultItem);

        return rsModel.getModelAndView();
    }

    private ModelAndView insertDb(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "dbNm:{" + Validator.LENGTH + "=1,20};dbIp:{" + Validator.IP_ADDRESS + "};"
                              + "dbTotSize:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            DbVO vo = new DbVO();
            vo.setDbNm(request.getParameter("dbNm"));
            vo.setDbIp(request.getParameter("dbIp"));
            vo.setDbTotSize(Integer.parseInt(request.getParameter("dbTotSize")));

            vo.setCretrId(userVO.getMbrId());

            int resultCode = dbService.insertDb(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView updateDb(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "dbSeq:{" + Validator.NUMBER + "};dbNm:{" + Validator.LENGTH + "=1,20};"
                              + "dbIp:{" + Validator.IP_ADDRESS + "};dbTotSize:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        DbVO vo = new DbVO();
        vo.setDbSeq(Integer.parseInt(request.getParameter("dbSeq")));
        vo.setDbNm(request.getParameter("dbNm"));
        vo.setDbIp(request.getParameter("dbIp"));
        vo.setDbTotSize(Integer.parseInt(request.getParameter("dbTotSize")));
        // 이전 데이터 조회
        DbVO oldDbVO = dbService.getDb(vo);
        if (oldDbVO == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            return rsModel.getModelAndView();
        }

        // 데이터 변경 여부 확인
        boolean notChanged = oldDbVO.getDbNm().equals(vo.getDbNm()) && oldDbVO.getDbIp().equals(vo.getDbIp())
                            && (oldDbVO.getDbTotSize() == vo.getDbTotSize());
        if (notChanged) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = dbService.updateDb(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView deleteDb(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean isAdministrator = this.isAdministrator(userVO);
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String invalidParams = Validator.validate(request, "dbSeq:{" + Validator.NUMBER + "}");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            DbVO vo = new DbVO();
            vo.setDbSeq(Integer.parseInt(request.getParameter("dbSeq")));

            int resultCode = dbService.deleteDb(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_DELETE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getFileSize(HttpServletRequest request, ResultModel rsModel) throws Exception {
        FileVO vo = new FileVO();
        FileVO resultItem = fileService.getTotalFileSize(vo);

        if (resultItem == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("fileInfo");
        rsModel.setData("item", resultItem.getFileSize());

        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }
}
