/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.db.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.db.dao.DbDAO;
import kt.com.im.cms.db.vo.DbVO;


/**
 *
 * DB 관리에 관한 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 14.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("DbService")
public class DbServiceImpl implements DbService {

    @Resource(name = "DbDAO")
    private DbDAO DbDAO;

    /**
     * DB 목록 조회
     *
     * @param DbVO
     * @return 조회 목록 결과
     */
    @Override
    public List<DbVO> getDbList(DbVO vo) throws Exception {
        return DbDAO.getDbList(vo);
    }

    /**
     * DB 목록 수 조회
     *
     * @param DbVO
     * @return 조회 목록 결과
     */
    @Override
    public int getDbListTotalCount(DbVO vo) throws Exception {
        return DbDAO.getDbListTotalCount(vo);
    }

    /**
     * DB 정보 조회
     * 
     * @param DbVO
     * @return 검색조건에 해당되는 DB 정보
     */
    @Override
    public DbVO getDb(DbVO vo) throws Exception {
        return DbDAO.getDb(vo);
    }

    /**
     * DB 정보 추가
     * 
     * @param DbVO
     * @return 처리 결과 
     */
    @Override
    public int insertDb(DbVO vo) throws Exception {
        return DbDAO.insertDb(vo);
    }

    /**
     * DB 정보 수정
     * 
     * @param DbVO
     * @return 처리 결과 
     */
    @Override
    public int updateDb(DbVO vo) throws Exception {
        return DbDAO.updateDb(vo);
    }

    /**
     * DB 정보 삭제
     * 
     * @param DbVO
     * @return 처리 결과 
     */
    @Override
    public int deleteDb(DbVO vo) throws Exception {
        return DbDAO.deleteDb(vo);
    }

    /**
     * DB 중복 여부 조회
     * 
     * @param DbVO
     * @return 검색조건에 해당되는 결과
     */
    @Override
    public int getDuplicationDb(DbVO vo) throws Exception {
        return DbDAO.getDuplicationDb(vo);
    }
}
