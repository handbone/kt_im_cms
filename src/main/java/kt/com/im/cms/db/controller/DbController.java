/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.db.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * DB 관리에 관한 컨트롤러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 14.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class DbController {

    /**
     * DB 관리 상세 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/system/db", method = RequestMethod.GET)
    public ModelAndView dababaseManageView(ModelAndView mv) {
        mv.setViewName("/views/system//db/DbList");
        return mv;
    }

    /**
     * DB 관리 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/system/db/regist", method = RequestMethod.GET)
    public ModelAndView dababaseRegistView(ModelAndView mv) {
        mv.setViewName("/views/system/db/DbRegist");
        return mv;
    }

    /**
     * DB 관리 수정 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/system/db/edit/{seq}", method = RequestMethod.GET)
    public ModelAndView dababaseEditView(ModelAndView mv, @PathVariable(value="seq") String seq) {
        mv.addObject("seq", seq);
        mv.setViewName("/views/system/db/DbEdit");
        return mv;
    }
}