/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.db.vo;

import java.io.Serializable;

/**
 *
 * DB 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 14.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class DbVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -2984187831563945539L;

    /** DB 번호 */
    int dbSeq;

    /** DB IP */
    String dbIp;

    /** DB 명 */
    String dbNm;

    /** DB 총 용량 */
    int dbTotSize;

    /** 생성 일시 */
    String cretDt;

    /** 생성자 아이디 */
    String cretrId;

    /** 수정 일시 */
    String amdDt;

    /** 수정자 아이디 */
    String amdrId;

    public int getDbSeq() {
        return dbSeq;
    }

    public void setDbSeq(int dbSeq) {
        this.dbSeq = dbSeq;
    }

    public String getDbIp() {
        return dbIp;
    }

    public void setDbIp(String dbIp) {
        this.dbIp = dbIp;
    }

    public String getDbNm() {
        return dbNm;
    }

    public void setDbNm(String dbNm) {
        this.dbNm = dbNm;
    }

    public int getDbTotSize() {
        return dbTotSize;
    }

    public void setDbTotSize(int dbTotSize) {
        this.dbTotSize = dbTotSize;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
