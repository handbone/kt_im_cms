/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.content.service;

import java.util.List;

import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.release.vo.ReleaseVO;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 2.   A2TEC      최초생성
 *2018. 6. 22.  A2TEC      storContentsList,storContentsListTotalCount 추가
 *
 *      </pre>
 */

public interface ContentService {

    List<ContentVO> contentsList(ContentVO item);

    int contentsListTotalCount(ContentVO item);

    ContentVO contentInfo(ContentVO item);

    List<ContentVO> contentsGenreList(ContentVO item);

    List<ContentVO> contentServiceList(ContentVO item);

    int categoryInsert(ContentVO data);

    List<ContentVO> firstCtgList(ContentVO item);

    int categoryUpdate(ContentVO data);

    int categoryDelete(ContentVO data);

    boolean canDeleteFirstCategory(ContentVO data);

    int categoryDuplicationInfo(ContentVO data);

    ContentVO selectExistSecondCategory(ContentVO data);

    List<ContentVO> contsCtgList(ContentVO item);

    int contentsInsert(ContentVO item);

    int contsCtgValueUpdate(ContentVO item);

    int contentsGenreInsert(ContentVO item);

    int contentsServiceInsert(ContentVO item);

    int contentsSttusUpdate(ContentVO item);

    List<ContentVO> contentsThumbnailList(ContentVO item);

    List<ContentVO> contentsVideoList(ContentVO item);

    List<ContentVO> fileDataInfo(ContentVO item);

    ContentVO metadataXmlInfo(ContentVO metaItem);

    ContentVO contentCoverImageInfo(ContentVO item);

    int categoryValueUpdate(ContentVO item);

    ContentVO fileInfo(ContentVO item);

    int thumbnailDelete(ContentVO item);

    int contentsUpdate(ContentVO item);

    int contentsDelete(ContentVO item);

    int contentsGenreDelete(ContentVO item);

    int contentsServiceDelete(ContentVO item);

    List<ContentVO> contentsPrevList(ContentVO item);

    ContentVO contsMetaInfo(ContentVO metaItem);

    List<ContentVO> verifyRecordList(ContentVO item);

    int verifyRecordTotalCount(ContentVO item);

    List<ContentVO> contentDisplayList(ContentVO data);

    int contentDisplayListTotalCount(ContentVO data);

    int updateContentDispSttus(ContentVO data);

    List<ContentVO> contentDisplayHistoryList(ContentVO data);

    int contentDisplayHistoryListTotalCount(ContentVO data);

    List<ContentVO> contentDisplayOnList(ContentVO data);

    List<ContentVO> recommendContentList(ContentVO data);

    int contentDisplayOnListTotalCount(ContentVO data);

    void insertRecommendContents(ContentVO data);

    void updateRecommendContents(ContentVO data);

    List<ContentVO> searchCpRcmdContentList(ContentVO data);

    int deleteRecommendContents(ContentVO data);

    List<ContentVO> searchFinishContractContents(ContentVO data);

    int searchDispOnContents(ContentVO data);

    List<ContentVO> storContentsList(ContentVO item);

    int storContentsListTotalCount(ContentVO item);

    ContentVO mmsReleaseInfo(ContentVO item);

    List<ReleaseVO> mmsReleaseContsList(ContentVO item);

    int searchAllowContentsInfo(ContentVO item);

    ContentVO contentCdtNotInfo(ContentVO contitem);

    List<ContentVO> contentsDispList(ContentVO data);

    int contentsDispListTotalCount(ContentVO data);

    String getSubMetaCodeNm(ContentVO data);

    List<ContentVO> selectMyContentList(ContentVO data);

    int selectMyContentListTotalCount(ContentVO data);

    int selectCanPlayContents(ContentVO data);

    int forceUpdateContsApproveForLocal(ContentVO data);

    List<ContentVO> contentCdtNotInfoList(ContentVO searchConts);
}
