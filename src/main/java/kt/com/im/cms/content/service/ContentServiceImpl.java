/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.content.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.content.dao.ContentDAO;
import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.release.vo.ReleaseVO;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 7.   A2TEC      최초생성
 * 2018. 5. 10.  A2TEC      /api/contents (POST,GET,PUT)추가
 * 2018. 5. 10.  A2TEC      /api/contentsCategory (POST,GET,PUT)추가
 * 2018. 5. 14.  A2TEC      콘텐츠 수정관련 추가
 * 2018. 5. 15.  A2TEC      콘텐츠 삭제관련 추가
 * 2018. 5. 21.  A2TEC      콘텐츠 미리보기 파일 리스트 메소드 추가
 * 2018. 6. 1.   A2TEC      콘텐츠 전시 목록 추가
 * 2018. 6. 4.   A2TEC      콘텐츠 전시 상태 변경
 * 2018. 6. 22.  A2TEC      storContentsList,storContentsListTotalCount 추가
 *
 *      </pre>
 */

@Service("ContentService")
public class ContentServiceImpl implements ContentService {
    @Resource(name = "ContentDAO")
    private ContentDAO ContentDAO;

    /**
     * 검색조건에 해당되는 콘텐츠 리스트 정보
     *
     * @param ContentVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ContentVO> contentsList(ContentVO data) {
        return ContentDAO.contentsList(data);
    }

    /**
     * 검색조건에 해당되는 콘텐츠 리스트의 총 개수
     *
     * @param ContentVO
     * @return 총 콘텐츠 수 /
     */
    @Override
    public int contentsListTotalCount(ContentVO data) {
        int res = ContentDAO.contentsListTotalCount(data);
        return res;
    }

    /**
     * 콘텐츠 상세 정보
     *
     * @param ContentVO
     * @return 콘텐츠 정보 (파일 / 장르 제외)
     */
    @Override
    public ContentVO contentInfo(ContentVO item) {
        return ContentDAO.contentInfo(item);
    }

    /**
     * 콘텐츠 장르 리스트
     *
     * @param ContentVO
     * @return 콘텐츠 장르 정보
     */
    @Override
    public List<ContentVO> contentsGenreList(ContentVO data) {
        return ContentDAO.contentsGenreList(data);
    }

    /**
     * 콘텐츠 서비스 리스트
     *
     * @param ContentVO
     * @return 콘텐츠 서비스 리스트 정보
     */
    @Override
    public List<ContentVO> contentServiceList(ContentVO data) {
        return ContentDAO.contentServiceList(data);
    }

    /**
     * 카테고리 정보 등록
     *
     * @param ContentsVO
     * @return 카테고리 정보 등록 성공 여부
     */
    @Override
    public int categoryInsert(ContentVO data) {
        return ContentDAO.categoryInsert(data);
    }

    /**
     * 콘텐츠 첫번째 카테고리 리스트
     *
     * @param ContentVO
     * @return 콘텐츠 첫번째 카테고리 리스트 정보
     */
    @Override
    public List<ContentVO> firstCtgList(ContentVO data) {
        return ContentDAO.firstCtgList(data);
    }

    /**
     * 콘텐츠 카테고리 리스트
     *
     * @param ContentVO
     * @return 콘텐츠 카테고리 리스트 정보
     */
    @Override
    public List<ContentVO> contsCtgList(ContentVO data) {
        return ContentDAO.contsCtgList(data);
    }

    /**
     * 카테고리 정보 수정
     *
     * @param ContentsVO
     * @return 카테고리 정보 수정 성공 여부
     */
    @Override
    public int categoryUpdate(ContentVO data) {
        return ContentDAO.categoryUpdate(data);
    }

    /**
     * 카테고리 정보 삭제
     *
     * @param ContentsVO
     * @return 카테고리 정보 삭제 성공 여부
     */
    @Override
    public int categoryDelete(ContentVO data) {
        return ContentDAO.categoryDelete(data);
    }

    /**
     * 카테고리 정보 삭제 가능 여부
     *
     * @param ContentsVO
     * @return 카테고리 정보 삭제 성공 여부
     */
    @Override
    public boolean canDeleteFirstCategory(ContentVO data) {
        return ContentDAO.canDeleteFirstCategory(data);
    }

    /**
     * 카테고리 정보 중복 체크
     *
     * @param ContentsVO
     * @return 카테고리 정보 중복 여부
     */
    @Override
    public int categoryDuplicationInfo(ContentVO data) {
        return ContentDAO.categoryDuplicationInfo(data);
    }

    /**
     * 기존에 등록되어 있는 두번째 카테고리 정보
     *
     * @param ContentsVO
     * @return 조회 결과
     */
    @Override
    public ContentVO selectExistSecondCategory(ContentVO data) {
        return ContentDAO.selectExistSecondCategory(data);
    }

    /**
     * 콘텐츠 등록
     *
     * @param ContentVO
     * @return 콘텐츠 등록 성공 여부
     */
    @Override
    public int contentsInsert(ContentVO data) {
        int res = ContentDAO.contentsInsert(data);
        return res;
    }

    /**
     * 카테고리 값 (개수) 업데이트
     *
     * @param ContentVO
     * @return 카테고리 값 (개수) 업데이트 성공 여부
     */
    @Override
    public int contsCtgValueUpdate(ContentVO data) {
        int res = ContentDAO.contsCtgValueUpdate(data);
        return res;
    }

    /**
     * 콘텐츠 장르 정보 등록
     *
     * @param ContentVO
     * @return 콘텐츠 장르 정보 등록 성공 여부
     */
    @Override
    public int contentsGenreInsert(ContentVO data) {
        return ContentDAO.contentsGenreInsert(data);
    }

    /**
     * 콘텐츠 서비스 값 등록
     *
     * @param ContentVO
     * @return 콘텐츠 서비스 값 등록 성공 여부
     */
    @Override
    public int contentsServiceInsert(ContentVO data) {
        return ContentDAO.contentsServiceInsert(data);
    }

    /**
     * 콘텐츠 상태 값 변경
     *
     * @param ContentVO
     * @return 콘텐츠 상태 값 변경 성공 여부
     */
    @Override
    public int contentsSttusUpdate(ContentVO data) {
        return ContentDAO.contentsSttusUpdate(data);
    }

    /**
     * 콘텐츠 갤러리 이미지 목록
     *
     * @param ContentVO
     * @return 콘텐츠 갤러리 이미지 리스트
     */
    @Override
    public List<ContentVO> contentsThumbnailList(ContentVO data) {
        return ContentDAO.contentsThumbnailList(data);
    }

    /**
     * 콘텐츠 비디오 파일 목록
     *
     * @param ContentVO
     * @return 콘텐츠 비디오 파일 리스트
     */
    @Override
    public List<ContentVO> contentsVideoList(ContentVO data) {
        return ContentDAO.contentsVideoList(data);
    }

    /**
     * 콘텐츠 실행 파일 목록
     *
     * @param ContentVO
     * @return 콘텐츠 실행 파일 리스트
     */
    @Override
    public List<ContentVO> fileDataInfo(ContentVO data) {
        return ContentDAO.fileDataInfo(data);
    }

    /**
     * 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보
     *
     * @param ContentVO
     * @return 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보
     */
    @Override
    public ContentVO contsMetaInfo(ContentVO data) {
        return ContentDAO.contsMetaInfo(data);
    }

    /**
     * 콘텐츠 비디오 경로에 맞는 xml 파일 정보
     *
     * @param ContentVO
     * @return 콘텐츠 비디오 경로에 맞는 xml 파일 정보
     */
    @Override
    public ContentVO metadataXmlInfo(ContentVO data) {
        return ContentDAO.metadataXmlInfo(data);
    }

    /**
     * 콘텐츠 커버 이미지 파일 정보
     *
     * @param ContentVO
     * @return 콘텐츠 커버 이미지 파일 정보
     */
    @Override
    public ContentVO contentCoverImageInfo(ContentVO data) {
        return ContentDAO.contentCoverImageInfo(data);
    }

    /**
     * 카테고리 값 변경
     *
     * @param ContentVO
     * @return 카테고리 값 변경 성공 여부
     */
    @Override
    public int categoryValueUpdate(ContentVO data) {
        return ContentDAO.categoryValueUpdate(data);
    }

    /**
     * 파일 SEQ로 파일 정보 조회
     *
     * @param ContentsVO
     * @return 파일 정보 출력
     */
    @Override
    public ContentVO fileInfo(ContentVO data) {
        return ContentDAO.fileInfo(data);
    }

    /**
     * fileSeq를 이용한 파일 삭제
     *
     * @param ContentsVO
     * @return 파일 삭제 성공 여부
     */
    @Override
    public int thumbnailDelete(ContentVO data) {
        return ContentDAO.thumbnailDelete(data);
    }

    /**
     * 컨텐츠 정보 수정
     *
     * @param ContentsVO
     * @return 컨텐츠 정보 수정 성공 여부
     */
    @Override
    public int contentsUpdate(ContentVO data) {
        return ContentDAO.contentsUpdate(data);
    }

    /**
     * 컨텐츠 정보 삭제
     *
     * @param ContentsVO
     * @return 컨텐츠 정보 삭제 성공 여부
     */
    @Override
    public int contentsDelete(ContentVO data) {
        return ContentDAO.contentsDelete(data);
    }

    /**
     * 콘텐츠 장르 정보 삭제(exist)
     *
     * @param ContentsVO
     * @return 콘텐츠 장르 정보 삭제(exist) 성공 여부
     */
    @Override
    public int contentsGenreDelete(ContentVO data) {
        return ContentDAO.contentsGenreDelete(data);
    }

    /**
     * 콘텐츠 서비스 정보 삭제(exist)
     *
     * @param ContentsVO
     * @return 콘텐츠 서비스 정보 삭제(exist) 성공 여부
     */
    @Override
    public int contentsServiceDelete(ContentVO data) {
        return ContentDAO.contentsServiceDelete(data);
    }

    /**
     * 콘텐츠 미리보기 파일 목록
     *
     * @param ContentVO
     * @return 콘텐츠 미리보기 파일 리스트
     */
    @Override
    public List<ContentVO> contentsPrevList(ContentVO data) {
        return ContentDAO.contentsPrevList(data);
    }

    /**
     * 검색조건에 해당되는 검수 이력 리스트의 총 개수
     *
     * @param ContentVO
     * @return 검수 이력 리스트 /
     */
    @Override
    public List<ContentVO> verifyRecordList(ContentVO data) {
        return ContentDAO.verifyRecordList(data);
    }

    /**
     * 검색조건에 해당되는 콘텐츠 리스트의 총 개수
     *
     * @param ContentVO
     * @return 총 검수 이력 수 /
     */
    @Override
    public int verifyRecordTotalCount(ContentVO data) {
        int res = ContentDAO.verifyRecordTotalCount(data);
        return res;
    }

    /**
     * 검색조건에 해당되는 전시 관리 목록
     *
     * @param ContentVO
     * @return 전시 목록
     */
    @Override
    public List<ContentVO> contentDisplayList(ContentVO data) {
        return ContentDAO.contentDisplayList(data);
    }

    /**
     * 검색조건에 해당되는 전시 목록의 총 개수
     *
     * @param ContentVO
     * @return 전시 목록 총 개수
     */
    @Override
    public int contentDisplayListTotalCount(ContentVO data) {
        int res = ContentDAO.contentDisplayListTotalCount(data);
        return res;
    }

    /**
     * 콘텐츠를 전시 상태 변경
     *
     * @param ContentVO
     * @return int(result)
     */
    @Override
    public int updateContentDispSttus(ContentVO data) {
        return ContentDAO.updateContentDispSttus(data);
    }

    /**
     * 검색조건에 해당되는 전시 이력 목록
     *
     * @param ContentVO
     * @return 전시 이력 목록
     */
    @Override
    public List<ContentVO> contentDisplayHistoryList(ContentVO data) {
        return ContentDAO.contentDisplayHistoryList(data);
    }

    /**
     * 검색조건에 해당되는 전시 이력의 총 개수
     *
     * @param ContentVO
     * @return 전시 이력 목록 총 개수
     */
    @Override
    public int contentDisplayHistoryListTotalCount(ContentVO data) {
        int res = ContentDAO.contentDisplayHistoryListTotalCount(data);
        return res;
    }

    /**
     * 전시완료 상태의 콘텐츠 목록
     *
     * @param ContentVO
     * @return 전시완료 상태의 콘텐츠 목록
     */
    @Override
    public List<ContentVO> contentDisplayOnList(ContentVO data) {
        return ContentDAO.contentDisplayOnList(data);
    }

    /**
     * 추천 콘텐츠 목록
     *
     * @param ContentVO
     * @return 추천 콘텐츠 목록
     */
    @Override
    public List<ContentVO> recommendContentList(ContentVO data) {
        return ContentDAO.recommendContentList(data);
    }

    /**
     * 전시완료 상태의 콘텐츠 총 개수
     *
     * @param ContentVO
     * @return 전시완료 상태의 콘텐츠 총 개수
     */
    @Override
    public int contentDisplayOnListTotalCount(ContentVO data) {
        int res = ContentDAO.contentDisplayOnListTotalCount(data);
        return res;
    }

    /**
     * 추천 콘텐츠 추가
     *
     * @param ContentVO
     * @return
     */
    @Override
    public void insertRecommendContents(ContentVO data) {
        ContentDAO.insertRecommendContents(data);
    }

    /**
     * 추천 콘텐츠 수정
     *
     * @param ContentVO
     * @return
     */
    @Override
    public void updateRecommendContents(ContentVO data) {
        ContentDAO.updateRecommendContents(data);
    }

    /**
     * CP사 추천 콘텐츠 조회
     *
     * @param ContentVO
     * @return 조회 결과
     */
    @Override
    public List<ContentVO> searchCpRcmdContentList(ContentVO data) {
        return ContentDAO.searchCpRcmdContentList(data);
    }

    /**
     * 추천 콘텐츠 삭제(DEL_YN 컬럼 값 업데이트로 삭제 유무 판단)
     *
     * @param ContentVO
     * @return
     */
    @Override
    public int deleteRecommendContents(ContentVO data) {
        return ContentDAO.deleteRecommendContents(data);
    }

    /**
     * 공개기간 종료 콘텐츠 목록 조회
     *
     * @param ContentVO
     * @return 콘텐츠 목록
     */
    @Override
    public List<ContentVO> searchFinishContractContents(ContentVO data) {
        return ContentDAO.searchFinishContractContents(data);
    }

    /**
     * 전시완료 상태 및 계약기간이 유효한 콘텐츠인지 확인
     *
     * @param ContentVO
     * @return 조회 결과
     */
    @Override
    public int searchDispOnContents(ContentVO data) {
        return ContentDAO.searchDispOnContents(data);
    }

    /**
     * 검색조건에 해당되는 매장 콘텐츠 리스트 정보
     *
     * @param ContentVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ContentVO> storContentsList(ContentVO data) {
        return ContentDAO.storContentsList(data);
    }

    /**
     * 검색조건에 해당되는 매장 콘텐츠 리스트의 총 개수
     *
     * @param ContentVO
     * @return 총 매장 콘텐츠 수 /
     */
    @Override
    public int storContentsListTotalCount(ContentVO data) {
        return ContentDAO.storContentsListTotalCount(data);
    }

    /**
     * 편성 그룹 상세 정보
     *
     * @param ReleaseVO
     * @return 편성 그룹 상세 정보
     */
    @Override
    public ContentVO mmsReleaseInfo(ContentVO data) {
        return ContentDAO.mmsReleaseInfo(data);
    }

    /**
     * 편성 그룹 콘텐츠 목록
     *
     * @param ReleaseVO
     * @return 편성 그룹 콘텐츠 목록 조회 결과
     */
    @Override
    public List<ReleaseVO> mmsReleaseContsList(ContentVO data) {
        return ContentDAO.mmsReleaseContsList(data);
    }

    /**
     * 멤버의 서비스 번호로 접근 허용이 가능한 콘텐츠인지 확인
     *
     * @param ContentVO
     * @return 결과 값 (int)
     */
    @Override
    public int searchAllowContentsInfo(ContentVO data) {
        return ContentDAO.searchAllowContentsInfo(data);
    }

    /**
     * 콘텐츠 정보 조회 (조건&조인 없이)
     *
     * @param ContentVO
     * @return 결과 값 (int)
     */
    @Override
    public ContentVO contentCdtNotInfo(ContentVO data) {
        return ContentDAO.contentCdtNotInfo(data);
    }

    /**
     * 콘텐츠 정보 리스트 조회 (조건&조인 없이)
     *
     * @param ContentVO
     * @return 결과 값 (int)
     */
    @Override
    public List<ContentVO> contentCdtNotInfoList(ContentVO data) {
        return ContentDAO.contentCdtNotInfoList(data);
    }

    /**
     * 전시 콘텐츠 목록 (for LiveOn360)
     *
     * @param ContentVO
     * @return 추천 콘텐츠 목록
     */
    @Override
    public List<ContentVO> contentsDispList(ContentVO data) {
        return ContentDAO.contentsDispList(data);
    }

    /**
     * 전시 콘텐츠 목록 개수 (for LiveOn360)
     *
     * @param ContentVO
     * @return 결과 값 (int)
     */
    @Override
    public int contentsDispListTotalCount(ContentVO data) {
        return ContentDAO.contentsDispListTotalCount(data);
    }

    /**
     * 서브 메타 데이터 세부 항목 명 조회
     *
     * @param ContentVO
     * @return 세부 항목 명
     */
    @Override
    public String getSubMetaCodeNm(ContentVO data) {
        return ContentDAO.getSubMetaCodeNm(data);
    }

    /**
     * 시청내역 콘텐츠 목록 (for LiveOn360)
     *
     * @param ContentVO
     * @return 추천 콘텐츠 목록
     */
    @Override
    public List<ContentVO> selectMyContentList(ContentVO data) {
        return ContentDAO.selectMyContentList(data);
    }

    /**
     * 시청내역 콘텐츠 목록 개수 (for LiveOn360)
     *
     * @param ContentVO
     * @return 결과 값 (int)
     */
    @Override
    public int selectMyContentListTotalCount(ContentVO data) {
        return ContentDAO.selectMyContentListTotalCount(data);
    }

    /**
     * 시청내역 콘텐츠 재생 가능 여부 조회 (for LiveOn360)
     *
     * @param ContentVO
     * @return 결과 값 (int)
     */
    @Override
    public int selectCanPlayContents(ContentVO data) {
        return ContentDAO.selectCanPlayContents(data);
    }

    /**
     * 콘텐츠 검수 상태 강제 승인완료 (for local LiveOn360 Webtoon)
     *
     * @param ContentsVO
     * @return 강제 승인완료 변경 결과
     */
    @Override
    public int forceUpdateContsApproveForLocal(ContentVO data) {
        return ContentDAO.forceUpdateContsApproveForLocal(data);
    }
}
