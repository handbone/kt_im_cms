/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.content.web;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ConfigProperty;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.content.service.ContentService;
import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.device.service.DeviceService;
import kt.com.im.cms.device.vo.DeviceVO;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.release.vo.ReleaseVO;

/**
 *
 * 콘텐츠에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 7.   A2TEC      최초생성
 * 2018. 5. 10.  A2TEC      /api/contents (POST,GET,PUT)추가
 * 2018. 5. 10.  A2TEC      /api/contentsCategory (POST,GET,PUT)추가
 * 2018. 5. 14.  A2TEC      콘텐츠 수정관련 추가
 * 2018. 5. 15.  A2TEC      콘텐츠 삭제관련 추가
 * 2018. 5. 21.  A2TEC      콘텐츠 미리보기 파일 리스트 메소드 추가
 * 2018. 5. 29.  A2TEC      /api/contents verify (검수) 관련 추가
 * 2018. 5. 29.  A2TEC      /api/verify 추가
 * 2018. 6. 22.  A2TEC      /mms/contents 추가
 *
 *      </pre>
 */

@Controller
public class ContentApi {
    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "DeviceService")
    private DeviceService deviceService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Inject
    private FileSystemResource fsResource;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private ConfigProperty configProperty;

    @Autowired
    MessageSource messageSource;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * 콘텐츠 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contents")
    public ModelAndView contentsProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        boolean otherConnect = false;
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storCode");
            if (checkString.equals("")) {
                userVO = new MemberVO();
                rsModel.setViewName("../resources/api/contents/contentsProcess");
                String mbrTokn = CommonFnc.emptyCheckString("mbrTokn", request);
                String storCode = CommonFnc.emptyCheckString("storCode", request);
                String encodeToken = "";
                encodeToken = mbrTokn.replaceAll(" ", "+");

                userVO.setStorCode(storCode);
                userVO.setMbrTokn(encodeToken);

                int tokenSearchResult = 0;
                tokenSearchResult = memberService.searchMemberToken(userVO);

                if (tokenSearchResult == 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage("Not Token Info");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }
                otherConnect = true;
            } else {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        rsModel.setViewName("../resources/api/contents/contentsProcess");
        ContentVO item = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                boolean isManageAllowMember = this.isManageAllowMember(userVO);
                if (userVO == null || !isManageAllowMember) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                String sttus = request.getParameter("sttus") == null ? "" : request.getParameter("sttus");

                String checkString = CommonFnc.requiredChecking(request,
                        "contsSeq,contsCtgSeq,contsTitle,contsSubTitle,contsDesc,contsID,maxAvlNop");
                if (!sttus.equals("")) {
                    String rcessWhySbst = request.getParameter("rcessWhySbst") == null ? ""
                            : request.getParameter("rcessWhySbst");
                    item.setContsSeq(Integer.parseInt(request.getParameter("contsSeq")));
                    item.setSttusVal(sttus);
                    item.setRcessWhySbst(rcessWhySbst);
                    item.setSttusChgrId((String) session.getAttribute("id"));
                    int result = contentService.contentsSttusUpdate(item);
                    if (result == 1) {
                        // 전시완료 상태에서 승인완료로 변경될 경우 해당 콘텐츠가 추천 콘텐츠에 포함되어 있는지 확인하여 이를 비활성화 처리함.
                        String prevSttus = request.getParameter("prevSttus") == null ? ""
                                : request.getParameter("prevSttus");
                        if (prevSttus.equals("06")) {
                            ContentVO contVO = new ContentVO();
                            List<ContentVO> resultItem = contentService.recommendContentList(contVO);
                            List<Integer> delRcmdContsSeqList = new ArrayList<Integer>();
                            if (!resultItem.isEmpty()) {
                                int contsSeq = Integer.parseInt(request.getParameter("contsSeq"));
                                for (int j = 0; j < resultItem.size(); j++) {
                                    int rcmdContsSeq = resultItem.get(j).getContsSeq();
                                    if (contsSeq == rcmdContsSeq) {
                                        delRcmdContsSeqList.add(contsSeq);
                                    }
                                }
                            }

                            if (delRcmdContsSeqList.size() > 0) {
                                contVO = new ContentVO();
                                contVO.setAmdrID(userVO.getMbrId());
                                contVO.setDelRcmdContsSeqList(delRcmdContsSeqList);
                                contentService.deleteRecommendContents(contVO);
                            }
                        }

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    }
                    return rsModel.getModelAndView();
                } else if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                item.setContsSeq(Integer.parseInt(request.getParameter("contsSeq")));
                item.setMbrID((String) session.getAttribute("id"));
                item.setCretrID((String) session.getAttribute("id"));
                item.setContsCtgSeq(Integer.parseInt(request.getParameter("contsCtgSeq")));
                item.setContsTitle(request.getParameter("contsTitle"));
                item.setContsSubTitle(request.getParameter("contsSubTitle"));
                item.setContsDesc(request.getParameter("contsDesc"));
                item.setMaxAvlNop(Integer.parseInt(request.getParameter("maxAvlNop")));
                item.setCntrctStDt(request.getParameter("cntrctStDt"));
                item.setCntrctFnsDt(request.getParameter("cntrctFnsDt"));

                String fileType = request.getParameter("fileType") == null ? "" : request.getParameter("fileType");
                String exeFilePath = request.getParameter("exeFilePath") == null ? ""
                        : request.getParameter("exeFilePath");
                String contsID = request.getParameter("contsID") == null ? "" : request.getParameter("contsID");
                String versionModify = request.getParameter("versionModify") == null ? ""
                        : request.getParameter("versionModify");

                int cpSeq = request.getParameter("cpSeq") == null ? 0 : Integer.parseInt(request.getParameter("cpSeq"));

                item.setFileType(fileType);
                item.setExeFilePath(exeFilePath);
                item.setContsID(contsID);
                item.setFileType(fileType);
                item.setCpSeq(cpSeq);
                item.setVersionModify(versionModify);

                // 공통 코드 예외 처리
                item.setSttusVal("");

                String genres = request.getParameter("genre") == null ? "" : request.getParameter("genre");
                String services = request.getParameter("service") == null ? "" : request.getParameter("service");
                String[] genre = genres.split(",");
                String[] service = services.split(",");

                if (contsID != null) {
                    item.setContsID(contsID);
                }

                int result = 0;
                List<String> deleteFilePath = null;
                try {
                    String dThumbnailSeqs = request.getParameter("dThumbnailSeqs") == null ? ""
                            : request.getParameter("dThumbnailSeqs");
                    deleteFilePath = new ArrayList<String>();

                    if (dThumbnailSeqs != "") {
                        String[] dThumbnailSeq = dThumbnailSeqs.split(",");

                        for (int i = 0; i < dThumbnailSeq.length; i++) {
                            item.setFileSeq(Integer.parseInt(dThumbnailSeq[i]));
                            ContentVO resData = contentService.fileInfo(item);
                            String filePath = resData.getFilePath();
                            if (contentService.thumbnailDelete(item) == 1) {
                                deleteFilePath.add(filePath);
                                // video에 속한 metadata파일 삭제
                                if (filePath.contains("video")) {
                                    ContentVO metaItem = new ContentVO();
                                    metaItem.setFilePath(filePath);
                                    ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                                    if (metadataInfo != null) {
                                        if (contentService.thumbnailDelete(metadataInfo) == 1) {
                                            deleteFilePath.add(metadataInfo.getFilePath());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    result = contentService.contentsUpdate(item);

                    if (result == 1) {
                        List<Integer> list = new ArrayList<Integer>();
                        for (int i = 0; i < genre.length; i++) {
                            list.add(Integer.parseInt(genre[i]));
                        }
                        item.setList(list);

                        contentService.contentsGenreInsert(item);
                        contentService.contentsGenreDelete(item);

                        list = new ArrayList<Integer>();

                        for (int i = 0; i < service.length; i++) {
                            list.add(Integer.parseInt(service[i]));
                        }
                        item.setList(list);

                        contentService.contentsServiceInsert(item);
                        contentService.contentsServiceDelete(item);

                        for (int i = 0; i < deleteFilePath.size(); i++) {
                            File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                            f.delete();
                        }

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    }
                    rsModel.setResultType("contentsUpdate");
                } catch (Exception e) {

                } finally {
                }

                if (result == 1) {
                    for (int i = 0; i < deleteFilePath.size(); i++) {
                        File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                        f.delete();
                    }
                }
                rsModel.setResultType("contentsUpdate");
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            }
        } else { // get
            int contsSeq = request.getParameter("contsSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("contsSeq"));

            /** 리스트 검색 콘텐츠 상태 값 번호 */
            String sttusSeq = CommonFnc.emptyCheckString("sttus", request);

            /** 리스트 CP사 번호 */
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);

            String delYn = CommonFnc.emptyCheckString("delYn", request);

            String cntrctFnsDt = CommonFnc.emptyCheckString("cntrctFnsDt", request);

            /** 제외 코드값 */
            String remoteCodes = CommonFnc.emptyCheckString("remoteCode", request);

            String[] remoteCode = remoteCodes.split(",");

            List<String> list = new ArrayList<String>();
            if (remoteCode.length == 1) {
                if (remoteCode[0] != "") {
                    for (int i = 0; i < remoteCode.length; i++) {
                        list.add(remoteCode[i]);
                    }
                }
            } else {
                for (int i = 0; i < remoteCode.length; i++) {
                    list.add(remoteCode[i]);
                }
            }
            item.setList(list);
            item.setDelYn(delYn);
            item.setCpSeq(cpSeq);
            item.setSvcSeq(svcSeq);
            item.setSttusVal(sttusSeq);
            item.setCntrctFnsDt(cntrctFnsDt);

            if (list.isEmpty()) {
                item.setList(null);
            }

            if (contsSeq != 0) {
                if (otherConnect) {
                    item.setSttusVal("05");
                }

                item.setContsSeq(contsSeq);
                /** 해당 콘텐츠 장르 리스트 */
                List<ContentVO> genreList = contentService.contentsGenreList(item);

                /** 해당 콘텐츠 상세정보 */
                ContentVO resultItem = contentService.contentInfo(item);

                /** 해당 콘텐츠 서비스 리스트 */
                ContentVO coverImg = contentService.contentCoverImageInfo(item);

                /** 해당 콘텐츠 서비스 리스트 */
                List<ContentVO> serviceList = contentService.contentServiceList(item);

                /** 해당 콘텐츠 갤러리 이미지 리스트 */
                List<ContentVO> thumbnailList = contentService.contentsThumbnailList(item);

                /** 해당 콘텐츠 비디오 파일 리스트 */
                List<ContentVO> videoList = contentService.contentsVideoList(item);

                /** 해당 콘텐츠 실행 파일 리스트 */
                List<ContentVO> fileList = contentService.fileDataInfo(item);

                /** 미리보기 영상 파일 리스트 */
                List<ContentVO> prevList = contentService.contentsPrevList(item);

                for (ContentVO videoItem : videoList) {
                    ContentVO metaItem = new ContentVO();
                    metaItem.setContsSeq(contsSeq);
                    metaItem.setFilePath(videoItem.getFilePath());
                    ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                    ContentVO contsMetaInfo = contentService.contsMetaInfo(metaItem);
                    videoItem.setMetadataXmlInfo(metadataInfo);
                    videoItem.setContsXmlInfo(contsMetaInfo);
                }

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    // 서비스 관리자, 매장 관리자는 소속된 서비스의 콘텐츠에만 접근 가능
                    if (!otherConnect) {
                        if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                                || userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)) {
                            ContentVO vo = new ContentVO();
                            vo.setSvcSeq(userVO.getSvcSeq());
                            vo.setContsSeq(resultItem.getContsSeq());

                            int allowResult = contentService.searchAllowContentsInfo(vo);
                            if (allowResult == 0) {
                                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                                return rsModel.getModelAndView();
                            }
                        }
                    }

                    // 전시페이지 관리에서 콘텐츠 상세정보 요청 시에는 상태값이 승인완료 또는 전시완료 일 경우에만 접근 가능
                    String reqType = CommonFnc.emptyCheckString("reqType", request);
                    if (reqType.equals("display") && !resultItem.getSttusVal().equals("05")
                            && !resultItem.getSttusVal().equals("06")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage("Bad Request");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }

                    /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                    resultItem.setMbrNm(CommonFnc.getNameMask(resultItem.getMbrNm()));
                    resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                    resultItem.setAmdrID(CommonFnc.getIdMask(resultItem.getAmdrID()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("genreList", genreList);
                    rsModel.setData("serviceList", serviceList);
                    rsModel.setData("videoList", videoList);
                    rsModel.setData("thumbnailList", thumbnailList);
                    rsModel.setData("fileList", fileList);
                    rsModel.setData("prevList", prevList);
                    rsModel.setData("coverImg", coverImg);

                }
                rsModel.setResultType("contentsInfo");
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (contsSeq == 0 && CommonFnc.checkReqParameter(request, "contsSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                /** 검색여부 */
                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");

                /** 검수여부 */
                String verify = CommonFnc.emptyCheckString("verify", request);

                item.setVerify(verify);

                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    /** 검색 카테고리 */
                    String searchField = request.getParameter("searchField") == null ? ""
                            : request.getParameter("searchField");

                    /** 검색어 */
                    String searchString = request.getParameter("searchString") == null ? ""
                            : request.getParameter("searchString");

                    if (searchField == "CRETR_ID" || searchField.equals("CRETR_ID")) {
                        searchField = "A." + searchField;
                    }

                    item.settarget(searchField);
                    item.setkeyword(searchString);

                }

                /** 리스트 콘텐츠 카테고리 번호 */
                int contsCtgSeq = request.getParameter("contsCtgSeq") == null ? 0
                        : Integer.parseInt(request.getParameter("contsCtgSeq"));
                item.setContsCtgSeq(contsCtgSeq);

                /** 장르 번호 */
                int genreSeq = request.getParameter("genreSeq") == null ? 0
                        : Integer.parseInt(request.getParameter("genreSeq"));
                item.setGenreSeq(genreSeq);

                /** 현재 페이지 */
                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page"));

                /** 검색에 출력될 개수 */
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

                /** 정렬할 필드 */
                String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                /** 정렬 방법 */
                String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                if (otherConnect) {
                    limit = 10000;
                    currentPage = 1;
                    sttusSeq = "05";
                }

                /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                int page = (currentPage - 1) * limit + 1;

                /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                int pageEnd = (currentPage - 1) * limit + limit;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정

                item.setVerify(verify);

                if (sidx.equals("CONTS_ID")) {
                } else if (sidx.equals("CONTS_TITLE")) {
                } else if (sidx.equals("CP_NM")) {
                } else if (sidx.equals("CTG_NM")) {
                } else if (sidx.equals("CRETR_ID")) {
                } else if (sidx.equals("STTUS")) {
                    sidx = "COMN_CD_VALUE";
                } else if (sidx.equals("CRET_DT")) {
                } else if (sidx.equals("VRF_RQT_DT")) {
                } else if (sidx.equals("thumbnail")) {
                    sidx = "CONTS_SEQ";
                } else {
                    if (verify.equals("")) {
                        sidx = "CRET_DT";
                    } else {
                        sidx = "AMD_DT";
                        if (verify.equals("result")) {
                            sidx = "VRF_RQT_DT";
                        }
                    }
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setoffset(page);
                item.setlimit(pageEnd);
                item.setSttusVal(sttusSeq);

                /** 로그인 중인 계정 레벨 값 */
                int user_level = session.getAttribute("level") == null ? 0
                        : Integer.parseInt((String) session.getAttribute("level"));

                /** 조회 구분값 - 조회 쿼리가 공통으로 사용되고 있어 구분값 정의 */
                item.setSelectType("contsList");

                // CP사 정보(사용자 로그인 일 경우 해당 멤버가 작성한 콘텐츠만 출력)
                // if (user_level < 5) {
                // cpSeq = session.getAttribute("cpSeq") == null ? 0
                // : Integer.parseInt((String) session.getAttribute("cpSeq"));
                //
                // item.setCpSeq(cpSeq);
                // }
                List<ContentVO> resultItem = contentService.contentsList(item);
                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 정보(이름, 아이디)) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setMbrNm(CommonFnc.getNameMask(resultItem.get(i).getMbrNm()));
                        resultItem.get(i).setMbrID(CommonFnc.getIdMask(resultItem.get(i).getMbrID()));
                    }

                    /** 가져온 리스트 총 개수 */
                    int totalCount = contentService.contentsListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", (totalCount - 1) / limit);
                    rsModel.setResultType("contentsList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 카테고리 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsCategory")
    public ModelAndView contentsCategoryProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isNotAllowMember = this.isNotAllowMember(userVO);
        if (userVO == null || isNotAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ContentVO item = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                // Master 관리자, CMS 관리자만 접근 가능
                boolean isAdministrator = this.isAdministrator(userVO);
                if (!isAdministrator) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                String validateParams = "firstCtgID:{" + Validator.MAX_LENGTH + "=1};" + "firstCtgNm:{"
                        + Validator.MAX_LENGTH + "=50};" + "contCtgNm:{" + Validator.MAX_LENGTH + "=50}";
                String invalidParams = Validator.validate(request, validateParams);
                if (!invalidParams.isEmpty()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                            "invalid parameter(" + invalidParams + ")");
                    return rsModel.getModelAndView();
                }

                if (request.getParameter("secondCtgID") != null || request.getParameter("secondCtgNm") != null) {
                    validateParams = "secondCtgID:{" + Validator.MAX_LENGTH + "=1};" + "secondCtgNm:{"
                            + Validator.MAX_LENGTH + "=50}";
                    invalidParams = Validator.validate(request, validateParams);
                    if (!invalidParams.isEmpty()) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                                "invalid parameter(" + invalidParams + ")");
                        return rsModel.getModelAndView();
                    }
                }

                String firstCategoryId = request.getParameter("firstCtgID") == null ? ""
                        : request.getParameter("firstCtgID");
                String firstCategoryName = request.getParameter("firstCtgNm") == null ? ""
                        : request.getParameter("firstCtgNm");
                String secCategoryId = request.getParameter("secondCtgID") == null ? ""
                        : request.getParameter("secondCtgID");
                String secCategoryName = request.getParameter("secondCtgNm") == null ? ""
                        : request.getParameter("secondCtgNm");
                String contCtgNm = request.getParameter("contCtgNm") == null ? "" : request.getParameter("contCtgNm");

                item.setContCtgNm(contCtgNm);
                item.setFirstCtgID(firstCategoryId);
                item.setFirstCtgNm(firstCategoryName);

                if (!secCategoryId.equals("") && !secCategoryName.equals("")) {
                    item.setSecondCtgID(secCategoryId);
                    item.setSecondCtgNm(secCategoryName);
                }

                // 카테고리명 중복 검사
                ContentVO searchVO = new ContentVO();
                if (!secCategoryId.equals("") && !secCategoryName.equals("")) {
                    if (secCategoryName.equals(contCtgNm)) {
                        return rsModel.getModelAndView();
                    }
                    searchVO.setFirstCtgID(firstCategoryId);
                    searchVO.setFirstCtgNm(firstCategoryName);
                    searchVO.setSecondCtgNm(contCtgNm);

                    // 업데이트 할 하위 카테고리명을 기준으로 기존에 등록된 하위 카테고리가 있는지 여부 확인
                    ContentVO vo = contentService.selectExistSecondCategory(searchVO);
                    if (vo != null) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                        return rsModel.getModelAndView();
                    }
                } else {
                    if (firstCategoryName.equals(contCtgNm)) {
                        return rsModel.getModelAndView();
                    }
                    searchVO.setFirstCtgNm(contCtgNm);
                }

                int cnt = contentService.categoryDuplicationInfo(searchVO);
                if (cnt > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_EXIST_CAT_INFO);
                    return rsModel.getModelAndView();
                }

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

                TransactionStatus status = transactionManager.getTransaction(def);
                try {
                    int result = contentService.categoryUpdate(item);

                    if (result < 1) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    } else {
                        if (!secCategoryId.equals("") && !secCategoryName.equals("")) {
                            DeviceVO ditem = new DeviceVO();
                            ditem.setDevTypeNm(contCtgNm);
                            ditem.setBefDevTypeNm(secCategoryName);
                            result = deviceService.deviceCategoryUpdate(ditem);
                        }
                    }
                    transactionManager.commit(status);
                } catch (Exception e) {
                    transactionManager.rollback(status);
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR,
                            ResultModel.MESSAGE_FAIL_UPDATE);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            } else if (Method.equals("DELETE")) { // delete
                // Master 관리자, CMS 관리자만 접근 가능
                boolean isAdministrator = this.isAdministrator(userVO);
                if (!isAdministrator) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                String validateParams = "contsCtgSeq:{" + Validator.NUMBER + "}";
                String invalidParams = Validator.validate(request, validateParams);
                if (!invalidParams.isEmpty()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                            "invalid parameter(" + invalidParams + ")");
                    return rsModel.getModelAndView();
                }

                int contsCtgSeq = CommonFnc.emptyCheckInt("contsCtgSeq", request);
                item.setContsCtgSeq(contsCtgSeq);
                item.setSidx("CRET_DT");
                item.setSord("desc");
                item.setoffset(1);
                item.setlimit(10);

                // 등록된 콘텐츠가 있는 경우 하위 카테고리 삭제 불가
                int totalCount = contentService.contentsListTotalCount(item);
                if (totalCount > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                    return rsModel.getModelAndView();
                }

                // 상위 카테고리가 삭제 되는 경우 (하위 카테고리가 하나만 있는 경우)에
                // 상위 카테고리에 등록된 장르를 확인하여 등록된 장르가 있는 경우 삭제 불가
                boolean canDeleteFirstCategory = contentService.canDeleteFirstCategory(item);
                if (!canDeleteFirstCategory) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                    return rsModel.getModelAndView();
                }

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

                TransactionStatus status = transactionManager.getTransaction(def);
                try {
                    int result = contentService.categoryDelete(item);
                    if (result != 1) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    }
                    transactionManager.commit(status);
                } catch (Exception e) {
                    transactionManager.rollback(status);
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR,
                            ResultModel.MESSAGE_FAIL_DELETE);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            } else { // post
                // Master 관리자, CMS 관리자만 접근 가능
                boolean isAdministrator = this.isAdministrator(userVO);
                if (!isAdministrator) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                rsModel.setResultType("categoryInsert");

                String validateParams = "firstCtgID:{" + Validator.MAX_LENGTH + "=1};" + "firstCtgNm:{"
                        + Validator.MAX_LENGTH + "=50}";
                String invalidParams = Validator.validate(request, validateParams);
                if (!invalidParams.isEmpty()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                            "invalid parameter(" + invalidParams + ")");
                    return rsModel.getModelAndView();
                }

                if (item.getSecondCtgNm() != null && item.getSecondCtgID() != null) {
                    validateParams = "secondCtgID:{" + Validator.MAX_LENGTH + "=1};" + "secondCtgNm:{"
                            + Validator.MAX_LENGTH + "=50}";
                    invalidParams = Validator.validate(request, validateParams);
                    if (!invalidParams.isEmpty()) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                                "invalid parameter(" + invalidParams + ")");
                        return rsModel.getModelAndView();
                    }
                }

                item.setFirstCtgID(request.getParameter("firstCtgID"));
                item.setFirstCtgNm(request.getParameter("firstCtgNm"));
                item.setSecondCtgID(request.getParameter("secondCtgID"));
                item.setSecondCtgNm(request.getParameter("secondCtgNm"));

                int cnt = contentService.categoryDuplicationInfo(item);
                if (cnt > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_EXIST_CAT_INFO);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                if (item.getSecondCtgNm() != null && item.getSecondCtgID() != null) {
                    ContentVO searchVO = new ContentVO();
                    searchVO.setFirstCtgID(item.getFirstCtgID());
                    searchVO.setFirstCtgNm(item.getFirstCtgNm());
                    searchVO.setSecondCtgNm(item.getSecondCtgNm());

                    // 신규 등록 하위 카테고리명을 기준으로 기존에 등록된 하위 카테고리가 있는지 여부 확인
                    ContentVO vo = contentService.selectExistSecondCategory(searchVO);
                    if (vo != null) {
                        // 상위 카테고리는 달라도 기존 등록된 하위 카테고리가 있는 경우 동일한 ID를 부여
                        if (!item.getSecondCtgID().equals(vo.getSecondCtgID())) {
                            rsModel.setData("secondCtgId", vo.getSecondCtgID());
                            rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                            return rsModel.getModelAndView();
                        }
                    } else {
                        // 상위 카테고리의 종속 여부와 상관없이 전체 하위 카테고리 중 ID가 고유한지 확인
                        searchVO.setSecondCtgNm(null);
                        searchVO.setSecondCtgID(item.getSecondCtgID());

                        vo = contentService.selectExistSecondCategory(searchVO);
                        if (vo != null) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                            rsModel.setResultMessage(ResultModel.MESSAGE_EXIST_CAT_INFO);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                            return rsModel.getModelAndView();
                        }
                    }
                }

                item.setCretrID((String) session.getAttribute("id"));

                String insertType = (item.getSecondCtgID() == null) ? "first" : "second";
                // 첫번째 카테고리 등록시 두번째 카테고리명, ID에 대한 초기화 문구 설정
                if (item.getSecondCtgID() == null)
                    item.setSecondCtgID("1");

                if (item.getSecondCtgNm() == null)
                    item.setSecondCtgNm("basic");

                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

                TransactionStatus status = transactionManager.getTransaction(def);
                try {
                    int result = contentService.categoryInsert(item);
                    if (result != 1) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    } else {
                        rsModel.setData("insertType", insertType);
                        rsModel.setData("contsCtgSeq", item.getContsCtgSeq());
                    }
                    transactionManager.commit(status);
                } catch (Exception e) {
                    transactionManager.rollback(status);
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR,
                            ResultModel.MESSAGE_FAIL_INSERT_CAT);
                } finally {
                    if (!status.isCompleted()) {
                        transactionManager.rollback(status);
                    }
                }
            }
        } else { // get
                 // 콘텐츠 등록, 수정 시에 콘텐츠 카테고리를 그룹화 할 경우
            /*
             * 첫번째 카테고리 리스트를 가져오지 않으면 해당 하는 첫번째 카테고리명을 정적 문자열로 호출해야만 하기 때문에
             * 동적으로 처리하기 위해 첫 번째 리스트를 가져온 다음 해당하는 두 번째 리스트를 가져오게 구성되어 있음
             */

            /** 카테고리 그룹핑 검색 여부 */
            String GroupingYn = request.getParameter("GroupingYN") == null ? "" : request.getParameter("GroupingYN");
            String unique = CommonFnc.emptyCheckString("unique", request);
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

            item.setGroupingYN(GroupingYn);
            item.setUnique(unique);
            item.setStorSeq(storSeq);
            if (GroupingYn.equals("Y") || GroupingYn == "Y") {
                /** 첫번째 카테고리 명 */
                String firstCtgId = request.getParameter("firstCtgID") == null ? ""
                        : request.getParameter("firstCtgID");

                /** 콘텐츠 카테고리 번호 */
                int contsCtgSeq = request.getParameter("contsCtgSeq") == null ? 0
                        : Integer.parseInt(request.getParameter("contsCtgSeq"));

                List<ContentVO> firstCtgList = contentService.firstCtgList(item);
                rsModel.setData("firstCtgList", firstCtgList);

                if (contsCtgSeq != 0) {
                    item.setGroupingYN("N");
                    item.setContsCtgSeq(contsCtgSeq);
                } else {
                    if (firstCtgId == "") {
                        firstCtgId = firstCtgList.get(0).getFirstCtgID();
                    }
                    item.setFirstCtgID(firstCtgId);
                }
            }
            List<ContentVO> resultItem = contentService.contsCtgList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
            }
            if (unique.equals("on")) {
                rsModel.setResultType("contsCtgListNm");
            } else {
                rsModel.setResultType("contsCtgList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 검수 이력 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/verifyRecord")
    public ModelAndView verifyProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isManageAllowMember = this.isManageAllowMember(userVO);
        if (userVO == null || !isManageAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ContentVO item = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get

            /** CP 번호(검색조건) */
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            /** 서비스 번호(검색조건) */
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);

            String verify = CommonFnc.emptyCheckString("verify", request);
            item.setVerify(verify);
            item.setSvcSeq(svcSeq);

            /** 검색여부 */
            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");

            /** 공통 코드 예외 처리 */
            item.setSttusVal("");

            String sttusSeq = CommonFnc.emptyCheckString("sttus", request);

            item.setSearchConfirm(searchConfirm);
            if (searchConfirm.equals("true")) {
                /** 검색 카테고리 */
                String searchField = request.getParameter("searchField") == null ? ""
                        : request.getParameter("searchField");

                /** 검색어 */
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.settarget(searchField);
                item.setkeyword(searchString);
            }

            /** 현재 페이지 */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));

            /** 검색에 출력될 개수 */
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));

            /** 정렬할 필드 */
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

            /** 정렬 방법 */
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
            int page = (currentPage - 1) * limit + 1;

            /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
            int pageEnd = (currentPage - 1) * limit + limit;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
            if (sidx.equals("CONTS_ID")) {
            } else if (sidx.equals("CONTS_TITLE")) {
            } else if (sidx.equals("CTG_NM")) {
            } else if (sidx.equals("CRETR_ID")) {
            } else if (sidx.equals("STTUS")) {
                sidx = "COMN_CD_VALUE";
            } else if (sidx.equals("CRET_DT")) {
            } else {
                sidx = "CRET_DT";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setoffset(page);
            item.setlimit(pageEnd);
            item.setSttusVal(sttusSeq);
            item.setList(null);

            /** 로그인 중인 계정 레벨 값 */
            int user_level = session.getAttribute("level") == null ? 0
                    : Integer.parseInt((String) session.getAttribute("level"));

            // CP사 정보(사용자 로그인 일 경우 해당 멤버가 작성한 콘텐츠만 출력)
            if (user_level < 5) {
                cpSeq = session.getAttribute("cpSeq") == null ? 0
                        : Integer.parseInt((String) session.getAttribute("cpSeq"));

                item.setCpSeq(cpSeq);
            }
            List<ContentVO> resultItem = contentService.verifyRecordList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                /* 개인정보 마스킹 (이름, 아이디) */
                for (int i = 0; i < resultItem.size(); i++) {
                    resultItem.get(i).setMbrNm(CommonFnc.getNameMask(resultItem.get(i).getMbrNm()));
                    resultItem.get(i).setMbrID(CommonFnc.getIdMask(resultItem.get(i).getMbrID()));
                }

                /** 가져온 리스트 총 개수 */
                int totalCount = contentService.verifyRecordTotalCount(item);

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", (totalCount - 1) / limit);
                rsModel.setResultType("contentsList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 전시 관리 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsDisplay")
    public ModelAndView contentsDisplayList(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        // Master 관리자, CMS 관리자만 접근 가능
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAdministrator = this.isAdministrator(userVO);
        if (userVO == null || !isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            String contsSeqList = request.getParameter("contsSeqList") == null ? ""
                    : request.getParameter("contsSeqList");
            String dispOn = request.getParameter("dispOn") == null ? "" : request.getParameter("dispOn");

            if (dispOn == "") {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("Display state is empty");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            String[] dispContsSeqList = contsSeqList.split(",");

            if (dispContsSeqList.length == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }

            item.setAmdrID(userVO.getMbrId());
            item.setSttusChgrId(userVO.getMbrId());
            item.setDispContsSeqList(dispContsSeqList);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = 0;

                if (dispOn.equals("true")) {
                    item.setSttusVal("06"); // 전시완료
                } else {
                    item.setSttusVal("05"); // 승인완료
                }

                result = contentService.updateContentDispSttus(item);

                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);

                    List<Integer> delRcmdContsSeqList = new ArrayList<Integer>();
                    if (dispOn.equals("true")) {
                        rsModel.setData("dispOn", "Y");
                    } else {
                        rsModel.setData("dispOn", "N");

                        // 승인완료 상태로 변경 시 해당 콘텐츠가 추천 콘텐츠로 등록되어 있을 경우 추천 콘텐츠에서도 비활성화 되도록 처리함.
                        ContentVO contVO = new ContentVO();
                        List<ContentVO> resultItem = contentService.recommendContentList(contVO);
                        if (!resultItem.isEmpty()) {
                            for (int i = 0; i < dispContsSeqList.length; i++) {
                                int contsSeq = Integer.parseInt(dispContsSeqList[i]);
                                for (int j = 0; j < resultItem.size(); j++) {
                                    int rcmdContsSeq = resultItem.get(j).getContsSeq();
                                    if (contsSeq == rcmdContsSeq) {
                                        delRcmdContsSeqList.add(contsSeq);
                                    }
                                }
                            }
                        }

                        if (delRcmdContsSeqList.size() > 0) {
                            item.setDelRcmdContsSeqList(delRcmdContsSeqList);
                            contentService.deleteRecommendContents(item);
                        }
                    }
                    rsModel.setResultType("contentDisplyState");
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String list = request.getParameter("list") == null ? "" : request.getParameter("list");
            // 추천 콘텐츠 관리를 위해 전시완료 상태의 콘텐츠 리스트 조회
            if (list.equals("dispOnList")) {
                item.setSttusVal("06");

                List<ContentVO> resultItem = contentService.contentDisplayOnList(item);
                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    int totalCount = contentService.contentDisplayOnListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setResultType("contentDisplayOnList");
                }

                return rsModel.getModelAndView();
            }

            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            item.setSearchConfirm(searchConfirm);
            // 검색 여부가 true일 경우
            if (searchConfirm.equals("true")) {
                String searchField = request.getParameter("searchField") == null ? ""
                        : request.getParameter("searchField");
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.settarget(searchField);
                item.setkeyword(searchString);
            }

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
            if (sidx.equals("CONTS_TITLE")) {
                sidx = "CONTS_TITLE";
            } else if (sidx.equals("CONTS_CTG_NM")) {
                sidx = "CONTS_CTG_NM";
            } else if (sidx.equals("CRETR")) {
                sidx = "CRETR_NM";
            } else if (sidx.equals("STTUS_VAL")) {
                sidx = "STTUS_VAL";
            } else if (sidx.equals("AMD_DT")) {
                sidx = "AMD_DT";
            } else {
                sidx = "CONTS_SEQ";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setoffset(page);
            item.setlimit(pageEnd);

            String sttus = request.getParameter("sttus") == null ? "" : request.getParameter("sttus");
            item.setSttusVal(sttus);

            List<ContentVO> resultItem = contentService.contentDisplayList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (등록자 이름, 아이디) */
                for (int i = 0; i < resultItem.size(); i++) {
                    resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));
                    resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                }

                int totalCount = contentService.contentDisplayListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("contentDisplayList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 전시완료 이력 관리 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsDisplayHistory")
    public ModelAndView contentsDisplayHistoryList(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        // Master 관리자, CMS 관리자만 접근 가능
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAdministrator = this.isAdministrator(userVO);
        if (userVO == null || !isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            item.setSearchConfirm(searchConfirm);
            // 검색 여부가 true일 경우
            if (searchConfirm.equals("true")) {
                String searchField = request.getParameter("searchField") == null ? ""
                        : request.getParameter("searchField");
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.settarget(searchField);
                item.setkeyword(searchString);
            }

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
            if (sidx.equals("CONTS_ID")) {
                sidx = "CONTS_ID";
            } else if (sidx.equals("CONTS_TITLE")) {
                sidx = "CONTS_TITLE";
            } else if (sidx.equals("CRETR")) {
                sidx = "CRETR_NM";
            } else if (sidx.equals("ACTC_STTUS")) {
                sidx = "ACTC_STTUS";
            } else if (sidx.equals("AMD_DT")) {
                sidx = "AMD_DT";
            } else {
                sidx = "CONTS_HST_SEQ";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setoffset(page);
            item.setlimit(pageEnd);

            List<ContentVO> resultItem = contentService.contentDisplayHistoryList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (등록자 이름, 아이디) */
                for (int i = 0; i < resultItem.size(); i++) {
                    resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));
                    resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                }

                int totalCount = contentService.contentDisplayHistoryListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("contentDisplayHistoryList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 추천 콘텐츠 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/recommendContents")
    public ModelAndView recommendContentList(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        // Master 관리자, CMS 관리자만 접근 가능
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAdministrator = this.isAdministrator(userVO);
        if (userVO == null || !isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String rcmdContsSeq = CommonFnc.emptyCheckString("rcmdContsSeq", request);
            String[] rcmdContList = rcmdContsSeq.split(",");

            List<ContentVO> resultItem = contentService.recommendContentList(item);
            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                item.setMbrID(userVO.getMbrId());
                List<Integer> deleteRcmdContsSeqList = null;
                if (rcmdContsSeq.equals("")) { // delete all
                    deleteRcmdContsSeqList = new ArrayList<Integer>();
                    for (int i = 0; i < resultItem.size(); i++) {
                        deleteRcmdContsSeqList.add(resultItem.get(i).getContsSeq());
                    }
                    if (deleteRcmdContsSeqList.size() > 0) {
                        item.setAmdrID(userVO.getMbrId());
                        item.setDelRcmdContsSeqList(deleteRcmdContsSeqList);
                        contentService.deleteRecommendContents(item);
                    }
                } else {
                    // add or update
                    for (int i = 0; i < rcmdContList.length; i++) {
                        int contsSeq = Integer.parseInt(rcmdContList[i]);
                        int rank = i + 1;
                        boolean isExist = false;
                        boolean isUpdate = false;

                        for (int j = 0; j < resultItem.size(); j++) {
                            int curRcmdContsSeq = resultItem.get(j).getContsSeq();
                            int curRcmdRank = resultItem.get(j).getRcmdRank();
                            if (contsSeq == curRcmdContsSeq) {
                                isExist = true;
                                if (rank != curRcmdRank) {
                                    isUpdate = true;
                                }
                                break;
                            }
                        }

                        if (!isExist) {
                            item.setContsSeq(contsSeq);
                            item.setRcmdRank(rank);

                            // 추천 콘텐츠 추가 시 해당 콘텐츠가 전시완료 상태이고 계약기간이 유효한지 확인 후 추가.
                            int result = contentService.searchDispOnContents(item);
                            if (result == 1) {
                                contentService.insertRecommendContents(item);
                            }
                        }

                        if (isUpdate) {
                            item.setContsSeq(contsSeq);
                            item.setRcmdRank(rank);
                            contentService.updateRecommendContents(item);
                        }
                    }

                    // delete (실제 DB에서 삭제하지 않고 RCMD_DEL_YN의 값을 'Y'로 변경하여 이력은 저장
                    deleteRcmdContsSeqList = new ArrayList<Integer>();
                    for (int i = 0; i < resultItem.size(); i++) {
                        int curRcmdContsSeq = resultItem.get(i).getContsSeq();
                        boolean isExist = false;
                        for (int j = 0; j < rcmdContList.length; j++) {
                            int contsSeq = Integer.parseInt(rcmdContList[j]);
                            if (curRcmdContsSeq == contsSeq) {
                                isExist = true;
                                break;
                            }
                        }

                        if (!isExist) {
                            deleteRcmdContsSeqList.add(curRcmdContsSeq);
                        }
                    }
                    if (deleteRcmdContsSeqList.size() > 0) {
                        item.setAmdrID(userVO.getMbrId());
                        item.setDelRcmdContsSeqList(deleteRcmdContsSeqList);
                        contentService.deleteRecommendContents(item);
                    }
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            List<ContentVO> resultItem = contentService.recommendContentList(item);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setResultType("recommendContentList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 매장 콘텐츠 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/mms/contents")
    public ModelAndView mmscontentsProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);

        if (session.getAttribute("id") == null) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/contents/contentsProcess");
            ContentVO item = new ContentVO();
            if (request.getMethod().equals("POST")) {
                String Method = request.getParameter("_method");
                if (Method == null) {
                    Method = "POST";
                }
                if (Method.equals("PUT")) { // put
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else if (Method.equals("DELETE")) { // delete
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else { // post
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                }
            } else { // get
                int contsSeq = request.getParameter("contsSeq") == null ? 0
                        : Integer.parseInt(request.getParameter("contsSeq"));

                /** 리스트 검색 콘텐츠 상태 값 번호 */
                String sttusSeq = CommonFnc.emptyCheckString("sttus", request);

                /** 리스트 CP사 번호 */
                int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);

                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

                int frmtnSeq = CommonFnc.emptyCheckInt("frmtnSeq", request);

                String delYn = CommonFnc.emptyCheckString("delYn", request);

                String cntrctFnsDt = CommonFnc.emptyCheckString("cntrctFnsDt", request);

                String frmtnNm = CommonFnc.emptyCheckString("frmtnNm", request);

                /** 제외 코드값 */
                String remoteCodes = CommonFnc.emptyCheckString("remoteCode", request);

                String[] remoteCode = remoteCodes.split(",");

                List<String> list = new ArrayList<String>();
                for (int i = 0; i < remoteCode.length; i++) {
                    list.add(remoteCode[i]);
                }
                item.setList(list);
                item.setDelYn(delYn);
                item.setCpSeq(cpSeq);
                item.setSvcSeq(svcSeq);
                item.setSttusVal(sttusSeq);
                item.setCntrctFnsDt(cntrctFnsDt);
                item.setStorSeq(storSeq);
                item.setFrmtnNm(frmtnNm);

                if (frmtnSeq != 0 || !frmtnNm.equals("")) {
                    item.setFrmtnSeq(frmtnSeq);

                    /** 편성 그룹 정보 */
                    ContentVO resultItem = contentService.mmsReleaseInfo(item);

                    /** 편성 그룹 콘텐츠 정보 */
                    List<ReleaseVO> contsList = contentService.mmsReleaseContsList(item);

                    if (resultItem == null) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("contsList", contsList);

                    }
                    rsModel.setResultType("releaseInfo");
                    return rsModel.getModelAndView();
                } else {
                    if (contsSeq != 0) {
                        item.setContsSeq(contsSeq);
                        /** 해당 콘텐츠 장르 리스트 */
                        List<ContentVO> genreList = contentService.contentsGenreList(item);

                        /** 해당 콘텐츠 상세정보 */
                        ContentVO resultItem = contentService.contentInfo(item);

                        /** 해당 콘텐츠 서비스 리스트 */
                        ContentVO coverImg = contentService.contentCoverImageInfo(item);

                        /** 해당 콘텐츠 서비스 리스트 */
                        List<ContentVO> serviceList = contentService.contentServiceList(item);

                        /** 해당 콘텐츠 갤러리 이미지 리스트 */
                        List<ContentVO> thumbnailList = contentService.contentsThumbnailList(item);

                        /** 해당 콘텐츠 비디오 파일 리스트 */
                        List<ContentVO> videoList = contentService.contentsVideoList(item);

                        /** 해당 콘텐츠 실행 파일 리스트 */
                        List<ContentVO> fileList = contentService.fileDataInfo(item);

                        /** 미리보기 영상 파일 리스트 */
                        List<ContentVO> prevList = contentService.contentsPrevList(item);

                        for (ContentVO videoItem : videoList) {
                            ContentVO metaItem = new ContentVO();
                            metaItem.setContsSeq(contsSeq);
                            metaItem.setFilePath(videoItem.getFilePath());
                            ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                            ContentVO contsMetaInfo = contentService.contsMetaInfo(metaItem);
                            videoItem.setMetadataXmlInfo(metadataInfo);
                            videoItem.setContsXmlInfo(contsMetaInfo);

                        }
                        if (resultItem == null) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        } else {
                            /* 개인정보 마스킹 (등록자 이름, 아이디) */
                            resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));
                            resultItem.setCretrID(CommonFnc.getIdMask(resultItem.getCretrId()));
                            resultItem.setAmdrID(CommonFnc.getIdMask(resultItem.getAmdrID()));

                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                            rsModel.setData("item", resultItem);
                            rsModel.setData("genreList", genreList);
                            rsModel.setData("serviceList", serviceList);
                            rsModel.setData("videoList", videoList);
                            rsModel.setData("thumbnailList", thumbnailList);
                            rsModel.setData("fileList", fileList);
                            rsModel.setData("prevList", prevList);
                            rsModel.setData("coverImg", coverImg);

                        }
                        rsModel.setResultType("contentsInfo");

                    } else {
                        /** 검색여부 */
                        String searchConfirm = request.getParameter("_search") == null ? "false"
                                : request.getParameter("_search");

                        /** 검수여부 */
                        item.setSearchConfirm(searchConfirm);
                        if (searchConfirm.equals("true")) {
                            /** 검색 카테고리 */
                            String searchField = request.getParameter("searchField") == null ? ""
                                    : request.getParameter("searchField");

                            /** 검색어 */
                            String searchString = request.getParameter("searchString") == null ? ""
                                    : request.getParameter("searchString");

                            if (searchField == "CRETR_ID" || searchField.equals("CRETR_ID")) {
                                searchField = "A." + searchField;
                            }

                            item.settarget(searchField);
                            item.setkeyword(searchString);

                        }

                        /** 리스트 콘텐츠 카테고리 번호 */
                        int contsCtgSeq = request.getParameter("contsCtgSeq") == null ? 0
                                : Integer.parseInt(request.getParameter("contsCtgSeq"));
                        item.setContsCtgSeq(contsCtgSeq);

                        /** 장르 번호 */
                        int genreSeq = request.getParameter("genreSeq") == null ? 0
                                : Integer.parseInt(request.getParameter("genreSeq"));
                        item.setGenreSeq(genreSeq);

                        /** 현재 페이지 */
                        int currentPage = request.getParameter("page") == null ? 1
                                : Integer.parseInt(request.getParameter("page"));

                        /** 검색에 출력될 개수 */
                        int limit = request.getParameter("rows") == null ? 10
                                : Integer.parseInt(request.getParameter("rows"));

                        /** 정렬할 필드 */
                        String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                        /** 정렬 방법 */
                        String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                        /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                        int page = (currentPage - 1) * limit + 1;

                        /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                        int pageEnd = (currentPage - 1) * limit + limit;

                        // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                        if (sidx.equals("CONTS_ID")) {
                        } else if (sidx.equals("CONTS_TITLE")) {
                        } else if (sidx.equals("CTG_NM")) {
                        } else if (sidx.equals("CRETR_ID")) {
                        } else if (sidx.equals("STTUS")) {
                            sidx = "COMN_CD_VALUE";
                        } else if (sidx.equals("CRET_DT")) {
                        } else if (sidx.equals("VRF_RQT_DT")) {
                        } else {
                            sidx = "CRET_DT";
                        }

                        item.setSidx(sidx);
                        item.setSord(sord);
                        item.setoffset(page);
                        item.setlimit(pageEnd);
                        item.setSttusVal(sttusSeq);

                        /** 로그인 중인 계정 레벨 값 */
                        int user_level = session.getAttribute("level") == null ? 0
                                : Integer.parseInt((String) session.getAttribute("level"));

                        // CP사 정보(사용자 로그인 일 경우 해당 멤버가 작성한 콘텐츠만 출력)
                        // if (user_level < 5) {
                        // cpSeq = session.getAttribute("cpSeq") == null ? 0
                        // : Integer.parseInt((String) session.getAttribute("cpSeq"));
                        //
                        // item.setCpSeq(cpSeq);
                        // }

                        List<ContentVO> resultItem = contentService.storContentsList(item);

                        if (resultItem.isEmpty()) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        } else {
                            /* 개인정보 마스킹 (등록자 이름, 아이디, 수정자 아이디) */
                            for (int i = 0; i < resultItem.size(); i++) {
                                resultItem.get(i).setMbrNm(CommonFnc.getNameMask(resultItem.get(i).getMbrNm()));
                                resultItem.get(i).setMbrID(CommonFnc.getIdMask(resultItem.get(i).getMbrID()));
                            }

                            /** 가져온 리스트 총 개수 */
                            int totalCount = contentService.storContentsListTotalCount(item);

                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                            rsModel.setData("item", resultItem);
                            rsModel.setData("totalCount", totalCount);
                            rsModel.setData("currentPage", currentPage);
                            rsModel.setData("totalPage", (totalCount - 1) / limit);
                            rsModel.setResultType("contentsList");
                        }
                    }
                }
            }
        }
        return rsModel.getModelAndView();
    }

    /**
     * 전시 콘텐츠 목록 API (for LiveOn360)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsDispList")
    public ModelAndView contentsDispList(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String firstCtgID = request.getParameter("firstCtgID") == null ? "G" : request.getParameter("firstCtgID");
            item.setFirstCtgID(firstCtgID);

            // searchString은 장르명으로 콘텐츠 검색하기 위한 목적
            String searchString = CommonFnc.emptyCheckString("searchString", request);
            item.setGenreNm(searchString);

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int limit = request.getParameter("rows") == null ? 8 : Integer.parseInt(request.getParameter("rows"));
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

            int page = (currentPage - 1) * limit;
            int pageEnd = page + limit - 1;

            // 정렬 항목 지정
            if (sidx.equals("CONTS_TITLE")) {
                sidx = "CONTS_TITLE";
            } else if (sidx.equals("AMD_DT")) {
                sidx = "AMD_DT";
            }
            item.setSidx(sidx);
            item.setlimit(limit);

            if (currentPage == 1) {
                item.setoffset(0);
            } else {
                item.setoffset(page);
            }

            String sttus = "06"; // 전시완료 상태인 콘텐츠만 노출
            item.setSttusVal(sttus);

            List<ContentVO> resultItem = contentService.contentsDispList(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            } else {
                int totalCount = contentService.contentsDispListTotalCount(item);
                int totalPage = (limit > 0) ? (int) Math.ceil((double) totalCount / limit) : 1;

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalPage);
            }
            rsModel.setResultType("contentsDispList");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 다중 장르 전시 콘텐츠 목록 API (for LiveOn360)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/multiGenreDispContsList")
    public ModelAndView multiGenreDispContsListProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO vo = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (!method.equals("GET")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq,firstCtgID");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            String firstCtgID = CommonFnc.emptyCheckString("firstCtgID", request);
            vo.setFirstCtgID(firstCtgID);

            // request 요청 시 콘텐츠 검수 상태 리스트를 받아옴. 해당 값이 없을 경우와 승인완료 또는 전시완료 콘텐츠를 요청한 것이 아닌 경우 전시완료 콘텐츠 목록만 전달
            String contsSttus = CommonFnc.emptyCheckString("contsSttus", request);
            String[] contsSttusArr = contsSttus.split(",");
            List<String> sttusList = new ArrayList<String>();
            for (int i = 0; i < contsSttusArr.length; i++) {
                // 05: 승인완료, 06: 전시완료
                if (contsSttusArr[i].equals("05") || contsSttusArr[i].equals("06")) {
                    sttusList.add(contsSttusArr[i]);
                }
            }

            vo.setSttusList(sttusList);

            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 8);
            vo.setlimit(limit);

            // CONTS_TITLE일 경우 이름 순 정렬, 이외의 경우 또는 값이 없을 경우 최신 순으로 정렬
            String sidx = CommonFnc.emptyCheckString("sort", request);
            vo.setSidx(sidx);

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int page = (currentPage - 1) * limit;

            if (currentPage == 1) {
                vo.setoffset(0);
            } else {
                vo.setoffset(page);
            }

            String genreList = CommonFnc.emptyCheckString("genreList", request);
            String[] genreArr = genreList.split(",");

            List<Map<Object, Object>> resultList = new ArrayList<Map<Object, Object>>();
            if (genreList.equals("")) {
                Map<Object, Object> mData = new HashMap<Object, Object>();
                List<ContentVO> contList = contentService.contentsDispList(vo);
                if (contList.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }

                int totCnt = contentService.contentsDispListTotalCount(vo);
                int totalPage = (limit > 0) ? (int) Math.ceil((double) totCnt / limit) : 1;

                mData.put("genreName", "All");
                mData.put("contList", contList);
                mData.put("totalCount", totCnt);
                mData.put("currentPage", currentPage);
                mData.put("totalPage", totalPage);

                resultList.add(mData);
            } else {
                int emptyCnt = 0;
                for (int i = 0; i < genreArr.length; i++) {
                    vo.setGenreNm(genreArr[i]);
                    Map<Object, Object> mData = new HashMap<Object, Object>();
                    List<ContentVO> contList = contentService.contentsDispList(vo);
                    int totCnt = contentService.contentsDispListTotalCount(vo);
                    int totalPage = (limit > 0) ? (int) Math.ceil((double) totCnt / limit) : 1;

                    if (totCnt == 0) {
                        emptyCnt++;
                    }

                    mData.put("genreName", genreArr[i]);
                    mData.put("contList", contList);
                    mData.put("totalCount", totCnt);
                    mData.put("currentPage", currentPage);
                    mData.put("totalPage", totalPage);

                    resultList.add(mData);
                }

                if (emptyCnt == genreArr.length) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    return rsModel.getModelAndView();
                }
            }

            rsModel.setData("item", resultList);
            rsModel.setResultType("multiGenreDispContList");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 전시 콘텐츠 검색 API (for LiveOn360)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/searchContentsDispList")
    public ModelAndView searchContentsDispList(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO vo = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (!method.equals("GET")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq,firstCtgID,accRoute");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            String firstCtgID = CommonFnc.emptyCheckString("firstCtgID", request);
            vo.setFirstCtgID(firstCtgID);

            String keyword = CommonFnc.emptyCheckString("keyword", request);
            vo.setkeyword(keyword);

            String accRoute = CommonFnc.emptyCheckString("accRoute", request);
            String monPayStr = messageSource.getMessage("common.monthly.payment", null, Locale.KOREA); // 월정액 문자
            String[] accRouteList = accRoute.split("/");
            String uidSe = "";
            for (int i = 0; i < accRouteList.length; i++) {
                if (accRouteList[i].equals("KT_ID")) {
                    uidSe = "01";
                } else if (accRouteList[i].equals("OTM")) {
                    uidSe = "02";
                } else if (accRouteList[i].equals(monPayStr)){
                    uidSe = "03";
                }
            }

            if (uidSe.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(accRoute)");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            vo.setUidSe(uidSe);
            System.out.println(">> uidSe : " + uidSe);

            // CONTS_TITLE일 경우 이름 순 정렬, 이외의 경우 또는 값이 없을 경우 최신 순으로 정렬
            String sidx = CommonFnc.emptyCheckString("sort", request);
            vo.setSidx(sidx);

            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 8);
            vo.setlimit(limit);

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int page = (currentPage - 1) * limit;
            int pageEnd = page + limit - 1;

            if (currentPage == 1) {
                vo.setoffset(0);
            } else {
                vo.setoffset(page);
            }

            List<ContentVO> resultList = contentService.contentsDispList(vo);

            if (resultList.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }

            int contTotCnt = contentService.contentsDispListTotalCount(vo);
            int totalPage = (limit > 0) ? (int) Math.ceil((double) contTotCnt / limit) : 1;

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", resultList);
            rsModel.setData("totalCount", contTotCnt);
            rsModel.setData("row", limit);
            rsModel.setData("currentPage", currentPage);
            rsModel.setData("totalPage", totalPage);
            rsModel.setResultType("searchResultContentsDispList");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 시청내역 전시 콘텐츠 목록 API (for LiveOn360)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/myPlayContsList")
    public ModelAndView myPlayContsListProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO vo = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq,svcSeq,firstCtgID,uid");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            vo.setSvcSeq(svcSeq);

            String firstCtgID = CommonFnc.emptyCheckString("firstCtgID", request);
            vo.setFirstCtgID(firstCtgID);

            String uid = CommonFnc.emptyCheckString("uid", request);
            vo.setUid(uid);

            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 8);
            vo.setlimit(limit);

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int page = (currentPage - 1) * limit;
            int pageEnd = page + limit - 1;

            if (currentPage == 1) {
                vo.setoffset(0);
            } else {
                vo.setoffset(page);
            }

            List<ContentVO>myPlayContList = contentService.selectMyContentList(vo);

            if (myPlayContList.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }

            int myPlayContListTotCnt = contentService.selectMyContentListTotalCount(vo);
            int totalPage = (limit > 0) ? (int) Math.ceil((double) myPlayContListTotCnt / limit) : 1;

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setData("item", myPlayContList);
            rsModel.setData("totalCount", myPlayContListTotCnt);
            rsModel.setData("row", limit);
            rsModel.setData("currentPage", currentPage);
            rsModel.setData("totalPage", totalPage);
            rsModel.setResultType("myPlayContList");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 재생 가능 콘텐츠 여부 확인 API (for LiveOn360)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/canPlayContents")
    public ModelAndView canPlayContentsProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO vo = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String checkString = CommonFnc.requiredChecking(request, "mbrTokn,storSeq,contsSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            vo.setContsSeq(contsSeq);

            boolean canPlay = (contentService.selectCanPlayContents(vo) > 0);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            if (canPlay) {
                rsModel.setData("canPlay", "Yes");
            } else {
                rsModel.setData("canPlay", "No");
            }
            rsModel.setResultType("resultCanPlayContents");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 검수 상태 강제 승인 완료 변경 API (for Local LiveOn360 Webtoon Contents)
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/forceUpdateContsApprove")
    public ModelAndView forceUpdateContsApprove(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String forceUpdate = configProperty.getProperty("force.update.conts.approve.for.local");

            if (forceUpdate != null && forceUpdate.equals("Y")) {
                DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                TransactionStatus status = transactionManager.getTransaction(def);

                try {
                    int result = contentService.forceUpdateContsApproveForLocal(item);
                    if (result > 0) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        transactionManager.commit(status);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        transactionManager.rollback(status);
                    }
                } catch (Exception e){
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                    transactionManager.rollback(status);
                } finally {
                    if(!status.isCompleted()){
                        transactionManager.rollback(status);
                    }
                }
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    private boolean isManageAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe);
    }

    private boolean isNotAllowMember(MemberVO userVO) {
        if (userVO == null) {
            return true;
        }
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

}