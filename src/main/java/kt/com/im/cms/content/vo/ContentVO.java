/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.content.vo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 2.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class ContentVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8221104658530904385L;

    /** 콘텐츠 번호 */
    int contsSeq;

    /** 콘텐츠 제외 상태 코드 값 */
    String remoteSttus;

    /** 콘텐츠 상태 코드 값 */
    String sttusVal;

    /** 콘텐츠 상태값 명 */
    String sttusNm;

    /** 매장 명 */
    String storNm;

    /** 편성 명 */
    String frmtnNm;

    /** 등록자 아이디 */
    String cretrId;

    /** 편성 번호 */
    int frmtnSeq;

    /** 콘텐츠 버전 */
    int contsVer;

    /** 매장 Device 종류별 개수 */
    int storDevCnt;

    /** 카테고리 유니크 검색 여부 */
    String unique;

    /** 매장 번호 */
    int storSeq;

    /** 콘텐츠 실행 경로 */
    String exeFilePath;

    /** 콘텐츠 이용가능 최대 인원 */
    int maxAvlNop;

    /** 콘텐츠 아이디 */
    private String cretrID;

    /** 콘텐츠 검수 상태 변경 날짜 */
    private String vrfRqtDt;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 콘텐츠 파일 타입 */
    private String fileType;

    /** 콘텐츠 버전 수정 확인 변수 */
    private String versionModify;

    /** 메타 파일 경로 */
    private String mtdPath;

    /** 콘텐츠 설명 */
    private String contsDesc;

    /** 콘텐츠 부제 */
    private String contsSubTitle;

    /** 콘텐츠 전시기간(시작) */
    private String cntrctStDt;

    /** 콘텐츠 전시기간(종료) */
    private String cntrctFnsDt;

    /** 콘텐츠 카테고리 명(첫번째) */
    private String firstCtgNm;

    /** 콘텐츠 카테고리 명(두번째) */
    private String secondCtgNm;

    /** 콘텐츠 카테고리 아이디(첫번째) */
    private String firstCtgID;

    /** 콘텐츠 카테고리 아이디(두번째) */
    private String secondCtgID;

    /** 콘텐츠 카테고리 아이디(두번째) */
    private String GroupingYN;

    /** 콘텐츠 카테고리 번호 */
    private int contsCtgSeq;

    /** 콘텐츠 카테고리에 해당되는 콘텐츠 개수 */
    private int ctgCnt;

    /** 콘텐츠 반려 사유 */
    private String rcessWhySbst;

    /** 콘텐츠 삭제 여부 */
    private String delYn;

    /** CP사 번호 */
    int cpSeq;

    /** File 번호 */
    private int fileSeq;

    /** File 경로 */
    private String filePath;

    /** File 사이즈 */
    private BigInteger fileSize;

    /** File 명 */
    private String orginlFileNm;

    /** CP사 */
    private String cpNm;

    /** 서비스사 번호 */
    private int svcSeq;

    /** 서비스사 명 */
    private String svcNm;

    /** 회원 아이디 */
    private String mbrID;

    /** 회원 이름 */
    private String mbrNm;

    /** 콘텐츠 카테고리 명 */
    private String contCtgNm;

    /** 콘텐츠 아이디 */
    private String contsID;

    /** 콘텐츠 생성날짜 */
    private String cretDt;

    /** 콘텐츠 수정날짜 */
    private String amdDt;

    /** 콘텐츠 수정자 */
    private String amdrID;

    /** 게시물 조회 페이지 */
    private int offset;

    /** 검수 검색 여부 */
    private String verify;

    /** 게시물 조회 개수 */
    private int limit;

    /** 검색어 */
    private String keyword;

    /** 검색 필드 */
    private String target;

    /** 정렬 필드 */
    private String sidx;

    /** 정렬 값(desc, asc) */
    private String sord; // 콘텐츠 리스트 조회시 정렬 방법

    /** 공통 코드명 */
    private String comnCdNm;

    /** 공통 코드번호 */
    private int comnCdSeq;

    private String searchConfirm;

    /** 콘텐츠 비디오 경로에 맞는 xml 파일 정보 */
    private ContentVO metadataXmlInfo;

    /** 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보 */
    private ContentVO contsXmlInfo;

    /** 콘텐츠 장르 리스트 정보 */
    private List<ContentVO> genreList;

    /** 커버 이미지 리스트 정보 */
    private ContentVO coverImg;

    /** 서비스 리스트 정보 */
    private List<ContentVO> ServiceList;

    /** 갤러리 이미지 리스트 정보 */
    private List<ContentVO> thumbnailList;

    /** 비디오 리스트 정보 */
    private List<ContentVO> videoList;

    /** 파일 리스트 정보 */
    private List<ContentVO> fileDataList;

    /** 미리보기 리스트 정보 */
    private List<ContentVO> prevList;

    /** foreach 리스트 정보 */
    private List<Integer> list;

    /** 검수_ 이력 번호 */
    private int actcSeq;

    /** 검수_ 이력 상태값 */
    private String actcSttus;

    /** 검수_ 이전 이력 상태값 */
    private String prevActcSttus;

    /** 장르 코드 */
    private int genreSeq;

    /** 장르 명 */
    private String genreNm;

    /** 등록자 */
    private String cretrNm;

    /** 전시 상태 변경 목록 */
    private int[] dispContsSeqList;

    /** 콘텐츠 수정 이력 번호 */
    private int contsHstSeq;

    /** 추천 콘텐츠 설정 여부 */
    private String rcmdYn;

    /** 추천 콘텐츠 번호 */
    private int rcmdContsSeq;

    /** 추천 콘텐츠 삭제 목록 */
    private List<Integer> delRcmdContsSeqList;

    /** 추천 순위 */
    private int rcmdRank;

    /** 상태 변경자 아이디 */
    private String sttusChgrId;

    /** 메타데이터 입력값 */
    private String metaData;

    /** 메타데이터 코드 */
    private int contsSubMetadataSeq;

    /** 메타데이터 분류 코드 */
    private String metaDataCtgCode;

    /** 메타데이터 Key 값 */
    private String metaDataKeyVal;

    /** 인증 회원 아이디 */
    private String uid;

    /** 인증 회원 계정 인증 경로(계정 등급) */
    private String accRoute;

    /** device(LiveOn Pico) */
    private String devSerial;

    /** 재생 가능 여부 */
    private String canPlay;

    /** 재생 가능 여부 사유 */
    private String canPlayResn;

    /** 계정 등급 (KT_ID : 01, KT_ID/OTM : 02, KT_ID/OTM/월정액 : 03) */
    private String uidSe;

    /** 콘텐츠 상태 리스트 */
    private List<String> sttusList;

    /** 조회 구분자 */
    private String selectType;

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public String getRemoteSttus() {
        return remoteSttus;
    }

    public void setRemoteSttus(String remoteSttus) {
        this.remoteSttus = remoteSttus;
    }

    public String getSttusVal() {
        return sttusVal;
    }

    public void setSttusVal(String sttusVal) {
        this.sttusVal = sttusVal;
    }

    public String getSttusNm() {
        return sttusNm;
    }

    public void setSttusNm(String sttusNm) {
        this.sttusNm = sttusNm;
    }

    public int getMaxAvlNop() {
        return maxAvlNop;
    }

    public int getContsVer() {
        return contsVer;
    }

    public void setContsVer(int contsVer) {
        this.contsVer = contsVer;
    }

    public String getExeFilePath() {
        return exeFilePath;
    }

    public void setExeFilePath(String exeFilePath) {
        this.exeFilePath = exeFilePath;
    }

    public int getActcSeq() {
        return actcSeq;
    }

    public String getActcSttus() {
        return actcSttus;
    }

    public String getPrevActcSttus() {
        return prevActcSttus;
    }

    public void setPrevActcSttus(String prevActcSttus) {
        this.prevActcSttus = prevActcSttus;
    }

    public void setMaxAvlNop(int maxAvlNop) {
        this.maxAvlNop = maxAvlNop;
    }

    public void setActcSeq(int actcSeq) {
        this.actcSeq = actcSeq;
    }

    public void setActcSttus(String actcSttus) {
        this.actcSttus = actcSttus;
    }

    public String getCretrID() {
        return cretrID;
    }

    public void setCretrID(String cretrID) {
        this.cretrID = cretrID;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getVersionModify() {
        return versionModify;
    }

    public void setVersionModify(String versionModify) {
        this.versionModify = versionModify;
    }

    public String getMtdPath() {
        return mtdPath;
    }

    public void setMtdPath(String mtdPath) {
        this.mtdPath = mtdPath;
    }

    public String getContsDesc() {
        return contsDesc;
    }

    public void setContsDesc(String contsDesc) {
        this.contsDesc = contsDesc;
    }

    public String getContsSubTitle() {
        return contsSubTitle;
    }

    public void setContsSubTitle(String contsSubTitle) {
        this.contsSubTitle = contsSubTitle;
    }

    public String getCntrctStDt() {
        return cntrctStDt;
    }

    public String getVrfRqtDt() {
        return vrfRqtDt;
    }

    public void setCntrctStDt(String cntrctStDt) {
        this.cntrctStDt = cntrctStDt;
    }

    public void setVrfRqtDt(String vrfRqtDt) {
        this.vrfRqtDt = vrfRqtDt;
    }

    public String getCntrctFnsDt() {
        return cntrctFnsDt;
    }

    public void setCntrctFnsDt(String cntrctFnsDt) {
        this.cntrctFnsDt = cntrctFnsDt;
    }

    public String getFirstCtgNm() {
        return firstCtgNm;
    }

    public void setFirstCtgNm(String firstCtgNm) {
        this.firstCtgNm = firstCtgNm;
    }

    public String getSecondCtgNm() {
        return secondCtgNm;
    }

    public void setSecondCtgNm(String secondCtgNm) {
        this.secondCtgNm = secondCtgNm;
    }

    public String getFirstCtgID() {
        return firstCtgID;
    }

    public void setFirstCtgID(String firstCtgID) {
        this.firstCtgID = firstCtgID;
    }

    public String getSecondCtgID() {
        return secondCtgID;
    }

    public void setSecondCtgID(String secondCtgID) {
        this.secondCtgID = secondCtgID;
    }

    public String getGroupingYN() {
        return GroupingYN;
    }

    public void setGroupingYN(String GroupingYN) {
        this.GroupingYN = GroupingYN;
    }

    public int getContsCtgSeq() {
        return contsCtgSeq;
    }

    public void setContsCtgSeq(int contsCtgSeq) {
        this.contsCtgSeq = contsCtgSeq;
    }

    public int getCtgCnt() {
        return ctgCnt;
    }

    public void setCtgCnt(int ctgCnt) {
        this.ctgCnt = ctgCnt;
    }

    public int getCpSeq() {
        return cpSeq;
    }

    public void setCpSeq(int cpSeq) {
        this.cpSeq = cpSeq;
    }

    public int getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(int fileSeq) {
        this.fileSeq = fileSeq;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public BigInteger getFileSize() {
        return fileSize;
    }

    public void setFileSize(BigInteger fileSize) {
        this.fileSize = fileSize;
    }

    public String getOrginlFileNm() {
        return orginlFileNm;
    }

    public void setOrginlFileNm(String orginlFileNm) {
        this.orginlFileNm = orginlFileNm;
    }

    public String getCpNm() {
        return cpNm;
    }

    public void setCpNm(String cpNm) {
        this.cpNm = cpNm;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getMbrID() {
        return mbrID;
    }

    public void setMbrID(String mbrID) {
        this.mbrID = mbrID;
    }

    public String getMbrNm() {
        return mbrNm;
    }

    public void setMbrNm(String mbrNm) {
        this.mbrNm = mbrNm;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getAmdrID() {
        return amdrID;
    }

    public void setAmdrID(String amdrID) {
        this.amdrID = amdrID;
    }

    public int getoffset() {
        return offset;
    }

    public void setoffset(int offset) {
        this.offset = offset;
    }

    public int getlimit() {
        return limit;
    }

    public void setlimit(int limit) {
        this.limit = limit;
    }

    public String getkeyword() {
        return keyword;
    }

    public void setkeyword(String keyword) {
        this.keyword = keyword;
    }

    public String gettarget() {
        return target;
    }

    public void settarget(String target) {
        this.target = target;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public int getComnCdSeq() {
        return comnCdSeq;
    }

    public void setComnCdSeq(int comnCdSeq) {
        this.comnCdSeq = comnCdSeq;
    }

    public String getContCtgNm() {
        return contCtgNm;
    }

    public void setContCtgNm(String contCtgNm) {
        this.contCtgNm = contCtgNm;
    }

    public String getContsID() {
        return contsID;
    }

    public void setContsID(String contsID) {
        this.contsID = contsID;
    }

    public String getComnCdNm() {
        return comnCdNm;
    }

    public void setComnCdNm(String comnCdNm) {
        this.comnCdNm = comnCdNm;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public ContentVO getMetadataXmlInfo() {
        return metadataXmlInfo;
    }

    public void setMetadataXmlInfo(ContentVO metadataXmlInfo) {
        this.metadataXmlInfo = metadataXmlInfo;
    }

    public void setContsXmlInfo(ContentVO contsXmlInfo) {
        this.contsXmlInfo = contsXmlInfo;
    }

    public ContentVO getContsXmlInfo() {
        return contsXmlInfo;
    }

    public void setRcessWhySbst(String rcessWhySbst) {
        this.rcessWhySbst = rcessWhySbst;
    }

    public String getRcessWhySbst() {
        return rcessWhySbst;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public int getGenreSeq() {
        return genreSeq;
    }

    public void setGenreSeq(int genreSeq) {
        this.genreSeq = genreSeq;
    }

    public String getGenreNm() {
        return genreNm;
    }

    public void setGenreNm(String genreNm) {
        this.genreNm = genreNm;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public int[] getDispContsSeqList() {
        return dispContsSeqList;
    }

    public void setDispContsSeqList(String[] dispContsSeqList) {
        this.dispContsSeqList = new int[dispContsSeqList.length];
        for (int i = 0; i < dispContsSeqList.length; i++) {
            this.dispContsSeqList[i] = Integer.parseInt(dispContsSeqList[i]);
        }
    }

    public int getContsHstSeq() {
        return contsHstSeq;
    }

    public void setContsHstSeq(int contsHstSeq) {
        this.contsHstSeq = contsHstSeq;
    }

    public String getRcmdYn() {
        return rcmdYn;
    }

    public void setRcmdYn(String rcmdYn) {
        this.rcmdYn = rcmdYn;
    }

    public int getRcmdContsSeq() {
        return rcmdContsSeq;
    }

    public void setRcmdContsSeq(int rcmdContsSeq) {
        this.rcmdContsSeq = rcmdContsSeq;
    }

    public int getRcmdRank() {
        return rcmdRank;
    }

    public void setRcmdRank(int rcmdRank) {
        this.rcmdRank = rcmdRank;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public String getFrmtnNm() {
        return frmtnNm;
    }

    public void setFrmtnNm(String frmtnNm) {
        this.frmtnNm = frmtnNm;
    }

    public int getFrmtnSeq() {
        return frmtnSeq;
    }

    public void setFrmtnSeq(int frmtnSeq) {
        this.frmtnSeq = frmtnSeq;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public List<Integer> getDelRcmdContsSeqList() {
        return delRcmdContsSeqList;
    }

    public void setDelRcmdContsSeqList(List<Integer> delRcmdContsSeqList) {
        this.delRcmdContsSeqList = new ArrayList<Integer>(delRcmdContsSeqList.size());
        for (int i = 0; i < delRcmdContsSeqList.size(); i++) {
            this.delRcmdContsSeqList.add(delRcmdContsSeqList.get(i));
        }
    }

    public List<ContentVO> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<ContentVO> genreList) {
        this.genreList = genreList;
    }

    public ContentVO getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(ContentVO coverImg) {
        this.coverImg = coverImg;
    }

    public List<ContentVO> getServiceList() {
        return ServiceList;
    }

    public void setServiceList(List<ContentVO> serviceList) {
        ServiceList = serviceList;
    }

    public List<ContentVO> getThumbnailList() {
        return thumbnailList;
    }

    public void setThumbnailList(List<ContentVO> thumbnailList) {
        this.thumbnailList = thumbnailList;
    }

    public List<ContentVO> getVideoList() {
        return videoList;
    }

    public void setVideoList(List<ContentVO> videoList) {
        this.videoList = videoList;
    }

    public List<ContentVO> getFileDataList() {
        return fileDataList;
    }

    public void setFileDataList(List<ContentVO> fileDataList) {
        this.fileDataList = fileDataList;
    }

    public List<ContentVO> getPrevList() {
        return prevList;
    }

    public void setPrevList(List<ContentVO> prevList) {
        this.prevList = prevList;
    }

    public String getUnique() {
        return unique;
    }

    public void setUnique(String unique) {
        this.unique = unique;
    }

    public int getStorDevCnt() {
        return storDevCnt;
    }

    public void setStorDevCnt(int storDevCnt) {
        this.storDevCnt = storDevCnt;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public String getSttusChgrId() {
        return sttusChgrId;
    }

    public void setSttusChgrId(String sttusChgrId) {
        this.sttusChgrId = sttusChgrId;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public int getContsSubMetadataSeq() {
        return contsSubMetadataSeq;
    }

    public void setContsSubMetadataSeq(int contsSubMetadataSeq) {
        this.contsSubMetadataSeq = contsSubMetadataSeq;
    }

    public String getMetaDataCtgCode() {
        return metaDataCtgCode;
    }

    public void setMetaDataCtgCode(String metaDataCtgCode) {
        this.metaDataCtgCode = metaDataCtgCode;
    }

    public String getMetaDataKeyVal() {
        return metaDataKeyVal;
    }

    public void setMetaDataKeyVal(String metaDataKeyVal) {
        this.metaDataKeyVal = metaDataKeyVal;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAccRoute() {
        return accRoute;
    }

    public void setAccRoute(String accRoute) {
        this.accRoute = accRoute;
    }

    public String getDevSerial() {
        return devSerial;
    }

    public void setDevSerial(String devSerial) {
        this.devSerial = devSerial;
    }

    public String getCanPlay() {
        return canPlay;
    }

    public void setCanPlay(String canPlay) {
        this.canPlay = canPlay;
    }

    public String getCanPlayResn() {
        return canPlayResn;
    }

    public void setCanPlayResn(String canPlayResn) {
        this.canPlayResn = canPlayResn;
    }

    public String getUidSe() {
        return uidSe;
    }

    public void setUidSe(String uidSe) {
        this.uidSe = uidSe;
    }

    public List<String> getSttusList() {
        return sttusList;
    }

    public void setSttusList(List<String> sttusList) {
        this.sttusList = new ArrayList<String>(sttusList.size());
        for (int i = 0; i < sttusList.size(); i++) {
            this.sttusList.add(sttusList.get(i));
        }
    }

    public String getSelectType() {
        return selectType;
    }

    public void setSelectType(String selectType) {
        this.selectType = selectType;
    }
}