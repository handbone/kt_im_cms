/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.content.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.ConfigProperty;

/**
 *
 * 콘텐츠 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 2.   A2TEC      최초생성
* 2018. 5. 14.  A2TEC      콘텐츠 검수결과 확인 페이지 이동 메소드 추가 (/content/sttus)
* 2018. 6. 22.  A2TEC      /mms/contents 추가
 *
 *      </pre>
 */
@Component
@Controller
public class ContentController {

    @Autowired
    private ConfigProperty configProperty;

    /**
     * 콘텐츠 리스트 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents", method = RequestMethod.GET)
    public ModelAndView getContentsList(ModelAndView mv) {
        mv.setViewName("/views/contents/ContentList");
        return mv;
    }

    /**
     * 콘텐츠 리스트 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/verify", method = RequestMethod.GET)
    public ModelAndView getVerifyList(ModelAndView mv) {
        mv.setViewName("/views/verify/verifyList");
        return mv;
    }

    /**
     * (검수) 콘텐츠 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/verify/{contsSeq}", method = RequestMethod.GET)
    public ModelAndView verifyView(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/verify/verifyDetail");
        return mv;
    }

    /**
     * 콘텐츠 수정 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/verify/{contsSeq}/edit", method = RequestMethod.GET)
    public ModelAndView verifyEdit(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/verify/verifyEdit");
        return mv;
    }

    /**
     * 콘텐츠 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/{contsSeq}", method = RequestMethod.GET)
    public ModelAndView contentsView(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("storeDomain", configProperty.getProperty("content.store.domain"));
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/contents/ContentDetail");
        return mv;
    }

    /**
     * 콘텐츠 수정 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/{contsSeq}/edit", method = RequestMethod.GET)
    public ModelAndView contentsEdit(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/contents/ContentEdit");
        return mv;
    }

    /**
     * 콘텐츠 등록 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/regist", method = RequestMethod.GET)
    public ModelAndView contentsRegist(ModelAndView mv) {
        mv.setViewName("/views/contents/ContentRegist");
        return mv;
    }

    /**
     * 콘텐츠 검수 결과 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/sttus", method = RequestMethod.GET)
    public ModelAndView contentsSttusList(ModelAndView mv) {
        mv.setViewName("/views/contents/ContentSttusList");
        return mv;
    }

    /**
     * 카테고리 리스트 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/category", method = RequestMethod.GET)
    public ModelAndView getCategory(ModelAndView mv) {
        mv.setViewName("/views/contents/CategoryList");
        return mv;
    }

    /**
     * 전시페이지 관리 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/display", method = RequestMethod.GET)
    public ModelAndView getDisplayList(ModelAndView mv) {
        mv.setViewName("/views/contents/DisplayList");
        return mv;
    }

    /**
     * 전시페이지 상세 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/display/{contsSeq}/edit", method = RequestMethod.GET)
    public ModelAndView displayEdit(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/contents/DisplayEdit");
        return mv;
    }

    /**
     * 추천 콘텐츠 관리 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/recommend", method = RequestMethod.GET)
    public ModelAndView getDisplayRecommend(ModelAndView mv) {
        mv.setViewName("/views/contents/DisplayRecommend");
        return mv;
    }

    /**
     * 매장 콘텐츠 관리 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/mms/contents", method = RequestMethod.GET)
    public ModelAndView mmsContentList(ModelAndView mv) {
        mv.setViewName("/views/service/contents/ContentList");
        return mv;
    }

    /**
     * 매장 콘텐츠 상세 정보 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/mms/contents/{contsSeq}", method = RequestMethod.GET)
    public ModelAndView mmsContentList(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/service/contents/ContentDetail");
        return mv;
    }

    /**
     * 계약 정보 리스트(CP사별)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contract", method = RequestMethod.GET)
    public ModelAndView getContractList(ModelAndView mv) {
        mv.setViewName("/views/contract/contractList");
        return mv;
    }

    /**
     * 계약 정보 리스트(특정 CP사 계약 리스트)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contract/{cpSeq}", method = RequestMethod.GET)
    public ModelAndView contractView(ModelAndView mv, @PathVariable(value = "cpSeq") String cpSeq) {
        mv.addObject("cpSeq", cpSeq);
        mv.setViewName("/views/contract/contractCpList");
        return mv;
    }

    /**
     * 계약 정보 상세 정보(특정 CP사 계약 정보)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contractDetail/{buyinSeq}", method = RequestMethod.GET)
    public ModelAndView contractDetail(ModelAndView mv, @PathVariable(value = "buyinSeq") String buyinSeq) {
        mv.addObject("cpSeq", buyinSeq);
        mv.setViewName("/views/contract/contractDetail");
        return mv;
    }

    /**
     * 계약 정보 수정(특정 CP사 계약 정보/ 수정)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contractDetail/{buyinSeq}/edit", method = RequestMethod.GET)
    public ModelAndView contractEdit(ModelAndView mv, @PathVariable(value = "buyinSeq") String buyinSeq) {
        mv.addObject("buyinSeq", buyinSeq);
        mv.setViewName("/views/contract/contractEdit");
        return mv;
    }

    /**
     * 계약 정보 등록(특정 CP사 계약 정보/ 등록)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contractDetail/{cpSeq}/regist", method = RequestMethod.GET)
    public ModelAndView contractRegist(ModelAndView mv, @PathVariable(value = "cpSeq") String cpSeq) {
        mv.addObject("cpSeq", cpSeq);
        mv.setViewName("/views/contract/contractRegist");
        return mv;
    }

    /**
     * 계약 정보 리스트(특정 CP사 계약 리스트)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/billing/{storSeq}", method = RequestMethod.GET)
    public ModelAndView billingView(ModelAndView mv, @PathVariable(value = "storSeq") String storSeq) {
        mv.addObject("storSeq", storSeq);
        mv.setViewName("/views/contract/billingList");
        return mv;
    }

    /**
     * 계약 정보 상세 정보(특정 CP사 계약 정보)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/billingDetail/{ratSeq}", method = RequestMethod.GET)
    public ModelAndView billingDetail(ModelAndView mv, @PathVariable(value = "ratSeq") String ratSeq) {
        mv.addObject("ratSeq", ratSeq);
        mv.setViewName("/views/contract/billingDetail");
        return mv;
    }

    /**
     * 계약 정보 수정(특정 CP사 계약 정보/ 수정)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/billingDetail/{ratSeq}/edit", method = RequestMethod.GET)
    public ModelAndView billingEdit(ModelAndView mv, @PathVariable(value = "ratSeq") String ratSeq) {
        mv.addObject("ratSeq", ratSeq);
        mv.setViewName("/views/contract/billingEdit");
        return mv;
    }

    /**
     * 계약 정보 등록(특정 CP사 계약 정보/ 등록)
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/billingDetail/{storSeq}/regist", method = RequestMethod.GET)
    public ModelAndView billingRegist(ModelAndView mv, @PathVariable(value = "storSeq") String storSeq) {
        mv.addObject("storSeq", storSeq);
        mv.setViewName("/views/contract/billingRegist");
        return mv;
    }

}
