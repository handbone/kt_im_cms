/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.notice.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
*
* NoticeVO
*
 * @author A2TEC
 * @since 2018.06.20
 * @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 6. 20.   A2TEC      최초생성
*
*
* </pre>
*/

public class NoticeVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5046948878362537770L;

    /** 공지사항 번호 */
    private int noticeSeq;

    /** 공지사항 구분 */
    private String noticeType;

    /** 공지사항 구분명 */
    private String noticeTypeNm;

    /** 공지사항 제목 */
    private String noticeTitle;

    /** 공지사항 내용 (noticeSbst: notice Substance) */
    private String noticeSbst;

    /** 조회수 */
    private int retvNum;

    /** 첨부파일 수 */
    private int fileCount;

    /** 공지사항 게시 시작 일자 */
    private String stDt;

    /** 공지사항 게시 종료 일자 */
    private String fnsDt;

    /** 공지사항 등록 일시 */
    private String regDt;

    /** 등록자 이름 */
    private String cretrNm;

    /** 공지사항 팝업 노출 여부 */
    private String popupViewYn;

    /** 공지사항 삭제 여부 */
    private String delYn;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** 공지사항 노출 우선 순위 */
    private String noticePrefRank;

    /** 사용자 아이디 */
    private String loginId;

    /** 사용자 권한 */
    private String mbrSe;

    /** 등록자 권한 정보 */
    private String cretrMbrSe;

    /** 사용자 계정의 서비스 번호 */
    private int mbrSvcSeq;

    /** 사용자 계정의 매장 번호 */
    private int mbrStorSeq;

    /** 첨부파일 번호 */
    private int fileSeq;

    /** 첨부파일 저장경로 */
    private String filePath;

    /** 첨부파일 삭제 리스트 */
    private List<Integer> deleteFileSeqList;

    /** 공지사항 삭제 리스트 */
    private List<Integer> deleteNoticeSeqList;

    /** 공지사항 리스트 타입 (null: 고객센터 공지사항 , service: 서비스매장관리 공지사항 */
    private String listType;

    /** 공통 코드 번호 */
    int comnCdSeq;

    /** 공통 코드 분류 */
    String comnCdCtg;

    /** 공통 코드 명 */
    String comnCdNm;

    /** 공통 코드 값 */
    String comnCdValue;

    /** 공통 코드 삭제 여부 */
    String comnDelYn;

    public int getNoticeSeq() {
        return noticeSeq;
    }

    public void setNoticeSeq(int noticeSeq) {
        this.noticeSeq = noticeSeq;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public String getNoticeTypeNm() {
        return noticeTypeNm;
    }

    public void setNoticeTypeNm(String noticeTypeNm) {
        this.noticeTypeNm = noticeTypeNm;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeSbst() {
        return noticeSbst;
    }

    public void setNoticeSbst(String noticeSbst) {
        this.noticeSbst = noticeSbst;
    }

    public int getRetvNum() {
        return retvNum;
    }

    public void setRetvNum(int retvNum) {
        this.retvNum = retvNum;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public String getStDt() {
        return stDt;
    }

    public void setStDt(String stDt) {
        this.stDt = stDt;
    }

    public String getFnsDt() {
        return fnsDt;
    }

    public void setFnsDt(String fnsDt) {
        this.fnsDt = fnsDt;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getPopupViewYn() {
        return popupViewYn;
    }

    public void setPopupViewYn(String popupViewYn) {
        this.popupViewYn = popupViewYn;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getNoticePrefRank() {
        return noticePrefRank;
    }

    public void setNoticePrefRank(String noticePrefRank) {
        this.noticePrefRank = noticePrefRank;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getMbrSe() {
        return mbrSe;
    }

    public void setMbrSe(String mbrSe) {
        this.mbrSe = mbrSe;
    }

    public String getCretrMbrSe() {
        return cretrMbrSe;
    }

    public void setCretrMbrSe(String cretrMbrSe) {
        this.cretrMbrSe = cretrMbrSe;
    }

    public int getMbrSvcSeq() {
        return mbrSvcSeq;
    }

    public void setMbrSvcSeq(int mbrSvcSeq) {
        this.mbrSvcSeq = mbrSvcSeq;
    }

    public int getMbrStorSeq() {
        return mbrStorSeq;
    }

    public void setMbrStorSeq(int mbrStorSeq) {
        this.mbrStorSeq = mbrStorSeq;
    }

    public int getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(int fileSeq) {
        this.fileSeq = fileSeq;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<Integer> getDeleteFileSeqList() {
        return deleteFileSeqList;
    }

    public void setDeleteFileSeqList(List<Integer> deleteFileSeqList) {
        this.deleteFileSeqList = new ArrayList<Integer>();
        for (int i = 0; i < deleteFileSeqList.size(); i++) {
            this.deleteFileSeqList.add(deleteFileSeqList.get(i));
        }
    }

    public List<Integer> getDeleteNoticeSeqList() {
        return deleteNoticeSeqList;
    }

    public void setDeleteNoticeSeqList(List<Integer> deleteNoticeSeqList) {
        this.deleteNoticeSeqList = new ArrayList<Integer>();
        for (int i = 0; i < deleteNoticeSeqList.size(); i++) {
            this.deleteNoticeSeqList.add(deleteNoticeSeqList.get(i));
        }
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public int getComnCdSeq() {
        return comnCdSeq;
    }

    public void setComnCdSeq(int comnCdSeq) {
        this.comnCdSeq = comnCdSeq;
    }

    public String getComnCdCtg() {
        return comnCdCtg;
    }

    public void setComnCdCtg(String comnCdCtg) {
        this.comnCdCtg = comnCdCtg;
    }

    public String getComnCdNm() {
        return comnCdNm;
    }

    public void setComnCdNm(String comnCdNm) {
        this.comnCdNm = comnCdNm;
    }

    public String getComnCdValue() {
        return comnCdValue;
    }

    public void setComnCdValue(String comnCdValue) {
        this.comnCdValue = comnCdValue;
    }

    public String getComnDelYn() {
        return comnDelYn;
    }

    public void setComnDelYn(String comnDelYn) {
        this.comnDelYn = comnDelYn;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
