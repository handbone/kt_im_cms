/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.notice.web;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.file.service.FileService;
import kt.com.im.cms.file.vo.FileVO;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.notice.service.NoticeService;
import kt.com.im.cms.notice.vo.NoticeVO;

/**
 *
 * 공지사항 관련 처리 API
 *
 * @author A2TEC
 * @since 2018.06.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 20.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class NoticeApi {

    private final static String viewName = "../resources/api/customer/notice/noticeProcess";

    /** 게시글 구분 */
    private final static String POST_SE_ALL = "01"; // 전체
    private final static String POST_SE_NORMAL = "02"; // 일반
    private final static String POST_SE_SERVICE = "03"; // 서비스
    private final static String POST_SE_STORE = "04"; // 매장
    private final static String POST_SE_CP = "05"; // CP
    private final static String POST_SE_VERIFY = "06"; // 검수

    @Resource(name="NoticeService")
    private NoticeService noticeService;

    @Resource(name="FileService")
    private FileService fileService;

    @Resource(name="MemberService")
    private MemberService memberService;

    //@Resource(name="ClientService")
    //private ClientService clientService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Inject
    private FileSystemResource fsResource;

    /**
     * 고객센터 공지사항 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/notice")
    public ModelAndView noticeProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAllowMember(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");

        NoticeVO item = new NoticeVO();
        FileVO fileVO = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // 고객센터 공지사항은 Master 관리자, CMS 관리자만 관리 가능
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            // Checking Mendatory S
            String invalidParams = CommonFnc.requiredChecking(request,"noticeTitle,noticeType,popupYn,startDt,endDt,noticeSbst,noticePrefRank");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String noticeTitle = CommonFnc.emptyCheckString("noticeTitle", request);
            String noticeType = CommonFnc.emptyCheckString("noticeType", request);
            String popupYn = CommonFnc.emptyCheckString("popupYn", request);
            String startDt = CommonFnc.emptyCheckString("startDt", request);
            String endDt = CommonFnc.emptyCheckString("endDt", request);
            String noticeSbst = CommonFnc.emptyCheckString("noticeSbst", request);
            String noticePrefRank = CommonFnc.emptyCheckString("noticePrefRank", request);
            String delYn = CommonFnc.emptyCheckString("delYn", request);

            item.setNoticeTitle(noticeTitle);
            item.setNoticeType(noticeType);
            item.setPopupViewYn(popupYn);
            item.setStDt(startDt);
            item.setFnsDt(endDt);
            item.setNoticeSbst(noticeSbst);
            item.setDelYn(delYn);
            item.setLoginId(userVO.getMbrId());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = 0;
                // 공지사항 중요도가 '일반'인 경우 0, 그렇지 않은 경우 3: 보통, 2: 높음, 1: 가장 높음
                // 중요도가 설정된 경우 동일 레벨 및 이전의 중요 공지사항은 한단계씩 레벨을 아래로 조정함.
                if (!noticePrefRank.equals("0")) {
                    int mostImptNotice = 0;
                    int imptNotice = 0;
                    int normalNotice = 0;
                    boolean doInsert = false;

                    int prefRank = Integer.parseInt(noticePrefRank);
                    for (int i = prefRank; i < 4; i++) {
                        NoticeVO vo = new NoticeVO();
                        vo.setNoticePrefRank(String.valueOf(i));
                        vo = noticeService.searchImptNotice(vo);

                        if (i == 1) {
                            if (vo == null) {
                                item.setNoticePrefRank(noticePrefRank);
                                result = noticeService.insertNotice(item);
                                doInsert = true;
                                break;
                            } else {
                                mostImptNotice = vo.getNoticeSeq();
                            }
                        } else if (i == 2) {
                            if (vo == null) {
                                if (mostImptNotice > 0) {
                                    break;
                                } else {
                                    item.setNoticePrefRank(noticePrefRank);
                                    result = noticeService.insertNotice(item);
                                    doInsert = true;
                                    break;
                                }
                            } else {
                                imptNotice = vo.getNoticeSeq();
                            }
                        } else if (i == 3) {
                            if (vo == null) {
                                if (imptNotice > 0) {
                                    break;
                                } else {
                                    item.setNoticePrefRank(noticePrefRank);
                                    result = noticeService.insertNotice(item);
                                    doInsert = true;
                                    break;
                                }
                            } else {
                                normalNotice = vo.getNoticeSeq();
                            }
                        }
                    }

                    if (!doInsert) {
                        NoticeVO vo = new NoticeVO();
                        vo.setLoginId(userVO.getMbrId());
                        if (normalNotice > 0) {
                            vo.setNoticeSeq(normalNotice);
                            vo.setNoticePrefRank(String.valueOf(0));
                            noticeService.updateImptNotice(vo);
                        }

                        if (imptNotice > 0) {
                            vo.setNoticeSeq(imptNotice);
                            vo.setNoticePrefRank(String.valueOf(3));
                            noticeService.updateImptNotice(vo);
                        }

                        if (mostImptNotice > 0) {
                            vo.setNoticeSeq(mostImptNotice);
                            vo.setNoticePrefRank(String.valueOf(2));
                            noticeService.updateImptNotice(vo);
                        }

                        item.setNoticePrefRank(noticePrefRank);
                        result = noticeService.insertNotice(item);
                    }
                } else {
                    item.setNoticePrefRank(noticePrefRank);
                    result = noticeService.insertNotice(item);
                }

                if (result > 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("noticeSeq", item.getNoticeSeq());
                    rsModel.setResultType("noticeInsert");
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch(Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            // 고객센터 공지사항은 Master 관리자, CMS 관리자만 관리 가능
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            // Checking Mendatory S
            String invalidParams = CommonFnc.requiredChecking(request,"noticeSeq,noticeTitle,noticeType,startDt,endDt,noticeSbst,noticePrefRank");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int noticeSeq = CommonFnc.emptyCheckInt("noticeSeq", request);
            String noticeTitle = CommonFnc.emptyCheckString("noticeTitle", request);
            String noticeType = CommonFnc.emptyCheckString("noticeType", request);
            String popupYn = CommonFnc.emptyCheckString("popupYn", request);
            String startDt = CommonFnc.emptyCheckString("startDt", request);
            String endDt = CommonFnc.emptyCheckString("endDt", request);
            String noticeSbst = CommonFnc.emptyCheckString("noticeSbst", request);
            String noticePrefRank = CommonFnc.emptyCheckString("noticePrefRank", request);
            String delYn = CommonFnc.emptyCheckString("delYn", request);

            item.setNoticeSeq(noticeSeq);
            item.setNoticeTitle(noticeTitle);
            item.setNoticeType(noticeType);
            item.setPopupViewYn(popupYn);
            item.setStDt(startDt);
            item.setFnsDt(endDt);
            item.setNoticeSbst(noticeSbst);
            item.setDelYn(delYn);
            item.setLoginId(userVO.getMbrId());

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = 0;

                // 공지사항 중요도가 '일반'인 경우 0, 그렇지 않은 경우 3: 보통, 2: 높음, 1: 가장 높음
                // 중요도가 설정된 경우 동일 레벨 및 이전의 중요 공지사항은 한단계씩 레벨을 아래로 조정함.
                if (!noticePrefRank.equals("0")) {
                    System.out.println(">> imptNotice");
                    int mostImptNotice = 0;
                    int imptNotice = 0;
                    int normalNotice = 0;
                    boolean doUpdate = false;

                    int prefRank = Integer.parseInt(noticePrefRank);
                    for (int i = prefRank; i < 4; i++) {
                        NoticeVO vo = new NoticeVO();
                        vo.setNoticePrefRank(String.valueOf(i));
                        vo = noticeService.searchImptNotice(vo);

                        if (i == 1) {
                            if (vo == null) {
                                item.setNoticePrefRank(noticePrefRank);
                                result = noticeService.updateNotice(item);
                                doUpdate = true;
                                break;
                            } else {
                                if (vo.getNoticeSeq() == noticeSeq) {
                                    break;
                                }
                                mostImptNotice = vo.getNoticeSeq();
                            }
                        } else if (i == 2) {
                            if (vo == null) {
                                if (mostImptNotice > 0) {
                                    break;
                                } else {
                                    item.setNoticePrefRank(noticePrefRank);
                                    result = noticeService.updateNotice(item);
                                    doUpdate = true;
                                    break;
                                }
                            } else {
                                if (vo.getNoticeSeq() == noticeSeq) {
                                    break;
                                }
                                imptNotice = vo.getNoticeSeq();
                            }
                        } else if (i == 3) {
                            if (vo == null) {
                                if (imptNotice > 0) {
                                    break;
                                } else {
                                    item.setNoticePrefRank(noticePrefRank);
                                    result = noticeService.updateNotice(item);
                                    doUpdate = true;
                                    break;
                                }
                            } else {
                                if (vo.getNoticeSeq() == noticeSeq) {
                                    break;
                                }
                                normalNotice = vo.getNoticeSeq();
                            }
                        }
                    }

                    if (!doUpdate) {
                        NoticeVO vo = new NoticeVO();
                        vo.setLoginId(userVO.getMbrId());
                        if (normalNotice > 0) {
                            vo.setNoticeSeq(normalNotice);
                            vo.setNoticePrefRank(String.valueOf(0));
                            noticeService.updateImptNotice(vo);
                        }

                        if (imptNotice > 0) {
                            vo.setNoticeSeq(imptNotice);
                            vo.setNoticePrefRank(String.valueOf(3));
                            noticeService.updateImptNotice(vo);
                        }

                        if (mostImptNotice > 0) {
                            vo.setNoticeSeq(mostImptNotice);
                            vo.setNoticePrefRank(String.valueOf(2));
                            noticeService.updateImptNotice(vo);
                        }

                        item.setNoticePrefRank(noticePrefRank);
                        result = noticeService.updateNotice(item);
                    }
                } else {
                    item.setNoticePrefRank(noticePrefRank);
                    result = noticeService.updateNotice(item);
                }

                System.out.println(">> result updateNotice : " + result);
                if (result == 1) {
                    String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);
                    List<String> deleteFilePath = new ArrayList<String>();

                    if (delFileSeqs != "") {
                        String[] delFileSeq = delFileSeqs.split(",");

                        for (int i = 0; i < delFileSeq.length; i++) {
                            item.setFileSeq(Integer.parseInt(delFileSeq[i]));
                            NoticeVO noticeData = noticeService.fileInfo(item);
                            String filePath = noticeData.getFilePath();
                            if (noticeService.attachFileDelete(item) == 1) {
                                deleteFilePath.add(filePath);
                            }
                        }
                    }

                    for (int i = 0; i < deleteFilePath.size(); i++) {
                        File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                        f.delete();
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.rollback(status);
                }
            } catch (Exception e){
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if(!status.isCompleted()){
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            /* 고객센터 공지사항은 Master 관리자, CMS 관리자만 관리 가능 */
            if (!this.isAdministrator(userVO)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            String noticeSeqList = CommonFnc.emptyCheckString("noticeSeqList", request);
            String[] delNoticeSeqList = noticeSeqList.split(",");

            if (delNoticeSeqList.length == 0) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(No notice list to delete.)");
                return rsModel.getModelAndView();
            }

            item.setLoginId(userVO.getMbrId());
            List<Integer> deleteNoticeSeqList = new ArrayList<Integer>();
            for(int i = 0; i < delNoticeSeqList.length; i++) {
                deleteNoticeSeqList.add(Integer.parseInt(delNoticeSeqList[i]));
            }
            item.setDeleteNoticeSeqList(deleteNoticeSeqList);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                noticeService.deleteNotice(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } catch(Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
            /* 공지사항 첨부파일을 삭제할 경우 아래 코드 사용
             * 
            String noticeSeqList = request.getParameter("noticeSeqList") == null ? "" : request.getParameter("noticeSeqList");
            String[] delNoticeSeqList = noticeSeqList.split(",");

            if (delNoticeSeqList.length == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("noticeSeq is empty");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            } else {
                item.setLoginId(session.getAttribute("id").toString());
                List<Integer> deleteNoticeSeqList = new ArrayList<Integer>();
                for(int i = 0; i < delNoticeSeqList.length; i++) {
                    deleteNoticeSeqList.add(Integer.parseInt(delNoticeSeqList[i]));
                }
                item.setDeleteNoticeSeqList(deleteNoticeSeqList);
            }

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = 0;
                List<Integer> deleteFileSeqList = new ArrayList<Integer>();
                List<String> deleteFilePathList = new ArrayList<String>();

                for (int i = 0; i < delNoticeSeqList.length; i++) {
                    int delNoticeSeq = Integer.parseInt(delNoticeSeqList[i]);
                    fileVO.setFileContsSeq(delNoticeSeq);
                    // 문의사항 첨부파일 목록 조회
                    fileVO.setFileSe("NOTICE");
                    List<FileVO> fList = fileService.fileList(fileVO);

                    if (fList != null) {
                        for (int fi = 0; fi < fList.size(); fi++) {
                            int fileSeq = fList.get(fi).getFileSeq();
                            deleteFileSeqList.add(fileSeq);
                            String filePath = fList.get(fi).getFilePath();
                            deleteFilePathList.add(filePath);
                        }
                    }
                }

                if (deleteFileSeqList.size() > 0) {
                    item.setDeleteFileSeqList(deleteFileSeqList);
                    result = noticeService.attachFileDelete(item);
                }

                if (result > 0) {
                    result = noticeService.deleteNotice(item);

                    if (result > 0) {
                        for (int i = 0; i < deleteFilePathList.size(); i++) {
                            File f = new File(fsResource.getPath() + deleteFilePathList.get(i));
                            f.delete();
                        }

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        transactionManager.commit(status);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        transactionManager.rollback(status);
                    }
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.rollback(status);
                }
            } catch(Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
            */
        } else { // GET
            int noticeSeq = CommonFnc.emptyCheckInt("noticeSeq", request);
            String popupView = CommonFnc.emptyCheckString("popupView", request);

            item.setPopupViewYn(popupView);
            item.setMbrSe(userVO.getMbrSe());
            item.setMbrSvcSeq(userVO.getSvcSeq());
            item.setMbrStorSeq(userVO.getStorSeq());

            if (popupView.equals("store")) {
                NoticeVO resultItem = noticeService.noticeDetail(item);

                if(resultItem == null){
                    rsModel.setNoData();
                    return rsModel.getModelAndView();
                } else {
                    noticeSeq = resultItem.getNoticeSeq();
                }
            }

            if (noticeSeq != 0) {
                item.setNoticeSeq(noticeSeq);
                fileVO.setFileContsSeq(noticeSeq);
                fileVO.setFileSe("NOTICE");
                List<FileVO> flist = fileService.fileList(fileVO);

                NoticeVO resultItem = noticeService.noticeDetail(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    boolean isAllowNotice = this.isAllowNotice(userVO, resultItem);
                    if (!isAllowNotice) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                        return rsModel.getModelAndView();
                    }

                    // 공지사항 수정을 위해 상세 정보 요청 시에는 조회 수 업데이트 하지 않음.
                    String searchType = CommonFnc.emptyCheckString("searchType", request);
                    if (!searchType.equals("edit")) {
                        noticeService.updateNoticeRetvNum(item);
                        resultItem.setRetvNum(resultItem.getRetvNum() + 1);
                    }

                    /* 개인정보 마스킹 (등록자 이름) */
                    resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));

                    // 내용 태그 원복
                    resultItem.setNoticeSbst(CommonFnc.unescapeStr(resultItem.getNoticeSbst()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("fileList",flist);
                    rsModel.setResultType("noticeDetail");
                }
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (noticeSeq == 0 && CommonFnc.checkReqParameter(request, "noticeSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                // 중요 공지사항 리스트 조회
                List<NoticeVO> imptNoticeList = noticeService.noticeImptList(item);
                int imptTotCnt = noticeService.noticeImptListTotalCount(item);

                String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if(searchConfirm.equals("true")){
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    if (searchField =="noticeTitle") {
                        searchField = "NOTICE_TITLE";
                    } else if (searchField =="cretrNm") {
                        searchField = "CRETR_NM";
                    }

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }
                /*
                 * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                 */
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 13);
                // 중요 공지사항은 최대 3개까지 지정 가능하므로 한 페이지에 표시할 row 갯수는 13로 하여 조회 쿼리에서는 10개 단위로 조회함.
                limit = limit - 3;
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);

                int page = (currentPage - 1) * limit + 1;
                int pageEnd = page + limit - 1;

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (sidx.equals("num")) {
                    sidx="NOTICE_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<NoticeVO> noticeList = noticeService.noticeList(item);
                List<NoticeVO> resultItem = new ArrayList<NoticeVO>();
                resultItem.addAll(imptNoticeList);
                resultItem.addAll(noticeList);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    /* 개인정보 마스킹 (등록자 이름) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));
                    }

                    int totalCount = noticeService.noticeListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                    // 전체 갯수는 중요공지사항을 포함하여 계산하고 totalpage 계산에서는 중요공지사항 갯수 제외, 이는 중요공지사항은 항상 표시하기 때문
                    rsModel.setData("imptTotCnt", imptTotCnt);
                    rsModel.setData("totalCount", totalCount + imptTotCnt);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("noticeList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 공지사항 등록 시 구분 항목 조회 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/noticeType")
    public ModelAndView noticeType(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 로그인되지 않았거나 마스터, CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null || !isAdministrator(userVO)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        NoticeVO item = new NoticeVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            String cretrMbrSe = CommonFnc.emptyCheckString("cretrMbrSe", request); // 등록자 권한 (수정일 경우에만 값 존재)
            item.setCretrMbrSe(cretrMbrSe);
            List<NoticeVO> resultItemList = noticeService.noticeTypeList(item);

            if(resultItemList.isEmpty()){
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            }else{
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItemList);
            }
            rsModel.setResultType("noticeTypeList");
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_ACCEPTOR.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    private boolean isAllowNotice(MemberVO userVO, NoticeVO resultItem) {
        /**
         * 매장관리자 - 전체, 일반, 매장의 활성화된 공지사항만 접근가능
         * 서비스관리자 - 전체, 일반, 서비스, 매장의 활성화된 공지사항만 접근가능
         * 검수자 - 전체, 검수의 활성화된 공지사항만 접근가능
         * 18.11.30 고객센터 컨셉 변경으로 서비스관리자, 매장관리자도 전체, 일반만 접근 가능 (구분 항목에서 서비스, 매장 항목은 삭제됨)
         */
        if ((userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)
                || userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)
                || userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_ACCEPTOR))
                && resultItem.getDelYn().equals("Y")) {
            return false;
        }

        if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_STORE)) {
            if (!resultItem.getNoticeType().equals(POST_SE_ALL)
                && !resultItem.getNoticeType().equals(POST_SE_NORMAL)
                && !resultItem.getNoticeType().equals(POST_SE_STORE)) {
                return false;
            }
        } else if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_SERVICE)) {
            if (!resultItem.getNoticeType().equals(POST_SE_ALL)
                && !resultItem.getNoticeType().equals(POST_SE_NORMAL)
                && !resultItem.getNoticeType().equals(POST_SE_SERVICE)
                && !resultItem.getNoticeType().equals(POST_SE_STORE)) {
                return false;
            }
        } else if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_ACCEPTOR)) {
            if (!resultItem.getNoticeType().equals(POST_SE_ALL)
                    && !resultItem.getNoticeType().equals(POST_SE_VERIFY)) {
                return false;
            }
        }

        return true;
    }
}
