/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.notice.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.notice.vo.NoticeVO;

/**
 *
 * 공지사항 관련 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.20
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 20.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("NoticeDAO")
public class NoticeDAOImpl extends mysqlAbstractMapper implements NoticeDAO {

    /**
     * 공지사항 리스트 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 공지사항 리스트
     */
    @Override
    public List<NoticeVO> noticeList(NoticeVO vo) {
        return selectList("NoticeDAO.noticeList", vo);
    }

    /**
     * 공지사항 리스트 합계 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 공지사항 리스트 합계
     */
    public int noticeListTotalCount(NoticeVO vo){
         int res = (Integer) selectOne("NoticeDAO.noticeListTotalCount", vo);
         return res;
    }

    /**
     * 공지사항 상세 정보 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 공지사항 상세 정보
     */
    @Override
    public NoticeVO noticeDetail(NoticeVO vo) {
        return selectOne("NoticeDAO.noticeDetail", vo);
    }

    /**
     * 공지사항 조회수 업데이트
     * @param NoticeVO
     * @return
     */
    @Override
    public void updateNoticeRetvNum(NoticeVO vo) {
        update("NoticeDAO.updateNoticeRetvNum", vo);
    }

    /**
     * 중요 공지사항 리스트 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 중요 공지사항 리스트
     */
    @Override
    public List<NoticeVO> noticeImptList(NoticeVO vo) {
        return selectList("NoticeDAO.noticeImptList", vo);
    }

    /**
     * 중요 공지사항 리스트 합계 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 중요 공지사항 리스트 합계
     */
    public int noticeImptListTotalCount(NoticeVO vo){
         int res = (Integer) selectOne("NoticeDAO.noticeImptListTotalCount", vo);
         return res;
    }

    /**
     * 공지사항 등록
     * @param NoticeVO
     * @return 등록 결과
     */
    @Override
    public int insertNotice(NoticeVO vo) {
        return insert("NoticeDAO.insertNotice", vo);
    }

    /**
     * 공지사항 수정
     * @param NoticeVO
     * @return 수정 결과
     */
    @Override
    public int updateNotice(NoticeVO vo) {
        return update("NoticeDAO.updateNotice", vo);
    }

    /**
     * 공지사항 삭제
     * @param NoticeVO
     * @return 삭제 결과
     */
    @Override
    public int deleteNotice(NoticeVO vo) {
        return update("NoticeDAO.deleteNotice", vo);
    }

    /**
     * 첨부파일 정보
     * @param NoticeVO
     * @return 첨부파일 정보
     */
    @Override
    public NoticeVO fileInfo(NoticeVO vo) {
        return selectOne("NoticeDAO.fileInfo", vo);
    }

    /**
     * 첨부파일 삭제
     * @param NoticeVO
     * @return 삭제결과
     */
    @Override
    public int attachFileDelete(NoticeVO vo) {
        return delete("NoticeDAO.attachFileDelete", vo);
    }

    /**
     * 회원 권한에 따른 서비스 공지사항 타입 목록 조회
     * @param NoticeVO
     * @return 공지사항 타입 목록
     */
    @Override
    public List<NoticeVO> noticeTypeList(NoticeVO vo) {
        return selectList("NoticeDAO.noticeTypeList", vo);
    }

    /**
     * 중요 공지사항 조회
     * @param NoticeVO
     * @return 첨부파일 정보
     */
    @Override
    public NoticeVO searchImptNotice(NoticeVO vo) {
        return selectOne("NoticeDAO.searchImptNotice", vo);
    }

    /**
     * 공지사항 노출 우선 순위 업데이트
     * @param NoticeVO
     * @return 업데이트 결과
     */
    @Override
    public int updateImptNotice(NoticeVO vo) {
        return update("NoticeDAO.updateImptNotice", vo);
    }

    /**
     * 공지사항 게시 일자 종료에 따른 비활성화
     * @param NoticeVO
     * @return 업데이트 결과
     */
    @Override
    public int updateNoticeDisplayFinish(NoticeVO vo) {
        return update("NoticeDAO.updateNoticeDisplayFinish", vo);
    }

}
