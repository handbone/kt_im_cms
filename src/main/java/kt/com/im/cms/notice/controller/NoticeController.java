/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.notice.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * 공지사항 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 3.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Component
@Controller
public class NoticeController {

    /**
     * 고객센터 공지사항 리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/customer/notice", method = RequestMethod.GET)
    public ModelAndView customerNoticeList(ModelAndView mv) {
        mv.setViewName("/views/customer/notice/NoticeList");
        return mv;
    }

    /**
     * 고객센터 공지사항 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/customer/notice/{noticeSeq}", method = RequestMethod.GET)
    public ModelAndView customerNoticeDetail(ModelAndView mv, @PathVariable(value="noticeSeq") String noticeSeq) {
        mv.addObject("noticeSeq", noticeSeq);
        mv.setViewName("/views/customer/notice/NoticeDetail");
        return mv;
    }

    /**
     * 고객센터 공지사항 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/customer/notice/regist", method = RequestMethod.GET)
    public ModelAndView customerNoticeRegist(ModelAndView mv) {
        mv.setViewName("/views/customer/notice/NoticeRegist");
        return mv;
    }

    /**
     * 고객센터 공지사항 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/customer/notice/{noticeSeq}/edit", method = RequestMethod.GET)
    public ModelAndView customerNoticeEdit(ModelAndView mv, @PathVariable(value="noticeSeq") String noticeSeq) {
        mv.addObject("noticeSeq", noticeSeq);
        mv.setViewName("/views/customer/notice/NoticeEdit");
        return mv;
    }

}
