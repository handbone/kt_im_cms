/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicecontract.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.content.service.ContentService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.servicecontract.service.ServiceContractService;
import kt.com.im.cms.servicecontract.vo.ServiceContractVO;
import kt.com.im.cms.store.service.StoreService;
import kt.com.im.cms.store.vo.StoreVO;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.content.vo.ContentVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;

/**
 *
 * 과금관리 관련 처리 API
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 10. 18.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class ServiceContractApi {

    @Resource(name="ServiceContractService")
    private ServiceContractService serviceContractService;

    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "StoreService")
    private StoreService storeService;

    /**
     * 과금 관리 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/serviceContract")
    public ModelAndView serviceContractProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/service/contract/serviceContractProcess");

        // 로그인 되지 않았거나 권한이 없을경우 접근불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ServiceContractVO item = new ServiceContractVO();
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int ratSeq = CommonFnc.emptyCheckInt("ratSeq", request);

            if (ratSeq != 0) {
                item.setRatSeq(ratSeq);

                ServiceContractVO resultItem = serviceContractService.serviceContractDetail(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    StoreVO vo = new StoreVO();
                    vo.setStorSeq(resultItem.getStorSeq());

                    StoreVO resultCItem = storeService.storeDetail(vo);
                    if (resultCItem == null) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    } else {
                        /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                        resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                        resultItem.setAmdrId(CommonFnc.getIdMask(resultItem.getAmdrId()));

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("marketNm", resultCItem.getSvcNm() + "(" + resultCItem.getStorNm() + ")");
                        rsModel.setData("svcNm", resultCItem.getSvcNm());
                        rsModel.setData("storNm", resultCItem.getStorNm());
                        rsModel.setData("item", resultItem);
                        rsModel.setResultType("serviceContractDetail");
                    }
                }
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (ratSeq == 0 && CommonFnc.checkReqParameter(request, "ratSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                int storSeq = CommonFnc.emptyCheckInt("storSeq", request);

                item.setSvcSeq(svcSeq);
                item.setStorSeq(storSeq);

                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    String searchField = CommonFnc.emptyCheckString("searchField", request); // 검색 카테고리
                    String searchString = CommonFnc.emptyCheckString("searchString", request); // 검색어

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }

                int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page")); // 현재 페이지
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows")); // 검색에 출력될 개수
                String sidx = CommonFnc.emptyCheckString("sidx", request); // 정렬할 필드
                String sord = CommonFnc.emptyCheckString("sord", request); // 정렬 방법
                int page = (currentPage - 1) * limit + 1; // 페이지 & 출력개수 계산 변수 (페이지 개수)
                int pageEnd = (currentPage - 1) * limit + limit; // 페이지 & 출력개수 계산 변수 (마지막페이지값)

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (!sidx.equals("RAT_SEQ") && !sidx.equals("RAT_CONT_NO") && !sidx.equals("CONTS_TITLE")
                        && !sidx.equals("RAT_CONT_TYPE") && !sidx.equals("REG_DT") && !sidx.equals("SET_VAL")
                        && !sidx.equals("TOT_PRICE") && !sidx.equals("USE_PRICE") && !sidx.equals("CRETR_NM")
                        && !sidx.equals("AMD_DT") && !sidx.equals("CONTS_COUNT")) {
                    sidx = "RAT_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<ServiceContractVO> resultList = serviceContractService.serviceContractList(item);

                if (resultList.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 이름, 아이디) */
                    for (int i = 0; i < resultList.size(); i++) {
                        resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
                        resultList.get(i).setCretrId(CommonFnc.getIdMask(resultList.get(i).getCretrId()));
                    }

                    // 가져온 리스트 총 개수
                    int totalCount = serviceContractService.serviceContractListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultList);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("serviceContractList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_STORE.equals(mbrSe);
    }

}
