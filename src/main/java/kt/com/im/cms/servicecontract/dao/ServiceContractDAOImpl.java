/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicecontract.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.servicecontract.vo.ServiceContractVO;

/**
 *
 * 과금관리 관련 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 10. 18.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("ServiceContractDAO")
public class ServiceContractDAOImpl extends mysqlAbstractMapper implements ServiceContractDAO {

    /**
     * 과금관리 리스트 조회
     * @param ServiceContractVO
     * @return 조회 목록
     */
    @Override
    public List<ServiceContractVO> serviceContractList(ServiceContractVO vo) {
        return selectList("ServiceContractDAO.serviceContractList", vo);
    }

    /**
     * 과금관리 리스트 합계 조회
     * @param ServiceContractVO
     * @return 목록 합계
     */
    public int serviceContractListTotalCount(ServiceContractVO vo) {
         int res = (Integer) selectOne("ServiceContractDAO.serviceContractListTotalCount", vo);
         return res;
    }

    /**
     * 과금 계약 상세 정보 조회
     * @param ServiceContractVO
     * @return 상세 정보
     */
    @Override
    public ServiceContractVO serviceContractDetail(ServiceContractVO vo) {
        return selectOne("ServiceContractDAO.serviceContractDetail", vo);
    }

}
