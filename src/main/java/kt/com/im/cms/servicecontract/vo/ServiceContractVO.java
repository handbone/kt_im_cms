/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicecontract.vo;

import java.io.Serializable;

/**
 *
 * ContractVO
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 10. 18.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class ServiceContractVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2822401408366315062L;

    /** 매장 번호 */
    private int storSeq;

    /** 서비스 번호 */
    private int svcSeq;

    private String marketNm;

    /** 매장 계약 시퀀스 번호 */
    private int ratSeq;

    /** 매장 계약 유형 */
    private String ratContType;

    /** 매장 계약 번호 */
    private String ratContNo;

    /** 매장 계약 기간 (시작) */
    private String ratContStDt;

    /** 매장 계약 기간 (종료) */
    private String ratContFnsDt;

    /** 수급 계약 시퀀스 번호 */
    private int buyinSeq;

    /** 매장 계약 콘텐츠 시퀀스 번호 */
    private int ratContsSeq;

    /** 계약 콘텐츠 시퀀스 번호 */
    private int buyinContsSeq;

    /** 계약 수정 이력 번호 */
    private int buyinContHstSeq;

    /** 매장 계약 수정 이력 번호 */
    private int ratContHstSeq;

    /** 과금 여부 */
    private String ratYn;

    /** 수급 계약 번호 */
    private String buyinContNo;

    /** 수급 계약 기간 (시작) */
    private String buyinContStDt;

    /** 수급 계약 기간 (종료) */
    private String buyinContFnsDt;

    /** 수급 계약 종류 */
    private String buyinContType;

    /** 수급 실행 횟수 제한 횟수 */
    private int runLmtCnt;

    /** 수급 실행 횟수 제한 금액 */
    private int runPerPrc;

    /** 수급 시간당 과금 시간 */
    private int ratTime;

    /** 수급 시간당 금액 */
    private int ratPrc;

    /** 계약 번호 중복 개수 */
    private int ratContCnt;

    /** 수급 일시금 지급형 금액 */
    private int lmsmpyPrc;

    /** 콘텐츠 개수 */
    private int contsCount;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 이용 횟수 */
    private int useCount;

    /** 타입 별 값 */
    private int setVal;

    /** 수량 */
    private int count;

    /** 금액 */
    private int totPrice;

    /** 실사용금액 */
    private int usePrice;

    /** 등록일(수정일시 또는 생성일시) */
    private String regDt;

    /** 등록자 */
    private String cretrNm;

    /** 생성자 아이디 */
    private String cretrId;

    /** 생성 일시 */
    private String cretDt;

    /** 수정자 아이디 */
    private String amdrId;

    /** 수정 일시 */
    private String amdDt;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getMarketNm() {
        return marketNm;
    }

    public void setMarketNm(String marketNm) {
        this.marketNm = marketNm;
    }

    public int getRatSeq() {
        return ratSeq;
    }

    public void setRatSeq(int ratSeq) {
        this.ratSeq = ratSeq;
    }

    public String getRatContType() {
        return ratContType;
    }

    public void setRatContType(String ratContType) {
        this.ratContType = ratContType;
    }

    public String getRatContNo() {
        return ratContNo;
    }

    public void setRatContNo(String ratContNo) {
        this.ratContNo = ratContNo;
    }

    public String getRatContStDt() {
        return ratContStDt;
    }

    public void setRatContStDt(String ratContStDt) {
        this.ratContStDt = ratContStDt;
    }

    public String getRatContFnsDt() {
        return ratContFnsDt;
    }

    public void setRatContFnsDt(String ratContFnsDt) {
        this.ratContFnsDt = ratContFnsDt;
    }

    public int getBuyinSeq() {
        return buyinSeq;
    }

    public void setBuyinSeq(int buyinSeq) {
        this.buyinSeq = buyinSeq;
    }

    public int getRatContsSeq() {
        return ratContsSeq;
    }

    public void setRatContsSeq(int ratContsSeq) {
        this.ratContsSeq = ratContsSeq;
    }

    public int getBuyinContsSeq() {
        return buyinContsSeq;
    }

    public void setBuyinContsSeq(int buyinContsSeq) {
        this.buyinContsSeq = buyinContsSeq;
    }

    public int getBuyinContHstSeq() {
        return buyinContHstSeq;
    }

    public void setBuyinContHstSeq(int buyinContHstSeq) {
        this.buyinContHstSeq = buyinContHstSeq;
    }

    public int getRatContHstSeq() {
        return ratContHstSeq;
    }

    public void setRatContHstSeq(int ratContHstSeq) {
        this.ratContHstSeq = ratContHstSeq;
    }

    public String getRatYn() {
        return ratYn;
    }

    public void setRatYn(String ratYn) {
        this.ratYn = ratYn;
    }

    public String getBuyinContNo() {
        return buyinContNo;
    }

    public void setBuyinContNo(String buyinContNo) {
        this.buyinContNo = buyinContNo;
    }

    public String getBuyinContStDt() {
        return buyinContStDt;
    }

    public void setBuyinContStDt(String buyinContStDt) {
        this.buyinContStDt = buyinContStDt;
    }

    public String getBuyinContFnsDt() {
        return buyinContFnsDt;
    }

    public void setBuyinContFnsDt(String buyinContFnsDt) {
        this.buyinContFnsDt = buyinContFnsDt;
    }

    public String getBuyinContType() {
        return buyinContType;
    }

    public void setBuyinContType(String buyinContType) {
        this.buyinContType = buyinContType;
    }

    public int getRunLmtCnt() {
        return runLmtCnt;
    }

    public void setRunLmtCnt(int runLmtCnt) {
        this.runLmtCnt = runLmtCnt;
    }

    public int getRunPerPrc() {
        return runPerPrc;
    }

    public void setRunPerPrc(int runPerPrc) {
        this.runPerPrc = runPerPrc;
    }

    public int getRatTime() {
        return ratTime;
    }

    public void setRatTime(int ratTime) {
        this.ratTime = ratTime;
    }

    public int getRatPrc() {
        return ratPrc;
    }

    public void setRatPrc(int ratPrc) {
        this.ratPrc = ratPrc;
    }

    public int getRatContCnt() {
        return ratContCnt;
    }

    public void setRatContCnt(int ratContCnt) {
        this.ratContCnt = ratContCnt;
    }

    public int getLmsmpyPrc() {
        return lmsmpyPrc;
    }

    public void setLmsmpyPrc(int lmsmpyPrc) {
        this.lmsmpyPrc = lmsmpyPrc;
    }

    public int getContsCount() {
        return contsCount;
    }

    public void setContsCount(int contsCount) {
        this.contsCount = contsCount;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public int getSetVal() {
        return setVal;
    }

    public void setSetVal(int setVal) {
        this.setVal = setVal;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotPrice() {
        return totPrice;
    }

    public void setTotPrice(int totPrice) {
        this.totPrice = totPrice;
    }

    public int getUsePrice() {
        return usePrice;
    }

    public void setUsePrice(int usePrice) {
        this.usePrice = usePrice;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

}
