/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.sms.web;

import java.text.MessageFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mashape.unirest.http.JsonNode;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ConfigProperty;
import kt.com.im.cms.common.util.MailSender;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.SmsSender;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.content.service.ContentService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.qna.service.QnaService;
import kt.com.im.cms.sms.service.SmsService;
import kt.com.im.cms.sms.vo.SmsVO;

/**
 *
 * SMS, 이메일 발송 관련 처리 API
 *
 * @author A2TEC
 * @since 2018.09.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 9. 04.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class SmsApi {

    static public final String TYPE_SINGLE = "single";
    static public final String TYPE_MULTIPLE = "multiple";

    static public final String SECTION_MEMBER_INIT = "initMember";
    static public final String SECTION_MEMBER_REGIST = "registMember";
    static public final String SECTION_MEMBER_CONFIRM = "confirmMember";
    static public final String SECTION_CONTENT_REJECT = "rejectConts";
    static public final String SECTION_CONTENT_CONFIRM = "confirmConts";
    static public final String SECTION_CATEGORY_REGIST = "registCtg";
    static public final String SECTION_QNA_REGIST = "registQna";
    static public final String SECTION_QNA_REPLY = "replyQna";

    static public final String MSG_TITLE = "GiGA Live On";
    static public final String MSG_MEMBER_INIT = "[" + MSG_TITLE + " {0}]\r\n비밀번호가 초기화되었습니다. 자세한 내용은 메일에서 확인해주세요.";
    static public final String MSG_MEMBER_REGIST = "[" + MSG_TITLE + " OMS]\r\n{0} 계정이 발급되었습니다. 자세한 내용은 메일에서 확인해주세요.";
    static public final String MSG_MEMBER_REGIST_EMAIL = MSG_TITLE + " OMS 신규 계정 발급 완료";
    static public final String MSG_MEMBER_CONFIRM = "[" + MSG_TITLE + " Store]\r\n신청하신 아이디로 승인 완료 되었습니다.";
    static public final String MSG_CONTENT_REJECT = "[" + MSG_TITLE + " Store]\r\n콘텐츠 등록요청이 반려되었습니다. 반려사유를 확인하세요.";
    static public final String MSG_CONTENT_CONFIRM_OMS = "[" + MSG_TITLE + " OMS]\r\n신규 콘텐츠가 등록되었습니다. " + MSG_TITLE + " OMS에서 확인하세요.";
    static public final String MSG_CONTENT_CONFIRM_STORE = "[" + MSG_TITLE + " Store] 신청하신 콘텐츠가 " + MSG_TITLE + " OMS에 등록 되었습니다.";
    static public final String MSG_CATEGORY_REGIST = "[" + MSG_TITLE + " OMS]\r\n신규 카테고리가 등록되었습니다. 해당 카테고리를 이용하고자 하는 서비스/매장 관리자는 메일을 확인해 주세요.";
    static public final String MSG_CATEGORY_REGIST_EMAIL = MSG_TITLE + " OMS 신규 카테고리 등록 알림";
    static public final String MSG_QNA_REGIST = "[" + MSG_TITLE + " OMS]\r\n문의사항이 등록되었습니다. OMS 고객센터>문의사항에서 확인하세요.";
    static public final String MSG_QNA_REPLY = "[" + MSG_TITLE + " Store]\r\n문의사항에 대한 답변이 등록되었습니다. " + MSG_TITLE + " Store 고객센터>문의사항에서 확인하세요.";
    static public final String MSG_DESC = "[{0}] NO.{1}";

    static public final String MSG_NO_USE = "NO USE";
    static public final String MSG_SUCCESS = "SUCCESS";
    static public final String MSG_FAIL = "FAIL";

    private boolean enabledLog = false;

    @Resource(name = "SmsService")
    private SmsService smsService;

    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "QnaService")
    private QnaService qnaService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private ConfigProperty configProperty;

    @RequestMapping(value = "/api/sms")
    public ModelAndView smsProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("jsonView");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (enabledLog) {
            System.out.println("[SMS] method = " + method + " type = " + request.getParameter("type") + " section = " + request.getParameter("section") + " seq = " + request.getParameter("seq"));
        }
        if (method.equals("POST")) {
            String type = request.getParameter("type");
            if (TYPE_SINGLE.equalsIgnoreCase(type)) {
                return this.sendSingleSms(request, rsModel, userVO);
            }

            if (TYPE_MULTIPLE.equalsIgnoreCase(type)) {
                return this.sendMultipleSms(request, rsModel, userVO);
            }
            return this.sendSms(request, rsModel, userVO);
        }

        rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView sendSms(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) {
        boolean isAdministrator = MemberApi.MEMBER_SECTION_MASTER.equals(userVO.getMbrSe()) || MemberApi.MEMBER_SECTION_CMS.equals(userVO.getMbrSe());
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "destPhone:{" + Validator.PHONE_NO + "};" + "destName:{" + Validator.NAME + "};"
                + "msgBody:{" + Validator.MAX_LENGTH + "=1000}";

        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        SmsVO smsVo = new SmsVO();
        String destPhone = request.getParameter("destPhone");
        destPhone = destPhone.replaceAll("-", "").trim();
        smsVo.setDestPhone(destPhone);
        smsVo.setMsgBody(request.getParameter("msgBody"));
        smsVo.setCretrId(userVO.getMbrId());
        smsVo.setConnectUrl(configProperty.getProperty("sms.connect.url"));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            JsonNode sendResult = SmsSender.send(smsVo);
            String resultCode = (String) sendResult.getObject().get("result_code");
            smsVo.setResultCode(resultCode);
            smsService.insertSentSmsInfo(smsVo);

            transactionManager.commit(status);

            if (!"200".equals(resultCode)) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
        } catch (Exception e) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findId.serverError");
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView sendSingleSms(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) {
        if (!usesAutoSend()) {
            rsModel.setStatus(ResultModel.HTTP_STATUS_SERVICE_UNAVAILABLE);
            return rsModel.getModelAndView();
        }

        boolean isAdministrator = MemberApi.MEMBER_SECTION_MASTER.equals(userVO.getMbrSe()) || MemberApi.MEMBER_SECTION_CMS.equals(userVO.getMbrSe());
        boolean prevented = !isAdministrator && !(SECTION_CONTENT_REJECT.equalsIgnoreCase(request.getParameter("section")) && MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe()));
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "section;seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            SmsVO searchVO = new SmsVO();
            searchVO.setConnectUrl(configProperty.getProperty("sms.connect.url"));
            int seq = Integer.parseInt(request.getParameter("seq"));

            // SMS 발송 구분에 따라 SMS 수신인 쿼리 조회 조건 및 SMS 상세 내역 문구 설정
            String section = request.getParameter("section");
            String desc = "";
            if (SECTION_MEMBER_INIT.equalsIgnoreCase(section)) {
                searchVO.setMbrSeq(seq);
                desc = MessageFormat.format(MSG_DESC, new Object[] { "MEMBER", String.valueOf(seq) });
            } else if (SECTION_MEMBER_CONFIRM.equalsIgnoreCase(section)) {
                searchVO.setMbrSeq(seq);
                desc = MessageFormat.format(MSG_DESC, new Object[] { "MEMBER", String.valueOf(seq) });
            } else if (SECTION_MEMBER_REGIST.equalsIgnoreCase(section)) {
                validateParams = "pwd:{" + Validator.PWD + "}";
                invalidParams = Validator.validate(request, validateParams);
                if (!invalidParams.isEmpty()) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                    return rsModel.getModelAndView();
                }

                searchVO.setMbrSeq(seq);
                desc = MessageFormat.format(MSG_DESC, new Object[] { "MEMBER", String.valueOf(seq) });
            } else if (SECTION_CONTENT_REJECT.equalsIgnoreCase(section)) {
                searchVO.setContsSeq(seq);
                desc = MessageFormat.format(MSG_DESC, new Object[] { "CONTENT", String.valueOf(seq) });
            } else if (SECTION_QNA_REPLY.equalsIgnoreCase(section)) {
                searchVO.setQnaSeq(seq);
                desc = MessageFormat.format(MSG_DESC, new Object[] { "QNA", String.valueOf(seq) });
            }

            SmsVO smsVo = smsService.getSmsDestnation(searchVO);
            smsVo.setConnectUrl(configProperty.getProperty("sms.connect.url"));
            if (smsVo == null) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            // SMS 발송 구분에 따라 발송 메세지 설정
            String msgBody = "";
            if (SECTION_MEMBER_INIT.equalsIgnoreCase(section)) {
                String title = MemberApi.MEMBER_SECTION_CP.equals(smsVo.getMbrSe()) ? "STORE" : "OMS";
                msgBody = MessageFormat.format(MSG_MEMBER_INIT, new Object[] { title });

                // 관리자에 의한 비밀번호의 초기화의 경우 메일이 정상전송된 경우에만 해당 API가 호출되기 때문에 강제 성공 메세지 설정
                smsVo.setSendMail(MSG_SUCCESS);
            } else if (SECTION_MEMBER_REGIST.equalsIgnoreCase(section)) {
                if (MemberApi.MEMBER_SECTION_CP.equals(smsVo.getMbrSe())) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                String title = "Master";
                if (MemberApi.MEMBER_SECTION_CMS.equals(smsVo.getMbrSe())) {
                    title = "CMS 관리자";
                } else if (MemberApi.MEMBER_SECTION_ACCEPTOR.equals(smsVo.getMbrSe())) {
                    title = "검수자";
                } else if (MemberApi.MEMBER_SECTION_SERVICE.equals(smsVo.getMbrSe())) {
                    title = "서비스 관리자";
                } else if (MemberApi.MEMBER_SECTION_STORE.equals(smsVo.getMbrSe())) {
                    title = "매장 관리자";
                }
                msgBody = MessageFormat.format(MSG_MEMBER_REGIST, new Object[] { title });
            } else if (SECTION_MEMBER_CONFIRM.equalsIgnoreCase(section)) {
                if (!MemberApi.MEMBER_SECTION_CP.equals(smsVo.getMbrSe())) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
                msgBody = MSG_MEMBER_CONFIRM;
            } else if (SECTION_CONTENT_REJECT.equalsIgnoreCase(section)) {
                msgBody = MSG_CONTENT_REJECT;
            } else if (SECTION_QNA_REPLY.equalsIgnoreCase(section)) {
                msgBody = MSG_QNA_REPLY;
            }

            smsVo.setMsgBody(msgBody);
            smsVo.setSendDesc(desc);
            smsVo.setCretrId(userVO.getMbrId());

            // SMS 발송 기능 활성화 시 이메일 발송 기능은 강제 활성화 처리, 이메일은 단독으로 사용 가능
            if (usesAutoSendSms() || usesAutoSendEmail()) {
                MailSender mailSender = new MailSender(configProperty);
                if (SECTION_MEMBER_REGIST.equalsIgnoreCase(section)) {
                    String subject = MSG_MEMBER_REGIST_EMAIL;
                    smsVo.setTempPwd(request.getParameter("pwd"));
                    mailSender.addRecipient(smsVo.getMbrEmail());
                    mailSender.setSubject(subject);
                    mailSender.setContent(mailSender.getRegistMemberContent(smsVo));
                    boolean sendResult = mailSender.send();
                    smsVo.setSendMail((sendResult) ? MSG_SUCCESS : MSG_FAIL);
                }
            }

            String resultCode = "0";
            if (usesAutoSendSms()) {
                String[] phonList = smsVo.getDestPhone().split(";");
                for (int i = 0; i < phonList.length; i++) {
                    smsVo.setDestPhone(phonList[i].replaceAll("-", "").trim());
                    JsonNode sendResult = SmsSender.send(smsVo);
                    resultCode = (String) sendResult.getObject().get("result_code");

                    smsVo.setResultCode(resultCode);
                    if (enabledLog) {
                        CommonFnc.voInfo(smsVo);
                    }
                    smsService.insertSentSmsInfo(smsVo);

                    transactionManager.commit(status);

                    if (!"200".equals(resultCode)) {
                        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }
                }
            }
        } catch (Exception e) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findId.serverError");
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView sendMultipleSms(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) {
        if (!usesAutoSend()) {
            rsModel.setStatus(ResultModel.HTTP_STATUS_SERVICE_UNAVAILABLE);
            return rsModel.getModelAndView();
        }

        String validateParams = "section;seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String section = request.getParameter("section");
        boolean isSupportMultipleSms = SECTION_CONTENT_CONFIRM.equalsIgnoreCase(section) || SECTION_CATEGORY_REGIST.equalsIgnoreCase(section);
        if (!isSupportMultipleSms) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        SmsVO searchVO = new SmsVO();
        int seq = Integer.parseInt(request.getParameter("seq"));
        searchVO.setConnectUrl(configProperty.getProperty("sms.connect.url"));

        String desc = "";
        // SMS 발송 구분에 따라 SMS 수신인 쿼리 조회 조건 및 SMS 상세 내역 문구 설정
        if (SECTION_CONTENT_CONFIRM.equalsIgnoreCase(section)) {
            boolean isAdministrator = MemberApi.MEMBER_SECTION_MASTER.equals(userVO.getMbrSe()) || MemberApi.MEMBER_SECTION_CMS.equals(userVO.getMbrSe());
            if (!isAdministrator && !MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            searchVO.setContsSeq(seq);
            desc = MessageFormat.format(MSG_DESC, new Object[] { "CONTENT", String.valueOf(seq) });
        } else if (SECTION_CATEGORY_REGIST.equalsIgnoreCase(section)) {
            boolean isAdministrator = MemberApi.MEMBER_SECTION_MASTER.equals(userVO.getMbrSe()) || MemberApi.MEMBER_SECTION_CMS.equals(userVO.getMbrSe());
            if (!isAdministrator) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            searchVO.setContsCtgSeq(seq);
            desc = MessageFormat.format(MSG_DESC, new Object[] { "CATEGORY", String.valueOf(seq) });
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            // SMS 발송 요청한 대상(seq)이 실제 DB에 존재하는지 확인
            SmsVO targetVO = smsService.getSmsDestnation(searchVO);
            targetVO.setConnectUrl(configProperty.getProperty("sms.connect.url"));
            if (targetVO == null) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            String[] statusList = { MemberApi.MEMBER_STATUS_ENABLE, MemberApi.MEMBER_STATUS_BLOCKED };
            searchVO.setStatusList(statusList);

            String[] sectionList = { MemberApi.MEMBER_SECTION_SERVICE, MemberApi.MEMBER_SECTION_STORE };
            searchVO.setSectionList(sectionList);

            List<SmsVO> destList = smsService.getSmsDestnationList(searchVO);
            if (destList == null || destList.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            // SMS 발송 구분에 따라 발송 메세지 설정
            String msgBody = "";
            if (SECTION_CONTENT_CONFIRM.equalsIgnoreCase(section)) {
                // 콘텐츠 스토어에서 콘텐츠 등록 당사자 (CP사 관리자, 마스터, CMS 관리자가 해당)는 목록에 별도로 추가
                targetVO.setMsgBody(MSG_CONTENT_CONFIRM_STORE);
                destList.add(0, targetVO);

                // 서비스, 매장 관리자 대상 메세지 설정
                msgBody = MSG_CONTENT_CONFIRM_OMS;
            } else if (SECTION_CATEGORY_REGIST.equalsIgnoreCase(section)) {
                // 서비스, 매장 관리자 대상 메세지 설정
                msgBody = MSG_CATEGORY_REGIST;
            }

            if (enabledLog) {
                System.out.println("[SMS] ========= Send Multiple SMS Start =========");
            }

            String sendMail = MSG_NO_USE;
            MailSender mailSender = null;
            // SMS 발송 기능 활성화 시 이메일 발송 기능은 강제 활성화, 단 이메일 발송 기능은 단독으로 사용 가능
            if (usesAutoSendSms() || usesAutoSendEmail()) {
                if (SECTION_CATEGORY_REGIST.equalsIgnoreCase(section)) {
                    mailSender = new MailSender(configProperty);
                    String subject = MSG_CATEGORY_REGIST_EMAIL;
                    mailSender.setSubject(subject);
                    mailSender.setContent(mailSender.getRegistCategoryContent(targetVO.getFirstCtgNm(), targetVO.getSecondCtgNm()));
                    for (int i = 0; i < destList.size(); i++) {
                        SmsVO smsVo = destList.get(i);
                        smsVo.setConnectUrl(configProperty.getProperty("sms.connect.url"));
                        mailSender.addRecipient(smsVo.getMbrEmail());
                    }
                    boolean sendResult = mailSender.send();
                    sendMail = (sendResult) ? MSG_SUCCESS : MSG_FAIL;
                }
            }

            for (int i = 0; i < destList.size(); i++) {
                SmsVO smsVo = destList.get(i);
                smsVo.setConnectUrl(configProperty.getProperty("sms.connect.url"));
                if (smsVo.getMsgBody() == null || smsVo.getMsgBody().isEmpty()) {
                    smsVo.setMsgBody(msgBody);
                }
                smsVo.setSendDesc(desc);
                smsVo.setCretrId(userVO.getMbrId());
                smsVo.setSendMail(sendMail);

                String[] phonList = smsVo.getDestPhone().split(";");
                for (int j = 0; j < phonList.length; j++) {
                    String resultCode = "0";

                    smsVo.setDestPhone(phonList[j].replaceAll("-", "").trim());
                    if (usesAutoSendSms()) {
                        JsonNode sendResult = SmsSender.send(smsVo);
                        resultCode = (String) sendResult.getObject().get("result_code");
                    }
                    smsVo.setResultCode(resultCode);

                    if (enabledLog) {
                        CommonFnc.voInfo(smsVo);
                    }
                    smsService.insertSentSmsInfo(smsVo);
                }
            }
            if (enabledLog) {
                System.out.println("[SMS] ========= Send Multiple SMS End =========");
            }
            transactionManager.commit(status);
        } catch (Exception e) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findId.serverError");
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
        return rsModel.getModelAndView();
    }

    private boolean usesAutoSend() {
        return "Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.sms")) || "Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.email"));
    }

    private boolean usesAutoSendSms() {
        return "Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.sms"));
    }

    private boolean usesAutoSendEmail() {
        return "Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.email"));
    }
}
