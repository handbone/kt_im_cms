/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.sms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.sms.dao.SmsDAO;
import kt.com.im.cms.sms.vo.SmsVO;

/**
 *
 * SMS 발송 관리에 관한 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.09.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 9. 4.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("SmsService")
public class SmsServiceImpl implements SmsService {

    @Resource(name = "SmsDAO")
    private SmsDAO smsDAO;
    /**
     * SMS 발송 이력  추가
     * 
     * @param SmsVO
     * @return 처리 결과
     */
    @Override
    public int insertSentSmsInfo(SmsVO vo) throws Exception {
        return smsDAO.insertSentSmsInfo(vo);
    }

    /**
     * 발송된 SMS 여부 조회
     * 
     * @param SmsVO
     * @return 처리 결과
     */
    @Override
    public boolean hasSentSmsInfo(SmsVO vo) throws Exception {
        return smsDAO.hasSentSmsInfo(vo);
    }

    /**
     * SMS 수신자 조회
     * 
     * @param SmsVO
     * @return 조회 결과
     */
    @Override
    public SmsVO getSmsDestnation(SmsVO vo) throws Exception {
        return smsDAO.getSmsDestnation(vo);
    }

    /**
     * SMS 수신 목록 조회
     * 
     * @param SmsVO
     * @return 조회 결과
     */
    @Override
    public List<SmsVO> getSmsDestnationList(SmsVO vo) throws Exception {
        return smsDAO.getSmsDestnationList(vo);
    }
}
