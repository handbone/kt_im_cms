/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.keyword.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * Keyword 관리에 관한 컨트롤러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 14.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class KeywordController {

    /**
     * 검색 키워드 관리 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/keyword", method = RequestMethod.GET)
    public ModelAndView searchKeywordList(ModelAndView mv) {
        mv.setViewName("/views/service/keyword/KeywordList");
        return mv;
    }
}