/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.keyword.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.cms.common.dao.mysqlAbstractMapper;
import kt.com.im.cms.keyword.vo.KeywordVO;

/**
 *
 * Keyword 관리에 관한 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 14.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("KeywordDAO")
public class KeywordDAOImpl extends mysqlAbstractMapper implements KeywordDAO {

    /**
     * 검색 키워드 리스트 조회
     *
     * @param KeywordVO
     * @return 검색 키워드 리스트 목록
     */
    @Override
    public List<KeywordVO> keywordList(KeywordVO vo) {
        return selectList("KeywordDAO.selectKeywordList", vo);
    }

    /**
     * 검색 키워드 추가
     *
     * @param KeywordVO
     * @return 검색 키워드 추가 성공 여부
     */
    @Override
    public int keywordInsert(KeywordVO vo) {
        return insert("KeywordDAO.insertKeyword", vo);
    }

    /**
     * 검색 키워드 삭제
     *
     * @param KeywordVO
     * @return 검색 키워드 삭제 여부
     */
    @Override
    public int keywordDelete(KeywordVO vo) {
        return delete("KeywordDAO.deleteKeyword", vo);
    }

}
