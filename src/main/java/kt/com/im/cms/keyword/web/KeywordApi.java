/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.keyword.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.keyword.service.KeywordService;
import kt.com.im.cms.keyword.vo.KeywordVO;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;

/**
 *
 * Keyword 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 14.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class KeywordApi {

    private final static String viewName = "../resources/api/service/keyword/keywordProcess";

    @Resource(name = "KeywordService")
    private KeywordService keywordService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "stateTransactionManager")
    private DataSourceTransactionManager txManager;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    /**
     * 문의사항 등록 및 조회 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/keyword")
    public ModelAndView ketwordProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 로그인 되지 않았거나 Master or CMS 관리자가 아닌 경우 접근 불가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        KeywordVO item = new KeywordVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            String checkString = "";
            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request, "svcSeq,keywords,rankLists,typeLists");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E
            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            String keywords = CommonFnc.emptyCheckString("keywords", request);
            String rankLists = CommonFnc.emptyCheckString("rankLists", request);
            String typeLists = CommonFnc.emptyCheckString("typeLists", request);

            String[] keyword = keywords.split(",");
            String[] rankList = rankLists.split(",");
            String[] typeList = typeLists.split(",");

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            item.setSvcSeq(svcSeq);
            try {
                if (keyword.length > 0) {
                    keywordService.keywordDelete(item);
                    for (int i = 0; i < keyword.length; i++) {
                        item.setKeyword(keyword[i]);
                        item.setRank(Integer.parseInt(rankList[i]));
                        item.setType(typeList[i]);
                        keywordService.keywordInsert(item);
                    }
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                transactionManager.commit(status);
                rsModel.setResultType("keywordUpdate");
                return rsModel.getModelAndView();
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET

            /* Checking Mendatory S */
            // String checkString = CommonFnc.requiredChecking(request, "svcSeq");
            // if (!checkString.equals("")) {
            // rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            // rsModel.setResultMessage("invalid parameter(" + checkString + ")");
            // rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            // return rsModel.getModelAndView();
            // }
            /* Checking Mendatory E */

            int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
            String sord = CommonFnc.emptyCheckString("sord", request);
            String type = CommonFnc.emptyCheckString("type", request);
            String searchString = request.getParameter("searchString") == null ? ""
                    : request.getParameter("searchString");

            item.setSvcSeq(svcSeq);
            item.setSord(sord);
            item.setType(type);
            item.setSearchString(searchString);

            List<KeywordVO> resultItem = keywordService.keywordList(item);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                return rsModel.getModelAndView();
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            rsModel.setData("item", resultItem);
            rsModel.setResultType("keywordList");
            return rsModel.getModelAndView();
        }
        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();

    }

    @RequestMapping(value = "/api/keywordAPI")
    public ModelAndView ketwordAPIProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        // 로그인 되지 않았거나 Master or CMS 관리자가 아닌 경우 접근 불가

        request.setCharacterEncoding("UTF-8");
        KeywordVO item = new KeywordVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET

            String checkString = "";

            // Checking Mendatory S
            checkString = CommonFnc.requiredChecking(request, "storSeq,mbrTokn,svcSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            MemberVO member = new MemberVO();
            int storSeq = CommonFnc.emptyCheckInt("storSeq", request);
            String mbrTokn = CommonFnc.emptyCheckString("mbrTokn", request);
            String encodeToken = mbrTokn.replaceAll(" ", "+");

            encodeToken = mbrTokn.replaceAll(" ", "+");
            member.setStorSeq(storSeq);
            member.setMbrTokn(encodeToken);

            int tokenSearchResult = 0;
            try {
                tokenSearchResult = memberService.searchMemberToken(member);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
                String sord = CommonFnc.emptyCheckString("sord", request);
                String type = CommonFnc.emptyCheckString("type", request);
                String searchString = request.getParameter("searchString") == null ? ""
                        : request.getParameter("searchString");

                item.setSvcSeq(svcSeq);
                item.setSord(sord);
                item.setType(type);
                item.setSearchString(searchString);

                List<KeywordVO> resultItem = keywordService.keywordList(item);
                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
                rsModel.setResultType("keywordList");
            }
        }
        return rsModel.getModelAndView();

    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_SERVICE.equals(mbrSe);
    }
}
