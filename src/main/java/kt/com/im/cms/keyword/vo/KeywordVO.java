/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.cms.keyword.vo;

import java.io.Serializable;

/**
 *
 * Keyword 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.06.14
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 6. 14.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class KeywordVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -2984187831563945539L;

    /** 서비스 번호 */
    private int svcSeq;

    /** 검색명 */
    private String searchString;

    /** 키워드 번호 */
    private int keywordSeq;

    /** 키워드 명 */
    private String keyword;

    /** 우선 순위 (정렬) */
    private int rank;

    /** 정렬 방법 */
    private String sord;

    /** 검색 키워드 유형 */
    private String type;

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public int getKeywordSeq() {
        return keywordSeq;
    }

    public void setKeywordSeq(int keywordSeq) {
        this.keywordSeq = keywordSeq;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

}
