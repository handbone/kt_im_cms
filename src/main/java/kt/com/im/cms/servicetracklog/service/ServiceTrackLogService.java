/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicetracklog.service;

import java.util.List;

import kt.com.im.cms.servicetracklog.vo.ServiceTrackLogVO;

/**
 *
 * 서비스 트랙 로그 관련 서비스 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 4. 5.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public interface ServiceTrackLogService {

    int insertSvcTrackLog(ServiceTrackLogVO vo) throws Exception;

    List<ServiceTrackLogVO> svcTrackLogList(ServiceTrackLogVO vo) throws Exception;

    int svcTrackLogListTotalCount(ServiceTrackLogVO vo) throws Exception;

}
