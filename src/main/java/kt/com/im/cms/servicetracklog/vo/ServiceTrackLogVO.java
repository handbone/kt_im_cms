/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicetracklog.vo;

import java.io.Serializable;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 4. 5.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class ServiceTrackLogVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 935816937475010695L;

    /** 트랙 로그 이력 번호 */
    private int trackLogSeq;

    /** 서비스 번호 */
    private int svcSeq;

    /** 서비스 명 */
    private String svcNm;

    /** 앱 구분 */
    private String appType;

    /** 인증회원 아이디 */
    private String uid;

    /** 실행 구분 */
    private String excuteType;

    /** 실행 상세 구분 */
    private String detailType;

    /** 등록 일시 */
    private String regDt;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    public int getTrackLogSeq() {
        return trackLogSeq;
    }

    public void setTrackLogSeq(int trackLogSeq) {
        this.trackLogSeq = trackLogSeq;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getExcuteType() {
        return excuteType;
    }

    public void setExcuteType(String excuteType) {
        this.excuteType = excuteType;
    }

    public String getDetailType() {
        return detailType;
    }

    public void setDetailType(String detailType) {
        this.detailType = detailType;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
