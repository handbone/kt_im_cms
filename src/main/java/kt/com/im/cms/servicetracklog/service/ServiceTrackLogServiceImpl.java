/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicetracklog.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.cms.servicetracklog.dao.ServiceTrackLogDAO;
import kt.com.im.cms.servicetracklog.vo.ServiceTrackLogVO;

/**
 *
 * 서비스 트랙 로그 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2019. 4. 5.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("ServiceTrackLogService")
public class ServiceTrackLogServiceImpl implements ServiceTrackLogService {

    @Resource(name = "ServiceTrackLogDAO")
    private ServiceTrackLogDAO svcTrackLogDAO;

    /**
     * 서비스 트랙 로그 등록
     * @param ServiceTrackLogVO
     * @return 등록 결과
     */
    @Override
    public int insertSvcTrackLog(ServiceTrackLogVO vo) throws Exception {
        return svcTrackLogDAO.insertSvcTrackLog(vo);
    }

    /**
     * 서비스 트랙 로그 목록 조회
     * @param ServiceTrackLogVO
     * @return 검색 조건에 부합하는 트랙 로그 목록
     */
    @Override
    public List<ServiceTrackLogVO> svcTrackLogList(ServiceTrackLogVO vo) throws Exception {
        return svcTrackLogDAO.svcTrackLogList(vo);
    }

    /**
     * 서비스 트랙 로그 목록 합계 조회
     * @param ServiceTrackLogVO
     * @return 검색 조건에 부합하는 트랙 로그 목록 합계
     */
    public int svcTrackLogListTotalCount(ServiceTrackLogVO vo) throws Exception {
         return svcTrackLogDAO.svcTrackLogListTotalCount(vo);
    }

}
