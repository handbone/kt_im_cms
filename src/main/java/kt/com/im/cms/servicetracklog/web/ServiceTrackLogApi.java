/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.servicetracklog.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.servicetracklog.service.ServiceTrackLogService;
import kt.com.im.cms.servicetracklog.vo.ServiceTrackLogVO;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.CommonUtil;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.IntegratedMemberVO;
import kt.com.im.cms.member.vo.MemberVO;

@Controller
public class ServiceTrackLogApi {

    private final static String viewName = "../resources/api/log/serviceTrackLogProcess";

    @Resource(name="ServiceTrackLogService")
    private ServiceTrackLogService svcTrackLogService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private CommonUtil comnUtil;

    /**
     * 외부 서비스 앱의 사용자 사용 로그 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/serviceTrackLogAPI")
    public ModelAndView serviceTrackLog(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            String invalidParams = CommonFnc.requiredChecking(request,"mbrTokn,storSeq,svcSeq,appType,excuteType,detailType");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            if (!comnUtil.validMbrToken(request)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            return addTrackLog(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addTrackLog(HttpServletRequest request, ResultModel rsModel) throws Exception {
        int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);
        if (svcSeq == 0) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(svcSeq)");
            return rsModel.getModelAndView();
        }

        String appType = CommonFnc.emptyCheckString("appType", request);
        // appType이 없을 경우 오프라인 공지사항 요청으로 처리, SA(서비스앱) 또는 MA(미러링앱)의 값이 있을 경우 온라인 공지사항 요청으로 처리
        if (!appType.equals("") && !appType.equals("SA") && !appType.equals("MA")) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(appType)");
            return rsModel.getModelAndView();
        }

        String uid = CommonFnc.emptyCheckString("uid", request);
        if (!uid.isEmpty()) {
            String validateOptionalParams = "uid:{" + Validator.MAX_LENGTH + "=15}";
            String invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            // 현재 등록된 회원인지 여부 확인
            IntegratedMemberVO vo = new IntegratedMemberVO();
            vo.setSvcSeq(svcSeq);
            vo.setUid(uid);

            MemberVO resultMemVO = memberService.selectIntegratedMember(vo);
            boolean isExisted = (resultMemVO != null);
            if (!isExisted) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage("uid is not exist.");
                return rsModel.getModelAndView();
            }
        }

        String excuteType = CommonFnc.emptyCheckString("excuteType", request);
        // 실행 구분 길이 값 체크
        if (excuteType != null && !excuteType.isEmpty()) {
            String validateOptionalParams = "excuteType:{" + Validator.MAX_LENGTH + "=50}";
            String invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        String detailType = CommonFnc.emptyCheckString("detailType", request);
        // 실행 상세 구분 길이 값 체크
        if (detailType != null && !detailType.isEmpty()) {
            String validateOptionalParams = "detailType:{" + Validator.MAX_LENGTH + "=50}";
            String invalidParams = Validator.validate(request, validateOptionalParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
        }

        ServiceTrackLogVO item = new ServiceTrackLogVO();
        item.setSvcSeq(svcSeq);
        item.setAppType(appType);
        item.setUid(uid);
        item.setExcuteType(excuteType);
        item.setDetailType(detailType);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);

        try {
            int result = svcTrackLogService.insertSvcTrackLog(item);
            if (result > 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                transactionManager.commit(status);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                transactionManager.rollback(status);
            }
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

}
