/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.cms.pos.web;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.cms.common.util.AesAlgorithm;
import kt.com.im.cms.common.util.CommonFnc;
import kt.com.im.cms.common.util.ConfigProperty;
import kt.com.im.cms.common.util.ResultModel;
import kt.com.im.cms.common.util.Validator;
import kt.com.im.cms.device.service.DeviceService;
import kt.com.im.cms.member.service.MemberService;
import kt.com.im.cms.member.vo.LoginVO;
import kt.com.im.cms.member.vo.MemberVO;
import kt.com.im.cms.member.web.MemberApi;
import kt.com.im.cms.product.service.ProductService;
import kt.com.im.cms.product.vo.ProductVO;
import kt.com.im.cms.reservate.service.ReservateService;
import kt.com.im.cms.reservate.vo.ReservateVO;
import kt.com.im.cms.stats.service.StatsService;
import kt.com.im.cms.stats.vo.StatsVO;

/**
 *
 * POS와 연동되는 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 7. 23.   A2TEC      최초생성
 *
 *      </pre>
 */

@Controller
public class PosApi {

    @Resource(name = "ProductService")
    private ProductService productService;

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "StatsService")
    private StatsService statsService;

    @Resource(name = "ReservateService")
    private ReservateService reservateService;

    @Resource(name = "DeviceService")
    private DeviceService deviceService;

    @Autowired
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private ConfigProperty configProperty;

    private final static String viewName = "../resources/api/pos/posProcess";

    /**
     * POS와 연동하여 상품 목록 및 상세보기 정보를 제공하는 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/productAPI")
    public ModelAndView productAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        ProductVO item = new ProductVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            MemberVO mItem = new MemberVO();

            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "TOKEN");
            if (!checkString.equals("")
                    || (request.getParameter("STORE_CODE") == null && request.getParameter("PROD_ID") == null)) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                if (!checkString.equals("")) {
                    checkString += ",";
                }
                if (request.getParameter("STORE_CODE") == null && request.getParameter("PROD_ID") == null) {
                    checkString += "STORE_CODE OR PROD_ID";
                }

                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String mbrTokn = request.getParameter("TOKEN") == null ? "" : request.getParameter("TOKEN");
            String storCode = request.getParameter("STORE_CODE") == null ? "" : request.getParameter("STORE_CODE");
            String encodeToken = "";
            encodeToken = mbrTokn.replaceAll(" ", "+");

            mItem.setStorCode(storCode);
            mItem.setMbrTokn(encodeToken);

            int tokenSearchResult = 0;
            tokenSearchResult = memberService.searchMemberToken(mItem);

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                String gsrProdCode = request.getParameter("PROD_ID") == null ? "" : request.getParameter("PROD_ID");
                if (gsrProdCode != "") {
                    item.setGsrProdCode(gsrProdCode);
                    item.setStorCode(storCode);
                    item.setProdSeq(0);
                    item.setUseYn("Y");
                    ProductVO resultItem = productService.productDetail(item);
                    if (resultItem == null) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    } else {
                        List<ProductVO> resultCategoryItem = productService.productCategoryList(item);
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        for (int i = 0; i < resultCategoryItem.size(); i++) {
                            if (resultCategoryItem.get(i).getLmtCnt() == -1) { // LmtCnt가 -1인 경우 무제한, 0인 경우 사용불가
                                resultCategoryItem.get(i).setLmtCnt(100); // 무제한일 경우 100회로 설정
                            }
                        }
                        rsModel.setData("item", resultItem);
                        rsModel.setData("CItem", resultCategoryItem);
                    }
                    rsModel.setData("resultType", "productDetail");
                } else {
                    item.setSidx("PROD_SEQ");
                    item.setSord("");
                    item.setOffset(1);
                    item.setLimit(10000);
                    item.setStorSeq(0);
                    item.setStorCode(storCode);
                    item.setUseYn("Y");

                    List<ProductVO> resultItem = productService.productList(item);
                    if (resultItem.isEmpty()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setData("item", resultItem);
                    }
                    rsModel.setData("resultType", "productsListAPI_POS");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 결제 정보(승인, 취소) 및 태그 사용 여부 조회
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/posAPI")
    public ModelAndView addNewWorker(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            @RequestBody String jsonString)
            throws UnsupportedEncodingException, ParseException, UnknownHostException, SocketException {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        ReservateVO item = new ReservateVO();
        ProductVO pItem = new ProductVO();
        StatsVO sItem = new StatsVO();
        MemberVO mItem = new MemberVO();
        final Logger log = LogManager.getLogger();
        InetAddress local = InetAddress.getLocalHost();

        String ip = request.getHeader("X-Forwarded-For");
        /*
         * if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         * ip = request.getHeader("Proxy-Client-IP");
         * }
         * if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         * ip = request.getHeader("WL-Proxy-Client-IP");
         * }
         * if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         * ip = request.getHeader("HTTP_CLIENT_IP");
         * }
         * if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         * ip = request.getHeader("HTTP_X_FORWARDED_FOR");
         * }
         * if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
         * ip = request.getRemoteAddr();
         * }
         */

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj = (JSONObject) jsonParser.parse(jsonString);

        // JSONArray payList = (JSONArray) jsonObj.get("PAY_LIST");

        String storCode = (String) jsonObj.get("STORE_CODE") == null ? "" : (String) jsonObj.get("STORE_CODE");
        String token = (String) jsonObj.get("TOKEN") == null ? "" : (String) jsonObj.get("TOKEN");
        String customerGender = (String) jsonObj.get("CUSTOMER_GENDER") == null ? ""
                : (String) jsonObj.get("CUSTOMER_GENDER"); // 성별
        String date = (String) jsonObj.get("DATE") == null ? "" : (String) jsonObj.get("DATE"); // 날짜
        int type = (String) jsonObj.get("TYPE") == null ? 0 : Integer.parseInt((String) jsonObj.get("TYPE")); // 취소,승인

        String checkString = "";
        if (token.equals("")) {
            checkString += "TOKEN";
        }
        if (storCode.equals("")) {
            if (!checkString.equals(""))
                checkString += ",";
            checkString += "STORE_CODE";
        }
        if (customerGender.equals("")) {
            if (!checkString.equals(""))
                checkString += ",";
            checkString += "CUSTOMER_GENDER";
        }
        if (date.equals("")) {
            if (!checkString.equals(""))
                checkString += ",";
            checkString += "DATE";
        }
        if (type == 0) {
            if (!checkString.equals(""))
                checkString += ",";
            checkString += "TYPE";
        }

        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage("invalid parameter(" + checkString + ")");
            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            log.warn("[{}] : invalid parameter :{}", ip, checkString);
            return rsModel.getModelAndView();
        }

        String encodeToken = "";
        encodeToken = token.replaceAll(" ", "+");
        mItem.setStorCode(storCode);
        mItem.setMbrTokn(encodeToken);
        int tokenSearchResult = 0;
        try {
            tokenSearchResult = memberService.searchMemberToken(mItem);
        } catch (Exception e) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        if (tokenSearchResult == 0) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage("Not Token Info");
            rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            log.warn("[{}] : Not Token Info [{}]", ip, encodeToken);
        } else {
            // PAYMENT
            int payPrice = 0;
            if ((String) jsonObj.get("NET_AMOUNT_TOTAL") == null || jsonObj.get("NET_AMOUNT_TOTAL").equals("")) {
                payPrice = 0;
            } else {
                payPrice = Integer.parseInt((String) jsonObj.get("NET_AMOUNT_TOTAL"));
            }

            String paymentType = (String) jsonObj.get("PAY_METHOD") == null ? "" : (String) jsonObj.get("PAY_METHOD"); // 현금,카드
            String cardNumber = (String) jsonObj.get("CREDIT_NUMBER") == null ? ""
                    : (String) jsonObj.get("CREDIT_NUMBER"); // 카드번호
            String cardCompany = (String) jsonObj.get("CREDIT_COMPANY") == null ? ""
                    : (String) jsonObj.get("CREDIT_COMPANY"); // 카드회사
            String casherId = (String) jsonObj.get("CASHIER_ID") == null ? "" : (String) jsonObj.get("CASHIER_ID"); // 출납원ID
            String posId = (String) jsonObj.get("POS_ID") == null ? "" : (String) jsonObj.get("POS_ID"); // posId
            String receiptNo = (String) jsonObj.get("RECEIPT_NO") == null ? "" : (String) jsonObj.get("RECEIPT_NO"); // 영수증번호
            String cancelPosId = (String) jsonObj.get("CANCEL_POS_ID") == null ? ""
                    : (String) jsonObj.get("CANCEL_POS_ID"); // 삭제시포스번호
            String cancelReceiptNo = (String) jsonObj.get("CANCEL_RECEIPT_NO") == null ? ""
                    : (String) jsonObj.get("CANCEL_RECEIPT_NO"); // 삭제시원영수증번호

            // if (payList.size() != 0) {
            //
            // for (int i = 0; i < payList.size(); i++) {
            // StatsVO payDtl = new StatsVO();
            // JSONObject jsonObjchild = (JSONObject) payList.get(i);
            //
            // // PAYMENT_DETAIL
            // String payMethod = (String) jsonObjchild.get("PAY_METHOD") == null ? "" : (String)
            // jsonObjchild.get("PAY_METHOD"); // 할인 가격
            //
            // int payAmount = (String) jsonObjchild.get("PAY_AMOUNT") == null ? 0 : Integer.parseInt((String)
            // jsonObjchild.get("PAY_AMOUNT")); // 할인 가격
            //
            // payDtl.setSettlMthd(payMethod);
            // payDtl.setPayAmount(payAmount);
            //
            // int result = statsService.searchSettlList(sItem); // 결제 리스트 조회 (기존 PayListSearch)
            // log.debug("searchSettlList result : [{}]", result);
            // if (result != 0) {
            // rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            // rsModel.setResultMessage("Exist Info");
            // rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            // log.warn("[{}] : Exist Info", ip);
            // return rsModel.getModelAndView();
            // }
            //
            // }
            //
            //
            //
            // paymentType = "MXTR";
            // }

            // 결제 정보 등록
            sItem.setCstmrGrupCode(customerGender);
            sItem.setTotSettlAmt(payPrice);
            sItem.setSettlMthd(paymentType);
            sItem.setCrdtCardNo(cardNumber);
            sItem.setCrdtCardCompny(cardCompany);
            sItem.setSettlDt(date);
            sItem.setSelerId(casherId);
            sItem.setPosId(posId);
            sItem.setReptNo(receiptNo);
            sItem.setCanclPosId(cancelPosId);
            sItem.setCanclReptNo(cancelReceiptNo);
            sItem.setSettlType(type);
            sItem.setStorCode(storCode);
            sItem.setTagId("");

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = statsService.searchSettlList(sItem); // 결제 리스트 조회 (기존 PayListSearch)
                log.debug("searchSettlList result : [{}]", result);
                if (result != 0) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                    rsModel.setResultMessage("Exist Info");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    log.warn("[{}] : Exist Info", ip);
                    return rsModel.getModelAndView();
                }

                log.warn(
                        "[{}] : \n  INSERT INTO IM_PAYMENT (STORE_CODE,POS_ID,REPT_NO,SETTL_DT,SETTL_TYPE,CANCL_POS_ID,CANCL_REPT_NO,CSTMR_GRUP_CODE,SELER_ID,TOT_SETTL_AMT,SETTL_MTHD,CRDT_CARD_NO,CRDT_CARD_CMPNY) VALUES ('{}','{}','{}',TO_timestamp('{}','YYYY/MM/DD HH24:MI:SS')::timestamp,'{}','{}','{}','{}','{}','{}','{}','{}','{}')",
                        ip, storCode, posId, receiptNo, date, type, cancelPosId, cancelReceiptNo, customerGender,
                        casherId, payPrice, paymentType, cardNumber, cardCompany);

                // insert
                int insertResult = statsService.insertSettlInfo(sItem); // 결제 정보 생성 (기존 insertPayInfo)
                if (insertResult == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    log.warn("[{}] : insertPaySuccess", ip);
                    if (type != 2) { // 결제 태그 승인
                        JSONArray ja = (JSONArray) jsonObj.get("TAG_LIST");

                        for (int i = 0; i < ja.size(); i++) {
                            JSONObject jsonObjchild = (JSONObject) ja.get(i);

                            // PAYMENT_DETAIL
                            String sleNo = (String) jsonObjchild.get("SEQ") == null ? ""
                                    : (String) jsonObjchild.get("SEQ"); // 발매 순서
                            int discount = (String) jsonObjchild.get("DISCOUNT") == null ? 0
                                    : Integer.parseInt((String) jsonObjchild.get("DISCOUNT")); // 할인 가격
                            int netAmount = (String) jsonObjchild.get("NET_AMOUNT") == null ? 0
                                    : Integer.parseInt((String) jsonObjchild.get("NET_AMOUNT")); // 전체가격
                            String tagId = (String) jsonObjchild.get("TAG_ID") == null ? ""
                                    : (String) jsonObjchild.get("TAG_ID"); // tag Id
                            String gsrProdCode = (String) jsonObjchild.get("PROD_ID") == null ? ""
                                    : (String) jsonObjchild.get("PROD_ID"); // 상품 Id
                            String prodPrice = (String) jsonObjchild.get("PROD_PRICE") == null ? ""
                                    : (String) jsonObjchild.get("PROD_PRICE"); // 상품 가격

                            String apdTime = (String) jsonObjchild.get("APD_TIME") == null ? ""
                                    : (String) jsonObjchild.get("APD_TIME"); // 상품 가격

                            if (apdTime == "") {
                                apdTime = "0";
                            }
                            item = new ReservateVO();
                            item.setTagId(tagId);
                            item.setDiscount(discount);
                            item.setGsrProdCode(gsrProdCode);
                            item.setProdPrc(Integer.parseInt(prodPrice));
                            item.setTotSettlAmt(netAmount);
                            item.setSleNo(Integer.parseInt(sleNo));
                            item.setStorCode(storCode);
                            item.setApdTime(Integer.parseInt(apdTime));

                            // 태그발매 S
                            if (!tagId.equals("")) {
                                sItem.setTagId(tagId);
                                int searchResult = statsService.searchSettlList(sItem);
                                if (searchResult != 0 && type != 3) {
                                    rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
                                    rsModel.setResultMessage("Exist TAG_ID");
                                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                                    log.warn("[{}] : Exist TAG_ID : TAG_ID :'{}', STORE_CODE : '{}'", ip, tagId,
                                            storCode);
                                    return rsModel.getModelAndView();
                                }

                                item.setRegDt("");

                                // int year =
                                // Integer.parseInt(tagId.substring(19, 4));
                                int month = Integer.parseInt(tagId.substring(22, 24));
                                int day = Integer.parseInt(tagId.substring(24, 26));
                                int hour = Integer.parseInt(tagId.substring(26, 28));
                                int time = Integer.parseInt(tagId.substring(28, 30));

                                if (month < 1 || month > 12 || day < 1 || day > 31 || hour < 0 || hour > 24 || time > 59
                                        || time < 0) {
                                    item.setRegDt("sysdate");
                                }

                                int count = reservateService.existTagCount(item);

                                if (count == 0 && type == 1) {
                                    item.setSettlSeq(1);
                                    reservateService.reservateInsert(item);

                                    // 상품 카테고리 조회
                                    pItem = new ProductVO();
                                    pItem.setGsrProdCode(gsrProdCode);
                                    pItem.setStorCode(storCode);
                                    List<ProductVO> resultCategoryItem = productService.productCategoryList(pItem);
                                    for (int j = 0; j < resultCategoryItem.size(); j++) {
                                        item.setProdSeq(resultCategoryItem.get(j).getProdSeq());
                                        item.setDevCtgNm(resultCategoryItem.get(j).getDevCtgNm());
                                        item.setLmtCnt(resultCategoryItem.get(j).getLmtCnt());
                                        item.setDeduction(resultCategoryItem.get(j).getDeduction());
                                        item.setTagSeq(0);
                                        reservateService.reservateCategoryInsert(item);
                                    }
                                } else {
                                    if (type == 3) {
                                        item.setLoginId(casherId);
                                        ReservateVO ritem = reservateService.reservateUsedInfo(item);
                                        /* 사용 만료 및 반품 태그 존재시, 추가시간 불가능 */
                                        if (ritem.getUseSttus().equals("B") /* || ritem.getUseSttus().equals("E") */) {
                                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                                            switch (ritem.getUseSttus()) {
                                                case "B":
                                                    rsModel.setResultMessage("Request TAG in [Return Tag] exist ");
                                                    break;
                                                /*
                                                 * case "E":
                                                 * rsModel.setResultMessage("Request TAG in [Expiration Tag] exist ");
                                                 * break;
                                                 */
                                            }
                                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                                            return rsModel.getModelAndView();
                                        }

                                        reservateService.reservateAddTime(item); // 추가 시간 업데이트
                                    } else {
                                        // 결제 정보 업데이트 (기존 reservatePayUpdate)
                                        reservateService.reservateSettlUpdate(item);
                                    }
                                }
                            }
                            // 태그발매 E

                            sItem = new StatsVO();
                            sItem.setSleNo(Integer.parseInt(sleNo));
                            sItem.setTagId(tagId);
                            sItem.setGsrProdCode(gsrProdCode);
                            sItem.setDiscount(discount);
                            sItem.setApdTime(Integer.parseInt(apdTime));
                            // Sitem.setDiscountInfo(discountInfo);
                            sItem.setProdPrc(Integer.parseInt(prodPrice));
                            sItem.setSettlAmt(netAmount);
                            // sItem.setSeq(Integer.parseInt(seq));
                            statsService.insertSettlDetail(sItem); // 결제 상세 정보 생성 (기존 insertPayDetail)
                        }
                        log.warn("[{}] : TAG_ID Insert Success", ip);
                    } else if (type == 2) { // 결제 태그 취소 & 삭제(?)
                        // statsService.deletePayDetail(Sitem);
                        // 반품 태그관련 목록
                        StatsVO setInfo = statsService.searchSettlInfo(sItem);

                        if (setInfo != null) {
                            if (setInfo.getSettlType() == 3) {
                                setInfo.setSelerId(casherId);
                                statsService.subtractApdTime(setInfo);
                            } else {
                                List<StatsVO> resetitem = statsService.searchResetTicket(sItem);
                                if (resetitem.size() != 0) {
                                    // 추가시간 환불확인
                                    for (int i = 0; i < resetitem.size(); i++) {
                                        ReservateVO rItem = new ReservateVO();
                                        rItem.setTagSeq(resetitem.get(i).getTagSeq());
                                        rItem.setUseYn("N");
                                        rItem.setUseSttus("B");
                                        rItem.setRegDt(date);
                                        ReservateVO resultItem = reservateService.reservateDetail(rItem);
                                        int updateResult = reservateService.reservateUpdate(rItem);

                                        if (updateResult == 1) {
                                            // 카테고리 삭제
                                            reservateService.reservateCategoryDelete(rItem);

                                            // 카테고리 입력
                                            pItem = new ProductVO();
                                            pItem.setGsrProdCode("");
                                            pItem.setProdSeq(resultItem.getProdSeq());

                                            List<ProductVO> resultCategoryItem = productService
                                                    .productCategoryList(pItem);
                                            for (int j = 0; j < resultCategoryItem.size(); j++) {
                                                item = new ReservateVO();
                                                item.setProdSeq(resultCategoryItem.get(j).getProdSeq());
                                                item.setDevCtgNm(resultCategoryItem.get(j).getDevCtgNm());
                                                item.setLmtCnt(resultCategoryItem.get(j).getLmtCnt());
                                                item.setDeduction(resultCategoryItem.get(j).getDeduction());
                                                item.setTagSeq(resetitem.get(i).getTagSeq());
                                                reservateService.reservateCategoryInsert(item);
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                log.warn("Insert Fail");
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * TAG 사용여부 조회(API)
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/posTagAPI", method = { RequestMethod.GET })
    public ModelAndView posAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        request.setCharacterEncoding("UTF-8");
        ModelAndView mv = new ModelAndView();
        ReservateVO item = new ReservateVO();
        ProductVO Pitem = new ProductVO();
        StatsVO Sitem = new StatsVO();
        MemberVO mItem = new MemberVO();

        String tagId = request.getParameter("TAG_ID") == null ? "" : request.getParameter("TAG_ID");
        String storeCode = request.getParameter("STORE_CODE") == null ? "" : request.getParameter("STORE_CODE");

        String token = request.getParameter("TOKEN") == null ? "" : request.getParameter("TOKEN");

        /* Checking Mendatory S */
        String checkString = CommonFnc.requiredChecking(request, "TAG_ID,STORE_CODE,TOKEN");
        if (!checkString.equals("")) {
            mv.addObject("resultCode", "1400");
            mv.addObject("resultMsg", "invalid parameter(" + checkString + ")");
            mv.setViewName(viewName);
            response.setStatus(400);
            return mv;
        }
        /* Checking Mendatory E */

        String encodeToken = "";
        encodeToken = token.replaceAll(" ", "+");
        mItem.setStorCode(storeCode);
        mItem.setMbrTokn(encodeToken);

        int tokenSearchResult = 0;
        tokenSearchResult = memberService.searchMemberToken(mItem);
        if (tokenSearchResult == 0) {
            mv.addObject("resultCode", "1500");
            mv.addObject("resultMsg", "Not Token Info");
            response.setStatus(401);
        } else {
            item.setTagId(tagId);
            item.setStorCode(storeCode);

            ReservateVO resultItem = reservateService.reservateUsedInfo(item);

            if (resultItem != null) {
                if (resultItem.getSttlType().equals("Y")) {
                    resultItem.setUseYn(resultItem.getResetYn());
                }
            }

            if (resultItem == null) {
                mv.addObject("resultCode", "1010");
                mv.addObject("resultMsg", "No data");
            } else {
                mv.addObject("resultCode", "1000");
                mv.addObject("resultMsg", "Success");
                mv.addObject("resultType", "reservateUseInfo");
                mv.addObject("item", resultItem);
            }

        }

        mv.setViewName(viewName);
        return mv;
    }

    /**
     * 아이디/비밀번호 이용 사용자 정보 조회(API)
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/posMemberInfo")
    public ModelAndView memberInfoProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String validateParams = "USER_ID;USER_PWD";
            String invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST,
                        "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            LoginVO vo = new LoginVO();

            vo.setMbrId(request.getParameter("USER_ID"));

            String memberPwd = MemberApi.makeEncryptPassword(request.getParameter("USER_PWD"),
                    memberService.getSalt(vo));
            vo.setMbrPwd(memberPwd);
            vo.setMbrSe(MemberApi.MEMBER_SECTION_STORE);

            LoginVO memberResult = memberService.getLoginInfo(vo);
            if (memberResult == null) {
                rsModel.setNoData();
                rsModel.setResultMessage("Please check your USER ID and PASSWORD");
                return rsModel.getModelAndView();
            }

            if (!MemberApi.MEMBER_STATUS_ENABLE.equals(memberResult.getMbrSttus())) {
                rsModel.setNoData();
                rsModel.setResultMessage(ResultModel.MESSAGE_UNAVAILABLE_MEMBER);
                return rsModel.getModelAndView();
            }

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);
            try {
                String token = "";
                token = this.generateToken(memberResult.getMbrId());
                String oldToken = memberResult.getMbrTokn();
                if (oldToken == null) {
                    vo.setMbrTokn(token);
                } else {
                    vo.setMbrTokn(oldToken);
                    token = oldToken;
                }

                // 토큰 유효기간 만료된 토큰에 대해서 갱신 처리
                int resultCode = memberService.updateMemberToken(vo);
                if (resultCode != 1) {
                    token = oldToken;
                }
                token = token.replaceAll(System.getProperty("line.separator"), "");

                rsModel.setResultType("memberInfo");
                rsModel.setData("item", memberResult);
                rsModel.setData("token", token);
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
            return rsModel.getModelAndView();
        }

        return rsModel.getModelAndView();
    }

    /**
     * POS와 연동하여 매장에서 결제정보 리스트
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/payAPI")
    public ModelAndView payAPI(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        request.setCharacterEncoding("UTF-8");
        StatsVO item = new StatsVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            // Checking Mendatory S
            String checkString = CommonFnc.requiredChecking(request, "TOKEN,STORE_CODE");
            if (!checkString.equals("")) {
                rsModel.setResultMessage("invalid parameter(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String mbrTokn = request.getParameter("TOKEN") == null ? "" : request.getParameter("TOKEN");
            String storCode = request.getParameter("STORE_CODE") == null ? "" : request.getParameter("STORE_CODE");
            String encodeToken = "";
            encodeToken = mbrTokn.replaceAll(" ", "+");

            MemberVO mItem = new MemberVO();

            mItem.setStorCode(storCode);
            mItem.setMbrTokn(encodeToken);

            int tokenSearchResult = 0;
            tokenSearchResult = memberService.searchMemberToken(mItem);

            if (tokenSearchResult == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage("Not Token Info");
                rsModel.setStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            } else {
                item.setSidx("SETTL_SEQ");
                item.setStorCode(storCode);

                List<StatsVO> resultItem = statsService.payDetailLst(item);
                List<StatsVO> resultItemCopy = new ArrayList<StatsVO>();

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                } else {
                    for (int i = 0; i < resultItem.size(); i++) {
                        StatsVO Sitem = new StatsVO();
                        Sitem = resultItem.get(i);
                        Sitem.setTagInfo(statsService.payTagList(Sitem));
                        resultItemCopy.add(Sitem);
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setData("item", resultItem);
                }
                rsModel.setData("resultType", "paymentList");
            }

        }
        return rsModel.getModelAndView();
    }

    /**
     * token 생성 함수
     *
     * @param id
     * @return String
     * @exception Exception
     */
    public String generateToken(String id) throws Exception {
        return AesAlgorithm.encrypt(id + new Date().toString(), configProperty.getPropertyAfterDecrypt("aes.key"));
    }
}
