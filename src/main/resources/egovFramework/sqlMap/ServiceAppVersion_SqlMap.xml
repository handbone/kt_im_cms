<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<mapper namespace="ServiceAppVersionDAO">
    <sql id="listWhere">
        WHERE
        <if test="svcSeq gt 0">
            A.SVC_SEQ  = #{svcSeq}
        </if>
    </sql>

<!--
    SQL File Name : ServiceAppVersion_SqlMap.xml
    Description : 서비스 앱 버전 정보 관리
    Modification Information
       수정일 수정자 Version Query Id
    ──── ──── ─────── ────────────
    2019.02.12 허용수 1.0 최초 생성
-->

    <!-- 쿼리명 : 서비스 앱 버전 등록
                설명 : 서비스 앱 버전을 등록하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2019.02.12 허용수 최초생성
                작성자: 허용수
                최초작성일 : 2019.02.12
     -->
    <insert id="insertServiceAppVersion" parameterType="ServiceAppVersionVO">
        INSERT INTO IM_SERVICE_APP_VERSION
        (
            SVC_SEQ,
            REGR_ID,
            REG_DT,
            APP_TYPE,
            SVC_APP_VER,
            DL_STORE_TYPE,
            DL_STORE_PATH,
            USE_YN
            <if test="uptSbst != null">
                , UPT_SBST
            </if>
        )
        VALUES
        (
            #{svcSeq},
            #{regrId},
            NOW(),
            #{appType},
            #{svcAppVer},
            #{dlStoreType},
            #{dlStorePath},
            #{useYn}
            <if test="uptSbst != null">
                , #{uptSbst}
            </if>
        )
    </insert>

    <!-- 쿼리명 : 서비스 앱 버전 상세 정보
                설명 : 서비스 앱 버전 정보를 조회 하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2019.04.12 이화진 최초생성
                작성자: 이화진
                최초작성일 : 2019.04.12
     -->
     <select id="serviceAppVersionInfo" parameterType="ServiceAppVersionVO" resultType="ServiceAppVersionVO">
        SELECT
            SVC_APP_VER_SEQ,
            SVC_SEQ,
            SVC_NM,
            SVC_TYPE,
            APP_TYPE,
            SVC_APP_VER,
            DL_STORE_TYPE,
            DL_STORE_PATH,
            REGR_ID,
            MBR_NM,
            REG_DT,
            AMDR_ID,
            AMD_DT,
            UPT_SBST,
            USE_YN
        FROM(
            SELECT
                A.SVC_APP_VER_SEQ,
                A.SVC_SEQ,
                B.SVC_NM,
                B.SVC_TYPE,
                A.APP_TYPE,
                A.SVC_APP_VER,
                C.COMN_CD_NM as DL_STORE_TYPE,
                A.DL_STORE_PATH,
                A.REGR_ID,
                M.MBR_NM,
                TO_CHAR(A.REG_DT, 'YYYY-MM-DD') as REG_DT,
                A.AMDR_ID,
                TO_CHAR(A.AMD_DT, 'YYYY-MM-DD') as AMD_DT,
                A.UPT_SBST,
                A.USE_YN
            FROM
                IM_SERVICE_APP_VERSION A
                INNER JOIN IM_SERVICE B ON A.SVC_SEQ = B.SVC_SEQ AND B.DEL_YN = 'N' AND B.SVC_TYPE = 'ON'
                LEFT JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'APP_DL_STORE' AND A.DL_STORE_TYPE = C.COMN_CD_VALUE
                LEFT JOIN IM_MEMBER M ON M.MBR_ID = A.REGR_ID
            WHERE 
                A.SVC_APP_VER_SEQ = #{svcAppVerSeq}
        ) T
    </select>

    <!-- 쿼리명 : 서비스 앱 버전 리스트 목록
                설명 : 서비스 앱 버전목록을 조회 하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2019.04.12 이화진 최초생성
                작성자: 이화진
                최초작성일 : 2019.04.12
     -->
     <select id="serviceAppVersionList" parameterType="ServiceAppVersionVO" resultType="ServiceAppVersionVO">
     SELECT
            SVC_APP_VER_SEQ,
            SVC_SEQ,
            SVC_NM,
            SVC_TYPE,
            APP_TYPE,
            SVC_APP_VER,
            DL_STORE_TYPE,
            DL_STORE_PATH,
            REGR_ID,
            MBR_NM,
            TO_CHAR(REG_DT, 'YYYY-MM-DD') AS REG_DT,
            UPT_SBST,
            USE_YN,
            NUM
     FROM(
            SELECT ROW_NUMBER() OVER () AS NUM,
                   T1.*
            FROM (
                SELECT A.SVC_APP_VER_SEQ,
                       A.SVC_SEQ,
                       B.SVC_NM,
                       B.SVC_TYPE,
                       A.APP_TYPE,
                       A.SVC_APP_VER,
                       C.COMN_CD_NM AS DL_STORE_TYPE,
                       A.DL_STORE_PATH,
                       A.REGR_ID,
                       M.MBR_NM,
                       A.REG_DT,
                       A.UPT_SBST,
                       A.USE_YN
                FROM
                    IM_SERVICE_APP_VERSION A
                    INNER JOIN IM_SERVICE B ON A.SVC_SEQ = B.SVC_SEQ  AND B.DEL_YN = 'N' AND B.SVC_TYPE = 'ON'
                    LEFT JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'APP_DL_STORE' AND A.DL_STORE_TYPE = C.COMN_CD_VALUE
                    LEFT JOIN IM_MEMBER M ON M.MBR_ID = A.REGR_ID
                WHERE
                    B.SVC_TYPE = 'ON'
                <if test="svcSeq gt 0">
                    AND A.SVC_SEQ  = #{svcSeq}
                </if>
                <if test="appType != null and appType !=''">
                    AND A.APP_TYPE  = #{appType}
                </if>
                <if test="searchConfirm == 'true'">
                    AND
                    <![CDATA[UPPER(A.SVC_APP_VER) like CONCAT('%' || UPPER(#{keyword}),'%')]]>
                </if>

                <choose>
                    <when test="sidx == 'SVC_APP_VER'">
                        ORDER BY A.SVC_APP_VER <if test="sord == 'desc'">DESC</if>
                    </when>
                    <when test="sidx == 'SVC_NM'">
                        ORDER BY B.SVC_NM <if test="sord == 'desc'">DESC</if>
                    </when>
                    <when test="sidx == 'DL_STORE_TYPE'">
                        ORDER BY A.DL_STORE_TYPE <if test="sord == 'desc'">DESC</if>
                    </when>
                    <when test="sidx == 'DL_STORE_PATH'">
                        ORDER BY A.DL_STORE_PATH <if test="sord == 'desc'">DESC</if>
                    </when>
                    <when test="sidx == 'MBR_NM'">
                        ORDER BY M.MBR_NM <if test="sord == 'desc'">DESC</if>
                    </when>
                    <when test="sidx == 'REG_DT'">
                        ORDER BY A.REG_DT <if test="sord == 'desc'">DESC</if>
                    </when>
                    <when test="sidx == 'USE_YN'">
                        ORDER BY A.USE_YN <if test="sord == 'desc'">DESC</if>
                    </when>
                    <otherwise>
                        ORDER BY SVC_APP_VER_SEQ <if test="sord == 'desc'">DESC</if>
                    </otherwise>
                </choose>
            ) T1
        ) SUC
        WHERE NUM BETWEEN #{offset} AND #{limit}
     </select>

    <!-- 쿼리명 : 서비스 앱 버전 리스트 정보
                설명 : 서비스 앱 버전 리스트 조회 개수를 가져오기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2019.04.12 이화진 최초생성
                작성자: 이화진
                최초작성일 : 2019.04.12
     -->
     <select id="serviceAppVersionListTotalCount" parameterType="ServiceAppVersionVO" resultType="Integer">
        SELECT
            COUNT(*)
        FROM(
            SELECT
                *
            FROM
                IM_SERVICE_APP_VERSION A
                INNER JOIN IM_SERVICE B ON A.SVC_SEQ = B.SVC_SEQ  AND B.DEL_YN = 'N' AND B.SVC_TYPE = 'ON'
                LEFT JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'APP_DL_STORE' AND A.DL_STORE_TYPE = C.COMN_CD_VALUE
                LEFT JOIN IM_MEMBER M ON M.MBR_ID = A.REGR_ID
        WHERE
            B.SVC_TYPE = 'ON'
            <if test="svcSeq gt 0">
                AND A.SVC_SEQ = #{svcSeq}
            </if>
            <if test="appType != null and appType !=''">
                AND
                A.APP_TYPE = #{appType}
            </if>
            <if test="searchConfirm == 'true'">
                AND
                <![CDATA[UPPER(A.SVC_APP_VER) like CONCAT('%' || UPPER(#{keyword}),'%')]]>
            </if>
        ) T1
     </select>

    <!-- 쿼리명 : 가장 최근 서비스 앱 버전 정보
                설명 : 가장 최근의 서비스 앱 버전 정보를 조회 하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2019.02.12 허용수 최초생성
                작성자: 허용수
                최초작성일 : 2019.02.12
     -->
    <select id="selectLatestServiceAppVersion" parameterType="ServiceAppVersionVO" resultType="ServiceAppVersionVO">
        SELECT
            SVC_APP_VER_SEQ,
            SVC_SEQ,
            SVC_NM,
            SVC_TYPE,
            APP_TYPE,
            SVC_APP_VER,
            DL_STORE_TYPE,
            DL_STORE_PATH,
            REGR_ID,
            REG_DT,
            AMDR_ID,
            AMD_DT,
            UPT_SBST,
            USE_YN
        FROM(
            SELECT
                A.SVC_APP_VER_SEQ,
                A.SVC_SEQ,
                B.SVC_NM,
                B.SVC_TYPE,
                A.APP_TYPE,
                A.SVC_APP_VER,
                C.COMN_CD_NM AS DL_STORE_TYPE,
                A.DL_STORE_PATH,
                A.REGR_ID,
                TO_CHAR(A.REG_DT, 'YYYY-MM-DD MM:SS') AS REG_DT,
                A.AMDR_ID,
                TO_CHAR(A.AMD_DT, 'YYYY-MM-DD MM:SS') AS AMD_DT,
                A.UPT_SBST,
                A.USE_YN
            FROM
                IM_SERVICE_APP_VERSION A
                INNER JOIN IM_SERVICE B ON A.SVC_SEQ = B.SVC_SEQ  AND B.DEL_YN = 'N'
                LEFT JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'APP_DL_STORE' AND A.DL_STORE_TYPE = C.COMN_CD_VALUE
                LEFT JOIN IM_MEMBER M ON M.MBR_ID = A.REGR_ID
            WHERE 
                A.SVC_SEQ = #{svcSeq}
                AND A.USE_YN = 'Y'
                <if test="appType != null and appType !=''">
                    <choose>
                        <when test="appType == 'SA'.toString()">
                            AND (A.APP_TYPE = #{appType} OR A.APP_TYPE IS NULL)
                        </when>
                        <otherwise>
                            AND A.APP_TYPE = #{appType}
                        </otherwise>
                    </choose>
                </if>
            ORDER BY SVC_APP_VER_SEQ DESC
            LIMIT 1
        ) T
    </select>

    <!-- 쿼리명 : 서비스 앱 버전 삭제
                설명 : 서비스 앱 버전을 삭제하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2019.04.03 이화진 최초생성
                작성자: 이화진
                최초작성일 : 2019.04.03
     -->
     <delete id="deleteServiceAppVersion" parameterType="ServiceAppVersionVO">
        DELETE FROM IM_SERVICE_APP_VERSION
        WHERE SVC_APP_VER_SEQ IN
            <foreach collection="deleteSvcAppVersionSeqList" item="deleteSvcAppVerSeq" index="index" open="(" close=")" separator=",">
                #{deleteSvcAppVerSeq}
            </foreach>
     </delete>

    <!-- 쿼리명 : 서비스 앱 버전 수정
                설명 : 서비스 앱 버전을 수정하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2019.04.03 이화진 최초생성
                작성자: 이화진
                최초작성일 : 2019.04.03
     -->
    <update id="updateServiceAppVersion" parameterType="ServiceAppVersionVO">
    UPDATE
        IM_SERVICE_APP_VERSION
    SET
        DL_STORE_PATH = #{dlStorePath},
        USE_YN = #{useYn},
        AMDR_ID = #{amdrId},
        AMD_DT = NOW()
        <if test="uptSbst != null">
            , UPT_SBST = #{uptSbst}
        </if>
    WHERE
        SVC_APP_VER_SEQ = #{svcAppVerSeq}
    </update>

    <!-- 쿼리명 : 동일 정보 존재 유무 확인(버전명)
                설명 : 서비스 앱 버전 등록 또는 수정 시 동일 버전명이 존재하는지 확인하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2019.04.25 이화진 최초생성
                작성자: 이화진
                최초작성일 : 2019.04.25
     -->
     <select id="searchSvcAppVerInfo" parameterType="ServiceAppVersionVO" resultType="Integer">
        SELECT COUNT(*)
        FROM IM_SERVICE_APP_VERSION
        WHERE 1 = 1
            <choose>
                <when test="svcAppVer != null and svcAppVer != ''">
                    AND SVC_APP_VER = #{svcAppVer}
                </when>
            </choose>
            <if test="svcSeq != 0">
                AND SVC_SEQ IN (#{svcSeq})
            </if>
     </select>
</mapper>